<?php
   require_once("inc/session_start.php");
   require_once("functions.php");
   
   $html_array = load_page_html();
   
   require_once("inc/head.php");
?>
<!-- BEGIN BODY -->
<body class="fixed-top page-sidebar-fixed <?php echo $html_array['page_sidebar_closed']; ?>">
   <?php require_once("inc/top_menu.php"); ?>
   <!-- BEGIN CONTAINER -->   
   <div class="page-container row-fluid full-width-page">
      <!-- BEGIN PAGE -->
	  
      <?php require_once("inc/main_menu.php"); ?>
      <div class="page-content">

         <div class="container-fluid">
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid profile">
               <div class="span12">

                  <div class="portlet box blue">
                     <div class="portlet-title">
                        <div class="caption"><i class="icon-user"></i> <?php echo $html_array['profile']['first_name'] . " " . $html_array['profile']['last_name']; ?></div>
                     </div>

                     <div class="portlet-body">
                        <div class="span4">
                           <div class="profile_picture_wrapper"><?php echo $html_array['profile']['image']; ?></div>

                           <input type="hidden" id="user_id" value="<?php echo $_GET['id']; ?>" />
                           <?php echo $html_array['profile']['message_link']; ?>
                           <?php echo $html_array['profile']['profile_settings_link']; ?>

                           <?php echo $html_array['profile']['about']; ?>


                           <table class="profile_info_table"> 
                               <?php echo $html_array['profile']['info_table']; ?>
                           </table>

                           <br>
                           <?php echo $html_array['profile']['interests']; ?>

                           
                        </div>
                        <div class="span8">
                           <h3>Activity</h3>
                           <hr class="grey">

                           <ul class="timeline">


                                 <?php echo $html_array['user_timeline']; ?>

 
                           </ul>
                           
                        </div>

                        <div class="clearfix"></div>
                     </div>
                  </div>
                  <!--end span9-->
               </div>


            </div>
            <!-- END PAGE CONTENT--> 
         </div>
         <!-- END PAGE CONTAINER-->       
      </div>
      <!-- BEGIN PAGE -->
   </div>
   <!-- END PAGE CONTAINER--> 
   <!-- END CONTAINER -->
   <?php
      require_once("inc/footer.php");
   ?>
</body>
<!-- END BODY -->
</html>
<?php
   require_once("inc/session_end.php");
?>