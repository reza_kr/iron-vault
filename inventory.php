<?php
   require_once("inc/session_start.php");
   require_once("functions.php");

   $html_array = load_page_html();

   require_once("inc/head.php");
?>
<!-- BEGIN BODY -->
<body class="fixed-top page-sidebar-fixed <?php echo $html_array['page_sidebar_closed']; ?>">
	<?php require_once("inc/top_menu.php"); ?>
	<!-- BEGIN CONTAINER -->   
	<div class="page-container row-fluid">

		<?php require_once("inc/main_menu.php"); ?>

	  <!-- BEGIN PAGE -->
		<div class="page-content">
			<div class="container-fluid">

				<!-- BEGIN PAGE CONTENT-->
				<div class="row-fluid">
					<div class="span3 hidden-print">

						<div class="portlet box blue categories_wrapper">
							<div class="portlet-title">
								<div class="caption"><i class="icon-sitemap"></i> Categories</div>
                <div class="tools">
                   <a href='#add_new_category_modal' data-toggle="modal"><i class="icon-plus" title="Add New Category"></i></a>
                </div>
							</div>

							<div class="portlet-body categories">

								<form class="form-inline" action="#" method="post">
									<input type="text" id="search_categories" class="m-wrap " placeholder="Search..." autocomplete="off" />
								</form>

								<ul class="ver-inline-menu categories_list">
									<?php echo $html_array['categories_list']['li']; ?>
								</ul>
							</div>
						</div>
					</div>
					<div class="span9">
						<!-- BEGIN EXAMPLE TABLE PORTLET-->
						<div class="portlet box blue">
							<div class="portlet-title">
								<div class="caption"><i class="icon-edit"></i>Products – <?php echo $html_array['products']['cat_name']; ?></div>
							</div>
							
							<div class="portlet-body no-more-tables products">
								
								<?php echo $html_array['products']['table']; ?>
								
							</div>
						</div>
						<!-- END EXAMPLE TABLE PORTLET-->

						

					</div>
					<!--end span9-->
				</div>


        <div id="add_new_category_modal" class="modal hide fade" tabindex="-1" data-focus-on="input:first">
          <form class="form-horizontal zero_margin" action="#" name="inventory_add_new_category" method="post">
            <div class="modal-header">
              <i class="icon-remove right" data-dismiss="modal" aria-hidden="true"></i>
              <h3><i class="icon-sitemap"></i>&nbsp;&nbsp;Add New Category</h3>
            </div>
            <div class="modal-body">
              <div class="control-group">
                <label class="control-label">Category Name</label>
                <div class="controls">
                  <input type="text" id="cat_name" name="cat_name" class="span8 medium">
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <input type="button" class="btn light-blue right add" value="Add Category" />
              <input type="button" data-dismiss="modal" class="btn" value="Cancel">
            </div>
          </form>
        </div>


				<div id="edit_item_modal" class="modal hide fade" tabindex="-1" data-focus-on="input:first">
					<form class="zero_margin" action="inventory?action=update_inventory_item" method="post" enctype="multipart/form-data" name="inventory_edit_item">

						<div class="modal-header">
							<i class="icon-remove right" data-dismiss="modal" aria-hidden="true"></i>
							<h3><i class="icon-pencil"></i>&nbsp;&nbsp;Edit Item</h3>
						</div>

						<div class="modal-body">

							<div class="row-fluid">

                               <div class="span6 ">
                                  <div class="control-group">
                                    <label class="control-label">Product Name</label>
                                     <div class="controls">
                                     	<input type="hidden" name="current_cat" class="m-wrap span12 current_cat" value="<?php echo $_GET['cat_id']; ?>" />
                                        <input type="hidden" id="item_id" name="item_id" class="m-wrap span12" />
                                        <input type="text" id="item_name" name="item_name" class="m-wrap span12" />
                                      </div>
                                  </div>
                               </div>
                               <!--/span-->
                               <div class="span6 ">
                                  <div class="control-group">
                                     <label class="control-label">Product SKU/ID</label>
                                     <div class="controls">
                                        <input type="text" id="product_id" name="product_id" class="m-wrap span12" />
                                     </div>
                                  </div>
                               </div>
                               <!--/span-->
                            </div>

							<div class="row-fluid">

                               <div class="span6 ">
                                  <div class="control-group">
                                    <label class="control-label">Description</label>
                                     <div class="controls">
                                      <input type="text" id="item_desc" name="item_desc" class="m-wrap span12" />
                                     </div>
                                  </div>
                               </div>
                               <!--/span-->
                               <div class="span6 ">
                                  <div class="control-group">
                                     <label class="control-label">Category</label>
                                     <div class="controls">
                                       <select name="category" id="category" class="m-wrap span12">

                                       </select>
                                     </div> 
                                  </div>
                               </div> 
                               <!--/span-->
                            </div>

							<div class="row-fluid">

                               <div class="span6 ">
                                  <div class="control-group">
                                    <label class="control-label">Quantity</label>
                                     <div class="controls">

                                      <input type="text" id="item_qty" name="item_qty" class="m-wrap span12 quantity" />

                                     </div>
                                  </div>
                               </div>
                               <!--/span-->
                               <div class="span6 ">
                                  <div class="control-group">
                                     <label class="control-label">Price</label>
                                     <div class="controls">

											<input type="text" id="item_cost" name="item_cost" class="m-wrap span12 currency" />	

                                     </div> 
                                  </div>
                               </div> 
                               <!--/span-->
                            </div>

                            <div class="row-fluid">

                               <div class="span6 ">
                                  <div class="control-group">
                                    <label class="control-label">Image</label>
                                     <div class="controls">

                                      	<div class="fileupload fileupload-new" data-provides="fileupload">
                                          <div class="input-append">
                                             <div class="uneditable-input">
                                                <i class="icon-file fileupload-exists"></i> 
                                                <span class="fileupload-preview"></span>
                                             </div>
                                             <span class="btn btn-file">
                                                <span class="fileupload-new">Select file</span>
                                                <span class="fileupload-exists">Change</span>
                                                <input type="hidden" name="MAX_FILE_SIZE" value="2097152" />
                                                <input name="image_file" type="file" />
                                             </span>
                                             <a href="#" class="btn fileupload-exists remove" data-dismiss="fileupload">Remove</a>
                                          </div>
                                        </div>

                                     </div>
                                  </div>
                               </div>
                               <!--/span-->
                            </div>	

						</div>	

						<div class="modal-footer">
							<input type="submit" id="submit" class="btn light-green right" value="Update" />
							<input type="button" data-dismiss="modal" class="btn" value="Cancel" />
						</div>


					</form>	
				</div>


        <div id="add_new_item_modal" class="modal hide fade" tabindex="-1" data-focus-on="input:first">
          <form class="zero_margin" action="inventory?action=add_inventory_item" method="post" enctype="multipart/form-data" name="inventory_add_new_item">

            <div class="modal-header">
              <i class="icon-remove right" data-dismiss="modal" aria-hidden="true"></i>
              <h3><i class="icon-plus"></i>&nbsp;&nbsp;Add New Item</h3>
            </div>

            <div class="modal-body">

              <div class="row-fluid">

                               <div class="span6 ">
                                  <div class="control-group">
                                    <label class="control-label">Product Name</label>
                                     <div class="controls">
                                       <input type="hidden" name="current_cat" class="m-wrap span12 current_cat" value="<?php echo $_GET['cat_id']; ?>" />
                                       <input type="text" name="item_name" class="m-wrap span12" />
                                      </div>
                                  </div>
                               </div>
                               <!--/span-->
                               <div class="span6 ">
                                  <div class="control-group">
                                     <label class="control-label">Product SKU/ID</label>
                                     <div class="controls">
                                        <input type="text" name="product_id" class="m-wrap span12" />
                                     </div>
                                  </div>
                               </div>
                               <!--/span-->
                            </div>

              <div class="row-fluid">

                               <div class="span6 ">
                                  <div class="control-group">
                                    <label class="control-label">Description</label>
                                     <div class="controls">
                                      <input type="text" name="item_desc" class="m-wrap span12" />
                                     </div>
                                  </div>
                               </div>
                               <!--/span-->
                               <div class="span6 ">
                                  <div class="control-group">
                                     <label class="control-label">Category</label>
                                     <div class="controls">
                                       <select name="category" id="category" class="m-wrap span12">

                                       </select>
                                     </div> 
                                  </div>
                               </div> 
                               <!--/span-->
                            </div>

              <div class="row-fluid">

                               <div class="span6 ">
                                  <div class="control-group">
                                    <label class="control-label">Quantity</label>
                                     <div class="controls">

                                        <input type="text" name="item_qty" class="m-wrap span12 quantity" />

                                     </div>
                                  </div>
                               </div>
                               <!--/span-->
                               <div class="span6 ">
                                  <div class="control-group">
                                     <label class="control-label">Price</label>
                                     <div class="controls">

                      <input type="text" name="item_cost" class="m-wrap span12 currency" /> 

                                     </div> 
                                  </div>
                               </div> 
                               <!--/span-->
                            </div>                                        

                            <div class="row-fluid">

                               <div class="span6 ">
                                  <div class="control-group">
                                    <label class="control-label">Image</label>
                                     <div class="controls">

                                        <div class="fileupload fileupload-new" data-provides="fileupload">
                                          <div class="input-append">
                                             <div class="uneditable-input">
                                                <i class="icon-file fileupload-exists"></i> 
                                                <span class="fileupload-preview"></span>
                                             </div>
                                             <span class="btn btn-file">
                                                <span class="fileupload-new">Select file</span>
                                                <span class="fileupload-exists">Change</span>
                                                <input type="hidden" name="MAX_FILE_SIZE" value="2097152" />
                                                <input name="image_file" type="file" />
                                             </span>
                                             <a href="#" class="btn fileupload-exists remove" data-dismiss="fileupload">Remove</a>
                                          </div>
                                        </div>

                                     </div>
                                  </div>
                               </div>
                               <!--/span-->
                            </div>  

            </div>  

            <div class="modal-footer">
              <input type="submit" id="submit" class="btn light-green right" value="Add Item" />
              <input type="button" data-dismiss="modal" class="btn" value="Cancel" />
            </div>

          </form> 
        </div>

				<div id="add_multiple_items_modal" class="modal hide fade container" tabindex="-1" data-focus-on="input:first">
					<form class="zero_margin" action="inventory?action=add_multiple_inventory_items" method="post" name="inventory_add_multiple_items">

						<input type="hidden" name="current_cat" class="m-wrap span12 current_cat" value="<?php echo $_GET['cat_id']; ?>" />

						<div class="modal-header">
							<i class="icon-remove right" data-dismiss="modal" aria-hidden="true"></i>
							<h3><i class="icon-plus"></i>&nbsp;&nbsp;Add Multiple Items</h3>
						</div>

						<div class="modal-body">

							<?php 

							for($x = 1; $x < 6; $x++) {

								echo '<div class="row-fluid">

	                                   <div class="span2 ">
	                                      <div class="control-group">
	                                        <label class="control-label">Product Name</label>
	                                         <div class="controls">
	                                           <input type="text" name="item_name[' . $x . ']" class="m-wrap span12" />
	                                          </div>
	                                      </div>
	                                   </div>
	                                   <!--/span-->
	                                   <div class="span2 ">
	                                      <div class="control-group">
	                                         <label class="control-label">Product SKU/ID</label>
	                                         <div class="controls">
	                                            <input type="text" name="product_id[' . $x . ']" class="m-wrap span12" />
	                                         </div>
	                                      </div>
	                                   </div>
	                                   <!--/span-->

	                                   <div class="span2 ">
	                                      <div class="control-group">
	                                        <label class="control-label">Description</label>
	                                         <div class="controls">
	                                          <input type="text" name="item_desc[' . $x . ']" class="m-wrap span12" />
	                                         </div>
	                                      </div>
	                                   </div>
	                                   <!--/span-->
	                                   <div class="span2 ">
	                                      <div class="control-group">
	                                         <label class="control-label">Category</label>
	                                         <div class="controls">
	                                           <select name="category[' . $x . ']" id="category" class="m-wrap span12">';

	                                           echo '</select>
	                                         </div> 
	                                      </div>
	                                   </div> 
	                                   <!--/span-->

	                                   <div class="span2 ">
	                                      <div class="control-group">
	                                        <label class="control-label">Quantity</label>
	                                         <div class="controls">

	                                          <input type="text" name="item_qty[' . $x . ']" class="m-wrap span12 quantity" />

	                                         </div>
	                                      </div>
	                                   </div>
	                                   <!--/span-->
	                                   <div class="span2 ">
	                                      <div class="control-group">
	                                         <label class="control-label">Price</label>
	                                         <div class="controls">

													<input type="text" name="item_cost[' . $x . ']" class="m-wrap span12 currency" />	

	                                         </div> 
	                                      </div>
	                                   </div> 
	                                   <!--/span-->

	                                </div>';
	                        }

	                    	?>
						</div>	

						<div class="modal-footer">
							<input type="submit" id="submit" class="btn light-green right" value="Add Items" />
							<input type="button" data-dismiss="modal" class="btn" value="Cancel" />
						</div>


					</form>	
				</div>


				<div id="delete_item_modal" class="modal hide fade" tabindex="-1" data-focus-on="input:first">

					<form class="zero_margin" action="#" method="post">

						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							<h3><i class="icon-remove"></i>&nbsp;&nbsp;Delete Item</h3>
						</div>

						<div class="modal-body">

						</div>	
							
						<div class="modal-footer">
							<input type="submit" id="submit" class="btn light-red right delete" value="Delete" />
							<input type="button" data-dismiss="modal" class="btn" value="Cancel" />
						</div>

					</form>

				</div>


				<div id="edit_category_modal" class="modal hide fade" tabindex="-1" data-focus-on="input:first">

					<form class="zero_margin" action="#" method="post">


						<div class="modal-header">
							<i class="icon-remove right" data-dismiss="modal" aria-hidden="true"></i>
							<h3><i class="icon-user"></i>&nbsp;&nbsp;Edit Category</h3>
						</div>

						<div class="modal-body">

							<div class="row-fluid">

                               <div class="span6 ">
                                  <div class="control-group">
                                    <label class="control-label">Category Name</label>
                                     <div class="controls">
                                       <input type="text" class="cat_name" name="cat_name" class="m-wrap span12" />
                                       <input type="hidden" class="cat_id" name="cat_id" class="m-wrap span12" />
                                      </div>
                                  </div>
                               </div>

                               <!--/span-->
                            </div>


						</div>	
							
						<div class="modal-footer">
							<a href="#delete_category_modal"  data-dismiss="modal" data-toggle="modal" class="btn light-red left">Delete</a>
							<input type="submit" id="submit" class="btn light-green right save" value="Save" />
							<input type="button" data-dismiss="modal" class="btn right" value="Cancel" />
							
						</div>

					</form>

				</div>	



				<div id="delete_category_modal" class="modal hide fade" tabindex="-1" data-focus-on="input:first">

					<form class="zero_margin" action="#" method="post">

						<div class="modal-header">
							<i class="icon-remove right" data-dismiss="modal" aria-hidden="true"></i>
							<h3><i class="icon-user"></i>&nbsp;&nbsp;Delete Category</h3>
						</div>

						<div class="modal-body">

						Are you sure you want to delete this Category?
            <br><br>
            <i class="icon-warning-sign"></i> Any items within the category will be moved to 'Uncategorized'
                            <input type="hidden" class="cat_id" name="cat_id" class="m-wrap span12" />

						</div>	
							
						<div class="modal-footer">
							<input type="submit" id="submit" class="btn light-red right delete" value="Delete" />
							<input type="button" data-dismiss="modal" class="btn" value="Cancel" />
						</div>

					</form>

				</div>



        <div id="item_not_found_modal" class="modal hide fade" tabindex="-1" data-focus-on="input:first">

          <form class="zero_margin" action="#" method="post">

            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
              <h3><i class="icon-search"></i>&nbsp;&nbsp;Item Not Found</h3>
            </div>

            <div class="modal-body">
              The item you were looking for could not be found. Could it be that it was previously removed?<br><br>

              Try searching for it on this page!
            </div>  
              
            <div class="modal-footer">
              <input type="button" data-dismiss="modal" class="btn right" value="Close" />
            </div>

          </form>

        </div>


			</div>
		<!-- END PAGE CONTENT -->
		</div>

		<!-- END PAGE CONTAINER-->       
	</div>
	<!-- END PAGE CONTAINER--> 
	<!-- END CONTAINER -->
	<?php
	  require_once("inc/footer.php");
	?>
</body>
<!-- END BODY -->
</html>
<?php
   require_once("inc/session_end.php");
?>