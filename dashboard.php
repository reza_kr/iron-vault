<?php
   require_once("inc/session_start.php");
   require_once("functions.php");

   $html_array = load_page_html();

   require_once("inc/head.php");
   
?>
<!-- BEGIN BODY -->
<body class="fixed-top page-sidebar-fixed <?php echo $html_array['page_sidebar_closed']; ?>">
   <?php require_once("inc/top_menu.php"); ?>
   <!-- BEGIN CONTAINER -->   
   <div class="page-container row-fluid">

      <?php require_once("inc/main_menu.php"); ?>

      <!-- BEGIN PAGE -->
      <div class="page-content">
         <div class="container-fluid">
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
               <div class="span8">

                  	<div class="row-fluid">

               			<div class="portlet box blue">

		                    <div class="portlet-title line">
		                        <div class="caption"><i class="icon-bullhorn"></i> Announcements</div>
		                        <div class="tools">
		                            <i class="icon-plus show_new_announcement_form" title="New Announcement"></i>
		                        </div>
		                    </div>
		                    <div class="portlet-body announcements">
					                    
			                    <ul class="timeline">
			                    	<?php echo $html_array['announcements']; ?>
			                  	</ul>
		               			
			                    <!-- <h3>Personal</h3>

			                    <div class="row-fluid">

			                   		<div class="span4">
					                    Unread Messages
					                    <span class="unread_messages_count large_num"></span>
					                </div>

			                    	<div class="span4">
					                    Events Scheduled – Today
					                    <span class="events_count_today large_num"></span>
					                </div>

					                <div class="span4">
					                    Tasks Due – Today
					                    <span class="tasks_count_today large_num"></span>
					                </div>
				                </div>
			                    
			                    <hr>

			                    <h3>General</h3>

			                    <div class="row-fluid">

			                    	<div class="span4">
					                    Invoices Processed – Today
					                    <span class="invoices_processed_today large_num"></span>
					                </div>

					                <div class="span4">
					                    Invoices Processed – Lifetime
					                    <span class="invoices_processed_lifetime large_num"></span>
					                </div>

					                <div class="span4">
					                    Inventory Items
					                    <span class="inventory_items large_num"></span>
					                </div>
				                </div>

				                <div class="spacer_10"></div>
				                <div class="spacer_10"></div>

				                <div class="row-fluid">

				                	<div class="span4">
					                    Open Invoices
					                    <span class="open_invoices large_num"></span>
					                </div>

					                <div class="span4">
					                    Overdue Invoices
					                    <span class="overdue_invoices large_num"></span>
					                </div>

			                    	<div class="span4">
					                    Total Clients
					                    <span class="total_clients large_num"></span>
					                </div>

				                </div>

				                <hr>

				                <h3>Income &amp; Expenses</h3>

			                    <div class="row-fluid">

			                    	<div class="span12">

			                    		<div class="chart_wrapper">
			                    			<div class="income_expenses_chart"></div>
			                    		</div>
			                    	</div>

			                    </div> -->
			                </div>
			            </div>
               		</div>


               		<div class="row-fluid">
		                <!-- BEGIN PORTLET-->
		                <div class="portlet box blue chat">

		                    <div class="portlet-title line">
		                        <div class="caption"><i class="icon-comment"></i> Open Chat</div>
		                        <div class="tools">
		                            <i class="icon-external-link-sign popout" title="Pop-out Chat Window"></i>
		                        </div>
		                    </div>
		                    <div class="portlet-body" id="chats">
		                        <div class="stretch_width">The Open Chat is available to all the account holders within a business. The messages sent here are visible to anyone viewing them while logged in to their individual accounts. If you wish to send private messages to your peers, please use the Messages portal instead.</div>
		                        <div class="scroller" data-always-visible="1" data-rail-visible1="1">
		                            <ul class="chats">

		                            </ul>
		                        </div>
		                        <div class="chat-form">
		                            <div class="input-cont">   
		                              	<textarea class="m-wrap message_text" placeholder="Type a message..."></textarea>
		                              	<input class="first_name" type="hidden" value="<?php echo $_SESSION['first_name']; ?>" />
		                              	<input class="last_name" type="hidden" value="<?php echo $_SESSION['last_name']; ?>" />
		                              	<input class="image" type="hidden" value="<?php echo $_SESSION['image_thumb']; ?>" />
		                              	<input class="user_id" type="hidden" value="<?php echo $_SESSION['user_id']; ?>" />
		                              	<input class="account_id" type="hidden" value="<?php echo $_SESSION['account_id']; ?>" />
		                            </div>                    

		                           	<div class="btn-cont"> 
		                              	<a href="" class="btn light-blue icn-only send_message"><i class="icon-ok"></i> Send</a>
		                           	</div>

		                           	<?php echo $html_array['chat_sound_btn']; ?>

		                        </div>

		                        <audio id="chat_notif">
		                           	<source src="audio/notif.ogg" type="audio/ogg">
		                           	<source src="audio/notif.mp3" type="audio/mpeg">
		                        </audio>
		                    </div>
		                </div>
		                <!-- END PORTLET-->
	                </div>

               </div>

               <div class="span4">

               		<div class="row-fluid">

               			<div class="portlet box blue">

		                    <div class="portlet-title line">
		                        <div class="caption"><i class="icon-group"></i> Activity</div>
		                        <!-- <div class="tools">
									<a href="javascript:;" class="expand"></a>
								</div> -->
		                    </div>
		                    <div class="portlet-body activity_timeline">
		               			<ul class="timeline">
			                    	<?php echo $html_array['timeline']; ?>
			                  	</ul>
			                </div>
			            </div>
               		</div>

               </div>
            </div>
            <!-- END PAGE CONTENT--> 
         </div>
         <!-- END PAGE CONTAINER-->       
      </div>
      <!-- BEGIN PAGE -->
   </div>
   <!-- END PAGE CONTAINER--> 
   <!-- END CONTAINER -->
   <?php
      require_once("inc/footer.php");
   ?>

</body>
<!-- END BODY -->
</html>
<?php
   require_once("inc/session_end.php");
?>