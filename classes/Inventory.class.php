<?php

	session_start();

	require_once('error_handler.php');

	
	Class Inventory { 



		public function __construct () {

			require("inc/connect_dbo.php");

			$this->dbo = $dbo;

		}


		public function loadProduct() {

			$account_id = $_SESSION['account_id'];

			if($_GET['id'] || $_GET['action']) {
				//let the ACTION parameter pass through... don't redirect it to inventory.php
				$item_id = $_GET['id'];

			} else {

				header("Location: inventory.php");
				
			}

			$found = false;

			$query = $this->dbo->prepare("SELECT item_id, item_name, product_id, item_qty, item_cost, category, item_desc, image, qr_code, date_added FROM inventory WHERE account_id = ? AND item_id = ?");
			$query->execute(array($account_id, $item_id));

			$result = $query->fetchAll();

			foreach($result as $row) {

				$found = true;

	    		$product['item_id'] = $row[0];
	    		$product['item_name'] = $row[1];
	    		$product['product_id'] = $row[2];
	    		$product['item_qty'] = $row[3];
	    		$product['item_cost'] = $row[4];
	    		$product['cat_id'] = $row[5];
	    		$product['item_desc'] = $row[6];
	    		$product['date_added'] = $row[9];

	    		$item_image = $row[7];

	    		if($item_image != "") { //Image field
	    			$product['item_image'] = '<a href="https://cdn.ironvault.ca/item_imgs/' . $item_image . '" class="fancybox"><img src="https://cdn.ironvault.ca/item_imgs/' . $item_image . '" alt="' . $product['item_name'] . '" /></a><br><a href="#" class="remove_product_image" id="' . $product['item_id'] . '">Remove Image</a>';
	    		}


	    		$qr_code = $row[8];

	    		if($qr_code != "") { //Image field
	    			$product['item_qr_code'] = '<hr><img src="https://cdn.ironvault.ca/item_qr/' . $qr_code . '" alt="QR Code" /><hr>';
	    		} else {
	    			$product['item_qr_code'] = '<hr><a href="#" class="generate_qr_code" id="' . $product['item_id'] . '">Generate QR Code</a><hr>';
	    		}
			}


			if($found == false) {

				header('Location: inventory?e=item_not_found');
			}


	    	$product['option'] = "";


	    	$query = $this->dbo->prepare("SELECT id, cat_name FROM categories WHERE account_id = ?");
			$query->execute(array($account_id));

			$result = $query->fetchAll();

			foreach($result as $row) {

	    		$cat_id = $row[0];
	    		$cat_name = $row[1];


	    		if($cat_id == $product['cat_id']) {

	    			$product['option'] .= '<option value="' . $cat_id . '" selected="selected">' . $cat_name . '</option>';
	    			$product['category'] = $cat_name;

	    		}
	    		else {

	    			$product['option'] .= '<option value="' . $cat_id . '">' . $cat_name . '</option>';
	    		}

			} 



			//Prepare date range array
			$dataset = array();

			$start_date = date('Y-m-d', strtotime($product['date_added']));
			$current_date = $start_date;
			$end_date = date('Y-m-d');

			while ($current_date >= $start_date && $current_date <= $end_date) {

				$dataset[$current_date] = array(strtotime($current_date) * 1000, 0);

				$current_date = date('Y-m-d', strtotime($current_date . ' + 1 day'));
			}


			//Sales History
			$query = $this->dbo->prepare("SELECT invoices.date_processed, invoice_items.item_qty, invoice_items.item_cost
										FROM invoices
										INNER JOIN invoice_items
										ON invoices.id = invoice_items.invoice_id AND invoices.account_id = invoice_items.account_id
										WHERE invoices.account_id = ? AND invoices.status = ? AND invoice_items.item_id = ?;");
			$query->execute(array($account_id, 'processed', $item_id));

			$result = $query->fetchAll();

			foreach($result as $row) {

				$timestamp = strtotime($row[0]);
				$invoice_date = date('Y-m-d', strtotime($row[0]));
				
				$date = date('Y-m-d', $timestamp); //get rid of the time, only keep the date
				$timestamp = strtotime($date) * 1000; //convert back to timestamp
				// echo $timestamp; exit;
				$qty = $row[1];

				$dataset[$invoice_date] = array($timestamp, $qty);
			}
			// echo '<pre>';
			// echo $start_date . '<br>';
			// echo $current_date . '<br>';
			// echo $end_date . '<br>';
			// print_r($dataset); exit;

			$product['sales_history'] = json_encode(array_values($dataset));
  
			return $product;

	    }



	    public function loadProducts() {

	    	require_once("Account.class.php");
			$account = new Account();

	    	$account_id = $_SESSION['account_id'];
	    	$cat_id = $_POST['cat_id'];

	    	$products = array();

			if($cat_id == "") { //If there is no cat_id in POST....

				$cat_id_in_url = $_GET['cat_id'];

				if($cat_id_in_url) { //If there is a cat_id in GET....


					$query = $this->dbo->prepare("SELECT cat_name FROM categories WHERE id = ?");
					$query->execute(array($cat_id_in_url));

					$result = $query->fetchAll();

					foreach($result as $row) {
						$cat_name = $row[0];
					}
					
					if($cat_name != "") { //Checking to see if the cat_id is valid....
	    				
	    				$query = $this->dbo->prepare("SELECT * FROM inventory WHERE account_id = ? AND category = ?");
	    				$query->execute(array($account_id, $cat_id_in_url));

	    			} else { //Else redirect....

	    				header('Location: inventory');
	    			}

	    		} else { //Else show all products....

	    			$query = $this->dbo->prepare("SELECT * FROM inventory WHERE account_id = ?");
	    			$query->execute(array($account_id));
	    		}



	    	} else if ($cat_id == "-1") {
	    		
	    		$query = $this->dbo->prepare("SELECT * FROM inventory WHERE account_id = ?");
	    		$query->execute(array($account_id));

	    	} else if ($cat_id == "uncat") {
	    		
	    		$query = $this->dbo->prepare("SELECT * FROM inventory WHERE account_id = ? AND category = ?");
	    		$query->execute(array($account_id, ''));

	    	} else {

	    		$query = $this->dbo->prepare("SELECT * FROM inventory WHERE account_id = ? AND category = ?");
	    		$query->execute(array($account_id, $cat_id));
	    	}




	    	if($account->hasExportAccess() == true) {
				$export_tools = '<div class="btn-group pull-right tools">
									<button class="btn dropdown-toggle" data-toggle="dropdown">Tools <i class="icon-angle-down"></i>
									</button>
									<ul class="dropdown-menu pull-right">
										<li><a href="#" class="print">Print</a></li>
										<li><a href="#" class="save_as_pdf">Save as PDF</a></li>
										<li><a href="#" class="export_to_excel">Export as CSV (Excel)</a></li>
									</ul>
								</div>';
			}



	    	$products['table'] = '<div class="clearfix">
				<div class="btn-group">
					<a class="btn light-green" href="#" data-toggle="dropdown">
					<i class="icon-plus"></i> Add Item
					<i class="icon-angle-down"></i>
					</a>
					<ul class="dropdown-menu">
						<li><a data-toggle="modal" href="#add_new_item_modal" class="add_new_item">Single Item</a></li>
						<li><a data-toggle="modal" href="#add_multiple_items_modal" class="add_multiple_items">Multiple Items</a></li>
					</ul>
				</div>
					
				' . $export_tools . '
			</div>

			<table class="table table-bordered table-striped table-condensed cf inventory saveaspdf" id="data_table">
				<thead class="cf">
					<tr>
						<th class="sorting">Product Name</th>
						<th class="sorting">Product SKU/ID</th>
						<th class="sorting">Category</th>
						<th class="sorting">In Stock</th>
						<th class="sorting">Unit Price</th>
						<th>Action</th>

					</tr>
				</thead>
				<tbody>';


			$result = $query->fetchAll();

			foreach($result as $row) {

	    		$item_id = $row[1];
	    		$product_id = $row[2];
	    		$item_name = $row[3];
	    		$item_desc = $row[4];
	    		$category = $row[5];
	    		$item_qty = $row[6];
	    		$item_cost = $row[7];

	    		$query2 = $this->dbo->prepare("SELECT cat_name FROM categories WHERE id = ?");
				$query2->execute(array($category));

				$result2 = $query2->fetchAll();

				foreach($result2 as $row2) {
					$item_cat_name = $row2[0];
				}


	    		$products['table'] .= '<tr class="" id="' . $item_id . '">
					<td data-title="Product Name">' . $item_name . '</td>
					<td data-title="Product SKU/ID">' . $product_id . '</td>
					<td data-title="Category">' . $item_cat_name . '</td>
					<td data-title="In Stock">' . $item_qty . '</td>
					<td data-title="Unit Price">$' . $item_cost . '</td>
					<td data-title="Action" class="button">
						<div class="btn-group">
							<button class="btn light-blue dropdown-toggle" data-toggle="dropdown">Action <i class="icon-angle-down"></i>
							</button>
							<ul class="dropdown-menu pull-right">
								<li><a href="#edit_item_modal" data-toggle="modal" class="edit_item">Edit Item</a></li>
								<li><a href="#delete_item_modal" data-toggle="modal" class="delete_item">Delete Item</a></li>
							</ul>
						</div>
					</td>
					<input type="hidden" class="item_id" value="' . $item_id . '" />
					<input type="hidden" class="product_id" value="' . $product_id . '" />
					<input type="hidden" class="item_name" value="' . $item_name . '" />
					<input type="hidden" class="item_desc" value="' . $item_desc . '" />
					<input type="hidden" class="category" value="' . $category . '" />
					<input type="hidden" class="item_qty" value="' . $item_qty . '" />
					<input type="hidden" class="item_cost" value="' . $item_cost . '" />
				</tr>';

			} 

			$products['table'] .= '</tbody></table>';

			if($cat_id != "") { //If it's not an initial page load..., reinitialize the tableEditable.
			
				$products['table'] .= '<script>
								$(document).ready(function() {
									TableEditable.init();
								});
							</script>';
			}


			if($cat_name != "") {

				$products['cat_name'] = $cat_name;
			
			} else {

				$products['cat_name'] = "All";
			}


			return $products;

	    }





	    public function addInventoryItem() {

	    	require_once('scripts/aws.phar');
			
			$s3client = Aws\S3\S3Client::factory(array(
		    	'key'    => 'AKIAJKBMGCMLYBHN77PQ',
		    	'secret' => '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'
		    ));

	    	$page = basename($_SERVER['PHP_SELF']);

	    	if($page == "inventory.php") {

		    	if($_POST['current_cat']) {

		    		$param = "?cat_id=" . $_POST['current_cat'];
		    	}

		    	$location = $page . $param;
		    
		    } else if($page == "product.php") {

		    	$location = $page;
		    }


			$account_id = $_SESSION['account_id'];

			$item_name = $_POST['item_name'];	
			$product_id = $_POST['product_id'];
			$item_desc = $_POST['item_desc'];
			$category = $_POST['category'];
			$item_qty = $_POST['item_qty'];
			$item_cost = $_POST['item_cost'];


			$query = $this->dbo->prepare("INSERT INTO inventory SET account_id = ?, item_name = ?, product_id = ?, item_desc = ?, category = ?, item_qty = ?, item_cost = ?");
			$query->execute(array($account_id, $item_name, $product_id, $item_desc, $category, $item_qty, $item_cost));

			$item_id = $this->dbo->lastInsertId();


			if (($_FILES["image_file"]["type"] == "image/jpeg") || ($_FILES["image_file"]["type"] == "image/jpg") || ($_FILES["image_file"]["type"] == "image/png")) {
				
				$image_type = $_FILES["image_file"]["type"];
				$image_file = $_FILES["image_file"]["tmp_name"];

				
				$filename = $account_id . "_"  . $item_id . "_" . time() . ".jpg";
				
				$this->setProductImage($image_file, $filename, $image_type, 800, 800);


				//Store the main image...
				$s3client->putObject(array(
				    'ACL' => 'public-read',
				    'Bucket' => 'ironvault',
					'SourceFile' => 'tmp_files/' . $filename,
					'Key' => 'item_imgs/' . $filename,
					'ContentType' => 'image/jpeg',
					'StorageClass' => 'REDUCED_REDUNDANCY'
				));


				$query = $this->dbo->prepare("UPDATE inventory SET image = ? WHERE account_id = ? AND item_id = ?");
				$query->execute(array($filename, $account_id, $item_id));
				
			} 

			 
	    	header('Location: ' . $location);


	    }



	    public function updateInventoryItem() {

	    	require_once('scripts/aws.phar');
			
			$s3client = Aws\S3\S3Client::factory(array(
		    	'key'    => 'AKIAJKBMGCMLYBHN77PQ',
		    	'secret' => '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'
		    ));

	    	$account_id = $_SESSION['account_id'];

	    	$item_id = $_POST['item_id'];
			$item_name = $_POST['item_name'];	
			$product_id = $_POST['product_id'];
			$item_desc = $_POST['item_desc'];
			$category = $_POST['category'];
			$item_qty = $_POST['item_qty'];
			$item_cost = $_POST['item_cost'];

			$page = basename($_SERVER['PHP_SELF']);

	    	if($page == "inventory.php") {

		    	if($_POST['current_cat']) {

		    		$param = "?cat_id=" . $_POST['current_cat'];
		    	}

		    	$location = $page . $param;
		    
		    } else if($page == "product.php") {

		    	$location = $page . "?id=" . $item_id;

		    }

		    $query = $this->dbo->prepare("UPDATE inventory SET item_name = ?, product_id = ?, item_desc = ?, category = ?, item_qty = ?, item_cost = ? WHERE account_id = ? AND item_id = ?");
			$query->execute(array($item_name, $product_id, $item_desc, $category, $item_qty, $item_cost, $account_id, $item_id));


		    if (($_FILES["image_file"]["type"] == "image/jpeg") || ($_FILES["image_file"]["type"] == "image/jpg") || ($_FILES["image_file"]["type"] == "image/png")) {
				
				$image_type = $_FILES["image_file"]["type"];
				$image_file = $_FILES["image_file"]["tmp_name"];

				
				$filename = $account_id . "_"  . $item_id . "_" . time() . ".jpg";
				
				$this->setProductImage($image_file, $filename, $image_type, 800, 800);


				$query = $this->dbo->prepare("SELECT image FROM inventory WHERE account_id = ? AND item_id = ?");
				$query->execute(array($account_id, $item_id));

				$result = $query->fetchAll();

				foreach($result as $row) {

					$old_image = $row[0];
				}


				//Store the main image...
				$s3client->putObject(array(
				    'ACL' => 'public-read',
				    'Bucket' => 'ironvault',
					'SourceFile' => 'tmp_files/' . $filename,
					'Key' => 'item_imgs/' . $filename,
					'ContentType' => 'image/jpeg',
					'StorageClass' => 'REDUCED_REDUNDANCY '
				));


				$query = $this->dbo->prepare("UPDATE inventory SET image = ? WHERE account_id = ? AND item_id = ?");
				$query->execute(array($filename, $account_id, $item_id));

				$s3client->deleteObject(array(
				    'Bucket' => 'ironvault',
					'Key' => 'item_imgs/' . $old_image
				));
			}

			 
	    	header('Location: ' . $location);

	    }




	    public function setProductImage($image_file, $filename, $type, $max_width, $max_height) {
		
			if(file_exists($image_file)) {

					
				$path = "tmp_files/" . $filename;
				
				// if you upload a png you can use imagecreatefrompng
				if($type == "image/jpeg" || $type == "image/jpg") {
				
					$image = imagecreatefromjpeg($image_file);
					$srcwidth = ImagesX($image);
					$srcheight = ImagesY($image);
					if ($srcwidth < $max_width && $srcheight < $max_height) {
						
						// smaller than needed and will upload directly
						ImageJpeg($image, $path, 80);
						
						return $path;
						
					} else {
						// resize acording to aspect ratio
						if (($srcwidth/$srcheight) > ($max_width/$max_height)) {
							// assume that the width is bigger
							$new_width = $max_width;
							$new_height = round($max_width/$srcwidth*$srcheight);
						} else {
							// assume the height is bigger
							$new_width = round($max_height/$srcheight*$srcwidth);
							$new_height = $max_height;
						}
						$new_image = ImageCreateTrueColor($new_width, $new_height);

						ImageCopyResampled($new_image, $image, 0, 0, 0, 0, $new_width, $new_height, $srcwidth, $srcheight);
						
						
						// you can also use Imagepng
						ImageJpeg($new_image, $path, 80);
						
						//destroy tmp jpeg
						ImageDestroy($image);
						ImageDestroy($new_image);
						
						return $path;
					}
					
				} else if($type == "image/png") {
				
					$image = imagecreatefrompng($image_file);
					$srcwidth = ImagesX($image);
					$srcheight = ImagesY($image);
					if ($srcwidth < $max_width && $srcheight < $max_height) {
						
						// smaller than needed and will upload directly
						ImageJpeg($image, $path, 80);
						
						return $path;

						
					} else {
						// resize acording to aspect ratio
						if (($srcwidth/$srcheight) > ($max_width/$max_height)) {
							// assume that the width is bigger
							$new_width = $max_width;
							$new_height = round($max_width/$srcwidth*$srcheight);
						} else {
							// assume the height is bigger
							$new_width = round($max_height/$srcheight*$srcwidth);
							$new_height = $max_height;
						}

						$new_image = ImageCreateTrueColor($new_width, $new_height);

						ImageCopyResampled($new_image, $image, 0, 0, 0, 0, $new_width, $new_height, $srcwidth, $srcheight);
						
						// you can also use Imagepng
						ImageJpeg($new_image, $path, 80);
						
						//destroy tmp jpeg
						ImageDestroy($image);
						ImageDestroy($new_image);

						return $path;
					}
				
				}
				
			}
		}



		public function removeProductImage() {

			require_once('scripts/aws.phar');
			
			$s3client = Aws\S3\S3Client::factory(array(
		    	'key'    => 'AKIAJKBMGCMLYBHN77PQ',
		    	'secret' => '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'
		    ));

			$account_id = $_SESSION['account_id'];
			$item_id = $_POST['item_id'];

			$query = $this->dbo->prepare("SELECT image FROM inventory WHERE account_id = ? AND item_id = ?");
			$query->execute(array($account_id, $item_id));

			$result = $query->fetchAll();

			foreach($result as $row) {

	    		$filename = $row[0];
	    	}


	    	$s3client->deleteObject(array(
			    'Bucket' => 'ironvault',
				'Key' => 'item_imgs/' . $filename
			));

			$query = $this->dbo->prepare("UPDATE inventory SET image = ? WHERE account_id = ? AND item_id = ?");
			$query->execute(array('', $account_id, $item_id));

		}




	    public function deleteInventoryItem(){

	    	$account_id = $_SESSION['account_id'];

	    	$item_id = $_POST['item_id'];

	    	$query = $this->dbo->prepare("DELETE FROM inventory WHERE account_id = ? AND item_id = ?");
			$query->execute(array($account_id, $item_id));

			return true;

	    }



	    public function addMultipleInventoryItems() {


	    	$page = basename($_SERVER['PHP_SELF']);

	    	if($_POST['current_cat']) {

	    		$param = "?cat_id=" . $_POST['current_cat'];
	    	}

	    	$location = $page . $param;


	    	$account_id = $_SESSION['account_id'];

	    	foreach($_POST['item_name'] as $key => $value) {
			    //echo $key, ' => ', $value, '<br />';
			
		    	$item_name = $_POST['item_name'][$key];
		    	$product_id = $_POST['product_id'][$key];
		    	$item_desc = $_POST['item_desc'][$key];
		    	$category = $_POST['category'][$key];
		    	$item_qty = $_POST['item_qty'][$key];
		    	$item_cost = $_POST['item_cost'][$key];

		    	if($item_name != "" && $category != "") {

		    		$query = $this->dbo->prepare("INSERT INTO inventory SET account_id = ?, product_id = ?, item_name = ?, item_desc = ?, category = ?, item_qty = ?, item_cost = ?");
					$query->execute(array($account_id, $product_id, $item_name, $item_desc, $category, $item_qty, $item_cost));

			    }

	    	}

	    	header('Location: ' . $location);

	    }



	    public function addItemQty() {

	    	$account_id = $_SESSION['account_id'];

	    	$item_id = $_POST['item_id'];
	    	$item_qty = $_POST['qty'];

	    	//Not using PREPARED STATEMENT because it's too much of a hassle with adding qty to existing field
	    	$query = "UPDATE inventory 

				SET item_qty = item_qty + $item_qty 
 
				WHERE account_id = '$account_id' AND item_id = '$item_id' "; 
 
				$this->dbo->query($query);

			header('Location: product?id=' . $item_id);

	    }




	    public function loadProductQR() {

	    	$account_id = $_SESSION['account_id'];
	    	$product_id = $_POST['product_id'];

	    	$query = $this->dbo->prepare("SELECT qr_code FROM inventory WHERE account_id = ? AND item_id = ?");
			$query->execute(array($account_id, $product_id));

			$result = $query->fetchAll();

			foreach($result as $row) {

	    		$qr_code = $row[0];

	    		$data = '<hr><img src="https://cdn.ironvault.ca/item_qr/' . $qr_code . '" alt="qr_code" /><hr>';
	    	}


	    	if($data != "") {

	    		return $data;
	    	
	    	} else {

	    		return 'failed';
	    	}

	    }




	    //This one is used for ajax....
	    public function getCategories() {

	    	$account_id = $_SESSION['account_id'];
	    	$categories = '<option id="-1" value="" selected="selected">Select...</option>';

	    	$query = $this->dbo->prepare("SELECT id, cat_name FROM categories WHERE account_id = ?");
			$query->execute(array($account_id));

			$result = $query->fetchAll();

			foreach($result as $row) {

	    		$cat_id = $row[0];
	    		$cat_name = $row[1];

	    		$categories .= '<option id="' . $cat_id . '" value="' . $cat_id . '">' . $cat_name . '</option>';

			} 
			return $categories;

	    }

		
		//This one is used on page load....
	    public function loadCategories() {

	    	$account_id = $_SESSION['account_id'];
	    	$categories = array();

	    	$categories['li'] = '<li><a href="#" data-toggle="tab" id="-1"><i class="icon-sitemap"></i> All Categories</a></li>';
	    	$categories['option'] = "";

	    	$cat_id_in_url = $_GET['cat_id'];

	    	$query = $this->dbo->prepare("SELECT id, cat_name FROM categories WHERE account_id = ?");
			$query->execute(array($account_id));

			$result = $query->fetchAll();

			foreach($result as $row) {

	    		$cat_id = $row[0];
	    		$cat_name = $row[1];

	    		if($cat_id == $cat_id_in_url) {
	    			$categories['li'] .= '<li class="active"><a href="#' . $cat_id . '" data-toggle="tab" id="' . $cat_id . '"><i class="icon-sitemap"></i> ' . $cat_name . '</a><a data-toggle="modal" href="#edit_category_modal"><i class="icon-pencil right edit_category" id="' . $cat_id . '"></i></a></li>';
	    		
	    		} else {

	    			$categories['li'] .= '<li><a href="#' . $cat_id . '" data-toggle="tab" id="' . $cat_id . '"><i class="icon-sitemap"></i> ' . $cat_name . '</a><a data-toggle="modal" href="#edit_category_modal"><i class="icon-pencil right edit_category" id="' . $cat_id . '"></i></a></li>';
	    		}
	    		

	    		$categories['option'] .= '<option id="' . $cat_id . '" value="' . $cat_id . '">' . $cat_name . '</option>';

			} 

			$categories['li'] .= '<li><a href="#" data-toggle="tab" id="uncat"><i class="icon-sitemap"></i> Uncategorized</a></li>';

			return $categories;

	    }



	    public function updateCategory() {

	    	$account_id = $_SESSION['account_id'];
	    	$cat_name = $_POST['cat_name'];
	    	$cat_id = $_POST['cat_id'];

	    	$query = $this->dbo->prepare("UPDATE categories SET cat_name = ? WHERE account_id = ? AND id = ?");
			$query->execute(array($cat_name, $account_id, $cat_id));

	    	header('Location: inventory');
	    }



	    public function deleteCategory() {

	    	$account_id = $_SESSION['account_id'];
	    	$cat_id = $_POST['cat_id'];


	    	$query = $this->dbo->prepare("UPDATE inventory SET category = ? WHERE account_id = ? AND category = ?");
			$query->execute(array('', $account_id, $cat_id));


			$query = $this->dbo->prepare("DELETE FROM categories WHERE account_id = ? AND id = ?");
			$query->execute(array($account_id, $cat_id));

	    	header('Location: inventory');
	    }



	    public function addCategory() {

	    	$account_id = $_SESSION['account_id'];
	    	$cat_name = $_POST['cat_name'];

	    	$query = $this->dbo->prepare("INSERT INTO categories SET account_id = ?, cat_name = ?");
			$query->execute(array($account_id, $cat_name));

	    	$cat_id = $this->dbo->lastInsertId();

	    	$category = '<li>
	    					<a href="#' . $cat_id . '" data-toggle="tab" id="' . $cat_id . '"><i class="icon-sitemap"></i> ' . $cat_name . '</a>
	    					<a data-toggle="modal" href="#edit_category_modal"><i class="icon-pencil right edit_category" id="' . $cat_id . '" style="display: none !important"></i></a>
	    				</li>';

	    	return $category;

	    	header('Location: inventory');
	    }




		public function loadProductInvoices() {

	    	$account_id = $_SESSION['account_id'];

	    	if($_GET['id']) {

	    		$product_id = $_GET['id'];
	    	}


	    	$query = $this->dbo->prepare("SELECT invoice_id, item_qty FROM invoice_items WHERE account_id = ? AND item_id = ?");
			$query->execute(array($account_id, $product_id));

			$result = $query->fetchAll();

			foreach($result as $row) {

	    		$invoice_id = $row[0];
	    		$item_qty = $row[1];


	    		$query2 = $this->dbo->prepare("SELECT client_id, invoice_number, date_created, date_modified, created_by, status, total FROM invoices WHERE account_id = ? AND id = ?");
				$query2->execute(array($account_id, $invoice_id));

				$result2 = $query2->fetchAll();

				foreach($result2 as $row2) {

		    		$client_id = $row2[0];
		    		$invoice_number = $row2[1];
		    		$date_created = $row2[2];
		    		$date_modified = $row2[3];
		    		$created_by = $row2[4];
		    		$status = $row2[5];
		    		$total = $row2[6];


		    		$query3 = $this->dbo->prepare("SELECT first_name, last_name, business_name, type FROM clients WHERE account_id = ? AND client_id = ?");
					$query3->execute(array($account_id, $client_id));

					$result3 = $query3->fetchAll();

					foreach($result3 as $row3) {

		    			$first_name = $row3[0];
		    			$last_name = $row3[1];
		    			$business_name = $row3[2];
		    			$type = $row3[3];

		    			if($type == "individual") {

		    				$display_name = $first_name . " " . $last_name;
		    			} else if($type == "company") {

		    				$display_name = $business_name;
		    			}
		    		}


		    		$date_created = date('M j, Y – g:iA', strtotime($date_created) + $_SESSION['timezone_offset']);
		    		$date_modified = date('M j, Y – g:iA', strtotime($date_modified) + $_SESSION['timezone_offset']);


		    		$query4 = $this->dbo->prepare("SELECT first_name, last_name FROM users WHERE account_id = ? AND user_id = ?");
					$query4->execute(array($account_id, $created_id));

					$result4 = $query4->fetchAll();

					foreach($result4 as $row4) {

		    			$first_name = $row4[0];
		    			$last_name = $row4[1];
		    		}


		    		$status = str_replace("_", " ", $status);
		    		$status = ucwords($status);

		    	}


		    	$data .= '<tr id="' . $invoice_id . '">
                            <td class=" sorting_1">' . $invoice_number . '</td>
                            <td class=" ">' . $display_name . '</td> 
                            <td class=" ">' . $date_modified . '</td>
                            <td class=" ">' . $first_name . ' ' . $last_name . '</td>
                            <td class=" ">' . $item_qty . '</td>
                            <td class=" ">' . $status . '</td>
                            <td class=" ">' . $total . '</td>
                            <td data-title="Action" class="button">
                              <div class="btn-group">
                                <button class="btn light-blue dropdown-toggle" data-toggle="dropdown">Action <i class="icon-angle-down"></i>
                                </button>
                                <ul class="dropdown-menu pull-right">
                                  <li><a href="create_invoice.php?like=' . $invoice_id . '" data-toggle="modal" class="clone_invoice">Clone Invoice</a></li>
                                </ul>
                              </div>
                            </td>
                          </tr>';

			}

			return $data;
	    }

	}

?>