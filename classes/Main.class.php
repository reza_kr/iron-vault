<?php

	session_start();

	require_once('error_handler.php');

	
	Class Main { 

		public function __construct () {

			require("inc/connect_dbo.php");

			$this->dbo = $dbo;

		}


		// public function logVisitorIP() {

		// 	$ip = $_SERVER['REMOTE_ADDR'];
		// 	$page = basename($_SERVER['PHP_SELF']);

		// 	$ch = curl_init ("http://ipinfo.io/" . $ip);
		// 	curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
		// 	$ip_info = json_decode(curl_exec($ch));

		// 	$ip_address = $ip_info->{'ip'};
		// 	$hostname = $ip_info->{'hostname'};
		// 	$loc = $ip_info->{'loc'};
		// 	$org = $ip_info->{'org'};
		// 	$city = $ip_info->{'city'};
		// 	$region = $ip_info->{'region'};
		// 	$country = $ip_info->{'country'};

		// 	$datetime = date("Y-m-d H:i:s");

		// 	$query = $this->dbo->prepare("INSERT INTO ip_log SET ip = ?, hostname = ?, loc = ?, org = ?, city = ?, region = ?, country = ?, datetime = ?, page = ?");
		// 	$query->execute(array($ip_address, $hostname, $loc, $org, $city, $region, $country, $datetime, $page));

		// }



		public function getTimezoneDST($user_timezone, $time = null) {
		    if($time == null){
		        $time = gmdate('U');
		    }

		    $timezone = new DateTimeZone($user_timezone);

		    $transition = $timezone->getTransitions($time);

		    return $transition[0]['isdst'];
		}

		
		public function getTimezoneOffset($user_timezone, $time = null) {
			   
			if($user_timezone != '') {    

			    if($time == null){
			        $time = gmdate('U');
			    }

			    $timezone = new DateTimeZone($user_timezone);

			    $transition = $timezone->getTransitions($time);

			    return $transition[0]['offset'];

			} else {

				return '';
			}
		}

	}

?>