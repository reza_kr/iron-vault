<?php

	session_start();

	require_once('error_handler.php');



	class Chat {



		public function __construct () {

			require("inc/connect_dbo.php");

			$this->dbo = $dbo;

		}





		public function postNewMessage() {

			$account_id = $_SESSION['account_id'];

			$user_id = $_SESSION['user_id'];

			$message_text = $_POST['message_text'];

			$datetime = date('Y-m-d H:i:s');

			$query = $this->dbo->prepare("INSERT INTO chat SET account_id = ?, user_id = ?, message_text = ?, date = ?");
			$result = $query->execute(array($account_id, $user_id, $message_text, $datetime));


			if($result) {

				return true;

			} else {

				return false;
			}

		}




		//Load open chat messages....
		public function getMessages() {

			$account_id = $_SESSION['account_id'];
			$data = '';
			$message_count = 0;

			$last_retrieved_id = $_POST['last_retrieved_id'];

			if($last_retrieved_id) {

				$query = $this->dbo->prepare("SELECT * FROM (SELECT * FROM chat WHERE account_id = ? AND message_id < ? ORDER BY message_id DESC LIMIT 20) AS Last20 ORDER BY message_id ASC");
				$query->execute(array($account_id, $last_retrieved_id));

			} else {

				$query = $this->dbo->prepare("SELECT * FROM (SELECT * FROM chat WHERE account_id = ? ORDER BY message_id DESC LIMIT 20) AS Last20 ORDER BY message_id ASC");
				$query->execute(array($account_id));
			}	

			$result = $query->fetchAll();

			foreach($result as $row) {


				$image_url = "";


				$message_id = $row[0];

				$user_id = $row[2];

				$message_text = $row[3];

				$datetime = $row[4];

				$query = $this->dbo->prepare("SELECT first_name, last_name, gender, image, status FROM users WHERE account_id = ? AND user_id = ?");
				$query->execute(array($account_id, $user_id));

				$result = $query->fetchAll();

				foreach($result as $row) {


					$first_name = $row[0];

					$last_name = $row[1];

					$gender = $row[2];

					$image = $row[3];

					$status = $row[4];

				}



				if($user_id == $_SESSION['user_id']) {


					$class = "out";

				} else {


					$class = "in";

				}



				if($image) {


					$image_url = "https://cdn.ironvault.ca/pr_imgs/thumbs/" . $image;

				} else {


					if($gender == "male") {


						$image_url = "https://www.ironvault.ca/app/img/male_default_thumb.png";

					
					} else if($gender == "female") {


						$image_url = "https://www.ironvault.ca/app/img/female_default_thumb.png";

					} else {

						$image_url = "https://www.ironvault.ca/app/img/male_default_thumb.png";						
					}

					
				}


				$date = date('M j, Y', strtotime($datetime) + $_SESSION['timezone_offset']);
	    		$time = date('g:iA', strtotime($datetime) + $_SESSION['timezone_offset']);


	    		if($status == 'active') {

	    			$user_name = '<a href="profile.php?id=' . $user_id . '" class="name">' .$first_name. ' ' .$last_name. '</a>';
	    			$user_image = '<a href="profile.php?id=' . $user_id . '" class="name"><img class="avatar" alt="" src="' .$image_url. '" /></a>';

	    		} else {

	    			$user_name = $first_name. ' ' .$last_name;
	    			$user_image = '<img class="avatar" alt="" src="img/user_default_thumb.png" />';
	    		}


				$data .= '<li class="' .$class. '" id="' .$user_id. '">
	                         ' . $user_image . '
	                         <div class="message">
	                            <span class="arrow"></span>
	                            ' . $user_name . '
	                            <span class="datetime">on ' .$date. ' at ' .$time. '</span>
	                            <span class="body">
	                            ' .$message_text. '
	                            </span>
	                         </div>
	                      </li>';


	            if($message_count == 0) {

	            	$last_retrieved_id = $message_id; //the id  of the first message that is loaded in this batch
	            }

	            $message_count++;

			}

			if($data != '') {

            	$data = '<li class="ajax_load center"><a href="#" class="load_earlier_messages" id="' . $last_retrieved_id . '">Load Earlier Messages</a></li>' . $data;
            
            }

			return $data;

		}





		public function sendPrivateMessage() {

			$account_id = $_SESSION['account_id'];

			$user_id = $_SESSION['user_id'];

			$message_text = $_POST['message_text'];
			$convo_id = $_POST['convo_id'];

			$datetime = date('Y-m-d H:i:s');

			$query = $this->dbo->prepare("INSERT INTO messages SET account_id = ?, convo_id = ?, from_user = ?, message = ?, date = ?");
			$query->execute(array($account_id, $convo_id, $user_id, $message_text, $datetime));

			$query = $this->dbo->prepare("UPDATE conversations SET hide_from = ?, last_update = ? WHERE account_id = ? AND convo_id = ?");
			$result = $query->execute(array('', $datetime, $account_id, $convo_id));


			if($result) {

				return true;


			} else {

				return false;

			}

		}





		public function checkUserId($user_id) {

	    	$account_id = $_SESSION['account_id'];

	    	$query = $this->dbo->prepare("SELECT COUNT(*) FROM users WHERE account_id = ? AND user_id = ?");
			$query->execute(array($account_id, $user_id));

			$result = $query->fetchAll();

			foreach($result as $row) {

				$num_rows = $row[0];
			}

			if($num_rows > 0) {

				return true;
			
			} else {

				return false;
			}

	    }




	    public function checkConvoId($convo_id) {

	    	$account_id = $_SESSION['account_id'];

	    	$query = $this->dbo->prepare("SELECT COUNT(*) FROM conversations WHERE account_id = ? AND convo_id = ?");
			$query->execute(array($account_id, $convo_id));

			$result = $query->fetchAll();

			foreach($result as $row) {

				$num_rows = $row[0];
			}

			if($num_rows > 0) {

				return true;
			
			} else {

				return false;
			}

	    }




		public function findConversation() {

			$account_id = $_SESSION['account_id'];
			$user_id = $_SESSION['user_id'];

			$receiver_user_id = $_GET['u_id'];

			$users = $user_id . "," . $receiver_user_id;

			$found_convo_id = 0;

			//Not using PREPARED STATEMENT because it's too much hassle to make it work with FIND_IN_SET.... 
			$query = "SELECT convo_id, users FROM conversations WHERE account_id = '$account_id' AND FIND_IN_SET('$user_id', users) AND FIND_IN_SET('$receiver_user_id', users) ";

			$result = $this->dbo->query($query);

			foreach($result as $row) {

				$convo_id = $row[0];
				$convo_users = $row[1];

				$convo_users_array = explode(",", $convo_users);
				$users_array = explode(",", $users);
				
				//array_diff needs to be checked both ways to ensure that the two always are actually the same!
				if(!array_diff($users_array, $convo_users_array) && !array_diff($convo_users_array, $users_array)) {
					$found_convo_id = $convo_id;
				}

			}

			return $found_convo_id;

		}



		public function initNewConversation() {

			$account_id = $_SESSION['account_id'];
			$user_id = $_SESSION['user_id'];

			$message_text = $_POST['message_text'];
			$users = $_POST['users'];

			$users .= "," . $user_id; //add yourself to the conversation of course!!

			//This first section is checking for an existing conversation between the users. 
			$match_found = false;
			$found_convo_id = 0;

			//Not using PREPARED STATEMENT because it's too much hassle to make it work with FIND_IN_SET.... 
			//First, get all of the conversations that this user has been a part of.. then compare the users in each to the users in the new convo
			$query = "SELECT convo_id, users FROM conversations WHERE account_id = '$account_id' AND FIND_IN_SET('$user_id', users)";

			$result = $this->dbo->query($query);

			foreach($result as $row) {

				$convo_id = $row[0];
				$convo_users = $row[1];

				$convo_users_array = explode(",", $convo_users);
				$users_array = explode(",", $users);

				//array_diff needs to be checked both ways to ensure that the two always are actually the same!
				if(!array_diff($users_array, $convo_users_array) && !array_diff($convo_users_array, $users_array)) {
					$match_found = true;
					$found_convo_id = $convo_id;
				}

			}

			$datetime = date('Y-m-d H:i:s');

			//If a convo has been found with the exact same users, then return the convo_id
			if($match_found == true) {

				$convo_id = $found_convo_id;

				$query = $this->dbo->prepare("INSERT INTO messages SET account_id = ?, convo_id = ?, from_user = ?, message = ?, date = ?");
				$query->execute(array($account_id, $convo_id, $user_id, $message_text, $datetime));

				$query = $this->dbo->prepare("UPDATE conversations SET hide_from = ?, last_update = ? WHERE account_id = ? AND convo_id = ?");
				$query->execute(array('', $datetime, $account_id, $convo_id));

			
			} else {

				$query = $this->dbo->prepare("INSERT INTO conversations SET account_id = ?, users = ?, last_update = ?");
				$query->execute(array($account_id, $users, $datetime));

				$convo_id = $this->dbo->lastInsertId();


				$query = $this->dbo->prepare("INSERT INTO messages SET account_id = ?, convo_id = ?, from_user = ?, message = ?, date = ?");
				$query->execute(array($account_id, $convo_id, $user_id, $message_text, $datetime));

			}
			

			//NOW INSERT NEW ROW INTO THE NOTIFICATIONS TABLE
			//NEW ROW FOR EACH USER ORRRR ONE ROW WITH MULTIPLE USER IDS IN THE USER_ID FIELD???
			//HOW WOULD IT SHOW UP ON THE NOTIFICATIONS BAR DROPDOWN??
			return $convo_id;
		}



		public function newConversation() {

			$account_id = $_SESSION['account_id'];
			$user_id = $_SESSION['user_id'];

			//u_id is used if the new conversation is to be assigned to a predefined user.
			$u_id = $_POST['u_id'];

			if($u_id != "") {

				$query = $this->dbo->prepare("SELECT user_id, first_name, last_name FROM users WHERE account_id = ? AND user_id = ? AND status = ?");
				$query->execute(array($account_id, $u_id, 'active'));

				$result = $query->fetchAll();

				foreach($result as $row) {

					$first_name = $row[1];
					$last_name = $row[2];
				}



				$data = '<div class="portlet box blue new_message nomarginbottom">
	                    <div class="portlet-title line">
	                        <div class="caption"><i class="icon-comment"></i> ' . $first_name . ' ' . $last_name . '</div>

	                        <div class="tools">
	                        	<i class="icon-remove close_message"></i>
	                        </div>
	                    </div>
	                    <div class="portlet-body" id="messages">';


				$data .= '	 <div class="chat-form">
	                           <div class="input-cont">   
	                              <textarea class="m-wrap message_text" placeholder="Type a message..."></textarea>
	                              <input id="u_id" type="hidden" value="' . $u_id . '" />
	                           </div>';

		                        if($_SESSION['sound'] == "on") {

									$data .= '<div class="btn-sound on"> 
					                      <a href="" class="btn light-blue icn-only toggle_volume"><i class="icon-volume-up"></i></a>
					                   </div>';

								} else if($_SESSION['sound'] == "off") {

									$data .= '<div class="btn-sound off"> 
					                      <a href="" class="btn grey icn-only toggle_volume"><i class="icon-volume-off"></i></a>
					                   </div>';
								}

	                            $data .= '
	                           		<div class="clearfix"></div>
		                        	<div class="btn-cont"> 
		                            	<a href="" class="btn light-blue icn-only send_message"><i class="icon-ok"></i> Send</a>
		                        	</div>';
		                           
		                        $data .= '</div>

		                        <audio id="chat_notif">
		                           <source src="audio/notif.ogg" type="audio/ogg">
		                           <source src="audio/notif.mp3" type="audio/mpeg">
		                        </audio>
		                    </div>
		                </div>

		                <script src="scripts/new_message.js"></script>';

			} else {

				//no u_id

				$data = '<div class="portlet box blue new_message nomarginbottom">
	                    <div class="portlet-title line">
	                        <div class="caption"><i class="icon-comment"></i> New Message</div>

	                        <div class="tools">
	                        	<i class="icon-remove close_message"></i>
	                        </div>
	                    </div>
	                    <div class="portlet-body" id="messages">';


	            $data .= '<form class="form-horizontal zero_margin">
	            			<div class="control-group new_message_recipients">
                             <label class="control-label" >To:</label>
                             <div class="controls">
                                <input type="text" id="recipients" class="m-wrap span6 select2">
                             </div>
                             <div class="clearfix"></div>
                          </div>
                          </form>';


				$data .= '	 <div class="chat-form">
	                           <div class="input-cont">   
	                              <textarea class="m-wrap message_text" placeholder="Type a message..."></textarea>
	                           </div>';

	                           $data .= '
		                        	<div class="btn-cont"> 
		                            	<a href="" class="btn light-blue icn-only send_message disabled"><i class="icon-ok"></i> Send</a>
		                        	</div>';

	                           if($_SESSION['sound'] == "on") {

									$data .= '<div class="btn-sound on"> 
					                      <a href="" class="btn light-blue icn-only toggle_volume"><i class="icon-volume-up"></i></a>
					                   </div>';

								} else if($_SESSION['sound'] == "off") {

									$data .= '<div class="btn-sound off"> 
					                      <a href="" class="btn grey icn-only toggle_volume"><i class="icon-volume-off"></i></a>
					                   </div>';
								}

		                           
		                        $data .= '</div>

		                        <audio id="chat_notif">
		                           <source src="audio/notif.ogg" type="audio/ogg">
		                           <source src="audio/notif.mp3" type="audio/mpeg">
		                        </audio>
		                    </div>
		                </div>

		                <script src="scripts/new_message.js"></script>';

			}

			

			return $data;

		}


		public function loadConversation() {

			$account_id = $_SESSION['account_id'];
			$user_id = $_SESSION['user_id'];

			$convo_id = $_GET['conv_id'];

			if($convo_id == "") {
				$convo_id = $_POST['convo_id'];
			}

			$users_array = array();

			// PREPENDING this part of the $data at the end so that I can put the user's names in the Title!!
			/*$data = '<div class="portlet box blue messages nomarginbottom">
	                    <div class="portlet-title line">
	                        <div class="caption"><i class="icon-comment"></i> ' . $name . '</div>

	                        <div class="tools">
	                        	<i class="icon-remove close_message"></i>
	                        </div>
	                    </div>
	                    <div class="portlet-body" id="messages">
	                        <div class="stretch_width">The Open Chat is available to all the account holders within a business. The messages sent here are visible to anyone viewing them while logged in to their individual accounts. If you wish to send private messages to your peers, please use the Messages portal instead.</div>
	                        <div class="scroller" data-always-visible="1" data-rail-visible1="1">
	                           <ul class="chats">';*/


	        $query = $this->dbo->prepare("SELECT users FROM conversations WHERE account_id = ? AND convo_id = ?");
			$query->execute(array($account_id, $convo_id));

			$result = $query->fetchAll();

			foreach($result as $row) {

				$users = $row[0];
			}


			//Not using PREPARED STATEMENT because it's too much hassle with IN function
			$query = "SELECT user_id, first_name, last_name, image, status, gender FROM users

			WHERE account_id = '$account_id'

			AND user_id IN ($users)";

			$result = $this->dbo->query($query);

			foreach($result as $row) {

				$this_user_id = $row[0];

				$users_array[$this_user_id]['id'] = $row[0];
				$users_array[$this_user_id]['first_name'] = $row[1];
				$users_array[$this_user_id]['last_name'] = $row[2];
				$users_array[$this_user_id]['image'] = $row[3];
				$users_array[$this_user_id]['status'] = $row[4];
				$users_array[$this_user_id]['gender'] = $row[5];
				//The indexes of $users_array are each user's ids!!
			}


			$query = $this->dbo->prepare("SELECT from_user, message, date FROM (SELECT * FROM messages WHERE account_id = ? AND convo_id = ? ORDER BY id DESC LIMIT 50) AS Last50 ORDER BY id ASC");
			$query->execute(array($account_id, $convo_id));

			$result = $query->fetchAll();

			foreach($result as $row) {

				$from_user = $row[0];
				$message = $row[1];
				$datetime = $row[2];


				$date = date('M j, Y', strtotime($datetime) + $_SESSION['timezone_offset']);
	    		$time = date('g:iA', strtotime($datetime) + $_SESSION['timezone_offset']);


				if($user_id == $from_user) {

					$my_message = true;
					
				} else {

					$my_message = false;
				}



				if($my_message == true) {

					$li_class = "out";
				} else {
					$li_class = "in";
				}


				
				if($users_array[$from_user]['image']) {

					$image = "https://cdn.ironvault.ca/pr_imgs/thumbs/" . $users_array[$from_user]['image'];

				} else {

					if($users_array[$from_user]['gender'] == "male") {

						$image = "https://www.ironvault.ca/app/img/male_default_thumb.png";
					
					} else if($users_array[$from_user]['gender'] == "female") {

						$image = "https://www.ironvault.ca/app/img/female_default_thumb.png";

					} else {

						$image = "https://www.ironvault.ca/app/img/male_default_thumb.png";						
					}
				}



				if($users_array[$from_user]['status'] == 'active') {

					$user_name = '<a href="profile.php?id=' . $users_array[$from_user]['id'] . '" class="name">' . $users_array[$from_user]['first_name'] . ' ' . $users_array[$from_user]['last_name'] . '</a>';
					$user_image = '<a href="profile.php?id=' . $users_array[$from_user]['id'] . '" class="name"><img class="avatar" alt="" src="' . $image . '" /></a>';

				} else {

					$user_name = $users_array[$from_user]['first_name'] . ' ' . $users_array[$from_user]['last_name'];
					$user_image = '<img class="avatar" alt="" src="img/user_default_thumb.png" />';
				}


                $data .= '<li class="' . $li_class . '" id="' . $users_array[$from_user]['id'] . '">

	                         ' . $user_image . '

	                         <div class="message">

	                            <span class="arrow"></span>

	                            ' . $user_name . '

	                            <span class="datetime">on ' . $date . ' at ' . $time . '</span>

	                            <span class="body">

	                            ' . $message . '

	                            </span>

	                         </div>

	                      </li>';

			}


			$data .= '</ul>
                        </div>
                        <div class="chat-form">
                           <div class="input-cont">   
                              <textarea class="m-wrap message_text" placeholder="Type a message..."></textarea>
                              <input class="first_name" type="hidden" value="' . $_SESSION['first_name'] . '" />
                              <input class="last_name" type="hidden" value="' . $_SESSION['last_name'] . '" />
                              <input class="image" type="hidden" value="' . $_SESSION['image_thumb'] . '" />
                              <input class="user_id" type="hidden" value="' . $_SESSION['user_id'] . '" />
                              <input class="username" type="hidden" value="' . $_SESSION['username'] . '" />
                              <input class="account_id" type="hidden" value="' . $_SESSION['account_id'] . '" />
                              <input class="users" type="hidden" value="' . $users . '" />
                              <input class="convo_id" type="hidden" value="' . $convo_id . '" />
                           </div>';

                           $data .= '
	                        	<div class="btn-cont"> 
	                            	<a href="" class="btn light-blue icn-only send_message"><i class="icon-ok"></i> Send</a>
	                        	</div>';

	                        if($_SESSION['sound'] == "on") {

								$data .= '<div class="btn-sound on"> 
				                      <a href="" class="btn light-blue icn-only toggle_volume"><i class="icon-volume-up"></i></a>
				                   </div>';

							} else if($_SESSION['sound'] == "off") {

								$data .= '<div class="btn-sound off"> 
				                      <a href="" class="btn grey icn-only toggle_volume"><i class="icon-volume-off"></i></a>
				                   </div>';
							}
	                           
	                        $data .= '</div>

	                        <audio id="chat_notif">
	                           <source src="audio/notif.ogg" type="audio/ogg">
	                           <source src="audio/notif.mp3" type="audio/mpeg">
	                        </audio>
	                    </div>
	                </div>

	                <script src="https://www.ironvault.ca:8082/socket.io/socket.io.js"></script>
	                <script src="private_messages/private_messages.js"></script>
	                <script src="scripts/messages.js"></script>';


	        $first_names = "You, ";

	        foreach ($users_array as $user) {
	        	
	        	if($user['id'] != $user_id) { //Prevent your own name being shown...

	        		$first_names .= $user['first_name'] . ", ";
	        	}
	        }

	        $first_names = substr($first_names, 0, -2);

	        //PREPENDING THE FIRST PART OF THE $DATA down here, so I can put the user's names inside the Title !!
	        $data = '<div class="portlet box blue messages nomarginbottom">
	                    <div class="portlet-title line">
	                        <div class="caption"><i class="icon-comment"></i> ' . $first_names . '</div>

	                        <div class="tools">
	                        	<i class="icon-remove close_message"></i>
	                        </div>
	                    </div>
	                    <div class="portlet-body" id="messages">
	                        <div class="stretch_width">The Open Chat is available to all the account holders within a business. The messages sent here are visible to anyone viewing them while logged in to their individual accounts. If you wish to send private messages to your peers, please use the Messages portal instead.</div>
	                        <div class="scroller" data-always-visible="1" data-rail-visible1="1">
	                           <ul class="chats">' . $data;

	        //Update notifications table
	        $query = $this->dbo->prepare("UPDATE notifications SET status = ? WHERE account_id = ? AND user_id = ? AND convo_id = ?");
			$query->execute(array('viewed', $account_id, $user_id, $convo_id));


			return $data;

		}



		public function loadConversationList() {

			$account_id = $_SESSION['account_id'];
			$user_id = $_SESSION['user_id'];

			$data = "";

			//WOHOOO! Complex SQL statement is successful!
			//Used columns from  two tables here, conversations and messages to eliminate having to store the 'last_update' field on the conversations table. This way I can match the convo_id then order, then group them to get the datetime of the last message in the convo.
			$query = "SELECT * FROM (SELECT c.convo_id, c.users, c.hide_from, m.date FROM conversations c, messages m WHERE c.account_id = '$account_id' AND FIND_IN_SET('$user_id', c.users) AND NOT FIND_IN_SET('$user_id', c.hide_from) AND c.convo_id = m.convo_id ORDER BY m.date DESC) j GROUP BY j.convo_id ORDER BY j.date DESC";

			foreach($this->dbo->query($query) as $row) {

				$convo_id = $row[0];
				$users = $row[1];
				$hide_from = $row[2];


				//Get the last message from the conversation
				$query2 = $this->dbo->prepare("SELECT from_user, message, date FROM messages WHERE account_id = ? AND convo_id = ? ORDER BY id DESC LIMIT 1");
				$query2->execute(array($account_id, $convo_id));

				$result2 = $query2->fetchAll();

				foreach($result2 as $row2) {

	    			$from_user = $row2[0];
	    			$last_message = $row2[1];
	    			$last_update = $row2[2];
	    		}


				$last_message = preg_replace('/<br\s*\/?>/', " ", $last_message);

				if(date('Y-m-d') == date('Y-m-d', strtotime($last_update) + $_SESSION['timezone_offset'])) {

					$display_datetime = "Today, " . date('g:iA', strtotime($last_update) + $_SESSION['timezone_offset']);

				} else {

					$display_datetime = date('M j, Y, g:iA', strtotime($last_update) + $_SESSION['timezone_offset']);
				}


				//take out own user_id from the string of users ids. No need to show own name.
				$users = explode(",", $users);

				$this_user_pos = array_search($user_id, $users);
				unset($users[$this_user_pos]);

				//this makes the user with the last message in the convo appear first
				if($from_user != $user_id) {

					$from_user_pos = array_search($from_user, $users);
					unset($users[$from_user_pos]);
					array_unshift($users, $from_user);
				}

				$users_array_string = implode(",", $users);

				$count = 0;
				$users_string = "";
				$users_array = array();

				//Not using PREPARED STATEMENT because it's too much hassle to implement IN function
				$query3 = "SELECT first_name, last_name, image, gender FROM users WHERE account_id = '$account_id' AND user_id IN ($users_array_string)";

				foreach($this->dbo->query($query3) as $row3) {

					$first_name = $row3[0];
					$last_name = $row3[1];
					$image = $row3[2];
					$gender = $row3[3];

					$users_string .= $first_name . " " . $last_name . ", ";

					$users_array[$count]['first_name'] = $first_name;
					$users_array[$count]['last_name'] = $last_name;
					$users_array[$count]['image'] = $image;
					$users_array[$count]['gender'] = $gender;

					$count++;
				}

				$users_string = substr($users_string, 0, -2);



				if($users_array[0]['image']) {

					$image = "https://cdn.ironvault.ca/pr_imgs/thumbs/" . $users_array[0]['image'];

				} else {

					if($users_array[0]['gender'] == "male") {

						$image = "https://www.ironvault.ca/app/img/male_default_thumb.png";
					
					} else if($users_array[0]['gender'] == "female") {

						$image = "https://www.ironvault.ca/app/img/female_default_thumb.png";

					} else {

						$image = "https://www.ironvault.ca/app/img/male_default_thumb.png";						
					}
				}


				$read_marker = '<span class="read"></span>';
				$tr_class = '';


				$query4 = $this->dbo->prepare("SELECT status FROM notifications WHERE account_id = ? AND user_id = ? AND convo_id = ? LIMIT 1");
				$query4->execute(array($account_id, $user_id, $convo_id));

				$result4 = $query4->fetchAll();

				foreach($result4 as $row4) {

					$status = $row4[0];

					if($status == "unseen" || $status == "seen") {
						$read_marker = '<span class="unread"></span>';
						$tr_class = 'unread';
					} else {
						$read_marker = '<span class="read"></span>';
						$tr_class = '';
					}
				}



				$data .= '<tr class="' . $tr_class . '" id="' . $convo_id . '">
							<td class="profile_thumb"><img src="' . $image . '" /></td>
							<td class="read_marker">' . $read_marker . '</td>
							<td class="convo_details"><span class="users">' . $users_string . '</span><br><span class="last_message">' . $last_message . '</span></td>
							<td class="datetime">' . $display_datetime . ' <i class="icon-remove hide_convo" title="Hide Conversation"></i></td>
						</tr>';


			}


			return $data;

		}


		//Used for AJAX, when the table has already been loaded. Returns data array, without HTML tags.
		public function reloadConversationList() {

			
			//

			$account_id = $_SESSION['account_id'];
			$user_id = $_SESSION['user_id'];

			$data = "";

			//WOHOOO! Complex SQL statement is successful!
			//Used columns from  two tables here, conversations and messages to eliminate having to store the 'last_update' field on the conversations table. This way I can match the convo_id then order, then group them to get the datetime of the last message in the convo.
			$query = "SELECT * FROM (SELECT c.convo_id, c.users, c.hide_from, m.date FROM conversations c, messages m WHERE c.account_id = '$account_id' AND FIND_IN_SET('$user_id', c.users) AND NOT FIND_IN_SET('$user_id', c.hide_from) AND c.convo_id = m.convo_id ORDER BY m.date DESC) j GROUP BY j.convo_id ORDER BY j.date DESC";

			foreach($this->dbo->query($query) as $row) {

				$convo_id = $row[0];
				$users = $row[1];
				$hide_from = $row[2];


				//Get the last message from the conversation
				$query2 = $this->dbo->prepare("SELECT from_user, message, date FROM messages WHERE account_id = ? AND convo_id = ? ORDER BY id DESC LIMIT 1");
				$query2->execute(array($account_id, $convo_id));

				$result2 = $query2->fetchAll();

				foreach($result2 as $row2) {

	    			$from_user = $row2[0];
	    			$last_message = $row2[1];
	    			$last_update = $row2[2];
	    		}


				$last_message = preg_replace('/<br\s*\/?>/', " ", $last_message);

				if(date('Y-m-d') == date('Y-m-d', strtotime($last_update) + $_SESSION['timezone_offset'])) {

					$display_datetime = "Today, " . date('g:iA', strtotime($last_update) + $_SESSION['timezone_offset']);

				} else {

					$display_datetime = date('M j, Y, g:iA', strtotime($last_update) + $_SESSION['timezone_offset']);
				}


				//take out own user_id from the string of users ids. No need to show own name.
				$users = explode(",", $users);

				$this_user_pos = array_search($user_id, $users);
				unset($users[$this_user_pos]);

				//this makes the user with the last message in the convo appear first
				if($from_user != $user_id) {

					$from_user_pos = array_search($from_user, $users);
					unset($users[$from_user_pos]);
					array_unshift($users, $from_user);
				}

				$users_array_string = implode(",", $users);

				$count = 0;
				$users_string = "";
				$users_array = array();

				//Not using PREPARED STATEMENT because it's too much hassle to implement IN function
				$query3 = "SELECT first_name, last_name, image, gender FROM users WHERE account_id = '$account_id' AND user_id IN ($users_array_string)";

				foreach($this->dbo->query($query3) as $row3) {

					$first_name = $row3[0];
					$last_name = $row3[1];
					$image = $row3[2];
					$gender = $row3[3];

					$users_string .= $first_name . " " . $last_name . ", ";

					$users_array[$count]['first_name'] = $first_name;
					$users_array[$count]['last_name'] = $last_name;
					$users_array[$count]['image'] = $image;
					$users_array[$count]['gender'] = $gender;

					$count++;
				}

				$users_string = substr($users_string, 0, -2);



				if($users_array[0]['image']) {

					$image = "https://cdn.ironvault.ca/pr_imgs/thumbs/" . $users_array[0]['image'];

				} else {

					if($users_array[0]['gender'] == "male") {

						$image = "https://www.ironvault.ca/app/img/male_default_thumb.png";
					
					} else if($users_array[0]['gender'] == "female") {

						$image = "https://www.ironvault.ca/app/img/female_default_thumb.png";

					} else {

						$image = "https://www.ironvault.ca/app/img/male_default_thumb.png";						
					}
				}


				$read_marker = '<span class="read"></span>';
				$tr_class = '';


				$query4 = $this->dbo->prepare("SELECT status FROM notifications WHERE account_id = ? AND user_id = ? AND convo_id = ? LIMIT 1");
				$query4->execute(array($account_id, $user_id, $convo_id));

				$result4 = $query4->fetchAll();

				foreach($result4 as $row4) {

					$status = $row4[0];

					if($status == "unseen" || $status == "seen") {
						$read_marker = '<span class="unread"></span>';
						$tr_class = 'unread';
					} else {
						$read_marker = '<span class="read"></span>';
						$tr_class = '';
					}
				}



				$data .= '<tr class="' . $tr_class . '" id="' . $convo_id . '">
							<td class="profile_thumb"><img src="' . $image . '" /></td>
							<td class="read_marker">' . $read_marker . '</td>
							<td class="convo_details"><span class="users">' . $users_string . '</span><br><span class="last_message">' . $last_message . '</span></td>
							<td class="datetime">' . $display_datetime . ' <i class="icon-remove hide_convo" title="Hide Conversation"></i></td>
						</tr>';


			}


			return $data;

		}



		public function hideConversation() {

			$account_id = $_SESSION['account_id'];
			$user_id = $_SESSION['user_id'];

			$convo_id = $_POST['convo_id'];

			$query = $this->dbo->prepare("SELECT hide_from FROM conversations WHERE account_id = ? AND convo_id = ?");
			$query->execute(array($account_id, $convo_id));

			$result = $query->fetchAll();

			foreach($result as $row) {

				$hide_from = $row[0];
			}

			if($hide_from != "") {
			
				$hide_from = explode(",", $hide_from);

				array_push($hide_from, $user_id);

				$hide_from = implode(",", $hide_from);
			
			} else {

				$hide_from = $user_id;
			}


			$query = $this->dbo->prepare("UPDATE conversations SET hide_from = ? WHERE account_id = ? AND convo_id = ?");
			$query->execute(array($hide_from, $account_id, $convo_id));


		}




	}



?>