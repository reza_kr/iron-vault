<?php

	session_start();

	require_once('error_handler.php');

	require_once("scripts/aws.phar");


	Class Storage { 

		private $s3client;

		public function __construct () {

			require("inc/connect_dbo.php");

			$this->dbo = $dbo;

			$this->s3client = Aws\S3\S3Client::factory(array(
			    'key'    => 'AKIAJKBMGCMLYBHN77PQ',
			    'secret' => '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'
			    ));

		}


		public function calculateStorageCap($user_id) {

			$account_id = $_SESSION['account_id'];

			if($user_id == "") {

				$user_id = $_SESSION['user_id'];

				$type = 'fullstring';

			} else {

				$type = 'values';
			}

			$response = $this->s3client->getIterator('ListObjects', array(
			    'Bucket' => 'ironvault',
				'Prefix' => 'storage/' . $account_id . '/' . $user_id
			));

			foreach($response as $object) {
			    $used_space = $used_space + $object['Size'];
			}

			$query = $this->dbo->prepare("UPDATE storage SET used = ? WHERE account_id = ? AND user_id = ?");
			$query->execute(array($used_space, $account_id, $user_id));


			$query = $this->dbo->prepare("SELECT size_limit FROM storage WHERE account_id = ? AND user_id = ?");
			$query->execute(array($account_id, $user_id));

			$result = $query->fetchAll();

			foreach($result as $row) {
			    
			    $max_space = $row[0];
			}

			$avail_space = intval($max_space) - intval($used_space);
			$avail_space_in_bytes = $avail_space;

			$percentage = round(intval($used_space) / intval($max_space) * 100, 2);

			$used_space = $this->formatBytes($used_space);
			$avail_space = $this->formatBytes($avail_space);
			$max_space = $this->formatBytes($max_space);

			if($type == 'fullstring') {	
				
				$space_string = $percentage . ",<span class='storage_stats_percentage'>" . $percentage . "%</span><br>Available: " . $avail_space . "<br>Used: " . $used_space . " / " . $max_space . "," . $avail_space_in_bytes;

			} else if($type == 'values') {
				
				$space_string = $percentage . "," . $avail_space . "," . $used_space . "," . $max_space . "," . $avail_space_in_bytes;
			}

			return $space_string; 
		}

		//Much like the calculateStorageCap function, but only retrieves the user's storage quota limit, e.g. the max_space.
		public function getStorageQuota() {

			$account_id = $_SESSION['account_id'];
			$user_id = $_POST['user_id'];
			

			$query = $this->dbo->prepare("SELECT size_limit FROM storage WHERE account_id = ? AND user_id = ?");
			$query->execute(array($account_id, $user_id));

			$result = $query->fetchAll();

			foreach($result as $row) {
			    
			    $quota_limit = $row['size_limit'];
			}

			
			$quota_limit = $this->formatBytes($quota_limit);

			return $quota_limit; 
		}



		public function updateStorageQuota() {
			
			$account_id = $_SESSION['account_id'];
			$user_id = $_POST['user_id'];

			$quota = $_POST['update_quota'];

			$quota_bytes = $quota * 1073741824; //GB to Bytes

			$query = $this->dbo->prepare("UPDATE storage SET size_limit = ? WHERE account_id = ? AND user_id = ?");
			$query->execute(array($quota_bytes, $account_id, $user_id));

			header('Location: account_settings#user_accounts');

		}


		public function calculateAvailableSpace() {

			$account_id = $_SESSION['account_id'];
			$user_id = $_SESSION['user_id'];

			//$resp = $this->s3client->list_objects("ironvault", array("prefix" => "storage"));
			$response = $this->s3client->listObjects(array(
					'Bucket' => 'ironvault',
					'Prefix' => 'storage/' . $account_id . '/' . $user_id
				));

			$response = $this->s3client->getIterator('ListObjects', array(
			    'Bucket' => 'ironvault',
				'Prefix' => 'storage/' . $account_id . '/' . $user_id
			));

			foreach($response as $object) {
			    $used_space = $used_space + $object['Size'];
			}

			$query = $this->dbo->prepare("SELECT size_limit FROM storage WHERE account_id = ? AND user_id = ?");
			$query->execute(array($account_id, $user_id));

			$result = $query->fetchAll();

			foreach($result as $row) {
			    
			    $max_space = $row['size_limit'];
			}

			$avail_space = intval($max_space) - intval($used_space);

			return $avail_space;
		}



		public function formatBytes($bytes, $precision = 2) { 

		    $units = array('B', 'KB', 'MB', 'GB', 'TB'); 

		    $bytes = max($bytes, 0); 
		    $pow = floor(($bytes ? log($bytes) : 0) / log(1024)); 
		    $pow = min($pow, count($units) - 1); 

		    // Uncomment one of the following alternatives
		    $bytes /= pow(1024, $pow);
		    // $bytes /= (1 << (10 * $pow)); 

		    return round($bytes, $precision) . " " . $units[$pow]; 
		} 



		public function createLink() {

			$account_id = $_SESSION['account_id'];
			$user_id = $_SESSION['user_id'];

			$elfinder_hash = $_GET['h'];
			$kind = $_GET['kind'];

			if($account_id != "" && $user_id != "" && $elfinder_hash != "" && ($kind == "file" || $kind == "folder")) {

				//If there IS an exisitng link for the ELFINDER Hash, then get it and forward user to fileshare page.
				$query = $this->dbo->prepare("SELECT share_hash FROM fileshare WHERE account_id = ? AND user_id = ? AND elfinder_hash = ? LIMIT 1");
				$query->execute(array($account_id, $user_id, $elfinder_hash));

				$result = $query->fetchAll();

				foreach($result as $row) {
				    
				    $share_hash = $row['share_hash'];
				
				    header('Location: fileshare?h=' . $share_hash);
				}

				if(!$share_hash) {

					//Using the account_id, user_id and elfinder's file/folder hash to create a unique hash that's only about 10-12 characters long!
					//This is more appealing to the user and easier to manage.
					$pre_hash = $account_id . "_" . $user_id . "_" . $elfinder_hash . "_" . time();
					$share_hash = base_convert($pre_hash, 10, 36);

					$query = $this->dbo->prepare("INSERT INTO fileshare
					SET account_id = ?,
					user_id = ?,
					share_hash = ?,
					elfinder_hash = ?,
					kind = ?");

					$result = $query->execute(array($account_id, $user_id, $share_hash, $elfinder_hash, $kind));

					if($result == true) {

						header('Location: fileshare?h=' . $share_hash);

					} else {

						//handle fail
					}
				}

			} else {

				return false;
			}

		}



		public function removeLink() {

			$account_id = $_SESSION['account_id'];
			$user_id = $_SESSION['user_id'];

			$share_hash = $_GET['h'];

			if($account_id != "" && $user_id != "" && $share_hash != "") {

				$query = $this->dbo->prepare("DELETE FROM fileshare WHERE account_id = ? AND user_id = ? AND share_hash = ?");
				$query->execute(array($account_id, $user_id, $share_hash));

				header('Location: fileshare?r=removed');


			} else {

				return false;
			}

		}


		public function fileshareRemovedMessage() {


			if($_SESSION['account_id'] && $_SESSION['user_id'] && $_SESSION['status'] == active) {

				$message = '<p><br><i class="icon-ok"></i> Shared link has been sucessfully removed. <a href="storage">Return to Storage</a></p>';
			}

			return $message;

		}



		public function getElfinderHash() {

			//account_id along with business_name are stored in session vars regardless if the user is logged in or not. Thus it works for visitors as well.
			$account_id = $_SESSION['account_id'];

			$share_hash = $_GET['h'];
			$elfinder_hash = array();

			$query = $this->dbo->prepare("SELECT * FROM fileshare WHERE account_id = ? AND share_hash = ? LIMIT 1");
			$query->execute(array($account_id, $share_hash));

			$result = $query->fetchAll();

			foreach($result as $row) {
			    
			    $elfinder_hash['account_id'] = $row['account_id'];
			    $elfinder_hash['user_id'] = $row['user_id'];
			    $elfinder_hash['hash'] = $row['elfinder_hash'];
			    $elfinder_hash['kind'] = $row['kind'];
			
			}

			return $elfinder_hash;

		}



		public function getSharedFiles($elfinder_hash) {

			if(is_array($elfinder_hash) && sizeof($elfinder_hash) > 0) {

				if($elfinder_hash['kind'] == "file") {

					$cmd = "fileshare";
				} else if($elfinder_hash['kind'] == "folder") {

					$cmd = "open";
				}

				
				$url = $_SERVER['SERVER_NAME'] . "/fileshare_conn?cmd=" . $cmd . "&target=" . $elfinder_hash['hash'] . "&a=" . $elfinder_hash['account_id'] . "&u=" . $elfinder_hash['user_id'];

				$ch = curl_init();
				curl_setopt ($ch, CURLOPT_URL, $url); 
				curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
			    curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.6) Gecko/20070725 Firefox/2.0.0.6");
			    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

				$result = curl_exec($ch);
				
				curl_close($ch);

				$result = json_decode($result, true); //the TRUE parameter returns an array instead of an object

				if(is_array($result)) {

					$files = '<table class="table table-bordered table-striped table-condensed cf fileshare_table" id="data_table">
							<thead class="cf">
								<tr>
									<th>Filename</th>
									<th>Size</th>
									<th>Download</th>
								</tr>
							</thead>
							<tbody>';
				
					if($elfinder_hash['kind'] == "file") {

						$files .= '<tr>
									<td title="' . $result['name'] . '">' . $result['name'] . '</td>
									<td>' . $this->formatBytes($result['size']) . '</td>
									<td><a id="' . $elfinder_hash['hash'] . '" href="https://' . $_SERVER['SERVER_NAME'] . '/fileshare_conn?cmd=file&target=' . $result['hash'] . '&a=' . $elfinder_hash['account_id'] . '&u=' . $elfinder_hash['user_id'] . '&download=1' . '"/><i class="icon-download-alt" class="file_download"></i></a></td>
								</tr>';
						
					} else if($elfinder_hash['kind'] == "folder") {
						foreach ($result['files'] as $file) {

							if($file['mime'] != "directory") {

								$files .= '<tr>
											<td>' . $file['name'] . '</td>
											<td>' . $this->formatBytes($file['size']) . '</td>
											<td><a id="' . $elfinder_hash['hash'] . '" href="https://' . $_SERVER['SERVER_NAME'] . '/fileshare_conn?cmd=file&target=' . $file['hash'] . '&a=' . $elfinder_hash['account_id'] . '&u=' . $elfinder_hash['user_id'] . '&download=1' . '"/><i class="icon-download-alt" class="file_download"></i></a></td>
										</tr>';
							}
						}
					}

					$files .= '</tbody>
							</table>';

					$data['files'] = $files;

					if($_SESSION['account_id'] && $_SESSION['user_id'] && $_SESSION['status'] == active) {

						if($_SESSION['account_id'] == $elfinder_hash['account_id'] && $_SESSION['user_id'] == $elfinder_hash['user_id']) {
							
							$data['remove_button'] = '<a class="btn right" href="fileshare_remove?h=' . $_GET['h'] . '"><i class="icon-remove-sign"></i>&nbsp;&nbsp;Remove Link</a>';
						}
						
					}

					return $data;
				}
			}

		}

	}


?>