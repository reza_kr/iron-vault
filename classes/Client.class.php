<?php

	session_start();

	require_once('error_handler.php');

	
	Class Client { 



		public function __construct () {

			require("inc/connect_dbo.php");

			$this->dbo = $dbo;

		}



		public function addClient() { 

			$account_id = $_SESSION['account_id'];

			$client_type = $_POST['client_type'];
			$first_name = $_POST['first_name'];
			$last_name = $_POST['last_name'];
			$individual_email = $_POST['individual_email'];
			$gender = $_POST['gender'];
			$primary_phone = $_POST['primary_phone'];
			$alternative_phone = $_POST['alternative_phone'];
			$address = $_POST['address']; 

			$business_name = $_POST['business_name'];	
			$business_email = $_POST['business_email'];	

			$page = $_POST['page']; 

			if($page == 'clients') {

				if($client_type == "individual") {

					$query = $this->dbo->prepare("INSERT INTO clients SET account_id = ?, first_name = ?, last_name = ?, type = ?, email = ?, gender = ?, primary_phone = ?, alternative_phone = ?, address = ?");
					$result = $query->execute(array($account_id, $first_name, $last_name, $client_type, $individual_email, $gender, $primary_phone, $alternative_phone, $address));
					
					//print_r($query->errorInfo()); exit;
					
					$client_id = $this->dbo->lastInsertId();

					if($client_id != 0) {

						require_once("classes/Account.class.php");
						$account = new Account();

						$account->addToTimeline("client_added", $client_id);
					}

					header('Location: client_profile?id=' . $client_id);

				} else if($client_type == "business") {

					$query = $this->dbo->prepare("INSERT INTO clients SET account_id = ?, business_name = ?, type = ?, email = ?, primary_phone = ?, alternative_phone = ?, address = ?");
					$result = $query->execute(array($account_id, $business_name, $client_type, $business_email, $primary_phone, $alternative_phone, $address));

					$client_id = $this->dbo->lastInsertId();

					if($client_id != 0) {

						require_once("classes/Account.class.php");
						$account = new Account();

						$account->addToTimeline("client_added", $client_id);
					}

					header('Location: client_profile?id=' . $client_id);

				} else {
					
					header('Location: clients?r=failed');
					//error
				}

			} else if($page == 'new_invoice') { 

				if($client_type == "individual") {

					$query = $this->dbo->prepare("INSERT INTO clients SET account_id = ?, first_name = ?, last_name = ?, type = ?, email = ?, gender = ?, primary_phone = ?, alternative_phone = ?, address = ?");
					$result = $query->execute(array($account_id, $first_name, $last_name, $client_type, $individual_email, $gender, $primary_phone, $alternative_phone, $address));
					
					//return print_r($query->errorInfo());
					
					$client_ref_id = $this->dbo->lastInsertId();

					if($client_ref_id != 0) {

						require_once("classes/Account.class.php");
						$account = new Account();

						$account->addToTimeline("client_added", $client_id);

						$query = $this->dbo->prepare("SELECT client_id, first_name, last_name, business_name, type FROM clients WHERE account_id = ?");
						$query->execute(array($account_id));

						$result = $query->fetchAll();

						foreach($result as $row) {

			    			$client_id = $row[0];
			    			$first_name = $row[1];
			    			$last_name = $row[2];
			    			$business_name = $row[3];
			    			$type = $row[4];

			    			if($type == 'individual') {
			    				
			    				$client_display_name = $first_name . ' ' . $last_name;

			    			} else if($type == 'business') {

			    				$client_display_name = $business_name;
			    			}

			    			if($client_ref_id == $client_id) {
			    				
			    				$data .= '<option value="' . $client_id . '" selected>' . $client_display_name . '</option>';
			    			
			    			} else {

			    				$data .= '<option value="' . $client_id . '">' . $client_display_name . '</option>';
			    			}

			    		}

			    		return $data;
					}


				} else if($client_type == "business") {

					$query = $this->dbo->prepare("INSERT INTO clients SET account_id = ?, business_name = ?, type = ?, email = ?, primary_phone = ?, alternative_phone = ?, address = ?");
					$result = $query->execute(array($account_id, $business_name, $client_type, $business_email, $primary_phone, $alternative_phone, $address));

					$client_ref_id = $this->dbo->lastInsertId();

					if($client_ref_id != 0) {

						require_once("classes/Account.class.php");
						$account = new Account();

						$account->addToTimeline("client_added", $client_id);

						$query = $this->dbo->prepare("SELECT client_id, first_name, last_name, business_name, type FROM clients WHERE account_id = ?");
						$query->execute(array($account_id));

						$result = $query->fetchAll();

						foreach($result as $row) {

			    			$client_id = $row[0];
			    			$first_name = $row[1];
			    			$last_name = $row[2];
			    			$business_name = $row[3];
			    			$type = $row[4];

			    			if($type == 'individual') {
			    				
			    				$client_display_name = $first_name . ' ' . $last_name;

			    			} else if($type == 'business') {

			    				$client_display_name = $business_name;
			    			}

			    			if($client_ref_id == $client_id) {
			    				
			    				$data .= '<option value="' . $client_id . '" selected>' . $client_display_name . '</option>';
			    			
			    			} else {

			    				$data .= '<option value="' . $client_id . '">' . $client_display_name . '</option>';
			    			}

			    		}

			    		return $data;
					}


				} else {
					
					
					//error
				}

			}

	    }


	    public function deleteClient() { 

			$account_id = $_SESSION['account_id'];

			$client_id = $_POST['client_id'];

			$query = $this->dbo->prepare("DELETE FROM clients WHERE account_id = ? AND client_id = ?");
			$result = $query->execute(array($account_id, $client_id));

			header('Location: clients');

		}


	    public function  loadClientProfile() {

			$account_id = $_SESSION['account_id'];

			if($_GET['id']) {

				$client_id = $_GET['id'];

			} else {

				header('Location: clients');
			}

			$profile['client_id'] = $client_id;


			$query = $this->dbo->prepare("SELECT COUNT(*) FROM clients WHERE account_id = ? AND client_id = ?");
			$query->execute(array($account_id, $client_id));

			$result = $query->fetchAll();

			foreach($result as $row) {

				$num_rows = $row[0];
			}

			if($num_rows > 0) {

				$query = $this->dbo->prepare("SELECT first_name, last_name, business_name, email, gender, primary_phone, alternative_phone, fax, website, image, address, type FROM clients WHERE account_id = ? AND client_id = ?");
				$query->execute(array($account_id, $client_id));

				$result = $query->fetchAll();

				foreach($result as $row) {

					$first_name = $row[0]; 
					$last_name = $row[1]; 
					$business_name = $row[2]; 

					$profile['email'] = $row[3]; 
					$profile['gender'] = $row[4]; 
					$profile['primary_phone'] = $row[5]; 
					$profile['alternative_phone'] = $row[6]; 
					$profile['fax'] = $row[7]; 
					$profile['website'] = $row[8]; 
					$profile['image'] = $row[9];

					$address = $row[10]; 

					$profile['type'] = $row[11];


					$profile['primary_phone'] = preg_replace("/^(\d{3})(\d{3})(\d{4})$/", "($1) $2-$3", $profile['primary_phone']);
					$profile['alternative_phone']= preg_replace("/^(\d{3})(\d{3})(\d{4})$/", "($1) $2-$3", $profile['alternative_phone']);
					$profile['fax']= preg_replace("/^(\d{3})(\d{3})(\d{4})$/", "($1) $2-$3", $profile['fax']);

					$json = json_encode($row);
				}

			} else {

				header('Location: clients');
			}

			if($profile['type'] == "individual") {

				$profile['display_name'] = $first_name . " " . $last_name;
				$profile['business_name'] = $business_name;

			} else if($profile['type'] == "business") {

				$profile['display_name'] = $business_name;
			}


			$address_count = 0;

			// $full_address = ""; //For the info_table
			// $map_address = ""; //For the map locations

   //  		if($address != "") {
   //  			$full_address .= $address . "<br>";
   //  			$map_address .= $address . ", ";
   //  		}

   //  		if($city != "") {
   //  			$full_address .= $city . ", ";
   //  			$map_address .= $city . ", ";
   //  		}

   //  		if($province != "") {
   //  			$full_address .= $province . "<br>";
   //  			$map_address .= $province . ", ";
   //  		}

   //  		if($country != "") {
   //  			$full_address .= $country . ", ";
   //  			$map_address .= $country . ", ";
   //  		}

   //  		if($postal_code != "") {
   //  			$full_address .= $postal_code . ", ";
   //  			$map_address .= $postal_code . ", ";
   //  		}

   //  		if($full_address != "") {
   //  			$full_address = substr($full_address, 0, -2);
   //  			$map_address = substr($map_address, 0, -2);

   //  			$client_addresses_hidden .= '<input type="hidden" id="' . $address_count . '" class="client_address" label="Primary Address" value="' . $map_address .'" />';

   //  			$client_addresses_table .= '<tr id="' . $address_count . '">
			// 							    <td>Primary Address</td>
			// 							    <td>' . $map_address . '</td>
			// 							  </tr>';

   //  			$address_count++;
   //  		}


    		if($address != "") {

    			$client_addresses_hidden .= '<input type="hidden" id="' . $address_count . '" class="client_address" label="Primary Address" value="' . $address .'" />';

    			$client_addresses_table .= '<tr id="' . $address_count . '">
										    <td>Primary Address</td>
										    <td>' . $address . '</td>
										  </tr>';

    			$address_count++;
    		}


    		$profile['info_table'] .= '<input type="hidden" id="client_id" value="' . $client_id . '" />';


    		//Profile picture
    		if($profile['image'] != "") {

    			$profile['info_table'] .= '<tr><td colspan="3" class="image"><img src="' . $profile['image'] . '" alt="' . $profile['display_name'] . '" /></td></tr>';

    		} else {

	    		if($profile['type'] == "individual") {

					if($profile['gender'] == "male") {

						$profile['info_table'] .= '<tr><td colspan="3" class="image"><img src="img/male_default.png" alt="' . $profile['display_name'] . '" /></td></tr>';

					}

					if($profile['gender'] == "female") {

						$profile['info_table'] .= '<tr><td colspan="3" class="image"><img src="img/female_default.png" alt="' . $profile['display_name'] . '" /></td></tr>';

					}
	 
				} else if($profile['type'] == "business") {

					$profile['info_table'] .= '<tr><td colspan="3" class="image"><img src="img/company_default.png" alt="' . $profile['display_name'] . '" /></td></tr>';
				}
			}


			$profile['info_table'] .= '<tr><td colspan="3">';

			$profile['info_table'] .= "<input type='hidden' id='client_info_json' value='" . $json . "' />";

			$profile['info_table'] .= '<div class="spacer_10"></div><a class="btn light-blue" href="#update_client_info_modal" data-toggle="modal" id="update_client_info">Update Client Profile</a><div class="spacer_10"></div></td></tr>';



			$profile['info_table'] .= '<tr><td><i class="icon-map-marker"></i></td><td>Primary Address:</td><td><b>' . $address . '</b></td></tr>';


			if($profile['primary_phone'] != "") {

				$profile['info_table'] .= '<tr><td><i class="icon-phone"></i></td><td>Primary Phone:</td><td><b>' . $profile['primary_phone'] . '</b></td></tr>';
			}


			if($profile['alternative_phone'] != "") {

				$profile['info_table'] .= '<tr><td><i class="icon-phone"></i></td><td>Alternative Phone:</td><td><b>' . $profile['alternative_phone'] . '</b></td></tr>';
			}

			if($profile['fax'] != "") {

					$profile['info_table'] .= '<tr><td><i class="icon-print"></i></td><td>Fax:</td><td><b>' . $profile['fax'] . '</b></td></tr>';

			}

 
			if($profile['website'] != "") {

				$profile['info_table'] .= '<tr><td><i class="icon-globe"></i></td><td>Website:</td><td><b><a href="' . $profile['website'] . '" target="_blank">' . $profile['website'] . '</a></b></td></tr>';
			}

			if($profile['email'] != "") {

				$profile['info_table'] .= '<tr><td><i class="icon-envelope"></i></td><td>Email:</td><td><b>' . $profile['email'] . '</b></td></tr>';
			}

			if($profile['gender'] != "") {

				if($profile['gender'] == "male") {

					$profile['info_table'] .= '<tr><td><i class="icon-male"></i></td><td>Gender:</td><td><b>M</b></td></tr>';

				}

				if($profile['gender'] == "female") {

					$profile['info_table'] .= '<tr><td><i class="icon-female"></i></td><td>Gender:</td><td><b>F</b></td></tr>';

				}
 
			}


			$query = $this->dbo->prepare("SELECT name, address FROM locations WHERE account_id = ? AND client_id = ?");
			$query->execute(array($account_id, $client_id));

			$result = $query->fetchAll();

			foreach($result as $row) {

				$name = $row[0]; 
				$address = $row[1];

				$client_addresses_hidden .= '<input type="hidden" id="' . $address_count . '" class="client_address" label="' . $name . '" value="' . $address .'" />';

				$client_addresses_table .= '<tr id="' . $address_count . '">
										    <td>' . $name . '</td>
										    <td>' . $address . '</td>
										  </tr>';

				$address_count++;
			}


			if($client_addresses_table != "") {

				$profile['map'] = $client_addresses_hidden . '<h3 class="print_hidden">Map</h3>
								<div id="client_gmap"></div>

								<table class="table table-bordered table-striped table-condensed client_map_locations">
									<thead class="">
									  <tr>
									    <th class="sorting">Label</th>
									    <th class="sorting">Address</th>
									  </tr>
									</thead>
									<tbody>'
									  . $client_addresses_table .
									'</tbody>
								</table>

								<br><br>';

			}
			
			
			return $profile; 



	    }



	    public function loadClientNotes() {

	    	$account_id = $_SESSION['account_id'];

	    	$client_id = $_GET['id'];

	    	//Check for existing entry
			$query = $this->dbo->prepare("SELECT user_id, text, last_changed FROM client_notes WHERE account_id = ? AND client_id = ?");
			$query->execute(array($account_id, $client_id));

			$result = $query->fetchAll();

			$found = false;

			foreach($result as $row) {

				$found = true;

				$user_id = $row[0];
				$client_notes = $row[1];
				$last_changed = $row[2];
			}

			if($found) {

				$query = $this->dbo->prepare("SELECT first_name, last_name, status FROM users WHERE account_id = ? AND user_id = ?");
				$query->execute(array($account_id, $user_id));

				$result = $query->fetchAll();

				foreach($result as $row) {

					$first_name = $row[0];
					$last_name = $row[1];
					$status = $row[2];
				}


				if($status == 'active') {

					$user_link = '<a href="profile?id=' . $user_id . '">' . $first_name . " " . $last_name . '</a>'; 	

				} else {

					$user_link = $first_name . " " . $last_name; 
				}


				$notes['last_changed'] = 'Last Modified: ' . date('M j, Y, g:iA', strtotime($last_changed) + $_SESSION['timezone_offset']) . ' by ' . $user_link;
			}		


			
			$notes['text'] = $client_notes;


			return $notes;

	    }



	    public function saveClientNotes() {

	    	$account_id = $_SESSION['account_id'];
	    	$user_id = $_SESSION['user_id'];

	    	$client_id = $_POST['client_id'];

	    	$client_notes = $_POST['client_notes'];

	    	$last_changed = date('Y-m-d H:i:s');

	    	//Check for existing entry
			$query = $this->dbo->prepare("SELECT COUNT(*) FROM client_notes WHERE account_id = ? AND client_id = ?");
			$query->execute(array($account_id, $client_id));

			$result = $query->fetchAll();

			foreach($result as $row) {

				$num_rows = $row[0];
			}

			//Update or insert row
			if($num_rows > 0) {

				$query = $this->dbo->prepare("UPDATE client_notes SET user_id = ?, text = ?, last_changed = ? WHERE account_id = ? AND client_id = ?");
				$query->execute(array($user_id, $client_notes, $last_changed, $account_id, $client_id));

			} else {

				$query = $this->dbo->prepare("INSERT INTO client_notes SET account_id = ?, user_id = ?, client_id = ?, text = ?, last_changed = ?");
				$query->execute(array($account_id, $user_id, $client_id, $client_notes, $last_changed));
			}

			return $last_changed = date('M j, Y, g:iA', strtotime($last_changed) + $_SESSION['timezone_offset']);

	    }




	    public function loadClientFiles() {

	    	require_once('scripts/aws.phar');
			
			$s3client = Aws\S3\S3Client::factory(array(
		    	'key'    => 'AKIAJKBMGCMLYBHN77PQ',
		    	'secret' => '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'
		    ));


			$account_id = $_SESSION['account_id'];

			if($_GET['id']) {

				$client_id = $_GET['id'];

			} else if ($_POST['client_id']) {

				$client_id = $_POST['client_id'];
			
			} else {

				return false;
			}

			$files = array();


			$response = $s3client->getIterator('ListObjects', array(
			    'Bucket' => 'ironvault',
			    'Prefix' => 'clients/' . $account_id . '/' . $client_id
			));

			//$count = 0;

			foreach($response as $object) {
			    	
			    //if($count > 0) {	
			    	
		    	$object_path = explode('/', $object['Key']);
		    	$object_name = array_pop($object_path);

		    	$object_name_parts = explode('.', $object_name);
		    	$object_type = array_pop($object_name_parts);


		    	if($object_name == $object_type) {
		    		//then the file extension is missing... thus make it blank!
		    		$object_type = '';
		    	}


		    	$object_last_modified = date('M j, Y', strtotime($object['LastModified']) + $_SESSION['timezone_offset']);

		    	$object_array = array($object_name, $object_type, $object_last_modified);

		    	//Method to sort the output order of the files since the AWS SDK does not provide a sorting functionality
		    	//They last modified date of the file is used as its key when added to the files array. Since files MAY have the same timestamp, we must first check if the index is empty, otherwise shift by 1 and try again.
		    	$array_offset = 0;

		    	while(!empty($files[strtotime($object['LastModified']) + $array_offset])) {

		    		$array_offset++;
		    	
		    	}
		    	
		    	$files[strtotime($object['LastModified']) + $array_offset] = $object_array;

		    	//Sort the array by the key values, decending (latest dated ones first)
		    	krsort($files);
			    //}

			    //$count++;
			}



			foreach ($files as $file) {

				$file_type = strtolower($file[1]);

				if($file_type == "pdf") {
				 	$type_class = "attachment-pdf";
				 
				} else if($file_type == "jpg" || $file_type == "jpeg" || $file_type == "png" || $file_type == "gif" || $file_type == "bmp") {
				 	$type_class = "attachment-image";
				 
				} else if($file_type == "xls") {
				 	$type_class = "attachment-excel";
				 
				} else if($file_type == "zip" || $file_type == "rar" || $file_type == "tar" || $file_type == "gz" || $file_type == "xz") {
				 	$type_class = "attachment-zip";
				 
				} else if($file_type == "doc" || $file_type == "docx") {
				 	$type_class = "attachment-word";
				 
				} else {

				 	$type_class = "attachment-plain";
				}

				
				$attachments .= '<div class="attachment" data-filename="' . $file[0] . '">
		                           	<div class="attachment-thumb ' . $type_class . '"></div>
		                           	<div class="attachment-info" title="' . $file[0] . '">
		                              	' . $file[0] . '<br>
		                              	<span class="faded">Last Modified: ' . $file[2] . '</span>
		                           	</div>
		                           	<div class="btn-group">
										<button class="btn light-blue dropdown-toggle" data-toggle="dropdown">Action <i class="icon-angle-down"></i>
										</button>
										<ul class="dropdown-menu bottom-up">
											<li><a href="https://cdn.ironvault.ca/clients/' . $account_id . '/' . $client_id . '/' . $file[0] . '" class="download_file" download>Download File</a></li>
											<li><a href="#delete_client_file_modal" class="delete_file" data-toggle="modal">Delete</a></li>
										</ul>
									</div>
		                        </div>';
			}


			if($attachments == "") {

				$attachments = '<span class="box-message">No file attachments found. To add one now, select your file and hit \'Upload\'</span>';
			}


			return $attachments;

	    }



	    public function deleteClientFile() {

	    	require_once('scripts/aws.phar');
			
			$s3client = Aws\S3\S3Client::factory(array(
		    	'key'    => 'AKIAJKBMGCMLYBHN77PQ',
		    	'secret' => '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'
		    ));

	    	$account_id = $_SESSION['account_id'];
	    	$filename = $_POST['filename'];
	    	$client_id = $_POST['client_id'];

	    	$s3client->deleteObject(array(
			    'Bucket' => 'ironvault',
				'Key' => 'clients/' . $account_id . '/' . $client_id . '/' . $filename
			));
	    }



		public function updateClientProfile() {

	    	$account_id = $_SESSION['account_id'];

    		$client_id = $_POST['client_id'];
    		$client_type = $_POST['client_type']; 
    		$first_name = $_POST['first_name']; 
			$last_name = $_POST['last_name']; 
			$individual_email = $_POST['individual_email']; 
			$gender = $_POST['gender'];
			$primary_phone = $_POST['primary_phone'];
			$alternative_phone = $_POST['alternative_phone'];  
			$address = $_POST['address'];

			$business_name = $_POST['business_name'];
			$business_email = $_POST['business_email'];


			if($client_type == "individual") {

				$query = $this->dbo->prepare("UPDATE clients SET first_name = ?, last_name = ?, business_name = ?, type = ?, email = ?, gender = ?, primary_phone = ?, alternative_phone = ?, address = ? WHERE account_id = ? AND client_id = ?");
				$result = $query->execute(array($first_name, $last_name, $business_name, 'individual', $individual_email, $gender, $primary_phone, $alternative_phone, $address, $account_id, $client_id));
				
				//print_r($query->errorInfo()); exit;

			} else if($client_type == "business") {

				$query = $this->dbo->prepare("UPDATE clients SET first_name = ?, last_name = ?, business_name = ?, type = ?, email = ?, gender = ?, primary_phone = ?, alternative_phone = ?, address = ? WHERE account_id = ? AND client_id = ?");
				$result = $query->execute(array($first_name, $last_name, $business_name, 'business', $business_email, '', $primary_phone, $alternative_phone, $address, $account_id, $client_id));

			}

			require_once("classes/Account.class.php");
			$account = new Account();

			$account->addToTimeline("client_updated", $client_id);			

	    	header('Location: client_profile?id=' . $client_id);

	    } 




	    public function uploadClientPhoto() {

			require_once('scripts/aws.phar');
			
			$s3client = Aws\S3\S3Client::factory(array(
		    	'key'    => 'AKIAJKBMGCMLYBHN77PQ',
		    	'secret' => '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'
		    ));

	
			if (($_FILES["image_file"]["type"] == "image/jpeg") || ($_FILES["image_file"]["type"] == "image/jpg") || ($_FILES["image_file"]["type"] == "image/png")) {

				$stock_images = array('beach.jpg','bees.jpg','boats.jpg','bridge.jpg','broken_car.jpg','cat.jpg','coconut.jpg','dog.jpg','field.jpg','flower.jpg', 'fruits.jpg', 'horses.jpg', 'kitten.jpg', 'lights.jpg', 'rocks.jpg', 'sunflowers.jpg', 'waterfall.jpg', 'yellow_lights.jpg' );

				$account_id = $_SESSION['account_id'];
				$user_id = $_SESSION['user_id'];
				
				$image_type = $_FILES["image_file"]["type"];
				$image_file = $_FILES["image_file"]["tmp_name"];


				$query = $this->dbo->prepare("SELECT image FROM users WHERE account_id = ? AND user_id = ?");
				$query->execute(array($account_id, $user_id));

				$result = $query->fetchAll();

				foreach($result as $row) {

					$old_image = $row[0];
				}

				
				$filename = $account_id . "_" . $user_id . "_" . time() . ".jpg";

				$this->cropThumbnail($image_file, $filename, $image_type);
				$this->imageResize($image_file, $filename, $image_type);

				//Store the main image...
				$s3client->putObject(array(
				    'ACL' => 'public-read',
				    'Bucket' => 'ironvault',
					'SourceFile' => 'tmp_files/' . $filename,
					'Key' => 'pr_imgs/' . $filename,
					'ContentType' => 'image/jpeg',
					'StorageClass' => 'REDUCED_REDUNDANCY '
				));

				//Store the thumb image...
				$s3client->putObject(array(
				    'ACL' => 'public-read',
				    'Bucket' => 'ironvault',
					'SourceFile' => 'tmp_files/thumb_' . $filename,
					'Key' => 'pr_imgs/thumbs/' . $filename,
					'ContentType' => 'image/jpeg',
					'StorageClass' => 'REDUCED_REDUNDANCY '
				));


				//Then save the image filename into the DB
				$query = $this->dbo->prepare("UPDATE users SET image = ? WHERE account_id = ? AND user_id = ?");
				$query->execute(array($filename, $account_id, $user_id));

				$_SESSION['image'] = 'https://cdn.ironvault.ca/pr_imgs/' . $filename;
				$_SESSION['image_thumb'] = 'https://cdn.ironvault.ca/pr_imgs/thumbs/' . $filename;


				//Remove previous profile picture change from activity timeline, if any
				$query = $this->dbo->prepare("DELETE FROM timeline WHERE account_id = ? AND user_id = ? AND action = ?");
				$query->execute(array($account_id, $user_id, $profile_picture_updated));


				require_once("Account.class.php");
				$account = new Account();

				$account->addToTimeline("profile_picture_updated", $user_id);


				if(!in_array($old_image, $stock_images)) {

					$s3client->deleteObject(array(
					    'Bucket' => 'ironvault',
						'Key' => 'pr_imgs/' . $old_image
					));

					$s3client->deleteObject(array(
					    'Bucket' => 'ironvault',
						'Key' => 'pr_imgs/thumbs/' . $old_image
					));
				}

			
				header('Location: profile_settings#profile_picture');
				
			} else {
				header('Location: profile_settings#profile_picture?e=wrong_filetype');
			
				//return "File must be of type JPG/JPEG or PNG"; //not working :( cannot pass it after header()
			}
		}



		public function imageResize($image_file, $filename, $type) {
		
			if(file_exists($image_file)) {
			
				$max_width = 800;
				$max_height = 800;
					
				$path = "tmp_files/" . $filename;
				
				// if you upload a png you can use imagecreatefrompng
				if($type == "image/jpeg" || $type == "image/jpg") {
				
					$image = imagecreatefromjpeg($image_file);
					$srcwidth = ImagesX($image);
					$srcheight = ImagesY($image);
					if ($srcwidth < $max_width && $srcheight < $max_height) {
						
						// smaller than needed and will upload directly
						ImageJpeg($image, $path, 80);
						
						return $path;
						
					} else {
						// resize acording to aspect ratio
						if (($srcwidth/$srcheight) > ($max_width/$max_height)) {
							// assume that the width is bigger
							$new_width = $max_width;
							$new_height = round($max_width/$srcwidth*$srcheight);
						} else {
							// assume the height is bigger
							$new_width = round($max_height/$srcheight*$srcwidth);
							$new_height = $max_height;
						}
						$new_image = ImageCreateTrueColor($new_width, $new_height);

						ImageCopyResampled($new_image, $image, 0, 0, 0, 0, $new_width, $new_height, $srcwidth, $srcheight);
						
						// you can also use Imagepng
						ImageJpeg($new_image, $path, 80);
						
						//destroy tmp jpeg
						ImageDestroy($image);
						ImageDestroy($new_image);
						
						return $path;
					}
					
				} else if($type == "image/png") {
				
					$image = imagecreatefrompng($image_file);
					$srcwidth = ImagesX($image);
					$srcheight = ImagesY($image);
					if ($srcwidth < $max_width && $srcheight < $max_height) {
						
						// smaller than needed and will upload directly
						ImageJpeg($image, $path, 80);
						
						return $path;
						
					} else {
						// resize acording to aspect ratio
						if (($srcwidth/$srcheight) > ($max_width/$max_height)) {
							// assume that the width is bigger
							$new_width = $max_width;
							$new_height = round($max_width/$srcwidth*$srcheight);
						} else {
							// assume the height is bigger
							$new_width = round($max_height/$srcheight*$srcwidth);
							$new_height = $max_height;
						}
						$new_image = ImageCreateTrueColor($new_width, $new_height);

						ImageCopyResampled($new_image, $image, 0, 0, 0, 0, $new_width, $new_height, $srcwidth, $srcheight);
						
						// you can also use Imagepng
						ImageJpeg($new_image, $path, 80);
						
						//destroy tmp jpeg
						ImageDestroy($image);
						ImageDestroy($new_image);
						
						return $path;
					}
				
				}
				
			} else { 
				
			}
		}




		public function cropThumbnail($image_file, $filename, $type) {
		
			if($type == "image/jpeg" || $type == "image/jpg") {
			
				$image = imagecreatefromjpeg($image_file);
				$path = "tmp_files/thumb_" . $filename;

				$thumb_width = 200;
				$thumb_height = 200;

				$width = imagesx($image);
				$height = imagesy($image);

				$original_aspect = $width / $height;
				$thumb_aspect = $thumb_width / $thumb_height;

				if($original_aspect >= $thumb_aspect) {
				   // If image is wider than thumbnail (in aspect ratio sense)
				   $new_height = $thumb_height;
				   $new_width = $width / ($height / $thumb_height);
				} else {
				   // If the thumbnail is wider than the image
				   $new_width = $thumb_width;
				   $new_height = $height / ($width / $thumb_width);
				}

				$thumb = imagecreatetruecolor($thumb_width, $thumb_height);

				// Resize and crop
				imagecopyresampled($thumb, $image, 0 - ($new_width - $thumb_width) / 2, 0 - ($new_height - $thumb_height) / 2, 0, 0, $new_width, $new_height, $width, $height);
				imagejpeg($thumb, $path, 80);
				
				ImageDestroy($image);
				ImageDestroy($thumb);
			
			} else if($type == "image/png") {
			
				$image = imagecreatefrompng($image_file);
				$path = "tmp_files/thumb_" . $filename;

				$thumb_width = 200;
				$thumb_height = 200;

				$width = imagesx($image);
				$height = imagesy($image);

				$original_aspect = $width / $height;
				$thumb_aspect = $thumb_width / $thumb_height;

				if($original_aspect >= $thumb_aspect) {
				   // If image is wider than thumbnail (in aspect ratio sense)
				   $new_height = $thumb_height;
				   $new_width = $width / ($height / $thumb_height);
				} else {
				   // If the thumbnail is wider than the image
				   $new_width = $thumb_width;
				   $new_height = $height / ($width / $thumb_width);
				}

				$thumb = imagecreatetruecolor($thumb_width, $thumb_height);

				// Resize and crop
				imagecopyresampled($thumb, $image, 0 - ($new_width - $thumb_width) / 2, 0 - ($new_height - $thumb_height) / 2, 0, 0, $new_width, $new_height, $width, $height);
				imagejpeg($thumb, $path, 80);
				
				ImageDestroy($image);
				ImageDestroy($thumb);
			
			}
		}




	    public function loadClientsList() {

	    	$account_id = $_SESSION['account_id'];
	    	$data = "";

	    	$query = $this->dbo->prepare("SELECT client_id, first_name, last_name, business_name, email, primary_phone, address, image, last_invoice, last_invoice_date, type, gender FROM clients WHERE account_id = ?");
			$query->execute(array($account_id));

			$result = $query->fetchAll();

			foreach($result as $row) {

	    		$client_id = $row[0];
	    		$first_name = $row[1];
	    		$last_name = $row[2];
	    		$business_name = $row[3];
	    		$email = $row[4];
	    		$primary_phone = $row[5];
	    		$address = $row[6];
	    		$image = $row[7];
	    		$last_invoice = $row[8];
	    		$last_invoice_date = $row[9];
	    		$type = $row[10];
	    		$gender = $row[11];

	    		$last_invoice_date = date('M j, Y', strtotime($last_invoice_date) + $_SESSION['timezone_offset']);

	    		$display_name = "";

	    		if($type == "individual") {

	    			$display_name = $first_name . " " . $last_name;

	    		} else if($type == "business") {

	    			$display_name = $business_name;
	    		}


	    		$primary_phone = preg_replace("/^(\d{3})(\d{3})(\d{4})$/", "($1) $2-$3", $primary_phone);


	    		if($image != "") {

	    			$image = '<img src="' . $image . '" alt="' . $display_name . '" />';

	    		} else {

		    		if($type == "individual") {

						if($gender == "male") {

							$image = '<img src="img/male_default_thumb.png" alt="' . $display_name . '" />';

						}

						if($gender == "female") {

							$image = '<img src="img/female_default_thumb.png" alt="' . $display_name . '" />';

						}
		 
					} else if($type == "business") {

						$image = '<img src="img/company_default_thumb.png" alt="' . $display_name . '" />';
					}
				}


				if($last_invoice != "" && $last_invoice != 0) {

					$last_invoice_string = '<a href="https://' . $_SERVER["SERVER_NAME"] . '/invoice?id=' . $last_invoice . '">#' . $last_invoice . ' – ' . $last_invoice_date . '</a>';
				
				} else {

					$last_invoice_string = '–';
				}


	    		$data['table_view'] .= "<tr id='" . $client_id . "'>"; 

	    		$data['table_view'] .= '<td data-title="Name">' . $display_name . '</td>
							<td data-title="Address">' . $address . '</td>
							<td data-title="Email">' . $email . '</td>
							<td data-title="Primary Phone">' . $primary_phone . '</td>
							<td data-title="Last Invoice">' . $last_invoice_string . '</td>
							<td data-title="Action" class="button">
								<div class="btn-group">
									<button class="btn light-blue dropdown-toggle" data-toggle="dropdown">Action <i class="icon-angle-down"></i>
									</button>
									<ul class="dropdown-menu pull-right">
										<li><a href="client_profile?id=' . $client_id . '#update_client_profile" class="update_client_info">Update Client Info</a></li>
										<li><a href="#delete_client_modal" data-toggle="modal" class="delete_client">Delete Client</a></li>
									</ul>
								</div>
							</td>
						</tr>';


				if($type == "business") {
					$client_name = $business_name;
				} else if($type == "individual") {
					$client_name = $first_name . ' ' . $last_name;
				}

				$data['grid_view'] .= '<div id="' . $client_id . '" class="client_item span3 m-span3">
										<div class="client_image">' . $image . '</div>
										<span class="client_name">' . $client_name . '</span>
										<span class="client_email"><i class="icon-envelope"></i> ' . $email . '</span>
										<span class="client_phone"><i class="icon-phone"></i> ' . $primary_phone . '</span>
										<span class="client_address"><i class="icon-home"></i> ' . $address . '</span>
									</div>';

			}

			return $data;

	    }


	    public function clientsListDropdown() {

	    	$account_id = $_SESSION['account_id'];
	    	$data = "";

	    	$query = $this->dbo->prepare("SELECT client_id, first_name, last_name, business_name, type FROM clients WHERE account_id = ?");
			$query->execute(array($account_id));

			$result = $query->fetchAll();

			foreach($result as $row) {

				$display_name = '';

	    		$client_id = $row[0];
	    		$first_name = $row[1];
	    		$last_name = $row[2];
	    		$business_name = $row[3];
	    		$type = $row[4];

	    		if($type == 'individual') {

	    			$display_name = $first_name . ' ' . $last_name;

	    		} else if($type == 'business') {

	    			$display_name = $business_name;
	    		}

	    		$data .= '<option value="' . $client_id . '">' . $display_name . '</option>';

			}

			return $data;
	    }

	}

?>