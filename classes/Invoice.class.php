<?php

	session_start();

	require_once('error_handler.php');

	
	Class Invoice { 



		public function __construct () {

			require("inc/connect_dbo.php");

			$this->dbo = $dbo;

		}


		public function loadInvoices() {

			require_once("Account.class.php");
			$account = new Account();

			$currency = $account->getCurrency("");


			$account_id = $_SESSION['account_id'];

			$query = $this->dbo->prepare("SELECT id, client_id, invoice_number, date_created, date_modified, created_by, status, total FROM invoices WHERE account_id = ?");
			$query->execute(array($account_id));

			$result = $query->fetchAll();

			foreach($result as $row) {

				$client_display_name = '';
				$emp_name = '';

				$id = $row[0];
				$client_id = $row[1];
				$invoice_number = $row[2];
				$date_created = $row[3];
				$date_modified = $row[4];
				$created_by = $row[5];
				$status = ucwords($row[6]);
				$total = $row[7];

				$created_datetime = date('M j, Y, g:iA', strtotime($date_created) + $_SESSION['timezone_offset']);

				$modified_datetime = date('M j, Y, g:iA', strtotime($date_modified) + $_SESSION['timezone_offset']);


				$query = $this->dbo->prepare("SELECT first_name, last_name, business_name, type FROM clients WHERE account_id = ? AND client_id = ?");
				$query->execute(array($account_id, $client_id));

				$result = $query->fetchAll();

				foreach($result as $row) {

					$first_name = $row[0];
					$last_name = $row[1];
					$business_name = $row[2];
					$type = $row[3];

					if($type == "individual") {

						$client_display_name = $first_name . " " . $last_name;

					} else if($type == "business") {

						$client_display_name = $business_name;
					}

				}



				$query = $this->dbo->prepare("SELECT first_name, last_name FROM users WHERE account_id = ? AND user_id = ?");
				$query->execute(array($account_id, $created_by));

				$result = $query->fetchAll();

				foreach($result as $row) {

					$first_name = $row[0];
					$last_name = $row[1];

					$emp_name = $first_name . " " . $last_name;

				}



				$data .= '<tr class="" id="' . $id . '">
							<td>' . $invoice_number . '</td>
							<td>' . $client_display_name . '</td> 
							<td>' . $created_datetime . '</td>
							<td>' . $modified_datetime . '</td>
							<td>' . $emp_name . '</td>
							<td>' . $status . '</td>
							<td>' . $currency . $total . '</td>
							<td data-title="Action" class="button">
								<div class="btn-group">
									<button class="btn light-blue dropdown-toggle" data-toggle="dropdown">Action <i class="icon-angle-down"></i>
									</button>
									<ul class="dropdown-menu pull-right">
										<li><a href="new_invoice.php?like=' . $id . '" data-toggle="modal" class="clone_invoice">Clone Invoice</a></li>
									</ul>
								</div>
							</td>
						</tr>';

			}

			return $data;

		}



		public function loadRecurringInvoices() {

			require_once("Account.class.php");
			$account = new Account();

			$currency = $account->getCurrency("");


			$account_id = $_SESSION['account_id'];

			$query = $this->dbo->prepare("SELECT id, client_id, created_by, status, total FROM invoice_templates WHERE account_id = ?");
			$query->execute(array($account_id));

			$result = $query->fetchAll();

			foreach($result as $row) {

				$id = $row[0];
				$client_id = $row[1];
				$created_by = $row[2];
				$status = $row[3];
				$total = $row[4];

				$recur_start = date('M j, Y', strtotime($recur_start) + $_SESSION['timezone_offset']);
				$recur_end = date('M j, Y', strtotime($recur_end) + $_SESSION['timezone_offset']);

				$last_invoice_date = "";
				$next_invoice_date = "";

				if($status == "scheduled") {

					$action = '<a href="#" class="btn light-red pause width_50">Pause</a>';
				} else  if($status == "paused") {

					$action = '<a href="#" class="btn light-green start width_50">Start</a>';
				}


				$query = $this->dbo->prepare("SELECT first_name, last_name, business_name, type FROM clients WHERE account_id = ? AND client_id = ?");
				$query->execute(array($account_id, $client_id));

				$result = $query->fetchAll();

				foreach($result as $row) {

					$first_name = $row[0];
					$last_name = $row[1];
					$business_name = $row[2];
					$type = $row[3];

					if($type == "individual") {

						$client_display_name = $first_name . " " . $last_name;

					} else if($type == "company") {

						$client_display_name = $business_name;
					}

				}



				$query = $this->dbo->prepare("SELECT first_name, last_name FROM users WHERE account_id = ? AND user_id = ?");
				$query->execute(array($account_id, $created_by));

				$result = $query->fetchAll();

				foreach($result as $row) {

					$first_name = $row[0];
					$last_name = $row[1];

					$emp_name = $first_name . " " . $last_name;

				}



				$query = $this->dbo->prepare("SELECT date_created FROM invoices WHERE account_id = ? AND from_template_id = ? ORDER BY date_created DESC LIMIT 1");
				$query->execute(array($account_id, $created_by));

				$result = $query->fetchAll();

				foreach($result as $row) {

					$last_invoice_date = $row[0];

					$last_invoice_date = date('M j, Y', strtotime($last_invoice_date) + $_SESSION['timezone_offset']);

				}



				$data .= '<tr class="" id="' . $id . '">
							<td>' . $client_display_name . '</td> 
							<td>' . ucfirst($status) . '</td> 
							<td>' . $emp_name . '</td>
							<td>' . $last_invoice_date . '</td>
							<td>' . $next_invoice_date . '</td>
							<td>' . $currency . $total . '</td>
							<td data-title="Action" class="button">
								' . $action . '
							</td>
						</tr>';

			}

			return $data;

		}



		public function loadClientInvoices() {

			require_once("Account.class.php");
			$account = new Account();

			$currency = $account->getCurrency("");

			$account_id = $_SESSION['account_id'];

			$client_id = $_GET['id'];

			$query = $this->dbo->prepare("SELECT id, invoice_number, date_created, date_modified, created_by, status, total FROM invoices WHERE account_id = ? AND client_id = ?");
			$query->execute(array($account_id, $id));

			$result = $query->fetchAll();

			foreach($result as $row) {

				$id = $row[0];
				$client_id = $row[1];
				$invoice_number = $row[2];
				$date_created = $row[3];
				$date_modified = $row[4];
				$created_by = $row[5];
				$status = $row[6];
				$total = $row[7];

				$created_datetime = date('M j, Y, g:iA', strtotime($date_created) + $_SESSION['timezone_offset']);

				$modified_datetime = date('M j, Y, g:iA', strtotime($date_modified) + $_SESSION['timezone_offset']);



				$query = $this->dbo->prepare("SELECT first_name, last_name FROM users WHERE account_id = ? AND user_id = ?");
				$query->execute(array($account_id, $created_by));

				$result = $query->fetchAll();

				foreach($result as $row) {

					$first_name = $row[0];
					$last_name = $row[1];

					$emp_name = $first_name . " " . $last_name;

				}



				$data .= '<tr class="">
							<td>' . $invoice_number . '</td>
							<td>' . $created_datetime . '</td>
							<td>' . $modified_datetime . '</td>
							<td>' . $emp_name . '</td>
							<td>' . $status . '</td>
							<td>' . $currency . $total . '</td>
							<td data-title="Action" class="button">
								<div class="btn-group">
									<button class="btn light-blue dropdown-toggle" data-toggle="dropdown">Action <i class="icon-angle-down"></i>
									</button>
									<ul class="dropdown-menu pull-right">
										<li><a href="new_invoice.php?like=' . $id . '" data-toggle="modal" class="clone_invoice">Clone Invoice</a></li>
									</ul>
								</div>
							</td>
						</tr>';

			}

			return $data;

		}



		public function createNewInvoice() {

			$account_id = $_SESSION['account_id'];
			$user_id = $_SESSION['user_id'];

			$client_id = $_GET['client_id']; //If new invoice is being created from client profile page... add the client id to the invoice

			if($client_id == '') {

				$client_id = 0;
			}


			$query = $this->dbo->prepare("SELECT calculate_tax, tax_rate, currency FROM account_preferences WHERE account_id = ?");
			$query->execute(array($account_id));

			$result = $query->fetchAll();

			foreach($result as $row) {

    			$calculate_tax = $row[0];
    			$tax_rate = $row[1];
    			$currency = $row[2];
 
    		}

			$datetime = date('Y-m-d H:i:s');

			$query = $this->dbo->prepare("SELECT MAX(invoice_number) AS invoice_number FROM invoices WHERE account_id = ?");
			$query->execute(array($account_id));

			$result = $query->fetchAll();

			foreach($result as $row) {

    			$max_invoice_number = $row[0];
 
    		}

    		//Remember... Invoice Number and invoice_id are different!! 
    		$invoice_number = $max_invoice_number + 1;

    		$access_token = hash('SHA1', time());

			$query = $this->dbo->prepare("INSERT INTO invoices SET account_id = ?, client_id = ?, invoice_number = ?, date_created = ?, date_modified = ?, created_by = ?, modified_by = ?, status = ?, currency = ?, calculate_tax = ?, tax_rate = ?, access_token = ?");
			$query->execute(array($account_id, $client_id, $invoice_number, $datetime, $datetime, $user_id, $user_id, 'draft', $currency, $calculate_tax, $tax_rate, $access_token));

			$invoice_id = $this->dbo->lastInsertId();

			return $invoice_id;

		}



		public function createNewRecurringInvoice() {

			$account_id = $_SESSION['account_id'];
			$user_id = $_SESSION['user_id'];

			$client_id = $_GET['client_id']; //If new invoice is being created from client profile page... add the client id to the invoice

			if($client_id == '') {

				$client_id = 0;
			}

			$datetime = date('Y-m-d H:i:s');


			$query = $this->dbo->prepare("INSERT INTO invoice_templates SET account_id = ?, client_id = ?, date_created = ?, date_modified = ?, created_by = ?, modified_by = ?, status = ?");
			$query->execute(array($account_id, $client_id, $datetime, $datetime, $user_id, $user_id, 'draft'));

			$invoice_id = $this->dbo->lastInsertId();

			return $invoice_id;

		}



		public function cloneInvoice() {

			$account_id = $_SESSION['account_id'];
			$user_id = $_SESSION['user_id'];

			$like_id = $_GET['like']; //Get the id of the invoice to be cloned...


			$datetime = date('Y-m-d H:i:s');

			$query = $this->dbo->prepare("SELECT MAX(invoice_number) AS invoice_number FROM invoices WHERE account_id = ?");
			$query->execute(array($account_id));

			$result = $query->fetchAll();

			foreach($result as $row) {

    			$max_invoice_number = $row[0];
 
    		}

    		//Remember... Invoice Number and invoice_id are different!! 
    		$invoice_number = $max_invoice_number + 1;


    		//get the info from the invoice to be cloned... includes invoice_items and S3 files!!!
    		$query = $this->dbo->prepare("SELECT client_id, notes, payment_term, payment_type, total FROM invoices WHERE account_id = ? AND id = ?");
			$query->execute(array($account_id, $like_id));

			$result = $query->fetchAll();

			foreach($result as $row) {

    			$client_id = $row[0];
    			$notes = $row[1];
    			$payment_term = $row[2];
    			$payment_type = $row[3];
    			$total = $row[4];

    		}


    		$query = $this->dbo->prepare("SELECT calculate_tax, tax_rate, currency FROM account_preferences WHERE account_id = ?");
			$query->execute(array($account_id));

			$result = $query->fetchAll();

			foreach($result as $row) {

    			$calculate_tax = $row[0];
    			$tax_rate = $row[1];
    			$currency = $row[2];
 
    		}

    		if($calculate_tax == 0) {

    			$tax_rate = 0;
    		}


    		if($payment_term > 0) {

    			$due_date = date('Y-m-d H:i:s', strtotime($datetime . ' + ' . $payment_term . ' days'));
    		}


    		//Create new invoice...
    		$query = $this->dbo->prepare("INSERT INTO invoices SET account_id = ?, client_id = ?, invoice_number = ?, date_created = ?, date_modified = ?, created_by = ?, modified_by = ?, status = ?, notes = ?, invoice_date = ?, due_date = ?, payment_term = ?, payment_type = ?, currency = ?, calculate_tax = ?, tax_rate = ?");
			$query->execute(array($account_id, $client_id, $invoice_number, $datetime, $datetime, $user_id, $user_id, 'draft', $notes, $datetime, $due_date, $payment_term, $payment_type, $currency, $calculate_tax, $tax_rate));

			$invoice_id = $this->dbo->lastInsertId();



    		$subtotal = 0;
			$total = 0;
        	$after_discount = 0;
        	$tax_amount = 0;


    		$query = $this->dbo->prepare("SELECT item_id, item_qty FROM invoice_items WHERE account_id = ? AND invoice_id = ? ORDER BY id ASC");
			$query->execute(array($account_id, $like_id));

			$result = $query->fetchAll();

			foreach($result as $row) {

    			$item_id = $row[0];
    			$item_qty = $row[1];

    			$query2 = $this->dbo->prepare("SELECT item_cost FROM inventory WHERE account_id = ? AND item_id = ?");
				$query2->execute(array($account_id, $item_id));

				$result2 = $query2->fetchAll();

				foreach($result2 as $row2) {
 
					$item_cost = $row2[0];
 				}


 				$item_total = $item_qty * $item_cost;

				$subtotal = $subtotal + $item_total;


 				$query2 = $this->dbo->prepare("INSERT INTO invoice_items SET account_id = ?, invoice_id = ?, item_id = ?, item_qty = ?, item_cost = ?");
				$query2->execute(array($account_id, $invoice_id, $item_id, $item_qty, $item_cost));

    		}



    		// no discounts being copied when cloning invoices... soooo...
        	$after_discount = $subtotal;


        	if($calculate_tax == 1) {

	        	if($after_discount > 0) {

	        		$tax_amount = ($after_discount * ($tax_rate / 100));

	        	} else {

	        		$tax_amount = 0;
	        	}

	        }

        	$total = $after_discount + $tax_amount;


        	$query = $this->dbo->prepare("UPDATE invoices SET total = ? WHERE account_id = ? AND invoice_id = ?");
			$query->execute(array($total, $account_id, $invoice_id));


    		//Copy the files...
    		require_once('scripts/aws.phar');
			
			$s3client = Aws\S3\S3Client::factory(array(
		    	'key'    => 'AKIAJKBMGCMLYBHN77PQ',
		    	'secret' => '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'
		    ));


			$response = $s3client->getIterator('ListObjects', array(
			    'Bucket' => 'ironvault',
			    'Prefix' => 'invoices/' . $account_id . '/' . $like_id
			));

			foreach($response as $object) {

				$new_object_key = str_replace('/' . $like_id . '/', '/' . $invoice_id . '/', $object['Key']);

				$s3client->copyObject(array(
				    'Bucket'     => 'ironvault',
				    'Key'        => $new_object_key,
				    'CopySource' => "ironvault/" . $object['Key'],
				    'StorageClass' => 'REDUCED_REDUNDANCY'
				));

			}

    		
			return $invoice_id;

		}



		public function loadCreateInvoice() {

			require_once("Account.class.php");
			$account = new Account();
	    	
	    	$account_id = $_SESSION['account_id'];
	    	$business_name = $_SESSION['business_name'];

	    	$invoice_id = $_GET['id'];

	    	$data_array = array();

	    	$data_array['invoice_id'] = $invoice_id;


	    	$query = $this->dbo->prepare("SELECT client_id, invoice_number, notes, status, invoice_date, due_date, payment_term, payment_type, currency, discount_amount, discount_type, calculate_tax, tax_rate FROM invoices WHERE account_id = ? AND id = ?");
			$query->execute(array($account_id, $invoice_id));

			$result = $query->fetchAll();

			foreach($result as $row) {

				$client_ref_id = $row[0];
    			$invoice_number = $row[1];
    			$notes = $row[2];
    			$status = $row[3];
    			$invoice_date = $row[4];
    			$due_date = $row[5];
    			$payment_term = $row[6];
    			$payment_type = $row[7];
    			$currency = $row[8];
    			$discount_amount = $row[9];
    			$discount_type = $row[10];
    			$calculate_tax = $row[11];
    			$tax_rate = $row[12];

    		}


    		if(count($result) == 0) {

    		 	header('Location: invoices'); //There was no match for the invoice_id under this account
    		}

    		if($status == 'processed') {

    			header('Location: invoice?id=' . $invoice_id);
    		}


    		$data_array['invoice_number'] = $invoice_number;
    		$data_array['notes'] = $notes;

    		$data_array['status'] = ucwords($status);

    		if($invoice_date != '0000-00-00 00:00:00') {

    			$data_array['invoice_date'] = date('F j, Y', strtotime($invoice_date));
    		
    		} else {

    			$data_array['invoice_date'] = date('F j, Y');
    		}

    		if($due_date != '0000-00-00 00:00:00') {

    			$data_array['due_date'] = date('F j, Y', strtotime($due_date));
    		
    		} else {

    			$data_array['due_date'] = date('F j, Y');
    		}

			
			$data_array['payment_type'] =  '<option value="cash" ' . ($payment_type == 'cash' ? 'selected="selected"' : '') . '>Cash</option>
			<option value="debit_credit" ' . ($payment_type == 'debit_credit' ? 'selected="selected"' : '')  . '>Debit/Credit</option>
			<option value="cheque" ' . ($payment_type == 'cheque' ? 'selected="selected"' : '') . '>Cheque</option>
			<option value="wire_transfer" ' . ($payment_type == 'wire_transfer' ? 'selected="selected"' : '') . '>Wire Transfer</option>
			<option value="other" ' . ($payment_type == 'other' ? 'selected="selected"' : '') . '>Other</option>';
    		
			$data_array['payment_term'] = '<option value="0" ' . ($payment_term == 0 ? 'selected="selected"' : '') . '>Due on receipt</option>
			<option value="15" ' . ($payment_term == 15 ? 'selected="selected"' : '') . '>15 Days</option>
			<option value="30" ' . ($payment_term == 30 ? 'selected="selected"' : '') . '>30 Days</option>
			<option value="60" ' . ($payment_term == 60 ? 'selected="selected"' : '') . '>60 Days</option>
			<option value="90" ' . ($payment_term == 90 ? 'selected="selected"' : '') . '>90 Days</option>';

    		$data_array['currency'] = $account->getCurrency($currency);


        	if($discount_type == 'amount') {

        		$discount = money_format('%i', $discount_amount);

        		$data_array['discount_li'] = '<li id="discount">Discount: ' . $data_array['currency'] . '<strong>–' . $discount . '</strong> (<a href="#" id="remove_discount">remove</a>)</li>';
        	
        	} else if($discount_type == 'percent') {

        		$discount = (float)$discount_amount;

        		$data_array['discount_li'] = '<li id="discount">Discount: <strong>' . $discount . '</strong>% (<a href="#" id="remove_discount">remove</a>)</li>';
        	}


        	$data_array['discount_amount'] = $discount_amount;
        	$data_array['discount_type'] = $discount_type;



    		$data_array['calculate_tax'] = $calculate_tax;

    		$data_array['tax_rate'] = (float)$tax_rate;


    		$query = $this->dbo->prepare("SELECT discounts FROM account_preferences WHERE account_id = ?");
			$query->execute(array($account_id));

			$result = $query->fetchAll();

			foreach($result as $row) {

    			$discounts = $row[0];

    		}



    		if($discounts == 'supervisor' || $discounts == 'all') {

    			$data_array['discount_button'] = '<a href="#apply_discount" class="apply_discount btn light-blue right" data-toggle="modal">Apply Discount</a>';

    			$data_array['discount_modal'] = '<div id="apply_discount" class="modal hide fade" tabindex="-1" data-focus-on="input:first">
						<form class="zero_margin" action="#" method="post" class="apply_discount_form">

							<div class="modal-header">
								<i class="icon-remove right" data-dismiss="modal" aria-hidden="true"></i>
								<h3><i class="icon-book"></i>&nbsp;&nbsp;Apply Discount</h3>
							</div>

							<div class="modal-body">
								<p>Enter the desired amount of discount below.</p>
								<div class="row-fluid">

					               <div class="span6 ">
					                  <div class="control-group">
					                    <label class="control-label">Discount</label>
					                    <div class="controls">
					                       <input type="text" name="discount_amount" id="discount_amount" class="m-wrap span12"/>
					                    </div>
					                  </div>
					               </div>

					               <div class="span6 ">
					                  <div class="control-group">
					                    <label class="control-label">Type</label>
					                    <div class="controls">
					                       <select name="discount_type" id="discount_type" class="m-wrap span12">
					                       		<option value="amount">Amount (' . $data_array['currency'] . ')</option>
					                       		<option value="percent">Percentage (%)</option>
					                       </select>
					                    </div>
					                  </div>
					               </div>
					               
					            </div>';

				if($discounts == 'supervisor' && $_SESSION['account_type'] == 3) {

					require_once('classes/Account.class.php');
					$account = new Account();

					$data_array['discount_modal'] .= '<hr>
							<p>Provide supervisor\'s credentials.</p>
							<input type="hidden" id="verify_supervisor_account" value="1" />
		                    <div class="row-fluid">
		                       <div class="span6 ">
		                          <div class="control-group">
		                            <label class="control-label">Supervisor</label>
		                            <div class="controls">
		                               <select name="super_username" id="super_username" class="m-wrap span12">';	

		            $supervisors = $account->getSupervisors();

		            $data_array['discount_modal'] .= $supervisors;

		            $data_array['discount_modal'] .= '</select>
	                            <span class="help-block hidden verify_supervisor_error"></span>
	                            </div>
	                          </div>
	                       </div>

	                       <div class="span6 ">
	                          <div class="control-group">
	                            <label class="control-label">Password</label>
	                            <div class="controls">
	                               <input type="password" name="super_password" id="super_password" class="m-wrap span12" />
	                            </div>
	                          </div>
	                       </div>
	                       
	                    </div>';
				}		                                

				$data_array['discount_modal'] .= '</div>	

							<div class="modal-footer">
								<input type="hidden" id="invoice_id" value="' . $data_array['invoice_id'] . '" />
								<input type="submit" id="submit" class="btn light-green right apply_discount_submit" value="Apply Discount" />
								<input type="button" data-dismiss="modal" class="btn" value="Cancel" />
							</div>


						</form>
					</div>';

    		}


	    	$query = $this->dbo->prepare("SELECT client_id, first_name, last_name, business_name, type FROM clients WHERE account_id = ?");
			$query->execute(array($account_id));

			$result = $query->fetchAll();

			foreach($result as $row) {

    			$client_id = $row[0];
    			$first_name = $row[1];
    			$last_name = $row[2];
    			$business_name = $row[3];
    			$type = $row[4];

    			if($type == 'individual') {
    				
    				$client_display_name = $first_name . ' ' . $last_name;

    			} else if($type == 'business') {

    				$client_display_name = $business_name;
    			}

    			if($client_ref_id == $client_id) {
    				
    				$data_array['clients_list'] .= '<option value="' . $client_id . '" selected>' . $client_display_name . '</option>';
    			
    			} else {

    				$data_array['clients_list'] .= '<option value="' . $client_id . '">' . $client_display_name . '</option>';
    			}

    		}



    		$query = $this->dbo->prepare("SELECT item_id, item_name, item_desc, item_cost FROM inventory WHERE account_id = ?");
			$query->execute(array($account_id));

			$result = $query->fetchAll();

			foreach($result as $row) {

    			$item_id = $row[0];
    			$item_name = $row[1];
    			$item_desc = $row[2];
    			$item_cost = $row[3]; 

    			$data_array['product_list'] .= '<option value="' . $item_id . '" class="' . $item_id . '" desc="' . $item_desc  . '" cost="' . $item_cost . '">' . $item_name . '</option>';
 
    		}


    		$count = 1;

    		$query = $this->dbo->prepare("SELECT item_id, item_qty FROM invoice_items WHERE account_id = ? AND invoice_id = ? ORDER BY id ASC");
			$query->execute(array($account_id, $invoice_id));

			$result = $query->fetchAll();

			foreach($result as $row) {

    			$item_id = $row[0];
    			$item_qty = $row[1];

    			$query2 = $this->dbo->prepare("SELECT item_name, item_desc, item_cost FROM inventory WHERE account_id = ? AND item_id = ?");
				$query2->execute(array($account_id, $item_id));

				$result2 = $query2->fetchAll();

				foreach($result2 as $row2) {

	    			$item_name = $row2[0];
	    			$item_desc = $row2[1];
	    			$item_cost = $row2[2]; 
	 
	    		}


	    		$data_array['items'] .= '<tr> 
										<td>' . $count . '</td>
										<td class="item_name">
											<div class="control-group">
												<div class="controls">
													<select id="" name="select_product" class="span12 select2 search_dropdown select_product">
														<option value="' . $item_id . '" class="' . $item_id . '" desc="' . $item_desc  . '" cost="' . $item_cost . '" selected="selected">' . $item_name . '</option>'
														. $data_array['product_list'] .
													'</select>
												</div> 
											</div>
										</td>
										<td class="hidden-480 item_desc">' . $item_desc . '</td>
										<td class="hidden-480 item_qty"><input type="text" name="quantity" class="span2 m-wrap small quantity" value="' . $item_qty . '"></td>
										<td class="hidden-480 item_cost"><input type="text" value="' . $item_cost . '"class="m-wrap small" readonly/></td>
										<td class="item_total"><input type="text" value="0.00"class="m-wrap small" readonly/></td>
										<td><a href="#" class="remove_item"><i class="icon-remove"></i></a></td>
									</tr>';
				$count++;
    		}


    		if($data_array['items'] == '') {  //If there are no items for invoice... add blank row


    			$data_array['items'] = '<tr> 
										<td>1</td>
										<td class="item_name">
											<div class="control-group">
												<div class="controls">
													<select id="" name="select_product" class="span12 select2 search_dropdown select_product">
														<option value=""></option>' .
														$data_array['product_list'] .
													'</select>
												</div> 
											</div>
										</td>
										<td class="hidden-480 item_desc"></td>
										<td class="hidden-480 item_qty"><input type="text" name="quantity" class="span2 m-wrap small quantity" value="1"></td>
										<td class="hidden-480 item_cost"><input type="text" value="0.00"class="m-wrap small" readonly/></td>
										<td class="item_total"><input type="text" value="0.00"class="m-wrap small" readonly/></td>
										<td></td>
									</tr>';

    		}


    		return $data_array;

	    }



	    public function loadCreateRecurringInvoice() {

			require_once("Account.class.php");
			$account = new Account();
	    	
	    	$account_id = $_SESSION['account_id'];
	    	$business_name = $_SESSION['business_name'];

	    	$invoice_id = $_GET['id'];

	    	$data_array = array();

	    	$data_array['invoice_id'] = $invoice_id;


	    	$query = $this->dbo->prepare("SELECT client_id, notes, status, payment_term, payment_type, currency, discount_amount, discount_type, calculate_tax FROM invoice_templates WHERE account_id = ? AND id = ?");
			$query->execute(array($account_id, $invoice_id));

			$result = $query->fetchAll();

			foreach($result as $row) {

				$client_ref_id = $row[0];
    			$notes = $row[1];
    			$status = $row[2];
    			$payment_term = $row[3];
    			$payment_type = $row[4];
    			$currency = $row[5];
    			$discount_amount = $row[6];
    			$discount_type = $row[7];
    			$calculate_tax = $row[8];
    			$email_invoice = $row[9];

    			$recurring_interval = $row[10];
    			$recurring_daily_days = $row[11];
    			$recurring_weekly_weeks = $row[12];
    			$recurring_weekly_weekday = $row[13];
    			$recurring_monthly_day = $row[14];
    			$recurring_monthly_months = $row[15];
    			$recurring_yearly_month = $row[16];
    			$recurring_yearly_day = $row[17];
    			$recurring_start_date = $row[18];
    			$recurring_date_range = $row[19];
    			$recurring_end_date = $row[20];
    			$recurring_end_count = $row[21];

    		}


    		if(count($result) == 0) {

    		 	header('Location: recurring_invoices'); //There was no match for the invoice_id under this account
    		}

    		if($status == 'processed') {

    			header('Location: recurring_invoice?id=' . $invoice_id);
    		}


    		$data_array['invoice_number'] = $invoice_number;
    		$data_array['notes'] = $notes;

    		$data_array['status'] = ucwords($status);

			
			$data_array['payment_type'] =  '<option value="cash" ' . ($payment_type == 'cash' ? 'selected="selected"' : '') . '>Cash</option>
			<option value="debit_credit" ' . ($payment_type == 'debit_credit' ? 'selected="selected"' : '')  . '>Debit/Credit</option>
			<option value="cheque" ' . ($payment_type == 'cheque' ? 'selected="selected"' : '') . '>Cheque</option>
			<option value="wire_transfer" ' . ($payment_type == 'wire_transfer' ? 'selected="selected"' : '') . '>Wire Transfer</option>
			<option value="other" ' . ($payment_type == 'other' ? 'selected="selected"' : '') . '>Other</option>';
    		
			$data_array['payment_term'] = '<option value="0" ' . ($payment_term == 0 ? 'selected="selected"' : '') . '>Due on receipt</option>
			<option value="15" ' . ($payment_term == 15 ? 'selected="selected"' : '') . '>15 Days</option>
			<option value="30" ' . ($payment_term == 30 ? 'selected="selected"' : '') . '>30 Days</option>
			<option value="60" ' . ($payment_term == 60 ? 'selected="selected"' : '') . '>60 Days</option>
			<option value="90" ' . ($payment_term == 90 ? 'selected="selected"' : '') . '>90 Days</option>';

    		$data_array['currency'] = $account->getCurrency($currency);

    		
    		if($discount_type == 'amount') {

        		$discount = money_format('%i', $discount_amount);

        		$data_array['discount_li'] = '<li id="discount">Discount: ' . $data_array['currency'] . '<strong>–' . $discount . '</strong> (<a href="#" id="remove_discount">remove</a>)</li>';
        	
        	} else if($discount_type == 'percent') {

        		$data_array['discount_li'] = '<li id="discount">Discount: <strong>' . $discount_amount . '</strong>% (<a href="#" id="remove_discount">remove</a>)</li>';
        	}


        	$data_array['discount_amount'] = $discount_amount;
        	$data_array['discount_type'] = $discount_type;
    		

    		$data_array['calculate_tax'] = $calculate_tax;



    		$query = $this->dbo->prepare("SELECT tax_rate, discounts FROM account_preferences WHERE account_id = ?");
			$query->execute(array($account_id));

			$result = $query->fetchAll();

			foreach($result as $row) {

    			$data_array['tax_rate'] = $row[0];
    			$discounts = $row[1];

    		}



    		if($discounts == 'supervisor' || $discounts == 'all') {

    			$data_array['discount_button'] = '<a href="#apply_discount" class="apply_discount btn light-blue right" data-toggle="modal">Apply Discount</a>';

    			$data_array['discount_modal'] = '<div id="apply_discount" class="modal hide fade" tabindex="-1" data-focus-on="input:first">
						<form class="zero_margin" action="#" method="post" class="apply_discount_form">

							<div class="modal-header">
								<i class="icon-remove right" data-dismiss="modal" aria-hidden="true"></i>
								<h3><i class="icon-book"></i>&nbsp;&nbsp;Apply Discount</h3>
							</div>

							<div class="modal-body">
								<p>Enter the desired amount of discount below.</p>
								<div class="row-fluid">

					               <div class="span6 ">
					                  <div class="control-group">
					                    <label class="control-label">Discount</label>
					                    <div class="controls">
					                       <input type="text" name="discount_amount" id="discount_amount" class="m-wrap span12"/>
					                    </div>
					                  </div>
					               </div>

					               <div class="span6 ">
					                  <div class="control-group">
					                    <label class="control-label">Type</label>
					                    <div class="controls">
					                       <select name="discount_type" id="discount_type" class="m-wrap span12">
					                       		<option value="amount">Amount (' . $data_array['currency'] . ')</option>
					                       		<option value="percent">Percentage (%)</option>
					                       </select>
					                    </div>
					                  </div>
					               </div>
					               
					            </div>';

				if($discounts == 'supervisor' && $_SESSION['account_type'] == 3) {

					require_once('classes/Account.class.php');
					$account = new Account();

					$data_array['discount_modal'] .= '<hr>
							<p>Provide supervisor\'s credentials.</p>
							<input type="hidden" id="verify_supervisor_account" value="1" />
		                    <div class="row-fluid">
		                       <div class="span6 ">
		                          <div class="control-group">
		                            <label class="control-label">Supervisor</label>
		                            <div class="controls">
		                               <select name="super_username" id="super_username" class="m-wrap span12">';	

		            $supervisors = $account->getSupervisors();

		            $data_array['discount_modal'] .= $supervisors;

		            $data_array['discount_modal'] .= '</select>
	                            <span class="help-block hidden verify_supervisor_error"></span>
	                            </div>
	                          </div>
	                       </div>

	                       <div class="span6 ">
	                          <div class="control-group">
	                            <label class="control-label">Password</label>
	                            <div class="controls">
	                               <input type="password" name="super_password" id="super_password" class="m-wrap span12" />
	                            </div>
	                          </div>
	                       </div>
	                       
	                    </div>';
				}		                                

				$data_array['discount_modal'] .= '</div>	

							<div class="modal-footer">
								<input type="hidden" id="invoice_id" value="' . $data_array['invoice_id'] . '" />
								<input type="submit" id="submit" class="btn light-green right apply_discount_submit" value="Apply Discount" />
								<input type="button" data-dismiss="modal" class="btn" value="Cancel" />
							</div>


						</form>
					</div>';

    		}



	    	$query = $this->dbo->prepare("SELECT client_id, first_name, last_name, business_name, type FROM clients WHERE account_id = ?");
			$query->execute(array($account_id));

			$result = $query->fetchAll();

			foreach($result as $row) {

    			$client_id = $row[0];
    			$first_name = $row[1];
    			$last_name = $row[2];
    			$business_name = $row[3];
    			$type = $row[4];

    			if($type == 'individual') {
    				
    				$client_display_name = $first_name . ' ' . $last_name;

    			} else if($type == 'business') {

    				$client_display_name = $business_name;
    			}

    			if($client_ref_id == $client_id) {
    				
    				$data_array['clients_list'] .= '<option value="' . $client_id . '" selected>' . $client_display_name . '</option>';
    			
    			} else {

    				$data_array['clients_list'] .= '<option value="' . $client_id . '">' . $client_display_name . '</option>';
    			}

    		}



    		$query = $this->dbo->prepare("SELECT item_id, item_name, item_desc, item_cost FROM inventory WHERE account_id = ?");
			$query->execute(array($account_id));

			$result = $query->fetchAll();

			foreach($result as $row) {

    			$item_id = $row[0];
    			$item_name = $row[1];
    			$item_desc = $row[2];
    			$item_cost = $row[3]; 

    			$data_array['product_list'] .= '<option value="' . $item_id . '" class="' . $item_id . '" desc="' . $item_desc  . '" cost="' . $item_cost . '">' . $item_name . '</option>';
 
    		}


    		$count = 1;

    		$query = $this->dbo->prepare("SELECT item_id, item_qty FROM invoice_template_items WHERE account_id = ? AND invoice_id = ? ORDER BY id ASC");
			$query->execute(array($account_id, $invoice_id));

			$result = $query->fetchAll();

			foreach($result as $row) {

    			$item_id = $row[0];
    			$item_qty = $row[1];

    			$query2 = $this->dbo->prepare("SELECT item_name, item_desc, item_cost FROM inventory WHERE account_id = ? AND item_id = ?");
				$query2->execute(array($account_id, $item_id));

				$result2 = $query2->fetchAll();

				foreach($result2 as $row2) {

	    			$item_name = $row2[0];
	    			$item_desc = $row2[1];
	    			$item_cost = $row2[2]; 
	 
	    		}


	    		$data_array['items'] .= '<tr> 
										<td>' . $count . '</td>
										<td class="item_name">
											<div class="control-group">
												<div class="controls">
													<select id="" name="select_product" class="span12 select2 search_dropdown select_product">
														<option value="' . $item_id . '" class="' . $item_id . '" desc="' . $item_desc  . '" cost="' . $item_cost . '" selected="selected">' . $item_name . '</option>'
														. $data_array['product_list'] .
													'</select>
												</div> 
											</div>
										</td>
										<td class="hidden-480 item_desc">' . $item_desc . '</td>
										<td class="hidden-480 item_qty"><input type="text" name="quantity" class="span2 m-wrap small quantity" value="' . $item_qty . '"></td>
										<td class="hidden-480 item_cost"><input type="text" value="' . $item_cost . '"class="m-wrap small" readonly/></td>
										<td class="item_total"><input type="text" value="0.00"class="m-wrap small" readonly/></td>
										<td><a href="#" class="remove_item"><i class="icon-remove"></i></a></td>
									</tr>';
				$count++;
    		}


    		if($data_array['items'] == '') {  //If there are no items for invoice... add blank row


    			$data_array['items'] = '<tr> 
										<td>1</td>
										<td class="item_name">
											<div class="control-group">
												<div class="controls">
													<select id="" name="select_product" class="span12 select2 search_dropdown select_product">
														<option value=""></option>' .
														$data_array['product_list'] .
													'</select>
												</div> 
											</div>
										</td>
										<td class="hidden-480 item_desc"></td>
										<td class="hidden-480 item_qty"><input type="text" name="quantity" class="span2 m-wrap small quantity" value="1"></td>
										<td class="hidden-480 item_cost"><input type="text" value="0.00"class="m-wrap small" readonly/></td>
										<td class="item_total"><input type="text" value="0.00"class="m-wrap small" readonly/></td>
										<td></td>
									</tr>';

    		}


    		return $data_array;

	    }



	    public function saveInvoiceDraft() {

	    	$account_id = $_SESSION['account_id'];
	    	$user_id = $_SESSION['user_id'];

        	$invoice_id = $_POST['invoice_id'];
        	$client_id = $_POST['client_id'];
        	$payment_type = $_POST['payment_type'];
        	$invoice_date = $_POST['invoice_date'];
        	$payment_term = $_POST['payment_term'];
        	$due_date = $_POST['due_date'];
        	$additional_notes = $_POST['additional_notes'];
        	$items = json_decode($_POST['items']);

        	$datetime = date('Y-m-d H:i:s');

        	$invoice_date = date('Y-m-d H:i:s', strtotime($invoice_date));
        	$due_date = date('Y-m-d H:i:s', strtotime($due_date));


        	$query = $this->dbo->prepare("SELECT currency, tax_rate, calculate_tax FROM account_preferences WHERE account_id = ?");
			$query->execute(array($account_id));

			$result = $query->fetchAll();

			foreach($result as $row) {

				$currency = $row[0];
				$tax_rate = $row[1];
				$calculate_tax = $row[2];

			}


			$subtotal = 0;
			$total = 0;
        	$after_discount = 0;
        	$tax_amount = 0;


			//delete previous items from table
        	$query = $this->dbo->prepare("DELETE FROM invoice_items WHERE account_id = ? AND invoice_id = ?");
			$query->execute(array($account_id, $invoice_id));

        	foreach ($items as $key => $item) {

        		$item_id = $item[0];
        		$item_qty = $item[1];
        		//$item_cost = $item[2];


        		//Get the latest price of item
        		$query = $this->dbo->prepare("SELECT item_cost FROM inventory WHERE account_id = ? AND item_id = ?");
				$query->execute(array($account_id, $item_id));

				$result = $query->fetchAll();

				foreach($result as $row) {

					$item_cost = $row[0];
				}

				$item_total = $item_qty * $item_cost;

				$subtotal = $subtotal + $item_total;


				$query = $this->dbo->prepare("INSERT INTO invoice_items SET account_id = ?, invoice_id = ?, item_id = ?, item_qty = ?, item_cost = ?");
				$query->execute(array($account_id, $invoice_id, $item_id, $item_qty, $item_cost));

        	}


        	$query = $this->dbo->prepare("SELECT discount_amount, discount_type FROM invoices WHERE account_id = ? AND id = ? AND status = ?");
			$query->execute(array($account_id, $invoice_id, 'draft'));

			$result = $query->fetchAll();

			foreach($result as $row) {

				$discount_amount = $row[0];
				$discount_type = $row[1];

			}



        	if($discount_type == 'amount') {

        		$after_discount = $subtotal - $discount_amount;
        	
        	} else if($discount_type == 'percent') {

        		$after_discount = $subtotal * ((100 - $discount_amount) / 100);

        	} else {

        		//no discount
        		$after_discount = $subtotal;
        	}


        	if($calculate_tax == 1) {

	        	if($after_discount > 0) {

	        		$tax_amount = ($after_discount * ($tax_rate / 100));

	        	} else {

	        		$tax_amount = 0;
	        	}

	        }

        	$total = $after_discount + $tax_amount;



        	$query = $this->dbo->prepare("UPDATE invoices SET client_id = ?, date_modified = ?, modified_by = ?, notes = ?, status = ?, total = ?, payment_type = ?, invoice_date = ?, payment_term = ?, due_date = ?, currency = ?, calculate_tax = ?, tax_rate = ? WHERE account_id = ? AND id = ? AND status = ?");
			$query->execute(array($client_id, $datetime, $user_id, $additional_notes, 'draft', $total, $payment_type, $invoice_date, $payment_term, $due_date, $currency, $calculate_tax, $tax_rate, $account_id, $invoice_id, 'draft'));

        	return print_r($this->dbo->errorInfo());

	    }


	    public function saveRecurringInvoiceDraft() {

	    	$account_id = $_SESSION['account_id'];
	    	$user_id = $_SESSION['user_id'];

        	$invoice_id = $_POST['invoice_id'];
        	$client_id = $_POST['client_id'];
        	$payment_type = $_POST['payment_type'];
        	$payment_term = $_POST['payment_term'];
        	$additional_notes = $_POST['additional_notes'];
        	$items = json_decode($_POST['items']);
        	$process_first_invoice = $_POST['process_first_invoice'];

        	$recurring_interval = $_POST['recurring_interval'];
    		$recurring_daily_days = $_POST['recurring_daily_days'];
        	$recurring_weekly_weeks = $_POST['recurring_weekly_weeks'];
        	$recurring_weekly_weekday = $_POST['ecurring_weekly_weekday'];
        	$recurring_monthly_day = $_POST['recurring_monthly_day'];
        	$recurring_monthly_months = $_POST['recurring_monthly_months'];
        	$recurring_yearly_month = $_POST['recurring_yearly_month'];
        	$recurring_yearly_day = $_POST['recurring_yearly_day'];
        	$recurring_start_date = $_POST['recurring_start_date'];
        	$recurring_date_range = $_POST['recurring_date_range'];
        	$recurring_end_date = $_POST['recurring_end_date'];
        	$recurring_end_count = $_POST['recurring_end_count'];

        	$datetime = date('Y-m-d H:i:s');

        	$invoice_date = date('Y-m-d H:i:s', strtotime($invoice_date));
        	$due_date = date('Y-m-d H:i:s', strtotime($due_date));


        	$query = $this->dbo->prepare("SELECT currency, tax_rate, calculate_tax FROM account_preferences WHERE account_id = ?");
			$query->execute(array($account_id));

			$result = $query->fetchAll();

			foreach($result as $row) {

				$currency = $row[0];
				$tax_rate = $row[1];
				$calculate_tax = $row[2];

			}


			$subtotal = 0;
			$total = 0;
        	$after_discount = 0;
        	$tax_amount = 0;


			//delete previous items from table
        	$query = $this->dbo->prepare("DELETE FROM invoice_items WHERE account_id = ? AND invoice_id = ?");
			$query->execute(array($account_id, $invoice_id));

        	foreach ($items as $key => $item) {

        		$item_id = $item[0];
        		$item_qty = $item[1];
        		//$item_cost = $item[2];


        		//Get the latest price of item
        		$query = $this->dbo->prepare("SELECT item_cost FROM inventory WHERE account_id = ? AND item_id = ?");
				$query->execute(array($account_id, $item_id));

				$result = $query->fetchAll();

				foreach($result as $row) {

					$item_cost = $row[0];
				}

				$item_total = $item_qty * $item_cost;

				$subtotal = $subtotal + $item_total;


				$query = $this->dbo->prepare("INSERT INTO invoice_items SET account_id = ?, invoice_id = ?, item_id = ?, item_qty = ?, item_cost = ?");
				$query->execute(array($account_id, $invoice_id, $item_id, $item_qty, $item_cost));

        	}


        	$query = $this->dbo->prepare("SELECT discount_amount, discount_type FROM invoice_templates WHERE account_id = ? AND id = ? AND status = ?");
			$query->execute(array($account_id, $invoice_id, 'draft'));

			$result = $query->fetchAll();

			foreach($result as $row) {

				$discount_amount = $row[0];
				$discount_type = $row[1];

			}



        	if($discount_type == 'amount') {

        		$after_discount = $subtotal - $discount_amount;
        	
        	} else if($discount_type == 'percent') {

        		$after_discount = $subtotal * ((100 - $discount_amount) / 100);

        	} else {

        		//no discount
        		$after_discount = $subtotal;
        	}


        	if($calculate_tax == 1) {

	        	if($after_discount > 0) {

	        		$tax_amount = ($after_discount * ($tax_rate / 100));

	        	} else {

	        		$tax_amount = 0;
	        	}

	        }

        	$total = $after_discount + $tax_amount;

        	
        	$query = $this->dbo->prepare("UPDATE invoice_templates SET client_id = ?, date_modified = ?, modified_by = ?, notes = ?, status = ?, total = ?, payment_type = ?, invoice_date = ?, payment_term = ?, due_date = ?, currency = ?, calculate_tax = ?, tax_rate = ? WHERE account_id = ? AND id = ? AND status = ?");
			$query->execute(array($client_id, $datetime, $user_id, $additional_notes, 'draft', $total, $payment_type, $invoice_date, $payment_term, $due_date, $currency, $calculate_tax, $tax_rate, $account_id, $invoice_id, 'draft'));

        	//return print_r($this->dbo->errorInfo());

	    }



	    public function processInvoice() {

	    	$account_id = $_SESSION['account_id'];
	    	$user_id = $_SESSION['user_id'];

        	$invoice_id = $_POST['invoice_id'];
        	$client_id = $_POST['client_id'];
        	$payment_type = $_POST['payment_type'];
        	$invoice_date = $_POST['invoice_date'];
        	$payment_term = $_POST['payment_term'];
        	$due_date = $_POST['due_date'];
        	$additional_notes = $_POST['additional_notes'];
        	$items = json_decode($_POST['items']);
        	$email_invoice = $_POST['email_invoice'];

        	$datetime = date('Y-m-d H:i:s');

        	$invoice_date = date('Y-m-d H:i:s', strtotime($invoice_date));
        	$due_date = date('Y-m-d H:i:s', strtotime($due_date));


        	$query = $this->dbo->prepare("SELECT currency, tax_rate, calculate_tax FROM account_preferences WHERE account_id = ?");
			$query->execute(array($account_id));

			$result = $query->fetchAll();

			foreach($result as $row) {

				$currency = $row[0];
				$tax_rate = $row[1];
				$calculate_tax = $row[2];

			}


			$subtotal = 0;
			$total = 0;
        	$after_discount = 0;
        	$tax_amount = 0;


			//delete previous items from table
        	$query = $this->dbo->prepare("DELETE FROM invoice_items WHERE account_id = ? AND invoice_id = ?");
			$query->execute(array($account_id, $invoice_id));

        	foreach ($items as $key => $item) {

        		$item_id = $item[0];
        		$item_qty = $item[1];
        		//$item_cost = $item[2];


        		//Get the latest price of item
        		$query = $this->dbo->prepare("SELECT item_cost FROM inventory WHERE account_id = ? AND item_id = ?");
				$query->execute(array($account_id, $item_id));

				$result = $query->fetchAll();

				foreach($result as $row) {

					$item_cost = $row[0];
				}

				$item_total = $item_qty * $item_cost;

				$subtotal = $subtotal + $item_total;


				$query = $this->dbo->prepare("INSERT INTO invoice_items SET account_id = ?, invoice_id = ?, item_id = ?, item_qty = ?, item_cost = ?");
				$query->execute(array($account_id, $invoice_id, $item_id, $item_qty, $item_cost));

        	}


        	$query = $this->dbo->prepare("SELECT discount_amount, discount_type, access_token FROM invoices WHERE account_id = ? AND id = ? AND status = ?");
			$query->execute(array($account_id, $invoice_id, 'draft'));

			$result = $query->fetchAll();

			foreach($result as $row) {

				$discount_amount = $row[0];
				$discount_type = $row[1];
				$access_token = $row[2]; //need the access token in case the invoice is going to get emailed to client further down

			}



        	if($discount_type == 'amount') {

        		$after_discount = $subtotal - $discount_amount;
        	
        	} else if($discount_type == 'percent') {

        		$after_discount = $subtotal * ((100 - $discount_amount) / 100);

        	} else {

        		//no discount
        		$after_discount = $subtotal;
        	}


        	if($calculate_tax == 1) {

	        	if($after_discount > 0) {

	        		$tax_amount = ($after_discount * ($tax_rate / 100));

	        	} else {

	        		$tax_amount = 0;
	        	}

	        }

        	$total = $after_discount + $tax_amount;



        	$query = $this->dbo->prepare("UPDATE invoices SET client_id = ?, date_modified = ?, modified_by = ?, notes = ?, status = ?, date_processed = ?, total = ?, payment_type = ?, invoice_date = ?, payment_term = ?, due_date = ?, currency = ?, calculate_tax = ?, tax_rate = ? WHERE account_id = ? AND id = ? AND status = ?");
			$query->execute(array($client_id, $datetime, $user_id, $additional_notes, 'processed', $datetime, $total, $payment_type, $invoice_date, $payment_term, $due_date, $currency, $calculate_tax, $tax_rate, $account_id, $invoice_id, 'draft'));


			// =============================
        	// Email Invoice...

        	if($email_invoice == 1) {

				require_once('classes/class.phpmailer.php');


				$query = $this->dbo->prepare("SELECT first_name, last_name, business_name, type, email FROM clients WHERE account_id = ? AND client_id = ?");
				$query->execute(array($account_id, $client_id));

				$result = $query->fetchAll();

				foreach($result as $row) {

					$client_first_name = $row[0];
					$client_last_name = $row[1];
					$client_business_name = $row[2];
					$client_type = $row[3];
					$client_email = $row[4];
					$client_email = 'reza_karami@outlook.com';
				}


				if($client_type == 'individual') {

					$client_display_name = $first_name . ' ' . $last_name;
				
				} else if($client_type == 'business') {

					$client_display_name = $client_business_name;
				}


				$invoice_date =  date('d/m/Y', strtotime($invoice_date));
				$invoice_link = 'https://sunsetblvd.ironvault.ca/view_invoice?id=' . $invoice_id .'&t=' . $access_token;

				$business_name = $_SESSION['business_name'];

				$logo = $_SESSION['business_logo'];

				if($logo != '') {

					$business_logo = '<img src="' . $logo . '" border="0" style="max-height: 40px; max-width: 300px;vertical-align:middle;display:block;">';
				
				} else {

					$business_logo = '<span style="font-size:28px;line-height:36px;color:#434343;">' . $business_name . '</span>';

				}

				$emailHeader = '<table width="690" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff" align="center" style="max-width:690px;padding-top:0px;font-family:Arial, Helvetica, sans-serif;font-weight:normal;font-size:13px;line-height:21px;color:#434343;border-collapse:collapse;background-color:#ffffff;border:1px solid #dddddd;">
							        <tbody><tr>
							            <td colspan="3" width="100%">
							            <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#f9f9f9" style="padding-top:0px;border-collapse:collapse;font-family:Arial, Helvetica, sans-serif;font-weight:normal;font-size:13px;line-height:21px;color:#434343;background-color:#E6E6E6;border-bottom:1px solid #dddddd;">
							              <tbody><tr>
							                <td width="3%"></td>
							                <td width="94%" height="64">' . $business_logo . '</td>
							                <td width="3%"></td>
							              </tr>
							            </tbody></table></td>
							        </tr>
							        <tr> 
							          <td colspan="3" width="100%" height="30"></td>
							        </tr>';

				$emailFooter = '<tr>
							          <td colspan="3" width="100%" height="25"></td>
							        </tr>
							 
							      
							        <tr>
							          <td colspan="3" width="100%" height="25" style="border-collapse:collapse;border-bottom:1px solid #f1f1f1;"></td>
							        </tr>
							        <tr bgcolor="#E6E6E6" style="background-color:#E6E6E6;">

							            	<td width="3%" style="border-top:1px solid #dddddd;"></td>
							              <td width="94%" style="padding:20px 0 5px 0; border-top:1px solid #dddddd;"><p style="padding:0;font-family:Arial, Helvetica, sans-serif;font-weight:normal;font-size:10px;line-height:12px;color:#989898;">Powered by Iron Vault – www.ironvault.ca 
							              </td>
							              <td width="3%" style="border-top:1px solid #dddddd;"></td>
							        </tr>


							      </tbody>

							</table>';

				$mail = new PHPMailer;
				$mail->CharSet = 'UTF-8';
				$mail->isHTML(true);  
				$mail->WordWrap = 50;
				$mail->From = 'noreply@ironvault.ca';
				$mail->FromName = $business_name; 



				$mail->addAddress($client_email);

				$mail->Subject = $client_display_name . ' – Invoice (' . $invoice_date . ')';

				$mail->Body   .= $emailHeader;	

				$mail->Body    .=  '<tr>
								          <td width="3%"></td>
								          <td width="94%">
								            <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff" style="font-family:Arial, Helvetica, sans-serif;font-weight:normal;font-size:13px;line-height:21px;color:#434343;border-collapse:collapse;">
								                <tbody>
								                    <tr>
								                      <td width="60%" valign="top">

								                          <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff" style="font-family:Arial, Helvetica, sans-serif;font-weight:normal;font-size:13px;line-height:21px;color:#434343;border-collapse:collapse;">
								                                <tbody>

								                                  <tr>
								                                      <td width="100%" style="font-size:28px;line-height:36px;color:#434343;">Your Invoice is Ready</td>
								                                  </tr>
								    
								                                </tbody>
								                          </table>

								                      </td>
								                    </tr>
								                    <tr>
								                    	   <td colspan="2" width="100%" height="10" style="border-collapse:collapse;border-bottom:1px solid #f1f1f1;"></td>
								                    </tr>

								                </tbody>
								            </table>

								          </td>  
								          <td width="3%"></td>
								        </tr>'; 

				$mail->Body   .= '<tr>
								            <td colspan="3" width="100%" height="25"></td>
								        </tr>

								        <tr>
								        	 <td width="3%"></td>

								          	  <td width="94%">
								                  	<p style="padding:0;font-family:Arial, Helvetica, sans-serif;font-weight:normal;font-size:13px;line-height:21px;color:#434343;">

								                  	<strong>Dear ' . $client_display_name . ',</strong><br><br>Your invoice is ready. Please follow the link below to view and/or print your invoice.<br><br> 

								                  	<a href="' . $invoice_link . '" style="background-color: #7dabf5 !important; color: #FFF !important; border: 1px solid #619BF8; border-bottom: 2px solid #3E82F0; outline: none !important; border-radius: 3px !important; padding: 7px 14px; font-family: \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 14px; text-decoration: none;">View Invoice</a>

								                  	<br><br>Sincerely,<br><strong>' . $business_name . '</strong>


								                	</p>
								              </td>

								        	   <td width="3%"></td>
								        </tr>';

				$mail->Body   .= $emailFooter;


				$status = $mail->send();

				if ($status) { 

				   //return true;
				
				} else {

					//return false;
				}

        	}

        	return 'success';

	    }



	    public function viewInvoice() { 

	    	require_once("Account.class.php");
			$account = new Account();


	    	$account_id = $_SESSION['account_id'];
	    	$business_name = $_SESSION['business_name'];

	    	$invoice_id = $_GET['id'];

	    	$data_array = array();

	    	$query = $this->dbo->prepare("SELECT client_id, invoice_number, date_created, date_modified, created_by, modified_by, notes, status, date_processed, total, paid, invoice_date, due_date, payment_term, payment_status, payment_type, currency, discount_amount, discount_type, discount_approved_by, calculate_tax, tax_rate FROM invoices WHERE account_id = ? AND id = ?");
			$query->execute(array($account_id, $invoice_id));

			$result = $query->fetchAll();

			foreach($result as $row) {

    			$client_id = $row[0];
    			$invoice_number = $row[1];
    			$date_created = $row[2];
    			$date_modified = $row[3];
    			$created_by = $row[4];
    			$modified_by = $row[5];
    			$notes = nl2br($row[6]);
    			$status = $row[7];
    			$date_processed = $row[8];
    			$total = $row[9];
    			$paid = $row[10];
    			$invoice_date = $row[11];
    			$due_date = $row[12];
    			$payment_term = $row[13];
    			$payment_status = $row[14];
    			$payment_type = $row[15];
    			$currency = $row[16];
    			$discount_amount = $row[17];
    			$discount_type = $row[18];
    			$discount_approved_by = $row[19];
    			$calculate_tax = $row[20];
    			$tax_rate = (float)$row[21];

    		}


    		if($status == 'draft') {

    			header('Location: new_invoice?id=' . $invoice_id);
    		}




    		$query = $this->dbo->prepare("SELECT business_info_on_invoice, employee_info_on_invoice FROM account_preferences WHERE account_id = ?");
			$query->execute(array($account_id));

			$result = $query->fetchAll();

			foreach($result as $row) {

    			$business_info_on_invoice = $row[0];
    			$employee_info_on_invoice = $row[1];
 
    		}


    		if($business_info_on_invoice == 'show') {

    			$query = $this->dbo->prepare("SELECT business_name, primary_email, address, city, province, country, postal_code, logo, phone, fax FROM accounts WHERE account_id = ?");
				$query->execute(array($account_id));

				$result = $query->fetchAll();

				foreach($result as $row) {

					$business_name = $row[0];
					$business_email = $row[1];
	    			$business_address = $row[2];
	    			$business_city = $row[3];
	    			$business_province = $row[4];
	    			$business_country = $row[5];
	    			$business_postal_code = $row[6];
	    			$business_logo = $row[7];
	    			$business_phone = $row[8];
	    			$business_fax = $row[9];

	    			$query2 = $this->dbo->prepare("SELECT country_name FROM countries WHERE country_id = ?");
					$query2->execute(array($business_country));

					$result2 = $query2->fetchAll();

					foreach($result2 as $row2) {
						
						$business_country = $row2[0];
					}


	    			$business_phone = preg_replace("/^(\d{3})(\d{3})(\d{4})$/", "($1) $2-$3", $business_phone);
	    			$business_fax = preg_replace("/^(\d{3})(\d{3})(\d{4})$/", "($1) $2-$3", $business_fax);

	    		}

    			$data_array['business_info'] = '<h2>' . $business_name . '</h2><br>';
    			
    			$data_array['business_info'] .= '<table class="invoice_contact_info">';

				$business_full_address = "";

	    		if($business_address != "") {
	    			$business_full_address .= $business_address . "<br>";
	    		}

	    		if($business_city != "") {
	    			$business_full_address .= $business_city . ", ";
	    		}

	    		if($business_province != "") {
	    			$business_full_address .= $business_province . "<br>";
	    		}

	    		if($business_country != "") {
	    			$business_full_address .= $business_country . ", ";
	    		}

	    		if($business_postal_code != "") {
	    			$business_full_address .= $business_postal_code . ", ";
	    		}

	    		if($business_full_address != "") {

	    			$business_full_address = substr($business_full_address, 0, -2);

	    			$data_array['business_info'] .= '<tr><td>Address:</td><td>' . $business_full_address . '</td></tr>';
	    		}

    			
    			if($business_phone != '') {

    				$data_array['business_info'] .= '<tr><td>Phone:</td><td>' . $business_phone . '</td></tr>';
    			}

    			if($business_fax != '') {

    				$data_array['business_info'] .= '<tr><td>Fax:</td><td>' . $business_fax . '</td></tr>';
    			}

    			if($business_email != '') {

    				$data_array['business_info'] .= '<tr><td>Email:</td><td>' . $business_email . '</td></tr>';
    			}


    			$data_array['business_info'] .= '</table>';


    			if($business_logo != '') {

    				$data_array['business_logo'] = '<div class="span3 invoice-logo-space">
    												<img src="https://cdn.ironvault.ca/logo/' . $business_logo . '" alt="logo" class="business_logo left">
    												</div>';
    			}

    		}

			
    		if($employee_info_on_invoice == 'show') {

    			$query = $this->dbo->prepare("SELECT first_name, last_name, email, position, primary_phone, alternative_phone FROM users WHERE account_id = ? AND user_id = ?");
				$query->execute(array($account_id, $created_by));

				$result = $query->fetchAll();

				foreach($result as $row) {

					$emp_first_name = $row[0];
					$emp_last_name = $row[1];
					$emp_email = $row[2];
					$emp_position = $row[3];
	    			$emp_primary_phone = $row[4];
	    			$emp_alternative_phone = $row[5];


	    			$emp_primary_phone = preg_replace("/^(\d{3})(\d{3})(\d{4})$/", "($1) $2-$3", $emp_primary_phone);
	    			$emp_alternative_phone = preg_replace("/^(\d{3})(\d{3})(\d{4})$/", "($1) $2-$3", $emp_alternative_phone);

	    		}

    			$data_array['employee_info'] = '<h2>&nbsp;</h2><br>';

    			$data_array['employee_info'] .= '<table class="invoice_contact_info">';

    			$data_array['employee_info'] .= '<tr><td>Contact Person:</td><td>' . $emp_first_name . ' ' . $emp_last_name . '</td></tr>';
    				
    			if($emp_position != '') {

    				$data_array['employee_info'] .= '<tr><td>Position:</td><td>' . $emp_position . '</td></tr>';
    			}
    			
    			if($emp_primary_phone != '') {

    				$data_array['employee_info'] .= '<tr><td>Primary Phone:</td><td>' . $emp_primary_phone . '</td></tr>';
    			}

    			if($emp_alternative_phone != '') {

    				$data_array['employee_info'] .= '<tr><td>Alternative Phone:</td><td>' . $emp_alternative_phone . '</td></tr>';
    			}

    			if($emp_email != '') {

    				$data_array['employee_info'] .= '<tr><td>Email:</td><td>' . $emp_email . '</td></tr>';
    			}

    			$data_array['employee_info'] .= '</table>';

    		}



    		$query = $this->dbo->prepare("SELECT first_name, last_name, business_name, type, email, primary_phone, alternative_phone, fax, website, address FROM clients WHERE account_id = ? AND client_id = ?");
			$query->execute(array($account_id, $client_id));

			$result = $query->fetchAll();

			foreach($result as $row) {

    			$client_first_name = $row[0];
    			$client_last_name = $row[1];
    			$client_business_name = $row[2];
    			$client_type = $row[3];
    			$client_email = $row[4];
    			$client_primary_phone = $row[5];
    			$client_alternative_phone = $row[6];
    			$client_fax = $row[7];
    			$client_website = $row[8];
    			$client_address = $row[9];

    			$client_primary_phone = preg_replace("/^(\d{3})(\d{3})(\d{4})$/", "($1) $2-$3", $client_primary_phone);
	    		$client_alternative_phone = preg_replace("/^(\d{3})(\d{3})(\d{4})$/", "($1) $2-$3", $client_alternative_phone);
	    		$client_fax = preg_replace("/^(\d{3})(\d{3})(\d{4})$/", "($1) $2-$3", $client_fax);
    		}

    		if($client_type == 'individual') {

    			$client_display_name = $client_first_name . ' ' . $client_last_name;
    		
    		} else if($client_type == 'business') {

    			$client_display_name = $client_business_name;
    		}

    		$data_array['client_info'] = '<h2>' . $client_display_name . '</h2><br>';

    		$data_array['client_info'] .= '<table class="invoice_contact_info">';

    		if($client_address != '') {

    			$data_array['client_info'] .= '<tr><td>Address:</td><td>' . $client_address . '</td></tr>';
    		}

    		if($client_primary_phone != '') {

    			$data_array['client_info'] .= '<tr><td>Primary Phone:</td><td>' . $client_primary_phone . '</td></tr>';
    		}

    		if($client_email != '') {

    			$data_array['client_info'] .= '<tr><td>Email:</td><td>' . $client_email . '</td></tr>';
    		}

    		$data_array['client_info'] .= '</table>';


    		$data_array['client_additional_info'] = '<h4>&nbsp;</h4><h2>&nbsp;</h2><br>';

    		$data_array['client_additional_info'] .= '<table class="invoice_contact_info">';

    		if($client_alternative_phone != '') {

    			$data_array['client_additional_info'] .= '<tr><td>Alternative Phone:</td><td>' . $client_alternative_phone . '</td></tr>';
    		}

    		if($client_fax != '') {

    			$data_array['client_additional_info'] .= '<tr><td>Fax:</td><td>' . $client_fax . '</td></tr>';
    		}

    		if($client_website != '') {

    			$data_array['client_additional_info'] .= '<tr><td>Website:</td><td>' . $client_website . '</td></tr>';
    		}

    		$data_array['client_additional_info'] .= '</table>';


    		$query = $this->dbo->prepare("SELECT user_id, first_name, last_name FROM users WHERE account_id = ? AND user_id = ?");
			$query->execute(array($account_id, $created_by));

			$result = $query->fetchAll();

			foreach($result as $row) {

    			$created_by_id = $row[0];
    			$created_by_first_name = $row[1];
    			$created_by_last_name = $row[2];
    		}


    		$query = $this->dbo->prepare("SELECT user_id, first_name, last_name FROM users WHERE account_id = ? AND user_id = ?");
			$query->execute(array($account_id, $modified_by));

			$result = $query->fetchAll();

			foreach($result as $row) {

    			$modified_by_id = $row[0];
    			$modified_by_first_name = $row[1];
    			$modified_by_last_name = $row[2];
    		}



    		$data_array['invoice_number'] = $invoice_number;

    		$data_array['payment_type'] = ucwords(str_replace('_', ' ', $payment_type));

    		$data_array['invoice_date'] = date('F j, Y', strtotime($invoice_date));

    		if($payment_term > 0) {

    			$data_array['payment_term'] = $payment_term . ' Days';
    		
    		} else {

    			$data_array['payment_term'] = 'Due on receipt';
    		}

    		$data_array['due_date'] = date('F j, Y', strtotime($due_date));

    		$data_array['notes'] = '<p>' . $notes . '</p>';



    		//Calculate invoice totals
    		$subtotal = 0;
			$total = 0;
        	$after_discount = 0;
        	$tax_amount = 0;


    		$count = 1;

    		$query = $this->dbo->prepare("SELECT item_id, item_qty, item_cost FROM invoice_items WHERE account_id = ? AND invoice_id = ?");
			$query->execute(array($account_id, $invoice_id));

			$result = $query->fetchAll();

			foreach($result as $row) {

    			$item_id = $row[0];
    			$item_qty = $row[1]; 
    			$item_cost = $row[2]; 

    			$item_total = $item_cost * $item_qty;

    			//Invoice subtotal
    			$subtotal = $subtotal + $item_total;

    			$query2 = $this->dbo->prepare("SELECT item_name, item_desc FROM inventory WHERE account_id = ? AND item_id = ?");
				$query2->execute(array($account_id, $item_id));

				$result2 = $query2->fetchAll();

				foreach($result2 as $row2) {

					$item_name = $row2[0];
					$item_desc = $row2[1];
				}

    			$invoice_items .= '<tr>
									<td id="count">' . $count . '</td>
									<td>' . $item_name . '</td>
									<td>' . $item_desc . '</td>
									<td>' . $item_qty . '</td>
									<td>' . $item_cost . '</td>
									<td>' . money_format('%i', $item_total) . '</td>
								</tr>';
 
 				$count++;
    		}

    		if($invoice_items == '') {

    			$invoice_items = '<tr>
									<td colspan="6" class="center">Invoice contains no products or services.</td>
								</tr>';
    		}

    		$data_array['invoice_items'] = $invoice_items;


    		$data_array['invoice_totals'] = '<ul class="unstyled amounts">
												<li>Subtotal: $<b><span class="subtotal">' . money_format('%i', $subtotal) . '</span></b></li>';


        	if($discount_type == 'amount') {

        		$after_discount = $subtotal - $discount_amount;

        		$data_array['invoice_totals'] .= '<li id="discount">Discount: $<strong>–' . money_format('%i', $discount_amount) . '</strong></li>';
												
        	
        	} else if($discount_type == 'percent') {

        		$after_discount = $subtotal * ((100 - $discount_amount) / 100);

        		$data_array['invoice_totals'] .= '<li id="discount">Discount: <strong>' . $discount_amount . '%</strong></li>';

        	} else {

        		//no discount
        		$after_discount = $subtotal;
        	}

        	

        	if($calculate_tax == 1) {

	        	if($after_discount > 0) {

	        		$tax_amount = ($after_discount * ($tax_rate / 100));

	        	} else {

	        		$tax_amount = 0;
	        	}

	        	$data_array['invoice_totals'] .= '<li>Tax (' . $tax_rate . '%): $<b><span class="tax">' . money_format('%i', $tax_amount) . '</span></b></li>';																						

	        }

        	$total = $after_discount + $tax_amount;

        	$data_array['invoice_totals'] .= '<li>Total: $<b><span class="total">' . money_format('%i', $total) . '</span></b></li>
											</ul>';

			$balance_due = money_format('%i', $total - $paid);

			$amount_received;

			$data_array['payment_status'] = '<h2>Balance: $' . $balance_due . '</h2><br>
											<table class="invoice_contact_info">
											<tr><td>Payment Status:</td><td>Partial</td></tr>
											<tr><td>Amount Received:</td><td>$200.00</td></tr>
											</table>';
											

			

    		return $data_array;

	    }



	    public function viewInvoiceByToken() { 

	    	require_once("Account.class.php");
			$account = new Account();

	    	$business_name = $_SESSION['business_name'];

	    	$invoice_id = $_GET['id'];
	    	$access_token = $_GET['t'];

	    	$data_array = array();

	    	$found = false;

	    	$query = $this->dbo->prepare("SELECT client_id, invoice_number, date_created, date_modified, created_by, modified_by, notes, status, date_processed, total, paid, invoice_date, due_date, payment_term, payment_status, payment_type, currency, discount_amount, discount_type, discount_approved_by, calculate_tax, tax_rate, account_id FROM invoices WHERE id = ? AND access_token = ?");
			$query->execute(array($invoice_id, $access_token));

			$result = $query->fetchAll();

			foreach($result as $row) {

				$found = true;

    			$client_id = $row[0];
    			$invoice_number = $row[1];
    			$date_created = $row[2];
    			$date_modified = $row[3];
    			$created_by = $row[4];
    			$modified_by = $row[5];
    			$notes = nl2br($row[6]);
    			$status = $row[7];
    			$date_processed = $row[8];
    			$total = $row[9];
    			$paid = $row[10];
    			$invoice_date = $row[11];
    			$due_date = $row[12];
    			$payment_term = $row[13];
    			$payment_status = $row[14];
    			$payment_type = $row[15];
    			$currency = $row[16];
    			$discount_amount = $row[17];
    			$discount_type = $row[18];
    			$discount_approved_by = $row[19];
    			$calculate_tax = $row[20];
    			$tax_rate = (float)$row[21];
    			$account_id = $row[22];

    		}


    		if($found == false) {

    			//Invoice was not found using the given ID and Access Token... navigate away!
    			//header('Location: ');
    			echo 'lol';
    			exit;
    		}



    		$query = $this->dbo->prepare("SELECT business_info_on_invoice, employee_info_on_invoice FROM account_preferences WHERE account_id = ?");
			$query->execute(array($account_id));

			$result = $query->fetchAll();

			foreach($result as $row) {

    			$business_info_on_invoice = $row[0];
    			$employee_info_on_invoice = $row[1];
 
    		}


    		if($business_info_on_invoice == 'show') {

    			$query = $this->dbo->prepare("SELECT business_name, primary_email, address, city, province, country, postal_code, logo, phone, fax FROM accounts WHERE account_id = ?");
				$query->execute(array($account_id));

				$result = $query->fetchAll();

				foreach($result as $row) {

					$business_name = $row[0];
					$business_email = $row[1];
	    			$business_address = $row[2];
	    			$business_city = $row[3];
	    			$business_province = $row[4];
	    			$business_country = $row[5];
	    			$business_postal_code = $row[6];
	    			$business_logo = $row[7];
	    			$business_phone = $row[8];
	    			$business_fax = $row[9];

	    			$query2 = $this->dbo->prepare("SELECT country_name FROM countries WHERE country_id = ?");
					$query2->execute(array($business_country));

					$result2 = $query2->fetchAll();

					foreach($result2 as $row2) {
						
						$business_country = $row2[0];
					}


	    			$business_phone = preg_replace("/^(\d{3})(\d{3})(\d{4})$/", "($1) $2-$3", $business_phone);
	    			$business_fax = preg_replace("/^(\d{3})(\d{3})(\d{4})$/", "($1) $2-$3", $business_fax);

	    		}

    			$data_array['business_info'] = '<h2>' . $business_name . '</h2><br>';
    			
    			$data_array['business_info'] .= '<table class="invoice_contact_info">';

				$business_full_address = "";

	    		if($business_address != "") {
	    			$business_full_address .= $business_address . "<br>";
	    		}

	    		if($business_city != "") {
	    			$business_full_address .= $business_city . ", ";
	    		}

	    		if($business_province != "") {
	    			$business_full_address .= $business_province . "<br>";
	    		}

	    		if($business_country != "") {
	    			$business_full_address .= $business_country . ", ";
	    		}

	    		if($business_postal_code != "") {
	    			$business_full_address .= $business_postal_code . ", ";
	    		}

	    		if($business_full_address != "") {

	    			$business_full_address = substr($business_full_address, 0, -2);

	    			$data_array['business_info'] .= '<tr><td>Address:</td><td>' . $business_full_address . '</td></tr>';
	    		}

    			
    			if($business_phone != '') {

    				$data_array['business_info'] .= '<tr><td>Phone:</td><td>' . $business_phone . '</td></tr>';
    			}

    			if($business_fax != '') {

    				$data_array['business_info'] .= '<tr><td>Fax:</td><td>' . $business_fax . '</td></tr>';
    			}

    			if($business_email != '') {

    				$data_array['business_info'] .= '<tr><td>Email:</td><td>' . $business_email . '</td></tr>';
    			}


    			$data_array['business_info'] .= '</table>';


    			if($business_logo != '') {

    				$data_array['business_logo'] = '<div class="span3 invoice-logo-space">
    												<img src="https://cdn.ironvault.ca/logo/' . $business_logo . '" alt="logo" class="business_logo left">
    												</div>';
    			}

    		}

			
    		if($employee_info_on_invoice == 'show') {

    			$query = $this->dbo->prepare("SELECT first_name, last_name, email, position, primary_phone, alternative_phone FROM users WHERE account_id = ? AND user_id = ?");
				$query->execute(array($account_id, $created_by));

				$result = $query->fetchAll();

				foreach($result as $row) {

					$emp_first_name = $row[0];
					$emp_last_name = $row[1];
					$emp_email = $row[2];
					$emp_position = $row[3];
	    			$emp_primary_phone = $row[4];
	    			$emp_alternative_phone = $row[5];


	    			$emp_primary_phone = preg_replace("/^(\d{3})(\d{3})(\d{4})$/", "($1) $2-$3", $emp_primary_phone);
	    			$emp_alternative_phone = preg_replace("/^(\d{3})(\d{3})(\d{4})$/", "($1) $2-$3", $emp_alternative_phone);

	    		}

    			$data_array['employee_info'] = '<h2>&nbsp;</h2><br>';

    			$data_array['employee_info'] .= '<table class="invoice_contact_info">';

    			$data_array['employee_info'] .= '<tr><td>Contact Person:</td><td>' . $emp_first_name . ' ' . $emp_last_name . '</td></tr>';
    				
    			if($emp_position != '') {

    				$data_array['employee_info'] .= '<tr><td>Position:</td><td>' . $emp_position . '</td></tr>';
    			}
    			
    			if($emp_primary_phone != '') {

    				$data_array['employee_info'] .= '<tr><td>Primary Phone:</td><td>' . $emp_primary_phone . '</td></tr>';
    			}

    			if($emp_alternative_phone != '') {

    				$data_array['employee_info'] .= '<tr><td>Alternative Phone:</td><td>' . $emp_alternative_phone . '</td></tr>';
    			}

    			if($emp_email != '') {

    				$data_array['employee_info'] .= '<tr><td>Email:</td><td>' . $emp_email . '</td></tr>';
    			}

    			$data_array['employee_info'] .= '</table>';

    		}



    		$query = $this->dbo->prepare("SELECT first_name, last_name, business_name, type, email, primary_phone, alternative_phone, fax, website, address FROM clients WHERE account_id = ? AND client_id = ?");
			$query->execute(array($account_id, $client_id));

			$result = $query->fetchAll();

			foreach($result as $row) {

    			$client_first_name = $row[0];
    			$client_last_name = $row[1];
    			$client_business_name = $row[2];
    			$client_type = $row[3];
    			$client_email = $row[4];
    			$client_primary_phone = $row[5];
    			$client_alternative_phone = $row[6];
    			$client_fax = $row[7];
    			$client_website = $row[8];
    			$client_address = $row[9];

    			$client_primary_phone = preg_replace("/^(\d{3})(\d{3})(\d{4})$/", "($1) $2-$3", $client_primary_phone);
	    		$client_alternative_phone = preg_replace("/^(\d{3})(\d{3})(\d{4})$/", "($1) $2-$3", $client_alternative_phone);
	    		$client_fax = preg_replace("/^(\d{3})(\d{3})(\d{4})$/", "($1) $2-$3", $client_fax);
    		}

    		if($client_type == 'individual') {

    			$client_display_name = $client_first_name . ' ' . $client_last_name;
    		
    		} else if($client_type == 'business') {

    			$client_display_name = $client_business_name;
    		}

    		$data_array['client_info'] = '<h2>' . $client_display_name . '</h2><br>';

    		$data_array['client_info'] .= '<table class="invoice_contact_info">';

    		if($client_address != '') {

    			$data_array['client_info'] .= '<tr><td>Address:</td><td>' . $client_address . '</td></tr>';
    		}

    		if($client_primary_phone != '') {

    			$data_array['client_info'] .= '<tr><td>Primary Phone:</td><td>' . $client_primary_phone . '</td></tr>';
    		}

    		if($client_email != '') {

    			$data_array['client_info'] .= '<tr><td>Email:</td><td>' . $client_email . '</td></tr>';
    		}

    		$data_array['client_info'] .= '</table>';


    		$data_array['client_additional_info'] = '<h4>&nbsp;</h4><h2>&nbsp;</h2><br>';

    		$data_array['client_additional_info'] .= '<table class="invoice_contact_info">';

    		if($client_alternative_phone != '') {

    			$data_array['client_additional_info'] .= '<tr><td>Alternative Phone:</td><td>' . $client_alternative_phone . '</td></tr>';
    		}

    		if($client_fax != '') {

    			$data_array['client_additional_info'] .= '<tr><td>Fax:</td><td>' . $client_fax . '</td></tr>';
    		}

    		if($client_website != '') {

    			$data_array['client_additional_info'] .= '<tr><td>Website:</td><td>' . $client_website . '</td></tr>';
    		}

    		$data_array['client_additional_info'] .= '</table>';


    		$query = $this->dbo->prepare("SELECT user_id, first_name, last_name FROM users WHERE account_id = ? AND user_id = ?");
			$query->execute(array($account_id, $created_by));

			$result = $query->fetchAll();

			foreach($result as $row) {

    			$created_by_id = $row[0];
    			$created_by_first_name = $row[1];
    			$created_by_last_name = $row[2];
    		}


    		$query = $this->dbo->prepare("SELECT user_id, first_name, last_name FROM users WHERE account_id = ? AND user_id = ?");
			$query->execute(array($account_id, $modified_by));

			$result = $query->fetchAll();

			foreach($result as $row) {

    			$modified_by_id = $row[0];
    			$modified_by_first_name = $row[1];
    			$modified_by_last_name = $row[2];
    		}



    		$data_array['invoice_number'] = $invoice_number;

    		$data_array['payment_type'] = ucwords(str_replace('_', ' ', $payment_type));

    		$data_array['invoice_date'] = date('F j, Y', strtotime($invoice_date));

    		if($payment_term > 0) {

    			$data_array['payment_term'] = $payment_term . ' Days';
    		
    		} else {

    			$data_array['payment_term'] = 'Due on receipt';
    		}

    		$data_array['due_date'] = date('F j, Y', strtotime($due_date));

    		$data_array['notes'] = '<p>' . $notes . '</p>';



    		//Calculate invoice totals
    		$subtotal = 0;
			$total = 0;
        	$after_discount = 0;
        	$tax_amount = 0;


    		$count = 1;

    		$query = $this->dbo->prepare("SELECT item_id, item_qty, item_cost FROM invoice_items WHERE account_id = ? AND invoice_id = ?");
			$query->execute(array($account_id, $invoice_id));

			$result = $query->fetchAll();

			foreach($result as $row) {

    			$item_id = $row[0];
    			$item_qty = $row[1]; 
    			$item_cost = $row[2]; 

    			$item_total = $item_cost * $item_qty;

    			//Invoice subtotal
    			$subtotal = $subtotal + $item_total;

    			$query2 = $this->dbo->prepare("SELECT item_name, item_desc FROM inventory WHERE account_id = ? AND item_id = ?");
				$query2->execute(array($account_id, $item_id));

				$result2 = $query2->fetchAll();

				foreach($result2 as $row2) {

					$item_name = $row2[0];
					$item_desc = $row2[1];
				}

    			$invoice_items .= '<tr>
									<td id="count">' . $count . '</td>
									<td>' . $item_name . '</td>
									<td>' . $item_desc . '</td>
									<td>' . $item_qty . '</td>
									<td>' . $item_cost . '</td>
									<td>' . money_format('%i', $item_total) . '</td>
								</tr>';
 
 				$count++;
    		}

    		if($invoice_items == '') {

    			$invoice_items = '<tr>
									<td colspan="6" class="center">Invoice contains no products or services.</td>
								</tr>';
    		}

    		$data_array['invoice_items'] = $invoice_items;


    		$data_array['invoice_totals'] = '<ul class="unstyled amounts">
												<li>Subtotal: $<b><span class="subtotal">' . money_format('%i', $subtotal) . '</span></b></li>';


        	if($discount_type == 'amount') {

        		$after_discount = $subtotal - $discount_amount;

        		$data_array['invoice_totals'] .= '<li id="discount">Discount: $<strong>–' . money_format('%i', $discount_amount) . '</strong></li>';
												
        	
        	} else if($discount_type == 'percent') {

        		$after_discount = $subtotal * ((100 - $discount_amount) / 100);

        		$data_array['invoice_totals'] .= '<li id="discount">Discount: <strong>' . $discount_amount . '%</strong></li>';

        	} else {

        		//no discount
        		$after_discount = $subtotal;
        	}

        	

        	if($calculate_tax == 1) {

	        	if($after_discount > 0) {

	        		$tax_amount = ($after_discount * ($tax_rate / 100));

	        	} else {

	        		$tax_amount = 0;
	        	}

	        	$data_array['invoice_totals'] .= '<li>Tax (' . $tax_rate . '%): $<b><span class="tax">' . money_format('%i', $tax_amount) . '</span></b></li>';																						

	        }

        	$total = $after_discount + $tax_amount;

        	$data_array['invoice_totals'] .= '<li>Total: $<b><span class="total">' . money_format('%i', $total) . '</span></b></li>
											</ul>';

			$balance_due = money_format('%i', $total - $paid);

			$amount_received;

			$data_array['payment_status'] = '<h2>Balance: $' . $balance_due . '</h2><br>
											<table class="invoice_contact_info">
											<tr><td>Payment Status:</td><td>Partial</td></tr>
											<tr><td>Amount Received:</td><td>$200.00</td></tr>
											</table>';
											

			

    		return $data_array;

	    }



	    public function viewRecurringInvoice() {

	    	require_once("Account.class.php");
			$account = new Account();


	    	$account_id = $_SESSION['account_id'];
	    	$business_name = $_SESSION['business_name'];

	    	$invoice_id = $_GET['id'];

	    	$data_array = array();

	    	$query = $this->dbo->prepare("SELECT client_id, invoice_number, date_created, date_modified, created_by, modified_by, notes, status, date_processed, total, paid, invoice_date, due_date, payment_term, payment_status, payment_type, currency, discount_amount, discount_type, discount_approved_by, calculate_tax, tax_rate FROM invoices WHERE account_id = ? AND id = ?");
			$query->execute(array($account_id, $invoice_id));

			$result = $query->fetchAll();

			foreach($result as $row) {

    			$client_id = $row[0];
    			$invoice_number = $row[1];
    			$date_created = $row[2];
    			$date_modified = $row[3];
    			$created_by = $row[4];
    			$modified_by = $row[5];
    			$notes = $row[6];
    			$status = $row[7];
    			$date_processed = $row[8];
    			$total = $row[9];
    			$paid = $row[10];
    			$invoice_date = $row[11];
    			$due_date = $row[12];
    			$payment_term = $row[13];
    			$payment_status = $row[14];
    			$payment_type = $row[15];
    			$currency = $row[16];
    			$discount_amount = $row[17];
    			$discount_type = $row[18];
    			$discount_approved_by = $row[19];
    			$calculate_tax = $row[20];
    			$tax_rate = $row[21];

    		}


    		if($status == 'draft') {

    			header('Location: new_invoice?id=' . $invoice_id);
    		}


    		$query = $this->dbo->prepare("SELECT client_id, first_name, last_name FROM clients WHERE account_id = ? AND client_id = ?");
			$query->execute(array($account_id, $client_id));

			$result = $query->fetchAll();

			foreach($result as $row) {

    			$client_id = $row[0];
    			$client_first_name = $row[1];
    			$client_last_name = $row[2];
    		}


    		$query = $this->dbo->prepare("SELECT user_id, first_name, last_name FROM users WHERE account_id = ? AND user_id = ?");
			$query->execute(array($account_id, $created_by));

			$result = $query->fetchAll();

			foreach($result as $row) {

    			$created_by_id = $row[0];
    			$created_by_first_name = $row[1];
    			$created_by_last_name = $row[2];
    		}


    		$query = $this->dbo->prepare("SELECT user_id, first_name, last_name FROM users WHERE account_id = ? AND user_id = ?");
			$query->execute(array($account_id, $modified_by));

			$result = $query->fetchAll();

			foreach($result as $row) {

    			$modified_by_id = $row[0];
    			$modified_by_first_name = $row[1];
    			$modified_by_last_name = $row[2];
    		}


    		$query = $this->dbo->prepare("SELECT item_id, item_name, item_desc, item_qty, item_cost FROM invoice_items WHERE account_id = ? AND invoice_id = ?");
			$query->execute(array($account_id, $invoice_id));

			$result = $query->fetchAll();

			foreach($result as $row) {

    			$item_id = $row[0];
    			$item_name = $row[1];
    			$item_desc = $row[2];
    			$item_qty = $row[3]; 
    			$item_cost = $row[4]; 

    			$invoice_items .= '<tr>
									<th id="count">' . $count . '</th>
									<th id="">' . $item_name . '</th>
									<th class="hidden-480">' . $item_desc . '</th>
									<th class="hidden-480">' . $item_qty . '</th>
									<th class="hidden-480">' . $item_cost . '</th>
									<th></th>
								</tr>';
 
 				$count++;
    		}


    		$query = $this->dbo->prepare("SELECT currency, tax_rate, discounts, business_info_on_invoice, calculate_tax, employee_info_on_invoice FROM account_preferences WHERE account_id = ?");
			$query->execute(array($account_id));

			$result = $query->fetchAll();

			foreach($result as $row) {

    			$currency = $row[0];
    			$tax_rate = $row[1];
    			$discounts = $row[2];
    			$business_info_on_invoice = $row[3];
    			$calculate_tax = $row[4];
    			$employee_info_on_invoice = $row[5];
 
    		}


    		$data_array['preferences']['currency'] = $account->getCurrency($currency);


    		//////

    		$data_array['preferences']['tax_rate'] = (float)$tax_rate;
    		$data_array['preferences']['calculate_tax'] = $calculate_tax;

    		/////

			
			$data_array['preferences']['discounts'] = $discounts; 

			$data_array['preferences']['business_info_on_invoice'] = $business_info_on_invoice;   

			$data_array['preferences']['employee_info_on_invoice'] = $employee_info_on_invoice;


			//////


    		return $data_array;

	    }



	    public function deleteInvoice() {

	    	$account_id = $_SESSION['account_id'];

	    	$invoice_id = $_POST['invoice_id'];


	    	$query = $this->dbo->prepare("DELETE FROM invoices WHERE account_id = ? AND id = ?");
			$query->execute(array($account_id, $invoice_id));


			$query = $this->dbo->prepare("DELETE FROM invoice_items WHERE account_id = ? AND invoice_id = ?");
			$query->execute(array($account_id, $invoice_id));


	    	require_once('scripts/aws.phar');
			
			$s3client = Aws\S3\S3Client::factory(array(
		    	'key'    => 'AKIAJKBMGCMLYBHN77PQ',
		    	'secret' => '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'
		    ));


			$response = $s3client->getIterator('ListObjects', array(
			    'Bucket' => 'ironvault',
			    'Prefix' => 'invoices/' . $account_id . '/' . $invoice_id
			));

			foreach($response as $object) {
			    				    	
		    	$s3client->deleteObject(array(
				    'Bucket' => 'ironvault',
					'Key' => $object['Key']
				));

			}

	    }



	    public function deleteRecurringInvoice() {

	    	$account_id = $_SESSION['account_id'];

	    	$invoice_id = $_POST['invoice_id'];


	    	$query = $this->dbo->prepare("DELETE FROM invoice_templates WHERE account_id = ? AND id = ?");
			$query->execute(array($account_id, $invoice_id));


			$query = $this->dbo->prepare("DELETE FROM invoice_template_items WHERE account_id = ? AND invoice_id = ?");
			$query->execute(array($account_id, $invoice_id));


	    	require_once('scripts/aws.phar');
			
			$s3client = Aws\S3\S3Client::factory(array(
		    	'key'    => 'AKIAJKBMGCMLYBHN77PQ',
		    	'secret' => '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'
		    ));


			$response = $s3client->getIterator('ListObjects', array(
			    'Bucket' => 'ironvault',
			    'Prefix' => 'recurring_invoices/' . $account_id . '/' . $invoice_id
			));

			foreach($response as $object) {
			    				    	
		    	$s3client->deleteObject(array(
				    'Bucket' => 'ironvault',
					'Key' => $object['Key']
				));

			}

	    }



	    public function applyInvoiceDiscount() {

	    	$account_id = $_SESSION['account_id'];
	    	$account_type = $_SESSION['account_type'];

	    	$invoice_id = $_POST['invoice_id'];
	    	$discount_amount = $_POST['discount_amount'];
	    	$discount_type = $_POST['discount_type'];


	    	$query = $this->dbo->prepare("SELECT discounts FROM account_preferences WHERE account_id = ?");
			$query->execute(array($account_id));

			$result = $query->fetchAll();

			foreach($result as $row) {

    			$discounts = $row[0];
 
    		}

    		if($discounts == "supervisor" && $account_type == 3) {

		    	$username = $_POST['username'];
		    	$password = $_POST['password'];

		    	$hash_password = hash('SHA512', $password);

		    	$query = $this->dbo->prepare("SELECT user_id FROM users WHERE account_id = ? AND username = ? AND password = ? AND type = ? AND status = ? LIMIT 1");
				$query->execute(array($account_id, $username, $hash_password, 1, 'active'));

				$result = $query->fetchAll();

				foreach($result as $row) {

					$user_id = $row[0];
				}

				if($user_id > 0) {

					$query = $this->dbo->prepare("UPDATE invoices SET discount_amount = ?, discount_type = ?, discount_approved_by = ? WHERE account_id = ? AND id = ? AND status = ?");
					$query->execute(array($discount_amount, $discount_type, $user_id, $account_id, $invoice_id, 'draft'));

					return 'success';

				} else {

					return "failed";
				}

			} else {

				$query = $this->dbo->prepare("UPDATE invoices SET discount_amount = ?, discount_type = ?, discount_approved_by = ? WHERE account_id = ? AND id = ? AND status = ?");
				$query->execute(array($discount_amount, $discount_type, '', $account_id, $invoice_id, 'draft'));

				return 'success';

			}

	    }



	    public function removeInvoiceDiscount() {

	    	$account_id = $_SESSION['account_id'];

	    	$invoice_id = $_POST['invoice_id'];

			$query = $this->dbo->prepare("UPDATE invoices SET discount_amount = ?, discount_type = ?, discount_approved_by = ? WHERE account_id = ? AND id = ? AND status = ?");
			$query->execute(array(0, '', 0, $account_id, $invoice_id, 'draft'));

			return 'success';

	    }



	    public function applyRecurringInvoiceDiscount() {

	    	$account_id = $_SESSION['account_id'];
	    	$account_type = $_SESSION['account_type'];

	    	$invoice_id = $_POST['invoice_id'];
	    	$discount_amount = $_POST['discount_amount'];
	    	$discount_type = $_POST['discount_type'];


	    	$query = $this->dbo->prepare("SELECT discounts FROM account_preferences WHERE account_id = ?");
			$query->execute(array($account_id));

			$result = $query->fetchAll();

			foreach($result as $row) {

    			$discounts = $row[0];
 
    		}

    		if($discounts == "supervisor" && $account_type == 3) {

		    	$username = $_POST['username'];
		    	$password = $_POST['password'];

		    	$hash_password = hash('SHA512', $password);

		    	$query = $this->dbo->prepare("SELECT user_id FROM users WHERE account_id = ? AND username = ? AND password = ? AND type = ? AND status = ? LIMIT 1");
				$query->execute(array($account_id, $username, $hash_password, 1, 'active'));

				$result = $query->fetchAll();

				foreach($result as $row) {

					$user_id = $row[0];
				}

				if($user_id > 0) {

					$query = $this->dbo->prepare("UPDATE invoice_templates SET discount_amount = ?, discount_type = ?, discount_approved_by = ? WHERE account_id = ? AND id = ? AND status = ?");
					$query->execute(array($discount_amount, $discount_type, $user_id, $account_id, $invoice_id, 'draft'));

					return 'success';

				} else {

					return "failed";
				}

			} else {

				$query = $this->dbo->prepare("UPDATE invoice_templates SET discount_amount = ?, discount_type = ?, discount_approved_by = ? WHERE account_id = ? AND id = ? AND status = ?");
				$query->execute(array($discount_amount, $discount_type, '', $account_id, $invoice_id, 'draft'));

				return 'success';

			}

	    }



	    public function removeRecurringInvoiceDiscount() {

	    	$account_id = $_SESSION['account_id'];

	    	$invoice_id = $_POST['invoice_id'];

			$query = $this->dbo->prepare("UPDATE invoice_templates SET discount_amount = ?, discount_type = ?, discount_approved_by = ? WHERE account_id = ? AND id = ? AND status = ?");
			$query->execute(array(0, '', 0, $account_id, $invoice_id, 'draft'));

			return 'success';

	    }

	   

	  //   public function saveInvoice() {

	  //   	$account_id = $_SESSION['account_id'];
	  //   	$business_name = $_SESSION['business_name'];

	  //   	$query = $this->dbo->prepare("SELECT MAX(invoice_number) AS invoice_number FROM invoices WHERE account_id = ?");
			// $query->execute(array($account_id));

			// $result = $query->fetchAll();

			// foreach($result as $row) {

   //  			$max_invoice_number = $row[0];
 
   //  		}

   //  		$max_invoice_number++;

			// $business_name_initial = strtoupper(substr($business_name, 0, 1));
			// $data_array['preferences']['invoice_number'] = $business_name_initial . $max_invoice_number;


	  //   }



	    public function loadInvoiceFiles($type) {

	    	require_once('scripts/aws.phar');
			
			$s3client = Aws\S3\S3Client::factory(array(
		    	'key'    => 'AKIAJKBMGCMLYBHN77PQ',
		    	'secret' => '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'
		    ));


			$account_id = $_SESSION['account_id'];
			
			if($_GET['id']) {

				$invoice_id = $_GET['id'];

			} else if ($_POST['invoice_id']) {

				$invoice_id = $_POST['invoice_id'];
			
			} else {

				return false;
			}


			$files = array();

			if($type == 'single_invoice') { //// determine whether to get from invoices/ or recurring_invoices...

				$response = $s3client->getIterator('ListObjects', array(
				    'Bucket' => 'ironvault',
				    'Prefix' => 'invoices/' . $account_id . '/' . $invoice_id
				));

			} else if($type == 'recurring_invoice') {

				$response = $s3client->getIterator('ListObjects', array(
				    'Bucket' => 'ironvault',
				    'Prefix' => 'recurring_invoices/' . $account_id . '/' . $invoice_id
				));
			}

			//$count = 0;

			foreach($response as $object) {
			    	
			    //if($count > 0) {	
			    	
		    	$object_path = explode('/', $object['Key']);
		    	$object_name = array_pop($object_path);

		    	$object_name_parts = explode('.', $object_name);
		    	$object_type = array_pop($object_name_parts);


		    	if($object_name == $object_type) {
		    		//then the file extension is missing... thus make it blank!
		    		$object_type = '';
		    	}


		    	$object_last_modified = date('M j, Y', strtotime($object['LastModified']) + $_SESSION['timezone_offset']);

		    	$object_array = array($object_name, $object_type, $object_last_modified);

		    	//Method to sort the output order of the files since the AWS SDK does not provide a sorting functionality
		    	//They last modified date of the file is used as its key when added to the files array. Since files MAY have the same timestamp, we must first check if the index is empty, otherwise shift by 1 and try again.
		    	$array_offset = 0;

		    	while(!empty($files[strtotime($object['LastModified']) + $array_offset])) {

		    		$array_offset++;
		    	
		    	}
		    	
		    	$files[strtotime($object['LastModified']) + $array_offset] = $object_array;

		    	//Sort the array by the key values, decending (latest dated ones first)
		    	krsort($files);
			    //}

			    //$count++;
			}


			foreach ($files as $file) {

				$file_type = strtolower($file[1]);

				if($file_type == "pdf") {
				 	$type_class = "attachment-pdf";
				 
				} else if($file_type == "jpg" || $file_type == "jpeg" || $file_type == "png" || $file_type == "gif" || $file_type == "bmp") {
				 	$type_class = "attachment-image";
				 
				} else if($file_type == "xls") {
				 	$type_class = "attachment-excel";
				 
				} else if($file_type == "zip" || $file_type == "rar" || $file_type == "tar" || $file_type == "gz" || $file_type == "xz") {
				 	$type_class = "attachment-zip";
				 
				} else if($file_type == "doc" || $file_type == "docx") {
				 	$type_class = "attachment-word";
				 
				} else {

				 	$type_class = "attachment-plain";
				}

				
				$attachments .= '<div class="attachment" data-filename="' . $file[0] . '">
		                           	<div class="attachment-thumb ' . $type_class . '"></div>
		                           	<div class="attachment-info" title="' . $file[0] . '">
		                              	' . $file[0] . '<br>
		                              	<span class="faded">Last Modified: ' .$file[2] . '</span>
		                           	</div>
		                           	<div class="btn-group">
										<button class="btn light-blue dropdown-toggle" data-toggle="dropdown">Action <i class="icon-angle-down"></i>
										</button>
										<ul class="dropdown-menu bottom-up">
											<li><a href="https://cdn.ironvault.ca/invoices/' . $account_id . '/' . $invoice_id . '/' . $file[0] . '" class="download_file" download>Download File</a></li>
											<li><a href="#delete_invoice_file_modal" class="delete_file" data-toggle="modal">Delete</a></li>
										</ul>
									</div>
		                        </div>';

			}


			if($attachments == "") {

				$attachments = '<span class="box-message">No file attachments found. To add one now, click on \'+ Add File(s)\'.</span>';
			}


			return $attachments;

	    }



	    public function loadInvoiceFilesByToken() { //This function is ONLY used by the view_invoice page because it makes use of the Access Token in given in the URL.

	    	require_once('scripts/aws.phar');
			
			$s3client = Aws\S3\S3Client::factory(array(
		    	'key'    => 'AKIAJKBMGCMLYBHN77PQ',
		    	'secret' => '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'
		    ));

			$invoice_id = $_GET['id'];
			$access_token = $_GET['t'];

			$query = $this->dbo->prepare("SELECT account_id FROM invoices WHERE id = ? AND access_token = ?");
			$query->execute(array($invoice_id, $access_token));

			$result = $query->fetchAll();

			foreach($result as $row) {

    			$account_id = $row[0];

    		}


			$files = array();


			$response = $s3client->getIterator('ListObjects', array(
			    'Bucket' => 'ironvault',
			    'Prefix' => 'invoices/' . $account_id . '/' . $invoice_id
			));

			//$count = 0;

			foreach($response as $object) {
			    	
			    //if($count > 0) {	
			    	
		    	$object_path = explode('/', $object['Key']);
		    	$object_name = array_pop($object_path);

		    	$object_name_parts = explode('.', $object_name);
		    	$object_type = array_pop($object_name_parts);


		    	if($object_name == $object_type) {
		    		//then the file extension is missing... thus make it blank!
		    		$object_type = '';
		    	}


		    	$object_last_modified = date('M j, Y', strtotime($object['LastModified']) + $_SESSION['timezone_offset']);

		    	$object_array = array($object_name, $object_type, $object_last_modified);

		    	//Method to sort the output order of the files since the AWS SDK does not provide a sorting functionality
		    	//They last modified date of the file is used as its key when added to the files array. Since files MAY have the same timestamp, we must first check if the index is empty, otherwise shift by 1 and try again.
		    	$array_offset = 0;

		    	while(!empty($files[strtotime($object['LastModified']) + $array_offset])) {

		    		$array_offset++;
		    	
		    	}
		    	
		    	$files[strtotime($object['LastModified']) + $array_offset] = $object_array;

		    	//Sort the array by the key values, decending (latest dated ones first)
		    	krsort($files);
			    //}

			    //$count++;
			}


			foreach ($files as $file) {

				$file_type = strtolower($file[1]);

				if($file_type == "pdf") {
				 	$type_class = "attachment-pdf";
				 
				} else if($file_type == "jpg" || $file_type == "jpeg" || $file_type == "png" || $file_type == "gif" || $file_type == "bmp") {
				 	$type_class = "attachment-image";
				 
				} else if($file_type == "xls") {
				 	$type_class = "attachment-excel";
				 
				} else if($file_type == "zip" || $file_type == "rar" || $file_type == "tar" || $file_type == "gz" || $file_type == "xz") {
				 	$type_class = "attachment-zip";
				 
				} else if($file_type == "doc" || $file_type == "docx") {
				 	$type_class = "attachment-word";
				 
				} else {

				 	$type_class = "attachment-plain";
				}

				
				$attachments .= '<div class="attachment" data-filename="' . $file[0] . '">
		                           	<div class="attachment-thumb ' . $type_class . '"></div>
		                           	<div class="attachment-info" title="' . $file[0] . '">
		                              	' . $file[0] . '<br>
		                              	<span class="faded">Last Modified: ' .$file[2] . '</span>
		                           	</div>
		                           	<div class="btn-group">
										<button class="btn light-blue dropdown-toggle" data-toggle="dropdown">Action <i class="icon-angle-down"></i>
										</button>
										<ul class="dropdown-menu bottom-up">
											<li><a href="https://cdn.ironvault.ca/invoices/' . $account_id . '/' . $invoice_id . '/' . $file[0] . '" class="download_file" download>Download File</a></li>
											<li><a href="#delete_invoice_file_modal" class="delete_file" data-toggle="modal">Delete</a></li>
										</ul>
									</div>
		                        </div>';

			}


			if($attachments == "") {

				$attachments = '<span class="box-message">No file attachments.</span>';
			}


			return $attachments;

	    }




	    public function deleteInvoiceFile($type) {

	    	require_once('scripts/aws.phar');
			
			$s3client = Aws\S3\S3Client::factory(array(
		    	'key'    => 'AKIAJKBMGCMLYBHN77PQ',
		    	'secret' => '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'
		    ));

	    	$account_id = $_SESSION['account_id'];
	    	$filename = $_POST['filename'];
	    	$invoice_id = $_POST['invoice_id'];

	    	if($type == 'single_invoice') { //// determine whether to get from invoices/ or recurring_invoices...

				$s3client->deleteObject(array(
				    'Bucket' => 'ironvault',
					'Key' => 'invoices/' . $account_id . '/' . $invoice_id . '/' . $filename
				));

			} else if($type == 'recurring_invoice') {

				$s3client->deleteObject(array(
				    'Bucket' => 'ironvault',
					'Key' => 'recurring_invoices/' . $account_id . '/' . $invoice_id . '/' . $filename
				));
			
			}

	    }



		public function createInvoiceLoadClientInfo() {

	    	$account_id = $_SESSION['account_id'];

	    	$client_id = $_POST['client_id'];


	    	$query = $this->dbo->prepare("SELECT first_name, last_name, business_name, email, primary_phone, image, address FROM clients WHERE account_id = ? AND client_id = ?");
			$query->execute(array($account_id, $client_id));

			$result = $query->fetchAll();

			foreach($result as $row) {

				$first_name = $row[0]; 
				$last_name = $row[1]; 
				$business_name = $row[2]; 
				$email = $row[3]; 
				$primary_phone = $row[4]; 
				$image = $row[5]; 
				$address = $row[6]; 

				$primary_phone = preg_replace("/^(\d{3})(\d{3})(\d{4})$/", "($1) $2-$3", $primary_phone);
			}


			$result = "<ul class= 'unstyled'>";

			if($address != "") {

				$result .= '<li title="' . $address . '"><i class="icon-map-marker"></i> Address: <strong>' . $address . '</strong></li>';
			}

			if($business_name != "") {

				$result .= '<li><i class="icon-home"></i> Business Name: <strong>' . $business_name . '</strong></li>';
			}


			if($primary_phone != "") {

				$result .= '<li><i class="icon-phone"></i> Primary Phone: <strong>' . $primary_phone . '</strong></li>';
			}


			if($email != "") {

				$result .= '<li><i class="icon-envelope"></i> Email: <strong>' . $email . '</strong></li>';
			}

			$result .= "</ul>";


			return $result;

	    }

	}


?>