<?php

	session_start();

	require_once('error_handler.php');

	
	Class Search { 


		public function __construct () {

			require("inc/connect_dbo.php");

			$this->dbo = $dbo;

		}


		public function search($q, $filter) {

			$q = strtolower($q);

			$account_id = $_SESSION['account_id'];
			$user_id = $_SESSION['user_id'];

			$result = "";

			$total_num_rows = 0;


			if(in_array("tasks", $_SESSION['searchable_modules']) AND ($filter == "tasks" || $filter == "")) {

				$query = "SELECT id, task, due_date, completed FROM tasks WHERE (LOWER(task) LIKE '%$q%') AND (account_id = '$account_id' AND user_id = '$user_id')";

				$query_result = $this->dbo->query($query);
				$num_rows = $query_result->rowCount();

				$total_num_rows += $num_rows;

				if($num_rows > 0) {

					$tasks .= 	'<div class="portlet box blue">
					   		<div class="portlet-title">
					   			<div class="caption"><i class="icon-tasks"></i> Tasks</div>
					   		</div>

					   		<div class="portlet-body">';

					$tasks .=	'<ul class="row-fluid">';

			    	foreach($query_result as $row) {

			    		$id = $row[0];
			    		$task = $row[1];
			    		$date_due = $row[2];
			    		$completed = $row[3];

			    		if($completed == 1) {
			    			$status = "Completed";
			    		} else {
			    			$status = "In Progress";
			    		}

			    		$date = date('j M, Y', strtotime($date_due) + $_SESSION['timezone_offset']);
			    		$time = date('g:iA', strtotime($date_due) + $_SESSION['timezone_offset']);	

			    		$tasks .= "<li><i class='icon-angle-right'></i> <a href='tasks?task_id=$id'><strong>Task:</strong> " . $task . " – <strong>Due:</strong> " . $date . " at " . $time . " – <strong>Status:</strong> " . $status . "</a></li>";
			    		// <span class='external_link'><a href='tasks?task_id=$id' target='_blank'><i class='icon-external-link-sign' title='Open in New Tab'></i></a></span>
			    	}

			    	$tasks .=	'</ul>';

			    	$tasks .= "<div class='clearfix'></div> </div></div>";
			    }
		    }

		    $result .= $tasks;


		    if(in_array("calendar", $_SESSION['searchable_modules']) AND ($filter == "calendar" || $filter == "")) {

		    	$query = "SELECT id, start, end, title, allDay FROM calendar WHERE LOWER(title) LIKE '%$q%' AND account_id = '$account_id' AND user_id = '$user_id'";

				$query_result = $this->dbo->query($query);
				$num_rows = $query_result->rowCount();

				$total_num_rows += $num_rows;

				if($num_rows > 0) {

					$calendar .= '<div class="portlet box blue">
					   		<div class="portlet-title">
					   			<div class="caption"><i class="icon-calendar"></i> Calendar</div>
					   		</div>

					   		<div class="portlet-body">';

					$calendar .=	'<ul>';

			    	foreach($query_result as $row) {

			    		$id = $row[0];
			    		$start = $row[1];
			    		$end = $row[2];
			    		$title = $row[3];
			    		$all_day = $row[4];

			    		$start_date = date('j M, Y', strtotime($start) + $_SESSION['timezone_offset']);
			    		$start_time = date('g:iA', strtotime($end) + $_SESSION['timezone_offset']);

			    		$end_date = date('j M, Y', strtotime($end) + $_SESSION['timezone_offset']);
			    		$end_time = date('g:iA', strtotime($end) + $_SESSION['timezone_offset']);

			    		if($start_date == $end_date) {
			    			$date_from_to = "<strong>On:</strong> " . $start_date . ", " . $start_time . " - " . $end_time;
			    		} else {
			    			$date_from_to = "<strong>From:</strong> " . $start_date . ", " . $start_time . " <strong>To:</strong> " . $end_date . ", " . $end_time;
			    		}

			    		$calendar .= "<li><i class='icon-angle-right'></i> <a href='calendar?event_id=$id'><strong>Event:</strong> " . $title . " – " . $date_from_to .  "</a></li>";
			    	}

			    	$calendar .=	'</ul>';

			    	$calendar .= "</div></div>";
			    }
		    }

		    $result .= $calendar;


		    if(in_array("employees", $_SESSION['searchable_modules']) AND ($filter == "employees" || $filter == "")) {

		    	$query = "SELECT user_id, first_name, last_name, position FROM users WHERE (LOWER(first_name) LIKE '%$q%' OR LOWER(last_name) LIKE '%$q%' OR LOWER(email) LIKE '%$q%' OR LOWER(position) LIKE '%$q%' OR LOWER(primary_phone) LIKE '%$q%' OR LOWER(alternative_phone) LIKE '%$q%' OR LOWER(website) LIKE '%$q%') AND (account_id = '$account_id' AND status = 'active')";

				$query_result = $this->dbo->query($query);
				$num_rows = $query_result->rowCount();

				$total_num_rows += $num_rows;

				if($num_rows > 0) {

					$employees .= '<div class="portlet box blue">
						   		<div class="portlet-title">
						   			<div class="caption"><i class="icon-user"></i> ' . $_SESSION['users_label'] . '</div>
						   		</div>

						   		<div class="portlet-body">';

					$employees .=	'<ul>';

			    	foreach($query_result as $row) {

			    		$id = $row[0];
			    		$first_name = $row[1];
			    		$last_name = $row[2];
			    		$position = $row[3];

			    		if($position != "") {

			    			$employees .= "<li><i class='icon-angle-right'></i> <a href='profile?id=$id'><strong>" . $first_name . " " . $last_name . "</strong> – " . $position . "</a></li>";

			    		} else {


			    			$employees .= "<li><i class='icon-angle-right'></i> <a href='profile?id=$id'><strong>" . $first_name . " " . $last_name . "</strong></a></li>";

			    		}

			    	}

			    	$employees .=	'</ul>';

			    	$employees .= "</div></div>";
			    }
		    }

		    $result .= $employees;


		    if(in_array("inventory", $_SESSION['searchable_modules']) AND ($filter == "inventory" || $filter == "")) {

		    	$query = "SELECT item_id, product_id, item_name, category, item_qty, item_cost FROM inventory WHERE (LOWER(product_id) LIKE '%$q%' OR LOWER(item_name) LIKE '%$q%' OR LOWER(item_desc) LIKE '%$q%') AND (account_id = '$account_id')";

				$query_result = $this->dbo->query($query);
				$num_rows = $query_result->rowCount();

				$total_num_rows += $num_rows;

				if($num_rows > 0) {

					require_once("Account.class.php");
					$account = new Account();
					$currency = $account->getCurrency($currency);

					$inventory .= '<div class="portlet box blue">
						   		<div class="portlet-title">
						   			<div class="caption"><i class="icon-check"></i> Inventory</div>
						   		</div>

						   		<div class="portlet-body">';

					$inventory .= '<ul>';

			    	foreach($query_result as $row) {

			    		$id = $row[0];
			    		$product_id = $row[1];
			    		$item_name = $row[2];
			    		$cat_id = $row[3];
			    		$item_qty = $row[4];
			    		$item_cost = $row[5];

			    		$query = "SELECT cat_name FROM categories WHERE id = '$cat_id' AND account_id = '$account_id'";

						foreach($this->dbo->query($query) as $row) {
							$category = $row[0];
						}

			    		$inventory .= "<li><i class='icon-angle-right'></i> <a href='product?id=$id'><strong>" . $item_name . "</strong> – <strong>Category:</strong> " . $category . " – <strong>In Stock:</strong> " . $item_qty . " – <strong>Unit Price:</strong> " . $currency . $item_cost . "</a></li>";
			    	}

			    	$inventory .= '</ul>';

			    	$inventory .= "</div></div>";
			    }
		    }

		    $result .= $inventory;


		    if(in_array("clients", $_SESSION['searchable_modules']) AND ($filter == "clients" || $filter == "")) {

				$query = "SELECT client_id, first_name, last_name, business_name FROM clients WHERE (LOWER(first_name) LIKE '%$q%' OR LOWER(last_name) LIKE '%$q%' OR LOWER(business_name) LIKE '%$q%' OR LOWER(email) LIKE '%$q%' OR LOWER(primary_phone) LIKE '%$q%' OR LOWER(alternative_phone) LIKE '%$q%' OR LOWER(fax) LIKE '%$q%' OR LOWER(website) LIKE '%$q%' OR LOWER(notes) LIKE '%$q%' OR LOWER(address) LIKE '%$q%') AND (account_id = '$account_id')";

				$query_result = $this->dbo->query($query);
				$num_rows = $query_result->rowCount();

				$total_num_rows += $num_rows;

				if($num_rows > 0) {

					$clients .= '<div class="portlet box blue">
					   		<div class="portlet-title">
					   			<div class="caption"><i class="icon-group"></i> Clients</div>
					   		</div>

					   		<div class="portlet-body">';

					$clients .= '<ul>';

			    	foreach($query_result as $row) {

			    		$name = "";

			    		$id = $row[0];
			    		$first_name = $row[1];
			    		$last_name = $row[2];
			    		$business_name = $row[3];

			    		if($first_name != "") {
			    			$name .= $first_name . " ";
			    		}

			    		if($last_name != "") {
			    			$name .= $last_name;
			    		}

			    		if($business_name != "") {
			    			if($name != "") {
			    				$name .= " – " . $business_name;
			    			} else {
			    				$name = $business_name;
			    			}
			    		}

			    		$clients .= "<li><i class='icon-angle-right'></i> <a href='client_profile?id=$id'><strong>" . $name . "</strong></a></li>";
			    	}

			    	$clients .= '</ul>';

			    	$clients .= "</div></div>";
			    }
		    }

		    $result .= $clients;


		    if(in_array("invoices", $_SESSION['searchable_modules']) AND ($filter == "invoices" || $filter == "")) {

		    	$query = "SELECT id, client_id, invoice_number, date_created FROM invoices WHERE (LOWER(invoice_number) LIKE '%$q%') AND (account_id = '$account_id')";

		    	$query_result = $this->dbo->query($query);
				$num_rows = $query_result->rowCount();

				$total_num_rows += $num_rows;

				if($num_rows > 0) {

					$invoices .= '<div class="portlet box blue">
						   		<div class="portlet-title">
						   			<div class="caption"><i class="icon-book"></i> Invoices</div>
						   		</div>

						   		<div class="portlet-body">';

					$invoices .= '<ul>';

			    	foreach($query_result as $row) {

			    		$id = $row[0];
			    		$client_id = $row[1];
			    		$invoice_number = $row[2];
			    		$date_created = $row[3];

			    		$date = date('j M, Y', strtotime($date_created) + $_SESSION['timezone_offset']);
			    		$time = date('g:iA', strtotime($date_created) + $_SESSION['timezone_offset']);

			    		$query = "SELECT first_name, last_name, business_name FROM clients WHERE client_id = '$client_id' AND account_id = '$account_id'";

						foreach($this->dbo->query($query) as $row) {
							$first_name = $row[0];
							$last_name = $row[1];
							$business_name = $row[2];

							if($first_name != "") {
				    			$client_name .= $first_name . " ";
				    		}

				    		if($last_name != "") {
				    			$client_name .= $last_name;
				    		}

				    		if($business_name != "") {
				    			if($client_name != "") {
				    				$client_name .= ", " . $business_name;
				    			} else {
				    				$client_name = $business_name;
				    			}
				    		}
						}

			    		$invoices .= "<li><i class='icon-angle-right'></i> <a href='invoice?id=$id'><strong>Invoice #:</strong> " . $invoice_number . " – <strong>Client:</strong> " . $client_name . " – <strong>Date Created:</strong> " . $date . " at " . $time . "</a></li>";
			    	}

			    	$invoices .= '</ul>';

			    	$invoices .= "</div></div>";
			    }
		    }

		    $result .= $invoices;


		    if(in_array("locations", $_SESSION['searchable_modules']) AND ($filter == "locations" || $filter == "")) {

		    	$query = "SELECT id, name, address FROM locations WHERE (LOWER(name) LIKE '%$q%' OR LOWER(address) LIKE '%$q%') AND (account_id = '$account_id')";

		    	$query_result = $this->dbo->query($query);
				$num_rows = $query_result->rowCount();

				$total_num_rows += $num_rows;

				if($num_rows > 0) {

					$locations .= '<div class="portlet box blue">
						   		<div class="portlet-title">
						   			<div class="caption"><i class="icon-globe"></i> Locations</div>
						   		</div>

						   		<div class="portlet-body">';

					$locations .= '<ul>';

			    	foreach($query_result as $row) {

			    		$id = $row[0];
			    		$name = $row[1];
			    		$address = $row[2];

			    		$locations .= "<li><i class='icon-angle-right'></i> <a href='maps?l_id=$id'><strong>" . $name . "</strong> – " . $address . "</a></li>";
			    	}

			    	$locations .= '<ul>';

			    	$locations .= "</div></div>";
			    }
		    }

		    $result .= $locations;


		    /*if($filter == "storage" || $filter == "") {

		    	$query = "SELECT id, filename, type, size, date_added FROM storage WHERE LOWER(filename) LIKE '%$q%' AND account_id = '$account_id' AND user_id = '$user_id'";

				$query_result = $this->dbo->query($query);
				$num_rows = $query_result->rowCount();

				$total_num_rows += $num_rows;

				if($num_rows > 0) {

					$storage .= '<div class="portlet box blue">
					   		<div class="portlet-title">
					   			<div class="caption"><i class="icon-cloud"></i> Storage</div>
					   		</div>

					   		<div class="portlet-body">';

					$storage .= '<ul>';

			    	foreach($query_result as $row) {

			    		$id = $row[0];
						$filename = $row[1];
						$type = strtoupper($row[2]);
						$size = $row[3];
						$date_added = $row[4];

						$date = date('j M, Y', strtotime($date_added) + $_SESSION['timezone_offset']);
			    		$time = date('g:iA', strtotime($date_added) + $_SESSION['timezone_offset']);

			    		$storage .= "<li><i class='icon-angle-right'></i> <a href='storage?file_id=$id'><strong>" . $filename . "</strong> – <strong>Type:</strong> " . $type . " – <strong>Size:</strong> " . $size . " – <strong>Date Added:</strong> " . $date . " at " . $time . "</a></li>";
			    	}

			    	$storage .= '</ul>';

			    	$storage .= "</div></div>";
			    }
		    }

		    $result .= $storage;*/

		    if($total_num_rows == 1) {

		   		$result = '<div class="search_results_header"><span><strong>' . $total_num_rows . '</strong> result found</span><div class="loading"></div></div>' . $result;
		    
		    } else {

		    	$result = '<div class="search_results_header"><span><strong>' . $total_num_rows . '</strong> results found</span><div class="loading"></div></div>' . $result;
		    }


	    	return $result;

		}

	}

?>