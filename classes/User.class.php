<?php

	session_start();

	require_once('error_handler.php');

	
	Class User { 



		public function __construct () {

			require("inc/connect_dbo.php");

			$this->dbo = $dbo;

		}


		// sessionCheck NOT BEING USED ........ 
		public function sessionCheck($account_id, $user_id) {


			$query = $this->dbo->prepare("SELECT session_id FROM session WHERE account_id = ? AND user_id = ? AND session_end = ?");
			$query->execute(array($account_id, $user_id, 0));

			$result = $query->fetchAll();

			foreach($result as $row) {
			    
			    $session_id = $row['session_id'];
			}


			if($session_id != "") {

				$session_end = mktime();

				$query = $this->dbo->prepare("UPDATE session SET session_end = ? WHERE account_id = ? AND user_id = ? AND session_id = ?");
				$query->execute(array($session_end, $account_id, $user_id, $session_id));


				session_id($session_id);
				session_start();
				session_destroy();

				return true;
			
			} else {

				return false;
			}

		}



		public function login() {

			require_once("classes/Main.class.php");
			$main = new Main();

			//The SESSION account_id gets set every time the user successfully gets to the login page. If the account name (subdomain) is incorrect, they won't get to the login page at all.
	    	$account_id = $_SESSION['account_id'];

	    	$username = $_POST['username'];

	    	$password = $_POST['password'];
			
			$hash_password = hash('SHA512', $password);


			$query = $this->dbo->prepare("SELECT COUNT(*) FROM users WHERE account_id = ? AND username = ? AND password = ? LIMIT 1");
			$query->execute(array($account_id, $username, $hash_password));

			$result = $query->fetchAll();

			foreach($result as $row) {

				$num_rows = $row[0];
			}


			if($num_rows > 0) { 


				$query = $this->dbo->prepare("SELECT user_id, username, first_name, last_name, gender, image, sound, type, lock_timeout, position, email, primary_phone, status, timezone FROM users WHERE account_id = ? AND username = ? AND password =  ?LIMIT 1");
				$query->execute(array($account_id, $username, $hash_password));

				$result = $query->fetchAll();

				foreach($result as $row) {

		    		$user_id = $row[0];

		    		$status = $row[12];

		    		if($status == 'active') {

			    		$subdomain = $_SESSION['subdomain'];
			    		$business_name = $_SESSION['business_name'];

						
			    		//$this->sessionCheck($account_id, $user_id);

			    		//session_regenerate_id(true);
			    		//$session_id = session_id();


			    		$_SESSION['account_id'] = $account_id;
						$_SESSION['subdomain'] = $subdomain;
						$_SESSION['business_name'] = $business_name;

			    		$_SESSION['user_id'] = $row[0];
			    		$_SESSION['username'] = $row[1];
			    		$_SESSION['first_name'] = $row[2];
			    		$_SESSION['last_name'] = $row[3];
			    		$_SESSION['gender'] = $row[4];
			    		$gender = $row[4];

			    		//check for image
			    		if($row[5] == "") {

			    			if($gender == "male") {

			    				$_SESSION['image'] = "https://www.ironvault.ca/app/img/male_default.png";
			    				$_SESSION['image'] = "https://www.ironvault.ca/app/img/male_default_thumb.png";
			    			
			    			} else if($gender == "female") {

			    				$_SESSION['image'] = "https://www.ironvault.ca/app/img/female_default.png";
			    				$_SESSION['image'] = "https://www.ironvault.ca/app/img/female_default_thumb.png";
			    			}
			    		} else {

			    			$_SESSION['image'] = 'https://cdn.ironvault.ca/pr_imgs/' . $row[5];
							$_SESSION['image_thumb'] = 'https://cdn.ironvault.ca/pr_imgs/thumbs/' . $row[5];
			    		}
			    		
			    		$_SESSION['sound'] = $row[6];
						
						$_SESSION['account_type'] = $row[7];

						$_SESSION['lock_timeout'] = $row[8];

						$_SESSION['position'] = $row[9];
						$_SESSION['email'] = $row[10];
						$_SESSION['primary_phone'] = preg_replace("/^(\d{3})(\d{3})(\d{4})$/", "($1) $2-$3", $row[11]);

						$_SESSION['timezone'] = $row[13];
						$_SESSION['timezone_offset'] = $main->getTimezoneOffset($row[13]); //get the timezone offset from the MAIN class.

			    		$_SESSION['session_id'] = $session_id;

			    		$_SESSION['status'] = "active";

			    		$_SESSION['ip_address'] = $_SERVER['REMOTE_ADDR'];



			    		$query = $this->dbo->prepare("SELECT users_label FROM account_preferences WHERE account_id = ?");
						$query->execute(array($account_id));

						$result = $query->fetchAll();

						foreach($result as $row) {

							$users_label = $row[0];
						}

						$_SESSION['users_label_slang'] = $users_label;
						
			    		header('Location: dashboard');


			    	} else if($status == 'inactive') {

			    		header('Location: login?e=account_disabled');
			    	
			    	} else {

			    		header('Location: login?e=login_failed');
			    	}

				}


			} else {

				header('Location: login?e=login_failed');

			}

	    }



	    public function lockSession() {

	    	if($_SESSION['status'] == "active" || $_SESSION['status'] == "locked") {

	    		$data = array();

				$_SESSION['status'] = "locked";

				$data['full_name'] = $_SESSION['first_name'] . " " . $_SESSION['last_name'];
				$data['business_name'] = $_SESSION['business_name'];
				$data['image'] = $_SESSION['image'];

				 return $data;

			} else {

				header('Location: login');
			}
	    }



	    public function logout($msg) {


	    	$subdomain = $_SESSION['subdomain'];



	    	$account_id = $_SESSION['account_id'];

	    	$user_id = $_SESSION['user_id'];

			$session_id = $_SESSION['session_id'];

			

			$session_end = mktime();

			
			$query = $this->dbo->prepare("UPDATE session SET session_end = ? WHERE account_id = ? AND user_id = ? AND session_id = ?");
			$query->execute(array($session_end, $account_id, $user_id, $session_id));
			

			session_unset();  // destroy $_SESSION ram

			session_destroy();  // destroy $_SESSION file

			session_write_close();


			if($msg != "") {
				header('Location: login?r=' . $msg);
			} else {
				header('Location: login');
			}

	    }



	    public function unlock() {  

	    	$account_id = $_SESSION['account_id'];

	    	$username = $_SESSION['username'];

	    	$password = $_POST['password'];

	    	$hash_password = hash('SHA512', $password);

	    	
	    	$query = $this->dbo->prepare("SELECT COUNT(*) FROM users WHERE account_id = ? AND username = ? AND password =  ? AND status = ? LIMIT 1");
			$query->execute(array($account_id, $username, $hash_password, 'active'));

			$result = $query->fetchAll();

			foreach($result as $row) {

				$num_rows = $row[0];
			}


			if($num_rows > 0) {


				$_SESSION['status'] = "active";

				if($_SESSION['last_page']) {
					header('Location: ' . $_SESSION['last_page']);
				} else {
					header('Location: dashboard');
				}

			} else {

				header('Location: lock?e=wrong_password');
			}

	    }



	    public function overlayUnlock() {  

	    	$account_id = $_SESSION['account_id'];

	    	$username = $_SESSION['username'];

	    	$password = $_POST['password'];

	    	$hash_password = hash('SHA512', $password);

	    	
	    	$query = $this->dbo->prepare("SELECT COUNT(*) FROM users WHERE account_id = ? AND username = ? AND password =  ? AND status = ? LIMIT 1");
			$query->execute(array($account_id, $username, $hash_password, 'active'));

			$result = $query->fetchAll();

			foreach($result as $row) {

				$num_rows = $row[0];
			}


			if($num_rows > 0) {


				$_SESSION['status'] = "active";

				return 'success';

			} else {

				return 'error';
			}

	    }



	    public function resetPassword() {

	  		require_once("classes/Email.class.php");
			$phpMailer = new Email();

	    	$account_id = $_SESSION['account_id'];
	    	
	    	$email = $_POST['email'];

	    	$found = false;


			$query = $this->dbo->prepare("SELECT user_id, first_name, last_name FROM users WHERE account_id = ? AND email = ? AND status = ?");
			$query->execute(array($account_id, $email, "active"));

			$result = $query->fetchAll();

			foreach($result as $row) {

				$found = true;
			    
			    $user_id = $row['user_id'];
			    $first_name = $row['first_name'];
			    $last_name = $row['last_name'];

			    $token = $this->randomToken();

			    $token_hash = hash('SHA512', $token);

			    $datetime = date('Y-m-d H:i:s', time() + 86400); //NOW PLUS 1 DAY for EXPIRY

			    //$query = $this->dbo->prepare("UPDATE users SET password = ? WHERE account_id = ? AND user_id = ?");
			    $query = $this->dbo->prepare("INSERT INTO password_reset_token SET account_id = ?, email = ?, token = ?, expiry = ?");
				$query->execute(array($account_id, $email, $token_hash, $datetime));

				$phpMailer->resetPassword($first_name, $last_name, $email, $token_hash, $datetime);

				return true;

			}


			if($found == false) {

				$phpMailer->accountNotFound($email);

				return false;
			}


			

	    }


	    public function randomToken($length = 20) {
		    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		    $randomString = '';
		    for ($i = 0; $i < $length; $i++) {
		        $randomString .= $characters[rand(0, strlen($characters) - 1)];
		    }
		    return $randomString;
		}



		public function checkPasswordResetToken() {

			$account_id = $_SESSION['account_id'];
			$token = $_GET['token'];
			$email = html_entity_decode($_GET['email']);

			$num_rows = 0;

			$datetime = date('Y-m-d H:i:s');

			$query = $this->dbo->prepare("SELECT email FROM password_reset_token WHERE account_id = ? AND email = ? AND token = ? AND expiry >= ?");
			$query->execute(array($account_id, $email, $token, $datetime));

			$result = $query->fetchAll();

			foreach($result as $row) {

				$found_email = $row['email'];

			}

			if($found_email != "") {

				return $found_email;

			} else {

				return false;
			}

		}



		public function resetPasswordChange($email) {

			require_once("classes/Email.class.php");
			$phpMailer = new Email();

			$account_id = $_SESSION['account_id'];

			$new_password = $_POST['new_password'];
			$new_password2 = $_POST['new_password2'];

			$token = $_GET['token'];


			if($new_password == $new_password2) {

				//if (preg_match('/(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/', $new_password)) {
				if(strlen($new_password) >= 8) {

					$new_password = hash('SHA512', $new_password);

					$query = $this->dbo->prepare("UPDATE users SET password = '$new_password' WHERE account_id = ? AND email = ?");
					$query->execute(array($account_id, $email));


					$query = $this->dbo->prepare("SELECT first_name, last_name FROM users WHERE account_id = ? AND email = ?");
					$query->execute(array($account_id, $email));

					$result = $query->fetchAll();

					foreach($result as $row) {

						$first_name = $row['first_name'];
						$last_name = $row['last_name'];
					}


			    	$query = $this->dbo->prepare("DELETE FROM password_reset_token WHERE account_id = ? AND email = ?");
					$query->execute(array($account_id, $email));

					$phpMailer->confirmPasswordReset($first_name, $last_name, $email);

			    	header('Location: password_change?token=' . $token . '&email=' . $email . '&r=password_change_successful');

				} else {

					header('Location: password_change?token=' . $token . '&email=' . $email . '&e=insecure_password');
				}

			} else {

				header('Location: password_change?token=' . $token . '&email=' . $email . '&e=password_mismatch');
			}
		}



		public function getProfileThumbnail() { //usually accessed by the join.php page -- users at this point are not logged in yet, thus variables are passed from POST.

			if($_POST['account_id'] != '' && $_POST['user_id'] != '') {

	            $account_id = $_POST['account_id'];
	            $user_id = $_POST['user_id'];

	        } else {

	            $account_id = $_SESSION['account_id'];
	            $user_id = $_SESSION['user_id'];
	        }

			$query = $this->dbo->prepare("SELECT image FROM users WHERE account_id = ? AND user_id = ?");
			$query->execute(array($account_id, $user_id));

			$result = $query->fetchAll();

			foreach($result as $row) {

				$image = 'https://cdn.ironvault.ca/pr_imgs/thumbs/' . $row[0];
			}

			return $image;
		}



	    public function getUnseenConvoIds() {

	    	$account_id = $_SESSION['account_id'];
	    	$user_id = $_SESSION['user_id'];

	    	$unseen_convos = "";


	    	$query = $this->dbo->prepare("SELECT convo_id FROM notifications WHERE account_id = ? AND user_id = ? AND type = 'message' AND status = ?");
			$query->execute(array($account_id, $user_id, 'unseen'));

			$result = $query->fetchAll();

			foreach($result as $row) {

	    		$unseen_convos .= $row[0] . ",";
	    	}

	    	if($unseen_convos != "") {
	    		$unseen_convos = substr($unseen_convos, 0, -1);
	    	} else {
	    		$unseen_convos = 0;
	    	}

	    	return $unseen_convos;

	    }



	    public function addNotification() {

	    	$account_id = $_SESSION['account_id'];
	    	$user_id = $_SESSION['user_id'];

            $convo_id = $_POST['convo_id'];
            $type = $_POST['type'];
            $users = $_POST['receiver_user_ids'];

            //Take out own user ID from users list. DO not need to send notification to self.
			$users = explode(",", $users);
			$user_pos = array_search($user_id, $users);
			unset($users[$user_pos]);


            if($type == "message") {

            	foreach ($users as $user) {



            		$query = $this->dbo->prepare("SELECT COUNT(*) FROM notifications WHERE account_id = ? AND convo_id = ? AND user_id =  ? AND type = ?");
					$query->execute(array($account_id, $convo_id, $user, 'message'));

					$result = $query->fetchAll();

					foreach($result as $row) {

						$num_rows = $row[0];
					}


			    	$datetime = date('Y-m-d H:i:s');

					if($num_rows > 0) {

						//notification for this convo exists
						$query = $this->dbo->prepare("UPDATE notifications SET status = ?, date = ? WHERE account_id = ? AND convo_id = ? AND user_id =  ? AND type = ?");
						$query->execute(array('unseen', $datetime, $account_id, $convo_id, $user, 'message'));

					} else {

						//notification for this convo does not exist
						$query = $this->dbo->prepare("INSERT INTO notifications SET account_id = ?, convo_id = ?, user_id =  ?, type = ?, status = ?, date = ?");
						$query->execute(array($account_id, $convo_id, $user, 'message', 'unseen', $datetime));

					}
				}

			}

	    }



	    public function loadNotifications() {

	    	$account_id = $_SESSION['account_id'];
	    	$user_id = $_SESSION['user_id'];

	    	$msgs_notif_count = 0;
	    	$events_notif_count = 0;
	    	$tasks_notif_count = 0;


	    	$query = $this->dbo->prepare("SELECT notif_id, type, date FROM notifications WHERE account_id = ? AND user_id =  ? AND status = ?");
			$query->execute(array($account_id, $user_id, 'unseen'));

			$result = $query->fetchAll();

			foreach($result as $row) {

	    		if($row[1] == "message") {
	    			$msgs_notif_count++;
	    		} else if($row[1] == "event") {
	    			$events_notif_count++;
	    		} else if($row[1] == "task") {
	    			$tasks_notif_count++;
	    		}

	    	}

	    	$result = array($msgs_notif_count, $events_notif_count, $tasks_notif_count);

	    	//$_SESSION['messages_notifications']
	    	return $result;
	    }


	    public function updateNotificationsDropdown($which) { 


	    	/* 

				NOTE: The notifications that are added to the notifications table are of various kinds (messages, events, tasks), 
				the datetime stamps are therefore referring to different things for each kind.
				For the events, the datetime is referring to the START datetime of the event.
				For the tasks, the datetime is referring to the DUE date of the task.
				For the messages, the datetime is referring to the LAST MESSAGE received time.

	    	*/


	    	$account_id = $_SESSION['account_id'];
	    	$user_id = $_SESSION['user_id'];

	    	if($which == "messages") {


	    		$query = $this->dbo->prepare("SELECT notif_id, convo_id, status, date FROM notifications WHERE account_id = ? AND user_id =  ? AND type = ? ORDER BY date DESC LIMIT 5");
				$query->execute(array($account_id, $user_id, 'message'));

				$result = $query->fetchAll();

				foreach($result as $row) {

		    		$notif_id = $row[0];
		    		$convo_id = $row[1];
		    		$status = $row[2];
		    		$datetime = $row[3];

		    		$time_since = $this->time_since(time() - strtotime($datetime));
	    			$time_since .= " ago";


	    			$query2 = $this->dbo->prepare("SELECT from_user, message FROM messages WHERE account_id = ? AND convo_id = ? ORDER BY id DESC LIMIT 1");
					$query2->execute(array($account_id, $convo_id));

					$result2 = $query2->fetchAll();

					foreach($result2 as $row2) {

		    			$from_user = $row2[0];
		    			$message = $row2[1];
		    		}

		    		$message = preg_replace('/<br\s*\/?>/', " ", $message);



		    		$query3 = $this->dbo->prepare("SELECT users FROM conversations WHERE convo_id = ? AND account_id = ?");
					$query3->execute(array($convo_id, $account_id));

					$result3 = $query3->fetchAll();

					foreach($result3 as $row3) {

		    			$users = $row3[0];
		    		}


		    		//take out own user_id from the string of users ids. No need to show own name.
		    		$users = explode(",", $users);

					$this_user_pos = array_search($user_id, $users);
					unset($users[$this_user_pos]);

					//this makes the user with the last message in the convo appear first
					if($from_user != $user_id) {

						$from_user_pos = array_search($from_user, $users);
						unset($users[$from_user_pos]);
						array_unshift($users, $from_user);
					}

					$users_array_string = implode(",", $users);
		    		//now get the each user's information
		    		$count = 0;
					$users_string = "";
					$users_array = array();


					//Left this one as a traditional PHP/MySQL statement because IN() is a pain in the butt with Prepared statements...
		    		$query4 = "SELECT first_name, last_name, image FROM users WHERE account_id = '$account_id' AND user_id IN ($users_array_string)";

					foreach($this->dbo->query($query4) as $row4) {

						$first_name = $row4[0];
						$last_name = $row4[1];
						$image = $row4[2];

						$users_string .= $first_name . " " . $last_name . ", ";

						$users_array[$count]['first_name'] = $first_name;
						$users_array[$count]['last_name'] = $last_name;
						$users_array[$count]['image'] = 'https://cdn.ironvault.ca/pr_imgs/thumbs/' . $image;

						$count++;
					}

					$users_string = substr($users_string, 0, -2);


		    		$unread = "";
		    		if($status == "seen" || $status == "unseen") { //a bit couter-intuitive, but unread messages are marked as light blue. viewed/read messages are grey.
		    			$unread = 'class="unread"';
		    		}

		    		$html .= '<li ' . $unread. '>
			                     <a href="messages?conv_id=' . $convo_id . '">
				                     <span class="photo"><img src="' . $users_array[0]['image'] . '" alt=""></span>
				                     <span class="subject">
				                     	<span class="from">' . $users_string . '</span>
				                     	<span class="time">' . $time_since . '</span>
				                     </span>
				                     <span class="message">
					                     ' . $message . '
				                     </span>  
			                     </a>
			                  </li>';
		    	}

		    	$html .= '<li class="external">
		                     <a href="messages">See all Messages <i class="m-icon-swapright"></i></a>
		                  </li>';

		    } else if($which == "events") {


		    	$query = $this->dbo->prepare("SELECT notif_id, event_id, status, date FROM notifications WHERE account_id = ? AND user_id = ? AND type = ? ORDER BY date LIMIT 10");
				$query->execute(array($account_id, $user_id, 'event'));

				$result = $query->fetchAll();

				foreach($result as $row) {

		    		$notif_id = $row[0];
		    		$event_id = $row[1];
		    		$status = $row[2];
		    		$datetime = $row[3];

		    		if( (time() - strtotime($datetime)) > 0 ) {
			    		$time_since = $this->time_since(time() - strtotime($datetime));
		    			$time_since = "in " . $time_since;
		    		} else {
		    			$time_since = $this->time_since(strtotime($datetime) - time());
		    			$time_since = $time_since . " ago";
		    		}


		    		$query2 = $this->dbo->prepare("SELECT start, end, title, allDay FROM calendar WHERE account_id = ? AND user_id = ? AND id = ?");
					$query2->execute(array($account_id, $user_id, $event_id));

					$result2 = $query2->fetchAll();

					foreach($result2 as $row2) {

		    			$start = $row2[0];
		    			$end = $row2[1];
		    			$title = $row2[2];
		    			$all_day = $row2[3];
		    		}


					$unread = "";
		    		if($status == "seen") { //a bit couter-intuitive, but unread messages are marked as light blue. viewed/read messages are grey.
		    			$unread = 'class="unread"';
		    		}

		    		$html .= '<li ' . $unread. '>
								<a href="calendar?event_id=' . $event_id . '">
									<i class="icon-calendar"></i>
									<span class="subject">
				                     	<span class="time">' . $time_since . '</span>
				                     </span>
				                     <span class="message">
										' . $title . '
									</span>
								</a>
							</li>';
		    	}

		    	$html .= '<li class="external">
		                     <a href="calendar">See Calendar <i class="m-icon-swapright"></i></a>
		                  </li>';

		    } else if($which == "tasks") {


		    	$query = $this->dbo->prepare("SELECT notif_id, task_id, status, date FROM notifications WHERE account_id = ? AND user_id = ? AND type = ? ORDER BY date LIMIT 10");
				$query->execute(array($account_id, $user_id, 'task'));

				$result = $query->fetchAll();

				foreach($result as $row) {

		    		$notif_id = $row[0];
		    		$task_id = $row[1];
		    		$status = $row[2];
		    		$datetime = $row[3];


		    		if( (time() - strtotime($datetime)) > 0 ) {
			    		$time_since = $this->time_since(time() - strtotime($datetime));
		    			$time_since = "due in " . $time_since;
		    		} else {
		    			$time_since = $this->time_since(strtotime($datetime) - time());
		    			$time_since = "due " . $time_since . " ago";
		    		}


		    		$query2 = $this->dbo->prepare("SELECT task, date_due FROM tasks WHERE account_id = ? AND user_id = ? AND id = ?");
					$query2->execute(array($account_id, $user_id, $task_id));

					$result2 = $query2->fetchAll();

					foreach($result2 as $row2) {

		    			$task = $row2[0];
		    			$date_due = $row2[1];
		    		}


					$unread = "";
		    		if($status == "seen") { //a bit couter-intuitive, but unread messages are marked as light blue. viewed/read messages are grey.
		    			$unread = 'class="unread"';
		    		}

		    		$html .= '<li ' . $unread. '>
								<a href="tasks?task_id=' . $task_id . '">
									<i class="icon-tasks"></i>
									<span class="subject">
				                     	<span class="time">' . $time_since . '</span>
				                     </span>
				                     <span class="message">
										' . $task . '
									</span>
								</a>
							</li>';
		    	}

		    	$html .= '<li class="external">
		                     <a href="tasks">See all Tasks <i class="m-icon-swapright"></i></a>
		                  </li>';
		    
		    }

	    	return $html;

	    }


	    public function time_since($since) {
		    $chunks = array(
		        array(60 * 60 * 24 * 365 , 'year'),
		        array(60 * 60 * 24 * 30 , 'month'),
		        array(60 * 60 * 24 * 7, 'week'),
		        array(60 * 60 * 24 , 'day'),
		        array(60 * 60 , 'hour'),
		        array(60 , 'minute'),
		        array(1 , 'second')
		    );

		    for ($i = 0, $j = count($chunks); $i < $j; $i++) {
		        $seconds = $chunks[$i][0];
		        $name = $chunks[$i][1];
		        if (($count = floor($since / $seconds)) != 0) {
		            break;
		        }
		    }

		    $print = ($count == 1) ? '1 '.$name : "$count {$name}s";
		   
		    return $print;
		}



		public function removeMessageNotification() {

	    	$account_id = $_SESSION['account_id'];
	    	$user_id = $_SESSION['user_id'];

	    	$convo_id = $_POST['convo_id'];

	    	$datetime = date('Y-m-d H:i:s');


	    	$query = $this->dbo->prepare("UPDATE notifications SET status = ?, date = ? WHERE account_id = ? AND user_id = ? AND convo_id = ? AND type = ?");
			$result = $query->execute(array('viewed', $datetime, $account_id, $user_id, $convo_id, 'message'));

	    	return $result;
	    }


	    public function clearMessagesNotifications() {

	    	$account_id = $_SESSION['account_id'];
	    	$user_id = $_SESSION['user_id'];

	    	$query = $this->dbo->prepare("UPDATE notifications SET status = ? WHERE status = ? AND account_id = ? AND user_id = ? AND type = ?");
			$result = $query->execute(array('seen', 'unseen', $account_id, $user_id, 'message'));

	    	return true;
	    }


	    public function clearEventsNotifications() {

	    	$account_id = $_SESSION['account_id'];
	    	$user_id = $_SESSION['user_id'];

	    	$query = $this->dbo->prepare("UPDATE notifications SET status = ? WHERE status = ? AND account_id = ? AND user_id = ? AND type = ?");
			$result = $query->execute(array('seen', 'unseen', $account_id, $user_id, 'event'));

	    	return true;
	    }


	    public function clearTasksNotifications() {

	    	$account_id = $_SESSION['account_id'];
	    	$user_id = $_SESSION['user_id'];

	    	$query = $this->dbo->prepare("UPDATE notifications SET status = ? WHERE status = ? AND account_id = ? AND user_id = ? AND type = ?");
			$result = $query->execute(array('seen', 'unseen', $account_id, $user_id, 'task'));

	    	return true;
	    }


		public function loadTopMenu() {



	    	$account_id = $_SESSION['account_id'];

	    	$username = $_SESSION['username'];



	    	$top_menu = array();


	    	$query = $this->dbo->prepare("SELECT first_name, last_name, image FROM users WHERE account_id = ? AND username = ? LIMIT 1");
			$query->execute(array($account_id, $username));

			$result = $query->fetchAll();

			foreach($result as $row) {
				

	    		$top_menu['first_name'] = $row[0];

	    		$top_menu['last_name'] = $row[1];

	    		$top_menu['image'] = $row[2];



	    		return $top_menu;

			}

	    }


	    public function loadUserProfile() {
		
			$account_id = $_SESSION['account_id'];

			if($_GET['id']) {

				$user_id = $_GET['id'];

				if ($_GET['id'] == $_SESSION['user_id']) {

						header('Location: profile');

				}

				//$profile['back_button'] = '<a href="employees" class="btn portlet_title_btn"><i class="icon-arrow-left"></i></a>';
			
			} else {

				$user_id = $_SESSION['user_id'];
			}



			$query = $this->dbo->prepare("SELECT status FROM users WHERE account_id = ? AND user_id = ?");
			$query->execute(array($account_id, $user_id));

			$result = $query->fetchAll();

			foreach($result as $row) {

				$status = $row[0];
			}


			if($status == 'active') {


				$query = $this->dbo->prepare("SELECT first_name, last_name, email, position, working_since, gender, dob, primary_phone, alternative_phone, website, about, image, hometown, country, languages, hobbies, fav_books, fav_movies, fav_music, show_dob_year FROM users WHERE account_id = ? AND user_id = ? AND status = ?");
				$query->execute(array($account_id, $user_id, 'active'));

				$result = $query->fetchAll();

				foreach($result as $row) {

					$profile['first_name'] = $row[0]; 
					$profile['last_name'] = $row[1]; 
					$profile['email'] = $row[2]; 
					$profile['position'] = $row[3]; 
					$profile['working_since'] = $row[4]; 
					$profile['gender'] = $row[5]; 
					$profile['dob'] = $row[6]; 
					$profile['primary_phone'] = $row[7]; 
					$profile['alternative_phone'] = $row[8]; 
					$profile['website'] = $row[9]; 
					$profile['about'] = $row[10]; 
					$profile['image'] = $row[11]; 
					$profile['hometown'] = $row[12]; 
					$profile['country'] = $row[13]; 
					$profile['languages'] = $row[14]; 
					$profile['hobbies'] = $row[15]; 
					$profile['fav_books'] = $row[16]; 
					$profile['fav_movies'] = $row[17]; 
					$profile['fav_music'] = $row[18]; 
					$profile['show_dob_year'] = $row[19]; 


					if($profile['image'] == "") {

		    			if($profile['gender'] == "male") {

		    				$profile['image'] = '<img src="https://www.ironvault.ca/app/img/male_default.png" alt="" class="profile_pic" />';
		    			
		    			} else if($profile['gender'] == "female") {

		    				$profile['image'] = '<img src="https://www.ironvault.ca/app/img/female_default.png" alt="" class="profile_pic" />';
		    			}

		    		} else {

		    			$profile['image'] = '<a href="https://cdn.ironvault.ca/pr_imgs/' . $profile['image'] . '" class="fancybox"><img src="https://cdn.ironvault.ca/pr_imgs/' . $profile['image'] . '" alt="" class="profile_pic" /></a>';
		    		}


		    		if($profile['about'] != "") {

		    			$profile['about'] = '<hr class="grey"><div class="spacer_10"></div>
		    			<h3>About</h3>
                           <p class="about">' . $profile['about'] . '</p>';

		    		}


					$profile['primary_phone'] = preg_replace("/^(\d{3})(\d{3})(\d{4})$/", "($1) $2-$3", $profile['primary_phone']);
					$profile['alternative_phone']= preg_replace("/^(\d{3})(\d{3})(\d{4})$/", "($1) $2-$3", $profile['alternative_phone']);


					if($profile['dob'] == "0000-00-00") {

						$profile['dob'] = "";
					
					} else {

						if($profile['show_dob_year'] == 1) {

							$profile['dob'] = date("F j, Y", strtotime($profile['dob']));
						
						} else {

							$profile['dob'] = date("F j", strtotime($profile['dob']));
						}
					}



					if($profile['working_since'] == "0000-00-00") {

						$profile['working_since'] = "";
					
					} else {

						$profile['working_since'] = date("F, Y", strtotime($profile['working_since']));
					}


					$query = $this->dbo->prepare("SELECT country_name FROM countries WHERE country_id = ?");
					$query->execute(array($profile['country']));

					$result = $query->fetchAll();

					foreach($result as $row) {
						
						$profile['country'] = $row[0];
					}


					$profile['languages'] = str_replace(",",", ", $profile['languages']);

				}


				if($profile['primary_phone'] != "") {

					$profile['info_table'] .= '<tr><td><i class="icon-phone"></i></td><td>Primary Phone:</td><td><b>' . $profile['primary_phone'] . '</b></td></tr>';
				}
					

				if($profile['alternative_phone'] != "") {

					$profile['info_table'] .= '<tr><td><i class="icon-phone"></i></td><td>Alternative Phone:</td><td><b>' . $profile['alternative_phone'] . '</b></td></tr>';
				}

				if($profile['website'] != "") {

					if (!preg_match("~^(?:f|ht)tps?://~i", $profile['website'])) {
				        $http_website = "http://" . $profile['website'];
				    }

				    $profile['info_table'] .= '<tr><td><i class="icon-globe"></i></td><td>Website:</td><td><b><a href="' . $http_website . '" target="_blank">' . $profile['website'] . '</a></b></td></tr>';
				}

				if($profile['dob'] != "") {

					$profile['info_table'] .= '<tr><td><i class="icon-calendar"></i></td><td>Birth Date:</td><td><b>' . $profile['dob'] . '</b></td></tr>';
				}
				if($profile['languages'] != "") {

					$profile['info_table'] .= '<tr><td><i class="icon-comments"></i></td><td>Languages:</td><td><b>' . $profile['languages'] . '</b></td></tr>';
				}

				if($profile['position'] != "") {

					$profile['info_table'] .= '<tr><td><i class="icon-puzzle-piece"></i></td><td>Position:</td><td><b>' . $profile['position'] . '</b></td></tr>';
				}

				if($profile['working_since'] != "") {

					$profile['info_table'] .= '<tr><td><i class="icon-flag"></i></td><td>Working Since:</td><td><b>' . $profile['working_since'] . '</b></td></tr>';
				}

				if($profile['hometown'] != "" && $profile['country'] != "-1" && $profile['country'] != "") {
					
					$profile['info_table'] .= '<tr><td><i class="icon-map-marker"></i></td><td>From:</td><td><b>' . $profile['hometown'] . ', ' . $profile['country'] . '</b></td></tr>';
				
				} else if($profile['hometown'] != "") {

					$profile['info_table'] .= '<tr><td><i class="icon-map-marker"></i></td><td>From:</td><td><b>' . $profile['hometown'] . '</b></td></tr>';

				} else if($profile['country'] != "-1" && $profile['country'] != "") {

					$profile['info_table'] .= '<tr><td><i class="icon-map-marker"></i></td><td>From:</td><td><b>' . $profile['country'] . '</b></td></tr>';
				}


				if ($_GET['id']) {

					$profile['message_link'] = '<div class="spacer_10"></div><a class="btn light-blue" href="message?u_id=' . $user_id . '"><i class="icon-envelope"></i> Message ' . $profile['first_name'] . '</a><div class="spacer_20"></div>';

				} else {

					$profile['profile_settings_link'] = '<div class="spacer_10"></div><a class="btn light-blue" href="profile_settings"><i class="icon-cogs"></i> Profile Settings</a><div class="spacer_20"></div>';
				}


				if($profile['hobbies'] != "" || $profile['fav_books'] != "" || $profile['fav_movies'] != "" || $profile['fav_music'] != "") {

					$profile['interests'] = "<h3>Interests</h3>";

					if($profile['hobbies'] != "") {
						$profile['interests'] .= '<span class="interest_title">Hobbies</span>
						<ul class="unstyled inline sidebar-tags">';

						$hobbies = explode(",", $profile['hobbies']);

						foreach ($hobbies as $hobby) {

							$profile['interests'] .= '<li><span><i class="icon-tags"></i> ' . $hobby . '</span></li>';
						}

						$profile['interests'] .= '</ul>';
					}


					if($profile['fav_books'] != "") {
						$profile['interests'] .= '<span class="interest_title">Books</span>
						<ul class="unstyled inline sidebar-tags">';

						$books = explode(",", $profile['fav_books']);

						foreach ($books as $book) {

							$profile['interests'] .= '<li><span><i class="icon-tags"></i> ' . $book . '</span></li>';
						}

						$profile['interests'] .= '</ul>';
					}


					if($profile['fav_movies'] != "") {
						$profile['interests'] .= '<span class="interest_title">Movies</span>
						<ul class="unstyled inline sidebar-tags">';

						$movies = explode(",", $profile['fav_movies']);

						foreach ($movies as $movie) {

							$profile['interests'] .= '<li><span><i class="icon-tags"></i> ' . $movie . '</span></li>';
						}

						$profile['interests'] .= '</ul>';
					}


					if($profile['fav_music'] != "") {
						$profile['interests'] .= '<span class="interest_title">Music</span>
						<ul class="unstyled inline sidebar-tags">';

						$music = explode(",", $profile['fav_music']);

						foreach ($music as $song) {

							$profile['interests'] .= '<li><span><i class="icon-tags"></i> ' . $song . '</span></li>';
						}

						$profile['interests'] .= '</ul>';
					}
				}

				
				return $profile; 
		 	
		 	} else if($status == 'inactive') {

		 		header('Location: inactive_user');
		 	
		 	} else {

		 		header('Location: ' . $_SESSION['users_label_slang']);
		 	}
		
		}


		//Profile Settings Page....
		public function loadProfileSettings() {

			$account_id = $_SESSION['account_id'];
			$user_id = $_SESSION['user_id'];


			$query = $this->dbo->prepare("SELECT first_name, last_name, username, email, position, working_since, gender, dob, primary_phone, alternative_phone, website, about, image, hometown, country, hobbies, fav_books, fav_movies, fav_music, languages, lock_timeout, show_dob_year, timezone FROM users WHERE account_id = ? AND user_id = ? AND status = ?");
			$query->execute(array($account_id, $user_id, 'active'));

			$result = $query->fetchAll();

			foreach($result as $row) {

				$profile_settings['first_name'] = $row[0]; 
				$profile_settings['last_name'] = $row[1]; 
				$profile_settings['username'] = $row[2]; 
				$profile_settings['email'] = $row[3]; 
				$profile_settings['position'] = $row[4]; 
				$profile_settings['working_since'] = $row[5]; 
				$profile_settings['gender'] = $row[6]; 
				$profile_settings['dob'] = $row[7]; 
				$profile_settings['primary_phone'] = $row[8]; 
				$profile_settings['alternative_phone'] = $row[9]; 
				$profile_settings['website'] = $row[10]; 
				$profile_settings['about'] = $row[11]; 
				$profile_settings['image'] = "$row[12]"; 
				$profile_settings['hometown'] = $row[13]; 
				$profile_settings['country'] = $row[14];
				$profile_settings['hobbies'] = $row[15];
				$profile_settings['fav_books'] = $row[16];
				$profile_settings['fav_movies'] = $row[17];
				$profile_settings['fav_music'] = $row[18];
				$profile_settings['languages'] = $row[19];
				$profile_settings['lock_timeout'] = $row[20];
				$profile_settings['show_dob_year'] = $row[21];
				$profile_settings['timezone'] = $row[22];
				
				if($profile_settings['working_since'] != "0000-00-00") {
					$profile_settings['working_since_month'] = date("n", strtotime($profile_settings['working_since'])); 
					$profile_settings['working_since_year'] = date("Y", strtotime($profile_settings['working_since']));

					$profile_settings['working_since_month']--;

				}

				if($profile_settings['dob'] != "0000-00-00") {

					$profile_settings['dob_day'] = date("j", strtotime($profile_settings['dob'])); 
					$profile_settings['dob_month'] = date("n", strtotime($profile_settings['dob'])); 
					$profile_settings['dob_year'] = date("Y", strtotime($profile_settings['dob'])); 

					$profile_settings['dob_month']--; //offset for the dropdown array, index 0

				}

				$profile_settings['lock_timeout_opt'] = "";
				$lock_timeout_arr = array('-1' => 'Never', '5' => '5 Minutes', '15' => '15 Minutes', '30' => '30 Minutes', '60' => '1 Hour');

				foreach ($lock_timeout_arr as $key => $value) {
				
					if($key == $profile_settings['lock_timeout']) {	

						$profile_settings['lock_timeout_opt'] .= "<option value=\"$key\" selected=\"selected\">$value</option>";

					} else {

						$profile_settings['lock_timeout_opt'] .= "<option value=\"$key\">$value</option>";
					}
				
				}

			}

			return $profile_settings;

		}



		public function updatePersonalInfo() {

			require_once("classes/Main.class.php");
			$main = new Main();

			$account_id = $_SESSION['account_id'];
			$user_id = $_SESSION['user_id'];

			$first_name = $_POST['first_name'];
			$last_name = $_POST['last_name'];
			$position = $_POST['position'];
			$working_since_month = $_POST['working_since_month'];
			$working_since_year = $_POST['working_since_year'];
			$gender = $_POST['gender'];
			$dob_day = $_POST['dob_day'];
			$dob_month = $_POST['dob_month'];
			$dob_year = $_POST['dob_year'];
			$show_dob_year = $_POST['show_dob_year'];
			$email = $_POST['email'];
			$primary_phone = $_POST['primary_phone'];
			$alternative_phone = $_POST['alternative_phone'];
			$hometown = $_POST['hometown'];
			$country = $_POST['country'];
			$timezone = $_POST['timezone'];
			$website = $_POST['website'];
			$interests = $_POST['interests']; //interests = hobbies
			$fav_books = $_POST['fav_books'];
			$fav_movies = $_POST['fav_movies'];
			$fav_music = $_POST['fav_music'];
			$about = $_POST['about'];
			$languages = $_POST['languages'];


			if($interests == ",") {
				$interests = "";
			}

			if($fav_books == ",") {
				$fav_books = "";
			}

			if($fav_movies == ",") {
				$fav_movies = "";
			}

			if($fav_music == ",") {
				$fav_music = "";
			}

			if($languages == ",") {
				$languages = "";
			}


			$dob_month++; //offset +1 because of array index 0

			$dob = $dob_year . "-" . $dob_month . "-" . $dob_day;
			//Check if the date entered is valid?

			$working_since_month++; //offset +1 because of array index 0

			$working_since = $working_since_year . "-" . $working_since_month . "-10";


			$query = $this->dbo->prepare("UPDATE users SET first_name = ?, last_name = ?, position = ?, working_since = ?, gender = ?, dob = ?, show_dob_year = ?, 
				email = ?, primary_phone = ?, alternative_phone = ?, hometown = ?, country = ?, timezone = ?, website = ?, hobbies = ?, fav_books = ?, fav_movies = ?, fav_music = ?, about = ?, languages = ? WHERE account_id = ? AND user_id = ?");
			$query->execute(array($first_name, $last_name, $position, $working_since, $gender, $dob, $show_dob_year, $email, $primary_phone, $alternative_phone, $hometown, $country, $timezone, $website, $interests, $fav_books, $fav_movies, $fav_music, $about, $languages, $account_id, $user_id));
	

			$_SESSION['timezone'] = $timezone;
			$_SESSION['timezone_offset'] = $main->getTimezoneOffset($timezone); //get the timezone offset from the MAIN class.


			require_once("Account.class.php");
			$account = new Account();

			$account->addToTimeline("profile_updated", $user_id);


			header('Location: profile_settings#personal_info');		

		}




		public function selectProfilePicture() {

			require_once('scripts/aws.phar');
			
			$s3client = Aws\S3\S3Client::factory(array(
		    	'key'    => 'AKIAJKBMGCMLYBHN77PQ',
		    	'secret' => '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'
		    ));

			$account_id = $_SESSION['account_id'];

			$user_id = $_SESSION['user_id'];

			$image = $_POST['profile_picture'];

			if($image != "") {

				$image .= ".jpg";


				$query = $this->dbo->prepare("SELECT image FROM users WHERE account_id = ? AND user_id = ?");
				$query->execute(array($account_id, $user_id));

				$result = $query->fetchAll();

				foreach($result as $row) {

					$old_image = $row[0];
				}


				$stock_images = array('beach.jpg','bees.jpg','boats.jpg','bridge.jpg','broken_car.jpg','cat.jpg','coconut.jpg','dog.jpg','field.jpg','flower.jpg', 'fruits.jpg', 'horses.jpg', 'kitten.jpg', 'lights.jpg', 'rocks.jpg', 'sunflowers.jpg', 'waterfall.jpg', 'yellow_lights.jpg' );

				if(!in_array($old_image, $stock_images)) {

					$s3client->deleteObject(array(
					    'Bucket' => 'ironvault',
						'Key' => 'pr_imgs/' . $old_image
					));
				}


				$query = $this->dbo->prepare("UPDATE users SET image = ? WHERE account_id = ? AND user_id = ?");
				$result = $query->execute(array($image, $account_id, $user_id));

				$_SESSION['image'] = 'https://cdn.ironvault.ca/pr_imgs/' . $image;
				$_SESSION['image_thumb'] = 'https://cdn.ironvault.ca/pr_imgs/thumbs/' . $image;

				require_once("Account.class.php");
				$account = new Account();

				$account->addToTimeline("profile_picture_updated", $user_id);
			}

			header('Location: profile_settings#profile_picture');

		}




		// public function uploadPhoto() {

		// 	require_once('scripts/aws.phar');
			
		// 	$s3client = Aws\S3\S3Client::factory(array(
		//     	'key'    => 'AKIAJKBMGCMLYBHN77PQ',
		//     	'secret' => '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'
		//     ));

	
		// 	if (($_FILES["image_file"]["type"] == "image/jpeg") || ($_FILES["image_file"]["type"] == "image/jpg") || ($_FILES["image_file"]["type"] == "image/png")) {

		// 		$stock_images = array('beach.jpg','bees.jpg','boats.jpg','bridge.jpg','broken_car.jpg','cat.jpg','coconut.jpg','dog.jpg','field.jpg','flower.jpg', 'fruits.jpg', 'horses.jpg', 'kitten.jpg', 'lights.jpg', 'rocks.jpg', 'sunflowers.jpg', 'waterfall.jpg', 'yellow_lights.jpg' );

		// 		$account_id = $_SESSION['account_id'];
		// 		$user_id = $_SESSION['user_id'];
				
		// 		$image_type = $_FILES["image_file"]["type"];
		// 		$image_file = $_FILES["image_file"]["tmp_name"];


		// 		$query = $this->dbo->prepare("SELECT image FROM users WHERE account_id = ? AND user_id = ?");
		// 		$query->execute(array($account_id, $user_id));

		// 		$result = $query->fetchAll();

		// 		foreach($result as $row) {

		// 			$old_image = $row[0];
		// 		}

				
		// 		$filename = $account_id . "_" . $user_id . "_" . time() . ".jpg";

		// 		$this->cropThumbnail($image_file, $filename, $image_type);
		// 		$this->imageResize($image_file, $filename, $image_type);

		// 		//Store the main image...
		// 		$s3client->putObject(array(
		// 		    'ACL' => 'public-read',
		// 		    'Bucket' => 'ironvault',
		// 			'SourceFile' => 'tmp_files/' . $filename,
		// 			'Key' => 'pr_imgs/' . $filename,
		// 			'ContentType' => 'image/jpeg',
		// 			'StorageClass' => 'REDUCED_REDUNDANCY'
		// 		));

		// 		//Store the thumb image...
		// 		$s3client->putObject(array(
		// 		    'ACL' => 'public-read',
		// 		    'Bucket' => 'ironvault',
		// 			'SourceFile' => 'tmp_files/thumb_' . $filename,
		// 			'Key' => 'pr_imgs/thumbs/' . $filename,
		// 			'ContentType' => 'image/jpeg',
		// 			'StorageClass' => 'REDUCED_REDUNDANCY'
		// 		));


		// 		//Then save the image filename into the DB
		// 		$query = $this->dbo->prepare("UPDATE users SET image = ? WHERE account_id = ? AND user_id = ?");
		// 		$query->execute(array($filename, $account_id, $user_id));

		// 		$_SESSION['image'] = 'https://cdn.ironvault.ca/pr_imgs/' . $filename;
		// 		$_SESSION['image_thumb'] = 'https://cdn.ironvault.ca/pr_imgs/thumbs/' . $filename;


		// 		require_once("Account.class.php");
		// 		$account = new Account();

		// 		$account->addToTimeline("profile_picture_updated", $user_id);


		// 		if(!in_array($old_image, $stock_images)) {

		// 			$s3client->deleteObject(array(
		// 			    'Bucket' => 'ironvault',
		// 				'Key' => 'pr_imgs/' . $old_image
		// 			));

		// 			$s3client->deleteObject(array(
		// 			    'Bucket' => 'ironvault',
		// 				'Key' => 'pr_imgs/thumbs/' . $old_image
		// 			));
		// 		}

			
		// 		header('Location: profile_settings#profile_picture');
				
		// 	} else {
		// 		header('Location: profile_settings#profile_picture?e=wrong_filetype');
			
		// 		//return "File must be of type JPG/JPEG or PNG"; //not working :( cannot pass it after header()
		// 	}
		// }



		public function handleProfilePictureUpload($filename) { //Called from the UploadHandler.php

			$stock_images = array('beach.jpg','bees.jpg','boats.jpg','bridge.jpg','broken_car.jpg','cat.jpg','coconut.jpg','dog.jpg','field.jpg','flower.jpg', 'fruits.jpg', 'horses.jpg', 'kitten.jpg', 'lights.jpg', 'rocks.jpg', 'sunflowers.jpg', 'waterfall.jpg', 'yellow_lights.jpg' );

            $account_id = $_SESSION['account_id'];
            $user_id = $_SESSION['user_id'];


			$query = $this->dbo->prepare("SELECT image FROM users WHERE account_id = ? AND user_id = ?");
			$query->execute(array($account_id, $user_id));

			$result = $query->fetchAll();

			foreach($result as $row) {

				$old_image = $row[0];
			}


			//Then save the image filename into the DB
			$query = $this->dbo->prepare("UPDATE users SET image = ? WHERE account_id = ? AND user_id = ?");
			$query->execute(array($filename, $account_id, $user_id));

			$_SESSION['image'] = 'https://cdn.ironvault.ca/pr_imgs/' . $filename;
			$_SESSION['image_thumb'] = 'https://cdn.ironvault.ca/pr_imgs/thumbs/' . $filename;


			require_once("Account.class.php");
			$account = new Account();

			$account->addToTimeline("profile_picture_updated", $user_id);


			if(!in_array($old_image, $stock_images)) {

				require_once('scripts/aws.phar');
		
				$s3client = Aws\S3\S3Client::factory(array(
			    	'key'    => 'AKIAJKBMGCMLYBHN77PQ',
			    	'secret' => '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'
			    ));

				$s3client->deleteObject(array(
				    'Bucket' => 'ironvault',
					'Key' => 'pr_imgs/' . $old_image
				));

				$s3client->deleteObject(array(
				    'Bucket' => 'ironvault',
					'Key' => 'pr_imgs/thumbs/' . $old_image
				));
			}

			
			return true;
			//header('Location: profile_settings#profile_picture');
		}



		// public function imageResize($image_file, $filename, $type) {
		
		// 	if(file_exists($image_file)) {
			
		// 		$max_width = 800;
		// 		$max_height = 800;
					
		// 		$path = "tmp_files/" . $filename;
				
		// 		// if you upload a png you can use imagecreatefrompng
		// 		if($type == "image/jpeg" || $type == "image/jpg") {
				
		// 			$image = imagecreatefromjpeg($image_file);
		// 			$srcwidth = ImagesX($image);
		// 			$srcheight = ImagesY($image);
		// 			if ($srcwidth < $max_width && $srcheight < $max_height) {
						
		// 				// smaller than needed and will upload directly
		// 				ImageJpeg($image, $path, 80);
						
		// 				return $path;
						
		// 			} else {
		// 				// resize acording to aspect ratio
		// 				if (($srcwidth/$srcheight) > ($max_width/$max_height)) {
		// 					// assume that the width is bigger
		// 					$new_width = $max_width;
		// 					$new_height = round($max_width/$srcwidth*$srcheight);
		// 				} else {
		// 					// assume the height is bigger
		// 					$new_width = round($max_height/$srcheight*$srcwidth);
		// 					$new_height = $max_height;
		// 				}
		// 				$new_image = ImageCreateTrueColor($new_width, $new_height);

		// 				ImageCopyResampled($new_image, $image, 0, 0, 0, 0, $new_width, $new_height, $srcwidth, $srcheight);
						
		// 				// you can also use Imagepng
		// 				ImageJpeg($new_image, $path, 80);
						
		// 				//destroy tmp jpeg
		// 				ImageDestroy($image);
		// 				ImageDestroy($new_image);
						
		// 				return $path;
		// 			}
					
		// 		} else if($type == "image/png") {
				
		// 			$image = imagecreatefrompng($image_file);
		// 			$srcwidth = ImagesX($image);
		// 			$srcheight = ImagesY($image);
		// 			if ($srcwidth < $max_width && $srcheight < $max_height) {
						
		// 				// smaller than needed and will upload directly
		// 				ImageJpeg($image, $path, 80);
						
		// 				return $path;
						
		// 			} else {
		// 				// resize acording to aspect ratio
		// 				if (($srcwidth/$srcheight) > ($max_width/$max_height)) {
		// 					// assume that the width is bigger
		// 					$new_width = $max_width;
		// 					$new_height = round($max_width/$srcwidth*$srcheight);
		// 				} else {
		// 					// assume the height is bigger
		// 					$new_width = round($max_height/$srcheight*$srcwidth);
		// 					$new_height = $max_height;
		// 				}
		// 				$new_image = ImageCreateTrueColor($new_width, $new_height);

		// 				ImageCopyResampled($new_image, $image, 0, 0, 0, 0, $new_width, $new_height, $srcwidth, $srcheight);
						
		// 				// you can also use Imagepng
		// 				ImageJpeg($new_image, $path, 80);
						
		// 				//destroy tmp jpeg
		// 				ImageDestroy($image);
		// 				ImageDestroy($new_image);
						
		// 				return $path;
		// 			}
				
		// 		}
				
		// 	} else { 
				
		// 	}
		// }




		// public function cropThumbnail($image_file, $filename, $type) {
		
		// 	if($type == "image/jpeg" || $type == "image/jpg") {
			
		// 		$image = imagecreatefromjpeg($image_file);
		// 		$path = "tmp_files/thumb_" . $filename;

		// 		$thumb_width = 200;
		// 		$thumb_height = 200;

		// 		$width = imagesx($image);
		// 		$height = imagesy($image);

		// 		$original_aspect = $width / $height;
		// 		$thumb_aspect = $thumb_width / $thumb_height;

		// 		if($original_aspect >= $thumb_aspect) {
		// 		   // If image is wider than thumbnail (in aspect ratio sense)
		// 		   $new_height = $thumb_height;
		// 		   $new_width = $width / ($height / $thumb_height);
		// 		} else {
		// 		   // If the thumbnail is wider than the image
		// 		   $new_width = $thumb_width;
		// 		   $new_height = $height / ($width / $thumb_width);
		// 		}

		// 		$thumb = imagecreatetruecolor($thumb_width, $thumb_height);

		// 		// Resize and crop
		// 		imagecopyresampled($thumb, $image, 0 - ($new_width - $thumb_width) / 2, 0 - ($new_height - $thumb_height) / 2, 0, 0, $new_width, $new_height, $width, $height);
		// 		imagejpeg($thumb, $path, 80);
				
		// 		ImageDestroy($image);
		// 		ImageDestroy($thumb);
			
		// 	} else if($type == "image/png") {
			
		// 		$image = imagecreatefrompng($image_file);
		// 		$path = "tmp_files/thumb_" . $filename;

		// 		$thumb_width = 200;
		// 		$thumb_height = 200;

		// 		$width = imagesx($image);
		// 		$height = imagesy($image);

		// 		$original_aspect = $width / $height;
		// 		$thumb_aspect = $thumb_width / $thumb_height;

		// 		if($original_aspect >= $thumb_aspect) {
		// 		   // If image is wider than thumbnail (in aspect ratio sense)
		// 		   $new_height = $thumb_height;
		// 		   $new_width = $width / ($height / $thumb_height);
		// 		} else {
		// 		   // If the thumbnail is wider than the image
		// 		   $new_width = $thumb_width;
		// 		   $new_height = $height / ($width / $thumb_width);
		// 		}

		// 		$thumb = imagecreatetruecolor($thumb_width, $thumb_height);

		// 		// Resize and crop
		// 		imagecopyresampled($thumb, $image, 0 - ($new_width - $thumb_width) / 2, 0 - ($new_height - $thumb_height) / 2, 0, 0, $new_width, $new_height, $width, $height);
		// 		imagejpeg($thumb, $path, 80);
				
		// 		ImageDestroy($image);
		// 		ImageDestroy($thumb);
			
		// 	}
		// }




		public function changePassword() {

			$account_id = $_SESSION['account_id'];

			$user_id = $_SESSION['user_id'];

			$current_password = hash('SHA512', $_POST['current_password']);

			$new_password = $_POST['new_password'];

			$new_password2 = $_POST['new_password2'];


			$query = $this->dbo->prepare("SELECT password FROM users WHERE account_id = ? AND user_id = ? AND status = ?");
			$query->execute(array($account_id, $user_id, 'active'));

			$result = $query->fetchAll();

			foreach($result as $row) {

	    		$password = $row[0];

			}

			if($current_password == $password) {

				if($new_password == $new_password2) {

					if (preg_match('/(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/', $new_password)) {

						$new_password = hash('SHA512', $new_password);

						$query = $this->dbo->prepare("UPDATE users SET password = ? WHERE account_id = ? AND user_id = ?");
						$query->execute(array($new_password, $account_id, $user_id));


				    	header('Location: profile_settings?pwd_e=success#security');

					} else {

						header('Location: profile_settings?pwd_e=insecure_password#security');
					}

				} else {

					header('Location: profile_settings?pwd_e=password_match_failed#security');
				}

			} else {

				header('Location: profile_settings?pwd_e=wrong_password#security');

			}

			//header('Location: profile_settings#security');

		}



		public function setScreenLock() {

			$account_id = $_SESSION['account_id'];

			$user_id = $_SESSION['user_id'];

			$screen_lock_timeout = $_POST['screen_lock_timeout'];

			if($screen_lock_timeout != "") {


				$query = $this->dbo->prepare("UPDATE users SET lock_timeout = ? WHERE account_id = ? AND user_id = ?");
				$query->execute(array($screen_lock_timeout, $account_id, $user_id));


		    	$_SESSION['lock_timeout'] = $screen_lock_timeout;


		    	header('Location: profile_settings?#security');
		    }
		}



		public function toggleSound($state) {

	    	$account_id = $_SESSION['account_id'];

	    	$user_id = $_SESSION['user_id'];

	    	$query = $this->dbo->prepare("UPDATE users SET sound = ? WHERE account_id = ? AND user_id = ?");
			$query->execute(array($state, $account_id, $user_id));

	    	$_SESSION['sound'] = $state;

	    }


	    public function togglePageSidebar() {

	    	$account_id = $_SESSION['account_id'];

	    	$user_id = $_SESSION['user_id'];

	    	$state = $_POST['state'];

	    	$query = $this->dbo->prepare("UPDATE users SET page_sidebar_closed = ? WHERE account_id = ? AND user_id = ?");
			$query->execute(array($state, $account_id, $user_id));

	    	$_SESSION['page_sidebar_closed'] = $state;
	    }


	    public function checkLastActive() {

	    	$blurred_timestamp = $_POST['timestamp'];

	    	$last_timestamp = $_SESSION['last_active'];

	    	if($last_timestamp != "") {

	    		if($last_timestamp > $blurred_timestamp) {
	    			// The user is active
	    			return "active";

	    		} else {
	    			//user is inactive
	    			return "inactive";
	    		}

	    	} else {

	    		//user is active
	    		return "active";
	    	}
	    }



	    public function updateLastActive() {

	    	$new_timestamp = $_POST['timestamp'];

	    	$_SESSION['last_active'] = $new_timestamp;

	    }


	}


?>