var express = require("express");
var https = require('https');
var fs = require('fs');
var socketio = require('socket.io');

var privatekey = fs.readFileSync('/usr/local/ssl/private/private.key', 'utf8').toString();
var certificate = fs.readFileSync('/usr/local/ssl/crt/certificate.crt', 'utf8').toString();
var intermediate = fs.readFileSync('/usr/local/ssl/crt/intermediate.crt', 'utf8').toString();

var options = {
    key: privatekey,
    cert: certificate,
    ca: intermediate
}

var app = express();

var server = https.createServer(options, app);

var io = socketio.listen(server);

io.enable('browser client minification');  // send minified client
io.enable('browser client etag');          // apply etag caching logic based on version number
io.enable('browser client gzip');          // gzip the file
io.set('log level', 1); 

server.listen(8080);


io.sockets.on('connection', function (socket) {

	socket.on('new connection', function(account_id){ 
        socket.join(account_id);
    });


    //socket.emit('message', { message: 'Welcome to Open Chat' });
    socket.on('send', function (data) {

    	var message = data.message;
    	var first_name = data.first_name;
    	var last_name = data.last_name;
    	var image = data.image;
    	var user_id = data.user_id;
    	var account_id = data.account_id;


        var message_object = new Object;

        message_object.message = message;
        message_object.first_name = first_name;
        message_object.last_name = last_name;
        message_object.image = image;
        message_object.user_id = user_id;

        io.sockets.in(account_id).emit('message', message_object)
        //io.sockets.emit('message', message_object);
    });

	
	socket.on('disconnect', function(){
        //socket.leave(account_id);
    });

});
