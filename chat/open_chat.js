$(document).ready(function() {
 
    var socket = io.connect('https://www.ironvault.ca:8080', {'secure': true, 'reconnect': true, 'reconnection delay': 1000});
    
    var user_id = $('.chat-form .user_id').val();
    var li_class;

    socket.on('message', function (data) {

        if(data.message) {


            var month=new Array();
            month[0]="Jan";
            month[1]="Feb";
            month[2]="Mar";
            month[3]="Apr";
            month[4]="May";
            month[5]="Jun";
            month[6]="Jul";
            month[7]="Aug";
            month[8]="Sep";
            month[9]="Oct";
            month[10]="Nov";
            month[11]="Dec";

            var datetime = new Date();

            var cur_date = datetime.getDate();
            var cur_month = month[datetime.getMonth()];
            var cur_year = datetime.getFullYear();
            var cur_hour = datetime.getHours(); //gmt_offset goes here
            var cur_minute = datetime.getMinutes();

            var ampm = "AM";

            if(cur_minute < 10) {
                cur_minute = "0" + cur_minute;
            }

            if (cur_hour > 12) {
                cur_hour -= 12;
                ampm = "PM";
            } else if (cur_hour === 0) {
               cur_hour = 12;
               //Stays as AM.
            }

            var date_string = cur_month + " " + cur_date + ", " + cur_year;
            var time_string =  cur_hour + ":" + cur_minute;



            if(data.user_id == user_id) {

                li_class = "out";
            } else {

                li_class = "in";
            }
            
            var html = '<li class="' + li_class + '" id="' + data.user_id + '">' +

                     '<img class="avatar" alt="" src="' + data.image + '" />' +

                     '<div class="message">' +

                        '<span class="arrow"></span>' +

                        '<a href="#" class="name">' + data.first_name + ' ' + data.last_name + '</a>' +

                        '<span class="datetime"> on ' + date_string + ' at ' + time_string + ampm + '</span>' +

                        '<span class="body">' +

                        data.message + 

                        '</span>' +

                     '</div>' +

                  '</li>';


            $('.chats').append(html);



            div_height = $('#chats .chats').height();

            $('#chats .scroller').slimScroll({

                scrollTo: div_height
            });


            if($('.btn-sound').hasClass("on")) {
                
                sound.play();
            }


        } else {
            console.log("There is a problem:", data);
        }
    });
 


    var message_text;

    var first_name = $('.chat-form .first_name').val();
    var last_name = $('.chat-form .last_name').val();
    var image = $('.chat-form .image').val();
    var user_id = $('.chat-form .user_id').val();
    var account_id = $('.chat-form .account_id').val();

    var message_object = new Object();


    socket.emit('new connection', account_id);


    $('.send_message').click(function() {
        message_text = stripHTML($(".message_text").val()); //stripHTML is a function
        message_text = message_text.replace(/\n/g, '<br />');

        message_text = message_text.trim();

        if(message_text != "") {
            message_text = linkify(message_text);

            message_object.message = message_text;
            message_object.first_name = first_name;
            message_object.last_name = last_name;
            message_object.image = image;
            message_object.user_id = user_id;
            message_object.account_id = account_id;

            socket.emit('send', message_object);
        }
    });



    $(document).keydown(function(e) {

        var code = (e.keyCode ? e.keyCode : e.which);

        if (code == 13) {

            if($('.message_text').is(':focus')) {
                message_text = stripHTML($(".message_text").val()); //stripHTML is a function
                message_text = message_text.replace(/\n/g, '<br />');

                message_text = message_text.trim();

                if(message_text != "") {
                    message_text = linkify(message_text);

                    message_object.message = message_text;
                    message_object.first_name = first_name;
                    message_object.last_name = last_name;
                    message_object.image = image;
                    message_object.user_id = user_id;
                    message_object.account_id = account_id;

                    socket.emit('send', message_object);

                }
            }
        }
    });


    function linkify(text) {  
        var urlRegex =/(\b(https?:\/\/|ftp:\/\/|file:\/\/|www.)[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;  
        return text.replace(urlRegex, function(url) {  

            var http_url; 

            if(/~^(?:f|ht)tps?:/i.test(url)) {
                
                http_url = url;
            
            } else {

                http_url = "http://" + url;
            }
                
            return '<a href="' + http_url + '" target="_blank">' + url + '</a>';  
        })  
    }


    function stripHTML(string) {
       var tmp = document.createElement("DIV");
       tmp.innerHTML = string;
       return tmp.textContent || tmp.innerText || "";
    }
 
});
