<?php
	
	session_start();

	require_once('error_handler.php');


	// ===============================================
	//These are actions which do no require the user to be logged in....
	if($_POST['action'] == "check_email") { //checking if email is available for signup...

		require_once("classes/Account.class.php");
		$account = new Account();

		$email = $_POST['email'];

		$result = $account->checkEmail($email);

		echo $result;

	} else if($_POST['action'] == "add_to_mailing_list") {

		require_once("classes/Account.class.php");

		$account = new Account();

		$result = $account->addToMailingList();

		echo $result;

	} else if($_POST['action'] == "business_pro_signup") {

		require_once("classes/Account.class.php");

		$account = new Account();

		$result = $account->signUp();

		echo $result;

	} else if($_POST['action'] == "load_timezone_select") {

		require_once("classes/Account.class.php");
		$account = new Account();

		$result = $account->loadTimezoneSelect();

		echo $result;

	} else if($_POST['action'] == "check_username_within_account") {

		require_once("classes/Account.class.php");
		$account = new Account();

		$result = $account->checkUsernameWithinAccount();

		echo $result;

	} else if($_POST['action'] == "get_new_user_profile_thumbnail") {

		require_once("classes/User.class.php");
		$user = new User();

		$result = $user->getProfileThumbnail();

		echo $result;

	} else if($_POST['action'] == "add_invited_user") {

		require_once("classes/Account.class.php");
		$account = new Account();

		$result = $account->addInvitedUser();

		echo $result;

	} else if($_POST['action'] == "check_session") {

		if($_SESSION['status'] == "active") {

			echo 'active';
		
		} else if($_SESSION['status'] == "locked") {

			echo 'locked';
		
		} else {

			echo 'inactive';
		}

	} else if($_POST['action'] == "lock_session") {

		require_once("classes/User.class.php");
		$user = new User();

		$result = $user->lockSession();

		echo $result;

	} else if($_POST['action'] == "unlock_session") {

		require_once("classes/User.class.php");
		$user = new User();

		$result = $user->overlayUnlock();

		echo $result;
	}



	// ==============================================



	// ==============================================
	// These are actions that require the user to be logged in...

	if($_SESSION['status'] == "active") { //if the user is logged in...

		if($_POST['action'] == "search") {


			require_once("classes/Search.class.php");

			$search = new Search();

			$query = $_POST['query'];
			$filter = $_POST['filter'];
			$instance = $_POST['instance'];

			$result = $search->search($query, $filter);


			echo $result;


		} else if($_POST['action'] == "get_users_json") {

			require_once("classes/Account.class.php");
			$account = new Account();

			$result = $account->getUsersJson();

			echo $result;


		} else if($_POST['action'] == "send_message") {



			require_once("classes/Chat.class.php");

			$chat = new Chat();


			$result = $chat->postNewMessage();


			echo $result;


			//echo $chat->getNewMessages($last_id);



		} else if($_POST['action'] == "get_messages") {



			require_once("classes/Chat.class.php");

			$chat = new Chat();



			$result = $chat->getMessages();

			echo $result;

		
		} else if($_POST['action'] == "load_earlier_messages") {



			require_once("classes/Chat.class.php");

			$chat = new Chat();



			$result = $chat->getMessages();

			echo $result;

		
		} else if($_POST['action'] == "init_new_conversation") {
			//This is run when a new conversation is being SENT. Handles adding a new conversation row and message to DB

			require_once("classes/Chat.class.php");

			$chat = new Chat();


			$result = $chat->initNewConversation();

			echo $result;

		
		} else if($_POST['action'] == "new_conversation") {
			//This is run to show the blank conversation HTML to the user. No initialization done here.

			require_once("classes/Chat.class.php");

			$chat = new Chat();


			$result = $chat->newConversation();

			echo $result;

		
		} else if($_POST['action'] == "load_conversation") {


			require_once("classes/Chat.class.php");

			$chat = new Chat();


			$result = $chat->loadConversation();

			echo $result;

		
		} else if($_POST['action'] == "reload_conversation_list") {


			require_once("classes/Chat.class.php");

			$chat = new Chat();


			$result = $chat->reloadConversationList();

			echo $result;

		
		} else if($_POST['action'] == "hide_conversation") {


			require_once("classes/Chat.class.php");

			$chat = new Chat();


			$result = $chat->hideConversation();

			echo $result;

		
		} else if($_POST['action'] == "send_private_message") {


			require_once("classes/Chat.class.php");

			$chat = new Chat();


			$result = $chat->sendPrivateMessage();

			echo $result;

		
		} else if($_POST['action'] == "toggle_sound") {

			require_once("classes/User.class.php");
			$user = new User();


			$state = $_POST['state']; 

			$user->toggleSound($state);

		
		} else if($_POST['action'] == "toggle_disable_account") {

			require_once("classes/Account.class.php");
			$account = new Account();

			$account->toggleDisableAccount($state);

		
		} else if($_POST['action'] == "load_dashboard_counters") {

			require_once("classes/Account.class.php");
			$account = new Account();

			$result = $account->loadDashboardCounters();

			echo $result;

		
		} else if($_POST['action'] == "load_income_expenses_graph") {

			require_once("classes/Account.class.php");
			$account = new Account();

			$result = $account->loadIncomeExpensesGraph();

			echo $result;

		
		} else if($_POST['action'] == "add_location") {

			require_once("classes/Account.class.php");

			$account = new Account();

			$result = $account->addLocation();

			echo $result;


		} else if($_POST['action'] == "remove_location") {

			require_once("classes/Account.class.php");

			$account = new Account();

			$result = $account->removeLocation();

			echo $result;


		} else if($_POST['action'] == "filter_locations") {

			require_once("classes/Account.class.php");

			$account = new Account();

			$result = $account->filterLocations();

			echo $result;


		} else if($_POST['action'] == "toggle_page_sidebar") {

			require_once("classes/User.class.php");
			$user = new User();

			$result = $user->togglePageSidebar();

			echo $result;
		
		} else if($_POST['action'] == "load_schedule") {

			require_once("classes/Account.class.php");

			$account = new Account();

			$result = $account->loadSchedule();

			echo $result;

		} else if($_POST['action'] == "add_event") {

			require_once("classes/Account.class.php");

			$account = new Account();

			$result = $account->addEvent();

			echo $result;

		} else if($_POST['action'] == "update_event") {

			require_once("classes/Account.class.php");

			$account = new Account();

			$result = $account->updateEvent();

			echo $result;

		} else if($_POST['action'] == "delete_event") {

			require_once("classes/Account.class.php");

			$account = new Account();

			$result = $account->deleteEvent();

			echo $result;

		} else if($_POST['action'] == "load_products") {

			require_once("classes/Inventory.class.php");

			$inventory = new Inventory();

			$result = $inventory->loadProducts();

			echo $result['table'];
		
		} else if($_POST['action'] == "update_category") {

			require_once("classes/Inventory.class.php");

			$inventory = new Inventory();

			$result = $inventory->updateCategory();

			echo $result;

		} else if($_POST['action'] == "delete_category") {

			require_once("classes/Inventory.class.php");

			$inventory = new Inventory();

			$result = $inventory->deleteCategory();

			echo $result;
		
		} else if($_POST['action'] == "add_category") {

			require_once("classes/Inventory.class.php");

			$inventory = new Inventory();

			$result = $inventory->addCategory();

			echo $result;
		
		} else if($_POST['action'] == "get_categories") {

			require_once("classes/Inventory.class.php");

			$inventory = new Inventory();

			$result = $inventory->getCategories();

			echo $result;


		} else if($_POST['action'] == "remove_logo") {

			require_once("classes/Account.class.php");

			$account = new Account();

			$result = $account->removeLogo();

			echo $result;
		
		} else if($_POST['action'] == "apply_invoice_discount") {

			require_once("classes/Invoice.class.php");

			$invoice = new Invoice();

			$result = $invoice->applyInvoiceDiscount();

			echo $result;
		
		} else if($_POST['action'] == "remove_invoice_discount") {

			require_once("classes/Invoice.class.php");

			$invoice = new Invoice();

			$result = $invoice->removeInvoiceDiscount();

			echo $result;
		
		} else if($_POST['action'] == "apply_recurring_invoice_discount") {

			require_once("classes/Invoice.class.php");

			$invoice = new Invoice();

			$result = $invoice->applyRecurringInvoiceDiscount();

			echo $result;
		
		} else if($_POST['action'] == "remove_recurring_invoice_discount") {

			require_once("classes/Invoice.class.php");

			$invoice = new Invoice();

			$result = $invoice->removeRecurringInvoiceDiscount();

			echo $result;
		
		} else if($_POST['action'] == "delete_user_account") {

			require_once("classes/Account.class.php");

			$account = new Account();

			$result = $account->deleteUserAccount();

			echo $result;
		
		} else if($_POST['action'] == "check_last_active") {

			require_once("classes/User.class.php");
			$user = new User();

			$result = $user->checkLastActive();

			echo $result;
		
		} else if($_POST['action'] == "update_last_active") {

			require_once("classes/User.class.php");
			$user = new User();

			$result = $user->updateLastActive();

			echo $result;
		
		} else if($_POST['action'] == "load_product_qr") {

			require_once("classes/Inventory.class.php");

			$inventory = new Inventory();

			$result = $inventory->loadProductQR();

			echo $result;
		
		} else if($_POST['action'] == "remove_product_image") {

			require_once("classes/Inventory.class.php");

			$inventory = new Inventory();

			$result = $inventory->removeProductImage();

			echo $result;
		
		} else if($_POST['action'] == "delete_item") {

			require_once("classes/Inventory.class.php");

			$inventory = new Inventory();

			$result = $inventory->deleteInventoryItem();

			echo $result;

		} else if($_POST['action'] == "save_notes") {

			require_once("classes/Account.class.php");

			$account = new Account();

			$result = $account->saveUserNotes();

			echo $result;

		} else if($_POST['action'] == "delete_note") {

			require_once("classes/Account.class.php");

			$account = new Account();

			$result = $account->deleteUserNote();

			echo $result;

		} 
		// else if($_POST['action'] == "reload_tasks_list") {

		// 	require_once("classes/Account.class.php");

		// 	$account = new Account();

		// 	$result = $account->loadTasksList();

		// 	echo $result;

		// } 
		else if($_POST['action'] == "load_tasks_list_dropdown") {

			require_once("classes/Account.class.php");

			$account = new Account();

			$result = $account->loadTasksListDropdown();

			echo $result;

		} else if($_POST['action'] == "add_task") {

			require_once("classes/Account.class.php");

			$account = new Account();

			$result = $account->addTask();

			echo $result;

		} else if($_POST['action'] == "update_task") {

			require_once("classes/Account.class.php");

			$account = new Account();

			$result = $account->updateTask();

			echo $result;

		} else if($_POST['action'] == "save_tasks") {

			require_once("classes/Account.class.php");

			$account = new Account();

			$result = $account->saveTasks();

			echo $result;

		} else if($_POST['action'] == "mark_tasks_as_complete") {

			require_once("classes/Account.class.php");

			$account = new Account();

			$result = $account->markTasksAsComplete();

			echo $result;

		} else if($_POST['action'] == "delete_tasks") {

			require_once("classes/Account.class.php");

			$account = new Account();

			$result = $account->deleteTasks();

			echo $result;

		} else if($_POST['action'] == "create_invoice_load_client_info") {

			require_once("classes/Invoice.class.php");

			$invoice = new Invoice();

			$result = $invoice->createInvoiceLoadClientInfo();

			echo $result;

		} else if($_POST['action'] == "verify_user") {

			require_once("classes/Account.class.php");

			$account = new Account();

			$result = $account->verifyUser();

			echo $result;

		} else if($_POST['action'] == "add_notification") {

			require_once("classes/User.class.php");

			$user = new User();

			$result = $user->addNotification();

			echo $result;

		} else if($_POST['action'] == "update_notifications_dropdown") {

			require_once("classes/User.class.php");

			$user = new User();

			$which = $_POST['which'];

			$result = $user->updateNotificationsDropdown($which);

			echo $result;

		} else if($_POST['action'] == "remove_message_notification") { //For single convo

			require_once("classes/User.class.php");

			$user = new User();

			$result = $user->removeMessageNotification();

			echo $result;

		} else if($_POST['action'] == "clear_messages_notifications") { //Clear all convos

			require_once("classes/User.class.php");

			$user = new User();

			$result = $user->clearMessagesNotifications();

			echo $result;

		} else if($_POST['action'] == "clear_events_notifications") {

			require_once("classes/User.class.php");

			$user = new User();

			$result = $user->clearEventsNotifications();

			echo $result;

		} else if($_POST['action'] == "clear_tasks_notifications") {

			require_once("classes/User.class.php");

			$user = new User();

			$result = $user->clearTasksNotifications();

			echo $result;

		} else if($_POST['action'] == "calculate_storage_cap") {

			require_once("classes/Storage.class.php");
			$storage = new Storage();

			$result = $storage->calculateStorageCap('');

			echo $result;

		} else if($_POST['action'] == "get_storage_quota") {

			require_once("classes/Storage.class.php");
			$storage = new Storage();

			$result = $storage->getStorageQuota('');

			echo $result;

		} else if($_POST['action'] == "imap_connect") {

			require_once("classes/Account.class.php");
			$account = new Account();

			$result = $account->imapConnect();

			echo $result;

		} else if($_POST['action'] == "imap_fetch") {

			require_once("classes/Account.class.php");
			$account = new Account();

			$result = $account->imapFetch();

			echo $result;

		} else if($_POST['action'] == "imap_fetch_from_db") {

			require_once("classes/Account.class.php");
			$account = new Account();

			$result = $account->imapFetchFromDB();

			echo $result;

		} else if($_POST['action'] == "imap_clear_emails") {

			require_once("classes/Account.class.php");
			$account = new Account();

			$result = $account->imapClearEmails();

			echo $result;

		} else if($_POST['action'] == "imap_disconnect") {

			require_once("classes/Account.class.php");
			$account = new Account();

			$result = $account->imapDisconnect();

			echo $result;

		} else if($_POST['action'] == "add_client") {

			require_once("classes/Client.class.php");
			$client = new Client();

			$result = $client->addClient();

			echo $result;

		} else if($_POST['action'] == "delete_client") {

			require_once("classes/Client.class.php");
			$client = new Client();

			$result = $client->deleteClient();

			echo $result;

		} else if($_POST['action'] == "save_client_notes") {

			require_once("classes/Client.class.php");
			$client = new Client();

			$result = $client->saveClientNotes();

			echo $result;

		} else if($_POST['action'] == "reload_client_files") {

			require_once("classes/Client.class.php");
			$client = new Client();

			$result = $client->loadClientFiles();

			echo $result;

		} else if($_POST['action'] == "delete_client_file") {

			require_once("classes/Client.class.php");
			$client = new Client();

			$result = $client->deleteClientFile();

			echo $result;

		} else if($_POST['action'] == "reload_invoice_files") {

			require_once("classes/Invoice.class.php");
			$invoice = new Invoice();

			$result = $invoice->loadInvoiceFiles('single_invoice');

			echo $result;

		} else if($_POST['action'] == "reload_recurring_invoice_files") {

			require_once("classes/Invoice.class.php");
			$invoice = new Invoice();

			$result = $invoice->loadInvoiceFiles('recurring_invoice');

			echo $result;

		} else if($_POST['action'] == "delete_invoice_file") {

			require_once("classes/Invoice.class.php");
			$invoice = new Invoice();

			$result = $invoice->deleteInvoiceFile('single_invoice');

			echo $result;

		} else if($_POST['action'] == "delete_recurring_invoice_file") {

			require_once("classes/Invoice.class.php");
			$invoice = new Invoice();

			$result = $invoice->deleteInvoiceFile('recurring_invoice');

			echo $result;

		} else if($_POST['action'] == "delete_invoice") {

			require_once("classes/Invoice.class.php");
			$invoice = new Invoice();

			$result = $invoice->deleteInvoice();

			echo $result;

		} else if($_POST['action'] == "delete_recurring_invoice") {

			require_once("classes/Invoice.class.php");
			$invoice = new Invoice();

			$result = $invoice->deleteRecurringInvoice();

			echo $result;

		} else if($_POST['action'] == "save_invoice_draft") {

			require_once("classes/Invoice.class.php");
			$invoice = new Invoice();

			$result = $invoice->saveInvoiceDraft();

			echo $result;

		} else if($_POST['action'] == "save_recurring_invoice_draft") {

			require_once("classes/Invoice.class.php");
			$invoice = new Invoice();

			$result = $invoice->saveRecurringInvoiceDraft();

			echo $result;

		} else if($_POST['action'] == "process_invoice") {

			require_once("classes/Invoice.class.php");
			$invoice = new Invoice();

			$result = $invoice->processInvoice();

			echo $result;

		} else if($_POST['action'] == "get_billing_history") {

			require_once("classes/Account.class.php");
			$account = new Account();

			$result = $account->getBillingHistory();

			echo $result;

		} else if($_POST['action'] == "post_announcement") {

			require_once("classes/Account.class.php");
			$account = new Account();

			$result = $account->postAnnouncement();

			echo $result;

		} else if($_POST['action'] == "load_earlier_announcements") {

			require_once("classes/Account.class.php");
			$account = new Account();

			$result = $account->loadEarlierAnnouncements();

			echo $result;

		} else if($_POST['action'] == "load_earlier_activities") {

			require_once("classes/Account.class.php");
			$account = new Account();

			
			$result = $account->loadTimeline();

			echo $result;

		} else if($_POST['action'] == "load_earlier_user_activities") {

			require_once("classes/Account.class.php");
			$account = new Account();

			if($_POST['user_id'] && $_POST['user_id'] != 0) {

	    		$user_id = $_POST['user_id'];

	    	} else {

	    		$user_id = $_SESSION['user_id'];
	    	}
			
			$result = $account->loadTimeline($user_id);

			echo $result;

		} else if($_POST['action'] == "get_profile_thumbnail_url") { //gets the thumbnail url from the session

			$result = $_SESSION['image_thumb'];

			echo $result;

		} else if($_POST['action'] == "get_profile_picture_url") { //gets the thumbnail url from the database

			$result = $_SESSION['image'];

			echo $result;

		} else if($_POST['action'] == "get_logo_url") {

			require_once("classes/Account.class.php");
			$account = new Account();

			$result = $account->getLogoURL();

			echo $result;

		} else if($_POST['action'] == "save_setup_wizard") {

			require_once("classes/Account.class.php");
			$account = new Account();

			$result = $account->saveSetupWizard();

			echo $result;

		} else if($_POST['action'] == "invite_new_users") {

			require_once("classes/Account.class.php");
			$account = new Account();

			$result = $account->inviteNewUsers();

			echo $result;

		} else if($_POST['action'] == "get_woeid") {

			$url = "http://where.yahooapis.com/v1/places.q('" . $_POST['loc'] . "')?appid=RqLWhTPV34EoaDfHNCEkPFpkwejZezMd3LY9gob04fI53q1Cy1ag39n6rlCmUvgqiYFrBQ--";

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
		    curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.6) Gecko/20070725 Firefox/2.0.0.6");
		    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

			$content = curl_exec($ch);
			
			curl_close($ch);

			echo $content;

			
		}

	}
?>