<?php
	require_once("inc/session_start.php");
	require_once("functions.php");

	if($_GET['m'] == "paypal_postcheck") {

		$data = paypal_postcheck();
	
	} else {

		$data = paypal_precheck();
	}

	require_once("inc/head.php");
?>
<!-- BEGIN BODY -->
<body class="fixed-top front-end">
	<?php require_once("inc/frontend_menu.php"); ?>
<!-- BEGIN CONTAINER -->
	<div class="page-container row-fluid full-width-page">
		<div class="page-content">
  
			<div class="container">
				<div class="row-fluid">
					<div class="span12">
						<h3 class="form-section">Payment Processing</h3>
						<br><br><br>


						<?php echo $data; ?>

					</div>
				  
				</div>
			</div>
		</div>
	</div>   
	<!-- END PAGE CONTAINER-->    
	<!-- END CONTAINER -->   
	<?php      require_once("inc/footer.php");   ?>
</body>
<!-- END BODY -->
</html>

<?php   require_once("inc/session_end.php");?>