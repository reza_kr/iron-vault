<?php

	require_once('error_handler.php');

  require_once("classes/Invoice.class.php");
  $invoice = new Invoice();

  $html_array['view_invoice'] = $invoice->viewInvoiceByToken();

  $html_array['invoice_files'] = $invoice->loadInvoiceFilesByToken();

  require_once("inc/head.php");
  
?>

<body class="fixed-top page-sidebar-fixed view_invoice">

  <div class="header navbar navbar-inverse navbar-fixed-top">

      <div class="navbar-inner">

         <div class="container-fluid">

            <!-- BEGIN LOGO -->

            <div class="brand">

               <img src="img/logo_small.png" alt="logo" />

            </div>

         </div>

      </div>

      <!-- END TOP NAVIGATION BAR -->

  </div>



  <!-- BEGIN PAGE CONTAINER-->
  <div class="container-fluid invoice_wrapper">
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
      <div class="span12">

        <div class="portlet box blue">
          <div class="portlet-title">
            <div class="caption"><i class="icon-book"></i>Invoice</div>
          </div>
          <div class="portlet-body">

            <div class="row-fluid invoice-logo">

              <?php echo $html_array['view_invoice']['business_logo']; ?>

              <div class="span3">
                
                <?php echo $html_array['view_invoice']['business_info']; ?>

              </div>
              <div class="span3">

                <?php echo $html_array['view_invoice']['employee_info']; ?>

              </div>
              <div class="span3">
                <?php echo $html_array['view_invoice']['payment_status']; ?>
              </div>
            </div>
            <hr />

            <div class="row-fluid">
              <div class="span3">
                <h4>Client:</h4>

                <div class="invoice_client_details">
                  <?php echo $html_array['view_invoice']['client_info']; ?>
                </div>
              </div>
              <div class="span3">
                <?php echo $html_array['view_invoice']['client_additional_info']; ?>
              </div>
              <div class="span3">

              </div>
              <div class="span3">
                
                <h4>Invoice #:</h4>
                <span id="invoice_num"><?php echo $html_array['view_invoice']['invoice_number']; ?></span>
              </div>
            </div>
            <hr />
            <div class="row-fluid">
              <div class="span3">
                <h4>Payment Type:</h4>

                <?php echo $html_array['view_invoice']['payment_type']; ?>
              </div>
              <div class="span3">
                <h4>Invoice Date:</h4>
                
                <?php echo $html_array['view_invoice']['invoice_date']; ?>
              </div>
              <div class="span3">
                <h4>Payment Term:</h4>

                <?php echo $html_array['view_invoice']['payment_term']; ?>
              </div>
              <div class="span3">
                <h4>Due Date:</h4>

                <?php echo $html_array['view_invoice']['due_date']; ?>
              </div>
              
            </div>
            <hr />
            <div class="row-fluid">
              <h4>Products/Services:</h4>
              <table class="table table-striped table-hover invoice_items">
                <thead>
                  <tr>
                    <th id="count">#</th>
                    <th id="">Item</th>
                    <th class="hidden-480">Description</th>
                    <th class="hidden-480">Quantity</th>
                    <th class="hidden-480">Rate</th>
                    <th>Item Total</th>
                  </tr>
                </thead>
                <tbody>
                  <?php echo $html_array['view_invoice']['invoice_items']; ?>
                </tbody> 
              </table>

            </div>
            <hr>
            <div class="row-fluid">
              <div class="span6">
                <h4>Additional Notes:</h4>
                <?php echo $html_array['view_invoice']['notes']; ?>
              </div>
              
              <div class="span6 invoice-block">
                <?php echo $html_array['view_invoice']['invoice_totals']; ?>
                
                <div class="clearfix"></div>
                
              </div>
            </div>
            <hr>
            
            <div class="row-fluid"> 
              <div class="span12">
                <a class="btn light-red" href="#delete_invoice_modal" data-toggle="modal">Discard Invoice <i class="icon-trash"></i></a>
                <a class="btn light-green right process_invoice">Process Invoice <i class="icon-ok"></i></a>
                <!-- <a class="btn light-blue save right">Save Template <i class="icon-save"></i></a> -->
                <a class="btn light-blue save_draft right">Save Draft <i class="icon-save"></i></a>
                <input type="hidden" id="invoice_id" value="<?php echo $html_array['create_invoice']['invoice_id']; ?>" />
              </div>
            </div>

          </div>
        </div>

        <div class="portlet box blue">
          <div class="portlet-title">
            <div class="caption">
              <i class="icon-file"></i>
              Attachments
            </div>
          </div>
          <div class="portlet-body attachments">

            <?php echo $html_array['invoice_files']; ?>

            <div class="clearfix"></div>
          </div>
        </div>

      </div>
    </div>

  </div>
  <!-- END PAGE CONTAINER--> 


<script src="scripts/custom.js?t=<?php echo time(); ?>"></script>

</body>
<!-- END BODY -->
</html>