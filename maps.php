<?php
   require_once("inc/session_start.php");
   require_once("functions.php");

   $html_array = load_page_html();

   require_once("inc/head.php");
?>
<!-- BEGIN BODY -->
<body class="fixed-top page-sidebar-fixed <?php echo $html_array['page_sidebar_closed']; ?>">
	<?php require_once("inc/top_menu.php"); ?>
	<!-- BEGIN CONTAINER -->   
	<div class="page-container row-fluid">

		<?php require_once("inc/main_menu.php"); ?>

	  	<!-- BEGIN PAGE -->
		<div class="page-content">
			<div class="container-fluid">

				<!-- BEGIN PAGE CONTENT-->
				<div class="row-fluid">
					<div class="span3 hidden-print">
						<div class="portlet box blue add_new_location">
							<div class="portlet-title expand">
								<div class="caption"><i class="icon-plus"></i> Add Map Location</div>
								<div class="tools">
									<a href="javascript:;" class="expand"></a>
								</div>
							</div>

							<div class="portlet-body">
								<form class="form-inline zero_margin" action="#">

									<div class="control-group">
										<div class="controls">
											<input type="text" id="loc_name" name="loc_name" class="m-wrap" placeholder="Name" />
										</div>
									</div>
									<div class="control-group">
										<div class="controls">
											<input type="text" id="loc_address" name="loc_address" class="m-wrap" placeholder="Address" />
										</div>
									</div>
									<div class="control-group">
										<div class="controls">
											<select name="clients_list" id="clients_list" class="span12 select2 clients_list client_dropdown">
												<option value=""></option>
												<?php echo $html_array['clients_list']; ?>
											</select>
											<div class="clearfix"></div>
										</div>
									</div>
									<div class="control-group">
										<div class="controls">
											<input type="button" id="submit" class="btn light-blue right add_location" value="Add Location" />
										</div>
									</div>
								</form>

								<div class="clearfix"></div>
							</div>
						</div>

						<div class="portlet box blue locations">
							<div class="portlet-title collapse">
								<div class="caption"><i class="icon-map-marker"></i> Saved Locations</div>
								<div class="tools">
									<a href="javascript:;" class="collapse"></a>
								</div>
							</div>

							<div class="portlet-body saved_locations">

								<form class="form-inline zero_margin" action="#">

									<div class="control-group">
										<div class="controls">
											<select name="saved_locations_filter" id="saved_locations_filter" class="span12 select2 filter_dropdown">
												<option value=""></option>
												<?php echo $html_array['clients_list']; ?>
											</select>
										</div>
									</div>

									<div class="space10"></div>

									<div class="control-group">
										<div class="controls">
											<input type="text" id="search_saved_locations" class="m-wrap " placeholder="Search..." autocomplete="off" />
										</div>
									</div>
								</form>

								<ul class="ver-inline-menu locations_list">
									<?php echo $html_array['saved_locations']; ?>
									
								</ul>
							</div>
						</div>

						<div class="portlet box blue directions nomarginbottom">
							<div class="portlet-title expand">
								<div class="caption"><i class="icon-location-arrow"></i> Directions</div>
								<div class="tools">
									<a href="javascript:;" class="expand"></a>
								</div>
							</div>

							<div class="portlet-body directions_list">
								<form class="form-inline" action="#">
									<div class="control-group">
										<div class="controls">
											<input type="text" id="start_address" name="start_address" class="m-wrap" placeholder="Start Address" />
											<div id="geo_marker" title="Find My Location"></div>
										</div>
									</div>
									<div class="control-group">
										<div class="controls">
											<input type="text" id="end_address" name="end_address" class="m-wrap" placeholder="End Address" />
										</div>
									</div>
									<div class="control-group">
										<div class="controls">
											<div class="btn-group travel_mode">
												<a class="btn selected" id="DRIVING"><img src="img/car.png" /></a>
												<a class="btn" id="TRANSIT"><img src="img/transit.png" /></a>
												<a class="btn" id="BICYCLING"><img src="img/bike.png" /></a>
												<a class="btn" id="WALKING"><img src="img/walk.png" /></a>
											</div>

											<a href="#" class="btn right switch_directions"><i class="icon-long-arrow-down"></i> <i class="icon-long-arrow-up"></i></a>
										</div>
									</div>
									<div class="control-group">
										<div class="controls">
											<a href="#" class="btn clear">Clear</a>
											<input type="button" id="submit" class="btn light-blue right get_directions" value="Get Directions" />
										</div>
									</div>

									<div class="clearfix"></div>
								</form>

								<div id="directions_panel"></div>

							</div>
						</div>
					</div>
					<div class="span9">
						<div class="portlet box blue map nomarginbottom">
							<div class="portlet-title">
								<div class="caption"><i class="icon-map-marker"></i> Map View</div>
								<div class="tools">
		                        	<i class="icon-external-link-sign show_fullscreen" title="Expand Map"></i>
		                        	<i class="icon-remove hide_fullscreen"></i>
		                        </div>
							</div>

							<div class="portlet-body">
								<form class="form-inline maps" action="#">
									<input type="text" id="gmap_geocoding_address" class="m-wrap large" placeholder="Address" />
									<a id="gmap_geocoding_btn" class="btn light-blue">Search</a>

									<input type="hidden" class="ref_loc_name" value="<?php echo $html_array['ref_loc']['name'];?>" />
									<input type="hidden" class="ref_loc_address" value="<?php echo $html_array['ref_loc']['address'];?>" />

									<div class="btn-group pull-right tools">
										<button class="btn dropdown-toggle" data-toggle="dropdown">Tools <i class="icon-angle-down"></i>
										</button>
										<ul class="dropdown-menu pull-right">
											<li><a id="gmap_save_location" href="#save_location_modal" data-toggle="modal">Save Location</a></li>
											<li><a id="get_directions_to_from_here" href="#">Get Directions</a></li>
											<li><a id="gmap_remove_markers" href="#">Clear Map</a></li>
										</ul>
									</div>
								</form>

								<div id="gmap" class="gmaps"></div>
							</div>
						</div>
					</div>
					<!--end span9-->

					<!-- modal -->
					<div id="save_location_modal" class="modal hide fade" tabindex="-1" data-focus-on="input:first">
						<div class="modal-dialog">
							<div class="modal-content">
								<form class="form-horizontal zero_margin" action="#">
									<div class="modal-header">
										<i class="icon-remove right" data-dismiss="modal" aria-hidden="true"></i>
										<h3><i class="icon-map-marker"></i>&nbsp;&nbsp;Save Location</h3>
									</div>
									<div class="modal-body">
										<div class="control-group">
											<label class="control-label">Name</label>
											<div class="controls">
												<input type="text" id="save_loc_name" name="save_loc_name" class="span8 medium" />
											</div>
										</div>
										<div class="control-group">
											<label class="control-label">Address</label>
											<div class="controls">
												<input type="text" id="save_loc_address" name="save_loc_address" class="span8 medium" />
											</div>
										</div>
										<div class="control-group">
											<label class="control-label">Assign to Client</label>
											<div class="controls">
												<select name="clients_list" id="clients_list_modal" class="span8 select2 clients_list client_dropdown_modal">
													<option value=""></option>
													<?php echo $html_array['clients_list']; ?>
												</select>
											</div>
										</div>
									</div>
									<div class="modal-footer">
										<input type="button" id="submit" class="btn light-blue right save_location" value="Save Location" />
										<input type="button" data-dismiss="modal" class="btn" value="Cancel" />
									</div>
								</form>
							</div>
						</div>
					</div>


					<div id="edit_location_modal" class="modal hide fade" tabindex="-1" data-focus-on="input:first">
						<form class="form-horizontal zero_margin" action="#">
							<div class="modal-header">
								<i class="icon-remove right" data-dismiss="modal" aria-hidden="true"></i>
								<h3><i class="icon-map-marker"></i>&nbsp;&nbsp;Edit Location</h3>
							</div>
							<div class="modal-body">
								<div class="control-group">
									<label class="control-label">Name</label>
									<div class="controls">
										<input type="text" id="save_loc_name" name="save_loc_name" class="m-wrap medium" />
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">Address</label>
									<div class="controls">
										<input type="text" id="save_loc_address" name="save_loc_address" class="m-wrap medium" />
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">Assign to Client</label>
									<div class="controls">
										<select name="assign_to_client" id="assign_to_client" class="clients_list">
											<option val="">Select (Optional)</option>
											<?php

												echo $html_array['clients_list'];

											?>
										</select>
									</div>
								</div>
							</div>
							<div class="modal-footer">
								<a href="#delete_location_modal"  data-dismiss="modal" data-toggle="modal" class="btn light-red left">Delete</a>
								<input type="submit" id="submit" class="btn light-green right save" value="Save" />
								<input type="button" data-dismiss="modal" class="btn right" value="Cancel" />
							</div>
						</form>
					</div>


					<div id="delete_location_modal" class="modal hide fade" tabindex="-1" data-focus-on="input:first">

						<form class="zero_margin" action="#" method="post">

							<div class="modal-header">
								<i class="icon-remove right" data-dismiss="modal" aria-hidden="true"></i>
								<h3><i class="icon-map-marker"></i>&nbsp;&nbsp;Delete Saved Location</h3>
							</div>

							<div class="modal-body">

							Are you sure you want to delete this Saved Location?

	                            <input type="hidden" class="loc_id" name="loc_id" class="m-wrap span12" />

							</div>	
								
							<div class="modal-footer">
								<input type="submit" id="submit" class="btn light-red right delete" value="Delete" />
								<input type="button" data-dismiss="modal" class="btn" value="Cancel" />
							</div>

						</form>

					</div>
					

					<div id="geo_error_modal" class="modal hide fade" tabindex="-1" data-focus-on="input:first">

	                    <form class="zero_margin" action="#" method="post">
	                      <div class="modal-header">
	                        <i class="icon-remove right" data-dismiss="modal" aria-hidden="true"></i>
	                        <h3><i class="icon-warning-sign"></i>&nbsp;&nbsp;Geolocation Not Found</h3>
	                      </div>

	                      <div class="modal-body">
	                        Your location could not be determined.
	                      </div>  
	                        
	                      <div class="modal-footer">
	                        <input type="submit" id="submit" class="btn light-green right" value="Close" />
	                      </div>

	                    </form>

	                </div>


				</div>
			</div>
		<!-- END PAGE CONTENT -->
		</div>

		<!-- END PAGE CONTAINER-->       
	</div>
	<!-- END PAGE CONTAINER--> 
	<!-- END CONTAINER -->
	<?php
	  require_once("inc/footer.php");
	?>
</body>
<!-- END BODY -->
</html>
<?php
   require_once("inc/session_end.php");
?>