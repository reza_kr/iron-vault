<?php 
	
	require_once('plugins/stripe/Stripe.php');

	$stripe = array(
	  "secret_key"      => "sk_test_Dif7cFf3xE85lCGrRVnrzwNX",
	  "publishable_key" => "pk_test_4GhzqeXc4gdjLrDJVjALyVZl"
	);

	Stripe::setApiKey($stripe['secret_key']);

	// =========== Add Customer and Credit Card =============
	// $result = Stripe_Customer::create(array(
	//   	"description" => "Business Name: Sunset Boulevard, Subdomain: sunsetblvd, Account_ID: 12",
	//   	"card" => array(
	// 	  	"number" => "5555555555554444",
	// 	  	"exp_month" => "06",
	// 	  	"exp_year" => "15",
	// 	  	"cvc" => "872",
	// 	  	"name" => "Reza Karami"
	// 	),
	//   	"email" => "reza_karami@outlook.com"
	// ));

	//NOTE: Adding customer returns a unique Customer ID and Card ID... Must save customer ID and Card ID in database! (cus_5TwePZRtOaccX5) (card_15IyrsFKvMJqFioqIWhvCWee)
	//		Don't actually need the CARD ID....

	// =========== Update Customer and Credit Card =============
	// $cu = Stripe_Customer::retrieve("cus_5TwePZRtOaccX5");
	// $cu->description = "Customer updated";
	// $cu->card = array(
	// 	  	"number" => "5555555555554444",
	// 	  	"exp_month" => "06",
	// 	  	"exp_year" => "15",
	// 	  	"cvc" => "872",
	// 	  	"name" => "Reza Karami"
	// 	);
	// $result = $cu->save();


	// =========== Delete Card ==============
	// $cu = Stripe_Customer::retrieve("cus_5TwePZRtOaccX5");
	// $result = $cu->cards->retrieve("card_15IyrsFKvMJqFioqIWhvCWee")->delete();


	// =========== Charge Account ============
	try {
  		
		$result = Stripe_Charge::create(array(
		  	"amount" => 2700,
		  	"currency" => "usd",
		  	"customer" => "cus_5TwePZRtOaccX5",
		  	"description" => "Iron Vault Business Pro",
		  	"receipt_email" => "reza_karami@outlook.com"
		));

	} catch(Stripe_CardError $e) {

	  	// Since it's a decline, Stripe_CardError will be caught
	  	$body = $e->getJsonBody();
	  	$error  = $body['error'];

	  	if($error['code'] == 'card_declined') {

	  		echo "Your card was declined. Please add another card and try again";
	  	}
	}

	//NOTE: Charging an Account will return a Charge ID if successful... Must save charge ID in database! (ch_15Iz3SFKvMJqFioq3ov3VIuA)
	//		A failed charge attempt will cause an error with 'card_declined' .... handle accordingly.

	echo "<pre>";
	print_r($result);

?>