<?php
  require_once("inc/session_start.php");
  require_once("functions.php");
   
  $html_array = load_page_html();
  
  require_once("inc/head.php");
?>
<!-- BEGIN BODY -->
<body class="fixed-top page-sidebar-fixed <?php echo $html_array['page_sidebar_closed']; ?>">
   <?php require_once("inc/top_menu.php"); ?>
   <!-- BEGIN CONTAINER -->   
   <div class="page-container row-fluid">

      <?php require_once("inc/main_menu.php"); ?>

      <!-- BEGIN PAGE -->
      <div class="page-content"> 
         <div class="container-fluid">

        <!-- BEGIN PAGE CONTENT-->
        <div class="row-fluid">
          <div class="span12">

            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet box blue">
              <div class="portlet-title">
                <div class="caption"><i class="icon-cloud"></i>Storage</div>
              </div>
              
              <div class="portlet-body no-more-tables nopadding">

                <div id="file_manager"></div>

                <script type="text/javascript" charset="utf-8">
                  // elFinder script executed in functions.php -> storage.php footer string
                </script>


                <div id="storage_limi_reached_modal" class="modal hide fade" tabindex="-1"s aria-hidden="false">
                    <div class="modal-header">
                      <i class="icon-remove right" data-dismiss="modal" aria-hidden="true"></i>
                      <h3><i class="icon-cloud"></i>&nbsp;&nbsp;Storage Capacity</h3>
                    </div>
                    <div class="modal-body">
                      <p>It appears that you've reached your current storage capacity limit!<br><br>Try removing some older, unused files or uprade for more space!</p>
                    </div>
                    <div class="modal-footer">
                      <a href="#" class="btn light-blue right">Add More Space</a>
                      <input type="button" data-dismiss="modal" class="btn" value="Close">
                    </div>
                </div>


              </div>
            </div>

          </div>
        </div>
        <!-- END PAGE CONTENT -->
      </div>
     
     
         <!-- END PAGE CONTAINER-->       
      </div>
      <!-- BEGIN PAGE -->
   </div>
   <!-- END PAGE CONTAINER--> 
   <!-- END CONTAINER -->
   <?php
      require_once("inc/footer.php");
   ?>
</body>
<!-- END BODY -->
</html>
<?php
   require_once("inc/session_end.php");
?>