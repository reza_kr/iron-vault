<?php

   require_once("inc/session_start.php");

   require_once("functions.php");

   $html_array = load_page_html();

   require_once("inc/head.php");

?>

<!-- BEGIN BODY -->
<body class="login">

  <div class="container-fluid">

    <br><h2 class="business_name"><?php echo $_SESSION['business_name']; ?></h2>

    <div id="lock_modal" class="modal fade in page-lock" tabindex="-1" data-focus-on="input:first">
        <div class="modal-header">
          <h3><i class="icon-lock"></i>&nbsp;&nbsp;Session Locked</h3>
        </div>

        <div class="modal-body">

            <!-- BEGIN LOGIN FORM -->

            <?php

                if($_GET['e'] == "wrong_password") {
                  echo '<div class="form_error">
                          <span><i class="icon-warning-sign"></i> Incorrect password. Please try again.</span>
                        </div>';
                }

            ?>

            <img class="page-lock-img" src="<?php echo $_SESSION['image_thumb'] ?>" alt="profile picture">

			<div class="page-lock-info">

				<h1><?php echo $html_array['full_name']; ?></h1>

				<span><?php echo $html_array['business_name']; ?></span>

				<span><em>Locked</em></span>

				<form class="form-search" action="unlock" method="post">

					<div class="input-append">

						<input type="password" class="m-wrap" name="password" placeholder="Password">

						<button type="submit" class="btn light-blue icn-only"><i class="icon icon-unlock white"></i>&nbsp;&nbsp;Unlock Session</button>

					</div>

					<div class="relogin">

						<a href="logout.php?ref=lock_screen">Login with a different account</a>

					</div>

				</form>

			</div>    

        </div>
    </div>


    <div class="login_footer">
      <div class="logo">

        <a href="https://www.ironvault.ca"><img src="img/logo_small.png" alt="Iron Vault" /></a> 

      </div>

      <div class="copyright">

        <?php echo date('Y'); ?> © IRON VAULT LTD

      </div>
    </div>

  </div> 


	<?php 

		$footer_scripts = load_footer_scripts();

		echo $footer_scripts;

	?>

</body>

<!-- END BODY -->

</html>

<?php

   require_once("inc/session_end.php");

?>