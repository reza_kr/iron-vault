<?php
   require_once("inc/session_start.php");
   require_once("functions.php");

   $html_array = load_page_html();

   require_once("inc/head.php");
?>
<!-- BEGIN BODY -->
<body class="fixed-top page-sidebar-fixed <?php echo $html_array['page_sidebar_closed']; ?>">
   <?php require_once("inc/top_menu.php"); ?>
   <!-- BEGIN CONTAINER -->   
   <div class="page-container row-fluid">

      <?php require_once("inc/main_menu.php"); ?>

      <!-- BEGIN PAGE -->
      <div class="page-content">
         <div class="container-fluid">
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->

            <div class="row-fluid">
               <div class="span12">

                  <div class="portlet box blue">
                     <div class="portlet-title">
                        <div class="caption"><i class="icon-tasks"></i>Tasks</div>
                     </div>
                     <div class="portlet-body">

                        <div class="row-fluid">
                           <div class="span3 print_hidden">

                              <!-- <div class="control-group">
                                 <div class="controls">
                                    <input type="text" class="search_tasks span12 m-wrap nomargin" placeholder="Search..." />
                                 </div>
                              </div>                              

                              <hr> -->

                              <div class="portlet box blue">
                                 <div class="portlet-title">
                                    <div class="caption"><i class="icon-sort-by-attributes"></i> Sorting</div>
                                 </div>

                                 <div class="portlet-body">

                                    <select name="sorting" id="sorting" class="span12 m-wrap">
                                       <option value="oldest_to_newest">Oldest Entry First</option>
                                       <option value="newest_to_oldest">Newest Entry First</option>
                                       <option value="high_to_low">Priority: High to Low</option>
                                       <option value="low_to_high">Priority: Low to High</option>
                                       <option value="earliest_to_latest">Due Date: Earliest First</option>
                                       <option value="latest_to_earliest">Due Date: Latest First</option>
                                    </select>
                                 </div>
                              </div>

                              <div class="portlet box blue">
                                 <div class="portlet-title">
                                    <div class="caption"><i class="icon-filter"></i> Filter</div>
                                 </div>

                                 <div class="portlet-body">

                                    <ul id="search_filters" class="ver-inline-menu">
                                       <li id="all" class="active"><a href="#" data-toggle="tab" id="-1"><i class="icon-tasks"></i> All</a></li>
                                       <li id="today"><a href="#" data-toggle="tab" id="-1"><i class="icon-calendar-empty"></i> Today</a></li>
                                       <li id="tomorrow"><a href="#" data-toggle="tab" id="-1"><i class="icon-calendar-empty"></i> Tomorrow</a></li>
                                       <li id="next_7_days"><a href="#" data-toggle="tab" id="-1"><i class="icon-calendar-empty"></i> Next 7 Week</a></li>
                                       <li id="overdue"><a href="#" data-toggle="tab" id="-1"><i class="icon-warning-sign"></i> Overdue</a></li>                             </ul>
                                    </ul>
                                 </div>
                              </div>

                           </div>

                           <div class="span9">

                              <a id="show_add_task_modal" class="btn light-green add_new" data-toggle="modal" href="#add_task_modal"><i class="icon-plus"></i> New Task</a>

                              <div class="btn-group pull-right tools">
                                 <button class="btn dropdown-toggle" data-toggle="dropdown">Tools <i class="icon-angle-down"></i>
                                 </button>
                                 <ul class="dropdown-menu pull-right">
                                    <li><a href="#" class="print">Print</a></li>
                                    <li><a href="#" class="save_as_pdf">Save as PDF</a></li>
                                 </ul>
                              </div>

                              <hr>
                              
                              <div class="dd saveaspdf" id="tasks_list">

                                 <?php echo $html_array['tasks_list']; ?>

                              </div>

                           </div>
                        </div>

                     </div>
                  </div>
               </div>

            </div>


            <div id="add_task_modal" class="modal fade" tabindex="-1" data-focus-on="input:first">
               <form class="form-horizontal zero_margin" action="#" name="task_add_event">
                  <div class="modal-header">
                     <i class="icon-remove right" data-dismiss="modal" aria-hidden="true"></i>
                     <h3><i class="icon-tasks"></i>&nbsp;&nbsp;New Task</h3>
                  </div>
                  <div class="modal-body">
                     <div class="control-group">
                        <label class="control-label">Title</label>
                        <div class="controls">
                           <input type="text" id="title" name="title" class="m-wrap span8" />
                        </div>
                     </div>

                     <hr>

                     <div class="control-group">
                        <label class="control-label">Parent Task</label>
                        <div class="controls">
                           <select id="parent" name="parent" class="m-wrap span8">
                              <option value="">No parent</option> 
                           </select>
                        </div>
                     </div>
                     <div class="control-group">
                        <label class="control-label">Priority</label>
                        <div class="controls">
                           <label class="radio"><input type="radio" name="priority" value="0" checked/> Normal</label>
                           <label class="radio"><input type="radio" name="priority" value="1"/> Medium</label>
                           <label class="radio"><input type="radio" name="priority" value="2"/> High</label>
                        </div>
                     </div>
                     <div class="control-group">
                        <label class="control-label">Due Date</label>
                        <div class="controls">
                           <div class="input-append date span8">
                              <input type="text" class="m-wrap date-picker" id="due_date" placeholder="Date">
                              <span class="add-on"><i class="icon-calendar"></i></span>

                              <input type="text" class="m-wrap time-picker" id="due_time" placeholder="Time">
                              <span class="add-on"><i class="icon-time"></i></span>
                           </div>
                        </div>
                     </div>
                     <div class="control-group">
                        <label class="control-label">Reminder</label>
                        <div class="controls">
                           <select name="reminder" id="reminder" class="m-wrap span4">
                              <option value="-1">No reminder</option>
                              <option value="0">At due date/time</option>
                              <option value="5">5 minutes</option>
                              <option value="10">10 minutes</option>
                              <option value="15">15 minutes</option>
                              <option value="30">30 minutes</option>
                              <option value="45">45 minutes</option>
                              <option value="60">1 hour</option>
                              <option value="120">2 hours</option>
                              <option value="240">4 hours</option>
                              <option value="360">6 hours</option>
                              <option value="720">12 hours</option>
                              <option value="1080">18 hours</option>
                              <option value="1440">1 day</option>
                           </select>&nbsp;&nbsp;<span>before task is due</span>
                        </div>
                     </div>
                  </div>
                  <div class="modal-footer">
                     <input type="button" id="submit" data-dismiss="modal" class="btn light-blue right add_task" value="Add Task" />
                     <input type="button" data-dismiss="modal" class="btn" value="Cancel" />
                  </div>
               </form>
            </div>


            <div id="edit_task_modal" class="modal fade" tabindex="-1" data-focus-on="input:first">
               <form class="form-horizontal zero_margin" action="#" name="task_edit_event">
                  <div class="modal-header">
                     <i class="icon-remove right" data-dismiss="modal" aria-hidden="true"></i>
                     <h3><i class="icon-tasks"></i>&nbsp;&nbsp;Update Task</h3>
                  </div>
                  <div class="modal-body">
                     <div class="control-group">
                        <label class="control-label">Title</label>
                        <div class="controls">
                           <input type="hidden" id="task_id" name="task_id"/>
                           <input type="text" id="title" name="title" class="m-wrap span8" />
                        </div>
                     </div>

                     <hr>

                     <div class="control-group">
                        <label class="control-label">Parent Task</label>
                        <div class="controls">
                           <select id="parent" name="parent" class="m-wrap span8">
                              <option value="">No parent</option> 
                           </select>
                        </div>
                     </div>
                     <div class="control-group">
                        <label class="control-label">Priority</label>
                        <div class="controls">
                           <label class="radio"><input type="radio" name="priority" value="0" checked/> Normal</label>
                           <label class="radio"><input type="radio" name="priority" value="1"/> Medium</label>
                           <label class="radio"><input type="radio" name="priority" value="2"/> High</label>
                        </div>
                     </div>
                     <div class="control-group">
                        <label class="control-label">Due Date</label>
                        <div class="controls">
                           <div class="input-append date span8">
                              <input type="text" class="m-wrap date-picker" id="due_date" placeholder="Date">
                              <span class="add-on"><i class="icon-calendar"></i></span>

                              <input type="text" class="m-wrap time-picker" id="due_time" placeholder="Time">
                              <span class="add-on"><i class="icon-time"></i></span>
                           </div>
                        </div>
                     </div>
                     <div class="control-group">
                        <label class="control-label">Reminder</label>
                        <div class="controls">
                           <select name="reminder" id="reminder" class="m-wrap span4">
                              <option value="-1">No reminder</option>
                              <option value="0">At due date/time</option>
                              <option value="5">5 minutes</option>
                              <option value="10">10 minutes</option>
                              <option value="15">15 minutes</option>
                              <option value="30">30 minutes</option>
                              <option value="45">45 minutes</option>
                              <option value="60">1 hour</option>
                              <option value="120">2 hours</option>
                              <option value="240">4 hours</option>
                              <option value="360">6 hours</option>
                              <option value="720">12 hours</option>
                              <option value="1080">18 hours</option>
                              <option value="1440">1 day</option>
                           </select>&nbsp;&nbsp;<span>before task is due</span>
                        </div>
                     </div>
                  </div>
                  <div class="modal-footer">
                     <input type="button" id="submit" data-dismiss="modal" class="btn light-blue right update_task" value="Update Task" />
                     <input type="button" data-dismiss="modal" class="btn" value="Cancel" />
                  </div>
               </form>
            </div>


            <div id="remove_task_modal" class="modal hide fade" tabindex="-1" data-focus-on="input:first">

               <form class="zero_margin" action="#" method="post">

                  <div class="modal-header">
                     <i class="icon-remove right" data-dismiss="modal" aria-hidden="true"></i>
                     <h3><i class="icon-trash"></i>&nbsp;&nbsp;Delete Task(s)?</h3>
                  </div>

                  <div class="modal-body">
                     <input type="hidden" id="task_id" />
                     This task appears to have sub-tasks associated to it. Would you like to delete all sub-tasks as well?
                  </div>
                     
                  <div class="modal-footer">
                     <input type="button" id="yes" data-dismiss="modal" class="btn light-red right delete" value="Yes, Delete All" />
                     <input type="button" id="no" data-dismiss="modal" class="btn" value="No, Preserve Sub-Tasks" />
                  </div>

               </form>

            </div>


            <!-- END PAGE CONTENT--> 
         </div>
         <!-- END PAGE CONTAINER-->       
      </div>
      <!-- BEGIN PAGE -->
   </div>
   <!-- END PAGE CONTAINER--> 
   <!-- END CONTAINER -->
   <?php
      require_once("inc/footer.php");
   ?>
</body>
<!-- END BODY -->
</html>
<?php
   require_once("inc/session_end.php");
?>