<!DOCTYPE html>
<html>
	<head>
		<link href="css/stylesheet.min.css" rel="stylesheet" type="text/css"/>
		<link href="css/custom.css" rel="stylesheet" type="text/css"/>

		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>

		<script type="text/javascript">

			$(document).ready(function() {

				$('#recurring_interval').change(function() {

			    	var interval = $(this).find('option:selected').val();
			    	recurring_interval = interval;

			    	$('.recurring_wrapper .interval').hide();
			    	
			    	$('.recurring_' + interval).show();
			    });


			    $('#recurring_date_range').change(function() {

			    	var range = $(this).find('option:selected').val();

			    	if(range == 'none') {

			    		$('.recurring_end').hide();
			    		$('.recurring_occurrences').hide();
			    	
			    	} else if(range == 'by') {

			    		$('.recurring_end').show();
			    		$('.recurring_occurrences').hide();
			    	
			    	} else if(range == 'after') {

			    		$('.recurring_end').hide();
			    		$('.recurring_occurrences').show();
			    	}
			    });
			
			});

		</script>

		<style type="text/css">

			.recurring_wrapper select, .recurring_wrapper input {
				width: auto;
				margin: 0;
			}

			.recurring_wrapper .vertical_border {
				position: relative;
				margin: 0 20px;
				height: 69px;
				width: 0;
				border-left: 1px solid #E0DFDF;
				border-right: 1px solid #FEFEFE;
			}

			.recurring_daily, .recurring_yearly, .recurring_weekly {
				display: none;
			}

			#recurring_date_range {
				margin: 0 10px;
			}

			.recurring_end, .recurring_occurrences {
				display: none;
			}
		</style>
	</head>

	<body>

		<div class="row-fluid">
			<div class="span12">
			</div>
		</div>

		<div class="row-fluid">
			<div class="span2">
			</div>

			<div class="span8">
				<h4>Recurrence:</h4>

				<form id="recurring_form" action="scripts/scan_recurring_invoices" method="post">
					<div class="row-fluid recurring_wrapper">
						<div class="left">
							<label>Interval</label>
							<select name="recurring_interval" id="recurring_interval" class="m-wrap">
								<option value="daily">Daily</option>
								<option value="weekly">Weekly</option>
								<option value="monthly" selected="selected">Monthly</option>
								<option value="yearly">Yearly</option>
							</select>
						</div>

						<div class="left">
							<div class="vertical_border"></div>
						</div>

						<div class="left interval recurring_daily">
							<label>Frequency</label>
							<span>every</span> <input type="text" name="recurring_daily_freq" id="recurring_daily_freq" class="quantity m-wrap" value="1"/> <span>day(s)</span>
						</div>

						<div class="left interval recurring_weekly">
							<label>Frequency</label>
							<span>every</span> <input type="text" name="recurring_weekly_freq" id="recurring_weekly_freq" class="quantity m-wrap" value="1"/> <span>week(s)</span>&nbsp;&nbsp;
						</div>

						<div class="left interval recurring_monthly">
							<label>Frequency</label>
							<span>every</span> <input type="text" name="recurring_monthly_freq" id="recurring_monthly_freq" class="quantity m-wrap" value="1"/> <span>month(s)</span>
						</div>

						<div class="left interval recurring_yearly">
							<label>Frequency</label>
							<span>every</span> <input type="text" name="recurring_yearly_freq" id="recurring_yearly_freq" class="quantity m-wrap" value="1"/> <span>year(s)</span>
						</div>

						<div class="left">
							<div class="vertical_border"></div>
						</div>

						<div class="left recurring_start">
							<label>Start Date</label>
							<input type="text" name="recurring_start_date" id="recurring_start_date" class="m-wrap datepicker" value=""/>
						</div>

						<div class="left">
							<label>&nbsp;&nbsp;&nbsp;End</label>
							<select name="recurring_date_range" id="recurring_date_range" class="m-wrap">
								<option value="none">None</option>
								<option value="by">By</option>
								<option value="after">After</option>
							</select>
						</div>

						<div class="left recurring_end">
							<label>End Date</label>
							<input type="text" name="recurring_end_date" id="recurring_end_date" class="m-wrap datepicker" value=""/>
						</div>

						<div class="left recurring_occurrences">
							<label>Occurrences</label>
							<input type="text" name="recurring_occurrence_count" id="recurring_occurrence_count" class="quantity m-wrap"> <span>occurrences</span>
						</div>

						<div class="left">
							<input type="submit" id="submit" value="Submit" />
						</div>
					</div>
				</form>

			</div>

			<div class="span2">
			</div>
		</div>

	</body>

</html>