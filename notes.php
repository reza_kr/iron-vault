<?php
   require_once("inc/session_start.php");
   require_once("functions.php");

   $html_array = load_page_html();

   require_once("inc/head.php");
?>
<!-- BEGIN BODY -->
<body class="fixed-top page-sidebar-fixed <?php echo $html_array['page_sidebar_closed']; ?>">
   <?php require_once("inc/top_menu.php"); ?>
   <!-- BEGIN CONTAINER -->   
   <div class="page-container row-fluid">

      <?php require_once("inc/main_menu.php"); ?>

      <!-- BEGIN PAGE -->
      <div class="page-content">
         <div class="container-fluid">
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->

            <div class="row-fluid">
               <div class="span12">

                  <div class="portlet box blue">
                     <div class="portlet-title">
                        <div class="caption"><i class="icon-edit"></i>Notes</div>
                     </div>
                     <div class="portlet-body">

                        <div class="row-fluid">

                           <div class="span3">

   	                     	<div id="notes_search_box">
                                 <!-- <div class="row-fluid">
   	                     	 	     <a class="btn light-blue add_new right" href="#">New Note</a>
                                 </div>
                                 <hr class="grey"> -->
   	                     	 	<div class="row-fluid">
                                    <input type="text" class="search_notes m-wrap span12" placeholder="Search Notes..." />
                                 </div>

                                 <div class="spacer_10"></div>
                                 <hr class="grey">
                                 <div class="spacer_10"></div>

                                 <div class="notes_wrapper">
                                    <ul class="notes">
                                       <?php echo $html_array['notes']; ?>
                                    </ul>
                                 </div>
   	                     	</div>

                           </div>
                           <div class="span9">
                           
                              <div class="row-fluid">
                                 <a class="btn light-blue left new_note" href="#"><i class="icon-plus"></i> New Note</a>
                                 <span class="note_title"></span>
                                 <a class="btn light-blue right save_notes" href="#">Save Notes</a>
                              </div>

                              <div class="spacer_10"></div>
                              <hr class="grey">
                              <div class="spacer_10"></div>

                              <div class="row-fluid">
                                 <input type="hidden" id="note_id" value="" />
                              	<div class="notepad_wrapper">
                                    <textarea class="notepad" placeholder="Type your note here"></textarea>
                                    <div class="hidden" id="tmp_content"></div>
                                 </div>
                              </div>

                           </div>
                        </div>

                     </div>
                  </div>


                  <div id="delete_note_modal" class="modal hide fade" tabindex="-1" data-focus-on="input:first">

						<form class="zero_margin" action="#" method="post">

							<div class="modal-header">
								<i class="icon-remove right" data-dismiss="modal" aria-hidden="true"></i>
								<h3><i class="icon-trash"></i>&nbsp;&nbsp;Delete Note</h3>
							</div>

							<div class="modal-body">

								Are you sure you want to delete this Note?

	                            <input type="hidden" class="note_id" name="note_id" class="m-wrap span12" />

							</div>	
								
							<div class="modal-footer">
								<input type="submit" id="submit" class="btn light-red right delete" value="Delete" />
								<input type="button" data-dismiss="modal" class="btn" value="Cancel" />
							</div>

						</form>

					</div>
               </div>

            </div>

            <!-- END PAGE CONTENT--> 
         </div>
         <!-- END PAGE CONTAINER-->       
      </div>
      <!-- BEGIN PAGE -->
   </div>
   <!-- END PAGE CONTAINER--> 
   <!-- END CONTAINER -->
   <?php
      require_once("inc/footer.php");
   ?>
</body>
<!-- END BODY -->
</html>
<?php
   require_once("inc/session_end.php");
?>