

<div class="page-sidebar nav-collapse collapse">

	<div class="sidebar_glow"></div>

<!-- BEGIN SIDEBAR MENU -->    



	<ul class="page-sidebar-menu">

		<li>

			<div class="btn-group create">
				<button class="btn light-blue dropdown-toggle" data-toggle="dropdown"><i class="icon-plus"></i> Create <i class="icon-angle-down"></i>
				</button>
				<ul class="dropdown-menu">
					<li><a href="calendar#add_new_event"><i class="icon-calendar"></i> New Event</a></li>
					<li><a href="tasks#add_new_task"><i class="icon-tasks"></i> New Task</a></li>
					<li><a href="clients#add_new_client"><i class="icon-user"></i> New Client</a></li>
					<li><a href="new_invoice"><i class="icon-book"></i> New Invoice</a></li>
					<li><a href="inventory#add_new_item"><i class="icon-check"></i> New Inventory Item</a></li>
				</ul>
			</div>

			<!-- BEGIN SIDEBAR TOGGLER BUTTON -->

			<div class="sidebar-toggler hidden-phone"></div>

			<!-- BEGIN SIDEBAR TOGGLER BUTTON -->

		</li>

		<!--<li>

			<form class="sidebar-search" action="">

				<div class="input-box">

					<a href="javascript:;" class="remove"></a>

					<input type="text" id="search_query" placeholder="Search..." />				

					<input type="button" class="submit" value=" " />

				</div>

			</form>

		</li>-->

		
		<?php echo $html_array['main_menu']; ?>

	</ul>

<!-- END SIDEBAR MENU -->

</div>