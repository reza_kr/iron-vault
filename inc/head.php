<!DOCTYPE html>

<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->

<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->

<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

<!-- BEGIN HEAD -->

<head>

   <noscript>
      <meta http-equiv="refresh" content="0; URL='https://www.ironvault.ca/no_script'">
   </noscript>

   <meta charset="utf-8" />

   <title><?php echo $html_array['page_title']; ?></title>

   <meta content="width=device-width, initial-scale=1.0" name="viewport" />

   <meta content="" name="description" />

   <meta content="" name="author" />

   <!-- BEGIN GLOBAL MANDATORY STYLES -->

   <!--<link href="plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>

   <link href="plugins/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css"/>

   <link href="plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>

   <link href="css/style-metro.css" rel="stylesheet" type="text/css"/>

   <link href="css/style.css" rel="stylesheet" type="text/css"/>

   <link href="css/style-responsive.css" rel="stylesheet" type="text/css"/>

   <link href="css/animate.css" rel="stylesheet" type="text/css"/>

   <link href="css/themes/default.css" rel="stylesheet" type="text/css" id="style_color"/>

   <link href="plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>

   <link href="plugins/select2/select2_metro.css" rel="stylesheet" type="text/css"/>
   
   <link href="plugins/data-tables/DT_bootstrap.css"  rel="stylesheet" />

   <link href="plugins/fullcalendar/fullcalendar/fullcalendar.css" rel="stylesheet" />

   <link href="plugins/bootstrap-datetimepicker/css/datetimepicker.css" rel="stylesheet" type="text/css"/>

   <link href="plugins/recurly-js/themes/default/recurly.css" rel="stylesheet" />

   <link rel="stylesheet" type="text/css" media="screen" href="plugins/elfinder/css/elfinder.min.css">
   <link rel="stylesheet" type="text/css" media="screen" href="plugins/elfinder/css/theme.css">-->


   <!-- END GLOBAL MANDATORY STYLES -->

   <!-- BEGIN PAGE LEVEL STYLES -->

   <!--<link href="css/pages.css" rel="stylesheet" type="text/css"/>-->


   <style type="text/css">
      .pace .pace-progress {
        background: #4b8df8;
        position: fixed;
        z-index: 99999;
        top: 0;
        left: 0;
        height: 2px;

        -webkit-transition: width 1s;
        -moz-transition: width 1s;
        -o-transition: width 1s;
        transition: width 1s;
      }

      .pace-inactive {
        display: none;
      }
   </style>
   <script type="text/javascript" src="scripts/pace.min.js"></script>


   <link href="css/stylesheet.min.css" rel="stylesheet" type="text/css"/>

   <link href="css/custom.css" rel="stylesheet" type="text/css"/>

   <!--<link href="css/dark.css" rel="stylesheet" type="text/css"/>-->

   <link href="css/print.css" rel="stylesheet" type="text/css"/>


   <?php

      $page = basename($_SERVER['PHP_SELF']);

      if($page == "maps.php") {

         echo '<style type="text/css" media="print">
           @page { size: landscape; }
         </style>';
      }

   ?>


   <!--<link href="plugins/bootstrap-fileupload/bootstrap-fileupload.css" rel="stylesheet" type="text/css" />

   <link href="plugins/bootstrap-tag/css/bootstrap-tag.css" rel="stylesheet" type="text/css" />
   <link href="plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css" rel="stylesheet" type="text/css" />
   <link href="plugins/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
   <link href="plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css"  rel="stylesheet" type="text/css" />
   <link href="plugins/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" rel="stylesheet" type="text/css" />
   <link href="plugins/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
   <link href="plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" type="text/css" />
   <link href="plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>
   <link href="plugins/bootstrap-toastr/toastr.min.css" rel="stylesheet" type="text/css"/>-->





   <!-- END PAGE LEVEL STYLES -->

   <link rel='shortcut icon' type='image/x-icon' href='img/favicon.ico' />



   <link rel="apple-touch-icon" sizes="57x57" href="apple-icon-57x57.png" />

   <link rel="apple-touch-icon" sizes="72x72" href="apple-icon-72x72.png" />

   <link rel="apple-touch-icon" sizes="114x114" href="apple-icon-114x114.png" />

   <link rel="apple-touch-icon" sizes="144x144" href="apple-icon-144x144.png" />

</head>