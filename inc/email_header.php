
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Email Confirmation</title>
		
		<style type="text/css">
			body{
				margin:0;
				padding:0;
				font-family: Calibri, Arial;
				font-size: 12px;
				color: #333;
			}
			img{
				border:0;
				height:auto;
				line-height:100%;
				outline:none;
				text-decoration:none;
			}
			
			h1, h2, h3, h4, h5 {
				font-weight: normal;
			}
			
			a {
				color: #555;
			}
			a:hover {
				color: #333;
				text-decoration: none;
			}
			
			header {
				position: relative;
				width: 100%;
				background: #0095c2;
			}
			#header_content {
				position: relative;
				width: 700px;
				padding: 20px;
				margin: 0 auto;
				box-sizing: border-box;
			}
			
			#content {
				position: relative;
				width: 700px;
				padding: 20px;
				margin: 0 auto;
				font-size: 14px;
				box-sizing: border-box;
			}
			
			footer {
				position: relative;
				width: 100%;
				background: #DDD;
			}
			#footer_content {
				position: relative;
				width: 700px;
				padding: 20px;
				margin: 0 auto;
				box-sizing: border-box;
			}
			
		</style>
	</head>

    <body style="margin:0; padding:0; font-family: Calibri, Arial; font-size: 12px; color: #333;">
    	<header style="position: relative; width: 100%; background: #0095c2;">
			<div id="header_content" style="position: relative; width: 700px; padding: 20px; margin: 0 auto; box-sizing: border-box;">
				<img src="http://azureusweb.com/dating_site/assets/images/logo.png" alt="logo" />
			</div>
		</header>
		
		<div id="content" style="position: relative; width: 700px; padding: 20px; margin: 0 auto; font-size: 14px; box-sizing: border-box;">