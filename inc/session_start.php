<?php

	ini_set('session.hash_function', 'sha512');
	ini_set("session.entropy_file", "/dev/urandom");
	ini_set("session.entropy_length", "512");

	session_start();		

	ob_start();

	error_reporting(E_ERROR);	

?>