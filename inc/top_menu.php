<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-43106507-1', 'ironvault.ca');
  ga('send', 'pageview');

</script>

<?php



   $top_menu = load_top_menu();



?>

<div class="header navbar navbar-inverse navbar-fixed-top">

   <!-- BEGIN TOP NAVIGATION BAR -->

   <div class="navbar-inner">

      <div class="container-fluid">

         <!-- BEGIN LOGO -->

         <a class="brand" href="dashboard">

         <img src="img/logo_small.png" alt="logo" />

         </a>

         <!-- END LOGO -->

         <div id="date_time_weather">
            <div id="time"></div>
            <div id="date"></div>
            <div id="divider"></div>
            <div id="weather">
               <div id="weather_icon"></div>
               <div id="weather_text"></div>
            </div> 
         </div>

         <!-- BEGIN RESPONSIVE MENU TOGGLER -->

         <a href="javascript:;" class="btn-navbar collapsed" data-toggle="collapse" data-target=".nav-collapse">

         <img src="img/menu-toggler.png" alt="" />

         </a>          

         <!-- END RESPONSIVE MENU TOGGLER -->            


         <!-- BEGIN TOP NAVIGATION MENU -->             
         <ul class="nav pull-right">

            <!-- BEGIN INBOX DROPDOWN -->

            <li class="dropdown messages_notification" id="header_inbox_bar">

               <a href="#" class="dropdown-toggle" data-toggle="dropdown">

                  <i class="icon-envelope"></i>

                  <?php echo $html_array['messages_notifications']; ?>

               </a>

               <ul class="dropdown-menu extended inbox">

                  <li class="loading"></li>

               </ul>

            </li>

            <!-- END INBOX DROPDOWN -->


            <!-- BEGIN NOTIFICATION DROPDOWN -->   

            <li class="dropdown events_notification" id="header_notification_bar">

               <a href="#" class="dropdown-toggle" data-toggle="dropdown">

                  <i class="icon-calendar"></i>

                  <?php echo $html_array['events_notifications']; ?>

               </a>

               <ul class="dropdown-menu extended notification">

                  <li class="loading"></li>

               </ul>

            </li>

            <!-- END NOTIFICATION DROPDOWN -->

            <!-- BEGIN TODO DROPDOWN -->

            <li class="dropdown tasks_notification" id="header_task_bar">

               <a href="#" class="dropdown-toggle" data-toggle="dropdown">

                  <i class="icon-tasks"></i>

                  <?php echo $html_array['tasks_notifications']; ?>

               </a>

               <ul class="dropdown-menu extended tasks">

                  <li class="loading"></li>

               </ul>

            </li>

            <!-- END TODO DROPDOWN -->



            <!-- BEGIN INBOX DROPDOWN -->

            <!-- <li class="dropdown email_notification" id="header_email_bar">

               <a href="#" class="dropdown-toggle" data-toggle="dropdown">

                  <i class="icon-envelope"></i>

                  

               </a>

               <div class="email_widget">


                  <table class="email_choose_provider">
                     <tr>
                        <td><a href="#" id="gmail"><img src="img/gmail.png" alt="Gmail" /></a></td>
                        <td><a href="#" id="outlook"><img src="img/outlook.png" alt="Outlook" /></a></td>
                     </tr>
                     <tr>
                        <td><a href="#" id="yahoo"><img src="img/yahoo.png" alt="Yahoo" /></a></td>
                        <td><a href="#" id="icloud"><img src="img/icloud.png" alt="iCloud" /></a></td>
                     </tr>
                     <tr>
                        <td><a href="#" id="aol"><img src="img/aol.png" alt="AOL" /></a></td>
                        <td><a href="#" id="mail_com"><img src="img/mail.com.png" alt="Mail.com" /></a></td>
                     </tr>
                     <tr>
                        <td><a href="#" id="gmx"><img src="img/gmx.png" alt="GMX" /></a></td>
                        <td><a href="#" id="imap"><img src="img/imap.png" alt="IMAP" /></a></td>
                     </tr>
                  </table>

                  <div class="imap_configuration">
                     <form name="imap_config" id="imap_config" method="post" action="">
                        <div class="row-fluid">
                           <div class="span12">
                              <div class="control-group nomargin">
                                 <label class="control-label" for="inbox_url">Email Inbox URL</label>
                                 <div class="controls">
                                    <input type="text" id="inbox_url" name="inbox_url" class="m-wrap span12" placeholder="http://mail.example.com">
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="row-fluid">
                           <div class="span10">
                              <div class="control-group nomargin">
                                 <label class="control-label" for="imap_address">IMAP Server Address</label>
                                 <div class="controls">
                                    <input type="text" id="imap_address" name="imap_address" class="m-wrap span12" placeholder="imap.example.com">
                                 </div>
                              </div>
                           </div>

                           <div class="span2">
                              <div class="control-group nomargin">
                                 <label class="control-label" for="port">Port</label>
                                 <div class="controls">
                                    <input type="text" id="port" name="port" class="m-wrap span12" placeholder="993">
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="row-fluid">
                           <div class="span12">
                              <div class="control-group nomargin">
                                 <label class="control-label" for="email">Email</label>
                                 <div class="controls">
                                    <input type="text" id="email" name="email" class="m-wrap span12">
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="row-fluid">
                           <div class="span12">
                              <div class="control-group">
                                 <label class="control-label" for="password">Password</label>
                                 <div class="controls">
                                    <input type="password" id="password" name="password" class="m-wrap span12">
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="row-fluid">
                           <div class="span12">
                              <div class="control-group nomarginbottom">
                                 <div class="controls">
                                    <input type="submit" id="submit" name="submit" class="btn light-blue right span4" value="Connect">
                                    <a href="#" class="btn imap_config_back">Back</a>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </form>
                  </div>


                  <div class="email_loading">
                     <h3><img src="img/loader_small.gif"> Please Wait...</h3>
                     <p>(This could take a moment)</p>
                  </div>

                  <div class="email_error">
                     <i class="icon-exclamation-sign"></i> <span class="email_error_msg">We were unable to connect the Email Server. Please double check your entries and try again.</span>
                     <br><br>
                     <a href="#" class="btn light-blue width_50 nomargin email_error_back">Go Back</a>
                  </div>

                  <div class="email_inbox_wrapper">
                     <span>Inbox (<span class="inbox_email_address"></span>)</span>

                     <span class="btn-group pull-right">
                        <a href="#" class="btn refresh_email"> <i class="icon-refresh"></i></a>
                        <button class="btn dropdown-toggle" data-toggle="dropdown"> <i class="icon-cog"></i>&nbsp;&nbsp;<i class="icon-angle-down"></i></button>
                        <ul class="dropdown-menu pull-right">
                           <li><a href="#" class="clear_email_inbox">Clear List</a></li>
                           <li><a href="#" class="disconnect_imap">Disconnect</a></li>
                        </ul>
                     </span>

                     <p class="email_last_update_wrapper">Last Fetched: <span class="email_last_update">N/A</span></p>
                     <p class="email_fetch_in_progress"><img src="img/loader_small.gif"> Fetch in progress...</p>

                     <ul class="email_inbox">

                     </ul>
                     <a href="http://mail.live.com" target="_blank" class="inbox_link">Go to Inbox</a>
                  </div>


               </div>

            </li> -->

            <!-- END INBOX DROPDOWN -->



            <!-- BEGIN USER LOGIN DROPDOWN -->

            <li class="dropdown user">

               <a href="#" class="dropdown-toggle" data-toggle="dropdown">

               <img alt="thumbnail" src="<?php echo $html_array['image_thumb']; ?>" />&nbsp;&nbsp;

               <span class="name"><?php echo $top_menu['first_name'] . " " . $top_menu['last_name']; ?></span>

               <i class="icon-angle-down"></i>

               </a>

               <ul class="dropdown-menu">

                  <li><a href="profile"><i class="icon-user"></i> My Profile</a></li>

                  <li><a href="profile_settings"><i class="icon-cog"></i> Profile Settings</a></li>

                  <li><a href="messages"><i class="icon-envelope"></i> Messages</a></li>

                  <li><a href="http://support.ironvault.ca/hc/communities/public/topics" target="_blank"><i class="icon-question"></i> Support</a></li>

                  <li><a href="http://myaccount.zendesk.com/account/dropboxes/2830039812" onClick="script: Zenbox.show(); return false;"><i class="icon-plus-sign-alt"></i> Feedback</a></li>

                  <li class="divider"></li>

                  <li><a href="#" id="fullscreen"><i class="icon-fullscreen"></i> Full-Screen</a></li>

                  <li><a href="#" id="lock_session"><i class="icon-lock"></i> Lock Screen</a></li>

                  <li><a href="logout"><i class="icon-signout"></i> Log Out</a></li>

               </ul>

            </li>

            <!-- END USER LOGIN DROPDOWN -->

         </ul>

         <!-- END TOP NAVIGATION MENU --> 


         <form class="search-form pull-right top_bar_search" role="form" action="search" method="get" tabindex="1">
            <input type="text" class="form-control input-medium input-sm search_query" name="query" placeholder="Search..." autocomplete="off" tabindex="2">
            <input type="submit" class="submit" value=" " tabindex="3">

            <div class="top_bar_search_results" tabindex="4">
               <div class="search_results_header" tabindex="5"><div class="loading"></div></div>
            </div>
         </form>

      </div>

   </div>

   <!-- END TOP NAVIGATION BAR -->

</div>