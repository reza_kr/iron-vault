<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-43106507-1', 'ironvault.ca');
  ga('send', 'pageview');

</script>

<?php

   $page = basename($_SERVER['PHP_SELF']);



   switch ($page) {

      case 'index.php':

         $home_active = 'active';

         break;

      case 'features.php':

         $features_active = 'active';

         break;

      case 'pricing.php':

         $pricing_active = 'active';

         break;

      case 'signup.php':

         $signup_active = 'active';

         break;

      case 'faq.php':

         $faq_active = 'active';

         break;

      case 'contact.php':

         $contact_active = 'active';

         break;

      default:

         break;

   }

?>

<!-- BEGIN HEADER -->

   <div class="header navbar navbar-inverse navbar-fixed-top">

      <!-- BEGIN TOP NAVIGATION BAR -->

      <div class="navbar-inner">

         <div class="container">

            <!-- BEGIN LOGO -->

            <a class="brand" href="index.php">

            <img src="img/logo_small.png" alt="logo" />

            </a>

            <!-- END LOGO -->

            <!-- BEGIN HORIZANTAL MENU -->

            <div class="navbar hor-menu hidden-phone hidden-tablet right">

               <div class="navbar-inner">

                  <ul class="nav">

                     <li class="<?php echo $home_active; ?>">

                        <a href="index.php">Home</a>

                     </li>

                     <li class="<?php echo $features_active; ?>">

                        <a href="features.php">Features</a>

                     </li>

                     <li class="<?php echo $pricing_active; ?>">

                        <a href="pricing.php">Plans &amp; Pricing</a>

                     </li>

                     <li class="<?php echo $signup_active; ?>">

                        <a href="signup.php">Sign Up</a>

                     </li>

                     <li class="<?php echo $faq_active; ?>">

                        <a href="faq.php">FAQ</a>

                     </li>

                     <li class="<?php echo $contact_active; ?>">

                        <a href="contact.php">Contact Us</a>

                     </li>

                  </ul>

               </div>

            </div>

            <!-- END HORIZANTAL MENU -->

            <!-- BEGIN RESPONSIVE MENU TOGGLER -->

            <a href="javascript:;" class="btn-navbar collapsed" data-toggle="collapse" data-target=".nav-collapse">

            <img src="img/menu-toggler.png" alt="" />

            </a>          

            <!-- END RESPONSIVE MENU TOGGLER -->  



         </div>

      </div>

      <!-- END TOP NAVIGATION BAR -->

   </div>

   <!-- END HEADER -->