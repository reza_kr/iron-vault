<div class="footer">

	<ul class="social-icons">
		<li><a href="https://www.facebook.com/pages/Iron-Vault/496485500430708" target="_blank" data-original-title="Facebook" class="facebook" title="Facebook"></a></li>
		<li><a href="https://twitter.com/IronVaultLtd" target="_blank" data-original-title="Twitter" class="twitter" title="Twitter"></a></li>
		<li><a href="https://plus.google.com/107413855669026561478/" target="_blank" data-original-title="Google+" class="googleplus" title="Google+"></a></li>
		<li><a href="http://www.youtube.com/channel/UCWCaVWvqIfhbb_6hCaFkhCA" target="_blank" data-original-title="YouTube" class="youtube" title="YouTube"></a></li>
	</ul>

	<div class="span pull-right">

		<span class="go-top"><i class="icon-angle-up"></i></span>

	</div>

</div>



<?php


	$footer_scritps = load_footer_scripts();



	echo $footer_scritps;


?>



<script src="scripts/custom.js"></script>

