<div class="footer">


</div>

<div class="loading_large"></div>
<div class="loading_page"><img src="img/loading.gif" /><span class="loading_page_message"></span></div>

<div class=" login login_overlay">

	<div class="container-fluid">

    <br><h2 class="business_name"><?php echo $_SESSION['business_name']; ?></h2>

    <div id="lock_modal" class="modal fade in page-lock" tabindex="-1" data-focus-on="input:first">
        <div class="modal-header">
          <h3><i class="icon-lock"></i>&nbsp;&nbsp;Session Locked</h3>
        </div>

        <div class="modal-body">

            <!-- BEGIN LOGIN FORM -->
            <span>For your security, your session has been locked.</span>
            <div class="spacer_10"></div>

            <div class="form_error">
              <span><i class="icon-warning-sign"></i> Incorrect password. Please try again.</span>
            </div>

            <img class="page-lock-img" src="<?php echo $_SESSION['image_thumb']; ?>" alt="profile picture">

			<div class="page-lock-info">

				<h1><?php echo $_SESSION['first_name'] . ' ' . $_SESSION['last_name']; ?></h1>

				<span><?php echo $_SESSION['business_name'] ?></span>

				<span><em>Locked</em></span>

				<form class="form-search" action="#" id="overlay_unlock" method="post">

					<div class="input-append">

						<input type="password" class="m-wrap" id="password" name="password" placeholder="Password">

						<button type="submit" class="btn light-blue icn-only unlock"><i class="icon icon-unlock white"></i>&nbsp;&nbsp;Unlock Session</button>

					</div>

					<div class="relogin">

						<a href="logout.php?ref=lock_screen">Login with a different account</a>

					</div>

				</form>

			</div>    

        </div>
    </div>


    <div class="login_footer">
      <div class="logo">

        <a href="https://www.ironvault.ca"><img src="img/logo_small.png" alt="Iron Vault" /></a> 

      </div>

      <div class="copyright">

        <?php echo date('Y'); ?> © IRON VAULT LTD

      </div>
    </div>

  </div>

</div>

<!-- <div class="overlay">
	<div class="overlay_message">
		<img src="img/loader_xlarge_greybg.gif" />
		<br>
		<br>
		<h3>Your connection was lost. Waiting to re-establish connection...
		<br><br><br>
		<a href="#" class="dismiss_overlay btn blue">Dismiss</a></h3>
	</div>
</div> -->


<script src="scripts/plugins.min.js"></script>
<script src="scripts/pages.js"></script>

<?php

	$footer_scritps = load_footer_scripts();

	echo $footer_scritps;


	echo "<script>";

	echo "var user_id = " . $_SESSION['user_id'] . "; ";
	echo "var account_id = " . $_SESSION['account_id'] . "; ";

	echo "var f_name = '" . $_SESSION['first_name'] . "'; ";
	echo "var l_name = '" . $_SESSION['last_name'] . "'; ";

	echo "var messages_notifications = " . $html_array['msg_notif_count'] . ";";
	echo "var events_notifications = " . $html_array['event_notif_count'] . ";";
	echo "var tasks_notifications = " . $html_array['task_notif_count'] . ";";
	echo "var total_notifications = " . $html_array['total_notif_count'] . ";";

	echo "var convo_notifs = '" . $html_array['convo_notifs'] . "';";
	echo "var convo_notifs_array = new Array();";

	echo "var lock_timeout = " . $_SESSION['lock_timeout'] . ";";
	echo "var password_change_error = '" . $_GET['pwd_e'] . "';";

	echo "</script>";


?>

<script src="plugins/simpleweather/jquery.simpleWeather.js"></script>

<script src="scripts/custom.js?t=<?php echo time(); ?>"></script>

<script src="https://www.ironvault.ca:8084/socket.io/socket.io.js"></script>
<script src="notifications/notifications_client.js"></script>

<script type="text/javascript" src="//assets.zendesk.com/external/zenbox/v2.6/zenbox.js"></script>
<style type="text/css" media="screen, projection">
  @import url(//assets.zendesk.com/external/zenbox/v2.6/zenbox.css);
</style>
<script type="text/javascript">
  if (typeof(Zenbox) !== "undefined") {
    Zenbox.init({
      dropboxID:   "20129364",
      url:         "https://ironvault.zendesk.com",
      tabTooltip:  "Support",
      tabImageURL: "https://assets.zendesk.com/external/zenbox/images/tab_support.png",
      tabColor:    "black",
      tabPosition: "Left",
      hide_tab:   "true"
    });
  }
</script>