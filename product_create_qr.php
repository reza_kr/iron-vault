<?php 
	
    session_start();
	
    require_once('inc/connect_dbo.php');

    $account_id = $_SESSION['account_id'];
    $subdomain = $_SESSION['subdomain'];
    $product_id = $_POST['product_id'];

    
    $query = $dbo->prepare("SELECT qr_code FROM inventory WHERE account_id = ? AND item_id = ?");
    $query->execute(array($account_id, $product_id));

    $result = $query->fetchAll();

    foreach($result as $row) {

        $qr_code = $row[0];
    }


    if($qr_code == "") {

        include('plugins/phpqrcode/qrlib.php'); 

        require_once("scripts/aws.phar");

        $s3client = Aws\S3\S3Client::factory(array(
            'key'    => 'AKIAJKBMGCMLYBHN77PQ',
            'secret' => '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'
        ));

        $tempDir = 'tmp_files/'; 
         
        $codeContents = 'https://' . $subdomain . '.ironvault.ca/product?id=' . $product_id; 
         
        $filename = 'prqr_' . $account_id . '_' . $product_id .'.png'; //prqr = Product QR

        $pngAbsoluteFilePath = $tempDir.$filename; 
        $urlRelativeFilePath = $tempDir.$filename; 
         
        // generating 
        if(!file_exists($pngAbsoluteFilePath)) { 
            QRcode::png($codeContents, $pngAbsoluteFilePath, QR_ECLEVEL_H, 8, 2);  //This line creates the QR Code
            

            $result = $s3client->putObject(array(
                'ACL' => 'public-read',
                'Bucket' => 'ironvault',
                'SourceFile' => 'tmp_files/' . $filename,
                'Key' => 'item_qr/' . $filename,
                'ContentType' => 'image/png',
                'StorageClass' => 'REDUCED_REDUNDANCY '
            ));


            $query = $dbo->prepare("UPDATE inventory SET qr_code = ? WHERE account_id = ? AND item_id = ?");
            $query->execute(array($filename, $account_id, $product_id));


        } else { 
            //File already generated! We can use this cached file to speed up site on common codes! 
        }

    } else {

        //Product has a QR code already... use it!

    }
    

?>