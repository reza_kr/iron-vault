<?php

   require_once("inc/session_start.php");
   require_once("functions.php");

   $html_array = load_page_html();

   require_once("inc/head.php");
?>
<!-- BEGIN BODY -->
<body class="fixed-top page-sidebar-fixed <?php echo $html_array['page_sidebar_closed']; ?>">
   <?php require_once("inc/top_menu.php"); ?>
   <!-- BEGIN CONTAINER -->   
   <div class="page-container row-fluid">

      <?php require_once("inc/main_menu.php"); ?>

      <!-- BEGIN PAGE -->
      <div class="page-content">
         <div class="container-fluid">

				<!-- BEGIN PAGE CONTENT-->
				<div class="row-fluid">
					<div class="span12">

                  <div class="portlet box blue">
                     <div class="portlet-title">
                        <!--<div class="caption"><a href="profile.php" class="btn portlet_title_btn"><i class="icon-angle-left"></i> Back to Profile</a></div>-->
                        <div class="caption"><a href="profile.php" class="btn portlet_title_btn"><i class="icon-arrow-left"></i></a><i class="icon-cog"></i> Profile Settings</div>
                     </div>
                     
                     <div class="portlet-body">

                        <div class="row-fluid profile_settings_nav">
                           <div class="span2">
                              <a href="#" id="personal_info" class="block_btn"><i class="icon-cog"></i><br>Personal Info</a>
                           </div>
                           <div class="span2">
                              <a href="#" id="profile_picture" class="block_btn"><i class="icon-picture"></i><br>Profile Picture</a>
                           </div>
                           <div class="span2">
                              <a href="#" id="security" class="block_btn"><i class="icon-lock"></i><br>Security</a>
                           </div>
                        </div>

                        <hr>


                        <div class="row-fluid section personal_info">

                           <div class="span12">

                              <h3>Personal Info</h3><br>

                              <form action="profile_settings?action=update_personal_info" class="horizontal-form personal_info" method="post">
                                 <div class="row-fluid">
                                    <div class="span6 ">
                                       <div class="control-group">
                                          <label class="control-label" for="firstName">First Name</label>
                                          <div class="controls">
                                             <input type="text" id="firstName" name="first_name" class="m-wrap span12" value="<?php echo $html_array['profile_settings']['first_name']; ?>">
                                          </div>
                                       </div>
                                    </div>
                                    <!--/span-->
                                    <div class="span6 ">
                                       <div class="control-group">
                                          <label class="control-label" for="lastName">Last Name</label>
                                          <div class="controls">
                                             <input type="text" id="lastName" name="last_name" class="m-wrap span12" value="<?php echo $html_array['profile_settings']['last_name']; ?>">
                                          </div>
                                       </div>
                                    </div>
                                    <!--/span-->
                                 </div>
                                 
                                 <div class="row-fluid">
                                    <div class="span6 ">
                                       <div class="control-group">
                                          <label class="control-label" for="firstName">Position</label>
                                          <div class="controls">
                                             <input type="text" id="firstName" name="position" class="m-wrap span12" value="<?php echo $html_array['profile_settings']['position']; ?>">
                                          </div>
                                       </div>
                                    </div>
                                    <!--/span-->

                                    <div class="span6">
                                       <div class="row-fluid">

                                          <div class="span3">
                                             <div class="control-group">
                                                <label class="control-label" for="working_since_month">Working Since</label>
                                                <div class="controls">
                                                   <select name="working_since_month" required class="m-wrap span12">
                                                      <option value='-1'>Month</option>
                                                      <?php 
                                                         $months = getSimpleArray("month");

                                                         foreach($months as $key=>$month) {
                                                            if($key == $html_array['profile_settings']['working_since_month']) {
                                                               echo "<option value='$key' selected='selected'>$month</option>";
                                                            } else {
                                                               echo "<option value='$key'>$month</option>";
                                                            }
                                                         }
                                                      ?>
                                                   </select>
                                                </div>
                                             </div>
                                          </div>
                                          
                                          <div class="span3">
                                             <div class="control-group">
                                                <label class="control-label" for="working_since_year">&nbsp;</label>
                                                <div class="controls">
                                                   <select name="working_since_year" required class="m-wrap span12">
                                                      <option value='-1'>Year</option>
                                                      <?php
                                                         $year = date('Y');

                                                         for($i = ($year); $i > ($year - 50); $i--) {
                                                            if($i == $html_array['profile_settings']['working_since_year']) {
                                                               echo "<option value='$i' selected='selected'>$i</option>";
                                                            } else {
                                                               echo "<option value='$i'>$i</option>";
                                                            }
                                                         }
                                                      ?>
                                                   </select>
                                                </div>
                                             </div>
                                          </div>

                                       </div>
                                    </div>

                                 </div>

                                 <div class="row-fluid">
                                    <div class="span6 ">
                                       <div class="control-group">
                                          <label class="control-label" >Gender</label>
                                          <div class="controls">
                                             <select  class="m-wrap span12" name="gender">
                                                <?php

                                                   if($html_array['profile_settings']['gender'] == "male") {
                                                      echo '<option value="male" selected="selected">Male</option>
                                                            <option value="female">Female</option>';
                                                   } else if($html_array['profile_settings']['gender'] == "female") {
                                                      echo '<option value="male">Male</option>
                                                            <option value="female" selected="selected">Female</option>';
                                                   }
                                                ?>
                                             </select>
                                          </div>
                                       </div>
                                    </div>
                                    <!--/span-->

                                    <div class="span6">
                                       <div class="row-fluid">

                                          <div class="span3">
                                             <div class="control-group">
                                                <label class="control-label" >Date of Birth</label>
                                                <div class="controls">
                                                   <select name="dob_day" required class="m-wrap span12">
                                                      <option value='-1'>Day</option>
                                                      <?php
                                                         for($i = 1; $i <= 31; $i++) {
                                                            if($i == $html_array['profile_settings']['dob_day']) {
                                                               echo "<option value='$i' selected='selected'>$i</option>";
                                                            } else {
                                                               echo "<option value='$i'>$i</option>";
                                                            }
                                                         }
                                                      ?>
                                                   </select>
                                                </div>
                                             </div>
                                          </div>

                                          <div class="span3">
                                             <div class="control-group">
                                                <label class="control-label" >&nbsp;</label>
                                                <div class="controls">
                                                   <select name="dob_month" required class="m-wrap span12">
                                                      <option value='-1'>Month</option>
                                                      <?php 
                                                         $months = getSimpleArray("month");

                                                         foreach($months as $key=>$month) {
                                                            if($key == $html_array['profile_settings']['dob_month']) {
                                                               echo "<option value='$key' selected='selected'>$month</option>";
                                                            } else {
                                                               echo "<option value='$key'>$month</option>";
                                                            }
                                                         }
                                                      ?>
                                                   </select>
                                                </div>
                                             </div>
                                          </div>

                                          <div class="span3">
                                             <div class="control-group">
                                                <label class="control-label" >&nbsp;</label>
                                                <div class="controls">
                                                   <select name="dob_year" required class="m-wrap span12">
                                                      <option value='-1'>Year</option>
                                                      <?php
                                                         $year = date('Y');

                                                         for($i = ($year - 18); $i > ($year - 98); $i--) {
                                                            if($i == $html_array['profile_settings']['dob_year']) {
                                                               echo "<option value='$i' selected='selected'>$i</option>";
                                                            } else {
                                                               echo "<option value='$i'>$i</option>";
                                                            }
                                                         }
                                                      ?>
                                                   </select>
                                                </div>
                                             </div>
                                          </div>

                                          <div class="span3">
                                             <div class="control-group">
                                                <label class="control-label" >&nbsp;</label>
                                                <div class="controls">
                                                   <select name="show_dob_year" class="m-wrap span12">
                                                      <?php

                                                         if($html_array['profile_settings']['show_dob_year'] == 1) {
                                                            echo '<option value="1" selected="selected">Show Year</option>
                                                                  <option value="0">Hide Year</option>';
                                                         } else {
                                                            echo '<option value="1">Show Year</option>
                                                                  <option value="0" selected="selected">Hide Year</option>';
                                                         }
                                                      ?>
                                                   </select>
                                                </div>
                                             </div>
                                          </div>

                                       </div>
                                    </div>
                                    <!--/span-->
                                 </div>
                                 <!--/row-->        
                                 <div class="row-fluid">
                                    <div class="span6 ">
                                       <div class="control-group">
                                          <label class="control-label" >Username</label>
                                          <div class="controls">
                                             <input type="text" name="username" class="m-wrap span12 zero_margin" disabled value="<?php echo $html_array['profile_settings']['username']; ?>">
                                             <span class="help-block">For your personal security, your username cannot be changed.</span>
                                          </div>
                                       </div>
                                    </div>
                                    <!--/span-->
                                    <div class="span6 ">
                                       <div class="control-group">
                                          <label class="control-label" >Email</label>
                                          <div class="controls">
                                             <input type="text" name="email" class="m-wrap span12" value="<?php echo $html_array['profile_settings']['email']; ?>">
                                          </div>
                                       </div>
                                    </div>
                                    <!--/span-->
                                 </div>

                                 <div class="row-fluid">
                                    <div class="span6 ">
                                       <div class="control-group">
                                          <label class="control-label" >Primary Phone</label>
                                          <div class="controls">
                                             <input type="text" name="primary_phone" class="m-wrap span12" value="<?php echo $html_array['profile_settings']['primary_phone']; ?>"> 
                                          </div>
                                       </div>
                                    </div>
                                    <!--/span-->
                                    <div class="span6 ">
                                       <div class="control-group">
                                          <label class="control-label" >Alternative Phone</label>
                                          <div class="controls">
                                             <input type="text" name="alternative_phone" class="m-wrap span12" value="<?php echo $html_array['profile_settings']['alternative_phone']; ?>"> 
                                          </div>
                                       </div>
                                    </div>
                                    <!--/span-->
                                 </div>
                                 <!--/row-->           
                                 <div class="row-fluid">
                                    <div class="span2">
                                       <div class="control-group">
                                          <label class="control-label" >Hometown</label>
                                          <div class="controls">
                                             <input type="text" name="hometown" class="m-wrap span12" value="<?php echo $html_array['profile_settings']['hometown']; ?>">
                                          </div>
                                       </div>
                                    </div>

                                    <div class="span2">
                                       <div class="control-group">
                                          <label class="control-label" >Country</label>
                                          <div class="controls">
                                             <select  class="m-wrap span12" name="country" id="country">
                                                <option value="-1">Country</option>
                                                <?php 
                                                   foreach($html_array['countries'] as $key => $country) {
                                                      if($key == $html_array['profile_settings']['country']) {
                                                         echo "<option value='$key' selected='selected'>$country</option>";
                                                      } else {
                                                         echo "<option value='$key'>$country</option>";
                                                      }
                                                   }
                                                ?>
                                             </select>
                                          </div>
                                       </div>
                                    </div>

                                    <div class="span2">
                                       <div class="control-group">
                                          <label class="control-label" >Timezone</label>
                                          <div class="controls">
                                             <select  class="m-wrap span12" name="timezone" id="timezone">
                                                <option value="-1">Timezone</option>
                                                <?php 

                                                   foreach ($html_array['timezone_select'] as $key => $label) {

                                                      if($key == $html_array['profile_settings']['timezone']) {
                                                         echo "<option value='$key' selected='selected'>$label</option>";
                                                      } else {
                                                         echo "<option value='$key'>$label</option>";
                                                      }
                                                   }

                                                ?>
                                             </select>
                                          </div>
                                       </div>
                                    </div>

                                    <div class="span6 ">
                                       <div class="control-group">
                                          <label class="control-label" >Website</label>
                                          <div class="controls">
                                             <input type="text" name="website" class="m-wrap span12" value="<?php echo $html_array['profile_settings']['website']; ?>">
                                          </div>
                                       </div>
                                    </div>
                                    <!--/span-->
                                 </div>

                                 <hr>

                                 <div class="row-fluid">
                                    <div class="span6 ">
                                       <div class="control-group">
                                          <label class="control-label" >Hobbies/Interests</label>
                                          <div class="controls">
                                             <input type="text" id="interests" class="m-wrap span12 select2">
                                             <input type="hidden" id="interests_hidden" name="interests" value="<?php echo $html_array['profile_settings']['hobbies']; ?>">
                                             <span class="help-block">Hit 'Enter' after each entry.</span>
                                          </div>
                                       </div>
                                    </div>
                                    <!--/span-->
                                    <div class="span6 ">
                                       <div class="control-group">
                                          <label class="control-label" >Favourite Books</label>
                                          <div class="controls">
                                             <input type="text" id="fav_books" class="m-wrap span12 select2">
                                             <input type="hidden" id="fav_books_hidden" name="fav_books" value="<?php echo $html_array['profile_settings']['fav_books']; ?>">
                                             <span class="help-block">Hit 'Enter' after each entry.</span>
                                          </div>
                                       </div>
                                    </div>
                                    <!--/span-->
                                 </div>

                                 <div class="row-fluid">
                                    <div class="span6 ">
                                       <div class="control-group">
                                          <label class="control-label" >Favourite Movies</label>
                                          <div class="controls">
                                             <input type="text" id="fav_movies" class="m-wrap span12 select2">
                                             <input type="hidden" id="fav_movies_hidden" name="fav_movies" value="<?php echo $html_array['profile_settings']['fav_movies']; ?>">
                                             <span class="help-block">Hit 'Enter' after each entry.</span>
                                          </div>
                                       </div>
                                    </div>
                                    <!--/span-->
                                    <div class="span6 ">
                                       <div class="control-group">
                                          <label class="control-label" >Favourite Music</label>
                                          <div class="controls">
                                             <input type="text" id="fav_music"  class="m-wrap span12 select2">
                                             <input type="hidden" id="fav_music_hidden" name="fav_music" value="<?php echo $html_array['profile_settings']['fav_music']; ?>">
                                             <span class="help-block">Hit 'Enter' after each entry.</span>
                                          </div>
                                       </div>
                                    </div>
                                    <!--/span-->
                                 </div>

                                 <div class="row-fluid">
                                    <div class="span6">
                                       <div class="control-group">
                                          <label class="control-label" >About</label>
                                          <div class="controls">
                                             <textarea class="m-wrap span12" rows="3" name="about"><?php echo $html_array['profile_settings']['about']; ?></textarea>
                                             <span class="help-block">Talk a little about yourself.</span>
                                          </div>
                                       </div>
                                    </div>

                                    <div class="span6 ">
                                       <div class="control-group">
                                          <label class="control-label" >Languages</label>
                                          <div class="controls">
                                             <input type="text" id="languages"  class="m-wrap span12 select2">
                                             <input type="hidden" id="languages_hidden" name="languages" value="<?php echo $html_array['profile_settings']['languages']; ?>">
                                             <span class="help-block">Hit 'Enter' after each entry.</span>
                                          </div>
                                       </div>
                                    </div>

                                 </div>

                                 <div class="clearfix"></div>
                                 <br>
                                 <input type="submit" class="btn light-green" value="Save Changes in Tab" />
                              </form>

                           </div>

                        </div>


                        <div class="row-fluid section profile_picture">

                           <div class="span4">
                              <h3>Current Photo</h3>

                              <div class="current_photo_wrapper">
                                 <img src="https://d3hk76y82gbwcp.cloudfront.net/pr_imgs/<?php echo $html_array['profile_settings']['image']; ?>" alt="profile picture" class="current_photo" />
                              </div>
                              
                              <div class="clearfix"></div>

                              <div id="profile_picture_progress" class="progress progress_thin progress-success progress-striped" style="display: none;">
                                 <div class="bar"></div>
                              </div>

                              <br>

                              <!-- <h3>Upload Photo</h3> -->

                              <div class="profile_picture_upload">
                                 <!-- <div id="profile_picture_preview_wrapper" style="display: none;"><img src="#" id="profile_picture_preview" style="display:none;" /></div> -->

                                 <span class="btn light-green fileinput-button">
                                      <i class="icon-plus"></i>
                                      <span>Upload Photo</span>
                                      <!-- The file input field used as target for the file upload widget -->
                                      <input id="profile_picture" type="file" name="image_file">
                                 </span>
                                 
                              </div>

                              <!-- <form method="post" action="profile_settings?action=upload_profile_picture" id="upload_profile_picture" name="upload_profile_picture" enctype="multipart/form-data">
                                 <div class="controls">
                                    <div class="thumbnail upload">
                                    </div>
                                 </div>
                                 <div class="space10"></div>

                                 <div class="fileupload fileupload-new" data-provides="fileupload">
                                    <div class="input-append">
                                       <div class="uneditable-input">
                                          <i class="icon-file fileupload-exists"></i> 
                                          <span class="fileupload-preview"></span>
                                       </div>
                                       <span class="btn btn-file">
                                          <span class="fileupload-new">Select file</span>
                                          <span class="fileupload-exists">Change</span>
                                          <input type="hidden" name="MAX_FILE_SIZE" value="2097152" />
                                          <input name="image_file" type="file" required />
                                       </span>
                                       <a href="#" class="btn fileupload-exists remove" data-dismiss="fileupload">Remove</a>
                                    </div>
                                 </div>
                                 <div class="clearfix"></div>
                                 <div class="submit-btn">
                                    <input type="submit" name="submit" value="Upload" id="upload" class="btn light-green">
                                 </div>
                              </form> -->
                           </div>

                           <div class="span8">
                              <h3>Choose From Gallery</h3>

                              <form action="profile_settings?action=select_profile_picture" method="post" class="gallery_pictures">
                                 <div class="row-fluid">
                                    <div class="control-group">
                                       <div class="controls">
                                          <div class="row-fluid">
                                             <div class="span2">
                                                <img src="img/profile/thumbs/beach.jpg" alt="Beach" /><br>
                                                <label class="radio">
                                                   <input type="radio" name="profile_picture" value="beach" />
                                                </label>
                                             </div>
                                             <div class="span2">
                                                <img src="img/profile/thumbs/bees.jpg" alt="Bees" /><br>
                                                <label class="radio">
                                                   <input type="radio" name="profile_picture" value="bees" />
                                                </label>
                                             </div>
                                             <div class="span2">
                                                <img src="img/profile/thumbs/boats.jpg" alt="Boats" /><br>
                                                <label class="radio">
                                                   <input type="radio" name="profile_picture" value="boats" />
                                                </label>
                                             </div>
                                             <div class="span2">
                                                <img src="img/profile/thumbs/bridge.jpg" alt="Bridge" /><br>
                                                <label class="radio">
                                                   <input type="radio" name="profile_picture" value="bridge" />
                                                </label>
                                             </div>
                                             <div class="span2">
                                                <img src="img/profile/thumbs/broken_car.jpg" alt="Broken Car" /><br>
                                                <label class="radio">
                                                   <input type="radio" name="profile_picture" value="broken_car" />
                                                </label>
                                             </div>
                                             <div class="span2">
                                                <img src="img/profile/thumbs/cat.jpg" alt="Cat" /><br>
                                                <label class="radio">
                                                   <input type="radio" name="profile_picture" value="cat" />
                                                </label>
                                             </div>
                                          </div>

                                          <div class="row-fluid">
                                             <div class="span2">
                                                <img src="img/profile/thumbs/coconut.jpg" alt="Coconut" /><br>
                                                <label class="radio">
                                                   <input type="radio" name="profile_picture" value="coconut" />
                                                </label>
                                             </div>
                                             <div class="span2">
                                                <img src="img/profile/thumbs/dog.jpg" alt="Dog" /><br>
                                                <label class="radio">
                                                   <input type="radio" name="profile_picture" value="dog" />
                                                </label>
                                             </div>
                                             <div class="span2">
                                                <img src="img/profile/thumbs/field.jpg" alt="Field" /><br>
                                                <label class="radio">
                                                   <input type="radio" name="profile_picture" value="field" />
                                                </label>
                                             </div>
                                             <div class="span2">
                                                <img src="img/profile/thumbs/flower.jpg" alt="Flower" /><br>
                                                <label class="radio">
                                                   <input type="radio" name="profile_picture" value="flower" />
                                                </label>
                                             </div>
                                             <div class="span2">
                                                <img src="img/profile/thumbs/fruits.jpg" alt="Fruits" /><br>
                                                <label class="radio">
                                                   <input type="radio" name="profile_picture" value="fruits" />
                                                </label>
                                             </div>
                                             <div class="span2">
                                                <img src="img/profile/thumbs/horses.jpg" alt="Horses" /><br>
                                                <label class="radio">
                                                   <input type="radio" name="profile_picture" value="horses" />
                                                </label>
                                             </div>
                                          </div>

                                          <div class="row-fluid">
                                             <div class="span2">
                                                <img src="img/profile/thumbs/kitten.jpg" alt="Kitten" /><br>
                                                <label class="radio">
                                                   <input type="radio" name="profile_picture" value="kitten" />
                                                </label>
                                             </div>
                                             <div class="span2">
                                                <img src="img/profile/thumbs/lights.jpg" alt="Lights" /><br>
                                                <label class="radio">
                                                   <input type="radio" name="profile_picture" value="lights" />
                                                </label>
                                             </div>
                                             <div class="span2">
                                                <img src="img/profile/thumbs/rocks.jpg" alt="Rocks" /><br>
                                                <label class="radio">
                                                   <input type="radio" name="profile_picture" value="rocks" />
                                                </label>
                                             </div>
                                             <div class="span2">
                                                <img src="img/profile/thumbs/sunflowers.jpg" alt="Sunflower" /><br>
                                                <label class="radio">
                                                   <input type="radio" name="profile_picture" value="sunflowers" />
                                                </label>
                                             </div>
                                             <div class="span2">
                                                <img src="img/profile/thumbs/waterfall.jpg" alt="Waterfall" /><br>
                                                <label class="radio">
                                                   <input type="radio" name="profile_picture" value="waterfall" />
                                                </label>
                                             </div>
                                             <div class="span2">
                                                <img src="img/profile/thumbs/yellow_lights.jpg" alt="Yellow Lights" /><br>
                                                <label class="radio">
                                                   <input type="radio" name="profile_picture" value="yellow_lights" />
                                                </label>
                                             </div>
                                          </div>
                                             
                                          <div class="clearfix"></div>
                                          
                                          <input type="submit" class="btn light-green" value="Update Picture" />
                                       </div>
                                    </div>
                                 </div>
                              </form>
                           </div>

                           <div class="clearfix"></div>

                        </div>


                        <div class="row-fluid section security">

                           <div class="span6">
                              <h3>Change Password</h3><br>

                              <form action="profile_settings?action=change_password" method="post" class="change_password">
                                 <label class="control-label">Current Password</label>
                                 <input type="password" class="m-wrap span8" name="current_password" id="current_password" />
                                 <label class="control-label">New Password</label>
                                 <input type="password" class="m-wrap span8" name="new_password" id="new_password" />
                                 <label class="control-label">Confirm New Password</label>
                                 <input type="password" class="m-wrap span8" name="new_password2" id="new_password2" />
                                 
                                 <div class="clearfix"></div>
                                 <br>
                                 <input type="submit" class="btn light-green" value="Change Password" />
                              </form>
                           </div>

                           <div class="span4">
                              <h3>Screen Lock Timeout</h3><br>

                              The Screen Lock is activated after a certain period of inactivity. Your may set a duration below or turn off the Screen Lock timeout altogether.
                              <br><br>
                              Remember, you can always lock your screen instantly by hitting the 'Esc' key while logged in.
                              <br><br>

                              <form action="profile_settings?action=set_screen_lock" method="post">
                                 <label class="control-label">Screen Lock Tiemout Duration</label>
                                 <select name="screen_lock_timeout" class="m-wrap span12">
                                    <?php echo $html_array['profile_settings']['lock_timeout_opt']; ?>
                                 </select>
                                 
                                 <div class="clearfix"></div>
                                 <br>
                                 <input type="submit" class="btn light-green" value="Save Changes" />
                              </form>

                           </div>


                           <div id="password_change_error" class="modal hide fade" tabindex="-1" data-focus-on="input:first">


                              <div class="modal-header">
                                 <h3><i class="icon-lock"></i>&nbsp;&nbsp;Password Change</h3>
                              </div>

                              <div class="modal-body">

                              </div>   
                                 
                              <div class="modal-footer">
                                 <input type="submit" id="submit" data-dismiss="modal" class="btn light-green right" value="OK" />
                              </div>

                           </div>

                        </div>

                     </div>
                  </div>  

					</div>
				</div>
				<!-- END PAGE CONTENT -->

			</div>
		 
         <!-- END PAGE CONTAINER-->       
      </div>
      <!-- BEGIN PAGE -->
   </div>
   <!-- END PAGE CONTAINER--> 
   <!-- END CONTAINER -->
   <?php
      require_once("inc/footer.php");
   ?>
</body>
<!-- END BODY -->
</html>
<?php
   require_once("inc/session_end.php");
?>