<?php

	session_start();

	$business_name = $_SESSION['subdomain'];

	$table = $_POST['table'];
	$table = str_replace(",", "", $table);

	$table_name = $_POST['table_name'];


	preg_match('/<table(>| [^>]*>)(.*?)<\/table( |>)/is', $table, $matches);
	$table = $matches[2];
	preg_match_all('/<tr(>| [^>]*>)(.*?)<\/tr( |>)/is', $table, $matches);
	$rows = $matches[2];

	$count = 0;
	foreach ($rows as $row){

		if($count == 0) {
			preg_match_all('/<th(>| [^>]*>)(.*?)<\/th( |>)/is', $row, $matches);
			$out[] = strip_tags(implode(',', $matches[2]));

		} else {

			preg_match_all('/<td(>| [^>]*>)(.*?)<\/td( |>)/is', $row, $matches);
			$out[] = strip_tags(implode(',', $matches[2]));
		}

		$count++;
	}

	$csv_output = implode("\n", $out);

	$filename = $business_name . "_" . $table_name . "_" . time() . ".csv";
	
	// apache_setenv('no-gzip', 1);  
	// ini_set('zlib.output_compression', 0); 

	//header("Content-type: application/vnd.ms-excel");
	header("Content-disposition: csv" . date("Y-m-d") . ".csv");
	//header( 'X-Sendfile: ' .$filename ); -- currently not working, so diasbled it.
	header( "Content-disposition: attachment; filename=$filename");
	

	print $csv_output;
	

?>