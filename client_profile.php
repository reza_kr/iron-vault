<?php
    require_once("inc/session_start.php");
    require_once("functions.php");
    
    $html_array = load_page_html();
    
    require_once("inc/head.php");
    ?>
<!-- BEGIN BODY -->
<body class="fixed-top page-sidebar-fixed 
    <?php echo $html_array['page_sidebar_closed']; ?>">
    <?php require_once("inc/top_menu.php"); ?>
    <!-- BEGIN CONTAINER -->
    <div class="page-container row-fluid">
        <?php require_once("inc/main_menu.php"); ?>
        <!-- BEGIN PAGE -->
        <div class="page-content">
            <div class="container-fluid">
                <!-- END PAGE HEADER-->
                <!-- BEGIN PAGE CONTENT-->
                <div class="row-fluid">
                    <div class="span12">
                        <div class="portlet box blue">
                            <div class="portlet-title">
                                <div class="caption">
                                    <a href="clients.php" class="btn portlet_title_btn">
                                    <i class="icon-arrow-left"></i>
                                    </a>
                                    <i class="icon-group"></i> Client Profile
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div class="row-fluid">
                                    <div class="span4 profile-info">
                                        <h3>
                                            <?php echo $html_array['client_profile']['display_name']; ?>
                                        </h3>
                                        <p>
                                            <?php echo $html_array['client_profile']['business_name']; ?>
                                        </p>
                                        <table class="client_info_table">
                                            <?php echo $html_array['client_profile']['info_table']; ?>
                                        </table>
                                        <br>
                                        <br>
                                        <h3>Client Notes</h3>
                                        <hr class="grey">
                                        <textarea class="client_notes_edit m-wrap" id="<?php echo $html_array['client_profile']['client_id']; ?>" placeholder="Click here and start typing. Remember to save!"><?php echo $html_array['client_notes']['text']; ?></textarea>
                                        <p class="client_notes_last_update">
                                            <?php echo $html_array['client_notes']['last_changed']; ?>
                                        </p>
                                        <a  class="btn light-green save_client_notes" href="#">Save Notes</a>
                                    </div>
                                    <div class="span8">
                                        <?php echo $html_array['client_profile']['map']; ?>
                                        <h3>
                                            <?php echo $html_array['client_profile']['display_name']; ?>'s Invoices
                                        </h3>
                                        <hr class="grey">
                                        <div class="clearfix">
                                            <div class="btn-group">
                                                <a href="new_invoice?client_id=<?php echo $html_array['client_profile']['client_id']; ?>" class="btn light-green">
                                                <i class="icon-book"></i>&nbsp;&nbsp;Create New Invoice
                                                </a>
                                            </div>
                                            <?php echo $html_array['export_tools']; ?>
                                        </div>
                                        <table class="table table-bordered table-striped table-condensed cf invoices saveaspdf" id="data_table">
                                            <thead class="cf">
                                                <tr>
                                                    <th class="sorting">Invoice #</th>
                                                    <th class="sorting">Date Created</th>
                                                    <th class="sorting">Last Modified</th>
                                                    <th class="sorting">Prepared By</th>
                                                    <th class="sorting">Status</th>
                                                    <th class="sorting">Total</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php echo $html_array['client_invoices']; ?>
                                            </tbody>
                                        </table>
                                        <br>
                                        <br>
                                        <a  class="btn light-red delete_client right" href="#delete_client_modal" data-toggle="modal">Delete Client</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="portlet box blue client_files">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="icon-file"></i>
                                    Client Files
                                </div>
                            </div>
                            <div class="portlet-body attachments">
                                <!--<span class="box-message">There are no attachments for this invoice. To add one now, select your file and hit 'Upload'</span>-->
                                <!-- <div class="float-right"><form method="post" action="doc_upload" id="doc_upload" name="doc_upload" enctype="multipart/form-data"><div class="controls"><div class="thumbnail thumbnail-cloud" ></div></div><div class="space10"></div><div class="fileupload fileupload-new" data-provides="fileupload"><div class="input-append"><div class="uneditable-input"><i class="icon-file fileupload-exists"></i><span class="fileupload-preview"></span></div><span class="btn btn-file"><span class="fileupload-new">Select file</span><span class="fileupload-exists">Change</span><input type="file" name="FileUpload" id="FileUpload" class="file"><input name="txtApiKey" type="hidden" id="txtApiKey" value="354994856"><input name="client_id" type="hidden" value="
                                    <?php //echo $_GET['id']; ?>"></span><a href="#" class="btn fileupload-exists remove" data-dismiss="fileupload">Remove</a></div></div><div class="clearfix"></div><div class="submit-btn"><input type="submit" name="submit" value="Upload" id="upload" class="btn light-green"></div></form></div> -->
                                <div class="attachment">
                                    <br>
                                    <i class="icon-cloud-upload xl_icon"></i>
                                    <br><br>
                                    <div class="btn-group">
                                        <a href="#upload_client_files_modal" data-toggle="modal" class="btn light-green"><i class="icon-plus"></i> Add File(s)</a>
                                    </div>
                                </div>

                                <?php echo $html_array['client_files']; ?>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div id="update_client_info_modal" class="modal hide fade" tabindex="-1" data-focus-on="input:first">
                            <form class="zero_margin" action="client_profile?action=update_client_profile" method="post">
                                <div class="modal-header">
                                    <i class="icon-remove right" data-dismiss="modal" aria-hidden="true"></i>
                                    <h3>
                                        <i class="icon-user"></i>&nbsp;&nbsp;Update Client Profile
                                    </h3>
                                </div>
                                <div class="modal-body">
                                    <div class="row-fluid">
                                        <div class="span6 ">
                                            <div class="control-group">
                                                <label class="control-label">Type</label>
                                                <div class="controls">
                                                    <select name="client_type" id="client_type" class="m-wrap span12">
                                                        <option value="individual">Individual</option>
                                                        <option value="business">Business</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    <hr class="grey">
                                    <div class="row-fluid individual">
                                        <div class="span6 ">
                                            <div class="control-group">
                                                <label class="control-label">First Name</label>
                                                <div class="controls">
                                                    <input type="text" name="first_name" id="first_name" class="m-wrap span12" />
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                        <div class="span6 ">
                                            <div class="control-group">
                                                <label class="control-label">Last Name</label>
                                                <div class="controls">
                                                    <input type="text" name="last_name" id="last_name" class="m-wrap span12" />
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    <div class="row-fluid company">
                                        <div class="span6 ">
                                            <div class="control-group">
                                                <label class="control-label">Business Name</label>
                                                <div class="controls">
                                                    <input type="text" name="business_name" id="business_name" class="m-wrap span12" />
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                        <div class="span6 ">
                                            <div class="control-group">
                                                <label class="control-label">Email</label>
                                                <div class="controls">
                                                    <input type="text" name="business_email" id="business_email" class="m-wrap span12" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row-fluid individual">
                                        <div class="span6 ">
                                            <div class="control-group">
                                                <label class="control-label">Email</label>
                                                <div class="controls">
                                                    <input type="text" name="individual_email" id="individual_email" class="m-wrap span12" />
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                        <div class="span6 ">
                                            <div class="control-group">
                                                <label class="control-label" >Gender</label>
                                                <div class="controls">
                                                    <select  class="m-wrap span12" name="gender" id="gender">
                                                        <option value="male">Male</option>
                                                        <option value="female">Female</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    <div class="row-fluid">
                                        <div class="span6 ">
                                            <div class="control-group">
                                                <label class="control-label">Primary Phone</label>
                                                <div class="controls">
                                                    <input type="text" name="primary_phone" id="primary_phone" class="m-wrap span12" />
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                        <div class="span6 ">
                                            <div class="control-group">
                                                <label class="control-label" >Alternative Phone</label>
                                                <div class="controls">
                                                    <input type="text" name="alternative_phone" id="alternative_phone" class="m-wrap span12" />
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    <div class="row-fluid">
                                        <div class="span12 ">
                                            <div class="control-group">
                                                <label class="control-label">Address</label>
                                                <div class="controls">
                                                    <input type="text" name="address" id="update_client_geo_address" class="m-wrap span12" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="modal-footer">
                                    <input type="hidden" name="client_id" id="client_id" value="
                                        <?php echo $_GET['id']; ?>" />
                                    <input type="submit" id="submit" class="btn light-green right" value="Update Client Info" />
                                    <input type="button" data-dismiss="modal" class="btn" value="Cancel" />
                                </div>
                            </form>
                            <div class="clearfix"></div>
                        </div>

                        <div id="delete_client_modal" class="modal hide fade" tabindex="-1" data-focus-on="input:first">
                            <form class="zero_margin" action="#" method="post">
                                <input type="hidden" id="client_id" value="
                                    <?php echo $_GET['id']; ?>" />
                                <input type="hidden" id="client_name" value="
                                    <?php echo $html_array['client_profile']['display_name']; ?>" />
                                <div class="modal-header">
                                    <i class="icon-remove right" data-dismiss="modal" aria-hidden="true"></i>
                                    <h3>
                                        <i class="icon-trash"></i>&nbsp;&nbsp;Delete Client
                                    </h3>
                                </div>
                                <div class="modal-body"></div>
                                <div class="modal-footer">
                                    <input type="submit" id="submit" class="btn light-red right delete" value="Delete" />
                                    <input type="button" data-dismiss="modal" class="btn" value="Cancel" />
                                </div>
                            </form>
                        </div>

                        <div id="upload_client_files_modal" class="modal hide fade" tabindex="-1" data-focus-on="input:first">
                            <form class="zero_margin" action="#" method="post">
                                <input type="hidden" id="client_id" value="<?php echo $_GET['id']; ?>" />
                                <div class="modal-header">
                                    <i class="icon-remove right" data-dismiss="modal" aria-hidden="true"></i>
                                    <h3>
                                        <i class="icon-cloud-upload"></i>&nbsp;&nbsp;Upload File(s)
                                    </h3>
                                </div>
                                <div class="modal-body">
                                    <div class="file_upload">
                                        <div id="file_preview_wrapper" style="display: none;">
                                            <img src="#" id="file_preview" style="display:none;" />
                                        </div>
                                        <span class="btn light-green fileinput-button">
                                            <i class="icon-plus"></i>
                                            <span>Add File(s)</span>
                                            <!-- The file input field used as target for the file upload widget -->
                                            <input id="file" type="file" name="file" multiple>
                                        </span>
                                        <div id="file_progress" class="progress progress_thin progress-success progress-striped" style="display: none;">
                                            <div class="bar"></div>
                                        </div>
                                    </div>

                                    <table class="added_files table table-bordered table-striped table-condensed" style="display:none;">
                                        <tr>
                                            <th>Filename</th>
                                            <th>Filesize</th>
                                            <th>Progress</th>
                                        </tr>
                                    </table>
                                </div>
                                <div class="modal-footer">
                                    <a href="#" class="btn light-red disabled cancel_uploads" >Cancel Uploads</a>
                                    <a href="#" data-dismiss="modal" class="btn light-green right">Hide</a>
                                </div>
                            </form>
                        </div>

                        <div id="delete_client_file_modal" class="modal hide fade" tabindex="-1" data-focus-on="input:first">
                            <form class="zero_margin" action="#" method="post">
                                <input type="hidden" id="client_id" value="
                                    <?php echo $_GET['id']; ?>" />
                                <input type="hidden" id="file_id" value="" />
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                    <h3>
                                        <i class="icon-remove"></i>&nbsp;&nbsp;Delete File
                                    </h3>
                                </div>
                                <div class="modal-body"></div>
                                <div class="modal-footer">
                                    <input type="submit" id="submit" class="btn light-red right delete" value="Delete" />
                                    <input type="button" data-dismiss="modal" class="btn" value="Cancel" />
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- END PAGE CONTENT-->
            </div>
            <!-- END PAGE CONTAINER-->
        </div>
        <!-- BEGIN PAGE -->
    </div>
    <!-- END PAGE CONTAINER-->
    <!-- END CONTAINER -->
    <?php
        require_once("inc/footer.php");
        ?>
</body>
<!-- END BODY -->
</html>
<?php
    require_once("inc/session_end.php");
    ?>