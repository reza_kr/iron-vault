<?php
   require_once("inc/session_start.php");
   require_once("functions.php");

   $html_array = load_page_html();

   require_once("inc/head.php");
?>
<!-- BEGIN BODY -->
<body class="fixed-top page-sidebar-fixed <?php echo $html_array['page_sidebar_closed']; ?>">
   <?php require_once("inc/top_menu.php"); ?>
   <!-- BEGIN CONTAINER -->   
   <div class="page-container row-fluid">

      <?php require_once("inc/main_menu.php"); ?>

      <!-- BEGIN PAGE -->
      <div class="page-content">
         <div class="container-fluid">
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">

               <div class="span12">
                  <div class="portlet box blue calendar">
                     <div class="portlet-title">
                        <div class="caption"><i class="icon-time"></i>Work Schedule</div>
                     </div>
                     <div class="portlet-body">
                        <div class="row-fluid">
                           <div class="span12">
                              <div id="work_schedule" class="has-toolbar"></div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>

               <a id="show_add_event_modal" data-toggle="modal" href="#add_event_modal"></a>
               <a id="show_edit_event_modal" data-toggle="modal" href="#edit_event_modal"></a>

               <div id="add_event_modal" class="modal fade" tabindex="-1" data-focus-on="input:first">
                  <form class="form-horizontal zero_margin" action="#" name="calendar_add_event">
                     <div class="modal-header">
                        <i class="icon-remove right" data-dismiss="modal" aria-hidden="true"></i>
                        <h3><i class="icon-time"></i>&nbsp;&nbsp;New Shift</h3>
                     </div>
                     <div class="modal-body">
                        <div class="control-group">
                           <label class="control-label">Start</label>
                           <div class="controls">
                              <div class="input-append date span8">
                                 <input type="text" class="m-wrap date-picker" id="start_date" placeholder="Date">
                                 <span class="add-on"><i class="icon-calendar"></i></span>

                                 <input type="text" class="m-wrap time-picker" id="start_time" placeholder="Time">
                                 <span class="add-on"><i class="icon-time"></i></span>
                              </div>
                           </div>
                        </div>
                        <div class="control-group duration">
                           <label class="control-label">Duration</label>
                           <div class="controls">
                              <select name="duration" id="duration" class="m-wrap span4">
                                 <option value="60">1 hour</option>
                                 <option value="120">2 hours</option>
                                 <option value="240">4 hours</option>
                                 <option value="360">6 hours</option>
                                 <option value="480" selected="selected">8 hours</option>
                                 <option value="720">12 hours</option>
                                 <option value="1080">18 hours</option>
                                 <option value="custom">Custom</option>
                              </select>
                           </div>
                        </div>
                        <div class="control-group end_date_time">
                           <label class="control-label">End</label>
                           <div class="controls">
                              <div class="input-append date span8">
                                 <input type="text" value="" class="m-wrap date-picker" id="end_date" placeholder="Date">
                                 <span class="add-on"><i class="icon-calendar"></i></span>

                                 <input type="text" value="" class="m-wrap time-picker" id="end_time" placeholder="Time">
                                 <span class="add-on"><i class="icon-time"></i></span>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="modal-footer">
                        <input type="button" id="submit" data-dismiss="modal" class="btn light-blue right add_shift" value="Add Shift" />
                        <input type="button" data-dismiss="modal" class="btn" value="Cancel" />
                     </div>
                  </form>
               </div>

               <div id="edit_event_modal" class="modal fade" data-focus-on="input:first" tabindex="-1">
                  <div class="modal-dialog">
                     <div class="modal-content">
                        <form class="form-horizontal zero_margin" action="#" name="calendar_edit_event">
                           <input type="hidden" id="id" name="id" class="m-wrap span8" />

                           <div class="modal-header">
                              <i class="icon-remove right" data-dismiss="modal" aria-hidden="true"></i>
                              <h3><i class="icon-time"></i>&nbsp;&nbsp;Edit Shift</h3>
                           </div>
                           <div class="modal-body">
                              <div class="control-group">
                                 <label class="control-label">Start</label>
                                 <div class="controls">
                                    <div class="input-append date span8">
                                       <input type="text" class="m-wrap date-picker" id="start_date" placeholder="Date">
                                       <span class="add-on"><i class="icon-calendar"></i></span>

                                       <input type="text" class="m-wrap time-picker" id="start_time" placeholder="Time">
                                       <span class="add-on"><i class="icon-time"></i></span>
                                    </div>
                                 </div>
                              </div>
                              <div class="control-group duration">
                                 <label class="control-label">Duration</label>
                                 <div class="controls">
                                    <select name="duration" id="duration" class="m-wrap span4">
                                       <option value="60">1 hour</option>
                                       <option value="120">2 hours</option>
                                       <option value="240">4 hours</option>
                                       <option value="360">6 hours</option>
                                       <option value="480">8 hours</option>
                                       <option value="720">12 hours</option>
                                       <option value="1080">18 hours</option>
                                       <option value="custom">Custom</option>
                                    </select>
                                 </div>
                              </div>
                              <div class="control-group end_date_time">
                                 <label class="control-label">End</label>
                                 <div class="controls">
                                    <div class="input-append date span8">
                                       <input type="text" value="" class="m-wrap date-picker" id="end_date" placeholder="Date">
                                       <span class="add-on"><i class="icon-calendar"></i></span>

                                       <input type="text" value="" class="m-wrap time-picker" id="end_time" placeholder="Time">
                                       <span class="add-on"><i class="icon-time"></i></span>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="modal-footer">
                              <input type="button" id="submit" data-dismiss="modal" class="btn light-blue right edit_shift" value="Update Shift" />
                              <a href="#delete_event_modal" data-dismiss="modal" data-toggle="modal" class="btn light-red left">Delete</a>
                           </div>
                        </form>
                     </div>
                  </div>
               </div>

               <div id="delete_event_modal" class="modal fade" tabindex="-1" data-focus-on="input:first">

                  <form class="zero_margin" action="#" method="post">

                     <div class="modal-header">
                        <i class="icon-remove right" data-dismiss="modal" aria-hidden="true"></i>
                        <h3><i class="icon-time"></i>&nbsp;&nbsp;Delete Shift</h3>
                     </div>

                     <div class="modal-body">

                     Are you sure you want to delete this Shift?

                               <input type="hidden" class="event_id" name="event_id">

                     </div>   
                        
                     <div class="modal-footer">
                        <input type="submit" id="submit" data-dismiss="modal" class="btn light-red right delete" value="Delete">
                        <input type="button" data-dismiss="modal" class="btn" value="Cancel">
                     </div>

                  </form>
               </div>

            </div>

            <!-- END PAGE CONTENT--> 
         </div>
         <!-- END PAGE CONTAINER-->       
      </div>
      <!-- BEGIN PAGE -->
   </div>
   <!-- END PAGE CONTAINER--> 
   <!-- END CONTAINER -->
   <?php
      require_once("inc/footer.php");
   ?>
</body>
<!-- END BODY -->
</html>
<?php
   require_once("inc/session_end.php");
?>