<?php
    //Using this script instead of the regular elfinder connector.php file because this is in the root folder and the file path is even MORE hidden.
    //Shows up as... https://sunsetblvd.ironvault.ca/file?cmd=file&target=s1_R2FsbGVyeS9vY2Vhbi12aWV3LmpwZw (example)
    require_once("classes/Search.class.php");
    require_once("inc/session_start.php");
    require_once("functions.php");


    require_once('plugins/elfinder/php/elFinderConnector.class.php');
    require_once('plugins/elfinder/php/elFinder.class.php');
    require_once('plugins/elfinder/php/elFinderVolumeDriver.class.php');
    require_once('plugins/elfinder/php/elFinderVolumeS3.class.php');

    // Required for MySQL storage connector
    // include_once dirname(__FILE__).DIRECTORY_SEPARATOR.'elFinderVolumeMySQL.class.php';
    // Required for FTP connector support
    // include_once dirname(__FILE__).DIRECTORY_SEPARATOR.'elFinderVolumeFTP.class.php';


    $account_id = $_SESSION['account_id'];
    $user_id = $_SESSION['user_id'];

    if($account_id != "" && $user_id != "") {

        /**
         * Simple function to demonstrate how to control file access using "accessControl" callback.
         * This method will disable accessing files/folders starting from  '.' (dot)
         *
         * @param  string  $attr  attribute name (read|write|locked|hidden)
         * @param  string  $path  file path relative to volume root directory started with directory separator
         * @return bool|null
         **/
        function access($attr, $path, $data, $volume) {
        	return strpos(basename($path), '.') === 0       // if file/folder begins with '.' (dot)
        		? !($attr == 'read' || $attr == 'write')    // set read+write to false, other (locked+hidden) set to true
        		:  null;                                    // else elFinder decide it itself
        }

        $opts = array(
        	'roots' => array(
        		    array(
                    'driver' => 'S3',
                    "s3" => array(
                        "key" => "AKIAJKBMGCMLYBHN77PQ",
                        "secret" => "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",
                        "region" => "us-west-2"
                    ),
                    "bucket" => "ironvault",
                    "acl" => "private",
                    'path' => 'storage/' . $account_id . "/" . $user_id,
                    "uploadMaxSize" => '10G'
                )
        	),
          'uploadOverwrite' => true
        );

        // run elFinder
        $connector = new elFinderConnector(new elFinder($opts));
        $connector->run();
    }

?>