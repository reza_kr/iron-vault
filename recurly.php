<?php

   require_once("inc/session_start.php");

   require_once("functions.php");

   $html_array = load_page_html();

   require_once("inc/head.php");



?>

<!-- BEGIN BODY -->

<body class="fixed-top front-end">

   

   <?php require_once("inc/frontend_menu.php"); ?>



   <!-- BEGIN CONTAINER -->   

   <div class="page-container row-fluid full-width-page">

      

      <!-- BEGIN PAGE -->

      <div class="page-content no-min-height">

         <!-- BEGIN PAGE CONTAINER-->

         <div class="container-fluid promo-page">

            <!-- BEGIN PAGE CONTENT-->

            <div class="row-fluid">

               <div class="span12">

               <div id="billing_form"></div>

               </div>

            </div>

         </div>

         <!-- END PAGE CONTENT-->

      </div>

      <!-- END PAGE CONTAINER--> 

   </div>

   <!-- END PAGE --> 

   <!-- END CONTAINER -->



   <?php

      require_once("inc/frontend_footer.php");

   ?>


   <script>
      jQuery(document).ready(function() {
      Recurly.config({
         subdomain: 'ironvault',
         currency: 'CAD'
      });

      Recurly.buildSubscriptionForm({
          target: '#billing_form',
          signature: '<?php echo $html_array["signature"]; ?>',
          successURL: 'recurly.php?ref=success',
          planCode: 'basic-plan',
          distinguishContactFromBillingInfo: true,
          collectCompany: true,
          collectContactInfo: true,
          termsOfServiceURL: 'https://ironvault.ca/terms_of_service.php',
          acceptedCards: ['mastercard',
                          'discover',
                          'american_express', 
                          'visa'],
          account: {
            firstName: 'Joe',
            lastName: 'User',
            email: 'test@example.net',
            phone: '555-555-5555',
            companyName: 'Acme'
          },
          billingInfo: {
            firstName: 'Joe',
            lastName: 'User',
            address1: '123 somestreet',
            address2: '45',
            city: 'San Francisco',
            zip: '94107',
            state: 'CA',
            country: 'US',
            cardNumber: '4111-1111-1111-1111',
            CVV: '123'
          }
        });

      });
   </script>

</body>

<!-- END BODY -->

</html>



<?php

   require_once("inc/session_end.php");

?>