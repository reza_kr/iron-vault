<?php
   require_once("inc/session_start.php");
   require_once("functions.php");

   $html_array = load_page_html();

   require_once("inc/head.php");
   
?>
<!-- BEGIN BODY -->
<body class="login">

  <div class="container-fluid">

    <br><h2 class="business_name"><?php echo $_SESSION['business_name']; ?></h2>

    <div id="setup_wizard_modal" class="modal fade in container" tabindex="-1" data-focus-on="input:first">
        <div class="modal-header">
          <h3><i class="icon-cogs"></i>&nbsp;&nbsp;Setup Wizard</h3>
        </div>

        <div class="modal-body">

            <!-- BEGIN LOGIN FORM -->

            <?php

                // if($_GET['e'] == "wrong_password") {
                //   echo '<div class="form_error">
                //           <span><i class="icon-warning-sign"></i> Incorrect password. Please try again.</span>
                //         </div>';
                // }

            ?>

            <div class="row-fluid">

	            <form action="" class="form-horizontal" id="setup_wizard">
					<div class="form-wizard">
						<div class="navbar steps">
							<div class="navbar-inner">
								<ul class="row-fluid">
									<li class="span3">
										<a href="#tab1" data-toggle="tab" class="step active">
										<span class="number">1</span>
										<span class="desc">Welcome Aboard!</span>   
										</a>
									</li>
									<li class="span3">
										<a href="#tab2" data-toggle="tab" class="step ">
										<span class="number">2</span>
										<span class="desc">Personal Info</span>   
										</a>
									</li>
									<li class="span3">
										<a href="#tab3" data-toggle="tab" class="step">
										<span class="number">3</span>
										<span class="desc">Business Info</span>   
										</a>
									</li>
									<li class="span3">
										<a href="#tab4" data-toggle="tab" class="step">
										<span class="number">4</span>
										<span class="desc">Invite Users</span>   
										</a> 
									</li>
								</ul>
							</div>
						</div>
						<div id="bar" class="progress progress-success progress-striped">
							<div class="bar"></div>
						</div>
						<div class="tab-content">
							<div class="alert alert-error hide">
								<button class="close" data-dismiss="alert"></button>
								You have some form errors. Please check below.
							</div>
							<div class="alert alert-success hide">
								<button class="close" data-dismiss="alert"></button>
								Your form validation is successful!
							</div>
							<div class="tab-pane active" id="tab1">
								<h3 class="block">Welcome aboard! Just a few more things to get you up and running...</h3>
								
								<div class="row-fluid">
									<div class="span2 setup_wizard_icons">
										<i class="icon-user xl_icon"></i>
									</div>
									<div class="span1"></div>
									<div class="span2 setup_wizard_icons">
										<i class="icon-briefcase xl_icon"></i>
									</div>
									<div class="span1"></div>
									<div class="span2 setup_wizard_icons">
										<i class="icon-group xl_icon"></i>
									</div>
									<div class="span1"></div>
									<div class="span2 setup_wizard_icons">
										<i class="icon-ok xl_icon"></i>
									</div>
									<div class="span1"></div>
								</div>

								<br>

							</div>
							<div class="tab-pane" id="tab2">

								<h3 class="block">A little about yourself first...</h3>
								
								<div class="row-fluid">
									<div class="span2 setup_wizard_icons">
										<i class="icon-user xl_icon light_blue"></i>
									</div>
									<div class="span5">
										
										<div class="control-group">
											<label class="control-label">First Name</label>
											<div class="controls">
												<input type="text" class="span12 m-wrap" name="first_name" id="first_name" />
											</div>
										</div>

										<div class="control-group">
											<label class="control-label">Last Name</label>
											<div class="controls">
												<input type="text" class="span12 m-wrap" name="last_name" id="last_name" />
											</div>
										</div>

										<div class="control-group">
											<label class="control-label">Gender</label>
											<div class="controls">
												<select class="span12 m-wrap" name="gender" id="gender">
													<option value="male">Male</option>
													<option value="female">Female</option>
												</select>
											</div>
										</div>

										<div class="control-group">
											<label class="control-label">Country</label>
											<div class="controls">
												<select class="m-wrap span12" name="country" id="country">
                                                	<option value="-1">Country</option>
                                                	<option value="af">Afghanistan</option><option value="al">Albania</option><option value="dz">Algeria</option><option value="as">American Samoa</option><option value="ad">Andorra</option><option value="ao">Angola</option><option value="ai">Anguilla</option><option value="aq">Antarctica</option><option value="ag">Antigua and Barbuda</option><option value="ar">Argentina</option><option value="am">Armenia</option><option value="aw">Aruba</option><option value="au">Australia</option><option value="at">Austria</option><option value="az">Azerbaijan</option><option value="bs">Bahamas</option><option value="bh">Bahrain</option><option value="bd">Bangladesh</option><option value="bb">Barbados</option><option value="by">Belarus</option><option value="be">Belgium</option><option value="bz">Belize</option><option value="bj">Benin</option><option value="bm">Bermuda</option><option value="bt">Bhutan</option><option value="bo">Bolivia</option><option value="ba">Bosnia and Herzegovina</option><option value="bw">Botswana</option><option value="bv">Bouvet Island</option><option value="br">Brazil</option><option value="io">British Indian Ocean Territory</option><option value="bn">Brunei Darussalam</option><option value="bg">Bulgaria</option><option value="bf">Burkina Faso</option><option value="bi">Burundi</option><option value="kh">Cambodia</option><option value="cm">Cameroon</option><option value="ca">Canada</option><option value="cv">Cape Verde</option><option value="ky">Cayman Islands</option><option value="cf">Central African Republic</option><option value="td">Chad</option><option value="cl">Chile</option><option value="cn">China</option><option value="cx">Christmas Island</option><option value="cc">Cocos (Keeling) Islands</option><option value="co">Colombia</option><option value="km">Comoros</option><option value="cg">Congo</option><option value="cd">Congo</option><option value="ck">Cook Islands</option><option value="cr">Costa Rica</option><option value="ci">Cote D'Ivoire</option><option value="hr">Croatia</option><option value="cu">Cuba</option><option value="cy">Cyprus</option><option value="cz">Czech Republic</option><option value="dk">Denmark</option><option value="dj">Djibouti</option><option value="dm">Dominica</option><option value="do">Dominican Republic</option><option value="ec">Ecuador</option><option value="eg">Egypt</option><option value="sv">El Salvador</option><option value="gq">Equatorial Guinea</option><option value="er">Eritrea</option><option value="ee">Estonia</option><option value="et">Ethiopia</option><option value="fk">Falkland Islands (Malvinas)</option><option value="fo">Faroe Islands</option><option value="fj">Fiji</option><option value="fi">Finland</option><option value="fr">France</option><option value="gf">French Guiana</option><option value="pf">French Polynesia</option><option value="tf">French Southern Territories</option><option value="ga">Gabon</option><option value="gm">Gambia</option><option value="ge">Georgia</option><option value="de">Germany</option><option value="gh">Ghana</option><option value="gi">Gibraltar</option><option value="gr">Greece</option><option value="gl">Greenland</option><option value="gd">Grenada</option><option value="gp">Guadeloupe</option><option value="gu">Guam</option><option value="gt">Guatemala</option><option value="gn">Guinea</option><option value="gw">Guinea-Bissau</option><option value="gy">Guyana</option><option value="ht">Haiti</option><option value="hm">Heard Island and Mcdonald Islands</option><option value="va">Holy See (Vatican City State)</option><option value="hn">Honduras</option><option value="hk">Hong Kong</option><option value="hu">Hungary</option><option value="is">Iceland</option><option value="in">India</option><option value="id">Indonesia</option><option value="ir">Iran</option><option value="iq">Iraq</option><option value="ie">Ireland</option><option value="il">Israel</option><option value="it">Italy</option><option value="jm">Jamaica</option><option value="jp">Japan</option><option value="jo">Jordan</option><option value="kz">Kazakhstan</option><option value="ke">Kenya</option><option value="ki">Kiribati</option><option value="kp">Korea</option><option value="kr">Korea</option><option value="kw">Kuwait</option><option value="kg">Kyrgyzstan</option><option value="la">Lao People's Democratic Republic</option><option value="lv">Latvia</option><option value="lb">Lebanon</option><option value="ls">Lesotho</option><option value="lr">Liberia</option><option value="ly">Libyan Arab Jamahiriya</option><option value="li">Liechtenstein</option><option value="lt">Lithuania</option><option value="lu">Luxembourg</option><option value="mo">Macao</option><option value="mk">Macedonia</option><option value="mg">Madagascar</option><option value="mw">Malawi</option><option value="my">Malaysia</option><option value="mv">Maldives</option><option value="ml">Mali</option><option value="mt">Malta</option><option value="mh">Marshall Islands</option><option value="mq">Martinique</option><option value="mr">Mauritania</option><option value="mu">Mauritius</option><option value="yt">Mayotte</option><option value="mx">Mexico</option><option value="fm">Micronesia</option><option value="md">Moldova</option><option value="mc">Monaco</option><option value="mn">Mongolia</option><option value="ms">Montserrat</option><option value="ma">Morocco</option><option value="mz">Mozambique</option><option value="mm">Myanmar</option><option value="na">Namibia</option><option value="nr">Nauru</option><option value="np">Nepal</option><option value="nl">Netherlands</option><option value="an">Netherlands Antilles</option><option value="nc">New Caledonia</option><option value="nz">New Zealand</option><option value="ni">Nicaragua</option><option value="ne">Niger</option><option value="ng">Nigeria</option><option value="nu">Niue</option><option value="nf">Norfolk Island</option><option value="mp">Northern Mariana Islands</option><option value="no">Norway</option><option value="om">Oman</option><option value="pk">Pakistan</option><option value="pw">Palau</option><option value="ps">Palestinian Territory</option><option value="pa">Panama</option><option value="pg">Papua New Guinea</option><option value="py">Paraguay</option><option value="pe">Peru</option><option value="ph">Philippines</option><option value="pn">Pitcairn</option><option value="pl">Poland</option><option value="pt">Portugal</option><option value="pr">Puerto Rico</option><option value="qa">Qatar</option><option value="re">Reunion</option><option value="ro">Romania</option><option value="ru">Russian Federation</option><option value="rw">Rwanda</option><option value="sh">Saint Helena</option><option value="kn">Saint Kitts and Nevis</option><option value="lc">Saint Lucia</option><option value="pm">Saint Pierre and Miquelon</option><option value="vc">Saint Vincent and the Grenadines</option><option value="ws">Samoa</option><option value="sm">San Marino</option><option value="st">Sao Tome and Principe</option><option value="sa">Saudi Arabia</option><option value="sn">Senegal</option><option value="cs">Serbia and Montenegro</option><option value="sc">Seychelles</option><option value="sl">Sierra Leone</option><option value="sg">Singapore</option><option value="sk">Slovakia</option><option value="si">Slovenia</option><option value="sb">Solomon Islands</option><option value="so">Somalia</option><option value="za">South Africa</option><option value="gs">South Georgia and the South Sandwich Islands</option><option value="es">Spain</option><option value="lk">Sri Lanka</option><option value="sd">Sudan</option><option value="sr">Suriname</option><option value="sj">Svalbard and Jan Mayen</option><option value="sz">Swaziland</option><option value="se">Sweden</option><option value="ch">Switzerland</option><option value="sy">Syrian Arab Republic</option><option value="tw">Taiwan</option><option value="tj">Tajikistan</option><option value="tz">Tanzania</option><option value="th">Thailand</option><option value="tl">Timor-Leste</option><option value="tg">Togo</option><option value="tk">Tokelau</option><option value="to">Tonga</option><option value="tt">Trinidad and Tobago</option><option value="tn">Tunisia</option><option value="tr">Turkey</option><option value="tm">Turkmenistan</option><option value="tc">Turks and Caicos Islands</option><option value="tv">Tuvalu</option><option value="ug">Uganda</option><option value="ua">Ukraine</option><option value="ae">United Arab Emirates</option><option value="gb">United Kingdom</option><option value="us">United States</option><option value="um">United States Minor Outlying Islands</option><option value="uy">Uruguay</option><option value="uz">Uzbekistan</option><option value="vu">Vanuatu</option><option value="ve">Venezuela</option><option value="vn">Viet Nam</option><option value="vg">Virgin Islands</option><option value="vi">Virgin Islands</option><option value="wf">Wallis and Futuna</option><option value="eh">Western Sahara</option><option value="ye">Yemen</option><option value="zm">Zambia</option><option value="zw">Zimbabwe</option>                                             
                                                </select>
											</div>
										</div>

										<div class="control-group">
											<label class="control-label">Timezone</label>
											<div class="controls">
												<select class="span12 m-wrap" name="timezone" id="timezone">
													
												</select>
											</div>
										</div>

										<br>

									</div>

									<div class="span5">

										<div class="control-group">
											<label class="control-label">Profile Picture</label>
											<div class="controls profile_picture_upload">
												<div id="profile_picture_preview_wrapper" style="display: none;"><img src="#" id="profile_picture_preview" style="display:none;" /></div>

												<span class="btn light-green fileinput-button">
											        <i class="icon-plus"></i>
											        <span>Add Photo</span>
											        <!-- The file input field used as target for the file upload widget -->
											        <input id="profile_picture" type="file" name="image_file">
											    </span>

											    <div id="profile_picture_progress" class="progress progress_thin progress-success progress-striped" style="display: none;">
													<div class="bar"></div>
												</div>
											</div>
										</div>

									</div>
								</div>

							</div>

							<div class="tab-pane" id="tab3">
								
								<h3 class="block">...and about your Business.</h3>
								
								<div class="row-fluid">
									<div class="span2 setup_wizard_icons">
										<i class="icon-briefcase xl_icon light_blue"></i>
									</div>
									<div class="span5">
										
										<div class="control-group">
											<label class="control-label">Business Name</label>
											<div class="controls">
												<input type="text" class="span12 m-wrap" name="business_name" id="business_name" value="<?php echo $_SESSION['business_name']; ?>"/>
											</div>
										</div>

										<div class="control-group">
											<label class="control-label">Industry</label>
											<div class="controls">
												<select class="span12 m-wrap select2" name="business_type" id="business_type">
	                                                <option value="">Select...</option>
	                                                <?php
	                                                	$business_type = getSimpleArray("business_type");

	                                                  	foreach($business_type as $key=>$country) {

															echo "<option value='$key'>$country</option>";

														}
	                                                ?>
	                                            </select>
											</div>
										</div>

										<div class="control-group">
											<label class="control-label">Year Founded</label>
											<div class="controls">
												<select class="span12 m-wrap" name="year_founded" id="year_founded">
													<option value='-1'>Year</option>
                                                   	<?php
                                                    	$year = date('Y');

                                                    	for($i = ($year); $i > ($year - 100); $i--) {
                                                        	
                                                        	echo "<option value='$i'>$i</option>";
                                                        
                                                      	}
                                                   ?>
												</select>
											</div>
										</div>

										<div class="control-group">
											<label class="control-label">Preferred Label for Users</label>
											<div class="controls">
												<select class="span12 m-wrap" name="users_label" id="users_label">
													<option value='employees'>Employees</option>
													<option value='team_members'>Team Members</option>
													<option value='associates'>Associates</option>
												</select>
											</div>
										</div>

										<br>

									</div>

									<div class="span5">

										<div class="control-group">
											<label class="control-label">Logo</label>
											<div class="controls logo_upload">
												<div id="logo_preview_wrapper" style="display: none;"><img src="#" id="logo_preview" style="display:none;" /></div>

												<span class="btn light-green fileinput-button">
											        <i class="icon-plus"></i>
											        <span>Add Logo</span>
											        <!-- The file input field used as target for the file upload widget -->
											        <input id="logo" type="file" name="image_file">
											    </span>

											    <div id="logo_progress" class="progress progress_thin progress-success progress-striped" style="display: none;">
													<div class="bar"></div>
												</div>
											</div>
										</div>

									</div>
								</div>

							</div>

							<div class="tab-pane" id="tab4">
								<h3 class="block">Finally, let's invite a few of your <span id="users_label_invite">Employees</span> to join you, shall we?</h3>

								<div class="row-fluid">
									<div class="span2 setup_wizard_icons">
										<i class="icon-group xl_icon light_blue"></i>
									</div>
									<div class="span10 class invite_users">
										
										<div class="control-group">
											<input type="text" class="span3 m-wrap" name="inv_first_name_1" id="inv_first_name_1" placeholder="First Name"/>
											<input type="text" class="span3 m-wrap" name="inv_last_name_1" id="inv_last_name_1" placeholder="Last Name"/>
											<input type="text" class="span6 m-wrap" name="inv_email_1" id="inv_email_1" placeholder="Email"/>				
										</div>

										<div class="control-group">
											<input type="text" class="span3 m-wrap" name="inv_first_name_2" id="inv_first_name_2" placeholder="First Name"/>
											<input type="text" class="span3 m-wrap" name="inv_last_name_2" id="inv_last_name_2" placeholder="Last Name"/>
											<input type="text" class="span6 m-wrap" name="inv_email_2" id="inv_email_2" placeholder="Email"/>	
										</div>

										<div class="control-group">
											<input type="text" class="span3 m-wrap" name="inv_first_name_3" id="inv_first_name_3" placeholder="First Name"/>
											<input type="text" class="span3 m-wrap" name="inv_last_name_3" id="inv_last_name_3" placeholder="Last Name"/>
											<input type="text" class="span6 m-wrap" name="inv_email_3" id="inv_email_3" placeholder="Email"/>
										</div>


										<br>

									</div>

								</div>
							</div>
						</div>
						<div class="form-actions clearfix">
							<a href="javascript:;" class="btn button-previous left" style="display: none;">Go Back</a>

							<a href="javascript:;" class="btn light-green button-submit right submit" style="display: none;">Save &amp; Finish <i class="icon-ok"></i></a>
							<a href="javascript:;" class="btn light-blue button-next right continue">Continue</a>
							
						</div>
					</div>
				</form>
			</div>

			<div class="loading_modal"></div>
             
        </div>
    </div>


  </div> 


	<?php 

		$footer_scripts = load_footer_scripts();

		echo $footer_scripts;

	?>

</body>

<!-- END BODY -->

</html>

<?php

   require_once("inc/session_end.php");

?>