<?php
   require_once("inc/session_start.php");
   require_once("functions.php");

   $html_array = load_page_html();

   require_once("inc/head.php");
?>
<!-- BEGIN BODY -->
<body class="fixed-top page-sidebar-fixed open_chat">
   <div class="header navbar navbar-inverse navbar-fixed-top">

      <!-- BEGIN TOP NAVIGATION BAR -->
      <?php

         $top_menu = load_top_menu();

      ?>

      <div class="navbar-inner">

         <div class="container-fluid">

            <!-- BEGIN LOGO -->

            <div class="brand">

               <img src="img/logo_small.png" alt="logo" />

            </div>


            <ul class="nav pull-right">

               <li class="dropdown user">

                  <img alt="" src="<?php echo $html_array['image_thumb']; ?>" />&nbsp;&nbsp;

                  <span class="name"><?php echo $top_menu['first_name'] . " " . $top_menu['last_name']; ?></span>

               </li>

            </ul>


         </div>

      </div>

      <!-- END TOP NAVIGATION BAR -->

   </div>

   <!-- BEGIN CONTAINER -->   
   <div class="page-container row-fluid">

      <!-- BEGIN PAGE -->
      <div class="page-content">
         <div class="container-fluid">
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">

               <div class="span12">

                  <!-- BEGIN PORTLET-->
                  <div class="portlet box blue chat popout-chat">
                     <div class="portlet-title line">
                        <div class="caption"><i class="icon-comment"></i> Open Chat</div>
                        
                     </div>
                     <div class="portlet-body" id="chats">
                        <div class="stretch_width">The Open Chat is available to all the account holders within a business. The messages sent here are visible to anyone viewing them while logged in to their individual accounts. If you wish to send private messages to your peers, please use the Messages portal instead.</div>
                        <div class="scroller" data-always-visible="1" data-rail-visible1="1">
                           <ul class="chats">

                           </ul>
                        </div>
                        <div class="chat-form">
                           <div class="input-cont">   
                              <textarea class="m-wrap message_text" placeholder="Type a message..."></textarea>
                              <input class="first_name" type="hidden" value="<?php echo $_SESSION['first_name']; ?>" />
                              <input class="last_name" type="hidden" value="<?php echo $_SESSION['last_name']; ?>" />
                              <input class="image" type="hidden" value="<?php echo $_SESSION['image_thumb']; ?>" />
                              <input class="user_id" type="hidden" value="<?php echo $_SESSION['user_id']; ?>" />
                              <input class="account_id" type="hidden" value="<?php echo $_SESSION['account_id']; ?>" />
                           </div>
                           <div class="btn-cont"> 
                              <a href="" class="btn light-blue icn-only send_message"><i class="icon-ok"></i> Send</a>
                           </div>

                           <?php echo $html_array['chat_sound_btn']; ?>
                        </div>

                        <audio id="chat_notif">
                           <source src="audio/notif.ogg" type="audio/ogg">
                           <source src="audio/notif.mp3" type="audio/mpeg">
                        </audio>
                     </div>
                  </div>
                  <!-- END PORTLET-->

               </div>
            </div>
            <!-- END PAGE CONTENT--> 
         </div>
         <!-- END PAGE CONTAINER-->       
      </div>
      <!-- BEGIN PAGE -->
   </div>
   <!-- END PAGE CONTAINER--> 
   <!-- END CONTAINER -->


   <div class="loading_large"><img src="img/loader_xlarge_greybg.gif" /></div>

   <div class="overlay">
      <div class="overlay_message">
         <img src="img/loader_xlarge_greybg.gif" />
         <br>
         <br>
         <h3>Your connection was lost. Waiting to re-establish connection...</h3>
         <br>
         <h3><a href="#" class="dismiss_overlay btn green">Dismiss</a></h3>
      </div>
   </div>


   <script src="plugins/jquery-1.10.1.min.js" type="text/javascript"></script>
   <script src="plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
   <!-- IMPORTANT! Load jquery-ui-1.10.1.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
   <script src="plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>      
   <script src="plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
   <!--[if lt IE 9]>
   <script src="plugins/excanvas.min.js"></script>
   <script src="plugins/respond.min.js"></script>  
   <![endif]-->   
   <script src="plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
   <script src="plugins/jquery.blockui.min.js" type="text/javascript"></script>  
   <script src="plugins/jquery.cookie.min.js" type="text/javascript"></script>
   <script src="plugins/uniform/jquery.uniform.min.js" type="text/javascript" ></script>
   <script src="plugins/jqury-idletimer/idle-timer.js" type="text/javascript" ></script>

   <!-- END CORE PLUGINS -->
   <script src="scripts/plugins.min.js"></script>
   <script src="https://www.ironvault.ca:8080/socket.io/socket.io.js"></script>
   <script src="scripts/pages.js"></script>
   <script src="chat/open_chat.js"></script> 
   <script src="scripts/chat.js"></script>            
   <script>
      jQuery(document).ready(function() {       
         // initiate layout and plugins
         App.init();
      });
   </script>


   <?php

      echo "<script>";

      echo "var lock_timeout = " . $_SESSION['lock_timeout'] . "; ";

      echo "</script>";


   ?>

   <script src="scripts/custom.js"></script>


   <!-- END JAVASCRIPTS -->

</body>
<!-- END BODY -->
</html>
<?php
   require_once("inc/session_end.php");
?>