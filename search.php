<?php

   require_once("inc/session_start.php");
   require_once("functions.php");

   $html_array = load_page_html();

   require_once("inc/head.php");
?>
<!-- BEGIN BODY -->
<body class="fixed-top page-sidebar-fixed <?php echo $html_array['page_sidebar_closed']; ?>">
   <?php require_once("inc/top_menu.php"); ?>
   <!-- BEGIN CONTAINER -->   
   <div class="page-container row-fluid">

      <?php require_once("inc/main_menu.php"); ?>

      <!-- BEGIN PAGE -->
      <div class="page-content">
         <div class="container-fluid">

				<!-- BEGIN PAGE CONTENT-->
				<div class="row-fluid">
					<div class="span12">
						<h3 class="page-title">Search</h3>

                  <div class="row-fluid">
                     <div class="span3">

                        <div class="portlet box blue">
                           <div class="portlet-title">
                              <div class="caption"><i class="icon-filter"></i> Filter</div>
                           </div>

                           <div class="portlet-body">

                              <ul id="search_filters" class="ver-inline-menu">
                                 <?php echo $html_array['search_filters']; ?>
                              </ul>

                           </div>
                        </div>

                     </div>

                     <div class="span9">
                        <div class="search_field">

                           <input type="text" id="query" class="span12" placeholder="Search..." value="<?php echo $_GET['query']; ?>" />
                        </div>

                        <div class="search_results">
                        <?php echo $html_array['search_results']; ?>
                        </div>
                     </div>
					</div>
				</div>
				<!-- END PAGE CONTENT -->

			</div>
		 
         <!-- END PAGE CONTAINER-->       
      </div>
      <!-- BEGIN PAGE -->
   </div>
   <!-- END PAGE CONTAINER--> 
   <!-- END CONTAINER -->
   <?php
      require_once("inc/footer.php");
   ?>
</body>
<!-- END BODY -->
</html>
<?php
   require_once("inc/session_end.php");
?>