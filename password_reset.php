<?php

   require_once("inc/session_start.php");

   require_once("functions.php");

   $html_array = load_page_html();

   require_once("inc/head.php");



?>



<body class="login">

  <div class="container-fluid">

    <br><h2 class="business_name"><?php echo $_SESSION['business_name']; ?></h2>

    <div id="login_modal" class="modal fade in" tabindex="-1" data-focus-on="input:first">
      <form class="form-vertical login-form" action="password_reset" method="post">
        <div class="modal-header">
          <h3><i class="icon-lock"></i>&nbsp;&nbsp;Password Reset</h3>
        </div>

        <?php 

            if($_GET['r'] == "request_sent") {

                echo '<div class="modal-body">
                        <strong>Found You!</strong><br><br>We have sent you an email with further instructions on how to reset your password.<br><br><a href="login">Back to Login page</a>
                      </div>';


            } else if($_GET['r'] == "account_not_found") {

                echo '<div class="modal-body">
                        <strong>Oops!</strong><br><br>We were unable to find an account associates with the email address you provided.<br><br>If you are a part of <strong>' . $_SESSION['business_name'] . '</strong> please contact your Supervisor for further assistance.<br><br><a href="login">Back to Login page</a>
                      </div>';

            } else {


                echo '<div class="modal-body">

                        <h3 class="form-title">Provide your email</h3>

                        <div class="alert alert-error hide">
                          <i class="icon-remove close" data-dismiss="alert"></i>
                          <span>Enter your email address.</span>
                        </div>

                        <div class="control-group">
                          <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                          <label class="control-label visible-ie8 visible-ie9">Email Address</label>
                          <div class="controls">
                            <div class="input-icon left">
                              <i class="icon-envelope"></i>
                              <input class="m-wrap placeholder-no-fix" type="text" placeholder="Email Address" name="email"/>
                            </div>
                          </div>
                        </div>

                        <div class="forget-password">
                          <p><a href="login">Remember your credentials?</a></p>
                        </div>
                    </div>

                    <div class="modal-footer">
                      <input type="submit" id="submit" class="btn light-green right fullwidth" value="Reset Password" />
                    </div>';
            }

        ?>

      </form> 
    </div>


    <div class="login_footer">
      <div class="logo">

        <a href="https://www.ironvault.ca"><img src="img/logo_small.png" alt="Iron Vault" /></a> 

      </div>

      <div class="copyright">

        <?php echo date('Y'); ?> © IRON VAULT LTD

      </div>
    </div>

  </div>  



  

  <?php



    $footer_scritps = load_footer_scripts();



    echo $footer_scritps;



  ?>
<script src="plugins/bootstrap-toastr/toastr.min.js"></script>
<script src="scripts/custom.js?t=<?php echo time(); ?>"></script>


</body>

<!-- END BODY -->

</html>



<?php

   require_once("inc/session_end.php");

?>