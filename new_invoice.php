<?php
   require_once("inc/session_start.php");
   require_once("functions.php");

   $html_array = load_page_html();

   require_once("inc/head.php");
?>
<!-- BEGIN BODY -->
<body class="fixed-top page-sidebar-fixed <?php echo $html_array['page_sidebar_closed']; ?>">
   <?php require_once("inc/top_menu.php"); ?>
   <!-- BEGIN CONTAINER -->   
   <div class="page-container row-fluid">

        <?php require_once("inc/main_menu.php"); ?>

		<div class="page-content">
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">

						<div class="portlet box blue">
							<div class="portlet-title">
								<div class="caption"><i class="icon-book"></i>New Invoice</div>
							</div>
							<div class="portlet-body">
								<form name="create_invoice" id="create_invoice" method="post" action="">

									<div class="row-fluid">
										<div class="span4">
											<h4>Client:</h4>
											<div class="control-group">
												<div class="controls">
													<select id="select_client" name="select_client" class="span8 select2 search_dropdown">
														<option value=""></option>
														<?php echo $html_array['create_invoice']['clients_list']; ?>
													</select>

													<div class="btn-group invoice_add_new_client">
														<a id="add_new_client" class="btn light-green" data-toggle="modal" href="#add_new_client_modal"><i class="icon-plus"></i> Add New Client</a>
													</div>
												</div>
											</div>

											<div class="clearfix"></div>
											<div class="invoice_client_details">

											</div>
										</div>
										<div class="span5">

										</div>
										<div class="span3">
											<h4>Invoice #:</h4>

											<span id="invoice_num"><?php echo $html_array['create_invoice']['invoice_number']; ?></span>
											<!-- <input type="text" name="invoice_num" id="invoice_num" class="span6 m-wrap zero_margin" value="" /> -->
											<!-- <span class="help-block">Leave blank to auto-generate.</span> -->
										</div>
									</div>
									<hr />
									<div class="row-fluid">
										<div class="span3">
											<h4>Payment Type:</h4>
											<select class="m-wrap" name="payment_type" id="payment_type">
												<?php echo $html_array['create_invoice']['payment_type']; ?>
											</select>
										</div>
										<div class="span3">
											<h4>Invoice Date:</h4>
											<input type="text" class="m-wrap datepicker" id="invoice_date" name="invoice_date" value="<?php echo $html_array['create_invoice']['invoice_date']; ?>"/>
										</div>
										<div class="span3">
											<h4>Term:</h4>
											<select class="m-wrap" name="payment_term" id="payment_term">
												<?php echo $html_array['create_invoice']['payment_term']; ?>
											</select>
										</div>
										<div class="span3">
											<h4>Due Date:</h4>
											<input type="text" class="m-wrap datepicker" id="due_date" name="due_date" value="<?php echo $html_array['create_invoice']['due_date']; ?>"/>
										</div>
										
									</div>
									<hr />
									<div class="row-fluid">
										<h4>Products/Services:</h4>
										<table class="table table-striped table-hover invoice_items">
											<thead>
												<tr>
													<th id="count">#</th>
													<th id="">Item</th>
													<th class="hidden-480">Description</th>
													<th class="hidden-480">Quantity</th>
													<th class="hidden-480">Rate</th>
													<th>Item Total</th>
													<th></th>
												</tr>
											</thead>
											<tbody>
												<?php echo $html_array['create_invoice']['items']; ?>
											</tbody> 
										</table>

										<a href="#" class="btn light-blue add_item">Add Item <i class="icon-plus"></i></a>
										<span></span>
										<?php echo $html_array['create_invoice']['discount_button']; ?>

									</div>
									<hr>
									<div class="row-fluid">
										<div class="span6">
											<h4>Additional Notes:</h4>
											<textarea name="additional_notes" class="span12 m-wrap additional_notes" rows="5"><?php echo $html_array['create_invoice']['notes']; ?></textarea>
										</div>
										
										<div class="span6 invoice-block">
											<input type="hidden" id="currency" value="<?php echo $html_array['create_invoice']['currency']; ?>" />
											<input type="hidden" id="cur_discount_amount" value="<?php echo $html_array['create_invoice']['discount_amount']; ?>" />
											<input type="hidden" id="cur_discount_type" value="<?php echo $html_array['create_invoice']['discount_type']; ?>" />
											<input type="hidden" id="calculate_tax" value="<?php echo $html_array['create_invoice']['calculate_tax']; ?>" />
											<input type="hidden" id="tax_rate" value="<?php echo $html_array['create_invoice']['tax_rate']; ?>" />
											<input type="hidden" id="tax_rate" value="<?php echo $html_array['create_invoice']['tax_rate']; ?>" />
											<ul class="unstyled amounts">
												<li>Subtotal: <?php echo $html_array['create_invoice']['currency']; ?><b><span class="subtotal">0.00</span></b></li>
												<!-- <li>Discount: <a href="#apply_discount" class="apply_discount" data-toggle="modal">Apply Discount</a><b><span class="discount"></span></b></li> -->
												<?php

												echo $html_array['create_invoice']['discount_li'];

												if($html_array['create_invoice']['calculate_tax'] == 1) {
													echo '<li>Tax (' . $html_array['create_invoice']['tax_rate'] . '%): ' . $html_array['create_invoice']['currency'] . '<b><span class="tax">0.00</span></b></li>';
												}

												?>
												
												<li>Total: <?php echo $html_array['create_invoice']['currency']; ?><b><span class="total">0.00</span></b></li>
											</ul>
											<div class="clearfix"></div>
											
										</div>
									</div>
									<hr>
									
									<div class="row-fluid">	
										<div class="span12">
											<a class="btn light-red" href="#delete_invoice_modal" data-toggle="modal">Discard Invoice <i class="icon-trash"></i></a>
											<a class="btn light-green right" href="#process_invoice_modal" data-toggle="modal">Process Invoice <i class="icon-ok"></i></a>
											<!-- <a class="btn light-blue save right">Save Template <i class="icon-save"></i></a> -->
											<a class="btn light-blue save_draft right">Save Draft <i class="icon-save"></i></a>
											<input type="hidden" id="invoice_id" value="<?php echo $html_array['create_invoice']['invoice_id']; ?>" />
										</div>
									</div>
								</form>
							</div>
						</div>

						<div class="portlet box blue invoice_files">
							<div class="portlet-title">
								<div class="caption">
									<i class="icon-file"></i>
									Attachments
								</div>
							</div>
							<div class="portlet-body attachments">

								<div class="attachment">
                                    <br>
                                    <i class="icon-cloud-upload xl_icon"></i>
                                    <br><br>
                                    <div class="btn-group">
                                        <a href="#upload_invoice_files_modal" data-toggle="modal" class="btn light-green"><i class="icon-plus"></i> Add File(s)</a>
                                    </div>
                                </div>

								<?php echo $html_array['invoice_files']; ?>

								<div class="clearfix"></div>
							</div>
						</div>

					</div>
				</div>

			</div>
			<!-- END PAGE CONTAINER--> 
		</div>
   </div>
   <!-- END PAGE CONTAINER--> 
   <!-- END CONTAINER -->
   <?php
      require_once("inc/footer.php");
   ?>


    <!-- <div id="apply_discount" class="modal hide fade" tabindex="-1" data-focus-on="input:first">
		<form class="zero_margin" action="#" method="post" class="apply_discount_form">

			<div class="modal-header">
				<i class="icon-remove right" data-dismiss="modal" aria-hidden="true"></i>
				<h3><i class="icon-book"></i>&nbsp;&nbsp;Apply Discount</h3>
			</div>

			<div class="modal-body">
				<p>Enter the desired amount of discount below.</p>
				<div class="row-fluid">

	               <div class="span6 ">
	                  <div class="control-group">
	                    <label class="control-label">Discount</label>
	                    <div class="controls">
	                       <input type="text" name="discount_amount" id="discount_amount" class="m-wrap span12"/>
	                    </div>
	                  </div>
	               </div>

	               <div class="span6 ">
	                  <div class="control-group">
	                    <label class="control-label">Type</label>
	                    <div class="controls">
	                       <select name="discount_type" id="discount_type" class="m-wrap span12">
	                       		<option value="amount">Amount (<?php //echo $html_array['create_invoice']['currency']; ?>)</option>
	                       		<option value="percent">Percentage (%)</option>
	                       </select>
	                    </div>
	                  </div>
	               </div>
	               
	            </div>

	            <?php //echo $html_array['create_invoice']['supervisor_required']; ?>			                                

			</div>	

			<div class="modal-footer">
				<input type="hidden" id="invoice_id" value="<?php //echo $html_array['create_invoice']['invoice_id']; ?>" />
				<input type="submit" id="submit" class="btn light-green right apply_discount_submit" value="Apply Discount" />
				<input type="button" data-dismiss="modal" class="btn" value="Cancel" />
			</div>


		</form>
	</div> -->

	<?php echo $html_array['create_invoice']['discount_modal']; ?>	



	<div id="add_new_client_modal" class="modal hide fade" tabindex="-1" data-focus-on="input:first">
		<form class="zero_margin" action="#" method="post">

			<div class="modal-header">
				<i class="icon-remove right" data-dismiss="modal" aria-hidden="true"></i>
				<h3><i class="icon-user"></i>&nbsp;&nbsp;Add New Client</h3>
			</div>

			<div class="modal-body">

				<div class="row-fluid">
                   <div class="span6 ">
                      <div class="control-group">
                         <label class="control-label">Type</label>
                         <div class="controls">
                         	<select name="client_type" id="client_type" class="m-wrap span12">
                             	<option value="individual">Individual</option>
                             	<option value="business">Business</option>
                         	</select>
                         </div>
                      </div>
                   </div>
                   <!--/span-->
                </div>

                <hr class="grey">

				<div class="row-fluid individual">
                   <div class="span6 ">
                      <div class="control-group">
                         <label class="control-label">First Name</label>
                         <div class="controls">
                            <input type="text" name="first_name" id="first_name" class="m-wrap span12" />
                         </div>
                      </div>
                   </div>
                   <!--/span-->
                   <div class="span6 ">
                      <div class="control-group">
                         <label class="control-label">Last Name</label>
                         <div class="controls">
                            <input type="text" name="last_name" id="last_name" class="m-wrap span12" />
                         </div>
                      </div>
                   </div>
                   <!--/span-->
                </div>

                <div class="row-fluid business">
                   <div class="span6 ">
                      <div class="control-group">
                         <label class="control-label">Business Name</label>
                         <div class="controls">
                            <input type="text" name="business_name" id="business_name" class="m-wrap span12" />
                         </div>
                      </div>
                   </div>
                   <!--/span-->
                   <div class="span6 ">
                      <div class="control-group">
                         <label class="control-label">Email</label>
                         <div class="controls">
                            <input type="text" name="business_email" id="business_email" class="m-wrap span12" />
                         </div>
                      </div>
                   </div>
                </div>

                <div class="row-fluid individual">
                   <div class="span6 ">
                      <div class="control-group">
                         <label class="control-label">Email</label>
                         <div class="controls">
                            <input type="text" name="individual_email" id="individual_email" class="m-wrap span12" />
                         </div>
                      </div>
                   </div>
                   <!--/span-->
                   <div class="span6 ">
                      <div class="control-group">
                         <label class="control-label" >Gender</label>
                         <div class="controls">
                            <select  class="m-wrap span12" name="gender" id="gender">
                               <option value="male">Male</option>
                               <option value="female">Female</option>
                            </select>
                         </div>
                      </div>
                   </div>
                   <!--/span-->
                </div>

                <div class="row-fluid">
                   <div class="span6 ">
                      <div class="control-group">
                         <label class="control-label">Primary Phone</label>
                         <div class="controls">
                            <input type="text" name="primary_phone" id="primary_phone" class="m-wrap span12" />
                         </div>
                      </div>
                   </div>
                   <!--/span-->
                   <div class="span6 ">
                      <div class="control-group">
                         <label class="control-label" >Alternative Phone</label>
                         <div class="controls">
                            <input type="text" name="alternative_phone" id="alternative_phone" class="m-wrap span12" />
                         </div>
                      </div>
                   </div>
                   <!--/span-->
                </div>


                <div class="row-fluid">
                   <div class="span12 ">
                      <div class="control-group">
                         <label class="control-label">Address</label>
                         <div class="controls">
                            <input type="text" name="address" id="add_client_geo_address" class="m-wrap span12" />
                         </div>
                      </div>
                   </div>
                </div>

				<div class="clearfix"></div>

			</div>

			<div class="modal-footer">
				<input type="submit" id="submit" class="btn light-green right" value="Add Client" />
				<input type="button" data-dismiss="modal" class="btn" value="Cancel" />
			</div>

		</form>

		<div class="clearfix"></div>
	</div>


	<div id="process_invoice_modal" class="modal hide fade" tabindex="-1" data-focus-on="input:first">
        <form class="zero_margin" action="#" method="post">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h3>
                    <i class="icon-book"></i>&nbsp;&nbsp;Process Invoice
                </h3>
            </div>
            <div class="modal-body">You are about to process this invoice. Would you like to email the invoice to the client at this time?</div>
            <div class="modal-footer">
            	<input type="submit" id="submit" class="btn light-green right process_and_email" value="Process Invoice & Email Client" />
                <input type="submit" id="submit" class="btn light-blue right process_only" value="Process Invoice Only" />
                <input type="button" data-dismiss="modal" class="btn" value="Cancel" />
            </div>
        </form>
    </div>


	<div id="delete_invoice_modal" class="modal hide fade" tabindex="-1" data-focus-on="input:first">
        <form class="zero_margin" action="#" method="post">
            <input type="hidden" id="invoice_id" value="<?php echo $html_array['create_invoice']['invoice_id']; ?>" />
            <div class="modal-header">
                <i class="icon-remove right" data-dismiss="modal" aria-hidden="true"></i>
                <h3>
                    <i class="icon-trash"></i>&nbsp;&nbsp;Discard Invoice
                </h3>
            </div>
            <div class="modal-body">
            	Are you sure you want to discard this invoice?
            	<br><br>
            	<i class="icon-warning-sign"></i> This action is permanent and cannot be undone.
            </div>
            <div class="modal-footer">
                <input type="submit" id="submit" class="btn light-red right delete" value="Discard Invoice" />
                <input type="button" data-dismiss="modal" class="btn" value="Cancel" />
            </div>
        </form>
    </div>


	<div id="upload_invoice_files_modal" class="modal hide fade" tabindex="-1" data-focus-on="input:first">
        <form class="zero_margin" action="#" method="post">
            <input type="hidden" id="invoice_id" value="<?php echo $html_array['create_invoice']['invoice_id']; ?>" />
            <div class="modal-header">
                <i class="icon-remove right" data-dismiss="modal" aria-hidden="true"></i>
                <h3>
                    <i class="icon-cloud-upload"></i>&nbsp;&nbsp;Upload File(s)
                </h3>
            </div>
            <div class="modal-body">
                <div class="file_upload">
                    <div id="file_preview_wrapper" style="display: none;">
                        <img src="#" id="file_preview" style="display:none;" />
                    </div>
                    <span class="btn light-green fileinput-button">
                        <i class="icon-plus"></i>
                        <span>Add File(s)</span>
                        <!-- The file input field used as target for the file upload widget -->
                        <input id="file" type="file" name="file" multiple>
                    </span>
                    <div id="file_progress" class="progress progress_thin progress-success progress-striped" style="display: none;">
                        <div class="bar"></div>
                    </div>
                </div>

                <table class="added_files table table-bordered table-striped table-condensed" style="display:none;">
                    <tr>
                        <th>Filename</th>
                        <th>Filesize</th>
                        <th>Progress</th>
                    </tr>
                </table>
            </div>
            <div class="modal-footer">
                <a href="#" class="btn light-red disabled cancel_uploads" >Cancel Uploads</a>
                <a href="#" data-dismiss="modal" class="btn light-green right">Hide</a>
            </div>
        </form>
    </div>

    <div id="delete_invoice_file_modal" class="modal hide fade" tabindex="-1" data-focus-on="input:first">
        <form class="zero_margin" action="#" method="post">
            <input type="hidden" id="invoice_id" value="<?php echo $html_array['create_invoice']['invoice_id']; ?>" />
            <input type="hidden" id="file_id" value="" />
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h3>
                    <i class="icon-trash"></i>&nbsp;&nbsp;Delete File
                </h3>
            </div>
            <div class="modal-body"></div>
            <div class="modal-footer">
                <input type="submit" id="submit" class="btn light-red right delete" value="Delete" />
                <input type="button" data-dismiss="modal" class="btn" value="Cancel" />
            </div>
        </form>
    </div>

</body>
<!-- END BODY -->
</html>
<?php
   require_once("inc/session_end.php");
?>