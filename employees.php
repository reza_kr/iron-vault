<?php
	require_once("inc/session_start.php");
	require_once("functions.php");
   
	$html_array = load_page_html();
	
	require_once("inc/head.php");
?>
<!-- BEGIN BODY -->
<body class="fixed-top page-sidebar-fixed <?php echo $html_array['page_sidebar_closed']; ?>">
   <?php require_once("inc/top_menu.php"); ?>
   <!-- BEGIN CONTAINER -->   
   <div class="page-container row-fluid">

      <?php require_once("inc/main_menu.php"); ?>

      <!-- BEGIN PAGE -->
      <div class="page-content"> 
         <div class="container-fluid">

				<!-- BEGIN PAGE CONTENT-->
				<div class="row-fluid">
					<div class="span12">

						<!-- BEGIN EXAMPLE TABLE PORTLET-->
						<div class="portlet box blue">
							<div class="portlet-title">
								<div class="caption"><i class="icon-user"></i>Employees</div>
								<div class="tools">
		                           <i class="icon-th grid" title="Grid View"></i>
		                           <i class="icon-align-justify list selected" title="List View"></i>
		                        </div>
							</div>
							
							<div class="portlet-body no-more-tables emp_table_wrapper">
								<div class="clearfix">
									
									<?php echo $html_array['export_tools']; ?>
								</div>
								<table class="table table-bordered table-striped table-condensed cf employees_table saveaspdf" id="data_table">
									<thead class="cf">
										<tr>
											<th class=""></th>
											<th class="sorting">First Name</th>
											<th class="sorting">Last Name</th>
											<th class="sorting">Position</th>
											<th class="sorting">Email</th>
											<th class="sorting">Primary Phone</th>
											<th>Message</th>
										</tr>
									</thead>
									<tbody>
										<?php echo $html_array['emp_table']; ?> 
										
									</tbody>
								</table> 

								<div class="gridview row-fluid">

									<div class="row-fluid">
				                     	<div id="emp_search_box" class="span6">
				                     		<input type="text" class="search_emp m-wrap" placeholder="Search..." />
				                     	</div>
				                    </div>

				                    <hr class="grey">

			                     	<div class="row-fluid emp_grid">

			                     		<?php echo $html_array['emp_grid']; ?>
									
										<div class="span3 m-span3 shuffle_sizer"></div>
			                     		<div class="clearfix"></div>
			                     	</div>
								</div>
							</div>
						</div>

						<!-- END EXAMPLE TABLE PORTLET-->
					</div>
				</div>
				<!-- END PAGE CONTENT -->
			</div>
		 
		 
		  
		 
		 
		 
		 
		 
		 
         <!-- END PAGE CONTAINER-->       
      </div>
      <!-- BEGIN PAGE -->
   </div>
   <!-- END PAGE CONTAINER--> 
   <!-- END CONTAINER -->
   <?php
      require_once("inc/footer.php");
   ?>
</body>
<!-- END BODY -->
</html>
<?php
   require_once("inc/session_end.php");
?>