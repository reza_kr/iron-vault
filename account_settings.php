<?php
   require_once("inc/session_start.php");
   require_once("functions.php");

   $html_array = load_page_html();

   require_once("inc/head.php");
?>
<!-- BEGIN BODY -->
<body class="fixed-top page-sidebar-fixed <?php echo $html_array['page_sidebar_closed']; ?>">
   <?php require_once("inc/top_menu.php"); ?>
   <!-- BEGIN CONTAINER -->   
   <div class="page-container row-fluid">

      <?php require_once("inc/main_menu.php"); ?>

	  <div class="page-content">

         <div class="container-fluid">
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid account_settings">


                <div class="span12">


                	<div class="portlet box blue">
						<div class="portlet-title">
							<div class="caption"><i class="icon-cogs"></i> Account Settings</div>
						</div>

						<div class="portlet-body">

							<div class="row-fluid account_settings_nav">
								<div class="span2">
									<a href="#" id="business_profile" class="block_btn"><i class="icon-cog"></i><br>Business Profile</a>
								</div>
								<div class="span2">
									<a href="#" id="preferences" class="block_btn"><i class="icon-ok"></i><br>Preferences</a>
								</div>
								<div class="span2">
									<a href="#" id="user_accounts" class="block_btn"><i class="icon-user"></i><br>User Accounts</a>
								</div>
								<div class="span2">
									<a href="#" id="modules" class="block_btn"><i class="icon-puzzle-piece"></i><br>Modules</a>
								</div>
								<div class="span2">
									<a href="#" id="billing" class="block_btn"><i class="icon-dollar"></i><br>Plan &amp; Payments</a>
								</div>
								<div class="span2">
									<a href="#" id="referrals" class="block_btn"><i class="icon-magnet"></i><br>Referrals</a>
								</div>
							</div>

							<hr>

							<div class="row-fluid section business_profile">

								<div class="span12">
									<h3>Business Profile</h3>
	                      			<br>

	                      			<p>The information provided is kept strictly confidental and is not shared with anyone.<br>
									They are used throughout the application and appear on invoices if you have enabled them to do so.</p>

									<form action="account_settings?action=update_business_profile" method="post" enctype="multipart/form-data" id="business_profile">
										<div class="row-fluid">
	                                       <div class="span6 ">
	                                          <div class="control-group">
	                                             <label class="control-label" for="business_name">Business Name</label>
	                                             <div class="controls">
	                                                <input type="text" id="business_name" name="business_name" class="m-wrap span12" value="<?php echo $html_array['business_profile']['business_name']; ?>">
	                                             </div>
	                                          </div>
	                                       </div>
	                                       <!--/span-->
	                                       <div class="span6 ">
	                                          <div class="control-group">
	                                             <label class="control-label" for="subdomain">Account Subdomain</label>
	                                             <div class="controls">
	                                                <input type="text" id="subdomain" name="subdomain" class="m-wrap span12 zero_margin" disabled value="<?php echo $html_array['business_profile']['subdomain']; ?>">
	                                                <span class="help-block">For security reasons, the Account Subdomain cannot be changed.</span>
	                                             </div>
	                                          </div>
	                                       </div>
	                                       <!--/span-->
	                                    </div>

	                                    <div class="row-fluid">
	                                       <div class="span6 ">
	                                          <div class="control-group">
	                                             <label class="control-label" for="business_type">Type of Business</label>
	                                             <div class="controls">
	                                             	<select class="span12 m-wrap select2" name="business_type" id="business_type">
		                                                <option value="">Select...</option>
		                                                <?php
		                                                	$business_type = getSimpleArray("business_type");

		                                                   foreach($business_type as $key=>$country) {
																if($key == $html_array['business_profile']['business_type']) {
																	echo "<option value='$key' selected='selected'>$country</option>";
																} else {
																	echo "<option value='$key'>$country</option>";
																}
															}
		                                                ?>
		                                             </select>
	                                             </div>
	                                          </div>
	                                       </div>
	                                       <!--/span-->
	                                       <div class="span6 ">
	                                          <div class="control-group">
	                                             <label class="control-label" for="year_founded">Year Founded</label>
	                                             <div class="controls">
	                                             	<select id="year_founded" name="year_founded" class="m-wrap span12">
	                                                   <option value='-1'>Year</option>
	                                                   <?php
	                                                      $year = date('Y');

	                                                      for($i = ($year); $i > ($year - 100); $i--) {
	                                                         if($i == $html_array['business_profile']['year_founded']) {
	                                                            echo "<option value='$i' selected='selected'>$i</option>";
	                                                         } else {
	                                                            echo "<option value='$i'>$i</option>";
	                                                         }
	                                                      }
	                                                   ?>
	                                                </select>
	                                             </div>
	                                          </div>
	                                       </div>
	                                       <!--/span-->
	                                    </div>

	                                    <div class="row-fluid">
	                                       <div class="span6 ">
	                                          <div class="control-group">
	                                             <label class="control-label" for="address">Address</label>
	                                             <div class="controls">
	                                                <input type="text" id="address" name="address" class="m-wrap span12" value="<?php echo $html_array['business_profile']['address']; ?>">
	                                             </div>
	                                          </div>
	                                       </div>
	                                       <!--/span-->
	                                       <div class="span6 ">
	                                          <div class="control-group">
	                                             <label class="control-label" for="city">City</label>
	                                             <div class="controls">
	                                                <input type="text" id="city" name="city" class="m-wrap span12" value="<?php echo $html_array['business_profile']['city']; ?>">
	                                             </div>
	                                          </div>
	                                       </div>
	                                       <!--/span-->
	                                    </div>

	                                    <div class="row-fluid">
	                                       <div class="span6 ">
	                                          <div class="control-group">
	                                             <label class="control-label" for="province">Province</label>
	                                             <div class="controls">
	                                                <input type="text" id="province" name="province" class="m-wrap span12" value="<?php echo $html_array['business_profile']['province']; ?>">
	                                             </div>
	                                          </div>
	                                       </div>
	                                       <!--/span-->
	                                       <div class="span6 ">
	                                          <div class="control-group">
	                                             <label class="control-label" for="country">Country</label>
	                                             <div class="controls">
	                                                <select id="country" class="m-wrap span12" name="country">
	                                                   <option value="-1">Select...</option>
	                                                   <?php 
	                                                      foreach($html_array['countries'] as $key=>$country) {
	                                                         if($key == $html_array['business_profile']['country']) {
	                                                            echo "<option value='$key' selected='selected'>$country</option>";
	                                                         } else {
	                                                            echo "<option value='$key'>$country</option>";
	                                                         }
	                                                      }
	                                                   ?>
	                                                </select>
	                                             </div>
	                                          </div>
	                                        </div>
	                                        <!--/span-->
	                                    </div>

	                                    <div class="row-fluid">
	                                       <div class="span6 ">
	                                          <div class="control-group">
	                                             <label class="control-label" for="postal_code">Postal Code</label>
	                                             <div class="controls">
	                                                <input type="text" id="postal_code" name="postal_code" class="m-wrap span12" value="<?php echo $html_array['business_profile']['postal_code']; ?>">
	                                             </div>
	                                          </div>
	                                       </div>
	                                       <!--/span-->

	                                       <div class="span6 ">
	                                          <div class="control-group">
	                                             <label class="control-label" for="postal_code">Phone</label>
	                                             <div class="controls">
	                                                <input type="text" id="phone" name="phone" class="m-wrap span12" value="<?php echo $html_array['business_profile']['phone']; ?>">
	                                             </div>
	                                          </div>
	                                       </div>
	                                       <!--/span-->
	                                    </div>

	                                    <div class="row-fluid">
	                                       <div class="span6 ">
	                                          <div class="control-group">
	                                             <label class="control-label" for="postal_code">Fax</label>
	                                             <div class="controls">
	                                                <input type="text" id="fax" name="fax" class="m-wrap span12" value="<?php echo $html_array['business_profile']['fax']; ?>">
	                                             </div>
	                                          </div>
	                                       </div>
	                                       <!--/span-->

	                                       <div class="span6 ">
	                                          <div class="control-group">
	                                             <label class="control-label" for="postal_code">Email</label>
	                                             <div class="controls">
	                                                <input type="text" id="email" name="email" class="m-wrap span12" value="<?php echo $html_array['business_profile']['email']; ?>">
	                                             </div>
	                                          </div>
	                                       </div>
	                                       <!--/span-->
	                                    </div>

	                                    <div class="space10"></div>

	                                    <div class="row-fluid">

	                                    	<div class="span6"></div>

	                                        <div class="span6 logo">
	                                       		<h3>Logo</h3>

	                                       		<div class="current_logo_wrapper">
					                                 <?php echo $html_array['current_logo']; ?>
					                            </div>
					                              
					                            <div class="clearfix"></div>

					                            <div id="logo_progress" class="progress progress_thin progress-success progress-striped" style="display: none;">
					                                 <div class="bar"></div>
					                            </div>

					                            <br>

					                            <div class="logo_upload">
					                                <!-- <div id="profile_picture_preview_wrapper" style="display: none;"><img src="#" id="profile_picture_preview" style="display:none;" /></div> -->

					                                <span class="btn light-green fileinput-button">
					                                    <i class="icon-plus"></i>
					                                    <span>Upload Logo</span>
					                                    <!-- The file input field used as target for the file upload widget -->
					                                    <input id="logo" type="file" name="image_file">
					                                </span>
					                                 
					                            </div>

		                                        <!-- <div class="fileupload fileupload-new" data-provides="fileupload">
		                                          	<div class="input-append">
		                                             	<div class="uneditable-input">
		                                                	<i class="icon-file fileupload-exists"></i> 
		                                                	<span class="fileupload-preview"></span>
		                                             	</div>
		                                             	<span class="btn btn-file">
		                                                	<span class="fileupload-new">Select file</span>
		                                                	<span class="fileupload-exists">Change</span>
		                                                	<input type="hidden" name="MAX_FILE_SIZE" value="2097152" />
		                                                	<input name="image_file" type="file" />
		                                             	</span>
		                                            	<a href="#" class="btn fileupload-exists remove" data-dismiss="fileupload">Remove</a>
		                                          	</div>
		                                       	</div> -->
	                                       	</div>
	                                       	<!--/span-->
	                                    </div>
										
										
										<div class="clearfix"></div>
										<br>
	        							<input type="submit" class="btn light-green" value="Save Changes in Tab" />
									</form>
								</div>

							</div>
							<div class="row-fluid section preferences">

								<div class="span12">

									<h3>Account Preferences</h3>
                          			<br>

                          			<form action="account_settings?action=update_account_preferences" method="post" id="account_preferences">
                          				
                          				<fieldset>
	                                   		<legend>General</legend>
	                                   		<div class="row-fluid">
	                                   			<div class="span3">
		                                          <div class="control-group">
		                                             <label class="control-label" for="users_label">Preferred Label for Users</label>
		                                             <div class="controls">
		                                                <select id="users_label" class="m-wrap span12" name="users_label">
			                                                <?php
			                                                	//1 = Only me (account holder)
			                                                	//2 = Account holder and supervisors
			                                                	//3 = All Users
																if($html_array['preferences']['users_label'] == "employees") {
																 echo '<option value="employees" selected="selected">Employees</option>
					                                                	<option value="team_members">Team Members</option>
					                                                	<option value="associates">Associates</option>';
																} else if($html_array['preferences']['users_label'] == "team_members") {
																 echo '<option value="employees">Employees</option>
					                                                	<option value="team_members" selected="selected">Team Members</option>
					                                                	<option value="associates">Associates</option>';
																} else if($html_array['preferences']['users_label'] == "associates") {
																 echo '<option value="employees">Employees</option>
					                                                	<option value="team_members">Team Members</option>
					                                                	<option value="associates" selected="selected">Associates</option>';
																}
															?>
		                                                </select>
		                                             </div>
		                                          </div>
		                                        </div>


	                                   			<div class="span3">
		                                          <div class="control-group">
		                                             <label class="control-label" for="export_tools_access">Print/PDF/CSV Export Tools Access</label>
		                                             <div class="controls">
		                                                <select id="export_tools_access" class="m-wrap span12" name="export_tools_access">

			                                                <?php
			                                                	//1 = Only me (account holder)
			                                                	//2 = Account holder and supervisors
			                                                	//3 = All Users
																if($html_array['preferences']['export_tools_access'] == "1") {
																 echo '<option value="1" selected="selected">Only Me (Account Holder)</option>
					                                                	<option value="2">Account Holder and Supervisors</option>
					                                                	<option value="3">All Users</option>';
																} else if($html_array['preferences']['export_tools_access'] == "2") {
																 echo '<option value="1">Only Me (Account Holder)</option>
					                                                	<option value="2" selected="selected">Account Holder and Supervisors</option>
					                                                	<option value="3">All Users</option>';
																} else if($html_array['preferences']['export_tools_access'] == "3") {
																 echo '<option value="1">Only Me (Account Holder)</option>
					                                                	<option value="2">Account Holder and Supervisors</option>
					                                                	<option value="3" selected="selected">All Users</option>';
																}
															?>
		                                                </select>
		                                             </div>
		                                          </div>
		                                        </div>


		                                        <div class="span3">
		                                          <div class="control-group">
		                                             <label class="control-label" for="post_announcements">Post Announcements</label>
		                                             <div class="controls">
		                                                <select id="post_announcements" class="m-wrap span12" name="post_announcements">

			                                                <?php
			                                                	//1 = Only me (account holder)
			                                                	//2 = Account holder and supervisors
			                                                	//3 = All Users
																if($html_array['preferences']['post_announcements'] == "1") {
																 echo '<option value="1" selected="selected">Only Me (Account Holder)</option>
					                                                	<option value="2">Account Holder and Supervisors</option>
					                                                	<option value="3">All Users</option>';
																} else if($html_array['preferences']['post_announcements'] == "2") {
																 echo '<option value="1">Only Me (Account Holder)</option>
					                                                	<option value="2" selected="selected">Account Holder and Supervisors</option>
					                                                	<option value="3">All Users</option>';
																} else if($html_array['preferences']['post_announcements'] == "3") {
																 echo '<option value="1">Only Me (Account Holder)</option>
					                                                	<option value="2">Account Holder and Supervisors</option>
					                                                	<option value="3" selected="selected">All Users</option>';
																}
															?>
		                                                </select>
		                                             </div>
		                                          </div>
		                                        </div>
	                                   		</div>
	                                   	</fieldset>


	                                   	<fieldset>
	                                   		<legend>Work Schedule</legend>
	                                   		<div class="row-fluid">
	                                   			<div class="span3">
		                                          <div class="control-group">
		                                             <label class="control-label" for="work_schedule_auth">Managed By</label>
		                                             <div class="controls">
		                                                <select id="work_schedule_auth" class="m-wrap span12" name="work_schedule_auth">
			                                                <?php
			                                                	//1 = Only me (account holder)
			                                                	//2 = Account holder and supervisors
			                                                	//3 = All Users
																if($html_array['preferences']['work_schedule_auth'] == "3") {
																 echo '<option value="3" selected="selected">Users, Individually</option>
					                                                	<option value="2">Account Holder and Supervisors</option>
					                                                	<option value="1">Only Me (Account Holder)</option>';
																} else if($html_array['preferences']['work_schedule_auth'] == "2") {
																 echo '<option value="3">Users, Individually</option>
					                                                	<option value="2" selected="selected">Account Holder and Supervisors</option>
					                                                	<option value="1">Only Me (Account Holder)</option>';
																} else if($html_array['preferences']['work_schedule_auth'] == "1") {
																 echo '<option value="3">Users, Individually</option>
					                                                	<option value="2">Account Holder and Supervisors</option>
					                                                	<option value="1" selected="selected">Only Me (Account Holder)</option>';
																}
															?>
		                                                </select>
		                                             </div>
		                                          </div>
		                                        </div>
	                                   		</div>
	                                   	</fieldset>


                          				<fieldset>
                              				<legend>Invoicing</legend>
											<div class="row-fluid">
		                                       <div class="span3">
		                                          <div class="control-group">
		                                             <label class="control-label" for="currency">Currency</label>
		                                             <div class="controls">
		                                                <select id="currency" class="m-wrap span12" name="currency">
		                                                   <?php 
		                                                      foreach($html_array['currencies'] as $key=>$currency) {
		                                                         if($key == $html_array['preferences']['currency']) {
		                                                            echo "<option value='$key' selected='selected'>$currency ($key)</option>";
		                                                         } else {
		                                                            echo "<option value='$key'>$currency ($key)</option>";
		                                                         }
		                                                      }
		                                                   ?>
		                                                </select>
		                                             </div>
		                                          </div>
		                                       </div>
		                                       <!--/span-->
		                                       <div class="span3">
		                                          <div class="control-group">
		                                             <label class="control-label" for="calculate_tax">Calculate Tax on Invoices</label>
		                                             <div class="controls">
		                                                <select id="calculate_tax" class="m-wrap span12" name="calculate_tax">
		                                                   <?php

		                                                      if($html_array['preferences']['calculate_tax'] == "1") {
		                                                         echo '<option value="1" selected="selected">Yes</option>
		                                                               <option value="0">No</option>';
		                                                      } else if($html_array['preferences']['calculate_tax'] == "0") {
		                                                         echo '<option value="1">Yes</option>
		                                                               <option value="0" selected="selected">No</option>';
		                                                      }
		                                                   ?>
		                                                </select>
		                                             </div>
		                                          </div>
		                                       </div>

		                                       <div class="span3">
		                                          <div class="control-group">
		                                             <label class="control-label" for="tax_rate">Tax Rate (%)</label>
		                                             <div class="controls">
		                                                <input type="text" id="tax_rate" name="tax_rate" class="m-wrap span12" value="<?php echo $html_array['preferences']['tax_rate']; ?>">
		                                             </div>
		                                          </div>
		                                       </div>
		                                       <!--/span-->
		                                       <div class="span3">
		                                          <div class="control-group">
		                                             <label class="control-label" for="discounts">Discounts</label>
		                                             <div class="controls">
		                                                <select id="discounts" class="m-wrap span12" name="discounts">
		                                                   <?php

		                                                      if($html_array['preferences']['discounts'] == "supervisor") {
		                                                         echo '<option value="supervisor" selected="selected">Supervisor\'s Approval Required</option>
		                                                               <option value="all">No Approval Required</option>
		                                                               <option value="disabled">Disable Discounts</option>';
		                                                      } else if($html_array['preferences']['discounts'] == "all") {
		                                                         echo '<option value="supervisor">Supervisor\'s Approval Required</option>
		                                                               <option value="all" selected="selected">No Approval Required</option>
		                                                               <option value="disabled">Disable Discounts</option>';
		                                                      } else if($html_array['preferences']['discounts'] == "disabled") {
		                                                         echo '<option value="supervisor">Supervisor\'s Approval Required</option>
		                                                               <option value="all">No Approval Required</option>
		                                                               <option value="disabled" selected="selected">Disable Discounts</option>';
		                                                      } else {
		                                                    	 echo '<option value="supervisor">Supervisor\'s Approval Required</option>
		                                                               <option value="all">No Approval Required</option>
		                                                               <option value="disabled">Disable Discounts</option>';
		                                                      }
		                                                   ?>
		                                                </select>
		                                             </div>
		                                          </div>
		                                       </div>
		                                       <!--/span-->
		                                    </div>

		                                    <div class="row-fluid">
		                                    	<div class="span3">
		                                          <div class="control-group">
		                                             <label class="control-label" for="auto_update_inventory">Auto-Update Inventory Count?</label>
		                                             <div class="controls">
		                                                <select id="auto_update_inventory" class="m-wrap span12" name="auto_update_inventory">
		                                                   <?php

		                                                      if($html_array['preferences']['auto_update_inventory'] == "auto") {
		                                                         echo '<option value="auto" selected="selected">Yes, Update Automatically</option>
		                                                               <option value="manual">No, Leave it to Us</option>';
		                                                      } else if($html_array['preferences']['auto_update_inventory'] == "manual") {
		                                                         echo '<option value="auto">Yes, Update Automatically</option>
		                                                               <option value="manual" selected="selected">No, Leave it to Us</option>';
		                                                      }
		                                                   ?>
		                                                </select>
		                                             </div>
		                                          </div>
		                                       </div>
		                                       <div class="span3">
		                                          <div class="control-group">
		                                             <label class="control-label" for="business_info_on_invoice">Show Business Information on Invoices?</label>
		                                             <div class="controls">
		                                                <select id="business_info_on_invoice" class="m-wrap span12" name="business_info_on_invoice">
		                                                   <?php

		                                                      if($html_array['preferences']['business_info_on_invoice'] == "show") {
		                                                         echo '<option value="show" selected="selected">Show</option>
		                                                               <option value="hide">Hide</option>';
		                                                      } else if($html_array['preferences']['business_info_on_invoice'] == "hide") {
		                                                         echo '<option value="show">Show</option>
		                                                               <option value="hide" selected="selected">Hide</option>';
		                                                      }
		                                                   ?>
		                                                </select>
		                                             </div>
		                                          </div>
		                                       </div>
		                                       <div class="span3">
		                                          <div class="control-group">
		                                             <label class="control-label" for="employee_info_on_invoice">Show Employee Information on Invoices?</label>
		                                             <div class="controls">
		                                                <select id="employee_info_on_invoice" class="m-wrap span12" name="employee_info_on_invoice">
		                                                   <?php

		                                                      if($html_array['preferences']['employee_info_on_invoice'] == "show") {
		                                                         echo '<option value="show" selected="selected">Show</option>
		                                                               <option value="hide">Hide</option>';
		                                                      } else if($html_array['preferences']['employee_info_on_invoice'] == "hide") {
		                                                         echo '<option value="show">Show</option>
		                                                               <option value="hide" selected="selected">Hide</option>';
		                                                      }
		                                                   ?>
		                                                </select>
		                                             </div>
		                                          </div>
		                                       </div>
		                                    </div>

		                                    Note: Changing the above settings will NOT affect previously processed invoices, whether created manually or from a recurring invoice.

	                                   	</fieldset>

	                                    <div class="clearfix"></div>
										<br>
            							<input type="submit" class="btn light-green" value="Save Changes in Tab" />
            						</form>

								</div>

							</div>
							<div class="row-fluid section user_accounts">

								<div class="span12">

									<h3>User Accounts</h3>
                          			<div class="btn-group">
										<a id="invite_new_users" class="btn light-green" data-toggle="modal" href="#invite_users_modal"><i class="icon-user"></i>&nbsp;&nbsp;Invite New Users</a>
									</div>

                          			<br>

                          			<form action="account_settings?action=update_user_accounts" method="post" name="user_accounts_form">
                              			<table class="table table-bordered table-striped table-condensed cf user_accounts_table" id="data_table">
											<thead class="cf">
												<tr>
													<th class="sorting">First Name</th>
													<th class="sorting">Last Name</th>
													<th class="sorting">Username</th>
													<th class="sorting">Status</th>
													<th class="">Strorage Used</th>
													<th class="storage_quota">Strorage Quota</th>
													<th class="reset_password">Reset Password</th>
													<th class="disable_account">Disable Account</th>
													<th class="delete_account">Delete Account</th>
												</tr>
											</thead>
											<tbody>
												<?php echo $html_array['user_accounts_list'] ?> 
												
											</tbody>
										</table>
									</form>

								</div>

							</div>
							<div class="row-fluid section modules">

								<div class="span12">
									<h3>Modules</h3>
                          			<br>

                          			<p>Turning on/off modules allows you to control which features of Iron Vault will be available to your Employees and/or Team members.</p>

									<form action="account_settings?action=update_modules" method="post" name="modules">

										<?php echo $html_array['module_settings']; ?>

										<div class="clearfix"></div>
										<br>
            							<input type="submit" class="btn light-green" value="Save Changes in Tab" />
									</form>
								</div>

							</div>
							<div class="row-fluid section billing">

								<div class="span12">

                          			<div class="row-fluid">

                          				<div class="span6">

                          					<h3>Plan &amp; Payments</h3>
                          					<br>

                          					<div class="plan_info">

                          						<table>
                          							<tr>
                          								<td>Plan Type:</td>
                          								<td><strong>Iron Vault Business Pro</strong></td>
                          							</tr>
                          							<tr>
                          								<td>Billing Cycle:</td>
                          								<td><strong>Monthly - 21st day of every month</strong></td>
                          							</tr>
                          							<tr>
                          								<td>Active Users:</td>
                          								<td><strong>7</strong></td>
                          							</tr>
                          							<tr>
                          								<td>Last Payment:</td>
                          								<td><strong>Jan 21th, 2014</strong></td>
                          							</tr>
                          							<tr>
                          								<td>Balance:</td>
                          								<td><strong>$0</strong></td>
                          							</tr>
                          						</table>

                          						<hr>
												
												<table>
                          							<tr>
                          								<td><a href="#billing_history_modal" id="billing_history" class="btn light-blue" data-toggle="modal">Billing History</a></td>
                          							</tr>
                          							<tr>
                          								<td><a href="#cancel_subscription_modal" id="cancel_subscription" class="btn light-red" data-toggle="modal">Cancel Subscription</a></td>
                          							</tr>
                          						</table>

                          						<hr>

                          						<div class="breakdown_of_charges">
                          							Breakdown of Charges this Cycle:<br><br>

                          							<table class="table table-bordered table-hover">
														<thead>
															<tr>
																<th>Services</th>
																<th>Unit</th>
																<th>Cost</th>
																<th>Service Total</th>
															</tr>
														</thead>
														<tbody>
															<tr>
																<td>Iron Vault Business Pro</td>
																<td>1</td>
																<td>$18</td>
																<td>$18</td>
															</tr>
															<tr>
																<td>Additional Users</td>
																<td>6</td>
																<td>$9/User</td>
																<td>$54</td>
															</tr>
															<tr>
																<td>Extra Storage</td>
																<td>121GB</td>
																<td>$0.10/GB</td>
																<td>$12.10</td>
															</tr>
															<tr>
																<td>Paying Referrals</td>
																<td>1</td>
																<td>– $18 (credit)</td>
																<td>– $18 (credit)</td>
															</tr>
															<tr>
																<td><strong>Total</strong></td>
																<td>-</td>
																<td>-</td>
																<td><strong>$84.10</strong></td>
															</tr>
														</tbody>
													</table>
                          							
                          						</div>

                          						<hr>

                          						<div class="next_bill_amount">
                          							Your account will be charged the following amount:
                          							<span class="bill_total right">$27</span> 
                          						</div>

                          						<div class="clearfix"></div>

                          					</div>

                          				</div>

                          				<div class="span6">

                          					<h3>Account Holder Info</h3>
                          					<br>

                          					<div class="row-fluid">
		                                       <div class="span6 ">
		                                          <div class="control-group">
		                                             <label class="control-label" for="first_name">First Name</label>
		                                             <div class="controls">
		                                                <input type="text" id="first_name" name="first_name" class="m-wrap span12" value="<?php echo $_SESSION['first_name']; ?>">
		                                             </div>
		                                          </div>
		                                       </div>
		                                       <!--/span-->
		                                       <div class="span6 ">
		                                          <div class="control-group">
		                                             <label class="control-label" for="last_name">Last Name</label>
		                                             <div class="controls">
		                                                <input type="text" id="last_name" name="last_name" class="m-wrap span12" value="<?php echo $_SESSION['last_name']; ?>">
		                                             </div>
		                                          </div>
		                                       </div>
		                                       <!--/span-->
		                                    </div>

                          					<div class="row-fluid">
		                                       <div class="span6 ">
		                                          <div class="control-group">
		                                             <label class="control-label" for="address">Address</label>
		                                             <div class="controls">
		                                                <input type="text" id="address" name="address" class="m-wrap span12" value="<?php echo $html_array['business_profile']['address']; ?>">
		                                             </div>
		                                          </div>
		                                       </div>
		                                       <!--/span-->
		                                       <div class="span6 ">
		                                          <div class="control-group">
		                                             <label class="control-label" for="city">City</label>
		                                             <div class="controls">
		                                                <input type="text" id="city" name="city" class="m-wrap span12" value="<?php echo $html_array['business_profile']['city']; ?>">
		                                             </div>
		                                          </div>
		                                       </div>
		                                       <!--/span-->
		                                    </div>

		                                    <div class="row-fluid">
		                                       <div class="span6 ">
		                                          <div class="control-group">
		                                             <label class="control-label" for="province">Province</label>
		                                             <div class="controls">
		                                                <input type="text" id="province" name="province" class="m-wrap span12" value="<?php echo $html_array['business_profile']['province']; ?>">
		                                             </div>
		                                          </div>
		                                       </div>
		                                       <!--/span-->
		                                       <div class="span6 ">
		                                          <div class="control-group">
		                                             <label class="control-label" for="country">Country</label>
		                                             <div class="controls">
		                                                <select id="country" class="m-wrap span12" name="country">
		                                                   <option value="-1">Select...</option>
		                                                   <?php 
		                                                      foreach($html_array['countries'] as $key=>$country) {
		                                                         if($key == $html_array['business_profile']['country']) {
		                                                            echo "<option value='$key' selected='selected'>$country</option>";
		                                                         } else {
		                                                            echo "<option value='$key'>$country</option>";
		                                                         }
		                                                      }
		                                                   ?>
		                                                </select>
		                                             </div>
		                                          </div>
		                                        </div>
		                                        <!--/span-->
		                                    </div>

		                                    <div class="row-fluid">
		                                       <div class="span6 ">
		                                          <div class="control-group">
		                                             <label class="control-label" for="postal_code">Postal Code</label>
		                                             <div class="controls">
		                                                <input type="text" id="postal_code" name="postal_code" class="m-wrap span12" value="<?php echo $html_array['business_profile']['postal_code']; ?>">
		                                             </div>
		                                          </div>
		                                       </div>
		                                       <!--/span-->

		                                       <div class="span6 ">
		                                          <div class="control-group">
		                                             <label class="control-label" for="postal_code">Phone</label>
		                                             <div class="controls">
		                                                <input type="text" id="phone" name="phone" class="m-wrap span12" value="<?php echo $html_array['business_profile']['phone']; ?>">
		                                             </div>
		                                          </div>
		                                       </div>
		                                       <!--/span-->
		                                    </div>


		                                    <hr>
		                                    <h3>Payment Method</h3>
                          					<br>


                          					<div class="row-fluid">
		                                       <div class="span6">
		                                            <img src="img/credit_card.png" alt="credit card" />
		                                       </div>
		                                    </div>

		                                    <div class="spacer_10"></div>
		                                    <div class="spacer_10"></div>

		                                    <div class="row-fluid">
		                                       <div class="span6 ">
		                                          <div class="control-group">
		                                          	 <label class="control-label" for="card_number">Card Number</label>
		                                             <div class="controls">
		                                                <input type="text" id="card_number" name="card_number" class="m-wrap span12">
		                                             </div>
		                                          </div>
		                                       </div>
		                                    </div>

                          					<div class="row-fluid">
		                                       <div class="span6 ">
		                                          <div class="control-group">
		                                          	 <label class="control-label" for="card_holder_name">Card Holder Name</label>
		                                             <div class="controls">
		                                                <input type="text" id="card_holder_name" name="card_holder_name" class="m-wrap span12">
		                                             </div>
		                                          </div>
		                                       </div>
		                                    </div>

		                                    <div class="row-fluid">
		                                       <div class="span6 ">
		                                          <div class="control-group">
		                                          	 <label class="control-label" for="card_holder_name">Card Expiry Date</label>
		                                             <div class="controls">
		                                             	<div class="span5">
			                                                <select id="card_expiry_month" name="card_expiry_month" class="m-wrap span12">
			                                                	<option value="">Month</option>
			                                                	<option value="0">01 (Jan)</option>
			                                                	<option value="1">02 (Feb)</option>
			                                                	<option value="2">03 (Mar)</option>
			                                                	<option value="3">04 (Apr)</option>
			                                                	<option value="4">05 (May)</option>
			                                                	<option value="5">06 (Jun)</option>
			                                                	<option value="6">07 (Jul)</option>
			                                                	<option value="7">08 (Aug)</option>
			                                                	<option value="8">09 (Sep)</option>
			                                                	<option value="9">10 (Oct)</option>
			                                                	<option value="10">11 (Nov)</option>
			                                                	<option value="11">12 (Dec)</option>
			                                                </select>
			                                            </div>

		                                                <div class="span1 slash"> / </div>

		                                                <div class="span6">
			                                                <select id="card_expiry_year" name="card_expiry_year" class="m-wrap span12">
			                                                	<option value="">Year</option>
			                                                	<?php
			                                                      $year = date('Y');

			                                                      for($i = ($year); $i < ($year + 30); $i++) {
			                                                           echo "<option value='$i'>$i</option>";
			                                                      }
			                                                    ?>
			                                                </select>
			                                            </div>
		                                             </div>
		                                          </div>
		                                       </div>
		                                    </div>

		                                    <div class="row-fluid">
		                                       <div class="span6 ">
		                                          <div class="control-group">
		                                          	 <label class="control-label" for="card_cvc">CVC/CVV/CID</label>
		                                             <div class="controls">
		                                             	<div class="span4">
		                                                	<input type="text" id="card_cvc" name="card_cvc" class="m-wrap span12">
		                                                </div>
		                                                <div class="span8">
		                                                	<a href="#cvc_modal" id="cvc" data-toggle="modal">What is CVC/CVV/CID?</a>
		                                                </div>
		                                             </div>
		                                          </div>
		                                       </div>
		                                    </div>

		                                    <div class="clearfix"></div>
											<br>
		        							<input type="submit" class="btn light-green" value="Update" />
                          				</div>

                          			</div>


								</div>


							</div>
							<div class="row-fluid section referrals">

								<div class="span12">

									<h3>Referrals</h3>
                          			<br>

                          			<h4>What is the Affilate Program?</h4>
									<p>A parag explaining how it works </p>  
										
									<div class="row-fluid">
										<div class="span6">

											<h5>referred People <h5>

											<table class="table table-bordered table-striped table-condensed cf clients saveaspdf" id="data_table">
												<thead class="cf">
													<tr>
														<th class="sorting"></th>
														<th class="sorting">Total</th>
														<th class="sorting">Credits</th>
												
													</tr> 
													<tr>
														<td>User Sign Ups</td>
														<td></td>
														<td></td>
													</tr>
													<tr>
														<td>Free Sign Ups</td>
														<td></td>
														<td></td>
													</tr>									
													<tr>
														<td>Paid Sign Ups</td>
														<td></td>
														<td></td>
													</tr>
												</thead>
												<tbody>
													<?php echo $html_array['clients_table']; ?>
												</tbody>
											</table>

										</div>

										<div class="span6">

											<div class="control-group">
												<label class="control-label">Your affiliate link </label>
												<div class="controls">
													<input type="text" placeholder="www.hello.com" class="m-wrap span12" />
												</div>
											</div> 
											
											<p>paragraph about how to take full advantage of the affiliate program</p>
											
										</div>
									</div>


									<div class="row-fluid">
										<div class="span6">
											<h5>Paid Referals <h5>

											<table class="table table-bordered table-striped table-condensed cf clients saveaspdf" id="data_table">
												<thead class="cf">
													<tr>
														
														<th class="sorting">Names</th>
														<th class="sorting">Credits</th>
												
													</tr> 

													<tr>
													
														<td></td>
														<td></td>
													</tr>

													<tr>
													
														<td></td>
														<td></td>
													</tr>									
												</thead>
												<tbody>
													<?php echo $html_array['clients_table']; ?>
													
												</tbody>
											</table>

										</div>	
									</div>

								</div>

							</div>

						</div>

					</div>

               </div>

            </div>
            <!-- END PAGE CONTENT--> 
         </div>
         <!-- END PAGE CONTAINER-->       
      </div>




      	<div id="delete_account_modal" class="modal hide fade" tabindex="-1" data-focus-on="input:first">

			<form class="zero_margin" action="#" method="post">

				<div class="modal-header">
					<i class="icon-remove right" data-dismiss="modal" aria-hidden="true"></i>
					<h3><i class="icon-user"></i>&nbsp;&nbsp;Delete User Account</h3>
				</div>

				<div class="modal-body">

				</div>	
					
				<div class="modal-footer">
					<input type="submit" id="submit" class="btn light-red right delete" value="Delete" />
					<input type="button" data-dismiss="modal" class="btn" value="Cancel" />
				</div>

			</form>
		</div>

		<div id="reset_password_modal" class="modal hide fade" tabindex="-1" data-focus-on="input:first">

			<form class="zero_margin" action="#" method="post">

				<div class="modal-header">
					<i class="icon-remove right" data-dismiss="modal" aria-hidden="true"></i>
					<h3><i class="icon-lock"></i>&nbsp;&nbsp;Password Reset</h3>
				</div>

				<div class="modal-body">

				</div>	
					
				<div class="modal-footer">
					<input type="submit" id="submit" class="btn light-red right reset" value="Reset" />
					<input type="button" data-dismiss="modal" class="btn" value="Cancel" />
				</div>

			</form>
		</div>


		<div id="storage_quota_modal" class="modal hide fade" tabindex="-1" aria-hidden="true">

			<form class="zero_margin" action="account_settings?action=update_storage_quota" method="post">

				<div class="modal-header">
					<i class="icon-remove right" data-dismiss="modal" aria-hidden="true"></i>
					<h3><i class="icon-cloud"></i>&nbsp;&nbsp;Storage Quota</h3>
				</div>

				<div class="modal-body">

					<div class="row-fluid">
						 <div class="span6 ">
						    <h3 id="user_name"></h3>
						 </div>
					</div>

					<div class="row-fluid">
						 <div class="span6 ">
						    <div class="control-group">
						       <label class="control-label">Current Quota</label>
						       <div class="controls">
						          <h3 id="current_quota"></h3>
						       </div>
						    </div>
						 </div>
						 <!--/span-->
						 <div class="span6 ">
						    <div class="control-group">
						       <label class="control-label">Change Quota</label>
						       <div class="controls">
						          <select name="update_quota" id="update_quota">
										<option value="10">10 GB</option>
										<option value="20">20 GB</option>
										<option value="30">30 GB</option>
										<option value="40">40 GB</option>
										<option value="50">50 GB</option>
										<option value="60">60 GB</option>
										<option value="70">70 GB</option>
										<option value="80">80 GB</option>
										<option value="90">90 GB</option>
										<option value="100">100 GB</option>
									</select>
						       </div>
						    </div>
						 </div>
						 <!--/span-->
					</div>

					<div class="row-fluid">
						 <div class="span6 ">
						    <div class="control-group">
						       <label class="control-label">Additional Charges</label>
						       <div class="controls">
						          <h3 id="additional_charges">–</h3>
						       </div>
						    </div>
						 </div>
					</div>

				</div>

				<div class="modal-footer">
					<input type="hidden" name="user_id" id="user_id" value="">
					<input type="submit" id="submit" class="btn light-green right" value="Update Quota">
				</div>

			</form>

			<div class="clearfix"></div>
		</div>


		<div id="invite_users_modal" class="modal hide fade" tabindex="-1" data-focus-on="input:first">

			<form class="zero_margin" action="#" method="post">

				<div class="modal-header">
					<i class="icon-remove right" data-dismiss="modal" aria-hidden="true"></i>
					<h3><i class="icon-user"></i>&nbsp;&nbsp;Invite New Users</h3>
				</div>

				<div class="modal-body">

					<p>Invite new users to join the <?php echo $_SESSION['business_name']; ?> network!</p>
					<br>

					  <div class="row-fluid">
                         <div class="span3 ">
                            <div class="control-group">
                               <div class="controls">
                                  <input type="text" name="inv_first_name_1" id="inv_first_name_1" class="m-wrap span12" placeholder="First Name">
                               </div>
                            </div>
                         </div>
                         <!--/span-->
                         <div class="span3">
                            <div class="control-group">
                               <div class="controls">
                                  <input type="text" name="inv_last_name_1" id="inv_last_name_1" class="m-wrap span12" placeholder="Last Name">
                               </div>
                            </div>
                         </div>
                         <!--/span-->
                         <div class="span6">
                            <div class="control-group">
                               <div class="controls">
                                  <input type="text" name="inv_email_1" id="inv_email_1" class="m-wrap span12" placeholder="Email">
                               </div>
                            </div>
                         </div>
                      </div>     

                      <div class="row-fluid">
                         <div class="span3 ">
                            <div class="control-group">
                               <div class="controls">
                                  <input type="text" name="inv_first_name_2" id="inv_first_name_2" class="m-wrap span12" placeholder="First Name">
                               </div>
                            </div>
                         </div>
                         <!--/span-->
                         <div class="span3">
                            <div class="control-group">
                               <div class="controls">
                                  <input type="text" name="inv_last_name_2" id="inv_last_name_2" class="m-wrap span12" placeholder="Last Name">
                               </div>
                            </div>
                         </div>
                         <!--/span-->
                         <div class="span6">
                            <div class="control-group">
                               <div class="controls">
                                  <input type="text" name="inv_email_2" id="inv_email_2" class="m-wrap span12" placeholder="Email">
                               </div>
                            </div>
                         </div>
                      </div>  

                      <div class="row-fluid">
                         <div class="span3 ">
                            <div class="control-group">
                               <div class="controls">
                                  <input type="text" name="inv_first_name_3" id="inv_first_name_3" class="m-wrap span12" placeholder="First Name">
                               </div>
                            </div>
                         </div>
                         <!--/span-->
                         <div class="span3">
                            <div class="control-group">
                               <div class="controls">
                                  <input type="text" name="inv_last_name_3" id="inv_last_name_3" class="m-wrap span12" placeholder="Last Name">
                               </div>
                            </div>
                         </div>
                         <!--/span-->
                         <div class="span6">
                            <div class="control-group">
                               <div class="controls">
                                  <input type="text" name="inv_email_3" id="inv_email_3" class="m-wrap span12" placeholder="Email">
                               </div>
                            </div>
                         </div>
                      </div>  

				</div>	
					
				<div class="modal-footer">
					<input type="submit" id="submit" class="btn light-green right invite_users" value="Send Invitations" />
					<input type="button" data-dismiss="modal" class="btn" value="Cancel" />
				</div>

			</form>

        	<div class="clearfix"></div>
		</div>
	  
	  

	  	<div id="cvc_modal" class="modal hide fade" tabindex="-1" aria-hidden="true">

			<div class="modal-header">
				<i class="icon-remove right" data-dismiss="modal" aria-hidden="true"></i>
				<h3><i class="icon-credit-card"></i>&nbsp;&nbsp;What is CVC/CVV/CID?</h3>
			</div>

			<div class="modal-body">

				<div class="row-fluid">

					<div class="span4">
						<img src="img/cvc.png" alt="CVC" />
					</div>

					<div class="span8">
						<h4>What is CVC/CVV/CID?</h4>
						<p>The Card Security Code (CVC/CVV/CID) is an additional three or four digit security code that is printed (not embossed) on the front or the back of your card.</p>

						<p>The CVC/CVV/CID is an extra security measure to ensure that you are in possession of the card.</p>
					</div>

				</div>

				<div class="clearfix"></div>

			</div>

			<div class="modal-footer">
				<input type="submit" id="submit" data-dismiss="modal" class="btn light-green right" value="Close">
			</div>

			<div class="clearfix"></div>
		</div>


	  	<div id="billing_history_modal" class="modal hide fade container" tabindex="-1">

			<form class="zero_margin" action="#" method="post">

				<div class="modal-header">
					<i class="icon-remove right" data-dismiss="modal" aria-hidden="true"></i>
					<h3><i class="icon-book"></i>&nbsp;&nbsp;Billing History</h3>
				</div>

				<div class="modal-body">

					<div class="row-fluid">
						<select name="billing_history_date_select" id="billing_history_date_select" class="m-wrap span3">
							<?php echo $html_array['plan_payments']['billing_history_dropdown']; ?>
						</select>

						<button class="btn dropdown-toggle pull-right print">Print <i class="icon-print"></i>
					</div>

					<hr>

					<div class="billing_history_details_wrapper">

						
					</div>

					<div class="loading_billing_history"></div>
				
				</div>

				<div class="modal-footer">
					<input type="submit" id="submit" data-dismiss="modal" class="btn light-green right" value="Close" />
				</div>
			</form>

		</div>



		<div id="cancel_subscription_modal" class="modal hide fade" tabindex="-1" aria-hidden="true">

			<div class="modal-header">
				<i class="icon-remove right" data-dismiss="modal" aria-hidden="true"></i>
				<h3><i class="icon-remove-sign"></i>&nbsp;&nbsp;Cancel Subscription</h3>
			</div>

			<div class="modal-body">

				<p>Just a few notes before you cancel your subscription:</p>

				<ul>
					<li>Cancellation will take effect at the end of your current billing cycle (<strong>Feb 21th, 2014</strong>)</li>
					<li>You can restart your subscription <strong>at any time</strong></li>
					<li>We will keep your data for a period of <strong>6 months</strong> after your last billing cycle</li>
					<li>After the cancellation has taken effect, your account will be downgraded to our <strong>Personal Planner</strong> account.</li>
				</ul>

				<div class="clearfix"></div>

			</div>

			<div class="modal-footer">
				<a href="#" id="cancel_subscription_submit" data-dismiss="modal" class="btn light-red right">Cancel Subscription</a>
				<a href="#" data-dismiss="modal" class="btn">Close</a>
			</div>

			<div class="clearfix"></div>
		</div>



		<div id="cancel_subscription_confirmation_modal" class="modal hide fade" tabindex="-1" aria-hidden="true">

			<div class="modal-header">
				<i class="icon-remove right" data-dismiss="modal" aria-hidden="true"></i>
				<h3><i class="icon-remove-sign"></i>&nbsp;&nbsp;Cancel Subscription</h3>
			</div>

			<div class="modal-body">

				<h4><i class="icon-ok"></i> Cancellation request received</h4>

				<p>Please remember:</p>

				<ul>
					<li>Your subcription will be cancelled at the end of your current billing cycle (<strong>Feb 21th, 2014</strong>)</li>
					<li>You have <strong>full access</strong> to all of your Business Pro account until then</li>
					<li>You can <strong>restart your subscription</strong> and get all of your data back for a period of <strong>6 months</strong> after your last billing cycle</li>
					<li>After the cancellation has taken effect, your account will be downgraded to our <strong>Personal Planner</strong> account.</li>
				</ul>

				<div class="clearfix"></div>

			</div>

			<div class="modal-footer">
				<a href="#" data-dismiss="modal" class="btn light-green right">Close</a>
			</div>

			<div class="clearfix"></div>
		</div>
	  
	  
   </div>
   <!-- END PAGE CONTAINER--> 
   <!-- END CONTAINER -->
   <?php
      require_once("inc/footer.php");
   ?>


</body>
<!-- END BODY -->
</html>
<?php
   require_once("inc/session_end.php");
?>