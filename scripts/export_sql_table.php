<?php
	
	session_start();

	$table = $_GET['table'];

	//ENTER THE RELEVANT INFO BELOW
	$db_name = DBNAME;
	$username = DBUSER;
	$password = DBPASS;
	$host_name = DBHOST;
	$export_path = $_SESSION['subdomain'] . "_" . $table . "_" . time() . ".sql";

	//DO NOT EDIT BELOW THIS LINE
	//Export the database and output the status to the page
	$command = 'mysqldump --opt -h' . $host_name . ' -u' . $username . ' -p' . $password . ' ' . $db_name . ' > /var/www/html/app/tmp_files/' . $export_path;
	exec($command, $output = array(), $worked);

	switch($worked) {
		case 0:
			echo 'Database <b>' .$db_name .'</b> successfully exported to <b>~/' .$export_path .'</b>';
			break;
		
		case 1:
			echo 'There was a warning during the export of <b>' .$db_name .'</b> to <b>~/' .$export_path .'</b>';
			break;
		
		case 2:
			echo 'There was an error during export. Please check your values:<br/><br/><table><tr><td>MySQL Database Name:</td><td><b>' .$db_name .'</b></td></tr><tr><td>MySQL User Name:</td><td><b>' .$username .'</b></td></tr><tr><td>MySQL Password:</td><td><b>NOTSHOWN</b></td></tr><tr><td>MySQL Host Name:</td><td><b>' .$host_name .'</b></td></tr></table>';
			break;
	}
?>