$(document).ready(function() {

	var Message = {

		init: function() {

			$('.send_message').off('click');
			$('.send_message').click(function() {
				newMessage.sendMessage();

				return false;
			});

			//AUDIO ELEMENT

			sound = new Audio();

			if(sound.canPlayType('audio/mpeg')) {
				sound.src = 'audio/notif.mp3';
			} else if(sound.canPlayType('audio/ogg')) {
				sound.src = 'audio/notif.ogg';
			} else {
				sound.src = null;
			}



			$('body').on('click', '.btn-sound', function() {

				if($(this).hasClass("on")) {

					$(this).removeClass("on");
					$(this).addClass("off");

					$(this).find("a").removeClass("light-blue");
					$(this).find("a").addClass("grey");

					$(this).find("i").removeClass("icon-volume-up");
					$(this).find("i").addClass("icon-volume-off");


					$.ajax({

						type: "POST",

						url: "ajax_functions",

						data: { action: "toggle_sound", state: "off" }

					}).done(function(result) {


					});


				} else if($(this).hasClass("off")) {

					$(this).removeClass("off");
					$(this).addClass("on");

					$(this).find("a").removeClass("grey");
					$(this).find("a").addClass("light-blue");

					$(this).find("i").removeClass("icon-volume-off");
					$(this).find("i").addClass("icon-volume-up");


					$.ajax({

						type: "POST",

						url: "ajax_functions",

						data: { action: "toggle_sound", state: "on" }

					}).done(function(result) {


					});
				}

				return false;
			});

			$(document).off('keydown');
			$(document).keydown(function(e) {

				var code = (e.keyCode ? e.keyCode : e.which);

				if (code == 13) {

					if($('.message_text').is(':focus')) {
						Message.sendMessage();
					}
				}
			});

		}, 

		sendMessage: function() {

			var message_text = $('.message_text').val();
			var convo_id = $('.convo_id').val();

			var trimmed = $.trim(message_text);



			if(trimmed != "") {

				message_text = message_text.replace(/\n/g, '<br />');

				$.ajax({

					type: "POST",

					url: "ajax_functions",

					data: { 

						action: "send_private_message",
						message_text: message_text,
						convo_id: convo_id
					}

				}).done(function(result) {

					if(result == 1) {



					}

				});

			}



			return false;
		}
	};

	Message.init();

});