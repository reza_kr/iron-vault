var FormWizard = function () {


    return {
        //main function to initiate the module
        init: function () {
            if (!jQuery().bootstrapWizard) {
                return;
            }

            // default form wizard
            $('#form-wizard-1').bootstrapWizard({
                'nextSelector': '.button-next',
                'previousSelector': '.button-previous',
                onTabClick: function (tab, navigation, index) {
                    return false;
                },
                onNext: function (tab, navigation, index) {
                    var total = navigation.find('li').length;
                    var current = index + 1;
                    // set wizard title
                    $('.step-title', $('#form-wizard-1')).text((index + 1) + ' of ' + total);
                    // set done steps
                    jQuery('li', $('#form-wizard-1')).removeClass("done");
                    var li_list = navigation.find('li');
                    for (var i = 0; i < index; i++) {
                        jQuery(li_list[i]).addClass("done");
                    }

                    if (current == 1) {
                        $('#form-wizard-1').find('.button-previous').hide();
                    } else {
                        $('#form-wizard-1').find('.button-previous').show();
                    }

                    if (current >= total) {
                        $('#form-wizard-1').find('.button-next').hide();
                        $('#form-wizard-1').find('.button-submit').show();
                    } else {
                        $('#form-wizard-1').find('.button-next').show();
                        $('#form-wizard-1').find('.button-submit').hide();
                    }

                },
                onPrevious: function (tab, navigation, index) {
                    var total = navigation.find('li').length;
                    var current = index + 1;
                    // set wizard title
                    $('.step-title', $('#form-wizard-1')).text((index + 1) + ' of ' + total);
                    // set done steps
                    jQuery('li', $('#form-wizard-1')).removeClass("done");
                    var li_list = navigation.find('li');
                    for (var i = 0; i < index; i++) {
                        jQuery(li_list[i]).addClass("done");
                    }

                    if (current == 1) {
                        $('#form-wizard-1').find('.button-previous').hide();
                    } else {
                        $('#form-wizard-1').find('.button-previous').show();
                    }

                    if (current >= total) {
                        $('#form-wizard-1').find('.button-next').hide();
                        $('#form-wizard-1').find('.button-submit').show();
                    } else {
                        $('#form-wizard-1').find('.button-next').show();
                        $('#form-wizard-1').find('.button-submit').hide();
                    }

                },
                onTabShow: function (tab, navigation, index) {
                    var total = navigation.find('li').length;
                    var current = index + 1;
                    var $percent = (current / total) * 100;
                    $('#form-wizard-1').find('.bar').css({
                        width: $percent + '%'
                    });
                }
            });

            $('#form-wizard-1').find('.button-previous').hide();
            $('#form-wizard-1 .button-submit').click(function () {
                alert('Finished! Hope you like it :)');
            }).hide();
        }

    };

}();