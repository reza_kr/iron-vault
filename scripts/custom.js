// Custom Javascript File
$(document).ready(function() {


	//GET THE PAGE NAME
	var page = window.location.pathname.substring(1);
	var hash = window.location.hash.substring(1); //Puts hash in variable, and removes the # character


	var $_GET = {}; //Hakish way of getting the GET parameters from the URL...

	document.location.search.replace(/\??(?:([^=]+)=([^&]*)&?)/g, function () {
	    function decode(s) {
	        return decodeURIComponent(s.split("+").join(" "));
	    }

	    $_GET[decode(arguments[1])] = decode(arguments[2]);
	});

	//document.write($_GET["test"]);

	if($_GET['r']) {

		var ref = $_GET['r'];

		/*if(ref == "ip_changed") {

			toastr.options.positionClass = 'toast-top-right';
		    toastr.options.extendedTimeOut = 20000; //1000;
		    toastr.options.timeOut = 20000;
		    toastr.options.fadeOut = 250;
		    toastr.options.fadeIn = 250;
		    toastr.warning('Your IP Address has changes. For your security, we require you to login again....<br><a target="_blank" href="https://www.ironvault.ca/faq#ip_changed">Why did this happen?</a>');
		}*/
	}


	function checkSession() {

		$.ajax({

			type: "POST",
			url: "ajax_functions",
			data: { 
				action: "check_session"
			}

		}).done(function(result) {

			if(result == 'locked') {

				if($('.login_overlay').is(':hidden')) {

					OverlayLock.init();

					$('.login_overlay').show();
					$('.login_overlay').animate({
						top: 0
					}, 750, function() {

						$('.login_overlay .container-fluid').fadeIn('fast');

					});
				}
			
			} else if(result == 'active') {

				if($('.login_overlay').is(':visible')) {

					$('.login_overlay .container-fluid').fadeOut('fast', function() {

						$('.login_overlay').animate({
							top: -2000
						}, 750, function() {

							$('.login_overlay').hide();
						});

					});
				}
			}

		});
	}

	var check_session_int = setInterval(checkSession, 5000);


	$('#overlay_unlock').submit(function() {

		$('#overlay_unlock .unlock').trigger('click');

		return false;
	});

	$('#overlay_unlock .unlock').click(function() {

		unlockSession();

		 return false;
	});


	function unlockSession() {

		var password = $('.login_overlay #password').val();

		$('.login_overlay .unlock').html('Just a Moment...');
		$('.login_overlay .unlock').addClass('disabled');

		$.ajax({

			type: "POST",
			url: "ajax_functions",
			data: { 
				action: "unlock_session",
				password: password
			}

		}).done(function(result) {

			$('.login_overlay .unlock').html('<i class="icon icon-unlock white"></i>&nbsp;&nbsp;Unlock Session');
			$('.login_overlay .unlock').removeClass('disabled');

			if(result == 'success') {

				$('.login_overlay .form_error').hide();

				if($('.login_overlay').is(':visible')) {

					$('.login_overlay .container-fluid').fadeOut('fast', function() {

						$('.login_overlay').animate({
							top: -2000
						}, 750, function() {

							$('.login_overlay').hide();
						});

					});
				}

			} else if(result == 'error') {

				$('.login_overlay .form_error').show();
				$('.login_overlay #password').val('');
			}

		});

	}


	//used for the notification system to track which convo's have notifications already displayed for this user... used in notifications_client.js
	// if($(convo_notifs).length > 0) {
	// 	if(convo_notifs != 0) {
	// 		convo_notifs_array = convo_notifs.split(",");
	// 	}
	// }


	$('body').on('click', '.disabled', function() {
		return false;
	});



	$('a.fancybox').fancybox();


	$('.email_notification > .dropdown-toggle').click(function() {

		$('.email_widget').toggle();

	});



	$(document).mouseup(function(e) {

	    var email_widget = $('.email_widget');
	    var email_widget_button = $('.email_notification > .dropdown-toggle');
	    var email_widget_button_icon = $('.email_notification > .dropdown-toggle i');

	    if (!email_widget.is(e.target) && email_widget.has(e.target).length === 0 && !email_widget_button.is(e.target) && !email_widget_button_icon.is(e.target)) {
	        email_widget.hide();
	    }
	});


	$('.email_widget .dropdown-menu li a').click(function(){

		$(this).closest('.btn-group').removeClass('open');
	});




	$("a#fullscreen").on('click', function() {

	    if (document.documentElement.requestFullscreen) {
          	document.documentElement.requestFullscreen();
        } else if (document.documentElement.mozRequestFullScreen) {
          	document.documentElement.mozRequestFullScreen();
        } else if (document.documentElement.webkitRequestFullscreen) {
          	document.documentElement.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
        } else if (document.documentElement.msRequestFullScreen) {
        	document.documentElement.msRequestFullScreen();
        }
	});



	// TOP ABR SEARCH
	// =====================================================================
	$('.top_bar_search').focusin(function() {

		$('.top_bar_search .search_query').animate({
			width: '350',
			backgroundColor: '#FAFAFA'
		}, 500, function() {
	
		});

		if($('.top_bar_search .search_query').val() != "") {
			$('.top_bar_search_results').show();
		}

	});

	$('.top_bar_search').focusout(function() {

		$('.top_bar_search_results').hide();

		if($('.top_bar_search .search_query').val() == "") {

			$('.top_bar_search .search_query').animate({
				width: '200',
				backgroundColor: '#EEE'
			}, 500);
		}

	});



	var t_requests = new Array();

	$('.top_bar_search .search_query').keyup(function(e){
		$('.top_bar_search_results').show();
		top_search();
	});


	function top_search() {

		var t_query = $('.top_bar_search .search_query').val();
		t_query = $.trim(t_query);

		if(t_query != "") {

			$('.top_bar_search_results .search_results_header .loading').show();

			for(var i = 0; i < t_requests.length; i++) {
    			t_requests[i].abort();
			}

			t_requests = [];

        	t_requests.push(
				$.ajax({

					type: "POST",
					url: "ajax_functions",
					data: { 
						action: "search",
						query: t_query,
						filter: filter
					}

				}).done(function(result) {

					$('.top_bar_search_results').html(result);

					$('.top_bar_search_results .portlet-body:empty').closest('.portlet').remove();

					$('.top_bar_search_results .search_results_header .loading').hide();

				})
			);
        
        } else {

        	for(var i = 0; i < t_requests.length; i++) {
    			t_requests[i].abort();
			}

			t_requests = [];

			$('.top_bar_search_results').hide();
        	$('.top_bar_search_results').html('<div class="search_results_header"><div class="loading"></div></div>');
        }
	}

	// =====================================================================



	/* used to override TD fields that have buttons in them. If overflow: hidden, the dropdown part of buttons won't show. */
	$('td.button').css('overflow', 'visible');

	$('.dataTables_paginate a').click(function() {

		$('td.button').css('overflow', 'visible');
	});



	//Check for window focus/blur events
	var is_active = true;

	$(window).focus(function() {
	    is_active = true;
	});

	$(window).blur(function() {
	    is_active = false;
	});



	$('.modal').on('shown', function() {
	    $(this).find('input:visible:first').focus();
	})



	var exclude_pages = new Array("index","features","pricing","signup","faq","contact","terms_of_use","privacy_policy","login","logout","lock","coming_soon");

	/*if($.inArray(page, exclude_pages) < 0 && page != "index" && page != "") {

		setInterval(function() {

			var isConnected = hasConnection();

			if (!isConnected && overlay_dismiss == false) { 

				setTimeout(function() {

					$('.overlay').fadeIn();
				}, 2000);

			} else { 

				$('.overlay').fadeOut()
			}

		}, 5000);
	}


	function hasConnection() {
		var s = $.ajax({ 
			type: "HEAD",
			url: "ping",
			//url: window.location.href.split("?")[0] + "?" + Math.random(),
			async: false
		}).status;

		return s >= 200 && s < 300 || s === 304;
	};*/

	var confirm_pageunload = false;


	var overlay_dismiss = false;

	$('.overlay').click(function() {

		overlay_dismiss = true;
		$(this).fadeOut();
	});

	$('.dismiss_overlay').click(function() {

		overlay_dismiss = true;
		$('.overlay').fadeOut();

		return false;
	});


	//RESIZE THE CHAT WINDOW WHEN BROWSER WINDOW IS RESIZED

	var width = $(window).width();

	var height = $(window).height();

	$('.page-sidebar').css("cssText", "height: " + (height - 40) + "px !important;");

	if(page != "open_chat") {
		$('.chat .slimScrollDiv').height(height - 200);
	}

	$('.popout-chat .slimScrollDiv').height(height - 157);

	$('.messages .slimScrollDiv').height(height - 174);
	$('.messages_list .portlet-body').height(height - 150);


	$('.gmaps').height(height - 193);
	$('.saved_locations').height(height - 254);
	$('.directions_list').height(height - 269);

	$('.categories').height(height - 153);
	$('.products').css('min-height', height - 153);


	//$('.page-content .container-fluid').fadeIn(250, function() {

	//Callback functions used to load all dependencies that do not work witohut having visible elements.. i.e. height determination of a div.
	var messages_div_height = $('#messages .chats').height();

	$('#messages .scroller').slimScroll({

	    scrollTo: messages_div_height

	});

	$('#messages .chats').animate({

		opacity: 1,
		duration: 200
	});

	//});


	/*if($('.page-content .container-fluid .row-fluid').height() < height) {

		$('.page-content .container-fluid > .row-fluid').animate({
			height: height - 85
		});
	}*/



	$(window).resize(function() {


		var fullwidth = window.innerWidth;

		var height = $(window).height();

		$('.popout-chat .slimScrollDiv').height(height - 157);


	// 	var width = $(window).width();

	// 	var height = $(window).height();


	// 	// if($('.page-content .container-fluid .row-fluid').height() < height) {

	// 	// 	$('.page-content .container-fluid > .row-fluid').animate({
	// 	// 		height: height - 85
	// 	// 	});
	// 	// }


	// 	$('.page-sidebar').css("cssText", "height: " + (height - 40) + "px !important;");


	// 	if(page != "open_chat") {
	// 		$('.chat .slimScrollDiv').height(height - 338);
	// 	}

	// 	$('.popout-chat .slimScrollDiv').height(height - 190);

	// 	$('.messages .slimScrollDiv').height(height - 266);
	// 	$('.messages_list .portlet-body').height(height - 197);

	// 	$('.gmaps').height(height - 198);
	// 	$('.saved_locations').height(height - 264);
	// 	$('.directions_list').height(height - 274);

	// 	$('.categories').height(height - 315);


	// 	var div_height = $('#chats .chats').height();

	// 	$('#chats .scroller').slimScroll({

	// 		scrollTo: div_height

	// 	});

	});

	
	
	// $('.email_loading').fadeIn('fast');
	// var imap_setup = false;
	// var fetch_in_progress = false;

	// $.ajax({

 //        url: 'ajax_functions',  //server script to process data
 //        type: 'POST',
 //        data: {
 //        	action: "imap_fetch_from_db"
 //        }

 //    }).done(function(data) {

 //    	var data_array = data.split('%lbr');
 //    	var status = data_array[0];
 //    	var unread_emails = data_array[1];
 //    	var num_emails = data_array[2];
 //    	var last_update = data_array[3];
 //    	var email_address = data_array[4];

 //    	if(status == "success") {

 //    		imap_setup = true;

	//     	if(unread_emails != "") {
	    	
	//     		$('.email_inbox').html(unread_emails);

	//     	} else {

	//     		$('.email_inbox').html('<li>No new emails to show</li>');
	//     	}


	//     	//$('.email_widget .email_count').text(num_emails);
	//     	$('.email_widget .email_last_update').text(last_update);

	//     	$('.email_widget .inbox_email_address').text(email_address);


	//     	$('.email_loading').fadeOut('fast', function() {

	// 			$('.email_inbox_wrapper').fadeIn('fast');

	// 		});

	// 	} else if(status == "failed") {

	// 		//no IMAP has been setup... show the IMAP setup div.
	// 		imap_setup = false;

	// 		$('.email_loading').fadeOut('fast', function() {

	// 			$('.email_choose_provider').fadeIn('fast');

	// 		});
	// 	}

	// });
	

	//Closing of the div on click away.
	// $(document).mouseup(function (e)
	// {
	//     var container = $(".email_widget_wrapper");

	//     if (!container.is(e.target) // if the target of the click isn't the container...
	//         && container.has(e.target).length === 0) // ... nor a descendant of the container
	//     {
	//         container.removeClass('open');
	//     }
	// });


	// if(imap_setup == true) {

	// 	var imap_fetch = setInterval(function() {

	// 		imapFetch();
	// 	}, 60000);
	// }


	// $('.email_widget .refresh_email').click(function() {

	// 	imapFetch();

	// });



	// function imapFetch() {

	// 	if(fetch_in_progress == false) {
	// 		fetch_in_progress = true;

	// 		$('.email_inbox').html('');

	// 		$('.email_last_update_wrapper').fadeOut('fast', function() {

	// 			$('.email_fetch_in_progress').fadeIn('fast');
	// 		});

	// 		$.ajax({

	// 	        url: 'ajax_functions',  //server script to process data
	// 	        type: 'POST',
	// 	        data: {
	// 	        	action: "imap_fetch"
	// 	        }

	// 	    }).done(function(data) {

	// 	    	fetch_in_progress = false;


	// 	    	var data_array = data.split('%lbr');
	// 	    	var unread_emails = data_array[0];
	// 	    	var num_emails = data_array[1];
	// 	    	var last_update = data_array[2];
	// 	    	var imap_error = data_array[3];
	// 	    	alert(imap_error);

	// 	    	$('.email_fetch_in_progress').fadeOut('fast', function() {

	// 	    		$('.email_widget .email_count').text(num_emails);
 //    				$('.email_widget .email_last_update').text(last_update);

	// 				$('.email_last_update_wrapper').fadeIn('fast');
	// 			});

	// 	    	if(imap_error != 1) {

	// 		    	if(unread_emails != "") {
			    	
	// 		    		$('.email_inbox').html(unread_emails);
	// 		    		imap_setup = true;

	// 		    	} else {

	// 		    		$('.email_inbox').html('<li>No new emails to show</li>');
	// 		    	}

	// 		    } else {

	// 		    	$('.email_inbox').html('<li>Unable to connect to mail server.</li>');
	// 		    }
	// 	    });
	// 	}
	// }


	// Email Widget 
	// $('.widget_toggle').click(function() {

	// 	if($('.email_widget_wrapper').is('.open')) {

	// 		$('.email_widget_wrapper').removeClass('open');
	// 		setTimeout(function() {
	// 			$('.widget_toggle i').removeClass('icon-chevron-down').addClass('icon-chevron-up');
	// 		}, 300);
	// 	} else {

	// 		$('.email_widget_wrapper').addClass('open');
	// 		setTimeout(function() {
	// 			$('.widget_toggle i').removeClass('icon-chevron-up').addClass('icon-chevron-down');
	// 		}, 300);
	// 	}
	// });


	// $('.email_widget .clear_email_inbox').click(function() {

	// 	$('.email_inbox').html('<li>No new emails to show</li>');


	// 	$.ajax({

	//         url: 'ajax_functions',  //server script to process data
	//         type: 'POST',
	//         data: {
	//         	action: "imap_clear_emails"
	//         }

	//     }).done(function(data) {

	//     });


	// 	return false;
	// });


	// $('.email_widget .disconnect_imap').click(function() {

	// 	$('.email_inbox_wrapper').fadeOut('fast', function() {

	// 		$('.email_choose_provider').fadeIn('fast');
	// 	})

	// 	$.ajax({

	//         url: 'ajax_functions',  //server script to process data
	//         type: 'POST',
	//         data: {
	//         	action: "imap_disconnect"
	//         }

	//     }).done(function(data) {

	//     });


	// 	return false;
	// });


	// $('.email_widget .email_error_back').click(function() {

	// 	$('.email_error').fadeOut('fast', function() {

	// 		$('.imap_configuration').fadeIn('fast');
	// 	});

	// 	return false;
	// });


	// $('.email_choose_provider a').click(function() {

	// 	var provider = $(this).attr('id');
	// 	var inbox_url = "";
	// 	var imap_address = "";
	// 	var port = "";

	// 	$('.email_choose_provider').fadeOut('fast', function() {

	// 		if(provider == "gmail") {

	// 			var inbox_url = "mail.google.com";
	// 			imap_address = "imap.gmail.com";
	// 			port = "993";

	// 		} else if(provider == "outlook") {

	// 			var inbox_url = "mail.live.com";
	// 			imap_address = "imap-mail.outlook.com";
	// 			port = "993";

	// 		} else if(provider == "yahoo") {

	// 			var inbox_url = "mail.yahoo.com";
	// 			imap_address = "imap.mail.yahoo.com";
	// 			port = "993";

	// 		} else if(provider == "icloud") {

	// 			var inbox_url = "mail.icloud.com";
	// 			imap_address = "imap.mail.me.com";
	// 			port = "993";

	// 		} else if(provider == "aol") {

	// 			var inbox_url = "mail.aol.com";
	// 			imap_address = "imap.aol.com";
	// 			port = "993";

	// 		} else if(provider == "mail_com") {

	// 			var inbox_url = "www.mail.com/int";
	// 			imap_address = "imap.mail.com";
	// 			port = "993";

	// 		} else if(provider == "gmx") {

	// 			var inbox_url = "www.gmx.com";
	// 			imap_address = "imap.gmx.com";
	// 			port = "993";

	// 		} else {

	// 			//leave empty

	// 		}


	// 		$('#imap_config #inbox_url').val(inbox_url);
	// 		$('#imap_config #imap_address').val(imap_address);
	// 		$('#imap_config #port').val(port);

	// 		$('.imap_configuration').fadeIn('fast');

	// 	});
	// });


	// $('.email_widget .imap_config_back').click(function() {

	// 	$('.imap_configuration').fadeOut('fast', function() {

	// 		$('.email_choose_provider').fadeIn('fast');
	// 	});

	// 	return false;
	// });


	// $('#imap_config input').focus(function() {
	// 	$(this).removeClass('error');
	// });


	// $('#imap_config #submit').click(function() {

	// 	var errors = 0;

	// 	if($('#imap_config #inbox_url').val() == "") {
	// 		$('#imap_config #inbox_url').addClass('error');
	// 		errors++;
	// 	}

	// 	if($('#imap_config #imap_address').val() == "") {
	// 		$('#imap_config #imap_address').addClass('error');
	// 		errors++;
	// 	}
		
	// 	if($('#imap_config #port').val() == "") {
	// 		$('#imap_config #port').addClass('error');
	// 		errors++;
	// 	}
		
	// 	if($('#imap_config #email').val() == "") {
	// 		$('#imap_config #email').addClass('error');
	// 		errors++;
	// 	}
		
	// 	if($('#imap_config #password').val() == "") {
	// 		$('#imap_config #password').addClass('error');
	// 		errors++;
	// 	}


	// 	if(errors == 0) {

	// 		$('.imap_configuration').fadeOut('fast', function() {

	// 			$('.email_loading').fadeIn('fast');

	// 			var inbox_url = $('#imap_config #inbox_url').val();
	// 			var imap_address = $('#imap_config #imap_address').val();
	// 			var port = $('#imap_config #port').val();
	// 			var email = $('#imap_config #email').val();
	// 			var password = $('#imap_config #password').val();

	// 			$.ajax({

	// 		        url: 'ajax_functions',  //server script to process data
	// 		        type: 'POST',
	// 		        data: {
	// 		        	action: "imap_connect",
	// 		        	inbox_url: inbox_url,
	// 		        	imap_address: imap_address,
	// 		        	port: port,
	// 		        	email: email,
	// 		        	password: password
	// 		        }

	// 		    }).done(function(data) {
	// 		    	console.log(data);
	// 		    	var data_array = data.split('%lbr');
	// 		    	var unread_emails = data_array[0];
	// 		    	var num_emails = data_array[1];
	// 		    	var last_update = data_array[2];
	// 		    	var imap_error = data_array[3];
	// 		    	alert(imap_error);


	// 		    	if(imap_error != 1) {

	// 			    	if(unread_emails != "") {
				    	
	// 			    		$('.email_inbox').html(unread_emails);
	// 			    		imap_setup = true;

	// 			    	} else {

	// 			    		$('.email_inbox').html('<li>No new emails to show</li>');
	// 			    	}

	// 			    	$('.email_widget .email_last_update').text(last_update);

	// 			    } else {

	// 			    	$('.email_inbox').html('<li>Unable to connect to mail server.</li>');
	// 			    }



	// 		  //   	if(unread_emails == "") {
			    	
	// 		  //   		$('.email_inbox').html('<li>No new emails to show</li>');

	// 		  //   	} else {

	// 		  //   		$('.email_inbox').html(unread_emails);
	// 		  //   		imap_setup = true;

	// 		  //   		$('.email_widget .email_last_update').text(last_update);
	// 		  //   	}


	// 		  //   	$('.email_loading').fadeOut('fast', function() {

	// 				// 	$('.email_inbox_wrapper').fadeIn('fast');

	// 				// });

	// 			});

	// 		});

	// 	}

	// 	return false;
	// });




	// INIT GOOGLE MAPS!
	if(page == "maps") {
		$('body').on("fadeInComplete", function(){
			initMaps();
		});
	}
	// *******************


	$(document).keydown(function(e) {

		var code = (e.keyCode ? e.keyCode : e.which);

		if (code == 27) {

			lockSession();
			
		}


	});


	$('#lock_session').click(function() {

		lockSession();
	});


	function lockSession() {

		$.ajax({

			type: "POST",
			url: "ajax_functions",
			data: { 
				action: "lock_session"
			}

		}).done(function(result) {


		});

		OverlayLock.init();

		$('.login_overlay').show();
		$('.login_overlay').animate({
			top: 0
		}, 750, function() {

			$('.login_overlay .container-fluid').fadeIn('fast');

			logoff_sound = new Audio();

			if(logoff_sound.canPlayType('audio/mpeg'))

				logoff_sound.src = 'audio/logoff.mp3';

			else if(logoff_sound.canPlayType('audio/ogg'))

				logoff_sound.src = 'audio/logoff.ogg';

			else

				logoff_sound.src = null;


			logoff_sound.play();
		});

	}


	if(!$('body').hasClass("page-sidebar-closed")) {

		$('.menu_footer').fadeIn();
	
	} else {

		$('.menu_footer').hide();
	}


	$('.sidebar-toggler').click(function() {

		if($('body').hasClass("page-sidebar-closed")) {
			var toggle = 0;
			$('.menu_footer').fadeIn();
		} else {

			var toggle = 1;
			$('.menu_footer').hide();
		}

		$.ajax({

	        url: 'ajax_functions',  //server script to process data
	        type: 'POST',
	        data: {

	        	action: "toggle_page_sidebar",
	        	state: toggle
	        }

	    }).done(function(data) {

		});

	});



	/* ********************** */

	$('.page-lock').fadeIn();

	/* ********************** */

	var timestamp = Math.round(new Date().getTime() / 1000);

	$.ajax({

        url: 'ajax_functions',  //server script to process data
        type: 'POST',
        data: {

        	action: "update_last_active",
        	timestamp: timestamp
        }

    }).done(function(data) {

	});

	$(window).focus(function() {

		timestamp = Math.round(new Date().getTime() / 1000);
		
		$.ajax({

	        url: 'ajax_functions',  //server script to process data
	        type: 'POST',
	        data: {

	        	action: "update_last_active",
	        	timestamp: timestamp
	        }

	    }).done(function(data) {

		});
	});

	$(window).blur(function() {
		
		timestamp = Math.round(new Date().getTime() / 1000);
		
		$.ajax({

	        url: 'ajax_functions',  //server script to process data
	        type: 'POST',
	        data: {

	        	action: "update_last_active",
	        	timestamp: timestamp
	        }

	    }).done(function(data) {
	    	
		});

	});

	var exclude_pages = new Array("index","features","pricing","signup","faq","contact","terms_of_use","privacy_policy","login","logout","lock","coming_soon");

	/*if($.inArray(page, exclude_pages) < 0 && page != "index" && page != "") {

		//lock_timeout is loaded from php to javascript in the footer.php file
		lock_timeout = lock_timeout * 60000; //minutes to miliseconds

		$(document).bind("idle.idleTimer", function(){

			$.ajax({

		        url: 'ajax_functions',  //server script to process data
		        type: 'POST',
		        data: {

		        	action: "check_last_active",
		        	timestamp: timestamp
		        }

		    }).done(function(data) { alert(data);

		    	if(data == "inactive") {

		    		//window.location.href = "lock";

		    	} else if(data == "active") {

		    		$.idleTimer(lock_timeout);
		    	}
		    	
			});

		});

		$.idleTimer(lock_timeout);
	}*/


	var username_checked = false;

	function check_username() {

		if($('#signup_form #first_name').val() != "" && $('#signup_form #last_name').val() != "" && username_checked == false) {

			username_checked = true;

			var first_name = $('#signup_form #first_name').val();
			var last_name = $('#signup_form #last_name').val();
			var username = first_name.charAt(0) + last_name;
			username = username.toLowerCase();

			$('#signup_form #username').val(username);
		}
	}


	function check_email() {

		var email = $('#signup_form #email').val();
		$('#signup_form #email').parent().append("<div class='loading'><img src='img/loader_small.gif'/></div>");

		$.ajax({

	        url: 'ajax_functions',  //server script to process data
	        type: 'POST',
	        data: {

	        	action: "check_email",
	        	email: email
	        }

	    }).done(function(data) {

			if(data == false) {

				$('#signup_form #email').css("border", "1px solid #f85e5e");
				$('#signup_form #email').attr("rel", "tooltip").attr("title", "Email is in use.");
	    		$('#signup_form #email').tooltip({placement: 'right', trigger: 'manual'}).tooltip('show');
			
			} else {

				$('#signup_form #email').css("border", "1px solid #e5e5e5");
				$('#signup_form #email').tooltip('destroy');

			}

			$('#signup_form .loading').remove();			

		});
	}



	function stripHTML(string) {
	   var tmp = document.createElement("DIV");
	   tmp.innerHTML = string;
	   return tmp.textContent || tmp.innerText || "";
	}




	$('body').on("click", ".print", function() {

		$('.btn-group.open').removeClass("open");

		window.print();

		return false;
	})




	//Currency Only

	$('.currency').keydown(function(e) {

		var code = (e.keyCode ? e.keyCode : e.which);

		if ((code != 46 && code > 31 && (code < 48 || code > 57)) && code != 190) {

			return false;

		} else {

			return true;

		}

	});

	// ////////////


	//Quantity Only

	$('.quantity').keydown(function(e) {

		var code = (e.keyCode ? e.keyCode : e.which);

		if (code != 46 && code != 190 && code > 31 && (code < 48 || code > 57)) {

			return false;

		} else {

			return true;

		}

	});

	// ////////////




	/*$('#signup_form .form-actions').slideDown(400);


	var personal_info = "To set up your account, we require some simple personal information that enables you to log in later.<br>";
   

	$('#signup_form #first_name').focus(function() {

		$('#personal_info').html(personal_info + "<hr>Your <strong>First Name</strong> and <strong>Last Name</strong> will be used to identity you as a user within your business account.");

	});

	$('#signup_form #last_name').focus(function() {

		$('#personal_info').html(personal_info + "<hr>Your <strong>First Name</strong> and <strong>Last Name</strong> will be used to identity you as a user within your business account.");

	});

	$('#signup_form #username').focus(function() {

		$('#personal_info').html(personal_info + "<hr>For easier access, you will be able to use your <strong>Username</strong> to login in the future.<br><br>May contain letters, numbers and underscore only.");

	});

	$('#signup_form #email').focus(function() {

		$('#personal_info').html(personal_info + "<hr>Having your <strong>Email Address</strong> on file allows us to verify your identity as the account holder.");

	});

	$('#signup_form #password').focus(function() {

		$('#personal_info').html(personal_info + "<hr>Choosing your <strong>Password</strong> is the most important step in this signup process. Having a secure password is the first step to keeping out unwanted intruders.<br><br> Password must be:<br> - Minimum of 8 characters<br> - Contain at least 1 digit<br> - Contain uppercase & lowercase letters");

	});

	$('#signup_form #password_confirm').focus(function() {

		$('#personal_info').html(personal_info + "<hr>Choosing your <strong>Password</strong> is the most important step in this signup process. Having a secure password is the first step to keeping out unwanted intruders.<br><br> Password must be:<br> - Minimum of 8 characters<br> - Contain at least 1 digit<br> - Contain uppercase & lowercase letters");

	});



	var business_profile = "Your business profile consists of basic information such as the business name and the business type.<br>";
   

	$('#signup_form #business_name').focus(function() {

		$('#business_profile').html(business_profile + "<hr>The name of your business will be displayed on various pages across the application to help and personalize the user's environment.");

	});

	$('#signup_form #subdomain').focus(function() {

		$('#business_profile').html(business_profile + "<hr>The <strong>Short Name</strong> of your business will be used for easy access to your application.<br><br> You will be able to login to your account by visiting '<strong>ironvault.ca/businessname</strong>'.");

	});

	$('#signup_form #business_type').focus(function() {

		$('#business_profile').html(business_profile + "<hr>To better serve our customers, we study the types of businesses that use our product. In turn, we are able to release functionality that will aid our members the most.");

	});

	$('#signup_form #country').focus(function() {

		$('#business_profile').html(business_profile + "<hr>Having an idea of the country of origin will give us a better idea of the demographcis of our members.");

	});

	$('#signup_form #address').focus(function() {

		$('#business_profile').html(business_profile + "<hr>The input of your business' address is completely optional. We do not mail out flyers or junk mail but rather take the provided information to help us better understand the demographcis of our members.");

	});

	$('#signup_form #city').focus(function() {

		$('#business_profile').html(business_profile + "<hr>The input of your business' address is completely optional. We do not mail out flyers or junk mail but rather take the provided information to help us better understand the demographcis of our members.");

	});

	$('#signup_form #province').focus(function() {

		$('#business_profile').html(business_profile + "<hr>The input of your business' address is completely optional. We do not mail out flyers or junk mail but rather take the provided information to help us better understand the demographcis of our members.");

	});

	$('#signup_form #postal_code').focus(function() {

		$('#business_profile').html(business_profile + "<hr>The input of your business' address is completely optional. We do not mail out flyers or junk mail but rather take the provided information to help us better understand the demographcis of our members.");

	});


	$('#signup_form #first_name').change(function() {

		check_username();
	});

	$('#signup_form #last_name').change(function() {

		check_username();
	});

	$('#signup_form #email').blur(function() {

		check_email();
	});


	$('#signup_form input, #signup_form select').blur(function() {

		$(this).css("border", "1px solid #e5e5e5");
	});

	$('#signup_form input, #signup_form select').focus(function() {

		$(this).css("border", "1px solid #AAA");
	});


	$('.plans select').attr("disabled", "disabled");
	$('.plans select.standard').removeAttr("disabled");

	var selected_val = $('.plans select.standard option:selected').val();
	var currently_selected = $('.plans select.standard option:selected').text();

	$('#plan_selected').val(selected_val);
	$('.currently_selected').html("Currently Selected:&nbsp;&nbsp;<strong>Standard Plan</strong> - " +currently_selected);


	$('.plans .basic').click(function() {
		$('.plans .pricing-table2').removeClass("selected");
		$(this).addClass("selected");

		$('.choose_plan input').removeAttr("checked");
		$('.choose_plan #basic').attr("checked", "checked");

		$('.choose_plan span.checked').removeClass("checked");
		$('.choose_plan #basic').parent().addClass("checked");

		$('.plans select').attr("disabled", "disabled");
		$('.plans select.basic').removeAttr("disabled");

		var selected_val = $('.plans select.basic option:selected').val();
		var currently_selected = $('.plans select.basic option:selected').text();

		$('#plan_selected').val(selected_val);
		$('.currently_selected').html("Currently Selected:&nbsp;&nbsp;<strong>Basic Plan</strong> - " +currently_selected);

		$('.trial_plan').removeClass("orange").addClass("green");
	});

	$('.plans .standard').click(function() {
		$('.plans .pricing-table2').removeClass("selected");
		$(this).addClass("selected");

		$('.choose_plan input').removeAttr("checked");
		$('.choose_plan input#standard').attr("checked", "checked");

		$('.choose_plan span.checked').removeClass("checked");
		$('.choose_plan #standard').parent().addClass("checked");

		$('.plans select').attr("disabled", "disabled");
		$('.plans select.standard').removeAttr("disabled");

		var selected_val = $('.plans select.standard option:selected').val();
		var currently_selected = $('.plans select.standard option:selected').text();

		$('#plan_selected').val(selected_val);
		$('.currently_selected').html("Currently Selected:&nbsp;&nbsp;<strong>Standard Plan</strong> - " +currently_selected);

		$('.trial_plan').removeClass("orange").addClass("green");
	});

	$('.plans .professional').click(function() {
		$('.plans .pricing-table2').removeClass("selected");
		$(this).addClass("selected");

		$('.choose_plan input').removeAttr("checked");
		$('.choose_plan input#professional').attr("checked", "checked");

		$('.choose_plan span.checked').removeClass("checked");
		$('.choose_plan #professional').parent().addClass("checked");

		$('.plans select').attr("disabled", "disabled");
		$('.plans select.professional').removeAttr("disabled");

		var selected_val = $('.plans select.professional option:selected').val();
		var currently_selected = $('.plans select.professional option:selected').text();

		$('#plan_selected').val(selected_val);
		$('.currently_selected').html("Currently Selected:&nbsp;&nbsp;<strong>Professional Plan</strong> - " +currently_selected);

		$('.trial_plan').removeClass("orange").addClass("green");
	});

	$('.plans .enterprise').click(function() {
		$('.plans .pricing-table2').removeClass("selected");
		$(this).addClass("selected");

		$('.choose_plan input').removeAttr("checked");
		$('.choose_plan input#enterprise').attr("checked", "checked");

		$('.choose_plan span.checked').removeClass("checked");
		$('.choose_plan #enterprise').parent().addClass("checked");

		$('.plans select').attr("disabled", "disabled");
		$('.plans select.enterprise').removeAttr("disabled");

		var selected_val = $('.plans select.enterprise option:selected').val();
		var currently_selected = $('.plans select.enterprise option:selected').text();

		$('#plan_selected').val(selected_val);
		$('.currently_selected').html("Currently Selected:&nbsp;&nbsp;<strong>Enterprise Plan</strong> - " +currently_selected);

		$('.trial_plan').removeClass("orange").addClass("green");
	});


	$('.choose_plan label#basic').click (function() {
		$('.plans .pricing-table2').removeClass("selected");
		$('.plans .basic').addClass("selected");

		$('.choose_plan input').removeAttr("checked");
		$('.choose_plan #basic').attr("checked", "checked");

		$('.plans select').attr("disabled", "disabled");
		$('.plans select.basic').removeAttr("disabled");

		$('.trial_plan').removeClass("orange").addClass("green");
	});

	$('.choose_plan label#standard').click (function() {
		$('.plans .pricing-table2').removeClass("selected");
		$('.plans .standard').addClass("selected");

		$('.choose_plan input').removeAttr("checked");
		$('.choose_plan #standard').attr("checked", "checked");

		$('.plans select').attr("disabled", "disabled");
		$('.plans select.standard').removeAttr("disabled");

		$('.trial_plan').removeClass("orange").addClass("green");
	});

	$('.choose_plan label#professional').click (function() {
		$('.plans .pricing-table2').removeClass("selected");
		$('.plans .professional').addClass("selected");

		$('.choose_plan input').removeAttr("checked");
		$('.choose_plan #professional').attr("checked", "checked");

		$('.plans select').attr("disabled", "disabled");
		$('.plans select.professional').removeAttr("disabled");

		$('.trial_plan').removeClass("orange").addClass("green");
	});

	$('.choose_plan label#enterprise').click (function() {
		$('.plans .pricing-table2').removeClass("selected");
		$('.plans .enterprise').addClass("selected");

		$('.choose_plan input').removeAttr("checked");
		$('.choose_plan #enterprise').attr("checked", "checked");

		$('.plans select').attr("disabled", "disabled");
		$('.plans select.enterprise').removeAttr("disabled");

		$('.trial_plan').removeClass("orange").addClass("green");
	});


	$('.trial_plan').click(function() {
		$('.plans .pricing-table2').removeClass("selected");
		$('.choose_plan input').removeAttr("checked");
		$('.choose_plan span.checked').removeClass("checked");
		$('.plans select').attr("disabled", "disabled");

		$('#plan_selected').val("trial");
		$('.currently_selected').html("Currently Selected:&nbsp;&nbsp;<strong>Basic Plan</strong> - 14-Day Trial");

		$('.trial_plan').removeClass("green").addClass("orange");
	});


	$('.steps .step').click(function() {

		return false;
	});


	$('.button-next').click(function() {

		if($('#signup_form #tab4').hasClass("active")) {

			var first_name = $('#first_name').val();
			var last_name = $('#last_name').val();
			var username = $('#username').val();
			var email = $('#email').val();
			var business_name = $('#business_name').val();
			var subdomain = $('#subdomain').val();
			var business_type = $('#business_type option:selected').text();
			var country = $('#country option:selected').text();
			var address = $('#address').val();
			var city = $('#city').val();
			var province = $('#province').val();
			var postal_code = $('#postal_code').val();
			
			var personal_info = "<h4>Personal Info</h4><p>";

			personal_info += "First Name: <strong>" + first_name +
				"</strong><br>Last Name: <strong>" + last_name +
				"</strong><br>Username: <strong>" + username +
				"</strong><br>Email: <strong>" + email;
			personal_info += "</p>";

			var business_profile = "<h4>Business Profile</h4><p>";
				
			business_profile += "Business Name: <strong>" + business_name +
				"</strong><br>Short Name: <strong>" + subdomain +
				"</strong><br>Business Type: <strong>" + business_type +
				"</strong><br>Country: <strong>" + country + "</strong>";

			if(address != "") business_profile += "<br>Address: <strong>" + address + "</strong>";
			if(city != "") business_profile += "<br>City: <strong>" + city + "</strong>";
			if(province != "") business_profile += "<br>Province: <strong>" + province + "</strong>";
			if(postal_code != "") business_profile += "<br>Postal Code: <strong>" + postal_code + "</strong>";

			business_profile += "</p>";

			if($('#plan_selected').val() == "trial") {

				var plan_details = "<h4>Plan Details</h4><p><br><br>Selected Plan: <strong>Basic Plan - 14-Day Trial" +
				"</strong><br>Term Duration/Monthly Cost: <strong>14 Days/Free</strong></p>";



			} else {
				var plan_selected = $('#plan_selected').val();
				var plan, term;
				var plan_details = "<h4>Plan Details</h4><p><br><br>Selected Plan: <strong>" + plan +
				"</strong><br>Term Duration/Monthly Cost: <strong>" + term + "</strong></p>";
			}



			var payment_details = "<h4>Payment Details</h4>";

			if($('#plan_selected').val() != "trial") {

				payment_details += "You will be taken to PayPal"
			}


			$('.personal_info').html(personal_info);
			$('.business_profile').html(business_profile);
			$('.plan_details').html(plan_details);
			$('.payment_details').html(plan_details);
		}
	});*/



	if(page == "setup_wizard") {

		function validateSetupWizard() {

			var empty = 0;

			$('#setup_wizard .tab-pane.active input[type!=file]').each(function() {

				if($(this).val() == '') {

					empty++;
				}
			});	

			$('#setup_wizard .tab-pane.active select').each(function() {

				if($(this).find('option:selected').val() == '' || $(this).find('option:selected').val() == '-1') {

					empty++;

				}

			});

			if(empty > 0) {

				$('#setup_wizard .continue').addClass('disabled');
			
			} else { 

				$('#setup_wizard .continue').removeClass('disabled');
			}

		}

		$('body').on('change', '#setup_wizard .tab-pane.active input[type!=file], #setup_wizard .tab-pane.active select', function() {

			validateSetupWizard();

		});

		$('#setup_wizard .button-previous').click(function() {
			setTimeout(function() {
	            validateSetupWizard();
	        }, 200);
	    });



		$('#setup_wizard #country').change(function() {

			var country_code = $(this).find('option:selected').val();

			$('#setup_wizard #timezone').html('<option>Loading...</option>');

			$.ajax({

		        url: 'ajax_functions',  //server script to process data
		        type: 'POST',
		        data: {
		        	action: "load_timezone_select",
		        	country_code: country_code
		        }

		    }).done(function(data) {

		    	$('#setup_wizard #timezone').html(data);
		    });

		});


		$('#setup_wizard #profile_picture').fileupload({
	        url: 'ajax_file_upload',
	        dataType: 'json',
	        autoUpload: true,
	        paramName: 'files[]',
	        acceptFileTypes: /(\.|\/)(jpe?g|png)$/i,
		    maxFileSize: 10000000, // 10 MB
		    imageMaxWidth: 800,
		    imageMaxHeight: 800,
	        formData: {
	        	action: 'upload_profile_picture'
	        },
	        start: function(e) {

	        	$('#profile_picture_preview_wrapper').hide();
	        	$('#profile_picture_preview').hide();
	        	$('#setup_wizard .profile_picture_upload .fileinput-button').hide();

	        	$('#profile_picture_progress .bar').css('width', '0%');
	        	$('#profile_picture_progress').show();

	        },
	        progressall: function (e, data) { 

	            var progress = parseInt(data.loaded / data.total * 100, 10);

	            $('#profile_picture_progress .bar').css('width', progress + '%');
	        },
	        done: function(e, data) {

	        	$.ajax({

			        url: 'ajax_functions',  //server script to process data
			        type: 'POST',
			        data: {
			        	action: "get_profile_thumbnail_url"
			        }

			    }).done(function(data) {

					if(data) {

						$('#profile_picture_preview').attr('src', data);
			    		
			    		setTimeout(function() {

			    			$('#profile_picture_progress').hide();

			    			$('#profile_picture_preview_wrapper').slideDown('fast', function() {

			    				$('#profile_picture_preview').fadeIn('fast');
			    			});

			    			$('#setup_wizard .profile_picture_upload .fileinput-button span').text('Change...');
			    			$('#setup_wizard .profile_picture_upload .fileinput-button i').remove();
			    			$('#setup_wizard .profile_picture_upload .fileinput-button').removeClass('light-green').show();

			    		}, 1000);

					}

				});

	        }

	    });


		$('#setup_wizard #logo').fileupload({
	        url: 'ajax_file_upload',
	        dataType: 'json',
	        autoUpload: true,
	        paramName: 'files[]',
	        acceptFileTypes: /(\.|\/)(jpe?g|png)$/i,
		    maxFileSize: 10000000, // 10 MB
		    imageMaxWidth: 800,
		    imageMaxHeight: 800,
	        formData: {
	        	action: 'upload_logo'
	        },
	        start: function(e) {

	        	$('#logo_preview_wrapper').hide();
	        	$('#logo_preview').hide();
	        	$('#setup_wizard .logo_upload .fileinput-button').hide();

	        	$('#logo_progress .bar').css('width', '0%');
	        	$('#logo_progress').show();

	        },
	        progressall: function (e, data) { 

	            var progress = parseInt(data.loaded / data.total * 100, 10);

	            $('#logo_progress .bar').css('width', progress + '%');
	        },
	        done: function(e, data) {

	        	$.ajax({

			        url: 'ajax_functions',  //server script to process data
			        type: 'POST',
			        data: {
			        	action: "get_logo_url"
			        }

			    }).done(function(data) {

					if(data) {

						$('#logo_preview').attr('src', data);
			    		
			    		setTimeout(function() {

			    			$('#logo_progress').hide();

			    			$('#logo_preview').slideDown('fast', function() {

			    				$('#logo_preview_wrapper').fadeIn('fast');
			    			});

			    			$('#setup_wizard .logo_upload .fileinput-button span').text('Change...');
			    			$('#setup_wizard .logo_upload .fileinput-button i').remove();
			    			$('#setup_wizard .logo_upload .fileinput-button').removeClass('light-green').show();

			    		}, 1000);

					}

				});

	        }

	    });


		$('#setup_wizard #users_label').change(function() {

			var label = $(this).find('option:selected').val();
				label = label.replace('_', ' ');

			$('#setup_wizard #users_label_invite').text(label);
		});


		$('#setup_wizard .submit').click(function() {

			$('#setup_wizard_modal .loading_modal').fadeIn('fast');

			var first_name = $('#first_name').val();
		    var last_name = $('#last_name').val();
		    var gender = $('#gender option:selected').val();
		    var country = $('#country option:selected').val();
		    var timezone = $('#timezone option:selected').val();
		    var business_name = $('#business_name').val();
		    var business_type = $('#business_type option:selected').val();
		    var year_founded = $('#year_founded option:selected').val();
		    var users_label = $('#users_label option:selected').val();
		    var first_name_1 = $('#inv_first_name_1').val();
		    var last_name_1 = $('#inv_last_name_1').val();
		    var email_1 = $('#inv_email_1').val();
		    var first_name_2 = $('#inv_first_name_2').val();
		    var last_name_2 = $('#inv_last_name_2').val();
		    var email_2 = $('#inv_email_2').val();
		    var first_name_3 = $('#inv_first_name_3').val();
		    var last_name_3 = $('#inv_last_name_3').val();
		    var email_3 = $('#inv_email_3').val();

		    $.ajax({

		        url: 'ajax_functions',  //server script to process data
		        type: 'POST',
		        data: {
		        	action: "save_setup_wizard",
		        	first_name: first_name,
		        	last_name: last_name,
		        	gender: gender,
		        	country: country,
		        	timezone: timezone,
		        	business_name: business_name,
		        	business_type: business_type,
		        	year_founded: year_founded,
		        	users_label: users_label,
		        	first_name_1: first_name_1,
		        	first_name_2: first_name_2,
		        	first_name_3: first_name_3,
		        	last_name_1: last_name_1,
		        	last_name_2: last_name_2,
		        	last_name_3:last_name_3,
		        	email_1: email_1,
		        	email_2: email_2,
		        	email_3: email_3
		        }

		    }).done(function(data) { 

		    	window.location.href = 'dashboard';

		    });

		});

	}


	if(page == "join") {

		var a_id = $('#a_id').val();
		var u_id = $('#u_id').val();


		function validateJoin() {

			var error = 0;

			$('#join input[type!=file]').each(function() {

				if($(this).val() == '') {

					error++;
				}
			});	

			$('#join select').each(function() {

				if($(this).find('option:selected').val() == '' || $(this).find('option:selected').val() == '-1') {

					error++;

				}

			});

			if($('#join #username').hasClass('error')) {

				error++;
			
			}

			if(error > 0) {

				$('#join .submit').addClass('disabled');
			
			} else { 

				$('#join .submit').removeClass('disabled');
			}

		}


		$('body').on('change', '#join input[type!=file], #join select', function() {

			validateJoin();

		});



		$('#join #username').change(function() {

			var account_id = $('#join #a_id').val();
			var user_id = $('#join #u_id').val();
			var username = $(this).val();

			$.ajax({

		        url: 'ajax_functions',  //server script to process data
		        type: 'POST',
		        data: {
		        	action: "check_username_within_account",
		        	account_id: account_id,
		        	user_id: user_id,
		        	username: username
		        }

		    }).done(function(data) {

		    	if(data == 'taken') {

		    		$('#join #username').addClass('error');

		    	} else {

		    		$('#join #username').removeClass('error');
		    	}

		    	validateJoin();
		    });

		});



		$('#join #country').change(function() {

			var country_code = $(this).find('option:selected').val();

			$('#join #timezone').html('<option>Loading...</option>');

			$.ajax({

		        url: 'ajax_functions',  //server script to process data
		        type: 'POST',
		        data: {
		        	action: "load_timezone_select",
		        	country_code: country_code
		        }

		    }).done(function(data) {

		    	$('#join #timezone').html(data);
		    });

		});


		$('#join #profile_picture').fileupload({
	        url: 'ajax_file_upload',
	        dataType: 'json',
	        autoUpload: true,
	        paramName: 'files[]',
	        acceptFileTypes: /(\.|\/)(jpe?g|png)$/i,
		    maxFileSize: 10000000, // 10 MB
		    imageMaxWidth: 800,
		    imageMaxHeight: 800,
	        formData: {
	        	action: 'upload_profile_picture'
	        },
	        start: function(e) {

	        	$('#profile_picture_preview_wrapper').hide();
	        	$('#profile_picture_preview').hide();
	        	$('#join .profile_picture_upload .fileinput-button').hide();

	        	$('#profile_picture_progress .bar').css('width', '0%');
	        	$('#profile_picture_progress').show();

	        },
	        progressall: function (e, data) { 

	            var progress = parseInt(data.loaded / data.total * 100, 10);

	            $('#profile_picture_progress .bar').css('width', progress + '%');
	        },
	        done: function(e, data) {

	        	$.ajax({

			        url: 'ajax_functions',  //server script to process data
			        type: 'POST',
			        data: {
			        	action: "get_new_user_profile_thumbnail",
			        	account_id: a_id,
			        	user_id: u_id
			        }

			    }).done(function(data) {

					if(data) {

						$('#profile_picture_preview').attr('src', data);
			    		
			    		setTimeout(function() {

			    			$('#profile_picture_progress').hide();

			    			$('#profile_picture_preview_wrapper').slideDown('fast', function() {

			    				$('#profile_picture_preview').fadeIn('fast');
			    			});

			    			$('#join .profile_picture_upload .fileinput-button span').text('Change...');
			    			$('#join .profile_picture_upload .fileinput-button i').remove();
			    			$('#join .profile_picture_upload .fileinput-button').removeClass('light-green').show();

			    		}, 1000);

					}

				});

	        }

	    });


		$('#join .submit').click(function() {

			if(!$(this).hasClass('disabled')) {

				$('#setup_wizard_modal .loading_modal').fadeIn('fast');

				var first_name = $('#first_name').val();
			    var last_name = $('#last_name').val();
			    var username = $('#username').val();
			    var password = $('#password').val();
			    var gender = $('#gender option:selected').val();
			    var country = $('#country option:selected').val();
			    var timezone = $('#timezone option:selected').val();
			    var a_id = $('#a_id').val();
			    var u_id = $('#u_id').val();
			    var email = $('#email').val();
			    var token = $('#token').val();

			    $.ajax({

			        url: 'ajax_functions',  //server script to process data
			        type: 'POST',
			        data: {
			        	action: "add_invited_user",
			        	first_name: first_name,
			        	last_name: last_name,
			        	username: username,
			        	password: password,
			        	gender: gender,
			        	country: country,
			        	timezone: timezone,
			        	a_id: a_id,
			        	u_id: u_id,
			        	email: email,
			        	token: token
			        }

			    }).done(function(data) {

			    	if(data == 'success') { 

			    		window.location.href = 'login';
			    	
			    	} else {

			    		$('#setup_wizard_modal .loading_modal').fadeOut('fast');
			    	}

			    });

			} else {

				return false;
			}

		});

	}


	if(page == "view_invoice") {


		$('.save_invoice').click(function() {

			var href = $('.invoice_preview').attr('src');
			//var id = $(this).attr('id');
			var id = 1001;

			$('body').append('<iframe class="downloader" style="position:absolute; left:-9999999px;" src="' + href + '" id="' + id + '"></iframe>');

			setTimeout(function() {

				$('iframe#' + id).remove();

			}, 10000);

			return false;
		});

	}


	if(page == "fileshare") {

		var images = ["img/bg/blurry_dock_small.jpg",
	            "img/bg/blurry_dock.jpg",
	            "img/bg/sunset.jpg",
	            "img/bg/rocks.jpg",
	            "img/bg/gudvagen.jpg",
	            "img/bg/canoe.jpg",
	            "img/bg/sunset_boat.jpg",
	            "img/bg/palmtrees.jpg",
	            "img/bg/snail.jpg",
	            "img/bg/underwater.jpg",
	            "img/bg/beach.jpg"];


	    images = shuffle(images);

	    $.backstretch(images, {
		          fade: 2000,
		          duration: 8000
		    });


		$('.file_download').click(function() {

			var href = $(this).attr('href');
			var id = $(this).attr('id');

			$('body').append('<iframe class="downloader" style="position:absolute; left:-9999999px;" src="' + href + '" id="' + id + '"></iframe>');

			setTimeout(function() {

				$('iframe#' + id).remove();

			}, 10000);

			return false;
		});

	}




	if(page == "lock") {

		if(!$_GET['e']) {

			logoff_sound = new Audio();

			if(logoff_sound.canPlayType('audio/mpeg'))

				logoff_sound.src = 'audio/logoff.mp3';

			else if(logoff_sound.canPlayType('audio/ogg'))

				logoff_sound.src = 'audio/logoff.ogg';

			else

				logoff_sound.src = null;


			logoff_sound.play();
		}

	}


	if(page == "paypal") {

		setTimeout(function() {

			$('#paypal').submit();

		}, 1200);

	}


	
	var count = 1;

	$('#add_new').click(function() {
		if ($('table.table').hasClass('clients')) {
			var check_for_new = setInterval(function() {

				if($('#data_table td.sorting_1')) {

					clearInterval(check_for_new);
					var index = $('#data_table td.sorting_1').index() + 1;

					//var last_row = $('#data_table tbody tr:last-child').html(); alert(last_row);
					//$('#data_table tr:last-child').remove();
					//$('#data_table tbody').prepend(last_row);
					for(x = 1; x <= count; x++) {

						$('#data_table td.sorting_1').removeClass("sorting_1");
						$('#data_table tr:nth-child(' + x + ') td:nth-child(1)').attr("data-title", "Username");
						$('#data_table tr:nth-child(' + x + ') td:nth-child(2)').attr("data-title", "Full Name");
						$('#data_table tr:nth-child(' + x + ') td:nth-child(3)').attr("data-title", "Points");
						$('#data_table tr:nth-child(' + x + ') td:nth-child(4)').attr("data-title", "Notes");
						$('#data_table tr:nth-child(' + x + ') td:nth-child(5)').attr("data-title", "Edit");
						$('#data_table tr:nth-child(' + x + ') td:nth-child(6)').attr("data-title", "Delete");

					}

				}
			}, 10);

		} 
		else if($('table.table').hasClass('inventory')) {
		
				var check_for_new = setInterval(function() {

				if($('#data_table td.sorting_1')) {

					clearInterval(check_for_new);
					var index = $('#data_table td.sorting_1').index() + 1;

					//var last_row = $('#data_table tbody tr:last-child').html(); alert(last_row);
					//$('#data_table tr:last-child').remove();
					//$('#data_table tbody').prepend(last_row);
					for(x = 1; x <= count; x++) {

						$('#data_table td.sorting_1').removeClass("sorting_1");
						$('#data_table tr:nth-child(' + x + ') td:nth-child(1)').attr("data-title", "Product Id");
						$('#data_table tr:nth-child(' + x + ') td:nth-child(2)').attr("data-title", "Product name");
						$('#data_table tr:nth-child(' + x + ') td:nth-child(3)').attr("data-title", "Quantity");
						$('#data_table tr:nth-child(' + x + ') td:nth-child(4)').attr("data-title", "Price");
						$('#data_table tr:nth-child(' + x + ') td:nth-child(5)').attr("data-title", "View Product");
						$('#data_table tr:nth-child(' + x + ') td:nth-child(6)').attr("data-title", "Delete");

					}

				}
			}, 10);
		
		
		}
		
		count++;
	});


	//$('.save_as_pdf').click(function() {
	$('body').on('click', '.save_as_pdf', function() {

		if($('table.saveaspdf').length > 0) {

			$('.btn-group.open').removeClass("open");

			$('.loading_large').fadeIn();

			var html = $('table.saveaspdf').clone().wrap('<p>').parent().html();
			//html += $(this).closest('.data_table_wrapper .dataTables_info').html();

			
			$(html).find('a').replaceWith(function() {
			    return $.text([this]);
			});

			$.ajax({

		        url: 'wkhtmltopdf',  //server script to process data
		        type: 'POST',
		        data: {
		        	action: "convert",
		        	html: html,
		        	page: page //used on the whtmltopdf.php file to put heading depending on data
		        }

		    }).done(function(data) {

				if(data) {

					var url = data;

					window.open(url, "_blank");
				}


				$('.loading_large').fadeOut('fast');

			});

		} else if($('div.saveaspdf').length > 0) {

			$('.btn-group.open').removeClass("open");

			$('.loading_large').fadeIn();

			var html = $('div.saveaspdf').clone().wrap('<p>').parent().html();
			//html += $(this).closest('.data_table_wrapper .dataTables_info').html();

			
			$(html).find('a').replaceWith(function() {
			    return $.text([this]);
			});

			$.ajax({

		        url: 'wkhtmltopdf',  //server script to process data
		        type: 'POST',
		        data: {
		        	action: "convert",
		        	html: html,
		        	page: page //used on the whtmltopdf.php file to put heading depending on data
		        }

		    }).done(function(data) {

				if(data) {

					var url = data;

					window.open(url, "_blank");
				}


				$('.loading_large').fadeOut('fast');

			});

		}

		return false;

	});



	if(page =="dashboard") {

		$('.announcements').height((height/2) - 136);
		$('.chat .slimScrollDiv').height((height/2) - 137);
		$('.activity_timeline').height(height - 153);

		$('.dashboard_stats .portlet-body').css('min-height', height - 153 + 'px');

		$('body').on("click", ".portlet .portlet-title.expand", function() {

			$('.portlet .collapse').closest(".portlet").children(".portlet-body").slideUp(300);
			$('.portlet .collapse').removeClass("collapse").addClass("expand");

			$(this).closest(".portlet").children(".portlet-body").delay(50).slideDown(300);
			$(this).delay(100).removeClass("expand").addClass("collapse");

		});

		

		$('.popout').click(function() {

			chat_popup = window.open('open_chat','Open Chat','width=760,height=560');
			chat_popup.focus();
		});


		$('.show_new_announcement_form').click(function() {

			$('.new_announcement').slideDown('fast');
		});


		$('.post_announcement').click(function() {

			var title = $('.new_announcement .announcement_title').val();
			var message = $('.new_announcement .announcement_text').val();
			var priority = $('.new_announcement .announcement_priority').val();


			$.ajax({

		        url: 'ajax_functions',  //server script to process data
		        type: 'POST',
		        data: {
		        	action: "post_announcement",
		        	title: title,
		        	message: message,
		        	priority: priority
		        }

		    }).done(function(data) {

				if(data) {

					$('.new_announcement').slideUp('fast');
					$('.new_announcement .announcement_title').val('');
					$('.new_announcement .announcement_text').val('');
					$('.new_announcement .announcement_priority').val('');

					$('.announcements ul li:first').after(data); //append after first element, which is the new announcement li

					$('.announcements ul li:hidden').slideDown();
				}

			});

			return false;

		});


		$('body').on("click", ".load_earlier_announcements", function() {

			load_earlier_announcements();

			return false;

		});

		loading_announcements = false;
		no_further_announcements = false;

		$('.announcements').scroll(function () {

			if(($('.announcements').scrollTop() + $('.announcements').height() > $('.announcements .timeline').height() - 100) && (loading_announcements == false) && (no_further_announcements == false)) {

				loading_announcements = true;
				load_earlier_announcements();

			}

		});


		function load_earlier_announcements() {

			$('.load_earlier_announcements').text('').addClass('loading_inline');

			var last_retrieved_id = $('.load_earlier_announcements').attr('id');

			$.ajax({

		        url: 'ajax_functions',  //server script to process data
		        type: 'POST',
		        data: {
		        	action: "load_earlier_announcements",
		        	last_retrieved_id: last_retrieved_id
		        }

		    }).done(function(data) {

				if(data) {

					loading_announcements = false;

					$('.load_earlier_announcements').closest('li').remove();
					$('.announcements ul').append(data);

				} else {

					$('.load_earlier_announcements').closest('li').remove();
					$('.announcements ul').append('<li class="center faded">No further announcements</li>');
					no_further_announcements = true;
				}

			});

		}




		$('body').on("click", ".load_earlier_activities", function() {

			load_earlier_activities();

			return false;

		});

		loading_activities = false;
		no_further_activities = false;

		$('.activity_timeline').scroll(function () {

			if(($('.activity_timeline').scrollTop() + $('.activity_timeline').height() > $('.activity_timeline .timeline').height() - 100) && (loading_activities == false) && (no_further_activities == false)) {

				loading_activities = true;
				load_earlier_activities();

			}

		});


		function load_earlier_activities() {

			$('.load_earlier_activities').text('').addClass('loading_inline');

			var last_retrieved_id = $('.load_earlier_activities').attr('id');

			$.ajax({

		        url: 'ajax_functions',  //server script to process data
		        type: 'POST',
		        data: {
		        	action: "load_earlier_activities",
		        	last_retrieved_id: last_retrieved_id
		        }

		    }).done(function(data) {

				if(data) {

					loading_activities = false;

					$('.load_earlier_activities').closest('li').remove();
					$('.activity_timeline ul').append(data);

				} else {

					$('.load_earlier_activities').closest('li').remove();
					$('.activity_timeline ul').append('<li class="center faded">No further activities</li>');
					no_further_activities = true;
				}

			});

		}



		$('body').on("click", ".load_earlier_messages", function() {

			load_earlier_messages();

			return false;

		});

		loading_messages = false;
		no_further_messages = false;

		$('#chats .scroller').scroll(function () { 

			//if(($('#chats .scroller').scrollTop() + $('#chats .scroller').height() > $('#chats .scroller .chats').height() - 100) && (loading_messages == false) && (no_further_messages == false)) {
			if(($('#chats .scroller').scrollTop() < 100) && (loading_messages == false) && (no_further_messages == false)) {

				loading_messages = true;
				load_earlier_messages();

			}

		});


		function load_earlier_messages() {

			$('.load_earlier_messages').text('').addClass('loading_inline');

			var last_retrieved_id = $('.load_earlier_messages').attr('id');

			$.ajax({

		        url: 'ajax_functions',  //server script to process data
		        type: 'POST',
		        data: {
		        	action: "load_earlier_messages",
		        	last_retrieved_id: last_retrieved_id
		        }

		    }).done(function(data) {

				if(data) {

					loading_messages = false;

					$('.load_earlier_messages').closest('li').remove();
					$('#chats ul').prepend(data);

				} else {

					$('.load_earlier_messages').closest('li').remove();
					$('#chats ul').prepend('<li class="center faded">No further messages</li>');
					no_further_messages = true;
				}

			});

		}


		// $.ajax({

	 //        url: 'ajax_functions',  //server script to process data
	 //        type: 'POST',
	 //        data: {
	 //        	action: "load_dashboard_counters"
	 //        },
	 //        dataType: 'json'

	 //    }).done(function(data) {

		// 	if(data) {

		// 		$('.unread_messages_count').text(data.unread_messages);
		// 		$('.events_count_today').text(data.events);
		// 		$('.tasks_count_today').text(data.tasks);
		// 		$('.invoices_processed_today').text(data.invoices_today);
		// 		$('.invoices_processed_lifetime').text(data.invoices_lifetime);
		// 		$('.inventory_items').text(data.inventory_items);
		// 		$('.open_invoices').text(data.open_invoices);
		// 		$('.overdue_invoices').text(data.overdue_invoices);
		// 		$('.total_clients').text(data.clients);
		// 	}

		// });



		// $.ajax({

	 //        url: 'ajax_functions',  //server script to process data
	 //        type: 'POST',
	 //        data: {
	 //        	action: "load_income_expenses_graph"
	 //        }

	 //    }).done(function(data) {

		// 	if(data) {


		// 	}

		// });




		// $('body').on("fadeInComplete", function(){


		// 	var start_date = '';
			
		// 	//Chart
		// 	var d1 = [];
	 //        for (var i = 0; i <= 10; i += 1)
	 //            d1.push([i, parseInt(Math.random() * 30)]);

	 //        var d2 = [];
	 //        for (var i = 0; i <= 10; i += 1)
	 //            d2.push([i, parseInt(Math.random() * 30)]);

	 //        var d3 = [];
	 //        for (var i = 0; i <= 10; i += 1)
	 //            d3.push([i, parseInt(Math.random() * 30)]);


	 //        function plotWithOptions() {
	 //            $.plot($(".income_expenses_chart"), 

	 //                [{
	 //                    label: "Income",
	 //                    data: d1,
	 //                    shadowSize: 0
	 //                }, {
	 //                    label: "Expenses",
	 //                    data: d2,
	 //                    shadowSize: 0
	 //                }],

	 //                {
	 //                    series: {
	 //                        bars: {
	 //                            show: true,
	 //                            barWidth: 0.25,
	 //                            lineWidth: 0, // in pixels
	 //                            shadowSize: 0,
	 //                            align: 'center'
	 //                        }
	 //                    },
	 //                    grid: {
	 //                    	hoverable: true,
	 //                        tickColor: "#eee",
	 //                        borderColor: "#eee",
	 //                        borderWidth: 1
	 //                    },

	 //                    xaxis: { 
	 //                    	mode: "time",
	 //                    	ticks: [[0, 'January'], [1, 'February'], [2, 'March'], [3, 'April'], [4, 'May'], [5, 'June'], [6, 'July'], [7, 'August'], [8, 'September'], [9, 'October'], [10, 'November'], [11, 'December']]
	 //                    },

	 //                    yaxis: {            
		// 				    tickFormatter: function (v, axis) {
		// 				        return "$" + v;
		// 				    }
		// 				},

		// 				colors: ["#4b8df8", "#E44F3E"]
	 //            });



		// 		function showTooltip(x, y, contents) {

	 //                $('<div id="tooltip">' + contents + '</div>').css({
	 //                    position: 'absolute',
	 //                    display: 'none',
	 //                    top: y + 5,
	 //                    left: x + 15,
	 //                    border: '1px solid #333',
	 //                    padding: '4px',
	 //                    color: '#fff',
	 //                    'border-radius': '3px',
	 //                    'background-color': '#333',
	 //                    opacity: 0.80
	 //                }).appendTo("body").fadeIn(200);
	 //            }


	 //            var previousPoint = null;
	 //            $(".income_expenses_chart").bind("plothover", function (event, pos, item) {

	 //                if(item) {
	 //                    if (previousPoint != item.dataIndex) {
	 //                        previousPoint = item.dataIndex;

	 //                        $("#tooltip").remove();

	 //                        var x = item.datapoint[0],
	 //                            y = item.datapoint[1];

	 //                        showTooltip(item.pageX, item.pageY, item.series.label + " of " + x + " = $" + y);
	 //                    }
	 //                } else {
	 //                    $("#tooltip").remove();
	 //                    previousPoint = null;
	 //                }
	 //            });
	 //        }   


	 //        plotWithOptions();

		// });

	}



	if(page == "open_chat") {

		$('body').on("click", ".load_earlier_messages", function() {

			load_earlier_messages();

			return false;

		});

		loading_messages = false;
		no_further_messages = false;

		$('#chats .scroller').scroll(function () { 

			//if(($('#chats .scroller').scrollTop() + $('#chats .scroller').height() > $('#chats .scroller .chats').height() - 100) && (loading_messages == false) && (no_further_messages == false)) {
			if(($('#chats .scroller').scrollTop() < 100) && (loading_messages == false) && (no_further_messages == false)) {

				loading_messages = true;
				load_earlier_messages();

			}

		});


		function load_earlier_messages() {

			$('.load_earlier_messages').text('').addClass('loading_inline');

			var last_retrieved_id = $('.load_earlier_messages').attr('id');

			$.ajax({

		        url: 'ajax_functions',  //server script to process data
		        type: 'POST',
		        data: {
		        	action: "load_earlier_messages",
		        	last_retrieved_id: last_retrieved_id
		        }

		    }).done(function(data) {

				if(data) {

					loading_messages = false;

					$('.load_earlier_messages').closest('li').remove();
					$('#chats ul').prepend(data);

				} else {

					$('.load_earlier_messages').closest('li').remove();
					$('#chats ul').prepend('<li class="center faded">No further messages</li>');
					no_further_messages = true;
				}

			});

		}
		
	}



	if(page == "invoices") {


		$('body').on("click", ".invoices tr td:not(:nth-child(8))", function() {

			var id = $(this).closest("tr").attr("id");

			window.location.href = "invoice?id=" + id;
		});


		$('#invoice_type').change(function() {

			window.location.href = "recurring_invoices";
		});
	}



	if(page == "recurring_invoices") {


		$('body').on("click", ".invoices tr td:not(:nth-child(7))", function() {

			var id = $(this).closest("tr").attr("id");

			window.location.href = "invoice?id=" + id;
		});


		$('#invoice_type').change(function() {

			window.location.href = "invoices";
		});
	}




	if(page == "invoice") {


		// ==================================
		// File upload


		var file_count = 0;
		var files = new Array();
		var invoice_id = $('#upload_invoice_files_modal #invoice_id').val();

		$('.file_upload #file').fileupload({
	        url: 'ajax_file_upload',
	        dataType: 'json',
	        autoUpload: true,
	        paramName: 'files[]',
		    maxFileSize: 100000000, // 100 MB
		    limitConcurrentUploads: 5,
	        formData: {
	        	action: 'upload_invoice_file',
	        	invoice_id: invoice_id
	        },
	        add: function(e, data) {

	        	data.files[0].id = "file_" + file_count;

	        	files.push(data);

	        	var filename = data.files[0].name;
	        	var filesize = bytesToSize(data.files[0].size);

	        	$('#upload_invoice_files_modal .added_files').show();

	        	$('#upload_invoice_files_modal .added_files tbody').append('<tr><td>' + filename + '</td><td>' + filesize + '</td><td><div id="' + data.files[0].id + '" class="file_progress progress progress_thin progress-success progress-striped"><div class="bar"></div></div></td></tr>');

	            file_count++;

	            if (data.autoUpload || (data.autoUpload !== false &&
			            $(this).fileupload('option', 'autoUpload'))) {
			        data.process().done(function () {
			            data.submit();
			        });
			    }

			    $('.cancel_uploads').removeClass('disabled');

	        },
	        start: function(e) {

	   //      	$('.profile_picture .profile_picture_upload .fileinput-button').fadeOut();

				// $('.current_photo_wrapper').slideUp();

	   //      	$('#profile_picture_progress .bar').css('width', '0%');
	   //      	$('#profile_picture_progress').slideDown();

	        },
	        progress: function (e, data) {

	            var progress = parseInt(data.loaded / data.total * 100, 10);
	            var file_id = data.files[0].id;

	            if(progress >= 100) {

	            	setTimeout(function() {

	            		$('.added_files #' + file_id).closest('td').html('Complete');
	            	}, 750);

	            } else {
	            	
	            	$('.added_files #' + file_id + ' .bar').css('width', progress + '%');
	            }
	        },
	        done: function(e, data) {

	        	$('.cancel_uploads').addClass('disabled');

	        	//reload the client files on the page
	        	$.ajax({

			        url: 'ajax_functions',  //server script to process data
			        type: 'POST',
			        data: {
			        	action: "reload_invoice_files",
			        	invoice_id: invoice_id
			        }

			    }).done(function(data) {

					if(data) {

						$('.attachments .attachment:not(:first)').remove();
						$('.attachments .clearfix').remove();
						$('.attachments .box-message').remove();

						$('.attachments').append(data);
						$('.attachments').append('<div class="clearfix"></div>');
					}

				});

	        }

	    });


		$('.cancel_uploads').click(function() {

			for (i = 0; i < files.length; i++) { console.log(files);

				var file_id = files[i].files[0].id;

				$('.added_files #' + file_id).closest('td').html('Cancelled');

				files[i].abort(); 
			}

			$('.cancel_uploads').addClass('disabled');

			return false;
		});


		$('body').on('click', '.attachment .delete_file', function() {

			var file_id = $(this).closest('.attachment').attr('data-filename');

			$('#delete_invoice_file_modal #file_id').val(file_id);

			$('#delete_invoice_file_modal .modal-body').html('Are you sure you want to delete ' + file_id + '?<br><br><i class="icon-warning-sign"></i> This action is permanent and cannot be undone.');


		});


		$('body').on('click', '#delete_invoice_file_modal .delete', function() {

			$('#delete_invoice_file_modal').modal('hide');

			var filename = $('#delete_invoice_file_modal #file_id').val();
			var invoice_id = $('#delete_invoice_file_modal #invoice_id').val();

			var attachment = $(".invoice_files .attachment[data-filename='" + filename + "']");
			
			$(attachment).animate({

				opacity: 0.5,
				duration: 300
			});

			$.ajax({

				type: "POST",
				url: "ajax_functions",
				data: { 
					action: "delete_invoice_file",
					filename: filename,
					invoice_id: invoice_id
				}

			}).done(function(result) {	

				$(attachment).fadeOut(function() {

					$(attachment).remove();
				});
			});
			
			return false;
		});

		// ==================================


	}



	if(page == "new_invoice" || page == "new_recurring_invoice") { //Handle both new_invoice and new_recurring_invoice... 2 subsequent IF statements handle any extra functionality for new_recurring_invoice.


		var items = new Array();
		var discount_amount = $('#cur_discount_amount').val(); //must be global
       	var discount_type = $('#cur_discount_type').val(); //must be global

		var autosaved = true; //boolean to determine if autosaved or manually saved.
		var saved = false; //boolean to determine if changes have been saved at all.



		if(page == "new_recurring_invoice") {


			var recurring_interval = 'monthly';


		    $('#recurring_interval').change(function() {

		    	var interval = $(this).find('option:selected').val();
		    	recurring_interval = interval;

		    	$('.recurring_wrapper .interval').hide();
		    	
		    	$('.recurring_' + interval).show();
		    });


		    $('#recurring_date_range').change(function() {

		    	var range = $(this).find('option:selected').val();

		    	if(range == 'none') {

		    		$('.recurring_end').hide();
		    		$('.recurring_occurrences').hide();
		    	
		    	} else if(range == 'by') {

		    		$('.recurring_end').show();
		    		$('.recurring_occurrences').hide();
		    	
		    	} else if(range == 'after') {

		    		$('.recurring_end').hide();
		    		$('.recurring_occurrences').show();
		    	}
		    });

		}




		$('.select_product').each(function() {

        	calculateItemTotal($(this));
        });
		calculateInvoiceTotal();

		$('select.search_dropdown').select2({
            placeholder: "Select..."
        });


		$(".datepicker").datetimepicker({
        	format: 'F j, Y',
            closeOnDateSelect: true,
            timepicker: false,
            validateOnBlur: false
        });



		initGeoAddress();

		$('#add_client_geo_address').keydown(function(event){

	    	if(event.keyCode == 13) {

	      		event.preventDefault();
	      		return false;
	    	}
	  	});

		//Run twice -- once at page load, and once of client_type dropdown change
		var type = $('#add_new_client_modal #client_type').find('option:selected').val();

		if(type == "individual") {
			$('#add_new_client_modal .row-fluid.business').fadeOut('fast', function() {
				$('#add_new_client_modal .row-fluid.individual').fadeIn('fast');
			});
		} else if(type == "business") {
			$('#add_new_client_modal .row-fluid.individual').fadeOut('fast', function() {
				$('#add_new_client_modal .row-fluid.business').fadeIn('fast');
			});
		}


		$('#add_new_client_modal #client_type').change(function() {
			var type = $(this).find('option:selected').val();

			if(type == "individual") {
				$('#add_new_client_modal .row-fluid.business').fadeOut('fast', function() {
					$('#add_new_client_modal .row-fluid.individual').fadeIn('fast');
				});
			} else if(type == "business") {
				$('#add_new_client_modal .row-fluid.individual').fadeOut('fast', function() {
					$('#add_new_client_modal .row-fluid.business').fadeIn('fast');
				});
			}
		});

		

        if($('#select_client').val() != "") {

        	loadClientInfo(); //used when creating an invoice from the client's profile page
        }

        $('#select_client').change(function() {

        	if($('#select_client').val() != "") {

	        	loadClientInfo();
	        }
        });


        function loadClientInfo() {

        	var client_id = $('#select_client').val();

        	$('.invoice_client_details').fadeOut('fast');

        	$.ajax({

		        url: 'ajax_functions',  //server script to process data
		        type: 'POST',
		        data: {
		        	action: "create_invoice_load_client_info",
		        	client_id: client_id
		        }

		    }).done(function(data) {;

		    	$('.invoice_client_details').html(data);
		    	$('.invoice_client_details').fadeIn('fast');
		    });
        }



        $('#add_new_client_modal form').submit(function() {

			$('#add_new_client_modal #submit').trigger('click');

			return false;
		});

		$('#add_new_client_modal #submit').click(function() {

			addClient();

			 return false;
		});


		function addClient() {

			$('#add_new_client_modal').modal('hide');

			$('.loading_large').fadeIn('fast');

			var client_type = $('#add_new_client_modal #client_type option:selected').val();
			var first_name = $('#add_new_client_modal #first_name').val();
			var last_name = $('#add_new_client_modal #last_name').val();
			var business_name = $('#add_new_client_modal #business_name').val();
			var individual_email = $('#add_new_client_modal #individual_email').val();
			var business_email = $('#add_new_client_modal #business_email').val();
			var gender = $('#add_new_client_modal #gender option:selected').val();
			var primary_phone = $('#add_new_client_modal #primary_phone').val();
			var alternative_phone = $('#add_new_client_modal #alternative_phone').val();
			var address = $('#add_new_client_modal #add_client_geo_address').val();

			$.ajax({

				type: "POST",
				url: "ajax_functions",
				data: { 
					action: "add_client",
					client_type: client_type,
					first_name: first_name,
					last_name: last_name,
					business_name: business_name,
					individual_email: individual_email,
					business_email: business_email,
					gender: gender,
					primary_phone: primary_phone,
					alternative_phone: alternative_phone,
					address: address,
					page: page //used to determine what to return since function is called from both clients and invoice page
				}

			}).done(function(data) {

				$('.loading_large').fadeOut('fast');

				if(data) {

					$('#select_client').html('<option value=""></option>' + data);

					$("#select_client").select2("destroy");
					$("#select_client").select2();

					loadClientInfo();

					autosaved = true;
					saveDraft();

				}

			});

		}



        $('#payment_term').change(function() {

        	var months = new Array("January", "February", "March", 
			"April", "May", "June", "July", "August", "September", 
			"October", "November", "December");


        	var term = $(this).find('option:selected').val();

        	term = parseInt(term);


        	if(term == 0) {

        		$('#due_date').val($('#invoice_date').val());

        	} else {

        		var invoice_date = new Date();

        		invoice_date = Date.parse($('#invoice_date').val());

	        	var due_date = new Date();

	        	due_date.setDate(invoice_date.getDate() + term);

	        	$('#due_date').val(months[due_date.getMonth()] + ' ' + due_date.getDate() + ', ' + due_date.getFullYear());

	        }

        });


        $('.select_product').on("change", function() {

        	var item_desc = $(this).find("option:selected").attr("desc");
        	var item_cost = fixDecimal(parseFloat($(this).find("option:selected").attr("cost")));

        	$(this).closest("tr").find("td:nth-child(3)").html(item_desc);
        	$(this).closest("tr").find("td:nth-child(5) input").val(formatMoney(item_cost));

        	calculateItemTotal($(this));
        });


        $('.item_qty .quantity').on("change", function() {

        	var item_cost = parseFloat($(this).closest("tr").find(".item_cost input").val());

			if(item_cost > 0) { 	
				calculateItemTotal($(this));
			}
        });


        $('.item_qty .quantity').keyup(function(e) {

        	var item_cost = parseFloat($(this).closest("tr").find(".item_cost input").val());

			if(item_cost > 0) { 	
				calculateItemTotal($(this));
			}
		});



        function calculateItemTotal(item) {

        	var item_qty = $(item).closest("tr").find(".item_qty .quantity").val();
        	var item_cost = parseFloat($(item).closest("tr").find(".item_cost input").val());

        	if(item_qty == '') {

        		item_qty = 0;
        	}

        	item_cost = item_cost * 10000; //avoid calculating decimals (floats)

        	var item_total = item_qty * item_cost;

        	item_total = fixDecimal(item_total / 10000);

        	$(item).closest("tr").find(".item_total input").val(formatMoney(item_total));

        	calculateInvoiceTotal();

        }


        function calculateInvoiceTotal() {

        	var subtotal = 0;
        	var total = 0;
        	var after_discount = 0;
        	var calculate_tax = $('#calculate_tax').val();
        	var tax_rate = $('#tax_rate').val() / 100;
        	var tax_amount = 0;


        	$('.invoice_items tr').each(function(index) {

        		if(index > 0) {

	        		var cur_item_qty = parseFloat($(this).find(".item_qty .quantity").val());
	        		var cur_item_cost = parseFloat($(this).find(".item_cost input").val());

	        		if(cur_item_qty > 0 && cur_item_cost != "") {
		        		var cur_item_total = cur_item_qty * cur_item_cost;

		        		subtotal += cur_item_total;
		        	}
        		}
        	});



        	if(discount_type == 'amount') {

        		after_discount = subtotal - discount_amount;
        	
        	} else if(discount_type == 'percent') {

        		after_discount = subtotal * ((100 - discount_amount) / 100);

        	} else {

        		//no discount
        		after_discount = subtotal;
        	}

        	after_discount = fixDecimal(parseFloat(after_discount));

        	if(calculate_tax == 1) {

	        	if(after_discount > 0) {

	        		tax_amount = (after_discount * tax_rate);
	        	} else {

	        		tax_amount = 0;
	        	}

	        	tax_amount = fixDecimal(parseFloat(tax_amount));


	        	$('.amounts .tax').html(formatMoney(tax_amount));

	        }


	        subtotal = fixDecimal(subtotal);

	        $('.amounts .subtotal').html(formatMoney(subtotal));

        	
        	total = fixDecimal(parseFloat(after_discount) + parseFloat(tax_amount));


        	$('.amounts .total').html(formatMoney(total));
	        
	        //} else {

	        // 	$('.amounts .subtotal').html("0.00");
	        // 	$('.amounts .tax').html("0.00");
	        // 	$('.amounts .total').html("0.00");
	        // }

        }


        function orderRows() {
        	var count = 0;

        	$('.invoice_items tr').each(function() {

        		$(this).find("td:first").html(count);
        		count++;
        	});
        }


        $('.add_item').click(function() {

        	$('.invoice_items tbody>tr:last').clone(true).insertAfter('.invoice_items tbody>tr:last');
		    //$('.invoice_items tbody>tr:last #name').val('');
		    //$(".invoice_items tbody>tr:last").each(function() {this.reset();});
		    $('.invoice_items .select2-container').remove();

		    var interval = setInterval(function() {

		    	var select = $('.invoice_items tbody>tr:last').prev().find("select.search_dropdown");
		    	
		    	if(!$(select).hasClass("select2-offscreen")) {

		    		$(select).addClass("select2-offscreen");
		    		clearInterval(interval);
		    	}

		    }, 10);


		    if($('.invoice_items tr:last select.select_product option:first').val() != '') {

	        	$('.invoice_items tr:last select.select_product option').removeAttr('selected');

	        	$('.invoice_items tr:last select.select_product').prepend('<option value="" selected="selected"></option>');
	        }
		    	

		    $('.invoice_items select.search_dropdown').select2({
	            placeholder: "Select...",
	            allowClear: true
	        });


	        $('.invoice_items tbody>tr:last td:first').html('');
	        $('.invoice_items tbody>tr:last td:nth-child(3)').html('');
	        $('.invoice_items tbody>tr:last td:nth-child(4) .quantity').val("1");
	        $('.invoice_items tbody>tr:last td:nth-child(5)').html('<input type="text" value="0.00"class="m-wrap small" readonly/>');
	        $('.invoice_items tbody>tr:last td:nth-child(6)').html('<input type="text" value="0.00"class="m-wrap small" readonly/>');
	        $('.invoice_items tbody>tr:last td:last').html('<a href="#" class="remove_item"><i class="icon-remove"></i></a>');

	        orderRows();

		    return false;
        });


		$('body').on("click", ".select2-search-choice-close", function() {

			$(this).closest("tr").remove();

        	calculateInvoiceTotal();

        	orderRows();

        	return false;
		});


        $('body').on("click", ".remove_item", function() {

        	$(this).closest("tr").remove();

        	if($('.invoice_items .remove_item').length <= 1) {

        		$('.invoice_items .remove_item').remove();
        	}

        	calculateInvoiceTotal();

        	orderRows();

        	saveDraft();

        	return false;
        });



        $('.apply_discount_submit').click(function() { 

        	if($('#apply_discount #discount_amount').val() > 0) {

        		$('#apply_discount #discount_amount').removeClass("error");

	        	if($('#verify_supervisor_account').val() == 1) {

	        		if($('#super_username option:selected').val() != "" && $('#super_password').val() != "") {

	        			$('#apply_discount').modal("hide");
	        			$('.loading_large').fadeIn('fast');

	        			var invoice_id = $('#invoice_id').val();

	        			discount_amount = parseFloat($('#apply_discount #discount_amount').val());
	        			discount_type = $('#apply_discount #discount_type option:selected').val();

	        			var username = $('#super_username option:selected').val();
	        			var password = $('#super_password').val();

	        			$('#super_username').removeClass("error");
	        			$('#super_password').removeClass("error");
	        			$('.verify_supervisor_error').addClass("hidden");
						$('.verify_supervisor_error').html("");

	        			$.ajax({

					        url: 'ajax_functions',  //server script to process data
					        type: 'POST',
					        data: {
					        	action: "apply_invoice_discount",
					        	invoice_id: invoice_id,
					        	discount_amount: discount_amount,
					        	discount_type: discount_type,
					        	username: username,
					        	password: password
					        }

					    }).done(function(data) {

					    	$('.loading_large').fadeOut();
	 
					    	$('#super_password').val('');

							if(data == "success") {

								$('.amounts li#discount').remove();

					        	currency = $('#currency').val();

					        	if(discount_type == 'amount') {

					        		discount = formatMoney(discount_amount);

					        		$('.amounts li:first').after('<li id="discount">Discount: ' + currency + '<strong>–' + discount + '</strong> (<a href="#" id="remove_discount">remove</a>)</li>');
					        	
					        	} else if(discount_type == 'percent') {

					        		$('.amounts li:first').after('<li id="discount">Discount: <strong>' + discount_amount + '</strong>% (<a href="#" id="remove_discount">remove</a>)</li>');
					        	}

					        	//discount = parseFloat(discount);

					        	calculateInvoiceTotal();

							} else if(data == "failed") {

								$('#apply_discount').modal("show");

								$('#super_username').addClass("error");
	        					$('#super_password').addClass("error");
	        					$('.verify_supervisor_error').removeClass("hidden");
	        					$('.verify_supervisor_error').addClass("text_error");
	        					$('.verify_supervisor_error').html("The supervisor could not be verified.");
							}
						});
	        		
	        		} else {

	        			$('#super_password').addClass("error");

	        		}

	        	} else {

	        		var invoice_id = $('#invoice_id').val();

        			discount_amount = parseFloat($('#discount_amount').val());
        			discount_type = $('#discount_type option:selected').val();

        			$('#apply_discount').modal("hide");
	        		$('.loading_large').fadeIn('fast');

        			$.ajax({

				        url: 'ajax_functions',  //server script to process data
				        type: 'POST',
				        data: {
				        	action: "apply_invoice_discount",
				        	invoice_id: invoice_id,
				        	discount_amount: discount_amount,
				        	discount_type: discount_type
				        }

				    }).done(function(data) {

						if(data == "success") {

							$('.loading_large').fadeOut('fast');

							$('.amounts li#discount').remove();

				        	currency = $('#currency').val();

				        	if(discount_type == 'amount') {

				        		discount = formatMoney(discount_amount);

				        		$('.amounts li:first').after('<li id="discount">Discount: ' + currency + '<strong>–' + discount + '</strong> (<a href="#" id="remove_discount">remove</a>)</li>');
				        	
				        	} else if(discount_type == 'percent') {

				        		$('.amounts li:first').after('<li id="discount">Discount: <strong>' + discount_amount + '</strong>% (<a href="#" id="remove_discount">remove</a>)</li>');
				        	}

				        	//discount = parseFloat(discount);

				        	calculateInvoiceTotal();

						}

					});

		        }

		    } else  {

		    	$('#discount_amount').addClass("error");
		    }

        	return false;
        });



		$('body').on('click', '#remove_discount', function() {

			$('.loading_large').fadeIn('fast');

			$.ajax({

		        url: 'ajax_functions',  //server script to process data
		        type: 'POST',
		        data: {
		        	action: "remove_invoice_discount",
		        	invoice_id: invoice_id
		        }

		    }).done(function(data) {

		    	discount_amount = 0;
		    	discount_type = '';

		    	$('.amounts li#discount').remove();

		    	calculateInvoiceTotal();

		    	$('.loading_large').fadeOut('fast');


		    });

		     return false;

		});


		
		$('body').on('change', '#create_invoice input, #create_invoice select, #create_invoice textarea', function() {

			changesMade();

		});

		$('body').on('click', '.add_item, .remove_item, .apply_discount_submit, #remove_discount', function() {

			changesMade();

		});


		function changesMade() {

			if($('.save_draft').hasClass('disabled')) {

				$('.save_draft').removeClass('disabled');
				$('.save_draft').html('Save Draft <i class="icon-save"></i>');

				//$('.autosave_note').html('');
				$('.save_draft').attr('title', '');

				confirm_pageunload = true;
			}

			saved = false;
		}



		setInterval(function() {

			if(saved == false) {

				autosaved = true;
				//update and save notes
				saveDraft();
			}

		}, 7500);



		$('.save_draft').click(function() {

			autosaved = false;
			saveDraft();

		    return false;

		});



		function saveDraft() { 

			$('.save_draft').addClass('disabled').text('Saving...');

			items = new Array();

			$('.invoice_items tr:not(:first)').each(function() {

				var item_id = $(this).find('.select_product option:selected').val();
				if(item_id != '' && item_id > 0) {

					var item_qty = $(this).find('.quantity').val();
					//var item_cost = $(this).find('.item_cost input').val();

					items.push([item_id, item_qty]);
				}

			});

			var items_json = JSON.stringify(items);

			var invoice_id = $('#invoice_id').val();
			var client_id = $('#select_client option:selected').val();
			var payment_type = $('#payment_type').val();
			var invoice_date = $('#invoice_date').val();
			var payment_term = $('#payment_term').val();
			var due_date = $('#due_date').val();
			var additional_notes = $('.additional_notes').val(); 


			if(page == "new_recurring_invoice") {

				var recurring_daily_freq = $('#recurring_daily_freq').val();
				var recurring_weekly_freq = $('#recurring_weekly_freq').val();
				var recurring_monthly_freq = $('#recurring_monthly_freq').val();
				var recurring_yearly_freq = $('#recurring_yearly_freq').val();
				var recurring_start_date = $('#recurring_start_date').val();
				var recurring_date_range = $('#recurring_date_range').val();
				var recurring_end_date = $('#recurring_end_date').val();
				var recurring_end_count = $('#recurring_end_count').val();

				$.ajax({

			        url: 'ajax_functions',  //server script to process data
			        type: 'POST',
			        data: {
			        	action: "save_recurring_invoice_draft",
			        	invoice_id: invoice_id,
			        	client_id: client_id,
			        	payment_type: payment_type,
			        	invoice_date: invoice_date,
			        	payment_term: payment_term,
			        	due_date: due_date,
			        	additional_notes: additional_notes,
			        	items: items_json,
			        	recurring_interval: recurring_interval,
			        	recurring_daily_freq: recurring_daily_freq,
			        	recurring_weekly_freq: recurring_weekly_freq,
			        	recurring_monthly_freq: recurring_monthly_freq,
			        	recurring_yearly_freq: recurring_yearly_freq,
			        	recurring_start_date: recurring_start_date,
			        	recurring_date_range: recurring_date_range,
			        	recurring_end_date: recurring_end_date,
			        	recurring_end_count: recurring_end_count
			        }

			    }).done(function(data) {

			    	$('.save_draft');
			    	$('.save_draft').addClass('disabled');
					$('.save_draft').html('Draft Saved <i class="icon-ok"></i>');

					saved = true;

					confirm_pageunload = false;

					var saved_timestamp = new Date();

					var hour = saved_timestamp.getHours();
					var minute = saved_timestamp.getMinutes();
					var timestamp;

					var saved_am_pm = "AM";

					if(hour >= 12) {
						saved_am_pm = "PM";
					}

				    if (hour > 12) {
				        hour = hour - 12;
				    }

					if (minute < 10) {

						minute = "0" + minute;
					}

					timestamp = hour + ":" + minute + " " + saved_am_pm;

					if(autosaved == true) {

						//$('.autosave_note').text('Autosaved at: ' + timestamp);
						$('.save_draft').attr('title', 'Autosaved at: ' + timestamp);
					
					} else if(autosaved == false) {

						//$('.autosave_note').text('Saved at: ' + timestamp);
						$('.save_draft').attr('title', 'Saved at: ' + timestamp);
					}

			    });

			} else if(page == "new_invoice") {

				$.ajax({

			        url: 'ajax_functions',  //server script to process data
			        type: 'POST',
			        data: {
			        	action: "save_invoice_draft",
			        	invoice_id: invoice_id,
			        	client_id: client_id,
			        	payment_type: payment_type,
			        	invoice_date: invoice_date,
			        	payment_term: payment_term,
			        	due_date: due_date,
			        	additional_notes: additional_notes,
			        	items: items_json
			        }

			    }).done(function(data) {

			    	$('.save_draft');
			    	$('.save_draft').addClass('disabled');
					$('.save_draft').html('Draft Saved <i class="icon-ok"></i>');

					saved = true;

					confirm_pageunload = false;

					var saved_timestamp = new Date();

					var hour = saved_timestamp.getHours();
					var minute = saved_timestamp.getMinutes();
					var timestamp;

					var saved_am_pm = "AM";

					if(hour >= 12) {
						saved_am_pm = "PM";
					}

				    if (hour > 12) {
				        hour = hour - 12;
				    }

					if (minute < 10) {

						minute = "0" + minute;
					}

					timestamp = hour + ":" + minute + " " + saved_am_pm;

					if(autosaved == true) {

						//$('.autosave_note').text('Autosaved at: ' + timestamp);
						$('.save_draft').attr('title', 'Autosaved at: ' + timestamp);
					
					} else if(autosaved == false) {

						//$('.autosave_note').text('Saved at: ' + timestamp);
						$('.save_draft').attr('title', 'Saved at: ' + timestamp);
					}

			    });

			}

		}



		$('#process_invoice_modal .process_and_email').click(function() {

			processInvoice(1);

			return false;

		});


		$('#process_invoice_modal .process_only').click(function() {

			processInvoice(0);

			return false;

		});


		function processInvoice(email_invoice) {

			$('#process_invoice_modal').modal('hide');

			$('.process_invoice').addClass('disabled').text('One Moment...');

			$('.loading_large').fadeIn('fast');

			items = new Array();

			$('.invoice_items tr:not(:first)').each(function() {

				var item_id = $(this).find('.select_product option:selected').val();
				if(item_id != '' && item_id > 0) {

					var item_qty = $(this).find('.quantity').val();
					//var item_cost = $(this).find('.item_cost input').val();

					items.push([item_id, item_qty]);
				}

			});

			var items_json = JSON.stringify(items);

			var invoice_id = $('#invoice_id').val();
			var client_id = $('#select_client option:selected').val();
			var payment_type = $('#payment_type').val();
			var invoice_date = $('#invoice_date').val();
			var payment_term = $('#payment_term').val();
			var due_date = $('#due_date').val();
			var additional_notes = $('.additional_notes').val(); 


			$.ajax({

		        url: 'ajax_functions',  //server script to process data
		        type: 'POST',
		        data: {
		        	action: "process_invoice",
		        	invoice_id: invoice_id,
		        	client_id: client_id,
		        	payment_type: payment_type,
		        	invoice_date: invoice_date,
		        	payment_term: payment_term,
		        	due_date: due_date,
		        	additional_notes: additional_notes,
		        	items: items_json,
		        	email_invoice: email_invoice
		        }

		    }).done(function(data) {

		    	if(data == 'success') {

		    		$('.loading_large').fadeOut('fast');

		    		window.location.href = "invoice?id=" + invoice_id;
		    	}

		    });

		}



		$('#delete_invoice_modal .delete').click(function() {

			$('.loading_large').fadeIn('fast');

			$('#delete_invoice_modal').modal('hide');

			var invoice_id = $('#delete_invoice_modal #invoice_id').val();

			$.ajax({

		        url: 'ajax_functions',  //server script to process data
		        type: 'POST',
		        data: {
		        	action: "delete_invoice",
		        	invoice_id: invoice_id
		        }

		    }).done(function(data) {

		    		$('.loading_large').fadeOut('fast');

		    		window.location.href = "invoices";

		    });

		    return false;
		});
	
		
		// ==================================
		// File upload


		var file_count = 0;
		var files = new Array();
		var invoice_id = $('#upload_invoice_files_modal #invoice_id').val();

		$('.file_upload #file').fileupload({
	        url: 'ajax_file_upload',
	        dataType: 'json',
	        autoUpload: true,
	        paramName: 'files[]',
		    maxFileSize: 100000000, // 100 MB
		    limitConcurrentUploads: 5,
	        formData: {
	        	action: 'upload_invoice_file',
	        	invoice_id: invoice_id
	        },
	        add: function(e, data) {

	        	data.files[0].id = "file_" + file_count;

	        	files.push(data);

	        	var filename = data.files[0].name;
	        	var filesize = bytesToSize(data.files[0].size);

	        	$('#upload_invoice_files_modal .added_files').show();

	        	$('#upload_invoice_files_modal .added_files tbody').append('<tr><td>' + filename + '</td><td>' + filesize + '</td><td><div id="' + data.files[0].id + '" class="file_progress progress progress_thin progress-success progress-striped"><div class="bar"></div></div></td></tr>');

	            file_count++;

	            if (data.autoUpload || (data.autoUpload !== false &&
			            $(this).fileupload('option', 'autoUpload'))) {
			        data.process().done(function () {
			            data.submit();
			        });
			    }

			    $('.cancel_uploads').removeClass('disabled');

	        },
	        start: function(e) {

	   //      	$('.profile_picture .profile_picture_upload .fileinput-button').fadeOut();

				// $('.current_photo_wrapper').slideUp();

	   //      	$('#profile_picture_progress .bar').css('width', '0%');
	   //      	$('#profile_picture_progress').slideDown();

	        },
	        progress: function (e, data) {

	            var progress = parseInt(data.loaded / data.total * 100, 10);
	            var file_id = data.files[0].id;

	            if(progress >= 100) {

	            	setTimeout(function() {

	            		$('.added_files #' + file_id).closest('td').html('Complete');
	            	}, 750);

	            } else {
	            	
	            	$('.added_files #' + file_id + ' .bar').css('width', progress + '%');
	            }
	        },
	        done: function(e, data) {

	        	$('.cancel_uploads').addClass('disabled');

	        	//reload the client files on the page
	        	$.ajax({

			        url: 'ajax_functions',  //server script to process data
			        type: 'POST',
			        data: {
			        	action: "reload_invoice_files",
			        	invoice_id: invoice_id
			        }

			    }).done(function(data) {

					if(data) {

						$('.attachments .attachment:not(:first)').remove();
						$('.attachments .clearfix').remove();
						$('.attachments .box-message').remove();

						$('.attachments').append(data);
						$('.attachments').append('<div class="clearfix"></div>');
					}

				});

	        }

	    });


		$('.cancel_uploads').click(function() {

			for (i = 0; i < files.length; i++) { console.log(files);

				var file_id = files[i].files[0].id;

				$('.added_files #' + file_id).closest('td').html('Cancelled');

				files[i].abort(); 
			}

			$('.cancel_uploads').addClass('disabled');

			return false;
		});


		$('body').on('click', '.attachment .delete_file', function() {

			var file_id = $(this).closest('.attachment').attr('data-filename');

			$('#delete_invoice_file_modal #file_id').val(file_id);

			$('#delete_invoice_file_modal .modal-body').html('Are you sure you want to delete ' + file_id + '?<br><br><i class="icon-warning-sign"></i> This action is permanent and cannot be undone.');


		});


		$('body').on('click', '#delete_invoice_file_modal .delete', function() {

			$('#delete_invoice_file_modal').modal('hide');

			var filename = $('#delete_invoice_file_modal #file_id').val();
			var invoice_id = $('#delete_invoice_file_modal #invoice_id').val();

			var attachment = $(".invoice_files .attachment[data-filename='" + filename + "']");
			
			$(attachment).animate({

				opacity: 0.5,
				duration: 300
			});

			$.ajax({

				type: "POST",
				url: "ajax_functions",
				data: { 
					action: "delete_invoice_file",
					filename: filename,
					invoice_id: invoice_id
				}

			}).done(function(result) {

				$(attachment).fadeOut(function() {

					$(attachment).remove();
				});
			});
			
			return false;
		});

		// ==================================


		//Run twice -- once at page load, and once of client_type dropdown change
		var type = $('#add_new_client_modal #client_type').find('option:selected').val();

		if(type == "individual") {
			$('#add_new_client_modal .row-fluid.company').fadeOut('fast', function() {
				$('#add_new_client_modal .row-fluid.individual').fadeIn('fast');
			});
		} else if(type == "company") {
			$('#add_new_client_modal .row-fluid.individual').fadeOut('fast', function() {
				$('#add_new_client_modal .row-fluid.company').fadeIn('fast');
			});
		}


		$('#add_new_client_modal #client_type').change(function() {
			var type = $(this).find('option:selected').val();

			if(type == "individual") {
				$('#add_new_client_modal .row-fluid.company').fadeOut('fast', function() {
					$('#add_new_client_modal .row-fluid.individual').fadeIn('fast');
				});
			} else if(type == "company") {
				$('#add_new_client_modal .row-fluid.individual').fadeOut('fast', function() {
					$('#add_new_client_modal .row-fluid.company').fadeIn('fast');
				});
			}
		});


	}



	if(page == "new_recurring_invoice") {

		function saveRecurringDraft() {

			$('.save_draft').addClass('disabled').text('Saving...');

			items = new Array();

			$('.invoice_items tr:not(:first)').each(function() {

				var item_id = $(this).find('.select_product option:selected').val();
				if(item_id != '' && item_id > 0) {

					var item_qty = $(this).find('.quantity').val();
					//var item_cost = $(this).find('.item_cost input').val();

					items.push([item_id, item_qty]);
				}

			});

			var items_json = JSON.stringify(items);

			var invoice_id = $('#invoice_id').val();
			var client_id = $('#select_client option:selected').val();
			var payment_type = $('#payment_type').val();
			var invoice_date = $('#invoice_date').val();
			var payment_term = $('#payment_term').val();
			var due_date = $('#due_date').val();
			var additional_notes = $('.additional_notes').val(); 

			var recurring_daily_days = $('#recurring_daily_days').val();
			var recurring_weekly_weeks = $('#recurring_weekly_weeks').val();
			var recurring_weekly_weekday = $('#recurring_weekly_weekday').val();
			var recurring_monthly_day = $('#recurring_monthly_day').val();
			var recurring_monthly_months = $('#recurring_monthly_months').val();
			var recurring_yearly_month = $('#recurring_yearly_month').val();
			var recurring_yearly_day = $('#recurring_yearly_day').val();
			var recurring_start_date = $('#recurring_start_date').val();
			var recurring_date_range = $('#recurring_date_range').val();
			var recurring_end_date = $('#recurring_end_date').val();
			var recurring_end_count = $('#recurring_end_count').val();

			$.ajax({

		        url: 'ajax_functions',  //server script to process data
		        type: 'POST',
		        data: {
		        	action: "save_recurring_invoice_draft",
		        	invoice_id: invoice_id,
		        	client_id: client_id,
		        	payment_type: payment_type,
		        	invoice_date: invoice_date,
		        	payment_term: payment_term,
		        	due_date: due_date,
		        	additional_notes: additional_notes,
		        	items: items_json,
		        	recurring_interval: recurring_interval,
		        	recurring_daily_days: recurring_daily_days,
		        	recurring_weekly_weeks: recurring_weekly_weeks,
		        	recurring_weekly_weekday: recurring_weekly_weekday,
		        	recurring_monthly_day: recurring_monthly_day,
		        	recurring_monthly_months: recurring_monthly_months,
		        	recurring_yearly_month: recurring_yearly_month,
		        	recurring_yearly_day: recurring_yearly_day,
		        	recurring_start_date: recurring_start_date,
		        	recurring_date_range: recurring_date_range,
		        	recurring_end_date: recurring_end_date,
		        	recurring_end_count: recurring_end_count
		        }

		    }).done(function(data) {

		    	$('.save_draft');
		    	$('.save_draft').addClass('disabled');
				$('.save_draft').html('Draft Saved <i class="icon-ok"></i>');

				saved = true;

				confirm_pageunload = false;

				var saved_timestamp = new Date();

				var hour = saved_timestamp.getHours();
				var minute = saved_timestamp.getMinutes();
				var timestamp;

				var saved_am_pm = "AM";

				if(hour >= 12) {
					saved_am_pm = "PM";
				}

			    if (hour > 12) {
			        hour = hour - 12;
			    }

				if (minute < 10) {

					minute = "0" + minute;
				}

				timestamp = hour + ":" + minute + " " + saved_am_pm;

				if(autosaved == true) {

					//$('.autosave_note').text('Autosaved at: ' + timestamp);
					$('.save_draft').attr('title', 'Autosaved at: ' + timestamp);
				
				} else if(autosaved == false) {

					//$('.autosave_note').text('Saved at: ' + timestamp);
					$('.save_draft').attr('title', 'Saved at: ' + timestamp);
				}

		    });

		}

	}



	if(page == "maps") {

		var markers = new Array();

		$('.filter_dropdown').select2({
            placeholder: "Filter...",
            allowClear: true
        });

        $('.client_dropdown').select2({
            placeholder: "Assign to Client... (Optional)",
            allowClear: true
        });

        $('.client_dropdown_modal').select2({
            placeholder: "Assign to Client... (Optional)",
            allowClear: true,
            minimumResultsForSearch: -1
        });


        $('.maps').submit(function() {

        	return false;
        })


		$('.show_fullscreen').click(function() {

			var fullwidth = window.innerWidth;
			var height = $(window).height();

			$(this).hide();
			$('i.hide_fullscreen').css("display", "inline-block");

			$('.map').parent().addClass("fullscreen");
			$('.fullscreen').css("cssText", "height: " + (height) + "px !important; width: " + (fullwidth) + "px !important;");
			//$('.fullscreen').css("cssText", "");
			$('.gmaps').height(height - 115);
			//$('.fullscreen').height(height);
			//$('.fullscreen').width(width);

			google.maps.event.trigger(map, 'resize'); 
		});


		$('i.hide_fullscreen').click(function() {

			var fullwidth = window.innerWidth;
			var height = $(window).height();

			$(this).css("display", "none");
			$('.show_fullscreen').show();

			$('.fullscreen').css("height", "");
			$('.fullscreen').css("width", "");
			$('.map').parent().removeClass("fullscreen");
			$('.gmaps').height(height - 208);
		});


		$(window).resize(function() {

			var fullwidth = window.innerWidth;

			var height = $(window).height();

			$('.fullscreen').css("cssText", "height: " + (height) + "px !important; width: " + (fullwidth) + "px !important;");
			//$('.gmaps').css("height", height - 103);
			google.maps.event.trigger(map, 'resize'); 
			
		});


		//$('.saved_locations li').hover(function() { 
		$('body').on("mouseenter", ".saved_locations li", function() {

			$(this).find('.edit_location').attr('style', 'display: block !important');

		});

		$('body').on("mouseleave", ".saved_locations li", function() {

			$(this).find('.edit_location').attr('style', 'display: none !important');

		});


		$('body').on("click", ".edit_location", function() {

			$('#edit_location_modal').modal("show");
		});


		$('.saved_locations a').live("click", function() { 

			//Clear the map markers before the geocoding kicks in.
			for (var i = 0, marker; marker = markers[i]; i++) {
	          marker.setMap(null); //clear all the markers
	        }

	        directionsDisplay.setMap(null); //clear any direction displays


			var address = $(this).attr("address");

			$('.maps #gmap_geocoding_address').val(address);

			getGeocode(address);

			return false;
		});



		$('#gmap_geocoding_address').keydown(function(e) {

			var code = (e.keyCode ? e.keyCode : e.which);

			if (code == 13) {

				//Clear the map markers before the geocoding kicks in.
				for (var i = 0, marker; marker = markers[i]; i++) {
		          marker.setMap(null); //clear all the markers
		        }

		        directionsDisplay.setMap(null); //clear any direction displays

		        return false;
			}

		});


		$('#search_saved_locations').keyup(function(e) {

			var code = (e.keyCode ? e.keyCode : e.which);

			if (code == 13) {

				return false;

			} else {

				searchLocations($(this).val());

			}

		});


		function searchLocations(searchVal) {

			$('.locations_list li').each(function() {

				var found = false;

				var regExp = new RegExp(searchVal, 'i');

				if(regExp.test($(this).text())) {
					found = true;

				}


				if(found == true) {
					$(this).show();
				} else {
					$(this).hide();
				}

			});

		}



		$('.add_new_location input').focus(function() {

			$(this).removeClass("error");
		});


		$('.add_location').click(function() {

			var loc_name = $('#loc_name').val();
			var loc_address = $('#loc_address').val();
			var client_id = $('#clients_list option:selected').val();

			var errors = 0;

			if(loc_name == "") {

				$('.add_new_location #loc_name').addClass("error");
				errors++;	
			} 

			if(loc_address == "") {

				$('.add_new_location #loc_address').addClass("error");
				errors++;
			}


			if(errors == 0) {

				$('.add_new_location #loc_name').removeClass("error");
				$('.add_new_location #loc_address').removeClass("error");

				$('#loc_name').val("");
				$('#loc_address').val("");
				$("#clients_list").select2('val', null);

				addLocation(loc_name, loc_address, client_id);
			}

			return false;

		});



		$('.save_location').click(function() {

			var loc_name = $('#save_loc_name').val();
			var loc_address = $('#save_loc_address').val();
			var client_id = $('#clients_list_modal option:selected').val();

			$('#save_location_modal').modal("hide");

			$('#save_loc_name').val("");
			$('#save_loc_address').val("");
			$("#clients_list_modal").select2('val', null);
			
			addLocation(loc_name, loc_address, client_id);

			return false;

		});


		$('#saved_locations_filter').change(function() {

			var client_id = $(this).find("option:selected").val();

			$('.loading_large').fadeIn('fast');

			$.ajax({

		        url: 'ajax_functions',  //server script to process data
		        type: 'POST',
		        data: {
		        	action: "filter_locations",
		        	client_id: client_id
		        }

		    }).done(function(data) {

				$('.loading_large').fadeOut('fast');

				$('.locations_list').fadeOut('fast', function() {

					$('.locations_list').html(data);

					$(this).fadeIn('fast');
				});

			});

		});


		$('#gmap_save_location').click(function() {

			$('#save_loc_address').val($('#gmap_geocoding_address').val());
			$('#save_loc_name').val($('#gmap_geocoding_address').val());

		});



		$('#delete_location_modal .delete').click(function() {

			$('#delete_location_modal').modal("hide");

			$('.loading_large').fadeIn('fast');

			var loc_id =  $.trim($('#delete_location_modal .loc_id').val());

			$.ajax({

		        url: 'ajax_functions',  //server script to process data
		        type: 'POST',
		        data: {
		        	action: "delete_location",
		        	loc_id: loc_id
		        }

		    }).done(function(data) {
		    	
		    	$('#delete_location_modal .loc_id').val("");

		    	$('.locations_list li a#' + loc_id).parent().remove();

		    	$('.loading_large').fadeOut('fast');

		    });

			return false;

		})



		$('#gmap_remove_markers').click(function() {

            for (var i = 0, marker; marker = markers[i]; i++) {
	          marker.setMap(null); //clear all the markers
	        }

	        directionsDisplay.setMap(null); //clear any direction displays

            $("#gmap_geocoding_address").val("");
        });


		function addLocation(loc_name, loc_address, client_id) {

			$('.loading_large').fadeIn('fast');

			$.ajax({

		        url: 'ajax_functions',  //server script to process data
		        type: 'POST',
		        data: {
		        	action: "add_location",
		        	loc_name: loc_name,
		        	loc_address: loc_address,
		        	client_id: client_id
		        }

		    }).done(function(data) {

				if(data) {

					var last_id = data;

					$('.loading_large').fadeOut('fast');

					$('.portlet .collapse').closest(".portlet").children(".portlet-body").slideUp(200);
					$('.portlet .collapse').removeClass("collapse").addClass("expand");

					$('.saved_locations').slideDown(200);
					$('.locations portlet-title').removeClass("expand").addClass("collapse");
					$('.locations portlet-title .tools a').removeClass("expand").addClass("collapse");

					if($('.filter_dropdown').select2('val') != "") {

						$('.filter_dropdown').select2('val', null);
						$('#saved_locations_filter').trigger("change");
					} else {

						$('.locations_list').prepend('<li><a href="#" address="' + loc_address + '"><i class="icon-map-marker"></i> ' + loc_name + '<span class="inline_address">| ' + loc_address + '</span></a><i class="icon-pencil right edit_location" id="' + last_id + '" style="display: none !important"></i></li>')
					}
				}

			});

		}


		$('body').on("click", ".portlet .portlet-title.expand", function() {

			$('.portlet .collapse').closest(".portlet").children(".portlet-body").slideUp(300);
			$('.portlet .collapse').removeClass("collapse").addClass("expand");

			$(this).closest(".portlet").children(".portlet-body").delay(50).slideDown(300);
			$(this).delay(100).removeClass("expand").addClass("collapse");

		});



		$('#gmap_geocoding_btn').click(function() {

			var address = $('#gmap_geocoding_address').val();

			getGeocode(address);
		});



		$('#get_directions_to_from_here').click(function() {


			//Clear any previous directions first...
			$('#directions_panel').html("");
			$('#start_address').val("");
			$('#end_address').val("");
			$('#geo_marker').removeClass("found");
			directionsDisplay.setMap(null);
			// -------

			var address = $('#gmap_geocoding_address').val();

			$('#end_address').val(address);


			$(".add_new_location .portlet-body").slideUp(300);
			$(".add_new_location .portlet-title").removeClass("collapse").addClass("expand");

			$(".locations .portlet-body").slideUp(300);
			$(".locations .portlet-title").removeClass("collapse").addClass("expand");

			$(".directions .portlet-body").delay(50).slideDown(300);
			$(".directions .portlet-title").delay(100).removeClass("expand").addClass("collapse");

		});


		$('.switch_directions').click(function() {

			var start_address = $('#start_address').val();
			var end_address = $('#end_address').val();

			$('#start_address').val(end_address);
			$('#end_address').val(start_address);

			if(start_address != "" && end_address != "") {

				$('.get_directions').trigger("click");
			}

			return false;

		});


		$('.travel_mode .btn').click(function() {

			$('.travel_mode .btn').removeClass("selected");
			$(this).addClass("selected");

			var start_address = $('#start_address').val();
			var end_address = $('#end_address').val();

			if(start_address != "" && end_address != "") {

				$('.get_directions').trigger("click");
			}

			return false;
		});


		$('#start_address').keyup(function(e) {

			var code = (e.keyCode ? e.keyCode : e.which);

			if (code == 13) {

				var start_address = $('#start_address').val();
				var end_address = $('#end_address').val();

				if(start_address != "" && end_address != "") {

					$('.get_directions').trigger("click");
				}

			}

		});

		$('#end_address').keyup(function(e) {

			var code = (e.keyCode ? e.keyCode : e.which);

			if (code == 13) {

				var start_address = $('#start_address').val();
				var end_address = $('#end_address').val();

				if(start_address != "" && end_address != "") {

					$('.get_directions').trigger("click");
				}

			}

		});


		$('#start_address').focus(function() {

			$(this).removeClass("error");
		});

		$('#end_address').focus(function() {

			$(this).removeClass("error");
		});

		var geo_marker, timeout;

		$('#geo_marker').click(function() {

			if(!$(this).hasClass("searching")) {

				$(this).addClass("searching");

				if(navigator.geolocation) {

					geo_marker = setInterval(function() {

						$('#geo_marker').toggle();
					}, 200);

					timeout = setTimeout(function() {
						clearInterval(geo_marker);
						$('#geo_marker').show();
					}, 5000);

					navigator.geolocation.getCurrentPosition(handle_geolocation_query); 
				}
			}
		});

        function handle_geolocation_query(position){  
            var lat = position.coords.latitude;
            var lng = position.coords.longitude;

            $('#start_address').val(lat + ', ' + lng);

			clearInterval(geo_marker);
			$('#geo_marker').removeClass("searching");
			$('#geo_marker').addClass("found");

			$('#geo_marker').show();

			$('#start_address').keydown(function(e) {

				$('#geo_marker').removeClass("found");

			});
        }


		$('.get_directions').click(function() {

			if(markers.length > 0) {
				for (var i = 0, marker; marker = markers[i]; i++) {
		          marker.setMap(null); //clear all the markers
		        }
		    }

			var selectedMode = $('.travel_mode .selected').attr("id");

		    var start_address = $('#start_address').val();
			var end_address = $('#end_address').val();
			var errors = 0;

			if(start_address == "") {
				$('#start_address').addClass("error");
				errors++;
			}

			if(end_address == "") {
				$('#end_address').addClass("error");
				errors++;
			}

			if(errors == 0) {

				$('#start_address').removeClass("error");
				$('#end_address').removeClass("error");

			    var request = {
			        origin: start_address,
			        destination: end_address,
			        travelMode: google.maps.TravelMode[selectedMode]
			    };

			    directionsService.route(request, function (response, status) {
			        if (status == google.maps.DirectionsStatus.OK) {

			        	$('#directions_panel').html("");
			        	directionsDisplay.setMap(map); //re-setting the map for the directions after clearing it

			            directionsDisplay.setDirections(response);
			        } else {

			        	if(status == "NOT_FOUND") {

			        		$('#directions_panel').html("Unable to find the specified location.");

			        	} else if(status == "ZERO_RESULTS") {

			        		$('#directions_panel').html("Directions between the specified locations could not be determined.");

			        	} else if(status == "OVER_QUERY_LIMIT") {

			        		$('#directions_panel').html("Query Limit Exceeded. The server has received too many requests in a short period of time. Please try again at a later time.");

			        	} else if(status == "UNKNOWN_ERROR") {

			        		$('#directions_panel').html("An unknown error has occured. Please try again at a later time.");
			        	
			        	}
			        }
			    });
			}

		    return false;

		});


		$('.directions .clear').click(function() {

			$('#directions_panel').html("");

			$('#start_address').val("");
			$('#end_address').val("");

			$('#geo_marker').removeClass("found");

			directionsDisplay.setMap(null);

			return false;
				
		});


		var ref_loc_name = $('.ref_loc_name').val(); //if page is loaded with loc_id in URL, location address is stored in referred_location.
		var ref_loc_address = $('.ref_loc_address').val();
		var location;

		if(ref_loc_address != "") {

			$('#gmap_geocoding_address').val(ref_loc_address);

			getGeocode(ref_loc_address);
		}


		function getGeocode(address) {

			//Not part of the function, but need to clear all markers first.
			//Clear any direction markers first
			for (var i = 0, marker; marker = markers[i]; i++) {
	          marker.setMap(null); //clear all the markers
	        }
	        directionsDisplay.setMap(null); //clear any direction displays
	        // ---------------------

			var geocoder = new google.maps.Geocoder();

		    geocoder.geocode( { 'address': address}, function(results, status) {

			    setMarker(map, ref_loc_name, results[0].geometry.location); 
		    });
		    
		}


		function setMarker(map, name, location) {

			for (var i = 0, marker; marker = markers[i]; i++) {
	          marker.setMap(null); //clear all the markers
	        }

			var image = {
				url: 'img/map_marker.png',
				size: new google.maps.Size(17, 25),
				origin: new google.maps.Point(0,0),
				anchor: new google.maps.Point(9, 25)
			};


		    //var myLatLng = new google.maps.LatLng(location);
		    var marker = new google.maps.Marker({
		        position: location,
		        map: map,
		        icon: image,
		        title: name
		    });

		    markers.push(marker);

		    map.setCenter(location);
		    map.setZoom(14);

		}


		/*$('.remove_location').click(function() {

			var loc_id = $(this).attr("id");
			var element = $(this);

			$.ajax({

		        url: 'ajax_functions',  //server script to process data
		        type: 'POST',
		        data: {
		        	action: "remove_location",
		        	loc_id: loc_id
		        }

		    }).done(function(data) {

				if(data) {

					$(element).parent().remove();
					
				}

			});

		});*/

	}



	if(page == "profile") {

    	$('.profile_pic').css({display: 'none', visibility: 'visible'}).fadeIn(700);


    	$('body').on("click", ".load_earlier_activities", function() {

			load_earlier_activities();

			return false;

		});

		loading_activities = false;
		no_further_activities = false;

		// $('.page-content').scroll(function () {

		// 	if(($('.page-content').scrollTop() + $('.page-content').height() > $('.page-content .container-fluid').height() - 100) && (loading_activities == false) && (no_further_activities == false)) {

		// 		loading_activities = true;
		// 		load_earlier_activities();

		// 	}

		// });


		function load_earlier_activities() {

			$('.load_earlier_activities').text('').addClass('loading_inline');

			var last_retrieved_id = $('.load_earlier_activities').attr('id');

			var user_id = $('.profile #user_id').val();

			$.ajax({

		        url: 'ajax_functions',  //server script to process data
		        type: 'POST',
		        data: {
		        	action: "load_earlier_user_activities",
		        	last_retrieved_id: last_retrieved_id,
		        	user_id: user_id
		        }

		    }).done(function(data) {

				if(data) {

					loading_activities = false;

					$('.load_earlier_activities').closest('li').remove();
					$('ul.timeline').append(data);

				} else {

					$('.load_earlier_activities').closest('li').remove();
					$('ul.timeline').append('<li class="center faded">No further activities</li>');
					no_further_activities = true;
				}

			});

		}

	}


	if(page == "profile_settings") {


		// $('input[type=file]').live('change', function(){

		// 	if($('.fileupload .fileupload-preview').html() == "") {

		// 		 $('.fileupload .uneditable-input').css('width', '181px');
		// 	}
		// });

	
		$('.profile_picture #profile_picture').fileupload({
	        url: 'ajax_file_upload',
	        dataType: 'json',
	        autoUpload: true,
	        paramName: 'files[]',
	        acceptFileTypes: /(\.|\/)(jpe?g|png)$/i,
		    maxFileSize: 10000000, // 10 MB
		    imageMaxWidth: 800,
		    imageMaxHeight: 800,
	        formData: {
	        	action: 'upload_profile_picture'
	        },
	        start: function(e) {

	        	// $('#profile_picture_preview_wrapper').hide();
	        	// $('#profile_picture_preview').hide();
	        	$('.profile_picture .profile_picture_upload .fileinput-button').fadeOut();

				$('.current_photo_wrapper').slideUp();

	        	$('#profile_picture_progress .bar').css('width', '0%');
	        	$('#profile_picture_progress').slideDown();

	        },
	        progressall: function (e, data) {

	            var progress = parseInt(data.loaded / data.total * 100, 10);

	            $('#profile_picture_progress .bar').css('width', progress + '%');
	        },
	        done: function(e, data) {

	        	$.ajax({

			        url: 'ajax_functions',  //server script to process data
			        type: 'POST',
			        data: {
			        	action: "get_profile_picture_url"
			        }

			    }).done(function(data) {

					if(data) {

						var thumbnail = data.replace('pr_imgs/', 'pr_imgs/thumbs/');

						$('.current_photo').attr('src', data);

						$(".current_photo").one("load", function() {
						  // do stuff
						}).each(function() {
						  if(this.complete) $(this).load();
						});

						$('.current_photo').load( function() {

							$('#profile_picture_progress').slideUp(function() {

								$('.current_photo_wrapper').slideDown();

				    			setTimeout(function() {

			    					$('.profile_picture .profile_picture_upload .fileinput-button').fadeIn();

			    				}, 500);
			    			});

			    			$('.header .nav li.user img').attr('src', thumbnail);

						});

					}

				});

	        }

	    });


		$('.profile_settings_nav a').click(function() { 

			var id = $(this).attr('id');

			$('.section:visible').fadeOut('fast', function() {

				$('.row-fluid.' + id).fadeIn('fast');
			});

			return false;
		});



		if(hash != "") {

			if(hash == "personal_info") {

				$('.section').hide();
				$('.row-fluid.personal_info').fadeIn('fast');
			
			} else if(hash == "profile_picture") {

				$('.section').hide();
				$('.row-fluid.profile_picture').fadeIn('fast');
			
			} else if(hash == "security") {

				$('.section').hide();
				$('.row-fluid.security').fadeIn('fast');
			
			}
		}



		$('#country').change(function() {

			var country_code = $('#country option:selected').val();

			$.ajax({

		        url: 'ajax_functions',  //server script to process data
		        type: 'POST',
		        data: {
		        	action: "load_timezone_select",
		        	country_code: country_code
		        }

		    }).done(function(data) {

				if(data) {

					data = '<option value="-1">Timezone</option>' + data;

					$('#timezone').html(data);
					
				}
			});

		});




        //On Load....
        var interests = $('#interests_hidden').val().split(",");
        var fav_books = $('#fav_books_hidden').val().split(",");
        var fav_movies = $('#fav_movies_hidden').val().split(",");
        var fav_music = $('#fav_music_hidden').val().split(",");
        var languages = $('#languages_hidden').val().split(",");


        $("#interests").val(interests);
        $('#fav_books').val(fav_books);
        $('#fav_movies').val(fav_movies);
        $('#fav_music').val(fav_music);
        $('#languages').val(languages);



        $("#interests").select2({
            tags: ['3D Computer Graphics Design','Adobe After Effects','Adobe Flash','Adobe Illustrator','Adobe Photoshop','Aerobics','Aircraft Spotting','Amateur Theater','Animation','Animation Design','Antique Cars','Antiques','Archery','Arts and Crafts','Astronomy','Aviation','Backgammon','Backpacking','Badminton','Baking','Banknotes','Barbecue (Grilling)','Baseball','Baseball Cards','Basketball','Birdfeeding and Birdwatching','Blogging','Board Games','Books','Bowling','Boxing','Cake Making / Cake Decorating','Camping','Canoeing/Kayaking','Card Games','Caving','Checkers','Cheerleading','Chess','Coins','Collecting','Comic Books','Computer Programming','Computer Games','Computers','Cricket','Cross Country','Cycling','DIY (Do It Yourself)','Dancing','Diving','Dog Breeding','Dominoes','Drawing','Electronics','Facebook','Fashion Design','Film-making','Fishing','Food','Football','Foreign Languages','Free Running','Games','Gardening','Gliding','Go-Karting','Golf','Gymnastics','Handbags','Hanggliding','Hats','Home Repairs','Home Theater','Horoscopes','Horse Riding','Horses','Hunting','Ice Skating','Ice Skating','Interior Design','Jewelry','Juggling','Karaoke','Kitesurfing','Knitting','Lacrosse','Lifecasting','Literature','Magic Tricks','Martial Arts','Model Aircraft ','Model Cars and RC Cars','Model Railways','Model Rockets','Monopoly','Mopeds','Motorcycles','Motorsports','Mountain Climbing','Music','Musical Instruments','Music Production','MySpace','News','Off-roading','Origami','Outdoor/Nature Activities','Paintball','Painting','Parkour','Performing Arts','Photographs','Photography','Poetry','Poker','Pottery','Rafting','Rallying','Reading','Recordings (Records, CDs, Cassettes)','Rock Climbing','Rocks and Minerals','Rowing','Running, Jogging','SCUBA Diving','Sailing','Sailing','Scooters','Scrapbooking','Sewing','Shooting','Short Movies','Sight Seeing','Singing','Skateboarding','Skating','Skiing','Snorkeling','Snowboarding','Soccer','Songwriting','Squash','Stamping','Stamps','StumbleUpon','Sudoku','Surfing','Swimming','Table Tennis','Tae Kwon Do','Tennis','Train Spotting','Traveling','Treasure Hunting','Trucks','Twitter','Unicycling','Urban Exploration','Video Games','Volleyball','Walking','Watches and Clocks','Watercolor','Weaving','Weight Lifting','Wine','Wine Tasting','Woodworking','Writing','Yachting','Yoga']
        });

        $("#fav_books").select2({
            tags: ['A Child Called It','A Short History of Nearly Everything','A Short History of Tractors in Ukrainian','A Thousand Splendid Suns','About a Boy','The Alchemist','Angelas Ashes:A Memoir of a Childhood','Angels and Demons','Atonement','Billy Connolly','Birdsong','Breaking Dawn','Brick Lane','Bridget Joness Diary','Bridget Jones: The Edge of Reason','Captain Corellis Mandolin','Chocolat','Dear Fatty','Deception','Delia\'s How to Cook','Digital Fortress','Down Under','Dr. Atkins\' New Diet Revolution','Eats, Shoots and Leaves','Eclipse','Fifty Shades Darker','Fifty Shades Freed','Fifty Shades of Grey','The Girl with the Dragon Tattoo','Hannibal','Happy Days with the Naked Chef','Harry Potter and the Chamber of Secrets','Harry Potter and the Deathly Hallows','Harry Potter and the Goblet of Fire','Harry Potter and the Half-blood Prince','Harry Potter and the Order of the Phoenix','Harry Potter and the Prisoner of Azkaban','I Can Make You Thin','Jamies 30-Minute Meals','Jamies Italy','Jamies Ministry of Food','Labyrinth','Life of Pi','Man and Boy','Memoirs of a Geisha','Men are from Mars, Women are from Venus','My Booky Wook','My Sister\'s Keeper','New Moon','Nigella Express','Nights of Rain and Stars','Northern Lights','Notes from a Small Island','One Day','PS, I Love You','Room on the Broom','Sharon Osbourne Extreme:My Autobiography','Small Island','Stupid White Men:...and Other Sorry Excuses for the State of the Natio','The "Beano" Annual','The Amber Spyglass','The Book Thief','The Boy in the Striped Pyjamas','The Broker','The Curious Incident of the Dog in the Night-time','The Da Vinci Code','The Ghost','The Girl Who Kicked the Hornets Nest','The Girl Who Played with Fire','The God Delusion','The Gruffalo','The Gruffalos Child','The Help','The House at Riverton','The Hunger Games','The Interpretation of Murder','The Island','The Kite Runner','The Lord of the Rings','The Lost Boy','The Lost Symbol','The Lovely Bones','The Memory Keeper\'s Daughter','The No.1 Ladies Detective Agency','The Return of the Naked Chef','The Shadow of the Wind','The Sound of Laughter','The Subtle Knife','The Summons','The Tales of Beedle the Bard','The Time Travelers Wife','The Very Hungry Caterpillar','The World According to Clarkson','To Kill a Mockingbird','Twilight','White Teeth','You are What You Eat:The Plan That Will Change Your Life']
        });

        $("#fav_movies").select2({
            tags: ['12 Angry Men','2001: A Space Odyssey','3 Idiots','8½','A Beautiful Mind','A Clockwork Orange','A Fistful of Dollars','A Separation','A Streetcar Named Desire','Alien','Aliens','All About Eve','All Quiet on the Western Front','Amadeus','American Beauty','American History X','Amores perros','Amélie','Anatomy of a Murder','Annie Hall','Apocalypse Now','Back to the Future','Barry Lyndon','Batman Begins','Beauty and the Beast','Before Sunrise','Ben-Hur','Bicycle Thieves','Black Swan','Blade Runner','Braveheart','Butch Cassidy and the Sundance Kid','Casablanca','Casino','Chinatown','Cinema Paradiso','Citizen Kane','City Lights','City of God','Cool Hand Luke','Das Boot','Dial M for Murder','Die Hard','District 9','Django Unchained','Dog Day Afternoon','Donnie Darko','Double Indemnity','Downfall','Dr. Strangelove or: How I Learned to Stop Worrying and Love the Bomb','Eternal Sunshine of the Spotless Mind','Fanny and Alexander','Fargo','Festen','Fight Club','Finding Nemo','For a Few Dollars More','Forrest Gump','Full Metal Jacket','Gandhi','Gladiator','Gone with the Wind','Good Will Hunting','Goodfellas','Gran Torino','Grave of the Fireflies','Groundhog Day','Harry Potter and the Deathly Hallows: Part 2','Harvey','Heat','High Noon','Hotel Rwanda','How to Train Your Dragon','Howls Moving Castle','Ikiru','In the Mood for Love','In the Name of the Father','Incendies','Inception','Indiana Jones and the Last Crusade','Infernal Affairs','Inglourious Basterds','Into the Wild','Intouchables','Ip Man','It Happened One Night','Its a Wonderful Life','Jaws','Jurassic Park','Kill Bill: Vol. 1','L.A. Confidential','La haine','La strada','Lawrence of Arabia','Life Is Beautiful','Life of Brian','Life of Pi','Like Stars on Earth','Lock, Stock and Two Smoking Barrels','Léon: The Professional','M','Manhattan','Mary and Max','Memento','Memories of Murder','Metropolis','Million Dollar Baby','Modern Times','Monsters, Inc.','Monty Python and the Holy Grail','Mr. Smith Goes to Washington','My Neighbor Totoro','Mystic River','Nausicaä of the Valley of the Wind','Network','No Country for Old Men','North by Northwest','Notorious','Oldboy','On the Waterfront','Once Upon a Time in America','Once Upon a Time in the West','One Flew Over the Cuckoos Nest','Pans Labyrinth','Papillon','Paths of Glory','Persona','Pirates of the Caribbean: The Curse of the Black Pearl','Platoon','Princess Mononoke','Psycho','Pulp Fiction','Raging Bull','Raiders of the Lost Ark','Rain Man','Ran','Rashômon','Ratatouille','Rear Window','Rebecca','Requiem for a Dream','Reservoir Dogs','Rocky','Roman Holiday','Rope','Rosemarys Baby','Saving Private Ryan','Scarface','Schindlers List','Se7en','Seven Samurai','Shutter Island','Sin City','Singin in the Rain','Sleuth','Slumdog Millionaire','Snatch.','Some Like It Hot','Spirited Away','Spring, Summer, Fall, Winter... and Spring','Stalag 17','Stalker','Stand by Me','Star Trek','Star Trek Into Darkness','Star Wars: Episode IV - A New Hope','Star Wars: Episode V - The Empire Strikes Back','Star Wars: Episode VI - Return of the Jedi','Strangers on a Train','Sunset Blvd.','Taxi Driver','Terminator 2: Judgment Day','The 400 Blows','The Apartment','The Artist','The Avengers','The Best Years of Our Lives','The Big Lebowski','The Big Sleep','The Bourne Ultimatum','The Bridge on the River Kwai','The Dark Knight','The Dark Knight Rises','The Deer Hunter','The Departed','The Diving Bell and the Butterfly','The Elephant Man','The Exorcist','The General','The Godfather','The Godfather: Part II','The Gold Rush','The Good, the Bad and the Ugly','The Graduate','The Grapes of Wrath','The Great Dictator','The Great Escape','The Green Mile','The Hobbit: An Unexpected Journey','The Hunt','The Hustler','The Kid','The Killing','The Kings Speech','The Lion King','The Lives of Others','The Lord of the Rings: The Fellowship of the Ring','The Lord of the Rings: The Return of the King','The Lord of the Rings: The Two Towers','The Maltese Falcon','The Man Who Shot Liberty Valance','The Manchurian Candidate','The Matrix','The Night of the Hunter','The Pianist','The Prestige','The Princess Bride','The Secret in Their Eyes','The Seventh Seal','The Shawshank Redemption','The Shining','The Silence of the Lambs','The Sixth Sense','The Sting','The Terminator','The Thing','The Third Man','The Treasure of the Sierra Madre','The Truman Show','The Untouchables','The Usual Suspects','The Wild Bunch','The Wizard of Oz','There Will Be Blood','To Kill a Mockingbird','Touch of Evil','Toy Story','Toy Story 3','Trainspotting','Twelve Monkeys','Unforgiven','Up','V for Vendetta','Vertigo','WALL·E','Warrior','Whos Afraid of Virginia Woolf?','Wild Strawberries','Witness for the Prosecution','Yojimbo']
        });

        $("#fav_music").select2({
            tags: ['50Cent','Adam Levine','Adele','Agnes Monica','Alejandro Sanz','Alicia Keys', 'Armin Van Buuren', 'Ashley Tisdale','Avril Lavigne','Beyoncé','Big Sean','Britney Spears','Bruno Mars','Calle 13','Carly Rae Jepsen','Chayanne','Cher Lloyd','Cheryl Cole','Chris Brown','Christina Aguilera','Ciar','Cody Simpson','Coldplay','Daddy Yankee','David Bisbal','David Guetta','Ed Sheeran','Enrique Iglesias','Harry Styles','Ivete Sangalo','J. Cole','Jaden Smith','Jason Mraz','Jennifer Lopez','Jessica Simpson','Jessie','Joe Jonas','John Legend','Jonas Brothers','Juan Luis Guerra','Juanes','Justin Bieber','Justin Timberlake','Kanye West','Katy Perry','Ke$ha','Kelly Clarkson','Kelly Rowland','LL COOL J','LMFAO','Lady Gaga','Lenny Kravitz','Liam Payne','Lil Wayne','Lily Allen','Louis Tomlinson','Luan Santana','Ludacris','Luis Fonsi','Mac Miller','Mariah Carey','Markus Schulz','Eminem','Mary J. Blige','Miley Cyrus','Miranda Cosgrove','Ne-Yo','Niall Horan','Nick Jonas','Nicki Minaj','Olly Murs','One Direction','P!nk','Paulina Rubio','Pitbull','Queen Latifah','Rev Run','Ricardo Montaner','Ricky Martin','Rihanna','Sandy Leah','Sean Kingston','Selena Gomez','Shakira','Sherina Munaf','Siwon Choi','Snoop Dogg','Soulja Boy','T-Raww','T.I.','Taylor Swift','Thalia','Trey Songz','Usher','Vidi Aldiano','Wiz Khalifa','Yoko Ono','Zayn Malik','Afgansyah Reza','Demi Lovato','will.i.am']
        });

		$("#languages").select2({
            tags: ['Afrikaans','Albanian','Arabic','Armenian','Basque','Bengali','Bulgarian','Catalan','Cambodian','Chinese (Cantonese)','Chinese (Mandarin)','Croatian','Czech','Danish','Dutch','English','Estonian','Fiji','Finnish','French','Georgian','German','Greek','Gujarati','Hebrew','Hindi','Hungarian','Icelandic','Indonesian','Irish','Italian','Japanese','Javanese','Korean','Latin','Latvian','Lithuanian','Macedonian','Malay','Malayalam','Maltese','Maori','Marathi','Mongolian','Nepali','Norwegian','Persian','Polish','Portuguese','Punjabi','Quechua','Romanian','Russian','Samoan','Serbian','Slovak','Slovenian','Spanish','Swahili','Swedish ','Tamil','Tatar','Telugu','Thai','Tibetan','Tonga','Turkish','Ukrainian','Urdu','Uzbek','Vietnamese','Welsh','Xhosa']
        });


		
		$('form.personal_info').submit(function() {

			var interests = "";

			$('#interests').parent().find("ul li").each(function() {

				interests += $(this).find("div").text() + ",";
			});

			interests = interests.replace(',,', '');

			$('#interests_hidden').val(interests);


			var fav_books = "";

			$('#fav_books').parent().find("ul li").each(function() {

				fav_books += $(this).find("div").text() + ",";
			});

			fav_books = fav_books.replace(',,', '');

			$('#fav_books_hidden').val(fav_books);


			var fav_movies = "";

			$('#fav_movies').parent().find("ul li").each(function() {

				fav_movies += $(this).find("div").text() + ",";
			});

			fav_movies = fav_movies.replace(',,', '');

			$('#fav_movies_hidden').val(fav_movies);


			var fav_music = "";

			$('#fav_music').parent().find("ul li").each(function() {

				fav_music += $(this).find("div").text() + ",";
			});

			fav_music = fav_music.replace(',,', '');

			$('#fav_music_hidden').val(fav_music);


			var languages = "";

			$('#languages').parent().find("ul li").each(function() {

				languages += $(this).find("div").text() + ",";
			});

			languages = languages.replace(',,', '');

			$('#languages_hidden').val(languages);

			
		});


		$('.profile_picture img').click(function() {

			//$('.profile_picture img').removeClass("selected");
			//$(this).addClass("selected");

			$('.profile_picture div.radio span').removeClass("checked");
			$(this).parent().find("div.radio span").addClass("checked");

			$('.profile_picture div.radio input').removeAttr("checked");
			$(this).parent().find("div.radio span input").attr("checked", "checked");
		});


		$('.profile_picture div.radio span').click(function() {

			$('.profile_picture div.radio input').removeAttr("checked");
			$(this).find("input").attr("checked", "checked");

			$('.profile_picture img').removeClass("selected");
			$(this).parent().parent().find("img").addClass("selected");
		});


		$('.change_password').submit(function() {

			var password_regex = /(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/;

			var current_password = $('#current_password').val();
			var new_password = $('#new_password').val();
			var new_password2 = $('#new_password2').val();

			if(current_password == "" || new_password == "" || new_password2 == "") {

				$('#password_change_error .modal-body').html("<h3>Error</h3>Please fill out all the fields.");
				$('#password_change_error').modal("show");

				return false;

			} else if(new_password != new_password2) {

				$('#password_change_error .modal-body').html("<h3>Error</h3>The password fields do not match. Please re-enter them carefully.");
				$('#password_change_error').modal("show");

				return false;
			
			} else if(!password_regex.test(new_password)) {

                $('#password_change_error .modal-body').html("<h3>Error</h3>Your password is not strong enough. Choose a password with the following criteria:<br><br> - Minimum of 8 characters<br> - Contain at least 1 digit<br> - Contain uppercase & lowercase letters");
				$('#password_change_error').modal("show");

				return false;
            }

		});


		if(password_change_error == "wrong_password") {

			$('#password_change_error .modal-body').html("<h3>Error</h3>The password you provided did not match our records. Please try again.");
			$('#password_change_error').modal("show");
		
		} else if(password_change_error == "password_match_failed") {

			$('#password_change_error .modal-body').html("<h3>Error</h3>The password fields do not match. Please re-enter them carefully.");
			$('#password_change_error').modal("show");
		
		} else if(password_change_error == "insecure_password") {

			$('#password_change_error .modal-body').html("<h3>Error</h3>Your password is not strong enough. Choose a password with the following criteria:<br><br> - Minimum of 8 characters<br> - Contain at least 1 digit<br> - Contain uppercase & lowercase letters");
			$('#password_change_error').modal("show");
		
		} else if(password_change_error == "success") {

			$('#password_change_error .modal-body').html("<h3>Success</h3>Your password has been changed.");
			$('#password_change_error').modal("show");
		
		}


	}

	

	if(page == "calendar") {


		var date = new Date();
		var d = date.getDate();
		var m = date.getMonth();
		var y = date.getFullYear();



        if(hash == "add_new_event") {
			$('#add_event_modal').modal("show");
		}


        //$('.date-picker').datepicker();
        // $(".date-picker").datetimepicker({
        // 	format: 'MM d, yyyy',
        //     showMeridian: true,
        //     autoclose: true,
        //     pickerPosition: (App.isRTL() ? "bottom-left" : "bottom-right"),
        //     todayBtn: true,
        //     minView: 2,
        //     initialDate: new Date()
        // });

        // $(".time-picker").datetimepicker({
        // 	format: 'H:iiP',
        //     showMeridian: true,
        //     autoclose: true,
        //     pickerPosition: (App.isRTL() ? "bottom-left" : "bottom-right"),
        //     startView: 1,
        //     initialDate: new Date()
        // });


        $(".date-picker").datetimepicker({
        	format: 'F j, Y',
            closeOnDateSelect: true,
            timepicker: false,
            validateOnBlur: false
        });

        $(".time-picker").datetimepicker({
        	format: 'g:iA',
        	formatTime: 'g:iA',
        	step: 15,
            closeOnDateSelect: true,
            datepicker: false,
            validateOnBlur: false
        });


        var start_date, start_time, start_datetime, start_timestamp;
        var end_date, end_time, end_datetime, end_timestamp;

        var allDay = false;
        //allDay is used by fullCalendar to determine if the event object should be set as a ful day event or not. true or false.

        //Already have a month declared on top
        //var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];


        $('#add_event_modal #start_date').change(function() {

        	check_start_date();
        });

        $('#add_event_modal #start_time').change(function() {
			
			check_start_date();
        });


        function check_start_date() {

        	start_date = $('#add_event_modal #start_date').val();
        	
        	start_time = $('#add_event_modal #start_time').val();
        	start_time = start_time.trim();

        	//If time is NOT empty, then process it and add it to datetime. Otherwise datetime = date only.
        	if(start_time != "") {
	        	start_ampm = start_time.substring(start_time.length - 2, start_time.length);
	        	start_time = start_time.substring(0, start_time.length - 2);

	        	start_temp = start_time.split(":");
	        	start_hour = start_temp[0];
	        	start_min = start_temp[1];

	        	
	        	if(start_ampm.toUpperCase() == "PM" && start_hour < 12) {
	        		start_hour = parseInt(start_hour) + 12;
	        	}

	    		if(start_ampm.toUpperCase() == "AM" && start_hour == 12) {
	    			start_hour = parseInt(start_hour) - 12;
	    		}

	    		if(parseInt(start_hour) > 23 || parseInt(start_hour) < 0 || parseInt(start_min) > 59 || parseInt(start_min) < 0) {
	        		$('#add_event_modal #start_time').addClass("error");
	        		$('#add_event_modal #start_time').focus();
	        	
	        	} else {
	        		start_time = start_hour + ':' + start_min;

	        		start_datetime = start_date + ' ' + start_time;
		        	start_datetime = new Date(Date.parse(start_datetime));
		        	start_timestamp = new Date(start_datetime).getTime();

	        	}

	    	} else {
        	
	        	start_datetime = start_date;
	        	start_datetime = new Date(Date.parse(start_datetime));
	        	start_timestamp = new Date(start_datetime).getTime();
	        }

        }


		$('#add_event_modal .duration select').change(function() {

			$('#add_event_modal #start_time').removeAttr('disabled');
	        $('#add_event_modal #end_time').removeAttr('disabled');

        	var duration = $(this).find('option:selected').val();

        	if(duration == "all_day") {

        		$('#add_event_modal #start_time').attr('disabled', 'disabled');
        		$('#add_event_modal #end_time').attr('disabled', 'disabled');

        		$('#add_event_modal .end_date_time').fadeOut('fast');

        		allDay = true;

			} else if(duration == "custom") {

				allDay = false;

				$('#add_event_modal .end_date_time').fadeIn('fast');

				/*var end_ampm = "AM";

	        	end_timestamp = start_timestamp;

	        	end_datetime = new Date(end_timestamp);
	        	
	        	end_date = months[end_datetime.getMonth()] + ' ' + end_datetime.getDate() + ', ' + end_datetime.getFullYear();

	        	end_hour = end_datetime.getHours();
	        	end_min = end_datetime.getMinutes();

			    if (end_hour >= 12) {
			        end_hour = end_hour - 12;
			        end_ampm = "PM";
			    }
			    if (end_hour == 0) {
			        end_hour = 12;
			        //Stays as AM.
			    }


			    if (end_min < 10) {
					end_min = "0" + end_min;
				}



	        	end_time = end_hour + ':' + end_min + end_ampm;

	        	$('#add_event_modal #end_date').val(end_date);
	        	$('#add_event_modal #end_time').val(end_time);*/
			
			} else {

				allDay = false;

				$('#add_event_modal .end_date_time').fadeOut('fast');

	        	end_timestamp = start_timestamp + (duration * 60 * 1000);

	        	end_datetime = new Date(end_timestamp);
			}

        });




		$('body').on("fadeInComplete", function(){


			if (App.isRTL()) {
	             if ($('.calendar #calendar').parents(".portlet").width() <= 720) {
	                $('.calendar #calendar').addClass("mobile");
	                h = {
	                    right: 'title, prev, next',
	                    center: '',
	                    right: 'agendaDay, agendaWeek, month, today'
	                };
	            } else {
	                $('.calendar #calendar').removeClass("mobile");
	                h = {
	                    right: 'title',
	                    center: '',
	                    left: 'agendaDay, agendaWeek, month, today, prev,next'
	                };
	            }                
	        } else {
	             if ($('.calendar #calendar').parents(".portlet").width() <= 720) {
	                $('.calendar #calendar').addClass("mobile");
	                h = {
	                    left: 'title, prev, next',
	                    center: '',
	                    right: 'today,month,agendaWeek,agendaDay'
	                };
	            } else {
	                $('.calendar #calendar').removeClass("mobile");
	                h = {
	                    left: 'title',
	                    center: '',
	                    right: 'prev,next,today,month,agendaWeek,agendaDay'
	                };
	            }
	        }



			var current_event;
			var calendar = $('.calendar #calendar').fullCalendar({
				header: h,
	            slotMinutes: 15,
	            timeFormat: 'h:mmTT{ - h:mmTT}',
				editable: true,
				viewDisplay: resizeCalendar,
				events: "../load_calendar",
				selectable: true,
				selectHelper: true,
				select: function(start, end, allDay) {

					//if all_day is true, it means the user clicked on a DAY box, not a timeslot. This also means the start and end datetime are the same.

					$('#add_event_modal form')[0].reset();

					if(allDay == true) { //the datetimes are the same, probably a day box click.

						var start_ampm = "AM";

						start_datetime = new Date(start);
			        	
			        	var start_date = months[start_datetime.getMonth()] + ' ' + start_datetime.getDate() + ', ' + start_datetime.getFullYear();

			        	var start_hour = start_datetime.getHours();
			        	var start_min = start_datetime.getMinutes();

					    if (start_hour >= 12) {
					        start_hour = start_hour - 12;
					        start_ampm = "PM";
					    }
					    if (start_hour == 0) {
					        start_hour = 12;
					        //Stays as AM.
					    }

					    if (start_min < 10) {
							start_min = "0" + start_min;
						}

			        	var start_time = start_hour + ':' + start_min + start_ampm;

			        	$('#add_event_modal #start_date').val(start_date);
			        	$('#add_event_modal #start_time').val(start_time);


			        	check_start_date();
			        	$('#add_event_modal .duration select').val('all_day');
			        	$('#add_event_modal .duration select').trigger('change');

			        } else { //the datimes are different, must be a timeslot click. Handle end datetime as well.

			        	var start_ampm = "AM";

						start_datetime = new Date(start);
			        	
			        	var start_date = months[start_datetime.getMonth()] + ' ' + start_datetime.getDate() + ', ' + start_datetime.getFullYear();

			        	var start_hour = start_datetime.getHours();
			        	var start_min = start_datetime.getMinutes();

					    if (start_hour >= 12) {
					        start_hour = start_hour - 12;
					        start_ampm = "PM";
					    }
					    if (start_hour == 0) {
					        start_hour = 12;
					        //Stays as AM.
					    }

					    if (start_min < 10) {
							start_min = "0" + start_min;
						}

			        	var start_time = start_hour + ':' + start_min + start_ampm;

			        	$('#add_event_modal #start_date').val(start_date);
			        	$('#add_event_modal #start_time').val(start_time);

			        	// --------------------

			        	var end_ampm = "AM";

						end_datetime = new Date(end);
			        	
			        	var end_date = months[end_datetime.getMonth()] + ' ' + end_datetime.getDate() + ', ' + end_datetime.getFullYear();

			        	var end_hour = end_datetime.getHours();
			        	var end_min = end_datetime.getMinutes();

					    if (end_hour >= 12) {
					        end_hour = end_hour - 12;
					        end_ampm = "PM";
					    }
					    if (end_hour == 0) {
					        end_hour = 12;
					        //Stays as AM.
					    }

					    if (end_min < 10) {
							end_min = "0" + end_min;
						}

			        	var end_time = end_hour + ':' + end_min + end_ampm;

			        	$('#add_event_modal #end_date').val(end_date);
			        	$('#add_event_modal #end_time').val(end_time);

			        	check_start_date();
			        	$('#add_event_modal .duration select').val('custom');
			        	$('#add_event_modal .duration select').trigger('change');

			        }

					
					$('#show_add_event_modal').trigger('click');

				},

				editable: true,
				eventDrop: function(event, delta) {

					current_event = event;

					updateEvent();

				},

				eventResize: function(event) {

					current_event = event;

					updateEvent();

				},

				eventClick: function(event) {

					current_event = event;

					$('#edit_event_modal form')[0].reset();

					var event_start_datetime, start_date, start_time;
					var event_end_datetime, end_date, end_time;

					event_start_datetime = event.start;

					if($(event_start_datetime).length) {

						var start_ampm = "AM";

						var start_date = months[event_start_datetime.getMonth()] + ' ' + event_start_datetime.getDate() + ', ' + event_start_datetime.getFullYear();

			        	var start_hour = event_start_datetime.getHours();
			        	var start_min = event_start_datetime.getMinutes();

					    if (start_hour >= 12) {
					        start_hour = start_hour - 12;
					        start_ampm = "PM";
					    }
					    if (start_hour == 0) {
					        start_hour = 12;
					        //Stays as AM.
					    }

					    if (start_min < 10) {
							start_min = "0" + start_min;
						}

			        	var start_time = start_hour + ':' + start_min + start_ampm;
			        }

		        	// ---------------------

		        	//If the event_end_datetime is the SAME as the start_endtime, fullCalendar treats the event as a full-day event and thus eliminates the end datetime altogether. Stupid, but that's just the way it is.
		        	event_end_datetime = event.end;

		        	if($(event_end_datetime).length) { 

		        		var end_ampm = "AM";

						var end_date = months[event_end_datetime.getMonth()] + ' ' + event_end_datetime.getDate() + ', ' + event_end_datetime.getFullYear();

			        	var end_hour = event_end_datetime.getHours();
			        	var end_min = event_end_datetime.getMinutes();

					    if (end_hour >= 12) {
					        end_hour = end_hour - 12;
					        end_ampm = "PM";
					    }
					    if (end_hour == 0) {
					        end_hour = 12;
					        //Stays as AM.
					    }

					    if (end_min < 10) {
							end_min = "0" + end_min;
						}

			        	var end_time = end_hour + ':' + end_min + end_ampm;
			        }


					$('#edit_event_modal #id').val(event.id);
					$('#edit_event_modal #title').val(event.title);
					$('#edit_event_modal #desc').val(event.desc);
					$('#edit_event_modal #start_date').val(start_date);
					$('#edit_event_modal #start_time').val(start_time);
					$('#edit_event_modal #end_date').val(end_date);
					$('#edit_event_modal #end_time').val(end_time);

					var allDay = event.allDay;
					var notify = event.notify;
					var notification_time = event.notification_time;

					if(allDay == "true") {

						$('#edit_event_modal #duration').val("all_day");
					} else {

						$('#edit_event_modal .duration').hide();
						$('#edit_event_modal .end_date_time').show();
					}

					if(notify == 1) {
						$('#edit_event_modal #reminder').val(notification_time);
					}

			        $('#show_edit_event_modal').trigger('click');
			    },

			    eventDestroy: function(event) {

			    }

			});



			$('.add_event').click(function() {

				if(end_datetime == "") {

					alert("Please specify a duration of End time for the event");
				} else { 

					var id; //returned back once row is added to DB
					var title = $('#add_event_modal #title').val();
					var desc = $('#add_event_modal #desc').val();
					var start = start_datetime;
					var end = end_datetime;
					var reminder = $('#add_event_modal #reminder').val();

					if(title) { 
						// start = new Date(start);
						// end = new Date(end);

						start = $.fullCalendar.formatDate(start, "yyyy-MM-dd HH:mm:ss");
						end = $.fullCalendar.formatDate(end, "yyyy-MM-dd HH:mm:ss");

						if(reminder >= 0) {

							var notify = 1;
							var notification_time = reminder;
						
						} else {

							var notify = 0;
							var notification_time = -1;
						}	


						$.ajax({

					        url: 'ajax_functions',  //server script to process data
					        type: 'POST',
					        data: {
					        	action: "add_event",
					        	title: title,
					        	desc: desc,
					        	start: start,
					        	end: end,
					        	allDay: allDay,
					        	notify: notify,
					        	notification_time: notification_time
					        }

					    }).done(function(data) {

					    	//data returns the last row id form DB.
					    	id = data;

					    	calendar.fullCalendar('renderEvent',
							{	
								id: id,
								title: title,
								desc: desc,
								start: start,
								end: end,
								allDay: allDay,
								notify: notify,
								notification_time: notification_time
							},
								true // make the event "stick"
							);

							current_event = calendar.fullCalendar( 'clientEvents', id);

							$('#add_event_modal #title').val('');
							$('#add_event_modal #desc').val('');
							$('#edit_event_modal #start_date').val('');
							$('#edit_event_modal #start_time').val('');
							$('#edit_event_modal #end_date').val('');
							$('#edit_event_modal #end_time').val('');
							
							title = '';
							desc = '';
							start = '';
							end = '';
							allDay = '';
							notify = '';
							notification_time = '';

					    });
					}
					calendar.fullCalendar('unselect');
				}
			});




			$('#edit_event_modal .duration select').change(function() {

				$('#edit_event_modal #start_time').removeAttr('disabled');
		        $('#edit_event_modal #end_time').removeAttr('disabled');

	        	var duration = $(this).find('option:selected').val();

	        	if(duration == "all_day") {

	        		$('#edit_event_modal #start_time').attr('disabled', 'disabled');
	        		$('#edit_event_modal #end_time').attr('disabled', 'disabled');

	        		$('#edit_event_modal .end_date_time').fadeOut('fast');

	        		allDay = true;

				} else if(duration == "custom") {

					allDay = false;

					$('#edit_event_modal .end_date_time').fadeIn('fast');
				
				} else {

					allDay = false;

					$('#edit_event_modal .end_date_time').fadeOut('fast');

		        	end_timestamp = start_timestamp + (duration * 60 * 1000);

		        	end_datetime = new Date(end_timestamp);

				}

	        });


			$('.edit_event').click(function() {

				current_event.title = $('#edit_event_modal #title').val();
				current_event.desc = $('#edit_event_modal #desc').val();
				current_event.start = start_datetime;
				current_event.end = end_datetime;
				//add notify, notification_time, allDay


				calendar.fullCalendar('updateEvent', current_event);

				updateEvent();

			});


			function updateEvent() {

				var start = $.fullCalendar.formatDate(current_event.start, "yyyy-MM-dd HH:mm:ss");
				var end = $.fullCalendar.formatDate(current_event.end, "yyyy-MM-dd HH:mm:ss");

				$.ajax({

			        url: 'ajax_functions',  //server script to process data
			        type: 'POST',
			        data: {
			        	action: "update_event",
			        	id: current_event.id,
			        	title: current_event.title,
			        	desc: current_event.desc,
			        	start: start,
			        	end: end,
			        	allDay: current_event.allDay,
			        	notify: current_event.notify,
			        	notification_time: current_event.notification_time
			        }

			    }).done(function(data) {

			    	$('#add_event_modal #title').val('');
					$('#add_event_modal #desc').val('');
					$('#edit_event_modal #start_date').val('');
					$('#edit_event_modal #start_time').val('');
					$('#edit_event_modal #end_date').val('');
					$('#edit_event_modal #end_time').val('');
					
					title = '';
					desc = '';
					start = '';
					end = '';
					allDay = '';
					notify = '';
					notification_time = '';
			    });

			}



			$('#delete_event_modal .delete').click(function() {

				calendar.fullCalendar('removeEvents', current_event.id);

				$.ajax({

			        url: 'ajax_functions',  //server script to process data
			        type: 'POST',
			        data: {
			        	action: "delete_event",
			        	id: current_event.id,
			        }

			    }).done(function(data) {
			    	//done
			    });
			});


		});

		$(window).resize(function() {

			resizeCalendar();
		});


		function resizeCalendar() {
			var currentView = $('.calendar #calendar').fullCalendar('getView');
			var height = $(window).height();

			currentView.setHeight(height - 190);
		}

	}




	if(page == "work_schedule") {


		var date = new Date();
		var d = date.getDate();
		var m = date.getMonth();
		var y = date.getFullYear();



        if(hash == "add_new_event") {
			$('#add_event_modal').modal("show");
		}


        //$('.date-picker').datepicker();
        // $(".date-picker").datetimepicker({
        // 	format: 'MM d, yyyy',
        //     showMeridian: true,
        //     autoclose: true,
        //     pickerPosition: (App.isRTL() ? "bottom-left" : "bottom-right"),
        //     todayBtn: true,
        //     minView: 2,
        //     initialDate: new Date()
        // });

        // $(".time-picker").datetimepicker({
        // 	format: 'H:iiP',
        //     showMeridian: true,
        //     autoclose: true,
        //     pickerPosition: (App.isRTL() ? "bottom-left" : "bottom-right"),
        //     startView: 1,
        //     initialDate: new Date()
        // });


        $(".date-picker").datetimepicker({
        	format: 'F j, Y',
            closeOnDateSelect: true,
            timepicker: false,
            validateOnBlur: false
        });

        $(".time-picker").datetimepicker({
        	format: 'g:iA',
        	formatTime: 'g:iA',
        	step: 15,
            closeOnDateSelect: true,
            datepicker: false,
            validateOnBlur: false
        });


        var start_date, start_time, start_datetime, start_timestamp;
        var end_date, end_time, end_datetime, end_timestamp;

        var allDay = false;
        //allDay is used by fullCalendar to determine if the event object should be set as a ful day event or not. true or false.

        //Already have a month declared on top
        //var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];


        $('#add_event_modal #start_date').change(function() {

        	check_start_date();
        });

        $('#add_event_modal #start_time').change(function() {
			
			check_start_date();
        });


        function check_start_date() {

        	start_date = $('#add_event_modal #start_date').val();
        	
        	start_time = $('#add_event_modal #start_time').val();
        	start_time = start_time.trim();

        	//If time is NOT empty, then process it and add it to datetime. Otherwise datetime = date only.
        	if(start_time != "") {
	        	start_ampm = start_time.substring(start_time.length - 2, start_time.length);
	        	start_time = start_time.substring(0, start_time.length - 2);

	        	start_temp = start_time.split(":");
	        	start_hour = start_temp[0];
	        	start_min = start_temp[1];

	        	
	        	if(start_ampm.toUpperCase() == "PM" && start_hour < 12) {
	        		start_hour = parseInt(start_hour) + 12;
	        	}

	    		if(start_ampm.toUpperCase() == "AM" && start_hour == 12) {
	    			start_hour = parseInt(start_hour) - 12;
	    		}

	    		if(parseInt(start_hour) > 23 || parseInt(start_hour) < 0 || parseInt(start_min) > 59 || parseInt(start_min) < 0) {
	        		$('#add_event_modal #start_time').addClass("error");
	        		$('#add_event_modal #start_time').focus();
	        	
	        	} else {
	        		start_time = start_hour + ':' + start_min;

	        		start_datetime = start_date + ' ' + start_time;
		        	start_datetime = new Date(Date.parse(start_datetime));
		        	start_timestamp = new Date(start_datetime).getTime();

	        	}

	    	} else {
        	
	        	start_datetime = start_date;
	        	start_datetime = new Date(Date.parse(start_datetime));
	        	start_timestamp = new Date(start_datetime).getTime();
	        }

        }


		$('#add_event_modal .duration select').change(function() {

			$('#add_event_modal #start_time').removeAttr('disabled');
	        $('#add_event_modal #end_time').removeAttr('disabled');

        	var duration = $(this).find('option:selected').val();

        	if(duration == "all_day") {

        		$('#add_event_modal #start_time').attr('disabled', 'disabled');
        		$('#add_event_modal #end_time').attr('disabled', 'disabled');

        		$('#add_event_modal .end_date_time').fadeOut('fast');

        		allDay = true;

			} else if(duration == "custom") {

				allDay = false;

				$('#add_event_modal .end_date_time').fadeIn('fast');

				/*var end_ampm = "AM";

	        	end_timestamp = start_timestamp;

	        	end_datetime = new Date(end_timestamp);
	        	
	        	end_date = months[end_datetime.getMonth()] + ' ' + end_datetime.getDate() + ', ' + end_datetime.getFullYear();

	        	end_hour = end_datetime.getHours();
	        	end_min = end_datetime.getMinutes();

			    if (end_hour >= 12) {
			        end_hour = end_hour - 12;
			        end_ampm = "PM";
			    }
			    if (end_hour == 0) {
			        end_hour = 12;
			        //Stays as AM.
			    }


			    if (end_min < 10) {
					end_min = "0" + end_min;
				}



	        	end_time = end_hour + ':' + end_min + end_ampm;

	        	$('#add_event_modal #end_date').val(end_date);
	        	$('#add_event_modal #end_time').val(end_time);*/
			
			} else {

				allDay = false;

				$('#add_event_modal .end_date_time').fadeOut('fast');

	        	end_timestamp = start_timestamp + (duration * 60 * 1000);

	        	end_datetime = new Date(end_timestamp);
			}

        });




		$('body').on("fadeInComplete", function(){


			if (App.isRTL()) {
	             if ($('.calendar #work_schedule').parents(".portlet").width() <= 720) {
	                $('.calendar #work_schedule').addClass("mobile");
	                h = {
	                    right: 'title, prev, next',
	                    center: '',
	                    right: 'agendaDay, agendaWeek, month, today'
	                };
	            } else {
	                $('.calendar #work_schedule').removeClass("mobile");
	                h = {
	                    right: 'title',
	                    center: '',
	                    left: 'agendaDay, agendaWeek, month, today, prev,next'
	                };
	            }                
	        } else {
	             if ($('.calendar #work_schedule').parents(".portlet").width() <= 720) {
	                $('.calendar #work_schedule').addClass("mobile");
	                h = {
	                    left: 'title, prev, next',
	                    center: '',
	                    right: 'today,month,agendaWeek,agendaDay'
	                };
	            } else {
	                $('.calendar #work_schedule').removeClass("mobile");
	                h = {
	                    left: 'title',
	                    center: '',
	                    right: 'prev,next,today,month,agendaWeek,agendaDay'
	                };
	            }
	        }



			var current_event;
			var calendar = $('.calendar #work_schedule').fullCalendar({
				header: h,
	            slotMinutes: 15,
	            timeFormat: 'h:mmTT{ - h:mmTT}',
				editable: true,
				viewDisplay: resizeWorkScheduleCalendar,
				events: "../load_work_schedule",
				selectable: true,
				selectHelper: true,
				select: function(start, end, allDay) {

					//if all_day is true, it means the user clicked on a DAY box, not a timeslot. This also means the start and end datetime are the same.

					$('#add_event_modal form')[0].reset();


		        	var start_ampm = "AM";

					start_datetime = new Date(start);
		        	
		        	var start_date = months[start_datetime.getMonth()] + ' ' + start_datetime.getDate() + ', ' + start_datetime.getFullYear();

		        	var start_hour = start_datetime.getHours();
		        	var start_min = start_datetime.getMinutes();

				    if (start_hour >= 12) {
				        start_hour = start_hour - 12;
				        start_ampm = "PM";
				    }
				    if (start_hour == 0) {
				        start_hour = 12;
				        //Stays as AM.
				    }

				    if (start_min < 10) {
						start_min = "0" + start_min;
					}

		        	var start_time = start_hour + ':' + start_min + start_ampm;

		        	$('#add_event_modal #start_date').val(start_date);
		        	$('#add_event_modal #start_time').val(start_time);

		        	// --------------------

		        	var end_ampm = "AM";

					end_datetime = new Date(end);
		        	
		        	var end_date = months[end_datetime.getMonth()] + ' ' + end_datetime.getDate() + ', ' + end_datetime.getFullYear();

		        	var end_hour = end_datetime.getHours();
		        	var end_min = end_datetime.getMinutes();

				    if (end_hour >= 12) {
				        end_hour = end_hour - 12;
				        end_ampm = "PM";
				    }
				    if (end_hour == 0) {
				        end_hour = 12;
				        //Stays as AM.
				    }

				    if (end_min < 10) {
						end_min = "0" + end_min;
					}

		        	var end_time = end_hour + ':' + end_min + end_ampm;

		        	$('#add_event_modal #end_date').val(end_date);
		        	$('#add_event_modal #end_time').val(end_time);

		        	check_start_date();
		        	$('#add_event_modal .duration select').val('custom');
		        	$('#add_event_modal .duration select').trigger('change');

					
					$('#show_add_event_modal').trigger('click');

				},

				editable: true,
				eventDrop: function(event, delta) {

					current_event = event;

					updateEvent();

				},

				eventResize: function(event) {

					current_event = event;

					updateEvent();

				},

				eventClick: function(event) {

					current_event = event;

					$('#edit_event_modal form')[0].reset();

					var event_start_datetime, start_date, start_time;
					var event_end_datetime, end_date, end_time;

					event_start_datetime = event.start;

					if($(event_start_datetime).length) {

						var start_ampm = "AM";

						var start_date = months[event_start_datetime.getMonth()] + ' ' + event_start_datetime.getDate() + ', ' + event_start_datetime.getFullYear();

			        	var start_hour = event_start_datetime.getHours();
			        	var start_min = event_start_datetime.getMinutes();

					    if (start_hour >= 12) {
					        start_hour = start_hour - 12;
					        start_ampm = "PM";
					    }
					    if (start_hour == 0) {
					        start_hour = 12;
					        //Stays as AM.
					    }

					    if (start_min < 10) {
							start_min = "0" + start_min;
						}

			        	var start_time = start_hour + ':' + start_min + start_ampm;
			        }

		        	// ---------------------

		        	//If the event_end_datetime is the SAME as the start_endtime, fullCalendar treats the event as a full-day event and thus eliminates the end datetime altogether. Stupid, but that's just the way it is.
		        	event_end_datetime = event.end;

		        	if($(event_end_datetime).length) { 

		        		var end_ampm = "AM";

						var end_date = months[event_end_datetime.getMonth()] + ' ' + event_end_datetime.getDate() + ', ' + event_end_datetime.getFullYear();

			        	var end_hour = event_end_datetime.getHours();
			        	var end_min = event_end_datetime.getMinutes();

					    if (end_hour >= 12) {
					        end_hour = end_hour - 12;
					        end_ampm = "PM";
					    }
					    if (end_hour == 0) {
					        end_hour = 12;
					        //Stays as AM.
					    }

					    if (end_min < 10) {
							end_min = "0" + end_min;
						}

			        	var end_time = end_hour + ':' + end_min + end_ampm;
			        }


					$('#edit_event_modal #id').val(event.id);
					//$('#edit_event_modal #title').val(event.title);
					//$('#edit_event_modal #desc').val(event.desc);
					$('#edit_event_modal #start_date').val(start_date);
					$('#edit_event_modal #start_time').val(start_time);
					$('#edit_event_modal #end_date').val(end_date);
					$('#edit_event_modal #end_time').val(end_time);

					//var allDay = event.allDay;
					//var notify = event.notify;
					//var notification_time = event.notification_time;


					$('#edit_event_modal .duration').hide();
					$('#edit_event_modal .end_date_time').show();


			        $('#show_edit_event_modal').trigger('click');
			    },

			    eventDestroy: function(event) {

			    }

			});



			$('.add_shift').click(function() {

				if(end_datetime == "") {

					alert("Please specify a duration of End time for the event");
				} else { 

					var id; //returned back once row is added to DB
					var title = $('#add_event_modal #title').val();
					var desc = $('#add_event_modal #desc').val();
					var start = start_datetime;
					var end = end_datetime;
					var reminder = $('#add_event_modal #reminder').val();

					if(title) { 
						// start = new Date(start);
						// end = new Date(end);

						start = $.fullCalendar.formatDate(start, "yyyy-MM-dd HH:mm:ss");
						end = $.fullCalendar.formatDate(end, "yyyy-MM-dd HH:mm:ss");

						if(reminder >= 0) {

							var notify = 1;
							var notification_time = reminder;
						
						} else {

							var notify = 0;
							var notification_time = -1;
						}	


						$.ajax({

					        url: 'ajax_functions',  //server script to process data
					        type: 'POST',
					        data: {
					        	action: "add_event",
					        	title: title,
					        	desc: desc,
					        	start: start,
					        	end: end,
					        	allDay: allDay,
					        	notify: notify,
					        	notification_time: notification_time
					        }

					    }).done(function(data) {

					    	//data returns the last row id form DB.
					    	id = data;

					    	calendar.fullCalendar('renderEvent',
							{	
								id: id,
								title: title,
								desc: desc,
								start: start,
								end: end,
								allDay: allDay,
								notify: notify,
								notification_time: notification_time
							},
								true // make the event "stick"
							);

							current_event = calendar.fullCalendar( 'clientEvents', id);

							$('#add_event_modal #title').val('');
							$('#add_event_modal #desc').val('');
							$('#edit_event_modal #start_date').val('');
							$('#edit_event_modal #start_time').val('');
							$('#edit_event_modal #end_date').val('');
							$('#edit_event_modal #end_time').val('');
							
							title = '';
							desc = '';
							start = '';
							end = '';
							allDay = '';
							notify = '';
							notification_time = '';

					    });
					}
					calendar.fullCalendar('unselect');
				}
			});




			$('#edit_event_modal .duration select').change(function() {

				$('#edit_event_modal #start_time').removeAttr('disabled');
		        $('#edit_event_modal #end_time').removeAttr('disabled');

	        	var duration = $(this).find('option:selected').val();

	        	if(duration == "all_day") {

	        		$('#edit_event_modal #start_time').attr('disabled', 'disabled');
	        		$('#edit_event_modal #end_time').attr('disabled', 'disabled');

	        		$('#edit_event_modal .end_date_time').fadeOut('fast');

	        		allDay = true;

				} else if(duration == "custom") {

					allDay = false;

					$('#edit_event_modal .end_date_time').fadeIn('fast');
				
				} else {

					allDay = false;

					$('#edit_event_modal .end_date_time').fadeOut('fast');

		        	end_timestamp = start_timestamp + (duration * 60 * 1000);

		        	end_datetime = new Date(end_timestamp);

				}

	        });


			$('.edit_shift').click(function() {

				current_event.title = $('#edit_event_modal #title').val();
				current_event.desc = $('#edit_event_modal #desc').val();
				current_event.start = start_datetime;
				current_event.end = end_datetime;
				//add notify, notification_time, allDay


				calendar.fullCalendar('updateEvent', current_event);

				updateEvent();

			});


			function updateEvent() {

				var start = $.fullCalendar.formatDate(current_event.start, "yyyy-MM-dd HH:mm:ss");
				var end = $.fullCalendar.formatDate(current_event.end, "yyyy-MM-dd HH:mm:ss");

				$.ajax({

			        url: 'ajax_functions',  //server script to process data
			        type: 'POST',
			        data: {
			        	action: "update_event",
			        	id: current_event.id,
			        	title: current_event.title,
			        	desc: current_event.desc,
			        	start: start,
			        	end: end,
			        	allDay: current_event.allDay,
			        	notify: current_event.notify,
			        	notification_time: current_event.notification_time
			        }

			    }).done(function(data) {

			    	$('#add_event_modal #title').val('');
					$('#add_event_modal #desc').val('');
					$('#edit_event_modal #start_date').val('');
					$('#edit_event_modal #start_time').val('');
					$('#edit_event_modal #end_date').val('');
					$('#edit_event_modal #end_time').val('');
					
					title = '';
					desc = '';
					start = '';
					end = '';
					allDay = '';
					notify = '';
					notification_time = '';
			    });

			}



			$('#delete_event_modal .delete').click(function() {

				calendar.fullCalendar('removeEvents', current_event.id);

				$.ajax({

			        url: 'ajax_functions',  //server script to process data
			        type: 'POST',
			        data: {
			        	action: "delete_event",
			        	id: current_event.id,
			        }

			    }).done(function(data) {
			    	//done
			    });
			});


		});

		$(window).resize(function() {

			resizeWorkScheduleCalendar();
		});


		function resizeWorkScheduleCalendar() {
			var currentView = $('.calendar #work_schedule').fullCalendar('getView');
			var height = $(window).height();

			currentView.setHeight(height - 190);
		}

	}




	if(page == "inventory") {


		if(hash == "add_new_item") {
			$('#add_new_item_modal').modal("show");
		}


		if($_GET['e'] == "item_not_found") {

			$('#item_not_found_modal').modal("show");
		}


		$('body').on("click", ".edit_category", function() {

			var cat_id = $.trim($(this).parent().parent().find("a:first-child").attr('id'));
			var cat_name = $.trim($(this).parent().parent().find("a:first-child").text());

			$('.cat_name').val(cat_name);
			$('.cat_id').val(cat_id);

		});


		//$('.categories_list li').hover(function() { 
		$('body').on("mouseenter", ".categories_list li", function() {

			$(this).find('.edit_category').attr('style', 'display: block !important');

		});
		$('body').on("mouseleave", ".categories_list li", function() {

			$(this).find('.edit_category').attr('style', 'display: none !important');

		});


		$('body').on('click', '.categories_list li a:first-child', function() {

			var cat_name = $.trim($(this).text());

			var cat_id = $(this).attr("id");

			if(cat_id == "-1") {
				
				History.pushState(null, null, page);

				$('.current_cat').val("");
			
			} else {
			
				History.pushState(null, null, page + "?cat_id=" + cat_id);

				$('.current_cat').val(cat_id);
			}

			$('.products').fadeOut('fast', function() {

				$.ajax({

			        url: 'ajax_functions',  //server script to process data
			        type: 'POST',
			        data: {
			        	action: "load_products",
			        	cat_id: cat_id
			        }

			    }).done(function(data) {
			    	if(cat_name == "All Categories") {

			    		$('.products').parent().find(".caption").html('<i class="icon-edit"></i> Products – All');
			    	} else {

			    		$('.products').parent().find(".caption").html('<i class="icon-edit"></i> Products – ' + cat_name);
			    	}

			    	$('.products').html(data);
			    	$('.products').fadeIn('fast');
			    });

			});


			//event.preventDefault();
			//window.location.hash = this.hash;

		});


		$('#edit_category_modal .save').click(function() {

			$('#edit_category_modal').modal("hide");

			$('.loading_large').fadeIn('fast');

			var cat_id =  $.trim($('#edit_category_modal .cat_id').val());
			var cat_name =  $.trim($('#edit_category_modal .cat_name').val());

			$.ajax({

		        url: 'ajax_functions',  //server script to process data
		        type: 'POST',
		        data: {
		        	action: "update_category",
		        	cat_id: cat_id,
		        	cat_name: cat_name
		        }

		    }).done(function(data) {
		    	
		    	$('#edit_category_modal .cat_id').val("");
				$('#edit_category_modal .cat_name').val("");

		    	$('.categories_list li a#' + cat_id).html('<i class="icon-sitemap"></i> ' + cat_name);

		    	$('.loading_large').fadeOut('fast');

		    });

			return false;

		})



		$('#delete_category_modal .delete').click(function() {

			$('#delete_category_modal').modal("hide");

			$('.loading_large').fadeIn('fast');

			var cat_id =  $.trim($('#delete_category_modal .cat_id').val());

			$.ajax({

		        url: 'ajax_functions',  //server script to process data
		        type: 'POST',
		        data: {
		        	action: "delete_category",
		        	cat_id: cat_id
		        }

		    }).done(function(data) {
		    	
		    	$('#edit_category_modal .cat_id').val("");
				$('#edit_category_modal .cat_name').val("");
		    	$('#delete_category_modal .cat_id').val("");

		    	$('.categories_list li a#' + cat_id).parent().remove();

		    	$('.loading_large').fadeOut('fast');

		    });

			return false;

		});


		$('#add_new_category_modal input').focus(function() {

			$(this).removeClass("error");
		});

		$('#add_new_category_modal .add').click(function() {

			addCategory();

			return false;

		})


		$('#add_new_category_modal #cat_name').keydown(function(e) {

			var code = (e.keyCode ? e.keyCode : e.which);

			if (code == 13) {

				e.preventDefault();

				addCategory();

				return false;

			}

			

		});


		function addCategory() {

			var cat_name =  $.trim($('#add_new_category_modal #cat_name').val());

			if(cat_name == "") {

				$('#add_new_category_modal #cat_name').addClass("error");
				
			} else {

				$('#add_new_category_modal #cat_name').removeClass("error");

				$('#add_new_category_modal').modal('hide');

				$('.loading_large').fadeIn('fast');

				$.ajax({

			        url: 'ajax_functions',  //server script to process data
			        type: 'POST',
			        data: {
			        	action: "add_category",
			        	cat_name: cat_name
			        }

			    }).done(function(data) {
			    	
			    	$('#add_new_category_modal #cat_name').val("");

			    	$('.categories_list').append(data);

			    	$('.loading_large').fadeOut('fast');

			    });

			}

			return false;

		}


		$('#search_categories').keyup(function(e) {

			var code = (e.keyCode ? e.keyCode : e.which);

			if (code == 13) {

				return false;

			} else {

				searchCategories($(this).val());

			}

		});


		function searchCategories(searchVal) {

			$('.categories_list li').each(function() {

				var found = false;

				var regExp = new RegExp(searchVal, 'i');

				if(regExp.test($(this).text())) {
					found = true;

				}


				if(found == true) {
					$(this).show();
				} else {
					$(this).hide();
				}

			});

		}



		$('body').on("click", ".add_new_item", function() {

			$.ajax({

		        url: 'ajax_functions',  //server script to process data
		        type: 'POST',
		        data: {
		        	action: "get_categories"
		        }

		    }).done(function(data) {

		    	var categories = data;
		    	
		    	$('#add_new_item_modal #category').html(categories);

				$('#add_new_item_modal #category option').removeAttr('selected');
				$('#add_new_item_modal #category option[id="-1"]').attr("selected", "selected");

				$('#add_new_item_modal #category option[value="' + category + '"]').attr("selected", "selected");

		    });

		});



		$('body').on("click", ".add_multiple_items", function() {

			$.ajax({

		        url: 'ajax_functions',  //server script to process data
		        type: 'POST',
		        data: {
		        	action: "get_categories"
		        }

		    }).done(function(data) {

		    	var categories = data;
		    	
		    	$('#add_multiple_items_modal #category').html(categories);

				$('#add_multiple_items_modal #category option').removeAttr('selected');
				$('#add_multiple_items_modal #category option[id="-1"]').attr("selected", "selected");

				$('#add_multiple_items_modal #category option[value="' + category + '"]').attr("selected", "selected");

		    });

		});



		$('body').on("click", ".edit_item", function() {

			$('#edit_item_modal #item_id').val("");
			$('#edit_item_modal #item_name').val("");
			$('#edit_item_modal #product_id').val("");
			$('#edit_item_modal #item_desc').val("");

			$('#edit_item_modal #item_qty').val("");
			$('#edit_item_modal #item_cost').val("");

			var item_id = $(this).closest("tr").find(".item_id").val();
			var product_id = $(this).closest("tr").find(".product_id").val();
			var item_name = $(this).closest("tr").find(".item_name").val();
			var item_desc = $(this).closest("tr").find(".item_desc").val();
			var category = $(this).closest("tr").find(".category").val();
			var item_qty = $(this).closest("tr").find(".item_qty").val();
			var item_cost = $(this).closest("tr").find(".item_cost").val();

			$('#edit_item_modal #item_id').val(item_id);
			$('#edit_item_modal #item_name').val(item_name);
			$('#edit_item_modal #product_id').val(product_id);
			$('#edit_item_modal #item_desc').val(item_desc);

			$('#edit_item_modal #item_qty').val(item_qty);
			$('#edit_item_modal #item_cost').val(item_cost);

			$.ajax({

		        url: 'ajax_functions',  //server script to process data
		        type: 'POST',
		        data: {
		        	action: "get_categories"
		        }

		    }).done(function(data) {

		    	var categories = data;
		    	
		    	$('#edit_item_modal #category').html(categories);

				$('#edit_item_modal #category option').removeAttr('selected');
				$('#edit_item_modal #category option[id="-1"]').attr("selected", "selected");

				$('#edit_item_modal #category option[value="' + category + '"]').attr("selected", "selected");

		    });

		});


		function getCategories() {

			$.ajax({

		        url: 'ajax_functions',  //server script to process data
		        type: 'POST',
		        data: {
		        	action: "get_categories"
		        }

		    }).done(function(data) {
		    	
		    	return data;

		    });

		}


		//$('.inventory tr td:not(:nth-child(6))').css("background", "red");
		$('body').on("click", ".inventory tr td:not(:nth-child(6))", function() {

			var id = $(this).closest("tr").attr("id");

			window.location.href = "product?id=" + id;
		});



		$('body').on("click", ".delete_item", function() {

			var item_id = $(this).closest("tr").attr("id");
			var item_name = $(this).closest("tr").find("td:first-child").text();

			$('#delete_item_modal .item_id').val(item_id);
			$('#delete_item_modal .modal-body').html('Are you sure you want to delete ' + item_name + ' from the inventory?<br><br><i class="icon-warning-sign"></i> This action is permanent and cannot be undone.<input type="hidden" class="item_id" name="item_id" class="m-wrap span12" value="' + item_id + '" />');
		
			$('#delete_item_modal .modal-footer').html('<input type="submit" id="submit" class="btn light-red right delete" value="Delete" /><input type="button" data-dismiss="modal" class="btn" value="Cancel" />');
		});


		$('body').on("click", "#delete_item_modal .delete", function() {

			var item_id = $('#delete_item_modal .item_id').val();

			$.ajax({

				type: "POST",
				url: "ajax_functions",
				data: { 
					action: "delete_item",
					item_id: item_id
				}

			}).done(function(result) {

				$('.inventory tr#' + item_id).remove();

				$('#delete_item_modal .modal-body').html("<i class='icon-ok'></i> The item has been removed.");

				$('#delete_item_modal .modal-footer').html('<input type="button" data-dismiss="modal" class="btn light-green right" value="Close" />');

			});

			return false;
		});


	}



	if(page == "messages") {


		if($_GET['conv_id']) {

			var convo_id = $_GET['conv_id'];

			$('.messages_table tr#' + convo_id).removeClass('unread');
			$('.messages_table tr#' + convo_id).find('.read_marker .unread').removeClass('unread').addClass('read');

			loadConversation(convo_id);
		
		} else if($_GET['u_id']) {

			var u_id = $_GET['u_id'];
			newConversation(u_id);

		} else if(hash == "new_message") {

			newConversation();
		} 


		$('#new_message').click(function() {

			newConversation();

    		return false;
		});



		$('body').on("click", ".messages_table tr:gt(0)" , function() {

			var convo_id = $(this).attr("id");

			$(this).removeClass('unread');
			$(this).find('.read_marker .unread').removeClass('unread').addClass('read');

			loadConversation(convo_id);

		});


		$('body').on("click", ".close_message", function() {

			$('#messages .chats').css('opacity', '0');

			$('.messages_list').removeClass('span4');
			$('.messages_list').addClass('span12');

    		$('.message_box').removeClass('span8');
    		$('.message_box').addClass('span0');

			$('.message_box').html('');

		});


		$('body').on("click", ".hide_convo", function(e) {

			//This prevents the TR click to trigger as well, which would then load the convo unnecessarily. BEAUTY!!
			e.stopPropagation();

			var parent_tr = $(this).parent().parent();

			var convo_id = $.trim($(parent_tr).attr('id'));
			

			$(parent_tr).css('opacity', '.4');

			$.ajax({

				type: "POST",
				url: "ajax_functions",
				data: { 
					action: "hide_conversation",
					convo_id: convo_id
				}

			}).done(function(data) {

				$(parent_tr).slideUp(300, function() {

					$(parent_tr).remove();
				});

				if($('.message_box .input-cont .convo_id').val() == convo_id) {

					$('.close_message').trigger('click');
				}  

			});

		});


		$('body').on("change", "#recipients" , function() {

			var recipients = $("#recipients").select2("val");

			if(recipients != "") {

				$('.new_message .send_message').removeClass('disabled');
			} else {

				$('.new_message .send_message').addClass('disabled');
			}

		});



		function newConversation(u_id) {

			$('.messages_list').removeClass('span12');
			$('.messages_list').addClass('span4');

    		$('.message_box').removeClass('span0');
    		$('.message_box').addClass('span8');


    		setTimeout(function() {

				$('.message_box').html('<div class="loading_message"><img src="img/loading.gif"></div>');
				$('.loading_message').width($('.message_box').width());

			}, 400);


    		$.ajax({

				type: "POST",
				url: "ajax_functions",
				data: { 
					action: "new_conversation",
					u_id: u_id
				}

			}).done(function(data) {

				$('.message_box').html(data);

				var height = $(window).height();
				$('.new_message .portlet-body').height(height - 120);


				$.ajax({

					type: "POST",
					url: "ajax_functions",
					data: { 
						action: "get_users_json"
					},
					dataType: 'json'

				}).done(function(users_data) {

					$("#recipients").select2({
						createSearchChoice: function() { return null; },
						tags: users_data
			        });

				});

			});
		}

	}





	if(page == "coming_soon") {

		$('.subscribe').click(function() {

			var email_regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
			var email = $('.email').val();

			if(email == "" || !email_regex.test(email)) {
                $('.email').css("border", "1px solid #f85e5e");
             
            } else {


            	$.ajax({

					type: "POST",
					url: "ajax_functions",
					data: { 
						action: "add_to_mailing_list", 
						email: email
					}

				}).done(function(result) {

					$('.email').val("");

					$('.mailing_list_alert').fadeIn();

					setTimeout(function(){
						$('.mailing_list_alert').fadeOut();
					},5000);

				});


            }

            return false;

		});


		$('.mailing_list').submit(function() {

			var email_regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
			var email = $('.email').val();

			if(email == "" || !email_regex.test(email)) {
                $('.email').css("border", "1px solid #f85e5e");
             
            } else {


            	$.ajax({

					type: "POST",
					url: "ajax_functions",
					data: { 
						action: "add_to_mailing_list", 
						email: email
					}

				}).done(function(result) {

					$('.email').val("");

					$('.mailing_list_alert').fadeIn();

					setTimeout(function(){
						$('.mailing_list_alert').fadeOut();
					},5000);

				});


            }

            return false;

		});

	}




	if(page == "statistics") {

		Charts.init();
		Charts.initCharts();
		//Charts.initPieCharts();

	}




	if(page == "account_settings") {



		if(hash != "") {

			if(hash == "business_profile") {

				$('.section').hide();
				$('.row-fluid.business_profile').fadeIn('fast');
			
			} else if(hash == "preferences") {

				$('.section').hide();
				$('.row-fluid.preferences').fadeIn('fast');
			
			} else if(hash == "user_accounts") {

				$('.section').hide();
				$('.row-fluid.user_accounts').fadeIn('fast');
			
			} else if(hash == "modules") {

				$('.section').hide();
				$('.row-fluid.modules').fadeIn('fast');
			
			} else if(hash == "billing") {

				$('.section').hide();
				$('.row-fluid.billing').fadeIn('fast');
			
			} else if(hash == "referrals") {

				$('.section').hide();
				$('.row-fluid.referrals').fadeIn('fast');
			
			}
		}


		$('.account_settings_nav a').click(function() {

			var id = $(this).attr('id'); 

			$('.section:visible').fadeOut('fast', function() {

				$('.row-fluid.' + id).fadeIn('fast');
			});

			return false;
		});


		$('.logo #logo').fileupload({
	        url: 'ajax_file_upload',
	        dataType: 'json',
	        autoUpload: true,
	        paramName: 'files[]',
	        acceptFileTypes: /(\.|\/)(jpe?g|png)$/i,
		    maxFileSize: 10000000, // 10 MB
		    imageMaxWidth: 800,
		    imageMaxHeight: 800,
	        formData: {
	        	action: 'upload_logo'
	        },
	        start: function(e) {

	        	// $('#profile_picture_preview_wrapper').hide();
	        	// $('#profile_picture_preview').hide();
	        	$('.logo .logo_upload .fileinput-button').fadeOut();

				$('.current_logo_wrapper').slideUp();

	        	$('#logo_progress .bar').css('width', '0%');
	        	$('#logo_progress').slideDown();

	        },
	        progressall: function (e, data) { 

	            var progress = parseInt(data.loaded / data.total * 100, 10);

	            $('#logo_progress .bar').css('width', progress + '%');
	        },
	        done: function(e, data) {

	        	$.ajax({

			        url: 'ajax_functions',  //server script to process data
			        type: 'POST',
			        data: {
			        	action: "get_logo_url"
			        }

			    }).done(function(data) {

					if(data) {

						$('.current_logo').attr('src', data);

						$(".current_logo").one("load", function() {
						  // do stuff
						}).each(function() {
						  if(this.complete) $(this).load();
						});

						$('.current_logo').load( function() {

							$('#logo_progress').slideUp(function() {

								$('.current_logo').show();
								$('.remove_logo').show();

								$('.current_logo_wrapper').slideDown();

				    			setTimeout(function() {

			    					$('.logo .logo_upload .fileinput-button').fadeIn();

			    				}, 500);
			    			});

						});

					}

				});

	        }

	    });


		$('.remove_logo').click(function() {

			$.ajax({

				type: "POST",
				url: "ajax_functions",
				data: { 
					action: "remove_logo"
				}

			}).done(function(result) {

				$('.current_logo').fadeOut('fast', function() {

					$('.current_logo').attr('src', '');
				});

				$('.remove_logo').fadeOut('fast');

			});

			return false;
		});



		$('body').on("click", ".user_accounts_table tr:gt(0) td:lt(4)", function() {

			var id = $(this).closest('tr').attr("id");

			window.location.href = "profile?id=" + id;
		});


		$('#invite_users_modal .invite_users').click(function () {

		    var first_name_1 = $('#inv_first_name_1').val();
		    var last_name_1 = $('#inv_last_name_1').val();
		    var email_1 = $('#inv_email_1').val();
		    var first_name_2 = $('#inv_first_name_2').val();
		    var last_name_2 = $('#inv_last_name_2').val();
		    var email_2 = $('#inv_email_2').val();
		    var first_name_3 = $('#inv_first_name_3').val();
		    var last_name_3 = $('#inv_last_name_3').val();
		    var email_3 = $('#inv_email_3').val();

		    $.ajax({

		        url: 'ajax_functions',  //server script to process data
		        type: 'POST',
		        data: {
		        	action: "invite_new_users",
		        	first_name_1: first_name_1,
		        	first_name_2: first_name_2,
		        	first_name_3: first_name_3,
		        	last_name_1: last_name_1,
		        	last_name_2: last_name_2,
		        	last_name_3:last_name_3,
		        	email_1: email_1,
		        	email_2: email_2,
		        	email_3: email_3
		        }

		    }).done(function(data) { 

		    	$('#invite_users_modal').modal('hide');
		    		
		    	window.location.href = 'account_settings?#user_accounts';

		    });

			return false;

		});



		$('body').on("click", ".user_accounts_table .storage_quota", function() {

			$('#storage_quota_modal #user_name').html('');
			$('#storage_quota_modal #current_quota').html('');
			$('#storage_quota_modal #additional_charges').html('–');

			var id = $(this).attr("id");
			var first_name = $(this).closest('tr').find('td:nth-child(1)').text();
			var last_name = $(this).closest('tr').find('td:nth-child(2)').text();

			$('#storage_quota_modal #user_id').val(id);

			$('#storage_quota_modal #user_name').html(first_name + ' ' + last_name);

			$('#storage_quota_modal ')

			$.ajax({

				type: "POST",
				url: "ajax_functions",
				data: { 
					action: "get_storage_quota",
					user_id: id
				}

			}).done(function(result) {

				var result_split = result.split(' ');

				$('#storage_quota_modal #update_quota').val(result_split[0]);

				$('#storage_quota_modal #current_quota').hide();
				$('#storage_quota_modal #current_quota').html(result);
				$('#storage_quota_modal #current_quota').fadeIn('fast');
				//// Incomplete.......

			});

		});


		$('body').on("change", "#storage_quota_modal #update_quota", function() {

			var quota_size = $('#storage_quota_modal #update_quota option:selected').val();

			var additional_charges;

			if(quota_size > 10) {

				additional_charges = quota_size * .1;

				additional_charges = '$' + additional_charges + '/Month'

			} else {

				additional_charges = '–';
			}

			
			$('#storage_quota_modal #additional_charges').fadeOut('fast', function() {

				$('#storage_quota_modal #additional_charges').html(additional_charges);
				$('#storage_quota_modal #additional_charges').fadeIn('fast');
			});
			

		});



		$('.delete_account').click(function() {

			var user_id = $(this).attr("id");
			var first_name = $(this).closest("tr").find("td:first-child").text();
			var last_name = $(this).closest("tr").find("td:nth-child(2)").text();

			$('#delete_account_modal .user_id').val(user_id);
			$('#delete_account_modal .modal-body').html('Are you sure you want to delete ' + first_name + ' ' + last_name + '\'s account?<br><br><i class="icon-warning-sign"></i> This action is permanent and cannot be undone.<input type="hidden" class="user_id" name="user_id" class="m-wrap span12" value="' + user_id + '" />');
		
			$('#delete_account_modal .modal-footer').html('<input type="submit" id="submit" class="btn light-red right delete" value="Delete" /><input type="button" data-dismiss="modal" class="btn" value="Cancel" />');
		
			
		});


		$('body').on("click", "#delete_account_modal .delete", function() {

			var user_id = $('#delete_account_modal .user_id').val();

			$.ajax({

				type: "POST",
				url: "ajax_functions",
				data: { 
					action: "delete_user_account",
					user_id: user_id
				}

			}).done(function(result) {

				$('.user_accounts_table tr#' + user_id).remove();

				$('#delete_account_modal .modal-body').html("<i class='icon-ok'></i> The user's account has been deleted.");

				$('#delete_account_modal .modal-footer').html('<input type="button" data-dismiss="modal" class="btn light-green right" value="Close" />');

			});

			return false;
		});



		$('.reset_password').click(function() {

			var user_id = $(this).attr("id");
			var first_name = $(this).closest("tr").find("td:first-child").text();
			var last_name = $(this).closest("tr").find("td:nth-child(2)").text();

			$('#reset_pasword_modal .user_id').val(user_id);
			$('#reset_password_modal .modal-body').html('You are about to reset ' + first_name + ' ' + last_name + '\'s password. To confirm this action, press \'Reset\' below.<br><br>Upon resetting, ' + first_name + ' ' + last_name + ' will receive an email with further instructions on setting up a new password. <input type="hidden" class="user_id" name="user_id" class="m-wrap span12" value="' + user_id + '" />');
			
			$('#reset_password_modal .modal-footer').html('<input type="submit" id="submit" class="btn light-red right reset" value="Reset" /><input type="button" data-dismiss="modal" class="btn" value="Cancel" />');

		});


		$('body').on("click", "#reset_password_modal .reset", function() {

			var user_id = $('#reset_pasword_modal .user_id').val();

			$.ajax({

				type: "POST",
				url: "ajax_functions",
				data: { 
					action: "reset_password",
					user_id: user_id
				}

			}).done(function(result) {

				$('#reset_password_modal .modal-body').html("<i class='icon-ok'></i> The user's password has been reset.");

				$('#reset_password_modal .modal-footer').html('<input type="button" data-dismiss="modal" class="btn light-green right" value="Close" />');

			});

			return false;
		});



		$('.disable_account input[type=checkbox]').change(function() {

			var user_id = $(this).closest("tr").attr("id");
			var toggle_state = 0;

			if($(this).is(":checked")) {
		    	toggle_state = 1;
		   	}

			$.ajax({

				type: "POST",
				url: "ajax_functions",
				data: { 
					action: "toggle_disable_account",
					user_id: user_id,
					toggle_state: toggle_state
				}

			}).done(function(result) {


			});

		});



		$('#billing_history').click(function() {

			getBillingHistory();
		});


		$('#billing_history_date_select').change(function() {

			getBillingHistory();
		});


		function getBillingHistory() {


			$('.loading_billing_history').fadeIn('fast');

			var date = $('#billing_history_date_select option:selected').val();

			$.ajax({

		        url: 'ajax_functions',  //server script to process data
		        type: 'POST',
		        data: {
		        	action: "get_billing_history",
		        	date: date
		        }

		    }).done(function(data) {

		    	$('.loading_billing_history').fadeOut('fast');

		    	$('.billing_history_details_wrapper').html(data);

		    });

		}



		$('#cancel_subscription_submit').click(function() {

			$('#cancel_subscription_submit').modal('hide');



			$('#cancel_subscription_confirmation_modal').modal('show');

			return false;
		});

	}



	if(page == "employees" || page == "team_members" || page == "associates") {

		$('body').on("click", ".employees_table tr:gt(0)", function() {

			var id = $(this).attr("id");

			window.location.href = "profile?id=" + id;
		});

		$('body').on("click", ".emp_item", function() {

			var id = $(this).attr("id");

			window.location.href = "profile?id=" + id;
		});


		$sizer = $('.emp_grid .shuffle_sizer');


		$('.search_emp').on('keyup change', function() {

		  var val = this.value.toLowerCase();


		  $('.emp_grid').shuffle('shuffle', function($el, shuffle) {

		    // Only search elements in the current group
		    if (shuffle.group !== 'all' && $.inArray(shuffle.group, $el.data('groups')) === -1) {
		      return false;
		    }

		    var text = $.trim( $el.text() ).toLowerCase();
		    return text.indexOf(val) !== -1;
		  });

		});


		$('.portlet-title .tools i.grid').click(function() {

			if(!$(this).hasClass('selected')) {

				$('.portlet-title .tools i').removeClass('selected');
				$(this).addClass('selected');

				$('.emp_table_wrapper .tools').fadeOut('fast');
				$('.portlet-body #data_table_wrapper').fadeOut('fast', function() {

					$('.portlet-body .gridview').fadeIn('fa');

					$('.emp_grid').shuffle({
						itemSelector: '.emp_item',
						sizer: $sizer
					});
				});
				
			}

			return false;
		});

		$('.portlet-title .tools i.list').click(function() {

			if(!$(this).hasClass('selected')) {

				$('.portlet-title .tools i').removeClass('selected');
				$(this).addClass('selected');

				$('.portlet-body .gridview').fadeOut('fast', function() {

					$('.emp_table_wrapper .tools').fadeIn('fast');
					$('.portlet-body #data_table_wrapper').fadeIn('fast');
				});
				
			}

			return false;
		});
	}


	if(page == "clients") {

		initGeoAddress();

		$('#add_client_geo_address').keydown(function(event){

	    	if(event.keyCode == 13) {
	    		
	      		event.preventDefault();
	      		return false;
	    	}
	  	});

		//Run twice -- once at page load, and once of client_type dropdown change
		var type = $('#add_new_client_modal #client_type').find('option:selected').val();

		if(type == "individual") {
			$('#add_new_client_modal .row-fluid.company').fadeOut('fast', function() {
				$('#add_new_client_modal .row-fluid.individual').fadeIn('fast');
			});
		} else if(type == "company") {
			$('#add_new_client_modal .row-fluid.individual').fadeOut('fast', function() {
				$('#add_new_client_modal .row-fluid.company').fadeIn('fast');
			});
		}



		if(hash == "add_new_client") {
			$('#add_new_client_modal').modal("show");
		}

		$('#add_new_client_modal #client_type').change(function() {
			var type = $(this).find('option:selected').val();

			if(type == "individual") {
				$('#add_new_client_modal .row-fluid.company').fadeOut('fast', function() {
					$('#add_new_client_modal .row-fluid.individual').fadeIn('fast');
				});
			} else if(type == "company") {
				$('#add_new_client_modal .row-fluid.individual').fadeOut('fast', function() {
					$('#add_new_client_modal .row-fluid.company').fadeIn('fast');
				});
			}
		});


		$('body').on("click", ".clients tr td:not(:last-child)", function() {

			var id = $(this).closest("tr").attr("id");

			window.location.href = "client_profile?id=" + id;
		});

 
		$('body').on("click", ".client_item", function() {

			var id = $(this).attr("id");

			window.location.href = "client_profile?id=" + id;
		});


		$sizer = $('.clients_grid .shuffle_sizer');


		$('.search_clients').on('keyup change', function() {

		  var val = this.value.toLowerCase();


		  $('.clients_grid').shuffle('shuffle', function($el, shuffle) {

		    // Only search elements in the current group
		    if (shuffle.group !== 'all' && $.inArray(shuffle.group, $el.data('groups')) === -1) {
		      return false;
		    }

		    var text = $.trim( $el.text() ).toLowerCase();
		    return text.indexOf(val) !== -1;
		  });

		});


		$('.portlet-title .tools i.grid').click(function() {

			if(!$(this).hasClass('selected')) {

				$('.portlet-title .tools i').removeClass('selected');
				$(this).addClass('selected');

				$('.clients_table_wrapper .tools').fadeOut('fast');
				$('.portlet-body #data_table_wrapper').fadeOut('fast', function() {

					$('.portlet-body .gridview').fadeIn('fa');

					$('.clients_grid').shuffle({
						itemSelector: '.client_item',
						sizer: $sizer
					});
				});
				
			}

			return false;
		});

		$('.portlet-title .tools i.list').click(function() {

			if(!$(this).hasClass('selected')) {

				$('.portlet-title .tools i').removeClass('selected');
				$(this).addClass('selected');

				$('.portlet-body .gridview').fadeOut('fast', function() {

					$('.clients_table_wrapper .tools').fadeIn('fast');
					$('.portlet-body #data_table_wrapper').fadeIn('fast');
				});
				
			}

			return false;
		});


		// $('#add_new_client_modal #submit').click(function() {

		// 	$('#add_new_client_modal').modal('hide');

		// 	var result = addNewClient();

		// 	if(result == true) {

		// 		window.location.reload();
		// 	}

		// 	return false;
		// });



		$('body').on("click", ".delete_client", function() {

			var client_id = $(this).closest("tr").attr("id");
			var client_name = $(this).closest("tr").find("td:first-child").text();

			$('#delete_client_modal .modal-body').html('Are you sure you want to delete ' + client_name + '\'s profile?<br><br><i class="icon-warning-sign"></i> This action is permanent and cannot be undone.<input type="hidden" class="client_id" name="client_id" class="m-wrap span12" value="' + client_id + '" />');
		
			$('#delete_client_modal .modal-footer').html('<input type="submit" id="submit" class="btn light-red right delete" value="Delete" /><input type="button" data-dismiss="modal" class="btn" value="Cancel" />');
		
			
		});


		$('body').on("click", "#delete_client_modal .delete", function() {

			var client_id = $('#delete_client_modal .client_id').val();

			$.ajax({

				type: "POST",
				url: "ajax_functions",
				data: { 
					action: "delete_client",
					client_id: client_id
				}

			}).done(function(result) { 

				window.location.reload();

				// var item = $('.clients_grid .client_item#' + client_id);
				// $('.clients_grid').shuffle('remove', item);

				// $('#delete_client_modal .modal-body').html("<i class='icon-ok'></i> The client's profile has been deleted.");

				// $('#delete_client_modal .modal-footer').html('<input type="button" data-dismiss="modal" class="btn light-green right" value="Close" />');

			});

			return false;
		});

	}



	if(page == "add_employee") {

		$('.profile_picture img').click(function() {

			//$('.profile_picture img').removeClass("selected");
			//$(this).addClass("selected");

			$('.profile_picture div.radio span').removeClass("checked");
			$(this).parent().find("div.radio span").addClass("checked");

			$('.profile_picture div.radio input').removeAttr("checked");
			$(this).parent().find("div.radio span input").attr("checked", "checked");
		});


		$('.profile_picture div.radio span').click(function() {

			$('.profile_picture div.radio input').removeAttr("checked");
			$(this).find("input").attr("checked", "checked");

			$('.profile_picture img').removeClass("selected");
			$(this).parent().parent().find("img").addClass("selected");
		});
	}


	if(page == "product") {

		$('.remove_product_image').click(function() {

			var item_id = $(this).attr("id");

			$(this).animate({
				opacity: 0,
				duration: 200
			});

			$('.product_image').animate({

				opacity: .6,
				duration: 200

			}, function() {

				$.ajax({

					type: "POST",
					url: "ajax_functions",
					data: { 
						action: "remove_product_image",
						item_id: item_id
					}

				}).done(function(result) {

					$('.product_image').animate({

						opacity: 0,
						duration: 400
					}, function() {

						$(this).slideUp(400, function() {

							$(this).remove();
						});
					});

				});

			});

			return false;

		});



		$('body').on("click", ".delete_item", function() {

			var item_id = $('#delete_item_modal #item_id').val();
			var item_name = $('#delete_item_modal #item_name').val();

			$('#delete_item_modal .modal-body').html('Are you sure you want to delete ' + item_name + ' from the inventory?<br><br><i class="icon-warning-sign"></i> This action is permanent and cannot be undone.');
		
			$('#delete_item_modal .modal-footer').html('<input type="submit" id="submit" class="btn light-red right delete" value="Delete" /><input type="button" data-dismiss="modal" class="btn" value="Cancel" />');
		});


		$('body').on("click", "#delete_item_modal .delete", function() {

			var item_id = $('#delete_item_modal #item_id').val();

			$.ajax({

				type: "POST",
				url: "ajax_functions",
				data: { 
					action: "delete_item",
					item_id: item_id
				}

			}).done(function(result) {

				$("#delete_item_modal").modal('hide');

				window.location.href = "inventory";

			});

			return false;
		});




		$('body').on("click", ".generate_qr_code", function() {

			var item_id = $(this).attr('id');

			$(this).html('<i class="loading_inline"></i>');

			$.ajax({

				type: "POST",
				url: "product_create_qr",
				data: { 
					product_id: item_id
				}

			}).done(function(result) {


				$.ajax({

					type: "POST",
					url: "ajax_functions",
					data: { 
						action: 'load_product_qr',
						product_id: item_id
					}

				}).done(function(data) {

					if(data == 'failed') {

						$('.qr_code').prepend('Generating QR Code failed. Please try again.<br>');

					} else {

						$('.qr_code').html(data);
					}
				});

			});

			return false;
		});




		$('body').on("click", ".invoices tr td:not(:nth-child(8))", function() {

			var id = $(this).closest("tr").attr("id");

			window.location.href = "invoice?id=" + id;
		});




		$.ajax({

	        url: 'ajax_functions',  //server script to process data
	        type: 'POST',
	        data: {
	        	action: "load_product_sales_history"
	        }

	    }).done(function(data) {

			var interval = setInterval(function() {

				if(fade_in_complete) {

					clearInterval(interval);

			        var d = $.parseJSON($('#sales_history_data').val());
					// first correct the timestamps - they are recorded as the daily
					// midnights in UTC+0100, but Flot always displays dates in UTC
					// so we have to add one hour to hit the midnights in the plot

					for (var i = 0; i < d.length; ++i) {
						d[i][0] += 60 * 60 * 1000;
					}

					// helper for returning the weekends in a period

					function weekendAreas(axes) {

						var markings = [],
							d = new Date(axes.xaxis.min);

						// go to the first Saturday

						d.setUTCDate(d.getUTCDate() - ((d.getUTCDay() + 1) % 7))
						d.setUTCSeconds(0);
						d.setUTCMinutes(0);
						d.setUTCHours(0);

						var i = d.getTime();

						// when we don't set yaxis, the rectangle automatically
						// extends to infinity upwards and downwards

						do {
							markings.push({ xaxis: { from: i, to: i + 2 * 24 * 60 * 60 * 1000 } });
							i += 7 * 24 * 60 * 60 * 1000;
						} while (i < axes.xaxis.max);

						return markings;
					}

					var options = {
						xaxis: {
							mode: "time",
							tickLength: 5,
							timeformat: "%m/%d/%y",
							minTickSize: [1, "day"]
						},
						yaxis: {
							minTickSize: 1
						},
						selection: {
							mode: "x"
						},
						grid: {
	                    	hoverable: true,
	                        tickColor: "#eee",
	                        borderColor: "#eee",
	                        borderWidth: 1,
	                        markings: weekendAreas
	                    },
						series: {
							lines: {
								show: true,
								lineWidth: 2,
								fill: true,
	            				fillColor: { colors: [{ opacity: 0.2 }, { opacity: 0.2}] }
							},
							points:{
					            show: true,
					            radius: 3
					        },
							shadowSize: 0
						},
						colors: ["#4b8df8", "#4b8df8"]
					};

					var plot = $.plot($(".sales_history_chart"),
						[{
		                    label: "Units Sold",
		                    data: d,
		                    lines: {
		                        lineWidth: 1,
		                    },
		                    shadowSize: 0
		                }],
			            options);

					var overview = $.plot(".sales_history_chart_zoom", [d], {
						series: {
							lines: {
								show: true,
								lineWidth: 2
							},
							shadowSize: 0
						},
						colors: ["#4b8df8"],
						grid: {
	                        tickColor: "#eee",
	                        borderColor: "#eee",
	                        borderWidth: 1,
	                        markings: weekendAreas
	                    },
						xaxis: {
							ticks: [],
							mode: "time"
						},
						yaxis: {
							ticks: [],
							min: 0,
							autoscaleMargin: 0.1
						},
						selection: {
							mode: "x"
						}
					});

					// now connect the two

					$(".sales_history_chart").bind("plotselected", function (event, ranges) {

						// do the zooming
						$.each(plot.getXAxes(), function(_, axis) {
							var opts = axis.options;
							opts.min = ranges.xaxis.from;
							opts.max = ranges.xaxis.to;
						});
						plot.setupGrid();
						plot.draw();
						plot.clearSelection();

						// don't fire event on the overview to prevent eternal loop

						overview.setSelection(ranges, true);
					});

					$(".sales_history_chart_zoom").bind("plotselected", function (event, ranges) {
						plot.setSelection(ranges);
					});


					function showTooltip(x, y, contents) {

		                $('<div id="tooltip">' + contents + '</div>').css({
		                    position: 'absolute',
		                    display: 'none',
		                    top: y + 5,
		                    left: x + 15,
		                    border: '1px solid #333',
		                    padding: '4px',
		                    color: '#fff',
		                    'border-radius': '3px',
		                    'background-color': '#333',
		                    opacity: 0.80
		                }).appendTo("body").fadeIn(200);
		            }


		            var previousPoint = null;
		            $(".sales_history_chart").bind("plothover", function (event, pos, item) {

		                if(item) {
		                    if (previousPoint != item.dataIndex) {
		                        previousPoint = item.dataIndex;

		                        $("#tooltip").remove();

		                        var x = item.datapoint[0],
		                            y = item.datapoint[1];

		                        showTooltip(item.pageX, item.pageY, y + " " + item.series.label);
		                    }
		                } else {
		                    $("#tooltip").remove();
		                    previousPoint = null;
		                }
		            });

		        }

			}, 200);

		});


	}



	if(page == "search") {

		var filter = "";

		var requests = new Array();

		$("#query").keyup(function(e){

			search();
		});


		$('#search_filters li').click(function() {

			$('#search_filters li').removeClass("selected");

			$(this).addClass("selected");
			filter = $(this).attr("id");

			search();
		});


		function search() {

			var query = $("#query").val();
			query = $.trim(query);

			if(query != "") {

				$('.search_results').show();
				$('.search_results_header .loading').show();

				for(var i = 0; i < requests.length; i++) {
	    			requests[i].abort();
				}

				requests = [];

	        	requests.push(
					$.ajax({

						type: "POST",
						url: "ajax_functions",
						data: { 
							action: "search",
							query: query,
							filter: filter
						}

					}).done(function(result) {

						$('.search_results').html(result);

						$('.search_results .portlet-body:empty').closest('.portlet').remove();

						if($('.search_results').html() == "") {

							$('.search_results').html('<div class="portlet box blue"><div class="portlet-body">Your search did not return any results. Please try a different query.</div></div>');
						}

						$('.search_results_header .loading').hide();

					})
				);
	        
	        } else {

	        	for(var i = 0; i < requests.length; i++) {
	    			requests[i].abort();
				}

				requests = [];

				$('.search_results').hide();
	        	$('.search_results').html('<div class="search_results_header"><div class="loading"></div></div>');
	        }
		}
	}



	if(page == "storage") {

		$('body').on('click', '.elfinder-navbar-dir:not(.ui-state-active)', function() {

			$('#file_manager .elfinder-cwd-wrapper').css('opacity', '.5');
		});


		// $('body').on('click', '#file_manager .sharelink #file_link', function() {

		// 	if($(this).val() != "") {

		// 		$(this).select();

		// 		$('.sharelink #file_link').animate({
		// 			width: '600px',
		// 			duration: 300
		// 		});
		// 	}
		// });

		// $('body').on('focusin', '#file_manager .sharelink #file_link', function() {

		// 	if($(this).val() != "") {

		// 		$('.sharelink #file_link').animate({
		// 			width: '600px',
		// 			duration: 300
		// 		});
		// 	}
		// });

		// $('body').on('focusout', '#file_manager .sharelink #file_link', function() {

		// 	$('.sharelink #file_link').animate({
		// 		width: '200px',
		// 		duration: 300
		// 	});
		// });
		
		

		$('body').on('click', '.elfinder-cwd-wrapper', function() {

			$('.sharelink #file_link').animate({
				width: '200px',
				duration: 300
			});

		});


		elFinder.prototype.i18.en.messages['Share'] = 'Share';
	    elFinder.prototype._options.commands.push('Share');
	    elFinder.prototype.commands.Share = function() {
	        this.exec = function() {
	             
	             var url = $('#file_manager .sharelink #file_link').val();
	             window.open(url, "_blank");
	             
	        }
	        this.getstate = function() {
	            var sel = this.fm.selectedFiles();

                return !this._disabled && sel.length == 1 && sel[0].phash && !sel[0].locked  ? 0 : -1;
	        }
	    }


	    $('body').on("fadeInComplete", function(){

			var elf = $("#file_manager").elfinder({
				lang: 'en',
				url : "file", //.php taken out!! 
				uiOptions : {
					// toolbar configuration
					toolbar : [
						["reload"],
						["mkdir", "mkfile", "upload"],
						["open", "download"],
						["quicklook", "Share"],
						["rm"],
						["rename", "edit"],
						["search"],
						["view"]
					],

					// directories tree options
					tree : {
						// expand current root on init
						openRootOnLoad : true,
						// auto load current dir parents
						syncTree : true
					}
				},
				contextmenu : {
					// navbarfolder menu
					navbar : ["open", "|", "rm"],

					// current directory menu
					cwd    : ["reload", "|", "upload", "mkdir", "mkfile"],

					// current directory file menu
					files  : [
						"open", "quicklook", "Share", "download", "|", "edit", "rename", "rm", "|", "info"
					]
				},
				handlers : {
					load: function(event, eventInstance) {

						var storage_stats_result = calculateStorageCap();

						var storage_stats = storage_stats_result.split(",");
						var storage_stats_percentage = storage_stats[0];
						var storage_stats_text = storage_stats[1];
						available_space = storage_stats[2];


						if(available_space <= 0) {
		
							$('#storage_limi_reached_modal').modal('show');
						} 

						var interval = setInterval(function() {

							if($('#file_manager .elfinder-navbar-wrapper .elfinder-navbar-root').length > 0) {

								$('#file_manager .elfinder-navbar-wrapper .elfinder-navbar-root').contents().last()[0].textContent = f_name + " " + l_name;

								$('#file_manager .elfinder-statusbar').prepend('<div class="sharelink"><input type="text" id="file_link" readonly /></div>');


								$('#file_manager .elfinder-statusbar').prepend('<div class="storage_stats"><div class="storage_stats_bar"><div class="storage_stats_outer_bar"><div class="storage_stats_inner_bar"></div></div></div><div class="storage_stats_text">' + storage_stats_text + '</div><span class="storage_stats_add_space"><a href="#">Add More Space</a></span></div>');

								$('.storage_stats .storage_stats_inner_bar').css('width', parseFloat(storage_stats_percentage) + '%');

								clearInterval(interval);
							}

						}, 100);
						
						
					},
					select: function(event, eventInstance) {

						//Modify to disallow sharing of MULTIPLE selected files. Only singles files or whole folders can be shared.

						if(event.data.selected.length == 1) {

							var hash = event.data.selected[0];
							var path = "";


							//If the selected item is a directory, then the fileshare kind will be submitted as folder instead of file.
							if($('#file_manager .elfinder-cwd-wrapper .ui-selected').is('.directory')) {

								if(hash != "" && hash != null) {

									path = "https://sunsetblvd.ironvault.ca/fileshare_create?kind=folder&h=" + hash;
									$('.sharelink #file_link').val(path);
								
								} else {

									$('.sharelink #file_link').val('');
								}

							} else {

								if(hash != "" && hash != null) {

									path = "https://sunsetblvd.ironvault.ca/fileshare_create?kind=file&h=" + hash;
									$('.sharelink #file_link').val(path);
								
								} else {

									$('.sharelink #file_link').val('');
								}

							}

						} else {

							$('.sharelink #file_link').val('');
						}
					},
					dblclick: function(event, eventInstance) {

						if($('#file_manager .elfinder-cwd-wrapper .ui-selected').is('.directory')) {
							//The selected icon is a directory
							$('#file_manager .elfinder-cwd-wrapper').css('opacity', '.5');
						}

					},
					open: function(event, eventInstance) {

						var count = 0;

						var interval = setInterval(function() {

							$('#file_manager .elfinder-cwd-wrapper table tbody tr.directory td:nth-child(3)').text("–");
							$('#file_manager .elfinder-cwd-wrapper table tbody tr.directory td:nth-child(4)').text("–");

							if(count >= 4) {
								clearInterval(interval);
							}

							count++;

						}, 100);


						$('#file_manager .elfinder-cwd-wrapper').css('opacity', '1');

					},
					upload: function(event, eventInstance) {
						
						var storage_stats_result = calculateStorageCap();

						var storage_stats = storage_stats_result.split(",");
						var storage_stats_percentage = storage_stats[0];
						var storage_stats_text = storage_stats[1];
						available_space = storage_stats[2];

						$('#file_manager .elfinder-statusbar .storage_stats .storage_stats_text').html(storage_stats_text);

						$('.storage_stats .storage_stats_inner_bar').css('width', parseInt(storage_stats_percentage) + '%');

					},
					remove: function(event, eventInstance) {

						var storage_stats_result = calculateStorageCap();

						var storage_stats = storage_stats_result.split(",");
						var storage_stats_percentage = storage_stats[0];
						var storage_stats_text = storage_stats[1];
						available_space = storage_stats[2];

						$('#file_manager .elfinder-statusbar .storage_stats .storage_stats_text').html(storage_stats_text);

						$('.storage_stats .storage_stats_inner_bar').css('width', parseInt(storage_stats_percentage) + '%');

					},
					add: function(event, eventInstance) {

						var storage_stats_result = calculateStorageCap();

						var storage_stats = storage_stats_result.split(",");
						var storage_stats_percentage = storage_stats[0];
						var storage_stats_text = storage_stats[1];
						available_space = storage_stats[2];

						$('#file_manager .elfinder-statusbar .storage_stats .storage_stats_text').html(storage_stats_text);

						$('.storage_stats .storage_stats_inner_bar').css('width', parseInt(storage_stats_percentage) + '%');
					},
					change: function(event, eventInstance) {

						var storage_stats_result = calculateStorageCap();

						var storage_stats = storage_stats_result.split(",");
						var storage_stats_percentage = storage_stats[0];
						var storage_stats_text = storage_stats[1];
						available_space = storage_stats[2];

						$('#file_manager .elfinder-statusbar .storage_stats .storage_stats_text').html(storage_stats_text);

						$('.storage_stats .storage_stats_inner_bar').css('width', parseInt(storage_stats_percentage) + '%');
					},
					sync: function(event, eventInstance) {

						var storage_stats_result = calculateStorageCap();

						var storage_stats = storage_stats_result.split(",");
						var storage_stats_percentage = storage_stats[0];
						var storage_stats_text = storage_stats[1];
						available_space = storage_stats[2];

						$('#file_manager .elfinder-statusbar .storage_stats .storage_stats_text').html(storage_stats_text);

						$('.storage_stats .storage_stats_inner_bar').css('width', parseInt(storage_stats_percentage) + '%');
					}
				},
				allowShortcuts: false

			}).elfinder("instance");
		

			$('.elfinder-workzone').height(height - 228);
		});
	}




	$('body').on('click', '.export_to_excel', function() {

		if($('.table')) {

			$('.btn-group.open').removeClass("open");


			var table = $('#data_table').clone();
			var table_name = "";

		    $(table).css("display", "none");
		    $(table).addClass("temp_table");

		    $('body').append(table);


		    if(page == "employees") {

		    	$(".temp_table th:nth-child(8)").remove();
			    $(".temp_table td:nth-child(8)").remove();

			    $(".temp_table th:nth-child(7)").remove();
			    $(".temp_table td:nth-child(7)").remove();

			    $(".temp_table th:nth-child(1)").remove();
			    $(".temp_table td:nth-child(1)").remove();

			    table_name = "employees";

		    } else if(page == "inventory") {

		    	$(".temp_table th:nth-child(7)").remove();
			    $(".temp_table td:nth-child(7)").remove();

			    $(".temp_table th:nth-child(6)").remove();
			    $(".temp_table td:nth-child(6)").remove();

			    table_name = "inventory";

		    } else if(page == "product") {

			    $(".temp_table th:nth-child(5)").remove();
			    $(".temp_table td:nth-child(5)").remove();

			    table_name = "item-invoices";
			
			} else if(page == "clients") {

				$(".temp_table th:nth-child(6)").remove();
			    $(".temp_table td:nth-child(6)").remove();

			    $(".temp_table th:nth-child(5)").remove();
			    $(".temp_table td:nth-child(5)").remove();

			    table_name = "clients";
			
			} else if(page == "client_profile") {

			    $(".temp_table th:nth-child(5)").remove();
			    $(".temp_table td:nth-child(5)").remove();

			    table_name = "client-invoices";

			} else if(page == "invoices") {

			    $(".temp_table th:nth-child(5)").remove();
			    $(".temp_table td:nth-child(5)").remove();

			    table_name = "invoices";
			    
			}


		    var export_table = $("<div />").append($('.temp_table').clone()).html();

		    $('.temp_table').remove();


		    $('body').append('<form target="iframe" style="display: none;" method="post" id="export_excel_form" action="export_excel"><input type="hidden" name="table" id="table_html"/><input type="hidden" name="table_name" id="table_name"/></form>');
		    $('body').append('<iframe name="iframe" style="display: none;"></iframe>');

		    $('#table_html').val(export_table);
		    $('#table_name').val(table_name);

		    $('#export_excel_form').submit();

		}

		return false;

	});


	// TIME, DATE & WEATHER WIDGET //

	if(navigator.userAgent.match(/Android/i)

		|| navigator.userAgent.match(/webOS/i)

		|| navigator.userAgent.match(/iPhone/i)

		|| navigator.userAgent.match(/iPad/i)

		|| navigator.userAgent.match(/iPod/i)

		|| navigator.userAgent.match(/BlackBerry/i)

		|| navigator.userAgent.match(/Windows Phone/i)

		|| page == "join"

		|| page == "login"

		|| page == "password_reset"

		|| page == "lock"

		|| page == "fileshare"

		|| page == "view_invoice"

		|| page == ""

	) {

		// do nothing 
		

	} else {
		

		var first_time = true;

		var weekday = new Array(7);
		weekday[0]="Sunday";
		weekday[1]="Monday";
		weekday[2]="Tuesday";
		weekday[3]="Wednesday";
		weekday[4]="Thursday";
		weekday[5]="Friday";
		weekday[6]="Saturday";

		var months = new Array(12);
		months[0] = "January";
		months[1] = "February";
		months[2] = "March";
		months[3] = "April";
		months[4] = "May";
		months[5] = "June";
		months[6] = "July";
		months[7] = "August";
		months[8] = "September";
		months[9] = "October";
		months[10] = "November";
		months[11] = "December";

		function startDateTime() {

			var today = new Date();
			var day = today.getDay();
			var date = today.getDate();
			var month = today.getMonth();
			var year = today.getFullYear();
			var full_date;

			var h = today.getHours();
			var m = today.getMinutes();
			var time;

			var am_pm = "AM";

			if(h >= 12) {
				am_pm = "PM";
			}

		    if (h > 12) {
		        h = h - 12;
		    } else if(h == 0) {
		    	h = 12;
		    }

			m = checkTime(m);

			time = h + ":" + m + " " + am_pm;

			full_date = date + " " + months[month].toUpperCase() + ", " + year + "<br>" + weekday[day].toUpperCase();

			$('#date_time_weather #time').html(time);
			$('#date_time_weather #date').html(full_date);

			if(first_time == true) {
				$('#date_time_weather #time').fadeIn();
				$('#date_time_weather #date').fadeIn();
				first_time = false;
			}

			t = setTimeout(function(){
				startDateTime();
			}, 1000);
		}

		function checkTime(i) {
			if (i<10) {

				i="0" + i;
			}
			
			return i;
		}

		startDateTime();

		// --------------------------- //

		var sunny_codes = [32, 19, 34, 36];
		var cloudy_codes = [28, 30, 44];
		var cloudy_night_codes = [27, 29];
		var overcast_codes = [26];
		var light_snow_codes = [13, 14, 42];
		var heavy_snow_codes = [15, 16, 41, 43];
		var light_rain_codes = [9, 40, 46];
		var heavy_rain_codes = [11, 12];
		var thunderstorm_codes = [0, 1, 2, 3, 4, 37, 38, 39, 45, 47];
		var sleet_codes = [5, 6, 7, 8, 10, 17, 18, 35];
		var windy_codes = [23, 24];
		var fog_codes = [19, 20, 21, 22];
		var moon_codes = [31, 33];
		var cold_codes = [25];

		var loc;
		var loc_found = false;

		function get_location(position) {

			var lat = position.coords.latitude;
	        var lng = position.coords.longitude;

	        loc = lat + "," + lng;

	        getWeather();

	        startInterval();
		}

		if(navigator.geolocation) {

			navigator.geolocation.getCurrentPosition(get_location); 
		
		}


		function startInterval() { //get the updated weather every 10min

			setInterval(function() {
				getWeather();
			}, 600000);
		}


		function getWeather() {

			$.ajax({

				type: "POST",
				url: "ajax_functions",
				data: { 
					action: "get_woeid",
					loc: loc
				}

			}).done(function(result) {

				var woeid = $(result).find("woeid").text();

				$.simpleWeather({
					woeid: woeid,
					unit: 'c',
					success: function(weather) {

						var icon, weather_icon;

						var code = parseInt(weather.currently_code);

						if($.inArray(code, sunny_codes) > -1) {
							var icon = "sunny";
						} else if($.inArray(code, cloudy_codes) > -1) {
							var icon = "light_clouds";
						} else if($.inArray(code, cloudy_night_codes) > -1) {
							var icon = "cloudy_night";
						} else if($.inArray(code, overcast_codes) > -1) {
							var icon = "overcast";
						} else if($.inArray(code, light_snow_codes) > -1) {
							var icon = "snow";
						} else if($.inArray(code, heavy_snow_codes) > -1) {
							var icon = "heavy_snow";
						} else if($.inArray(code, light_rain_codes) > -1) {
							var icon = "rain";
						} else if($.inArray(code, heavy_rain_codes) > -1) {
							var icon = "heavy_rain";
						} else if($.inArray(code, thunderstorm_codes) > -1) {
							var icon = "thunderstorm";
						} else if($.inArray(code, sleet_codes) > -1) {
							var icon = "sleet";
						} else if($.inArray(code, windy_codes) > -1) {
							var icon = "windy";
						} else if($.inArray(code, fog_codes) > -1) {
							var icon = "fog";
						} else if($.inArray(code, moon_codes) > -1) {
							var icon = "moon";
						} else if($.inArray(code, cold_codes) > -1) {
							var icon = "cold";
						} else if(code == 3200) {
							var icon = "na";
						} else {
							var icon = "na";
						}


						$('#date_time_weather #weather #weather_icon').html('<img src="../img/weather/' + icon + '.png" alt="Weather Icon" />');

						
						var weather_text = weather.temp + "&deg; C<br>" + weather.currently.toUpperCase();
				 
						$('#date_time_weather #weather #weather_text').html(weather_text);

						$('#date_time_weather #divider').fadeIn();
						$('#date_time_weather #weather').fadeIn();
					}
				});
			});

		}

	}



	if(page == "login") {


	}


	// INIT CLIENT PAGE MAP!
	if(page == "client_profile") {

		initGeoAddress();



		$("#update_client_info").click(function() {

			var client_info_json = $('.client_info_table #client_info_json').val();
			var client_info_obj = $.parseJSON(client_info_json);

			if(client_info_obj.type == "individual") {

				$('#update_client_info_modal .row-fluid.company').fadeOut('fast', function() {
					$('#update_client_info_modal .row-fluid.individual').fadeIn('fast');
				});

			} else if(client_info_obj.type == "business") {

				$('#update_client_info_modal .row-fluid.individual').fadeOut('fast', function() {
					$('#update_client_info_modal .row-fluid.company').fadeIn('fast');
				});

			}

			$('#update_client_info_modal #client_type').val(client_info_obj.type);
			$('#update_client_info_modal #first_name').val(client_info_obj.first_name);
			$('#update_client_info_modal #last_name').val(client_info_obj.last_name);
			$('#update_client_info_modal #business_name').val(client_info_obj.business_name);
			$('#update_client_info_modal #individual_email').val(client_info_obj.email);
			$('#update_client_info_modal #business_email').val(client_info_obj.email);
			$('#update_client_info_modal #gender').val(client_info_obj.gender);
			$('#update_client_info_modal #primary_phone').val(client_info_obj.primary_phone);
			$('#update_client_info_modal #alternative_phone').val(client_info_obj.alternative_phone);
			$('#update_client_info_modal #update_client_geo_address').val(client_info_obj.address);

		});


		if(hash == "update_client_profile") {
			
			$('#update_client_info').trigger('click');
		}


		$('.client_notes_edit').autosize();


		$('.client_notes_edit').focus(function() {

			$(this).addClass('active');
		});

		$('.client_notes_edit').blur(function() {

			$(this).removeClass('active');
		});



		$('.save_client_notes').click(function() {

			var client_id = $('.client_notes_edit').attr('id');
			var client_notes = $('.client_notes_edit').val();
			var last_update = '';

			$('.client_notes_last_update').html('Saving...');

			$.ajax({

				type: "POST",
				url: "ajax_functions",
				data: { 
					action: "save_client_notes",
					client_id: client_id,
					client_notes: client_notes
				}

			}).done(function(result) {	

				if(result != "") {

					last_update = result;

					$('.client_notes_last_update').html('Last Saved: ' + last_update);
				
				} else {

					$('.client_notes_last_update').html('Could not save. Please try again.');
				}
			});


			return false;

		});



		$('#update_client_info_modal #client_type').change(function() {
			var type = $(this).find('option:selected').val();

			if(type == "individual") {
				$('#update_client_info_modal .row-fluid.company').fadeOut('fast', function() {
					$('#update_client_info_modal .row-fluid.individual').fadeIn('fast');
				});
			} else if(type == "business") {
				$('#update_client_info_modal .row-fluid.individual').fadeOut('fast', function() {
					$('#update_client_info_modal .row-fluid.company').fadeIn('fast');
				});
			}
		});
			


		// $('body').on("click", "#update_client_info_modal #submit", function() {

		// 	var client_id = $('#update_client_info_modal #client_id').val();

		// 	var client_type = $('#update_client_info_modal #client_type').val();
		// 	var first_name = $('#update_client_info_modal #first_name').val();
		// 	var last_name = $('#update_client_info_modal #last_name').val();
		// 	var business_name = $('#update_client_info_modal #business_name').val();
		// 	var business_email = $('#update_client_info_modal #business_email').val();
		// 	var individual_email = $('#update_client_info_modal #individual_email').val();
		// 	var gender = $('#update_client_info_modal #gender').val();
		// 	var primary_phone = $('#update_client_info_modal #primary_phone').val();
		// 	var alternative_phone = $('#update_client_info_modal #alternative_phone').val();
		// 	var address = $('#update_client_info_modal #update_client_geo_address').val();

		// 	$.ajax({

		// 		type: "POST",
		// 		url: "ajax_functions",
		// 		data: { 
		// 			action: "update_client_info",
		// 			client_id: client_id,
		// 			client_type: client_type,
		// 			first_name: first_name,
		// 			last_name: last_name,
		// 			business_name: business_name,
		// 			business_email: business_email,
		// 			individual_email: individual_email,
		// 			gender: gender,
		// 			primary_phone: primary_phone,
		// 			alternative_phone: alternative_phone,
		// 			address: address
		// 		}

		// 	}).done(function(result) { 

		// 		window.location.reload();

		// 	});

		// 	return false;
		// });



		$('body').on("click", ".invoices tr td:not(:nth-child(7))", function() {

			var id = $(this).closest("tr").attr("id");

			window.location.href = "invoice?id=" + id;
		});


		var addresses = new Array(); 
		var client_map;
		var client_markers;

		var bounds = new google.maps.LatLngBounds();

		var markers = new Array();

		var marker_image = {
				url: '../img/map_marker.png'
			};


		$('.client_address').each(function() {

			var id = $(this).attr('id');
			var label = $(this).attr('label');
			var address = $(this).val();

			var address_item = new Array(id, label, address); 

			addresses[id] = address_item;
		});



		$('.client_map_locations tbody tr').hover(function() {

			var id = $(this).attr('id');

			if(markers[id]) {
				markers[id].setAnimation(google.maps.Animation.BOUNCE);
			}

		}, function() {

			var id = $(this).attr('id');

			if(markers[id]) {
				markers[id].setAnimation(null);
			}
		});




		if($(addresses).length > 0) {

			$('body').on("fadeInComplete", function(){

				var mapOptions = {
			        zoom: 5,
			        center: new google.maps.LatLng(40.580585, -92.900391),
			        mapTypeId: google.maps.MapTypeId.ROADMAP
			    };

			    client_map = new google.maps.Map(document.getElementById('client_gmap'), mapOptions);

			    google.maps.visualRefresh=true;

				for (var x = 0; x < addresses.length; x++) {

					var full_address = '<strong>' + addresses[x][1] + '</strong><br>' + addresses[x][2];
					var address_id = addresses[x][0];

					$.ajax({
					  url: 'https://maps.googleapis.com/maps/api/geocode/json?address='+addresses[x][2]+'&sensor=false',
					  dataType: 'json',
					  async: false,
					  data: null,
					  success: function(data) {

					  	if(data.status != "ZERO_RESULTS") {
						    var p = data.results[0].geometry.location
				            var latlng = new google.maps.LatLng(p.lat, p.lng);

				            var marker = new google.maps.Marker({
				                position: latlng,
				                map: client_map,
				                icon: marker_image
				            });

				            markers[address_id] = marker;

				            bounds.extend(marker.position);
				            client_map.fitBounds(bounds);

				            var infowindow = new google.maps.InfoWindow({
						      content: full_address
						    });

				            clientInfoWindow(client_map, infowindow, marker);
				        }
					  }
					});
			    }

			});
		}


		function clientInfoWindow(map, infowindow, marker) {

		  google.maps.event.addListener(marker, 'click', function() {
		  	infowindow.close();

		    infowindow.open(client_map, marker);
		  });
		}



		//File attachments ===============
		// $('body').on('click', '.attachment .view_file', function() {

		// 	var filename = $(this).closest('.attachment').attr('id');



		// 	return false;
		// });


		// $('body').on('click', '.attachment .download_file', function() {

		// 	var filename = $(this).closest('.attachment').attr('id');

			
			
		// 	return false;
		// });


		var file_count = 0;
		var files = new Array();
		var client_id = $('#upload_client_files_modal #client_id').val();

		$('.file_upload #file').fileupload({
	        url: 'ajax_file_upload',
	        dataType: 'json',
	        autoUpload: true,
	        paramName: 'files[]',
		    maxFileSize: 100000000, // 100 MB
		    limitConcurrentUploads: 5,
	        formData: {
	        	action: 'upload_client_file',
	        	client_id: client_id
	        },
	        add: function(e, data) {

	        	data.files[0].id = "file_" + file_count;

	        	files.push(data);

	        	var filename = data.files[0].name;
	        	var filesize = bytesToSize(data.files[0].size);

	        	$('#upload_client_files_modal .added_files').show();

	        	$('#upload_client_files_modal .added_files tbody').append('<tr><td>' + filename + '</td><td>' + filesize + '</td><td><div id="' + data.files[0].id + '" class="file_progress progress progress_thin progress-success progress-striped"><div class="bar"></div></div></td></tr>');

	            file_count++;

	            if (data.autoUpload || (data.autoUpload !== false &&
			            $(this).fileupload('option', 'autoUpload'))) {
			        data.process().done(function () {
			            data.submit();
			        });
			    }

			    $('.cancel_uploads').removeClass('disabled');

	        },
	        start: function(e) {

	   //      	$('.profile_picture .profile_picture_upload .fileinput-button').fadeOut();

				// $('.current_photo_wrapper').slideUp();

	   //      	$('#profile_picture_progress .bar').css('width', '0%');
	   //      	$('#profile_picture_progress').slideDown();

	        },
	        progress: function (e, data) {

	            var progress = parseInt(data.loaded / data.total * 100, 10);
	            var file_id = data.files[0].id;

	            if(progress >= 100) {

	            	setTimeout(function() {

	            		$('.added_files #' + file_id).closest('td').html('Complete');
	            	}, 750);

	            } else {
	            	
	            	$('.added_files #' + file_id + ' .bar').css('width', progress + '%');
	            }
	        },
	        done: function(e, data) {

	        	$('.cancel_uploads').addClass('disabled');

	        	//reload the client files on the page
	        	$.ajax({

			        url: 'ajax_functions',  //server script to process data
			        type: 'POST',
			        data: {
			        	action: "reload_client_files",
			        	client_id : client_id
			        }

			    }).done(function(data) {

					if(data) {

						$('.attachments .attachment:not(:first)').remove();
						$('.attachments .clearfix').remove();
						$('.attachments .box-message').remove();

						$('.attachments').append(data);
						$('.attachments').append('<div class="clearfix"></div>');
					}

				});

	        }

	    });


		$('.cancel_uploads').click(function() {

			for (i = 0; i < files.length; i++) { console.log(files);

				var file_id = files[i].files[0].id;

				$('.added_files #' + file_id).closest('td').html('Cancelled');

				files[i].abort(); 
			}

			$('.cancel_uploads').addClass('disabled');

			return false;
		});


		$('body').on('click', '.attachment .delete_file', function() {

			var file_id = $(this).closest('.attachment').attr('data-filename');

			$('#delete_client_file_modal #file_id').val(file_id);

			$('#delete_client_file_modal .modal-body').html('Are you sure you want to delete ' + file_id + '?<br><br><i class="icon-warning-sign"></i> This action is permanent and cannot be undone.');


		});


		$('body').on('click', '#delete_client_file_modal .delete', function() {

			$('#delete_client_file_modal').modal('hide');

			var filename = $('#delete_client_file_modal #file_id').val();
			var client_id = $('#delete_client_file_modal #client_id').val();

			var attachment = $(".client_files .attachment[data-filename='" + filename + "']");
			
			$(attachment).animate({

				opacity: 0.5,
				duration: 300
			});

			$.ajax({

				type: "POST",
				url: "ajax_functions",
				data: { 
					action: "delete_client_file",
					filename: filename,
					client_id: client_id
				}

			}).done(function(result) {	

				$(attachment).fadeOut(function() {

					$(attachment).remove();
				});
			});
			
			return false;
		});



		// $('.file').live('change', function(){

		// 	if($('.fileupload .fileupload-preview').html() != "") {

		// 		$('.fileupload .uneditable-input').css('background', '#FFFFFF');

		// 		 $('.fileupload .uneditable-input').css('width', '114px');

		// 		 var filename = $('.fileupload .fileupload-preview').text();

		// 		 var type = filename.split('.').pop();
		// 		 type = type.toLowerCase();

		// 		 if(type == "pdf") {
		// 		 	$('#doc_upload .thumbnail').removeClass().addClass("thumbnail thumbnail-pdf");
				 
		// 		 } else if(type == "jpg" || type == "jpeg" || type == "png" || type == "gif" || type == "bmp") {
		// 		 	$('#doc_upload .thumbnail').removeClass().addClass("thumbnail thumbnail-image");
				 
		// 		 } else if(type == "xls") {
		// 		 	$('#doc_upload .thumbnail').removeClass().addClass("thumbnail thumbnail-excel");
				 
		// 		 } else if(type == "zip" || type == "rar" || type == "tar" || type == "gz" || type == "xz") {
		// 		 	$('#doc_upload .thumbnail').removeClass().addClass("thumbnail thumbnail-zip");
				 
		// 		 } else if(type == "doc" || type == "docx") {
		// 		 	$('#doc_upload .thumbnail').removeClass().addClass("thumbnail thumbnail-word");
				 
		// 		 } else {

		// 		 	$('#doc_upload .thumbnail').removeClass().addClass("thumbnail thumbnail-plain");
		// 		 }

		// 	}
		// });


		// $('.file').live('change', function(){

		// 	if($('.fileupload .fileupload-preview').html() == "") {

		// 		 $('.fileupload .uneditable-input').css('width', '181px');
		// 	}
		// });

		// $('.fileupload .remove').click(function() {

		// 	$('#doc_upload .thumbnail').removeClass().addClass("thumbnail thumbnail-cloud");
		// });



		// $('#doc_upload #upload').click(function() {

		// 	if($('.fileupload .fileupload-preview').html() != "") {

		// 		$('.fileupload .uneditable-input').css('background', '#FFFFFF');

		// 		var formData = new FormData($('#doc_upload')[0]);
		// 	    $.ajax({

		// 	        url: 'doc_upload',  //server script to process data
		// 	        type: 'POST',
		// 	        data: formData,
		// 	        cache: false,
		// 	        contentType: false,
		// 	        processData: false

		// 	    }).done(function(data) {

		// 			if(data == "success") {


		// 			} else if(data == "failed") {


		// 			}
		// 		});

		// 	} else {

		// 		$('.fileupload .uneditable-input').css('background', '#FFCCCC');

		// 		return false;
		// 	}

		// 	//return false;
		// });



		$('body').on("click", ".delete_client", function() {

			var client_id = $('#delete_client_modal #client_id').val();
			var client_name = $('#delete_client_modal #client_name').val();

			$('#delete_client_modal .modal-body').html('Are you sure you want to delete ' + client_name + '\'s profile?<br><br><i class="icon-warning-sign"></i> This action is permanent and cannot be undone.');
	
		});


		$('body').on("click", "#delete_client_modal .delete", function() {

			var client_id = $('#delete_client_modal #client_id').val();

			$.ajax({

				type: "POST",
				url: "ajax_functions",
				data: { 
					action: "delete_client",
					client_id: client_id
				}

			}).done(function(result) {

				$("#delete_client_modal").modal('hide');

				window.location.href = "clients";

			});

			return false;
		});


	}




	if(page == "tasks") {

		if(hash == "add_new_task") {
			$('#add_task_modal').modal("show");
		}

		$('#sorting').change(function() {

			sort_tasks();

		});



		function reload_tasks(result) {

			$('#tasks_list').html(result);

			$('#tasks_list ol:empty').remove();
			$('#tasks_list .dd-item').has('ol').prepend('<button data-action="collapse" type="button">Collapse</button><button data-action="expand" type="button" style="display: none;">Expand</button>');

			sort_tasks();
			filter_tasks();

		}



		function sort_tasks() {

			var sorting = $('select#sorting option:selected').val();

			if(sorting == 'oldest_to_newest') {

				$('#tasks_list .dd-list>li').tsort({data:'id', order:'asc'});
			
			} else if(sorting == 'newest_to_oldest') {

				$('#tasks_list .dd-list>li').tsort({data:'id', order:'desc'});

			} else if(sorting == 'high_to_low') {

				$('#tasks_list .dd-list>li').tsort({data:'priority', order:'desc'});

			} else if(sorting == 'low_to_high') {

				$('#tasks_list .dd-list>li').tsort({data:'priority', order:'asc'});

			} else if(sorting == 'earliest_to_latest') {

				$('#tasks_list .dd-list>li').tsort({data:'duedate', order:'asc'}); ///////////

			} else if(sorting == 'latest_to_earliest') {

				$('#tasks_list .dd-list>li').tsort({data:'duedate', order:'desc'}); //////////

			} 
		}



		$('#search_filters li a').click(function() {

			setTimeout(function() {

				filter_tasks();
			}, 100);

		});



		function filter_tasks() {

			var filter = $('#search_filters li.active').attr('id');

			$('#tasks_list .no_results').remove();

			$('#tasks_list .dd-list .dd-item').removeClass('hidden');

			if(filter == 'all') {

				$('#tasks_list .dd-list .dd-item').removeClass('hidden');

			} else if(filter == 'today') { 

				var date = new Date();
				var today = date.getFullYear() + '-' + ("0" + (date.getMonth() + 1)).slice(-2) + '-' + ("0" + date.getDate()).slice(-2);

				$('#tasks_list .dd-list .dd-item').each(function() {

					var due_datetime = $(this).attr('data-duedate').split(' '); //split it into date and time

					if(due_datetime[0] == today) {

					} else {

						$(this).addClass('hidden');
					}

				});

			} else if(filter == 'tomorrow') {

				var date = new Date();
				var tomorrow_temp = new Date(date.getTime() + (24 * 60 * 60 * 1000));
				var tomorrow = tomorrow_temp.getFullYear() + '-' + ("0" + (tomorrow_temp.getMonth() + 1)).slice(-2) + '-' + ("0" + tomorrow_temp.getDate()).slice(-2);

				$('#tasks_list .dd-list .dd-item').each(function() {

					var due_datetime = $(this).attr('data-duedate').split(' '); //split it into date and time

					if(due_datetime[0] == tomorrow) {

					} else {

						$(this).addClass('hidden');
					}

				});

			} else if(filter == 'next_7_days') {

				var date = new Date();
				var next_7_days_array = new Array();

				for (var i = 0; i < 7; i++) {
					
					var temp = new Date(date.getTime() + (i * 24 * 60 * 60 * 1000));
					var temp2 = temp.getFullYear() + '-' + ("0" + (temp.getMonth() + 1)).slice(-2) + '-' + ("0" + temp.getDate()).slice(-2);
					next_7_days_array.push(temp2);

				};


				$('#tasks_list .dd-list .dd-item').each(function() {

					var due_datetime = $(this).attr('data-duedate').split(' '); //split it into date and time

					if($.inArray(due_datetime[0], next_7_days_array) > -1) {


					} else {

						$(this).addClass('hidden');
					}

				});

			} else if(filter == 'overdue') {


				$('#tasks_list .dd-list .dd-item').each(function() {

					var now = new Date();

					var due_datetime = $(this).attr('data-duedate').split(' '); //split it into date and time

					if(due_datetime[0] != '0000-00-00') {

						var due_date_array = due_datetime[0].split('-');
						var due_time_array = due_datetime[1].split(':');

						var due_datetime_obj = new Date(due_date_array[0], due_date_array[1] - 1, due_date_array[2], due_time_array[0], due_time_array[1], due_time_array[2]);


						if(now.getTime() > due_datetime_obj.getTime()) {
						    
						} else {

							$(this).addClass('hidden');
						}

					} else {

						$(this).addClass('hidden');
					}

				});

			}


			if($('#tasks_list .dd-list').children(':visible').length == 0 && filter != 'all') {

				$('#tasks_list').append('<span class="no_results">No tasks found – Filter: ' + filter.replace(/_/g, ' '));
			}

		}


		$('#tasks_list ol:empty').remove();

		$('#tasks_list').nestable({
			dropCallback: function(draggedEl) {

			    var task_id = draggedEl.sourceId;
			    
			    var parent_id = draggedEl.destId;

			    $('#tasks_list ol.dd-list .dd-item[data-id=' + task_id + ']').attr('data-parent', parent_id);

			    saveTasks();
			}
		});

		// .selectable({ filter: ".dd-list>.dd-item", cancel: ".dd-handle" })
		//     .find( "li" )
		//         .addClass( "ui-corner-all" )
		//         .prepend( "<div class='handle'><span class='ui-icon ui-icon-carat-2-n-s'></span></div>" );


		$(".date-picker").datetimepicker({
        	format: 'F j, Y',
            closeOnDateSelect: true,
            timepicker: false,
            validateOnBlur: false
        });

        $(".time-picker").datetimepicker({
        	format: 'g:iA',
        	formatTime: 'g:iA',
        	step: 15,
            closeOnDateSelect: true,
            datepicker: false,
            validateOnBlur: false
        });


		// $('body').on("fadeInComplete", function(){

		// 	$sizer = $('.tasks_list .shuffle_sizer');

		// 	$('.tasks_list').shuffle({
		// 		itemSelector: '.task_item'
		// 	});
		// });


		
		$('#show_add_task_modal').click(function() {

			//load all the tasks for parent dropdown!
			$.ajax({

				type: "POST",
				url: "ajax_functions",
				data: { 
					action: "load_tasks_list_dropdown"
				}

			}).done(function(data) {
				
				$('#add_task_modal #parent').html('<option value="">No parent</option>' + data);

			});
			

		});



		$('#add_task_modal .add_task').click(function() {


			var title = $('#add_task_modal #title').val();

			var priority = $('#add_task_modal input[name="priority"]:checked').val();
			var parent = $('#add_task_modal #parent option:selected').val();

			var due_date = $('#add_task_modal #due_date').val();
			var due_time = $('#add_task_modal #due_time').val();
			var due_datetime = datetime_convert(due_date, due_time);
			
			var reminder = $('#add_task_modal #reminder').val();

			$.ajax({
				type: "POST",
				url: "ajax_functions",
				data: { 
					action: "add_task",
					title: title,
					priority: priority,
					parent: parent,
					due_datetime: due_datetime,
					reminder: reminder
				}

			}).done(function(result) {

				reload_tasks(result);
			});

		});



		$('body').on('click','#tasks_list .dd-item .task_complete', function() {

			$(this).removeClass('icon-ok').addClass('loading_inline');

			var sorting = $('select#sorting option:selected').val();
			var filter = $('#search_filters li.active').attr('id');

			var task_ids = new Array();

			var task = $(this).closest('li');

			var primary_task_id = $(this).closest('li').attr('data-id');
			var completed = $(this).closest('li').attr('data-completed');

			if(completed == 1) {

				completed = 0;
			
			} else if(completed == 0) {

				completed = 1;
			}


			task_ids.push(primary_task_id);

			$(task).find('.dd-item').each(function() {

				var task_id = $(this).attr('data-id');
				task_ids.push(task_id);
			});


			$.ajax({
				type: "POST",
				url: "ajax_functions",
				data: { 
					action: "mark_tasks_as_complete",
					task_ids: task_ids,
					completed: completed,
					sorting: sorting,
					filter: filter
				}

			}).done(function(result) {

				reload_tasks(result);

			});
 
			
		});




		$('body').on('click','#tasks_list .dd-item .edit_task', function() {

			$('#edit_task_modal').modal('show');

			var task = $(this).closest('li');

			var task_id = $(task).attr('data-id');
			var task_title = $(task).attr('data-task');
			var task_parent = $(task).attr('data-parent');
			var task_priority = $(task).attr('data-priority');
			var task_duedate = $(task).attr('data-duedate');
			var task_completed = $(task).attr('data-completed');
			var task_notificationtime = $(task).attr('data-notificationtime');

			$('#edit_task_modal #task_id').val(task_id);

 			$('#edit_task_modal #title').val(task_title);
 			$('#edit_task_modal input[name=priority][value=' + task_priority + ']').prop('checked',true);

 			$('#edit_task_modal #reminder option[value=' + task_notificationtime + ']').prop('selected',true);

 			// Split datetime into [ Y, M, D, h, m, s ]
 			var task_duedate_array = task_duedate.split(/[- :]/);

 			if(task_duedate_array[0] != '0000') {
 			
				// Apply each element to the Date function
				var due_datetime = new Date(task_duedate_array[0], task_duedate_array[1]-1, task_duedate_array[2], task_duedate_array[3], task_duedate_array[4], task_duedate_array[5]);


				var due_ampm = "AM";

				var due_date = months[due_datetime.getMonth()] + ' ' + due_datetime.getDate() + ', ' + due_datetime.getFullYear();

	        	var due_hour = due_datetime.getHours();
	        	var due_min = due_datetime.getMinutes();

			    if (due_hour >= 12) {
			        due_hour = due_hour - 12;
			        due_ampm = "PM";
			    }
			    if (due_hour == 0) {
			        due_hour = 12;
			        //Stays as AM.
			    }

			    if (due_min < 10) {
					due_min = "0" + due_min;
				}

	        	var due_time = due_hour + ':' + due_min + due_ampm;


	 			$(".date-picker").datetimepicker({
	 				value: due_date,
		        	format: 'F j, Y',
		        	validateOnBlur: false
		        });

		        $(".time-picker").datetimepicker({
		        	value: due_time,
		        	format: 'g:iA',
		        	validateOnBlur: false
		        });

		    }

 			//load all the tasks for parent dropdown!
			$.ajax({

				type: "POST",
				url: "ajax_functions",
				data: { 
					action: "load_tasks_list_dropdown"
				}

			}).done(function(data) {
				
				$('#edit_task_modal #parent').html('<option value="">No parent</option>' + data);

				$('#edit_task_modal #parent option[value=' + task_id + ']').remove(); // Remove the task itself from the parent dropdown -- cannot be its own parent.

				$('#edit_task_modal #parent option[value=' + task_parent + ']').prop('selected',true);

			});
			
		});




		$('#edit_task_modal .update_task').click(function() {


			var id = $('#edit_task_modal #task_id').val();
			var title = $('#edit_task_modal #title').val();

			var priority = $('#edit_task_modal input[name="priority"]:checked').val();
			var parent = $('#edit_task_modal #parent option:selected').val();

			var due_date = $('#edit_task_modal #due_date').val();
			var due_time = $('#edit_task_modal #due_time').val();
			var due_datetime = datetime_convert(due_date, due_time);

			var reminder = $('#edit_task_modal #reminder').val();

			$.ajax({
				type: "POST",
				url: "ajax_functions",
				data: { 
					action: "update_task",
					id: id,
					title: title,
					priority: priority,
					parent: parent,
					due_datetime: due_datetime,
					reminder: reminder
				}

			}).done(function(result) {

				reload_tasks(result);

			});

		});




		$('body').on('click','#tasks_list .dd-item .remove_task', function() {

			var task = $(this).closest('li');
			var task_id = $(task).attr('data-id');

			if($(task).find('ol.dd-list').length) {

				$('#remove_task_modal #task_id').val(task_id);

				$('#remove_task_modal').modal('show');
			
			} else {

				removeTasks(task_id, false);
			}
			
		});


		$('#remove_task_modal input#yes').click(function() {

			var task_id = $('#remove_task_modal #task_id').val();

			removeTasks(task_id, true);
		});



		$('#remove_task_modal input#no').click(function() {

			var task_id = $('#remove_task_modal #task_id').val();

			removeTasks(task_id, false);
		});



		function removeTasks(task_id, remove_sub_tasks) {

			var task = $('#tasks_list .dd-list .dd-item[data-id=' + task_id + ']');

			var task_ids = new Array();

			if(remove_sub_tasks == true) {

				task_ids.push(task_id);

				$(task).find('.dd-item').each(function() {

					var task_id = $(this).attr('data-id');
					task_ids.push(task_id);
				});


				$(task).css('opacity', '.4');

				$.ajax({
					type: "POST",
					url: "ajax_functions",
					data: { 
						action: "delete_tasks",
						task_ids: task_ids
					}

				}).done(function(result) {
					

					reload_tasks(result);

				});

			} else if(remove_sub_tasks == false) {

				task_ids.push(task_id);

				var parent_id = $('#tasks_list .dd-list .dd-item[data-id=' + task_id + ']').attr('data-parent'); //used only for when subtasks are to be preseved.

				$(task).css('opacity', '.4');


				$.ajax({
					type: "POST",
					url: "ajax_functions",
					data: { 
						action: "delete_tasks",
						task_ids: task_ids,
						preserve_subtasks: 1,
						parent_id: parent_id
					}

				}).done(function(result) {


					reload_tasks(result);

				});
 			}

		}



		function saveTasks() {


			var tasks_object = $('#tasks_list').nestable('serialize');

			$.ajax({

				type: "POST",
				url: "ajax_functions",
				data: { 
					action: "save_tasks",
					tasks_list: tasks_object
				}

			}).done(function(result) {
				
			});

		}


		function datetime_convert(due_date, due_time) {

			var due_datetime, timestamp;

        	due_time = due_time.trim();

        	//If time is NOT empty, then process it and add it to datetime. Otherwise datetime = date only.
        	if(due_time != "") {
	        	var ampm = due_time.substring(due_time.length - 2, due_time.length);
	        	due_time = due_time.substring(0, due_time.length - 2);

	        	var temp = due_time.split(":");
	        	var hour = temp[0];
	        	var min = temp[1];

	        	
	        	if(ampm.toUpperCase() == "PM" && hour < 12) {
	        		hour = parseInt(hour) + 12;
	        	}

	    		if(ampm.toUpperCase() == "AM" && hour == 12) {
	    			hour = parseInt(hour) - 12;
	    		}

	    		if(parseInt(hour) > 23 || parseInt(hour) < 0 || parseInt(min) > 59 || parseInt(min) < 0) {
	        		$('#add_task_modal #due_time').addClass("error");
	        		$('#add_task_modal #due_time').focus();
	        	
	        	} else {
	        		due_time = hour + ':' + min;

	        		due_datetime = due_date + ' ' + due_time;
		        	due_datetime = new Date(Date.parse(due_datetime));
		        	timestamp = (new Date(due_datetime).getTime()) / 1000; //getTime returns time in milliseconds, need it in seconds so divide by 1000

	        	}

	    	} else {
        	
	        	due_datetime = due_date;
	        	due_datetime = new Date(Date.parse(due_datetime));
	        	timestamp = (new Date(due_datetime).getTime()) / 1000;
	        }

	        return timestamp;

        }

	}




	if(page == "notes") {

		var requests = new Array();
		var autosaved = true; //boolean to determine if autosaved or manually saved.
		var saved = false; //boolean to determine if changes have been saved at all.

		//The timeout is because the container div takes 300ms to load first! 
		$('body').on("fadeInComplete", function(){

			$sizer = $('.notes .note_item');

			$('.notes').shuffle({
				itemSelector: '.note_item',
				sizer: $sizer
			});
		});

		$('.notepad').height(height - 257);
		$('.notes_wrapper').height(height - 225);

		$('body').on("fadeInComplete", function() {
		
			$(".notepad").sceditor({
	        	plugins: "xhtml",
	        	style: "css/sceditor.min.css",
	        	toolbar: "bold,italic,underline,strike,subscript,superscript|left,center,right|bulletlist,orderedlist",
	        	emoticonsEnabled: false
	    	});

	    	$('.sceditor-container').height(height - 225);

	    	var first_note_title = $('.notes .note_item:first .note_item_title').text();
	    	var first_note_id = $('.notes .note_item:first').attr('id');
	    	var first_note_content = $('.notes .note_item:first .note_item_content').val();

	    	$('.note_title').text(first_note_title);
	    	$('#note_id').val(first_note_id);
	    	$('.notepad').sceditor('instance').val(first_note_content);

	    	$('.notes .note_item:first').addClass('active');
	    	$('.notes .note_item:first').addClass('changed');

	    	var iframe= $('iframe')[0];
			var iframewindow= iframe.contentWindow? iframe.contentWindow : iframe.contentDocument.defaultView;

			$(iframewindow.document).keyup(function(e) {

				var note_id = $('#note_id').val();
				var content = $('.notepad').sceditor('instance').val();
				//var content_html = $(content);

				$('#tmp_content').append(content);

				var title = $('#tmp_content').contents()
                       .filter(function() { 
                           return $.trim( this.innerHTML || this.data ); 
                       })
                       .first().text();

                title = $.trim(title);

        		$('#tmp_content').html('');

				if(title == '') {

					title = '(no title)';
				}

				$('.note_title').text(title);
				$('.notes .note_item#' + note_id + ' .note_item_title').text(title);

				///////////
				if($('.save_notes').hasClass('disabled')) {

					$('.save_notes').removeClass('disabled');
					$('.save_notes').html('Save Notes');

					//$('.autosave_note').html('');
					$('.save_notes').attr('title', '');

					confirm_pageunload = true;
				}

				saved = false;
			   
			});

			$(iframewindow).blur(function() {

				//UPDATE & SAVE THE NOTES
				autosaved = true;
				update_notes();
			   
			});


	    });


		$('body').on('click', '.notes .note_item', function(e) {

			//Ignore clicks on the trash icon for this...
			if($(e.target).is('i.icon-trash')){
	            e.preventDefault();
	            return;
	        }

			$('.notes .note_item').removeClass('active');
			$(this).addClass('active');
			$(this).addClass('changed');

			//save currently loaded note
			autosaved = true;
			update_notes();

			//load the selected note
			var title = $(this).find('.note_item_title').text();
			var id = $(this).attr('id');
			var content = $(this).find('.note_item_content').val();

			$('.note_title').text(title);
			$('#note_id').val(id);
			$('.notepad').sceditor('instance').val(content);

		});



		$('.new_note').click(function() {

			update_notes();

			var timestamp = new Date().getTime();

			// var new_div = $('<div id="' + timestamp + '" class="note_item span4 m-span4 new">' +
		 //                     	'<div class="note_header">' +
		 //                     		'<input type="text" class="note_title span11" placeholder="Title..." tabindex="1" />' +
		 //                     		'<a href="#delete_note_modal" class="delete_note span1" data-dismiss="modal" data-toggle="modal"><i class="icon-remove"></i></a>' +
		 //                     		'<div class="clearfix"></div>' +
		 //                     	'</div>' +
		 //                     	'<div class="note_content">' +
		 //                     		'<textarea class="note_edit" tabindex="2"></textarea>' +
		 //                     	'</div>' +
		 //                     '</div>');

			var new_div = $('<li class="note_item active changed new" id="' + timestamp + '">' +
	                           '<span class="note_item_title">(no title)</span>' +
	                           '<span class="note_item_date">Just now</span>' +
	                           '<a href="#delete_note_modal" class="delete_note right" data-toggle="modal"><i class="icon-trash"></i></a>' +
	                           '<input type="hidden" class="note_item_content" value="">' +
	                        '</li>');

			$('.notes .note_item').removeClass('active');

			$('.notes').prepend(new_div);

			$('.notes').shuffle('appended', new_div);
			$('.notes').shuffle('layout');


			$('.note_title').text('(no title)');
			$('#note_id').val(timestamp);

			$('.notepad').sceditor('instance').val('');
			$('.notepad').sceditor('instance').focus();

			return false;
		});


		function update_notes() {

			var title = $('.note_title').text();
			var id = $('#note_id').val();
			var content = $('.notepad').sceditor('instance').val();

			$('.notes .note_item#' + id + '').find('.note_item_title').text(title);
			$('.notes .note_item#' + id + '').find('.note_item_content').val(content);

			save_notes();

		}


		$('.search_notes').on('keyup change', function() {

		  var val = this.value.toLowerCase();

		  $('.notes').shuffle('shuffle', function($el, shuffle) {

		    // Only search elements in the current group
		    // if (shuffle.group !== 'all' && $.inArray(shuffle.group, $el.data('groups')) === -1) {
		    //   return false; 
		    // }

		    var text = $.trim( $el.find('.note_item_title').text() ).toLowerCase();
		    text += ' ' + $.trim( $el.find('.note_item_content').val() ).toLowerCase();
		    text += ' ' + $.trim( $el.find('.note_item_date').text() ).toLowerCase();
		    return text.indexOf(val) !== -1;
		  });

		});


		$('.save_notes').click(function() {

			if(!$(this).hasClass('disabled')) {

				autosaved = false;
				update_notes(); //this will call save_notes also.
			}

			return false; 
		});



		$('body').on('click', '.note_item .delete_note', function() {

			var note_id = $(this).closest('.note_item').attr('id');

			$('#delete_note_modal .note_id').val(note_id);
		});


		$('#delete_note_modal .delete').click(function() {

			$('#delete_note_modal').modal("hide");

			var note_id = $('#delete_note_modal .note_id').val();

			if(!$('.note_item#' + note_id).hasClass('new')) {
				delete_note(note_id);
			}

			return false;

		});

		function delete_note(note_id) {

			$.ajax({

				type: "POST",
				url: "ajax_functions",
				data: { 
					action: "delete_note",
					note_id: note_id
				}

			}).done(function(result) {

				var item = $('.note_item#' + note_id);
				$('.notes').shuffle('remove', item);

			});

		}


		setInterval(function() {

			if(saved == false) {

				autosaved = true;

				//update and save notes
				update_notes();
			}

		}, 7500);


		function save_notes() {

			var notes = new Array();


			//If autosave triggerd...
			if(autosaved == true) {

				$('.note_item.changed').each(function() {

					var new_note = false;

					if($(this).hasClass('new')) {
						new_note = true;
					}

					var note_id = $(this).attr('id');
					var note_title = $(this).find('.note_item_title').text();
					var note_content = $(this).find('.note_item_content').val();

					var note_instance = [note_id, note_title, note_content, new_note];

					notes.push(note_instance);

				});

			} else { //If manually clicked on save notes...

				$('.note_item').each(function() {

					var new_note = false;

					if($(this).hasClass('new')) {
						new_note = true;
					}

					var note_id = $(this).attr('id');
					var note_title = $(this).find('.note_item_title').text();
					var note_content = $(this).find('.note_item_content').val();

					var note_instance = [note_id, note_title, note_content, new_note];

					notes.push(note_instance);

				});
			}


			if(notes.length > 0) {

				$('.autosave_note').html('');

				$('.save_notes').addClass('disabled');
				$('.save_notes').html('Saving...');


				for(var i = 0; i < requests.length; i++) {
	    			requests[i].abort();
				}

				requests = [];

	        	requests.push(
					$.ajax({

						type: "POST",
						url: "ajax_functions",
						data: { 
							action: "save_notes",
							notes: notes
						}

					}).done(function(result) {

						notes = [];

						saved = true;

						$('.note_item').removeClass('changed');
						$('.note_item.active').addClass('changed');

						confirm_pageunload = false;

						if(result != "") {

							var new_ids = result.split(',');

							for(var i = 0; i < new_ids.length; i++) {

								var note_ids = new_ids[i].split('-');

								$('.note_item#' + note_ids[0]).attr('id', note_ids[1]);
								$('.note_item#' + note_ids[1]).removeClass('new');

								//replace temporary timestamp IDs with new IDs returned from rows inserted
								//result is returned as a string in the format of: 'old-new,old-new,old-new'
							}
						}


						$('.save_notes').addClass('disabled');
						$('.save_notes').html('<i class="icon-ok"></i> Saved');


						var saved_timestamp = new Date();

						var hour = saved_timestamp.getHours();
						var minute = saved_timestamp.getMinutes();
						var timestamp;

						var saved_am_pm = "AM";

						if(hour >= 12) {
							saved_am_pm = "PM";
						}

					    if (hour > 12) {
					        hour = hour - 12;
					    }

						if (minute < 10) {

							minute = "0" + minute;
						}

						timestamp = hour + ":" + minute + " " + saved_am_pm;

						if(autosaved == true) {

							//$('.autosave_note').text('Autosaved at: ' + timestamp);
							$('.save_notes').attr('title', 'Autosaved at: ' + timestamp);
						
						} else if(autosaved == false) {

							//$('.autosave_note').text('Saved at: ' + timestamp);
							$('.save_notes').attr('title', 'Saved at: ' + timestamp);
						}

					})
				);
			}
		}

	}


	// UPDATE NOTIFICATIONS DROPDOWN //

	function updateNotificationsDropdown(which) { //This function will load the LI items into the hidden dropdowns after a new notification is added. This ensures that by the time the user opens the dropdown, the LI's have been updated to the new notifications.

		$.ajax({

			type: "POST",
			url: "ajax_functions",
			data: { 
				action: "update_notifications_dropdown",
				which: which
			}

		}).done(function(data) {

			if(which == "messages") {

				$('.messages_notification ul').html(data);
			
			} else if(which == "events") {

				$('.events_notification ul').html(data);
			
			} else if(which == "tasks") {

				$('.tasks_notification ul').html(data);
			
			}

			updateTotalNotifications(); //located in notifications_client.js
		});
	}


	$('body').on('click', '.dropdown.messages_notification' ,function () {

		setTimeout(function() {

			if($('.dropdown.messages_notification').hasClass('open')) {

				updateNotificationsDropdown("messages");

				$.ajax({

					type: "POST",
					url: "ajax_functions",
					data: { 
						action: "clear_messages_notifications"
					}

				}).done(function(data) {


				});

				$(this).find('.messages_notification_badge').remove();
				messages_notifications = 0;

				convo_notifs_array = [];

			}

		}, 100);

	});


	$('body').on('click', '.dropdown.events_notification' ,function () {

		setTimeout(function() {

			if($('.dropdown.events_notification').hasClass('open')) {

				updateNotificationsDropdown("events");

				$.ajax({

					type: "POST",
					url: "ajax_functions",
					data: { 
						action: "clear_events_notifications"
					}

				}).done(function(data) {


				});

				$(this).find('.events_notification_badge').remove();
				events_notifications = 0;

			}

		}, 100);

	});


	$('body').on('click', '.dropdown.tasks_notification' ,function () {

		setTimeout(function() {

			if($('.dropdown.tasks_notification').hasClass('open')) {

				updateNotificationsDropdown("tasks");

				$.ajax({

					type: "POST",
					url: "ajax_functions",
					data: { 
						action: "clear_tasks_notifications"
					}

				}).done(function(data) {


				});

				$(this).find('.tasks_notification_badge').remove();
				tasks_notifications = 0;

			}

		}, 100);

	});

	//CRUCIAL

	var fade_in_complete = false;

	$(window).on('load',function() {

		$('.loading_page').fadeOut(100);

		$('.page-content .container-fluid').fadeIn(200, function() {

			//execute actions that need visible divs! For height and width determination mostly...
			$('body').trigger("fadeInComplete"); 

			fade_in_complete = true;

		});

		$('.sidebar_glow').fadeIn(500);
	});


	$(window).on('beforeunload',function() {

		if(confirm_pageunload == false) {

		   	$(".sidebar_glow").fadeOut(1000);

			$('.page-content .container-fluid').fadeOut(100);
			$('#date_time_weather').fadeOut();

			$('.loading_page').fadeIn(300);

		
		} else if(confirm_pageunload == true) {

			return 'You have unsaved content on this page.';
		}
		
	});


	// ****************************** //

	function ajaxVerifyUser() {

		//verify user from session variables
		// user_id, account_id are stored as global variables in the footer.php from PHP

		var status;

		$.ajax({

			type: "POST",
			url: "ajax_functions",
			data: { 
				action: "verify_user",
				user_id: user_id,
				account_id: account_id
			},
			async: false

		}).done(function(data) {

			var result = jQuery.parseJSON(data);
			status = result.status;

			if(status == false) { //user_id and account_id do NOT match the session

				user_id = result.user_id;
				account_id = result.account_id;
			}

		});

		return status;

	}

	// ============================== //

	//Modify sisyphus to be more specific towards each page. Triggering all forms across all pages is a hassle.
	// $("form").sisyphus({
	// 	excludeFields: $('.top_bar_search .search_query');
	// });

});


$(document).unbind('keydown').bind('keydown', function (event) {
    var doPrevent = false;
    if (event.keyCode === 8) {
        var d = event.srcElement || event.target;
        if ((d.tagName.toUpperCase() === 'INPUT' && (d.type.toUpperCase() === 'TEXT' || d.type.toUpperCase() === 'PASSWORD' || d.type.toUpperCase() === 'FILE')) 
             || d.tagName.toUpperCase() === 'TEXTAREA') {
            doPrevent = d.readOnly || d.disabled;
        }
        else {
            doPrevent = true;
        }
    }

    if (doPrevent) {
        event.preventDefault();
    }
});



function loadConversation(convo_id) {

	$('.messages_list').removeClass('span12');
	$('.messages_list').addClass('span4');

	$('.message_box').removeClass('span0');
	$('.message_box').addClass('span8');

	setTimeout(function() {

		$('.message_box').html('<div class="loading_message"><img src="img/loading.gif"></div>');
		$('.loading_message').width($('.message_box').width());

	}, 400);

	$.ajax({

		type: "POST",
		url: "ajax_functions",
		data: { 
			action: "load_conversation", 
			convo_id: convo_id
		},
		async: true

	}).done(function(data) {

		$('.message_box').html(data);

		var messages_div_height = $('#messages .chats').height();
		var height = $(window).height();
		//alert($('#messages .slimScrollDiv').length);

		$('#messages .scroller').slimScroll({
			height: (height - 177) + 'px'
			
		});

		//$('#messages .slimScrollDiv').height(height - 177); 
		//$('#messages .slimScrollDiv .scroller').height(height - 177); 

		messages_div_height + 'px';
		$('#messages .scroller').slimScroll({
		    scrollTo: messages_div_height
		});

		$('#messages .chats').animate({
			opacity: 1,
			duration: 200
		}, function() {

			$('ul.chats .body').each(function() {

				var text = $(this).html();
				text = linkify(text);

				$(this).html(text);
			});
		});


	});
}

//This is not working as it should. The table row is added, but the table Object is still the old one.
//Need to find a way to refresh the table object with the new rows
function reloadConversationList() {

	$.ajax({

		type: "POST",
		url: "ajax_functions",
		data: { 
			action: "reload_conversation_list"
		}

	}).done(function(data) {

		var messages_table = $('.messages_table').dataTable();
   
		messages_table.fnDestroy();


		$(messages_table).find('tbody').html(data);


		messages_table.dataTable({
            "aLengthMenu": [
                [5, 10, 20, -1],
                [5, 10, 20, "All"] // change per page values here
            ],
            // set the initial value
            "iDisplayLength": 10,
            "sDom": "<'row-fluid'<'span6'l><'span6 search'f>r>t<'row-fluid'<'span6'i><'span6 hidden-print'p>>",
            "sPaginationType": "bootstrap",
            "oLanguage": {
                "sLengthMenu": "_MENU_ Records per page",
                "oPaginate": {
                    "sPrevious": "Prev",
                    "sNext": "Next"
                }
            },
            "aoColumnDefs": [{
                    'bSortable': false,
                    'aTargets': [0,1]
                }
            ]
        });
						
	});
}


function linkify(text) {  
    var urlRegex =/(\b(https?:\/\/|ftp:\/\/|file:\/\/|www.)[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;  
    return text.replace(urlRegex, function(url) {  

    	var http_url; 

		if(/~^(?:f|ht)tps?:/i.test(url)) {
	        
	        http_url = url;
	    
	    } else {

	    	http_url = "http://" + url;
	    }

        return '<a href="' + http_url + '" target="_blank">' + url + '</a>';  
    });  
}



var available_space = 0;
function calculateStorageCap() {

	var storage_stats_result;

	$.ajax({

		type: "POST",
		url: "ajax_functions",
		async: false,
		data: { 
			action: "calculate_storage_cap"
		}

	}).done(function(result) {

		storage_stats_result = result;
	});

	return storage_stats_result;

}



jQuery.fn.center = function () {
    this.css("position","absolute");
    this.css("top", Math.max(0, (($(window).height() - $(this).outerHeight()) / 2) + 
                                                $(window).scrollTop()) + "px");
    this.css("left", Math.max(0, (($(window).width() - $(this).outerWidth()) / 2) + 
                                                $(window).scrollLeft()) + "px");
    return this;
}

function shuffle(o){ //v1.0
    for(var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
    return o;
};


function bytesToSize(bytes) {
   if(bytes == 0) return '0 Byte';
   var k = 1024;
   var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
   var i = Math.floor(Math.log(bytes) / Math.log(k));
   return (bytes / Math.pow(k, i)).toPrecision(3) + ' ' + sizes[i];
}



// Number.prototype.formatMoney = function(c, d, t) {
// 	alert(this);
// 	var n = this, 
//     c = isNaN(c = Math.abs(c)) ? 2 : c, 
//     d = d == undefined ? "." : d, 
//     t = t == undefined ? "," : t, 
//     s = n < 0 ? "-" : "", 
//     i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", 
//     j = (j = i.length) > 3 ? j % 3 : 0;

// 	return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");

// };


function fixDecimal(n) {

	var precision = 2;

	var multiplier = Math.pow( 10, precision + 1 ),
        wholeNumber = Math.floor( n * multiplier );

    n = (Math.round( wholeNumber / 10 ) * 10 / multiplier).toFixed(2);

    return n;
}

function formatMoney(n) {

    return n.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

}



// function addNewClient() {

// 	$('.loading_large').fadeIn('fast');

// 	var success = false;

// 	var client_type = $('#add_new_client_modal #client_type').val(); 
// 	var first_name = $('#add_new_client_modal #first_name').val();
// 	var last_name = $('#add_new_client_modal #last_name').val();
// 	var ind_email = $('#add_new_client_modal #individual_email').val();
// 	var gender = $('#add_new_client_modal #gender').val();
// 	var primary_phone = $('#add_new_client_modal #primary_phone').val();
// 	var alternative_phone = $('#add_new_client_modal #alternative_phone').val();
// 	var address = $('#add_new_client_modal #add_client_geo_address').val();

// 	var company_name = $('#add_new_client_modal #business_name').val();
// 	var company_email = $('#add_new_client_modal #business_email').val();

// 	$.ajax({

// 		type: "POST",
// 		url: "ajax_functions",
// 		async: false,
// 		data: { 
// 			action: "add_client",
// 			client_type: client_type,
// 			first_name: first_name, 
// 			last_name: last_name,
// 			ind_email: ind_email,
// 			gender: gender,
// 			primary_phone: primary_phone,
// 			alternative_phone: alternative_phone,
// 			address: address,
// 			business_name: business_name,
// 			business_email: business_email
// 		}

// 	}).done(function(result) {

// 		$('.loading_large').fadeOut('fast');

// 	});

// 	return true;
// }



var FormValidation = function () {


    return {
        //main function to initiate the module
        init: function () {

            $('#add_new_client_modal form').validate({
                rules: {
                    name: {
                        minlength: 2,
                        required: true
                    },
                    email: {
                        required: true,
                        email: true
                    },
                    url: {
                        required: true,
                        url: true
                    },
                    number: {
                        required: true,
                        number: true
                    },
                    digits: {
                        required: true,
                        digits: true
                    },
                    creditcard: {
                        required: true,
                        creditcard: true
                    },
                    occupation: {
                        minlength: 5,
                    },
                    category: {
                        required: true
                    }
                },

                invalidHandler: function (event, validator) {            
                    
                },

                highlight: function (element) {

                    $(element).addClass('error');
                },

                unhighlight: function (element) {

                    $(element).removeClass('error');
                },

                success: function (label) {
                    //label.addClass('valid').addClass('help-inline ok') // mark the current input as valid and display OK icon
                    //.closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                },

                submitHandler: function (form) {
                    $(form).children.removeClass('error');
                }
            });

            // //Sample 2
            // $('#form_2_select2').select2({
            //     placeholder: "Select an Option",
            //     allowClear: true
            // });

            // var form2 = $('#form_sample_2');
            // var error2 = $('.alert-error', form2);
            // var success2 = $('.alert-success', form2);

            // form2.validate({
            //     errorElement: 'span', //default input error message container
            //     errorClass: 'help-inline', // default input error message class
            //     focusInvalid: false, // do not focus the last invalid input
            //     ignore: "",
            //     rules: {
            //         name: {
            //             minlength: 2,
            //             required: true
            //         },
            //         email: {
            //             required: true,
            //             email: true
            //         },
            //         category: {
            //             required: true
            //         },
            //         options1: {
            //             required: true
            //         },
            //         options2: {
            //             required: true
            //         },
            //         occupation: {
            //             minlength: 5,
            //         },
            //         membership: {
            //             required: true
            //         },
            //         service: {
            //             required: true,
            //             minlength: 2
            //         }
            //     },

            //     messages: { // custom messages for radio buttons and checkboxes
            //         membership: {
            //             required: "Please select a Membership type"
            //         },
            //         service: {
            //             required: "Please select  at least 2 types of Service",
            //             minlength: jQuery.format("Please select  at least {0} types of Service")
            //         }
            //     },

            //     errorPlacement: function (error, element) { // render error placement for each input type
            //         if (element.attr("name") == "education") { // for chosen elements, need to insert the error after the chosen container
            //             error.insertAfter("#form_2_education_chzn");
            //         } else if (element.attr("name") == "membership") { // for uniform radio buttons, insert the after the given container
            //             error.addClass("no-left-padding").insertAfter("#form_2_membership_error");
            //         } else if (element.attr("name") == "service") { // for uniform checkboxes, insert the after the given container
            //             error.addClass("no-left-padding").insertAfter("#form_2_service_error");
            //         } else {
            //             error.insertAfter(element); // for other inputs, just perform default behavoir
            //         }
            //     },

            //     invalidHandler: function (event, validator) { //display error alert on form submit   
            //         success2.hide();
            //         error2.show();
            //         App.scrollTo(error2, -200);
            //     },

            //     highlight: function (element) { // hightlight error inputs
            //         $(element)
            //             .closest('.help-inline').removeClass('ok'); // display OK icon
            //         $(element)
            //             .closest('.control-group').removeClass('success').addClass('error'); // set error class to the control group
            //     },

            //     unhighlight: function (element) { // revert the change dony by hightlight
            //         $(element)
            //             .closest('.control-group').removeClass('error'); // set error class to the control group
            //     },

            //     success: function (label) {
            //         if (label.attr("for") == "service" || label.attr("for") == "membership") { // for checkboxes and radip buttons, no need to show OK icon
            //             label
            //                 .closest('.control-group').removeClass('error').addClass('success');
            //             label.remove(); // remove error label here
            //         } else { // display success icon for other inputs
            //             label
            //                 .addClass('valid').addClass('help-inline ok') // mark the current input as valid and display OK icon
            //             .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            //         }
            //     },

            //     submitHandler: function (form) {
            //         success2.show();
            //         error2.hide();
            //     }

            // });

            //apply validation on chosen dropdown value change, this only needed for chosen dropdown integration.
            // $('.chosen, .chosen-with-diselect', form2).change(function () {
            //     form2.validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
            // });

            //  //apply validation on select2 dropdown value change, this only needed for chosen dropdown integration.
            // $('.select2', form2).change(function () {
            //     form2.validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
            // });

        }

    };

}();