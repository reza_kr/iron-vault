$(document).ready(function() {

	var processing = false;

	var newMessage = {

		init: function() {

			$('.send_message').off('click');
			$('.send_message').click(function() {
				newMessage.sendMessage();
			});

			//AUDIO ELEMENT

			sound = new Audio();

			if(sound.canPlayType('audio/mpeg')) {
				sound.src = 'audio/notif.mp3';
			} else if(sound.canPlayType('audio/ogg')) {
				sound.src = 'audio/notif.ogg';
			} else {
				sound.src = null;
			}



			$('body').on('click', '.btn-sound', function() {

				if($(this).hasClass("on")) {

					$(this).removeClass("on");
					$(this).addClass("off");

					$(this).find("a").removeClass("light-blue");
					$(this).find("a").addClass("grey");

					$(this).find("i").removeClass("icon-volume-up");
					$(this).find("i").addClass("icon-volume-off");


					$.ajax({

						type: "POST",

						url: "ajax_functions",

						data: { action: "toggle_sound", state: "off" }

					}).done(function(result) {


					});


				} else if($(this).hasClass("off")) {

					$(this).removeClass("off");
					$(this).addClass("on");

					$(this).find("a").removeClass("grey");
					$(this).find("a").addClass("light-blue");

					$(this).find("i").removeClass("icon-volume-off");
					$(this).find("i").addClass("icon-volume-up");


					$.ajax({

						type: "POST",

						url: "ajax_functions",

						data: { action: "toggle_sound", state: "on" }

					}).done(function(result) {


					});
				}

				return false;
			});

			$(document).off('keydown');
	    	$(document).keydown(function(e) {

				var code = (e.keyCode ? e.keyCode : e.which);

				if (code == 13) {

					if($('.message_text').is(':focus')) {

						newMessage.sendMessage();
					}
				}
			});
	   	},

	    sendMessage: function() {

	    	//prevent multiple messages being sent
	    	if(processing == false) {

	    		processing = true;

		    	var message_text = $('.message_text').val();
				var recipients;

				//Handle the two variations of new message. If the new message is for multiple recipients, check the recipient select2 field. Otherwise check the hidden u_id field.
				if($("#recipients").length > 0) {

					recipients = $("#recipients").select2("val");
					recipients = recipients.toString();
				
				} else if($('#u_id').length > 0) {

					recipients = $('#u_id').val();
				}
				

				if(recipients != "") {

					var trimmed = $.trim(message_text);

					if(trimmed != "") {

						message_text = message_text.replace(/\n/g, '<br />');

						$.ajax({

							type: "POST",

							url: "ajax_functions",

							data: { 

								action: "init_new_conversation",
								message_text: message_text,
								users: recipients
							}

						}).done(function(convo_id) {

							if(convo_id != 0) {

								loadConversation(convo_id);
								reloadConversationList();

								//convo_id should be returned from the new DB row once this ajax call is done.
								sendMessageNotification(recipients, convo_id, account_id); //check notification_client.js
							}

							processing = false;

						});

					}
				
				} else {

					//no recipients selected. Do not send.
				}

			}

			return false;

	    }
	};

	newMessage.init();

});