$(document).ready(function() {



	//ACTIONS FOR MOBILE DEVICES

	if(navigator.userAgent.match(/Android/i)

		|| navigator.userAgent.match(/webOS/i)

		|| navigator.userAgent.match(/iPhone/i)

		|| navigator.userAgent.match(/iPad/i)

		|| navigator.userAgent.match(/iPod/i)

		|| navigator.userAgent.match(/BlackBerry/i)

		|| navigator.userAgent.match(/Windows Phone/i)

	) {

		if(page != "open_chat.php") {
			//remove the chat window if user device is mobile
			//$('.chat').remove();
		}

	}


	function stripHTML(string) {
	   var tmp = document.createElement("DIV");
	   tmp.innerHTML = string;
	   return tmp.textContent || tmp.innerText || "";
	}


	function linkify(text) {  
        var urlRegex =/(\b(https?:\/\/|ftp:\/\/|file:\/\/|www.)[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;  
        return text.replace(urlRegex, function(url) {  
            return '<a href="' + url + '" target="_blank">' + url + '</a>';  
        })  
    }


	function getMessages() {


		$.ajax({

			type: "POST",

			url: "ajax_functions",

			data: { 

				action: "get_messages" 
			}

		}).done(function(result) {

			$('ul.chats').append(result);


			$('body').on("fadeInComplete", function() {

				var chat_div_height = $('#chats .chats').height();

				chat_div_height + 'px';
				$('#chats .scroller').slimScroll({
				    scrollTo: chat_div_height
				});

			});


			$('ul.chats .body').each(function() {

				var text = $(this).html();
				text = linkify(text);

				$(this).html(text);
			});


		});


	}



	function sendMessage() {



		var message_text = stripHTML($('.message_text').val());
		var trimmed = $.trim(message_text);



		$('.message_text').val("");



		if(trimmed != "") {

			message_text = message_text.replace(/\n/g, '<br />');

			$.ajax({

				type: "POST",

				url: "ajax_functions",

				data: { action: "send_message", message_text: message_text }

			}).done(function(result) {

				if(result == 1) {

					//getMessages();

				}

			});

		}



		return false;

	}


	
	getMessages();



	$(document).keydown(function(e) {

		var code = (e.keyCode ? e.keyCode : e.which);

		if (code == 13) {



			if($('.message_text').is(':focus')) {

				sendMessage();

			}

		}



	});



	$(document).keyup(function(e) {

        var code = (e.keyCode ? e.keyCode : e.which);

        if (code == 13) {

            if($('.message_text').is(':focus')) {

                $('.message_text').val("");
            }
        }
    });



	//AUDIO ELEMENT

	sound = new Audio();

	if(sound.canPlayType('audio/mpeg'))

		sound.src = 'audio/notif.mp3';

	else if(sound.canPlayType('audio/ogg'))

		sound.src = 'audio/notif.ogg';

	else

		sound.src = null;


	$('body').on('click', '.btn-sound', function() {
	//$('.btn-sound').click(function() {

		if($(this).hasClass("on")) {

			$(this).removeClass("on");
			$(this).addClass("off");

			$(this).find("a").removeClass("light-blue");
			//$(this).find("a").addClass("grey");

			$(this).find("i").removeClass("icon-volume-up");
			$(this).find("i").addClass("icon-volume-off");


			$.ajax({

				type: "POST",

				url: "ajax_functions",

				data: { action: "toggle_sound", state: "off" }

			}).done(function(result) {


			});


		} else if($(this).hasClass("off")) {

			$(this).removeClass("off");
			$(this).addClass("on");

			//$(this).find("a").removeClass("grey");
			$(this).find("a").addClass("light-blue");

			$(this).find("i").removeClass("icon-volume-off");
			$(this).find("i").addClass("icon-volume-up");


			$.ajax({

				type: "POST",

				url: "ajax_functions",

				data: { action: "toggle_sound", state: "on" }

			}).done(function(result) {


			});
		}

		return false;
	});


	//HANDLE CHAT

	$('.send_message').click(sendMessage);


});