/**
Core script to handle the entire layout and base functions
**/
// app.js 
var App = function () {

    // IE mode
    var isRTL = false;
    var isIE8 = false;
    var isIE9 = false;
    var isIE10 = false;

    var sidebarWidth = 225;
    var sidebarCollapsedWidth = 35;

    var responsiveHandlers = [];

    // theme layout color set
    var layoutColorCodes = {
        'blue': '#4b8df8',
        'red': '#e02222',
        'green': '#35aa47',
        'purple': '#852b99',
        'grey': '#555555',
        'light-grey': '#fafafa',
        'yellow': '#ffb848'
    };

    var handleInit = function() {

        if ($('body').css('direction') === 'rtl') {
            isRTL = true;
        }

        isIE8 = !! navigator.userAgent.match(/MSIE 8.0/);
        isIE9 = !! navigator.userAgent.match(/MSIE 9.0/);
        isIE10 = !! navigator.userAgent.match(/MSIE 10/);
        
        if (isIE10) {
            jQuery('html').addClass('ie10'); // detect IE10 version
        }
    }

    var handleDesktopTabletContents = function () {
        // loops all page elements with "responsive" class and applies classes for tablet mode
        // For metornic  1280px or less set as tablet mode to display the content properly
        if ($(window).width() <= 1280 || $('body').hasClass('page-boxed')) {
            $(".responsive").each(function () {
                var forTablet = $(this).attr('data-tablet');
                var forDesktop = $(this).attr('data-desktop');
                if (forTablet) {
                    $(this).removeClass(forDesktop);
                    $(this).addClass(forTablet);
                }
            });
        }

        // loops all page elements with "responsive" class and applied classes for desktop mode
        // For metornic  higher 1280px set as desktop mode to display the content properly
        if ($(window).width() > 1280 && $('body').hasClass('page-boxed') === false) {
            $(".responsive").each(function () {
                var forTablet = $(this).attr('data-tablet');
                var forDesktop = $(this).attr('data-desktop');
                if (forTablet) {
                    $(this).removeClass(forTablet);
                    $(this).addClass(forDesktop);
                }
            });
        }
    }

    var handleSidebarState = function () {
        // remove sidebar toggler if window width smaller than 900(for table and phone mode)
        if ($(window).width() < 980) {
            $('body').removeClass("page-sidebar-closed");
        }
    }

    var runResponsiveHandlers = function () {
        // reinitialize other subscribed elements
        for (var i in responsiveHandlers) {
            var each = responsiveHandlers[i];
            each.call();
        }
    }

    var handleResponsive = function () {
        handleTooltips();
        handleSidebarState();
        handleDesktopTabletContents();
        handleSidebarAndContentHeight();
        handleChoosenSelect();
        handleFixedSidebar();
        runResponsiveHandlers();
    }

    var handleResponsiveOnInit = function () {
        handleSidebarState();
        handleDesktopTabletContents();
        handleSidebarAndContentHeight();
    }

    var handleResponsiveOnResize = function () {
        var resize;
        if (isIE8) {
            var currheight;
            $(window).resize(function() {
                if(currheight == document.documentElement.clientHeight) {
                    return; //quite event since only body resized not window.
                }                
                if (resize) {
                    clearTimeout(resize);
                }   
                resize = setTimeout(function() {
                    handleResponsive();    
                }, 50); // wait 50ms until window resize finishes.                
                currheight = document.documentElement.clientHeight; // store last body client height
            });
        } else {
            $(window).resize(function() {
                if (resize) {
                    clearTimeout(resize);
                }   
                resize = setTimeout(function() {
                    
                    handleResponsive();    
                }, 50); // wait 50ms until window resize finishes.
            });
        }   
    }

    //* BEGIN:CORE HANDLERS *//
    // this function handles responsive layout on screen size resize or mobile device rotate.
  
    var handleSidebarAndContentHeight = function () {
        var content = $('.page-content');
        var sidebar = $('.page-sidebar');
        var body = $('body');
        var height;

        if (body.hasClass("page-footer-fixed") === true && body.hasClass("page-sidebar-fixed") === false) {
            var available_height = $(window).height() - $('.footer').height();
            /*if (content.height() <  available_height) {
                content.attr('style', 'min-height:' + available_height + 'px !important');
            }*/
        } else {
            if (body.hasClass('page-sidebar-fixed')) {
                height = _calculateFixedSidebarViewportHeight();
            } else {
                height = sidebar.height() + 20;
            }
            /*if (height >= content.height()) {
                content.attr('style', 'min-height:' + height + 'px !important');
            } */
        }          
    }

    var handleSidebarMenu = function () {
        jQuery('.page-sidebar').on('click', 'li > a', function (e) {
                if ($(this).next().hasClass('sub-menu') == false) {
                    if ($('.btn-navbar').hasClass('collapsed') == false) {
                        $('.btn-navbar').click();
                    }
                    return;
                }

                var parent = $(this).parent().parent();

                parent.children('li.open').children('a').children('.arrow').removeClass('open');
                parent.children('li.open').children('.sub-menu').slideUp(200);
                parent.children('li.open').removeClass('open');

                var sub = jQuery(this).next();
                if (sub.is(":visible")) {
                    jQuery('.arrow', jQuery(this)).removeClass("open");
                    jQuery(this).parent().removeClass("open");
                    sub.slideUp(200, function () {
                            handleSidebarAndContentHeight();
                        });
                } else {
                    jQuery('.arrow', jQuery(this)).addClass("open");
                    jQuery(this).parent().addClass("open");
                    sub.slideDown(200, function () {
                            handleSidebarAndContentHeight();
                        });
                }

                e.preventDefault();
            });

        // handle ajax links
        jQuery('.page-sidebar').on('click', ' li > a.ajaxify', function (e) {
                e.preventDefault();
                App.scrollTop();

                var url = $(this).attr("href");
                var menuContainer = jQuery('.page-sidebar ul');
                var pageContent = $('.page-content');
                var pageContentBody = $('.page-content .page-content-body');

                menuContainer.children('li.active').removeClass('active');
                menuContainer.children('arrow.open').removeClass('open');

                $(this).parents('li').each(function () {
                        $(this).addClass('active');
                        $(this).children('a > span.arrow').addClass('open');
                    });
                $(this).parents('li').addClass('active');

                App.blockUI(pageContent, false);

                $.post(url, {}, function (res) {
                        App.unblockUI(pageContent);
                        pageContentBody.html(res);
                        App.fixContentHeight(); // fix content height
                        App.initUniform(); // initialize uniform elements
                    });
            });
    }

    var _calculateFixedSidebarViewportHeight = function () {
        var sidebarHeight = $(window).height() - $('.header').height() + 1;
        if ($('body').hasClass("page-footer-fixed")) {
            sidebarHeight = sidebarHeight - $('.footer').height();
        }

        return sidebarHeight; 
    }

    var handleFixedSidebar = function () {
        var menu = $('.page-sidebar-menu');

        if (menu.parent('.slimScrollDiv').size() === 1) { // destroy existing instance before updating the height
            menu.slimScroll({
                destroy: true
            });
            menu.removeAttr('style');
            $('.page-sidebar').removeAttr('style');            
        }

        if ($('.page-sidebar-fixed').size() === 0) {
            handleSidebarAndContentHeight();
            return;
        }

        if ($(window).width() >= 980) {
            var sidebarHeight = _calculateFixedSidebarViewportHeight();

            menu.slimScroll({
                size: '7px',
                color: '#a1b2bd',
                opacity: .3,
                position: isRTL ? 'left' : ($('.page-sidebar-on-right').size() === 1 ? 'left' : 'right'),
                height: sidebarHeight,
                allowPageScroll: false,
                disableFadeOut: false
            });
            handleSidebarAndContentHeight();
        }
    }

    var handleFixedSidebarHoverable = function () {
        if ($('body').hasClass('page-sidebar-fixed') === false) {
            return;
        }

        /*$('.page-sidebar').off('mouseenter').on('mouseenter', function () {
            var body = $('body');

            if ((body.hasClass('page-sidebar-closed') === false || body.hasClass('page-sidebar-fixed') === false) || $(this).hasClass('page-sidebar-hovering')) {
                return;
            }

            body.removeClass('page-sidebar-closed').addClass('page-sidebar-hover-on');
            $(this).addClass('page-sidebar-hovering');                
            $(this).animate({
                width: sidebarWidth
            }, 400, '', function () {
                $(this).removeClass('page-sidebar-hovering');
            });
        });

        $('.page-sidebar').off('mouseleave').on('mouseleave', function () {
            var body = $('body');

            if ((body.hasClass('page-sidebar-hover-on') === false || body.hasClass('page-sidebar-fixed') === false) || $(this).hasClass('page-sidebar-hovering')) {
                return;
            }

            $(this).addClass('page-sidebar-hovering');
            $(this).animate({
                width: sidebarCollapsedWidth
            }, 400, '', function () {
                $('body').addClass('page-sidebar-closed').removeClass('page-sidebar-hover-on');
                $(this).removeClass('page-sidebar-hovering');
            });
        });*/
    }

    var handleSidebarToggler = function () {
        // handle sidebar show/hide
        $('.page-sidebar').on('click', '.sidebar-toggler', function (e) {            
            var body = $('body');
            var sidebar = $('.page-sidebar');

            if ((body.hasClass("page-sidebar-hover-on") && body.hasClass('page-sidebar-fixed')) || sidebar.hasClass('page-sidebar-hovering')) {
                body.removeClass('page-sidebar-hover-on');
                sidebar.css('width', '').hide().show();
                e.stopPropagation();
                runResponsiveHandlers();

                $('.menu_footer').fadeIn();

                return;
            }

            $(".sidebar-search", sidebar).removeClass("open");

            if (body.hasClass("page-sidebar-closed")) {
                body.removeClass("page-sidebar-closed");
                if (body.hasClass('page-sidebar-fixed')) {
                    sidebar.css('width', '');
                }
            } else {
                body.addClass("page-sidebar-closed");
                $('.menu_footer').hide();
            }

            runResponsiveHandlers();
        });

        // handle the search bar close
        $('.page-sidebar').on('click', '.sidebar-search .remove', function (e) {
            e.preventDefault();
            $('.sidebar-search').removeClass("open");
        });

        // handle the search query submit on enter press
        $('.page-sidebar').on('keypress', '.sidebar-search input', function (e) {
            if (e.which == 13) {
                var query = $('#search_query').val();

                window.location.href = "search.php?q=" + query;
                return false; //<---- Add this line
            }
        });

        // handle the search submit
        $('.sidebar-search .submit').on('click', function (e) {
            e.preventDefault();
          
                if ($('body').hasClass("page-sidebar-closed")) {
                    if ($('.sidebar-search').hasClass('open') == false) {
                        if ($('.page-sidebar-fixed').size() === 1) {
                            $('.page-sidebar .sidebar-toggler').click(); //trigger sidebar toggle button
                        }
                        $('.sidebar-search').addClass("open");
                    } else {
                        var query = $('#search_query').val();

                        window.location.href = "search.php?q=" + query;
                    }
                } else {
                    var query = $('#search_query').val();

                    window.location.href = "search.php?q=" + query;
                }
        });
    }

    // var handleHorizontalMenu = function () {
    //     //handle hor menu search form toggler click
    //     $('.header').on('click', '.hor-menu .hor-menu-search-form-toggler', function (e) {
    //             if ($(this).hasClass('hide')) {
    //                 $(this).removeClass('hide');
    //                 $('.header .hor-menu .search-form').hide();
    //             } else {
    //                 $(this).addClass('hide');
    //                 $('.header .hor-menu .search-form').show();
    //             }
    //             e.preventDefault();
    //         });

    //     //handle hor menu search button click
    //     $('.header').on('click', '.hor-menu .search-form .btn', function (e) {
    //             var query = $('#search_query').val();

    //             window.location.href = "search.php?q=" + query;
    //             e.preventDefault();
    //         });

    //     //handle hor menu search form on enter press
    //     $('.header').on('keypress', '.hor-menu .search-form input', function (e) {
    //             if (e.which == 13) {
    //                 var query = $('#search_query').val();

    //                 window.location.href = "search.php?q=" + query;
    //                 return false;
    //             }
    //         });
    // }

    var handleGoTop = function () {
        /* set variables locally for increased performance */
        jQuery('.footer').on('click', '.go-top', function (e) {
                App.scrollTo();
                e.preventDefault();
            });
    }

    var handlePortletTools = function () {
        jQuery('body').on('click', '.portlet .tools a.remove', function (e) {
            e.preventDefault();
                jQuery(this).closest(".portlet").fadeOut('fast');
                //jQuery(this).closest(".portlet").remove();
                /*var removable = jQuery(this).parent(".portlet");
                if (removable.next().hasClass('portlet') || removable.prev().hasClass('portlet')) {
                    jQuery(this).parents(".portlet").remove();
                } else {
                    jQuery(this).parents(".portlet").parent().remove();
                }*/
        });

        jQuery('body').on('click', '.portlet .tools a.reload', function (e) {
            e.preventDefault();
                var el = jQuery(this).parents(".portlet");
                App.blockUI(el);
                window.setTimeout(function () {
                        App.unblockUI(el);
                    }, 1000);
        });

        /*jQuery('body').on('click', '.portlet .collapse, .portlet .expand', function (e) {
            e.preventDefault();
                var el = jQuery(this).closest(".portlet").children(".portlet-body");
                if (jQuery(this).hasClass("collapse")) {
                    jQuery(this).removeClass("collapse").addClass("expand");
                    jQuery(this).find("a").removeClass("collapse").addClass("expand");
                    el.slideUp(200);
                } else {
                    jQuery(this).removeClass("expand").addClass("collapse");
                    jQuery(this).find("a").removeClass("expand").addClass("collapse");
                    el.slideDown(200);
                }
        });*/
    }

    var handleUniform = function () {
        if (!jQuery().uniform) {
            return;
        }
        var test = $("input[type=checkbox]:not(.toggle), input[type=radio]:not(.toggle, .star)");
        if (test.size() > 0) {
            test.each(function () {
                    if ($(this).parents(".checker").size() == 0) {
                        $(this).show();
                        $(this).uniform();
                    }
                });
        }
    }

    var handleAccordions = function () {
        $(".accordion").collapse().height('auto');

        var lastClicked;

        //add scrollable class name if you need scrollable panes
        jQuery('body').on('click', '.accordion.scrollable .accordion-toggle', function () {
            lastClicked = jQuery(this);
        }); //move to faq section

        jQuery('body').on('shown', '.accordion.scrollable', function () {
            jQuery('html,body').animate({
                scrollTop: lastClicked.offset().top - 150
            }, 'slow');
        });
    }

    var handleTabs = function () {

        // function to fix left/right tab contents
        var fixTabHeight = function(tab) {
            $(tab).each(function() {
                var content = $($($(this).attr("href")));
                var tab = $(this).parent().parent();
                if (tab.height() > content.height()) {
                    content.css('min-height', tab.height());    
                } 
            });            
        }

        // fix tab content on tab shown
        $('body').on('shown', '.nav.nav-tabs.tabs-left a[data-toggle="tab"], .nav.nav-tabs.tabs-right a[data-toggle="tab"]', function(){
            fixTabHeight($(this)); 
        });

        $('body').on('shown', '.nav.nav-tabs', function(){
            handleSidebarAndContentHeight();
        });

        //fix tab contents for left/right tabs
        fixTabHeight('.nav.nav-tabs.tabs-left > li.active > a[data-toggle="tab"], .nav.nav-tabs.tabs-right > li.active > a[data-toggle="tab"]');

        //activate tab if tab id provided in the URL
        if(location.hash) {
            var tabid = location.hash.substr(1);
            $('a[href="#'+tabid+'"]').click();
        }
    }

    var handleScrollers = function () {
        $('.scroller').each(function () {
                $(this).slimScroll({
                        size: '7px',
                        color: '#a1b2bd',
                        position: isRTL ? 'left' : 'right',
                        height: '100%',
                        alwaysVisible: ($(this).attr("data-always-visible") == "1" ? true : false),
                        railVisible: ($(this).attr("data-rail-visible") == "1" ? true : false),
                        disableFadeOut: true,
                        allowPageScroll: true,
                        releaseScroll: false
                    });
            });
    }

    var handleTooltips = function () {
        if (App.isTouchDevice()) { // if touch device, some tooltips can be skipped in order to not conflict with click events
            jQuery('.tooltips:not(.no-tooltip-on-touch-device)').tooltip();
        } else {
            jQuery('.tooltips').tooltip();
        }
    }

    var handleDropdowns = function () {
        $('body').on('click', '.dropdown-menu.hold-on-click', function(e){
            e.stopPropagation();
        })
    }

    var handlePopovers = function () {
        jQuery('.popovers').popover();
    }

    var handleChoosenSelect = function () {
        if (!jQuery().chosen) {
            return;
        }

        $(".chosen").each(function () {
            $(this).chosen({
                allow_single_deselect: $(this).attr("data-with-diselect") === "1" ? true : false
            });
        });
    }

    var handleFancybox = function () {
        if (!jQuery.fancybox) {
            return;
        }

        if (jQuery(".fancybox-button").size() > 0) {
            jQuery(".fancybox-button").fancybox({
                groupAttr: 'data-rel',
                prevEffect: 'none',
                nextEffect: 'none',
                closeBtn: true,
                helpers: {
                    title: {
                        type: 'inside'
                    }
                }
            });
        }
    }

    // var handleTheme = function () {

    //     var panel = $('.color-panel');

    //     if ($('body').hasClass('page-boxed') == false) {
    //         $('.layout-option', panel).val("fluid");
    //     }
        
    //     $('.sidebar-option', panel).val("default");
    //     $('.header-option', panel).val("fixed");
    //     $('.footer-option', panel).val("default"); 

    //     //handle theme layout
    //     var resetLayout = function () {
    //         $("body").
    //             removeClass("page-boxed").
    //             removeClass("page-footer-fixed").
    //             removeClass("page-sidebar-fixed").
    //             removeClass("page-header-fixed");

    //         $('.header > .navbar-inner > .container').removeClass("container").addClass("container-fluid");

    //         if ($('.page-container').parent(".container").size() === 1) {
    //             $('.page-container').insertAfter('.header');
    //         } 

    //         if ($('.footer > .container').size() === 1) {                        
    //             $('.footer').html($('.footer > .container').html());                        
    //         } else if ($('.footer').parent(".container").size() === 1) {                        
    //             $('.footer').insertAfter('.page-container');
    //         }

    //         $('body > .container').remove(); 
    //     }

    //     var lastSelectedLayout = '';

    //     var setLayout = function () {

    //         var layoutOption = $('.layout-option', panel).val();
    //         var sidebarOption = "fixed";
    //         var headerOption = $('.header-option', panel).val();
    //         var footerOption = $('.footer-option', panel).val(); 

    //         if (sidebarOption == "fixed" && headerOption == "default") {
    //             alert('Default Header with Fixed Sidebar option is not supported. Proceed with Default Header with Default Sidebar.');
    //             $('.sidebar-option', panel).val("default");
    //             sidebarOption = 'default';
    //         }

    //         resetLayout(); // reset layout to default state

    //         if (layoutOption === "boxed") {
    //             $("body").addClass("page-boxed");

    //             // set header
    //             $('.header > .navbar-inner > .container-fluid').removeClass("container-fluid").addClass("container");
    //             var cont = $('.header').after('<div class="container"></div>');

    //             // set content
    //             $('.page-container').appendTo('body > .container');

    //             // set footer
    //             if (footerOption === 'fixed' || sidebarOption === 'default') {
    //                 $('.footer').html('<div class="container">'+$('.footer').html()+'</div>');
    //             } else {
    //                 $('.footer').appendTo('body > .container');
    //             }            
    //         }

    //         if (lastSelectedLayout != layoutOption) {
    //             //layout changed, run responsive handler:
    //             runResponsiveHandlers();
    //         }
    //         lastSelectedLayout = layoutOption;

    //         //header
    //         if (headerOption === 'fixed') {
    //             $("body").addClass("page-header-fixed");
    //             $(".header").removeClass("navbar-static-top").addClass("navbar-fixed-top");
    //         } else {
    //             $("body").removeClass("page-header-fixed");
    //             $(".header").removeClass("navbar-fixed-top").addClass("navbar-static-top");
    //         }

    //         //sidebar
    //         if (sidebarOption === 'fixed') {
    //             $("body").addClass("page-sidebar-fixed");
    //         } else {
    //             $("body").removeClass("page-sidebar-fixed");
    //         }
    //         handleFixedSidebar(); // reinitialize fixed sidebar
    //         handleFixedSidebarHoverable(); // reinitialize fixed sidebar hover effect

    //         //footer 
    //         if (footerOption === 'fixed') {
    //             $("body").addClass("page-footer-fixed");
    //         } else {
    //             $("body").removeClass("page-footer-fixed");
    //         }

    //         handleSidebarAndContentHeight(); // fix content height
    //     }

    //     // handle theme colors
    //     var setColor = function (color) {
    //         $('#style_color').attr("href", "css/themes/" + color + ".css");
    //         $.cookie('style_color', color);                
    //     }

    //     $('.icon-color', panel).click(function () {
    //         $('.color-mode').show();
    //         $('.icon-color-close').show();
    //     });

    //     $('.icon-color-close', panel).click(function () {
    //         $('.color-mode').hide();
    //         $('.icon-color-close').hide();
    //     });

    //     $('li', panel).click(function () {
    //         var color = $(this).attr("data-style");
    //         setColor(color);
    //         $('.inline li', panel).removeClass("current");
    //         $(this).addClass("current");
    //     });

    //     $('.layout-option, .header-option, .sidebar-option, .footer-option', panel).change(setLayout);

    // }

    var handleFixInputPlaceholderForIE = function () {
        //fix html5 placeholder attribute for ie7 & ie8
        if (isIE8 || isIE9) { // ie7&ie8
            // this is html5 placeholder fix for inputs, inputs with placeholder-no-fix class will be skipped(e.g: we need this for password fields)
            jQuery('input[placeholder]:not(.placeholder-no-fix), textarea[placeholder]:not(.placeholder-no-fix)').each(function () {

                var input = jQuery(this);

                if(input.val()=='' && input.attr("placeholder") != '') {
                    input.addClass("placeholder").val(input.attr('placeholder'));
                }

                input.focus(function () {
                    if (input.val() == input.attr('placeholder')) {
                        input.val('');
                    }
                });

                input.blur(function () {                         
                    if (input.val() == '' || input.val() == input.attr('placeholder')) {
                        input.val(input.attr('placeholder'));
                    }
                });
            });
        }
    }

    //* END:CORE HANDLERS *//

    return {

        //main function to initiate template pages
        init: function () {

            //IMPORTANT!!!: Do not modify the core handlers call order.

            //core handlers
            handleInit();
            handleResponsiveOnResize(); // set and handle responsive    
            handleUniform();        
            handleScrollers(); // handles slim scrolling contents 
            handleResponsiveOnInit(); // handler responsive elements on page load

            //layout handlers
            handleFixedSidebar(); // handles fixed sidebar menu
            handleFixedSidebarHoverable(); // handles fixed sidebar on hover effect 
            handleSidebarMenu(); // handles main menu
            //handleHorizontalMenu(); // handles horizontal menu
            handleSidebarToggler(); // handles sidebar hide/show            
            handleFixInputPlaceholderForIE(); // fixes/enables html5 placeholder attribute for IE9, IE8
            handleGoTop(); //handles scroll to top functionality in the footer
            //handleTheme(); // handles style customer tool

            //ui component handlers
            handlePortletTools(); // handles portlet action bar functionality(refresh, configure, toggle, remove)
            handleDropdowns(); // handle dropdowns
            handleTabs(); // handle tabs
            handleTooltips(); // handle bootstrap tooltips
            handlePopovers(); // handles bootstrap popovers
            handleAccordions(); //handles accordions
            handleChoosenSelect(); // handles bootstrap chosen dropdowns     

            App.addResponsiveHandler(handleChoosenSelect); // reinitiate chosen dropdown on main content resize. disable this line if you don't really use chosen dropdowns.
        },

        fixContentHeight: function () {
            handleSidebarAndContentHeight();
        },

        addResponsiveHandler: function (func) {
            responsiveHandlers.push(func);
        },

        // useful function to make equal height for contacts stand side by side
        setEqualHeight: function (els) {
            var tallestEl = 0;
            els = jQuery(els);
            els.each(function () {
                    var currentHeight = $(this).height();
                    if (currentHeight > tallestEl) {
                        tallestColumn = currentHeight;
                    }
                });
            els.height(tallestEl);
        },

        // wrapper function to scroll to an element
        scrollTo: function (el, offeset) {
            pos = el ? el.offset().top : 0;
            jQuery('html,body').animate({
                    scrollTop: pos + (offeset ? offeset : 0)
                }, 'slow');
        },

        scrollTop: function () {
            App.scrollTo();
        },

        // wrapper function to  block element(indicate loading)
        blockUI: function (el, centerY) {
            var el = jQuery(el); 
            el.block({
                    message: '<img src="img/ajax-loading.gif" align="">',
                    centerY: centerY != undefined ? centerY : true,
                    css: {
                        top: '10%',
                        border: 'none',
                        padding: '2px',
                        backgroundColor: 'none'
                    },
                    overlayCSS: {
                        backgroundColor: '#000',
                        opacity: 0.05,
                        cursor: 'wait'
                    }
                });
        },

        // wrapper function to  un-block element(finish loading)
        unblockUI: function (el) {
            jQuery(el).unblock({
                    onUnblock: function () {
                        jQuery(el).removeAttr("style");
                    }
                });
        },

        // initializes uniform elements
        initUniform: function (els) {

            if (els) {
                jQuery(els).each(function () {
                        if ($(this).parents(".checker").size() == 0) {
                            $(this).show();
                            $(this).uniform();
                        }
                    });
            } else {
                handleUniform();
            }

        },

        // initializes choosen dropdowns
        initChosenSelect: function (els) {
            $(els).chosen({
                    allow_single_deselect: true
                });
        },

        initFancybox: function () {
            handleFancybox();
        },

        getActualVal: function (el) {
            var el = jQuery(el);
            if (el.val() === el.attr("placeholder")) {
                return "";
            }

            return el.val();
        },

        getURLParameter: function (paramName) {
            var searchString = window.location.search.substring(1),
                i, val, params = searchString.split("&");

            for (i = 0; i < params.length; i++) {
                val = params[i].split("=");
                if (val[0] == paramName) {
                    return unescape(val[1]);
                }
            }
            return null;
        },

        // check for device touch support
        isTouchDevice: function () {
            try {
                document.createEvent("TouchEvent");
                return true;
            } catch (e) {
                return false;
            }
        },

        isIE8: function () {
            return isIE8;
        },

        isRTL: function () {
            return isRTL;
        },

        getLayoutColorCode: function (name) {
            if (layoutColorCodes[name]) {
                return layoutColorCodes[name];
            } else {
                return '';
            }
        }

    };

}();


// calander.js 
// var Calendar = function () {


//     return {
//         //main function to initiate the module
//         init: function () {

//             App.addResponsiveHandler(function () {
//                 Calendar.initCalendar();
//             });

//             $('.page-sidebar .sidebar-toggler').click(function () {
//                 Calendar.initCalendar();
//             });

//             Calendar.initCalendar();
//         },

//         initCalendar: function () {

//             if (!jQuery().fullCalendar) {
//                 return;
//             }

//             var date = new Date();
//             var d = date.getDate();
//             var m = date.getMonth();
//             var y = date.getFullYear();

//             var h = {};

//             if (App.isRTL()) {
//                  if ($('#calendar').parents(".portlet").width() <= 720) {
//                     $('#calendar').addClass("mobile");
//                     h = {
//                         right: 'title, prev, next',
//                         center: '',
//                         right: 'agendaDay, agendaWeek, month, today'
//                     };
//                 } else {
//                     $('#calendar').removeClass("mobile");
//                     h = {
//                         right: 'title',
//                         center: '',
//                         left: 'agendaDay, agendaWeek, month, today, prev,next'
//                     };
//                 }                
//             } else {
//                  if ($('#calendar').parents(".portlet").width() <= 720) {
//                     $('#calendar').addClass("mobile");
//                     h = {
//                         left: 'title, prev, next',
//                         center: '',
//                         right: 'today,month,agendaWeek,agendaDay'
//                     };
//                 } else {
//                     $('#calendar').removeClass("mobile");
//                     h = {
//                         left: 'title',
//                         center: '',
//                         right: 'prev,next,today,month,agendaWeek,agendaDay'
//                     };
//                 }
//             }
           

//             var initDrag = function (el) {
//                 // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
//                 // it doesn't need to have a start or end
//                 var eventObject = {
//                     title: $.trim(el.text()) // use the element's text as the event title
//                 };
//                 // store the Event Object in the DOM element so we can get to it later
//                 el.data('eventObject', eventObject);
//                 // make the event draggable using jQuery UI
//                 el.draggable({
//                     zIndex: 999,
//                     revert: true, // will cause the event to go back to its
//                     revertDuration: 0 //  original position after the drag
//                 });
//             }

//             var addEvent = function (title) {
//                 title = title.length == 0 ? "Untitled Event" : title;
//                 var html = $('<div class="external-event label">' + title + '</div>');
//                 jQuery('#event_box').append(html);
//                 initDrag(html);
//             }

//             $('#external-events div.external-event').each(function () {
//                 initDrag($(this))
//             });

//             $('#event_add').unbind('click').click(function () {
//                 var title = $('#event_title').val();
//                 addEvent(title);
//             });

//         }

//     };

// }();


//charts.js
// var Charts = function () {

//     return {
//         //main function to initiate the module

//         init: function () {

//             App.addResponsiveHandler(function () {
//                  //Charts.initPieCharts(); 
//             });
            
//         },

//         initCharts: function () {

//             if (!jQuery.plot) {
//                 return;
//             }

//             var data = [];
//             var totalPoints = 250;

//             // random data generator for plot charts

//             function getRandomData() {
//                 if (data.length > 0) data = data.slice(1);
//                 // do a random walk
//                 while (data.length < totalPoints) {
//                     var prev = data.length > 0 ? data[data.length - 1] : 50;
//                     var y = prev + Math.random() * 10 - 5;
//                     if (y < 0) y = 0;
//                     if (y > 100) y = 100;
//                     data.push(y);
//                 }
//                 // zip the generated y values with the x values
//                 var res = [];
//                 for (var i = 0; i < data.length; ++i) res.push([i, data[i]])
//                 return res;
//             }

//             //Basic Chart

//             //Interactive Chart

//             function chart1() {
//                 function randValue() {
//                     return (Math.floor(Math.random() * (1 + 40 - 20))) + 20;
//                 }
//                 var pageviews = [
//                     [1, randValue()],
//                     [2, randValue()],
//                     [3, 2 + randValue()],
//                     [4, 3 + randValue()],
//                     [5, 5 + randValue()],
//                     [6, 10 + randValue()],
//                     [7, 15 + randValue()],
//                     [8, 20 + randValue()],
//                     [9, 25 + randValue()],
//                     [10, 30 + randValue()],
//                     [11, 35 + randValue()],
//                     [12, 25 + randValue()],
//                     [13, 15 + randValue()],
//                     [14, 20 + randValue()],
//                     [15, 45 + randValue()],
//                     [16, 50 + randValue()],
//                     [17, 65 + randValue()],
//                     [18, 70 + randValue()],
//                     [19, 85 + randValue()],
//                     [20, 80 + randValue()],
//                     [21, 75 + randValue()],
//                     [22, 80 + randValue()],
//                     [23, 75 + randValue()],
//                     [24, 70 + randValue()],
//                     [25, 65 + randValue()],
//                     [26, 75 + randValue()],
//                     [27, 80 + randValue()],
//                     [28, 85 + randValue()],
//                     [29, 90 + randValue()],
//                     [30, 95 + randValue()]
//                 ];
//                 var visitors = [
//                     [1, randValue() - 5],
//                     [2, randValue() - 5],
//                     [3, randValue() - 5],
//                     [4, 6 + randValue()],
//                     [5, 5 + randValue()],
//                     [6, 20 + randValue()],
//                     [7, 25 + randValue()],
//                     [8, 36 + randValue()],
//                     [9, 26 + randValue()],
//                     [10, 38 + randValue()],
//                     [11, 39 + randValue()],
//                     [12, 50 + randValue()],
//                     [13, 51 + randValue()],
//                     [14, 12 + randValue()],
//                     [15, 13 + randValue()],
//                     [16, 14 + randValue()],
//                     [17, 15 + randValue()],
//                     [18, 15 + randValue()],
//                     [19, 16 + randValue()],
//                     [20, 17 + randValue()],
//                     [21, 18 + randValue()],
//                     [22, 19 + randValue()],
//                     [23, 20 + randValue()],
//                     [24, 21 + randValue()],
//                     [25, 14 + randValue()],
//                     [26, 24 + randValue()],
//                     [27, 25 + randValue()],
//                     [28, 26 + randValue()],
//                     [29, 27 + randValue()],
//                     [30, 31 + randValue()]
//                 ];

//                 var plot = $.plot($("#chart_1"), [{
//                             data: pageviews,
//                             label: "Unique Visits"
//                         }, {
//                             data: visitors,
//                             label: "Page Views"
//                         }
//                     ], {
//                         series: {
//                             lines: {
//                                 show: true,
//                                 lineWidth: 2,
//                                 fill: true,
//                                 fillColor: {
//                                     colors: [{
//                                             opacity: 0.05
//                                         }, {
//                                             opacity: 0.01
//                                         }
//                                     ]
//                                 }
//                             },
//                             points: {
//                                 show: true
//                             },
//                             shadowSize: 2
//                         },
//                         grid: {
//                             hoverable: true,
//                             clickable: true,
//                             tickColor: "#eee",
//                             borderWidth: 0
//                         },
//                         colors: ["#d12610", "#37b7f3", "#52e136"],
//                         xaxis: {
//                             ticks: 11,
//                             tickDecimals: 0
//                         },
//                         yaxis: {
//                             ticks: 11,
//                             tickDecimals: 0
//                         }
//                     });


//                 function showTooltip(x, y, contents) {
//                     $('<div id="tooltip">' + contents + '</div>').css({
//                             position: 'absolute',
//                             display: 'none',
//                             top: y + 5,
//                             left: x + 15,
//                             border: '1px solid #333',
//                             padding: '4px',
//                             color: '#fff',
//                             'border-radius': '3px',
//                             'background-color': '#333',
//                             opacity: 0.80
//                         }).appendTo("body").fadeIn(200);
//                 }

//                 var previousPoint = null;
//                 $("#chart_1").bind("plothover", function (event, pos, item) {
//                     $("#x").text(pos.x.toFixed(2));
//                     $("#y").text(pos.y.toFixed(2));

//                     if (item) {
//                         if (previousPoint != item.dataIndex) {
//                             previousPoint = item.dataIndex;

//                             $("#tooltip").remove();
//                             var x = item.datapoint[0].toFixed(2),
//                                 y = item.datapoint[1].toFixed(2);

//                             showTooltip(item.pageX, item.pageY, item.series.label + " of " + x + " = " + y);
//                         }
//                     } else {
//                         $("#tooltip").remove();
//                         previousPoint = null;
//                     }
//                 });
//             }


//             function chart2() {
//                 function randValue() {
//                     return (Math.floor(Math.random() * (1 + 40 - 20))) + 20;
//                 }
//                 var pageviews = [
//                     [1, randValue()],
//                     [2, randValue()],
//                     [3, 2 + randValue()],
//                     [4, 3 + randValue()],
//                     [5, 5 + randValue()],
//                     [6, 10 + randValue()],
//                     [7, 15 + randValue()],
//                     [8, 20 + randValue()],
//                     [9, 25 + randValue()],
//                     [10, 30 + randValue()],
//                     [11, 35 + randValue()],
//                     [12, 25 + randValue()],
//                     [13, 15 + randValue()],
//                     [14, 20 + randValue()],
//                     [15, 45 + randValue()],
//                     [16, 50 + randValue()],
//                     [17, 65 + randValue()],
//                     [18, 70 + randValue()],
//                     [19, 85 + randValue()],
//                     [20, 80 + randValue()],
//                     [21, 75 + randValue()],
//                     [22, 80 + randValue()],
//                     [23, 75 + randValue()],
//                     [24, 70 + randValue()],
//                     [25, 65 + randValue()],
//                     [26, 75 + randValue()],
//                     [27, 80 + randValue()],
//                     [28, 85 + randValue()],
//                     [29, 90 + randValue()],
//                     [30, 95 + randValue()]
//                 ];
//                 var visitors = [
//                     [1, randValue() - 5],
//                     [2, randValue() - 5],
//                     [3, randValue() - 5],
//                     [4, 6 + randValue()],
//                     [5, 5 + randValue()],
//                     [6, 20 + randValue()],
//                     [7, 25 + randValue()],
//                     [8, 36 + randValue()],
//                     [9, 26 + randValue()],
//                     [10, 38 + randValue()],
//                     [11, 39 + randValue()],
//                     [12, 50 + randValue()],
//                     [13, 51 + randValue()],
//                     [14, 12 + randValue()],
//                     [15, 13 + randValue()],
//                     [16, 14 + randValue()],
//                     [17, 15 + randValue()],
//                     [18, 15 + randValue()],
//                     [19, 16 + randValue()],
//                     [20, 17 + randValue()],
//                     [21, 18 + randValue()],
//                     [22, 19 + randValue()],
//                     [23, 20 + randValue()],
//                     [24, 21 + randValue()],
//                     [25, 14 + randValue()],
//                     [26, 24 + randValue()],
//                     [27, 25 + randValue()],
//                     [28, 26 + randValue()],
//                     [29, 27 + randValue()],
//                     [30, 31 + randValue()]
//                 ];

//                 var plot = $.plot($("#chart_2"), [{
//                             data: pageviews,
//                             label: "Unique Visits"
//                         }, {
//                             data: visitors,
//                             label: "Page Views"
//                         }
//                     ], {
//                         series: {
//                             lines: {
//                                 show: true,
//                                 lineWidth: 2,
//                                 fill: true,
//                                 fillColor: {
//                                     colors: [{
//                                             opacity: 0.05
//                                         }, {
//                                             opacity: 0.01
//                                         }
//                                     ]
//                                 }
//                             },
//                             points: {
//                                 show: true
//                             },
//                             shadowSize: 2
//                         },
//                         grid: {
//                             hoverable: true,
//                             clickable: true,
//                             tickColor: "#eee",
//                             borderWidth: 0
//                         },
//                         colors: ["#d12610", "#37b7f3", "#52e136"],
//                         xaxis: {
//                             ticks: 11,
//                             tickDecimals: 0
//                         },
//                         yaxis: {
//                             ticks: 11,
//                             tickDecimals: 0
//                         }
//                     });


//                 function showTooltip(x, y, contents) {
//                     $('<div id="tooltip">' + contents + '</div>').css({
//                             position: 'absolute',
//                             display: 'none',
//                             top: y + 5,
//                             left: x + 15,
//                             border: '1px solid #333',
//                             padding: '4px',
//                             color: '#fff',
//                             'border-radius': '3px',
//                             'background-color': '#333',
//                             opacity: 0.80
//                         }).appendTo("body").fadeIn(200);
//                 }

//                 var previousPoint = null;
//                 $("#chart_2").bind("plothover", function (event, pos, item) {
//                     $("#x").text(pos.x.toFixed(2));
//                     $("#y").text(pos.y.toFixed(2));

//                     if (item) {
//                         if (previousPoint != item.dataIndex) {
//                             previousPoint = item.dataIndex;

//                             $("#tooltip").remove();
//                             var x = item.datapoint[0].toFixed(2),
//                                 y = item.datapoint[1].toFixed(2);

//                             showTooltip(item.pageX, item.pageY, item.series.label + " of " + x + " = " + y);
//                         }
//                     } else {
//                         $("#tooltip").remove();
//                         previousPoint = null;
//                     }
//                 });
//             }


//             function chart3() {
//                 function randValue() {
//                     return (Math.floor(Math.random() * (1 + 40 - 20))) + 20;
//                 }
//                 var pageviews = [
//                     [1, randValue()],
//                     [2, randValue()],
//                     [3, 2 + randValue()],
//                     [4, 3 + randValue()],
//                     [5, 5 + randValue()],
//                     [6, 10 + randValue()],
//                     [7, 15 + randValue()],
//                     [8, 20 + randValue()],
//                     [9, 25 + randValue()],
//                     [10, 30 + randValue()],
//                     [11, 35 + randValue()],
//                     [12, 25 + randValue()],
//                     [13, 15 + randValue()],
//                     [14, 20 + randValue()],
//                     [15, 45 + randValue()],
//                     [16, 50 + randValue()],
//                     [17, 65 + randValue()],
//                     [18, 70 + randValue()],
//                     [19, 85 + randValue()],
//                     [20, 80 + randValue()],
//                     [21, 75 + randValue()],
//                     [22, 80 + randValue()],
//                     [23, 75 + randValue()],
//                     [24, 70 + randValue()],
//                     [25, 65 + randValue()],
//                     [26, 75 + randValue()],
//                     [27, 80 + randValue()],
//                     [28, 85 + randValue()],
//                     [29, 90 + randValue()],
//                     [30, 95 + randValue()]
//                 ];
//                 var visitors = [
//                     [1, randValue() - 5],
//                     [2, randValue() - 5],
//                     [3, randValue() - 5],
//                     [4, 6 + randValue()],
//                     [5, 5 + randValue()],
//                     [6, 20 + randValue()],
//                     [7, 25 + randValue()],
//                     [8, 36 + randValue()],
//                     [9, 26 + randValue()],
//                     [10, 38 + randValue()],
//                     [11, 39 + randValue()],
//                     [12, 50 + randValue()],
//                     [13, 51 + randValue()],
//                     [14, 12 + randValue()],
//                     [15, 13 + randValue()],
//                     [16, 14 + randValue()],
//                     [17, 15 + randValue()],
//                     [18, 15 + randValue()],
//                     [19, 16 + randValue()],
//                     [20, 17 + randValue()],
//                     [21, 18 + randValue()],
//                     [22, 19 + randValue()],
//                     [23, 20 + randValue()],
//                     [24, 21 + randValue()],
//                     [25, 14 + randValue()],
//                     [26, 24 + randValue()],
//                     [27, 25 + randValue()],
//                     [28, 26 + randValue()],
//                     [29, 27 + randValue()],
//                     [30, 31 + randValue()]
//                 ];

//                 var plot = $.plot($("#chart_3"), [{
//                             data: pageviews,
//                             label: "Unique Visits"
//                         }, {
//                             data: visitors,
//                             label: "Page Views"
//                         }
//                     ], {
//                         series: {
//                             lines: {
//                                 show: true,
//                                 lineWidth: 2,
//                                 fill: true,
//                                 fillColor: {
//                                     colors: [{
//                                             opacity: 0.05
//                                         }, {
//                                             opacity: 0.01
//                                         }
//                                     ]
//                                 }
//                             },
//                             points: {
//                                 show: true
//                             },
//                             shadowSize: 2
//                         },
//                         grid: {
//                             hoverable: true,
//                             clickable: true,
//                             tickColor: "#eee",
//                             borderWidth: 0
//                         },
//                         colors: ["#d12610", "#37b7f3", "#52e136"],
//                         xaxis: {
//                             ticks: 11,
//                             tickDecimals: 0
//                         },
//                         yaxis: {
//                             ticks: 11,
//                             tickDecimals: 0
//                         }
//                     });


//                 function showTooltip(x, y, contents) {
//                     $('<div id="tooltip">' + contents + '</div>').css({
//                             position: 'absolute',
//                             display: 'none',
//                             top: y + 5,
//                             left: x + 15,
//                             border: '1px solid #333',
//                             padding: '4px',
//                             color: '#fff',
//                             'border-radius': '3px',
//                             'background-color': '#333',
//                             opacity: 0.80
//                         }).appendTo("body").fadeIn(200);
//                 }

//                 var previousPoint = null;
//                 $("#chart_3").bind("plothover", function (event, pos, item) {
//                     $("#x").text(pos.x.toFixed(2));
//                     $("#y").text(pos.y.toFixed(2));

//                     if (item) {
//                         if (previousPoint != item.dataIndex) {
//                             previousPoint = item.dataIndex;

//                             $("#tooltip").remove();
//                             var x = item.datapoint[0].toFixed(2),
//                                 y = item.datapoint[1].toFixed(2);

//                             showTooltip(item.pageX, item.pageY, item.series.label + " of " + x + " = " + y);
//                         }
//                     } else {
//                         $("#tooltip").remove();
//                         previousPoint = null;
//                     }
//                 });
//             }


//             chart1();
//             chart2();
//             chart3();

//         },

//         initPieCharts: function () {

//             var data = [];
//             var series = Math.floor(Math.random() * 10) + 1;
//             series = series < 5 ? 5 : series;
            
//             for (var i = 0; i < series; i++) {
//                 data[i] = {
//                     label: "Series" + (i + 1),
//                     data: Math.floor(Math.random() * 100) + 1
//                 }
//             }

//             // DEFAULT
//             $.plot($("#pie_chart"), data, {
//                     series: {
//                         pie: {
//                             show: true
//                         }
//                     }
//                 });

//             // GRAPH 1
//             $.plot($("#pie_chart_1"), data, {
//                     series: {
//                         pie: {
//                             show: true
//                         }
//                     },
//                     legend: {
//                         show: false
//                     }
//                 });

//             // GRAPH 2
//             $.plot($("#pie_chart_2"), data, {
//                     series: {
//                         pie: {
//                             show: true,
//                             radius: 1,
//                             label: {
//                                 show: true,
//                                 radius: 1,
//                                 formatter: function (label, series) {
//                                     return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">' + label + '<br/>' + Math.round(series.percent) + '%</div>';
//                                 },
//                                 background: {
//                                     opacity: 0.8
//                                 }
//                             }
//                         }
//                     },
//                     legend: {
//                         show: false
//                     }
//                 });

//             // GRAPH 3
//             $.plot($("#pie_chart_3"), data, {
//                     series: {
//                         pie: {
//                             show: true,
//                             radius: 1,
//                             label: {
//                                 show: true,
//                                 radius: 3 / 4,
//                                 formatter: function (label, series) {
//                                     return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">' + label + '<br/>' + Math.round(series.percent) + '%</div>';
//                                 },
//                                 background: {
//                                     opacity: 0.5
//                                 }
//                             }
//                         }
//                     },
//                     legend: {
//                         show: false
//                     }
//                 });

//             // GRAPH 4
//             $.plot($("#pie_chart_4"), data, {
//                     series: {
//                         pie: {
//                             show: true,
//                             radius: 1,
//                             label: {
//                                 show: true,
//                                 radius: 3 / 4,
//                                 formatter: function (label, series) {
//                                     return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">' + label + '<br/>' + Math.round(series.percent) + '%</div>';
//                                 },
//                                 background: {
//                                     opacity: 0.5,
//                                     color: '#000'
//                                 }
//                             }
//                         }
//                     },
//                     legend: {
//                         show: false
//                     }
//                 });

//             // GRAPH 5
//             $.plot($("#pie_chart_5"), data, {
//                     series: {
//                         pie: {
//                             show: true,
//                             radius: 3 / 4,
//                             label: {
//                                 show: true,
//                                 radius: 3 / 4,
//                                 formatter: function (label, series) {
//                                     return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">' + label + '<br/>' + Math.round(series.percent) + '%</div>';
//                                 },
//                                 background: {
//                                     opacity: 0.5,
//                                     color: '#000'
//                                 }
//                             }
//                         }
//                     },
//                     legend: {
//                         show: false
//                     }
//                 });

//             // GRAPH 6
//             $.plot($("#pie_chart_6"), data, {
//                     series: {
//                         pie: {
//                             show: true,
//                             radius: 1,
//                             label: {
//                                 show: true,
//                                 radius: 2 / 3,
//                                 formatter: function (label, series) {
//                                     return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">' + label + '<br/>' + Math.round(series.percent) + '%</div>';
//                                 },
//                                 threshold: 0.1
//                             }
//                         }
//                     },
//                     legend: {
//                         show: false
//                     }
//                 });

//             // GRAPH 7
//             $.plot($("#pie_chart_7"), data, {
//                     series: {
//                         pie: {
//                             show: true,
//                             combine: {
//                                 color: '#999',
//                                 threshold: 0.1
//                             }
//                         }
//                     },
//                     legend: {
//                         show: false
//                     }
//                 });

//             // GRAPH 8
//             $.plot($("#pie_chart_8"), data, {
//                     series: {
//                         pie: {
//                             show: true,
//                             radius: 300,
//                             label: {
//                                 show: true,
//                                 formatter: function (label, series) {
//                                     return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">' + label + '<br/>' + Math.round(series.percent) + '%</div>';
//                                 },
//                                 threshold: 0.1
//                             }
//                         }
//                     },
//                     legend: {
//                         show: false
//                     }
//                 });

//             // GRAPH 9
//             $.plot($("#pie_chart_9"), data, {
//                     series: {
//                         pie: {
//                             show: true,
//                             radius: 1,
//                             tilt: 0.5,
//                             label: {
//                                 show: true,
//                                 radius: 1,
//                                 formatter: function (label, series) {
//                                     return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">' + label + '<br/>' + Math.round(series.percent) + '%</div>';
//                                 },
//                                 background: {
//                                     opacity: 0.8
//                                 }
//                             },
//                             combine: {
//                                 color: '#999',
//                                 threshold: 0.1
//                             }
//                         }
//                     },
//                     legend: {
//                         show: false
//                     }
//                 });

//             // DONUT
//             $.plot($("#donut"), data, {
//                     series: {
//                         pie: {
//                             innerRadius: 0.5,
//                             show: true
//                         }
//                     }
//                 });

//             // INTERACTIVE
//             $.plot($("#interactive"), data, {
//                     series: {
//                         pie: {
//                             show: true
//                         }
//                     },
//                     grid: {
//                         hoverable: true,
//                         clickable: true
//                     }
//                 });
//             $("#interactive").bind("plothover", pieHover);
//             $("#interactive").bind("plotclick", pieClick);

//             function pieHover(event, pos, obj) {
//             if (!obj)
//                     return;
//                 percent = parseFloat(obj.series.percent).toFixed(2);
//                 $("#hover").html('<span style="font-weight: bold; color: ' + obj.series.color + '">' + obj.series.label + ' (' + percent + '%)</span>');
//             }

//             function pieClick(event, pos, obj) {
//                 if (!obj)
//                     return;
//                 percent = parseFloat(obj.series.percent).toFixed(2);
//                 alert('' + obj.series.label + ': ' + percent + '%');
//             }

//         }
        
//     };

// }();



//comming-soon.js
var ComingSoon = function () {

    return {
        //main function to initiate the module
        init: function () {

            function shuffle(o){ //v1.0
                for(var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
                return o;
            };

            var images = ["img/bg/blurry_dock_small.jpg",
                    "img/bg/blurry_dock.jpg",
                    "img/bg/sunset.jpg",
                    "img/bg/rocks.jpg",
                    "img/bg/gudvagen.jpg",
                    "img/bg/canoe.jpg",
                    "img/bg/sunset_boat.jpg",
                    "img/bg/palmtrees.jpg",
                    "img/bg/snail.jpg",
                    "img/bg/underwater.jpg",
                    "img/bg/beach.jpg"];


            images = shuffle(images);

            $.backstretch(images, {
    		          fade: 2000,
    		          duration: 8000
    		    });

            var austDay = new Date(2013,8,9,8,0,0);
            $('#defaultCountdown').countdown({until: austDay});
            $('#year').text(austDay.getFullYear());
        }

    };

}();



//comming-soon.js
var BG = function () {

    return {
        //main function to initiate the module
        init: function () {

            function shuffle(o){ //v1.0
                for(var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
                return o;
            };

            var images = ["img/bg/blurry_dock_small.jpg",
                    "img/bg/blurry_dock.jpg",
                    "img/bg/sunset.jpg",
                    "img/bg/rocks.jpg",
                    "img/bg/gudvagen.jpg",
                    "img/bg/canoe.jpg",
                    "img/bg/sunset_boat.jpg",
                    "img/bg/palmtrees.jpg",
                    "img/bg/snail.jpg",
                    "img/bg/underwater.jpg",
                    "img/bg/beach.jpg"];

            images = shuffle(images);

            $.backstretch(images, {
                  fade: 2000,
                  duration: 8000
            });
        }

    };

}();



//contact-is.js
// var ContactUs = function () {

//     return {
//         //main function to initiate the module
//         init: function () {
// 			var map;
// 			$(document).ready(function(){
// 			  map = new GMaps({
// 				div: '#map',
// 				lat: -13.004333,
// 				lng: -38.494333
// 			  });
// 			   var marker = map.addMarker({
// 		            lat: -13.004333,
// 					lng: -38.494333,
// 		            title: 'Loop, Inc.',
// 		            infoWindow: {
// 		                content: "<b>Loop, Inc.</b> 795 Park Ave, Suite 120<br>San Francisco, CA 94107"
// 		            }
// 		        });

// 			   marker.infoWindow.open(map, marker);
// 			});
//         }
//     };

// }();



//form-components.js
var FormComponents = function () {

    var handleWysihtml5 = function () {
        if (!jQuery().wysihtml5) {
            return;
        }

        if ($('.wysihtml5').size() > 0) {
            $('.wysihtml5').wysihtml5({
                "stylesheets": ["plugins/bootstrap-wysihtml5/wysiwyg-color.css"]
            });
        }
    }

    var resetWysihtml5 = function () {
        if (!jQuery().wysihtml5) {
            return;
        }

        if ($('.wysihtml5').size() > 0) {
            $('.wysihtml5').wysihtml5({
                "stylesheets": ["plugins/bootstrap-wysihtml5/wysiwyg-color.css"]
            });
        }
    }

    var handleToggleButtons = function () {
        if (!jQuery().toggleButtons) {
            return;
        }
        $('.basic-toggle-button').toggleButtons();
        $('.text-toggle-button').toggleButtons({
            width: 200,
            label: {
                enabled: "Lorem Ipsum",
                disabled: "Dolor Sit"
            }
        });
        $('.danger-toggle-button').toggleButtons({
            style: {
                // Accepted values ["primary", "danger", "info", "success", "warning"] or nothing
                enabled: "danger",
                disabled: "info"
            }
        });
        $('.info-toggle-button').toggleButtons({
            style: {
                enabled: "info",
                disabled: ""
            }
        });
        $('.success-toggle-button').toggleButtons({
            style: {
                enabled: "success",
                disabled: "info"
            }
        });
        $('.warning-toggle-button').toggleButtons({
            style: {
                enabled: "warning",
                disabled: "info"
            }
        });

        $('.height-toggle-button').toggleButtons({
            height: 100,
            font: {
                'line-height': '100px',
                'font-size': '20px',
                'font-style': 'italic'
            }
        });
    }

    var handleTagsInput = function () {
        if (!jQuery().tagsInput) {
            return;
        }
        $('#tags_1').tagsInput({
            width: 'auto',
            'onAddTag': function () {
                //alert(1);
            },
        });
        $('#tags_2').tagsInput({
            width: 240
        });
    }

    var handlejQueryUIDatePickers = function () {
        $( ".ui-date-picker" ).datepicker();
    }

    var handleDatePickers = function () {

        // if (jQuery().datepicker) {
        //     $('.date-picker').datepicker({
        //         rtl : App.isRTL()
        //     });
        // }
    }

    var handleTimePickers = function () {
        
        if (jQuery().timepicker) {
            $('.timepicker-default').timepicker();
            $('.timepicker-24').timepicker({
                minuteStep: 1,
                showSeconds: true,
                showMeridian: false
            });
        }
    }

    var handleDateRangePickers = function () {
        if (!jQuery().daterangepicker) {
            return;
        }

        $('.date-range').daterangepicker(
            {
                opens: (App.isRTL() ? 'left' : 'right'),
                format: 'MM/dd/yyyy',
                separator: ' to ',
                startDate: Date.today().add({
                    days: -29
                }),
                endDate: Date.today(),
                minDate: '01/01/2012',
                maxDate: '12/31/2014',
            }
        );

        $('#form-date-range').daterangepicker({
            ranges: {
                'Today': ['today', 'today'],
                'Yesterday': ['yesterday', 'yesterday'],
                'Last 7 Days': [Date.today().add({
                        days: -6
                    }), 'today'],
                'Last 29 Days': [Date.today().add({
                        days: -29
                    }), 'today'],
                'This Month': [Date.today().moveToFirstDayOfMonth(), Date.today().moveToLastDayOfMonth()],
                'Last Month': [Date.today().moveToFirstDayOfMonth().add({
                        months: -1
                    }), Date.today().moveToFirstDayOfMonth().add({
                        days: -1
                    })]
            },
            opens: (App.isRTL() ? 'left' : 'right'),
            format: 'MM/dd/yyyy',
            separator: ' to ',
            startDate: Date.today().add({
                days: -29
            }),
            endDate: Date.today(),
            minDate: '01/01/2012',
            maxDate: '12/31/2014',
            locale: {
                applyLabel: 'Submit',
                fromLabel: 'From',
                toLabel: 'To',
                customRangeLabel: 'Custom Range',
                daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                firstDay: 1
            },
            showWeekNumbers: true,
            buttonClasses: ['btn-danger']
        },

        function (start, end) {
            $('#form-date-range span').html(start.toString('MMMM d, yyyy') + ' - ' + end.toString('MMMM d, yyyy'));
        });

        $('#form-date-range span').html(Date.today().add({
            days: -29
        }).toString('MMMM d, yyyy') + ' - ' + Date.today().toString('MMMM d, yyyy'));


        //modal version:

        $('#form-date-range-modal').daterangepicker({
            ranges: {
                'Today': ['today', 'today'],
                'Yesterday': ['yesterday', 'yesterday'],
                'Last 7 Days': [Date.today().add({
                        days: -6
                    }), 'today'],
                'Last 29 Days': [Date.today().add({
                        days: -29
                    }), 'today'],
                'This Month': [Date.today().moveToFirstDayOfMonth(), Date.today().moveToLastDayOfMonth()],
                'Last Month': [Date.today().moveToFirstDayOfMonth().add({
                        months: -1
                    }), Date.today().moveToFirstDayOfMonth().add({
                        days: -1
                    })]
            },
            opens: (App.isRTL() ? 'left' : 'right'),
            format: 'MM/dd/yyyy',
            separator: ' to ',
            startDate: Date.today().add({
                days: -29
            }),
            endDate: Date.today(),
            minDate: '01/01/2012',
            maxDate: '12/31/2014',
            locale: {
                applyLabel: 'Submit',
                fromLabel: 'From',
                toLabel: 'To',
                customRangeLabel: 'Custom Range',
                daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                firstDay: 1
            },
            showWeekNumbers: true,
            buttonClasses: ['btn-danger']
        },

        function (start, end) {
            $('#form-date-range-modal span').html(start.toString('MMMM d, yyyy') + ' - ' + end.toString('MMMM d, yyyy'));
        });

        $('#form-date-range-modal span').html(Date.today().add({
            days: -29
        }).toString('MMMM d, yyyy') + ' - ' + Date.today().toString('MMMM d, yyyy'));

    }

    var handleDatetimePicker = function () {        

          $(".form_datetime").datetimepicker({
              format: "dd MM yyyy - hh:ii",
              pickerPosition: (App.isRTL() ? "bottom-right" : "bottom-left")
          });

         $(".form_advance_datetime").datetimepicker({
              format: "dd MM yyyy - hh:ii",
              autoclose: true,
              todayBtn: true,
              startDate: "2013-02-14 10:00",
              pickerPosition: (App.isRTL() ? "bottom-right" : "bottom-left"),
              minuteStep: 10
          });

         $(".form_meridian_datetime").datetimepicker({
            format: "dd M yyyy - HH:ii P",
            showMeridian: true,
            autoclose: true,
            pickerPosition: (App.isRTL() ? "bottom-right" : "bottom-left"),
            todayBtn: true
        });
    }

    var handleClockfaceTimePickers = function () {

        if (!jQuery().clockface) {
            return;
        }

        $('.clockface_1').clockface();

        $('#clockface_2').clockface({
            format: 'HH:mm',
            trigger: 'manual'
        });

        $('#clockface_2_toggle').click(function (e) {
            e.stopPropagation();
            $('#clockface_2').clockface('toggle');
        });

        $('#clockface_2_modal').clockface({
            format: 'HH:mm',
            trigger: 'manual'
        });

        $('#clockface_2_modal_toggle').click(function (e) {
            e.stopPropagation();
            $('#clockface_2_modal').clockface('toggle');
        });

        $('.clockface_3').clockface({
            format: 'H:mm'
        }).clockface('show', '14:30');
    }

    var handleColorPicker = function () {
        if (!jQuery().colorpicker) {
            return;
        }
        $('.colorpicker-default').colorpicker({
            format: 'hex'
        });
        $('.colorpicker-rgba').colorpicker();
    }

    var handleSelec2 = function () {

        /*$('#select2_sample1').select2({
            placeholder: "Select an option",
            allowClear: true
        });

        $('#select2_sample2').select2({
            placeholder: "Select a State",
            allowClear: true
        });

        $("#select2_sample3").select2({
            allowClear: true,
            minimumInputLength: 1,
            query: function (query) {
                var data = {
                    results: []
                }, i, j, s;
                for (i = 1; i < 5; i++) {
                    s = "";
                    for (j = 0; j < i; j++) {
                        s = s + query.term;
                    }
                    data.results.push({
                        id: query.term + i,
                        text: s
                    });
                }
                query.callback(data);
            }
        });*/

        function format(state) {
            if (!state.id) return state.text; // optgroup
            return "<img class='flag' src='img/flags/" + state.id.toLowerCase() + ".png'/>&nbsp;&nbsp;" + state.text;
        }
        
        /*$("#select2_sample4").select2({
            allowClear: true,
            formatResult: format,
            formatSelection: format,
            escapeMarkup: function (m) {
                return m;
            }
        });

        $("#select2_sample5").select2({
            tags: ["red", "green", "blue", "yellow", "pink"]
        });


        function movieFormatResult(movie) {
            var markup = "<table class='movie-result'><tr>";
            if (movie.posters !== undefined && movie.posters.thumbnail !== undefined) {
                markup += "<td valign='top'><img src='" + movie.posters.thumbnail + "'/></td>";
            }
            markup += "<td valign='top'><h5>" + movie.title + "</h5>";
            if (movie.critics_consensus !== undefined) {
                markup += "<div class='movie-synopsis'>" + movie.critics_consensus + "</div>";
            } else if (movie.synopsis !== undefined) {
                markup += "<div class='movie-synopsis'>" + movie.synopsis + "</div>";
            }
            markup += "</td></tr></table>"
            return markup;
        }

        function movieFormatSelection(movie) {
            return movie.title;
        }

        $("#select2_sample6").select2({
            placeholder: "Search for a movie",
            minimumInputLength: 1,
            ajax: { // instead of writing the function to execute the request we use Select2's convenient helper
                url: "http://api.rottentomatoes.com/api/public/v1.0/movies.json",
                dataType: 'jsonp',
                data: function (term, page) {
                    return {
                        q: term, // search term
                        page_limit: 10,
                        apikey: "ju6z9mjyajq2djue3gbvv26t" // please do not use so this example keeps working
                    };
                },
                results: function (data, page) { // parse the results into the format expected by Select2.
                    // since we are using custom formatting functions we do not need to alter remote JSON data
                    return {
                        results: data.movies
                    };
                }
            },
            initSelection: function (element, callback) {
                // the input tag has a value attribute preloaded that points to a preselected movie's id
                // this function resolves that id attribute to an object that select2 can render
                // using its formatResult renderer - that way the movie name is shown preselected
                var id = $(element).val();
                if (id !== "") {
                    $.ajax("http://api.rottentomatoes.com/api/public/v1.0/movies/" + id + ".json", {
                        data: {
                            apikey: "ju6z9mjyajq2djue3gbvv26t"
                        },
                        dataType: "jsonp"
                    }).done(function (data) {
                        callback(data);
                    });
                }
            },
            formatResult: movieFormatResult, // omitted for brevity, see the source of this page
            formatSelection: movieFormatSelection, // omitted for brevity, see the source of this page
            dropdownCssClass: "bigdrop", // apply css that makes the dropdown taller
            escapeMarkup: function (m) {
                return m;
            } // we do not want to escape markup since we are displaying html in results
        });*/
    }

    var handleMultiSelect = function () {
        /*$('#my_multi_select1').multiSelect();
        $('#my_multi_select2').multiSelect({
            selectableOptgroup: true
        });*/
    }

    var handleInputMasks = function () {
        /*$.extend($.inputmask.defaults, {
            'autounmask': true
        });

        $("#mask_date").inputmask("d/m/y", {autoUnmask: true});  //direct mask        
        $("#mask_date1").inputmask("d/m/y",{ "placeholder": "*"}); //change the placeholder
        $("#mask_date2").inputmask("d/m/y",{ "placeholder": "dd/mm/yyyy" }); //multi-char placeholder
        $("#mask_phone").inputmask("mask", {"mask": "(999) 999-9999"}); //specifying fn & options
        $("#mask_tin").inputmask({"mask": "99-9999999"}); //specifying options only
        $("#mask_number").inputmask({ "mask": "9", "repeat": 10, "greedy": false });  // ~ mask "9" or mask "99" or ... mask "9999999999"
        $("#mask_decimal").inputmask('decimal', { rightAlignNumerics: false }); //disables the right alignment of the decimal input
        $("#mask_currency").inputmask('€ 999.999.999,99', { numericInput: true });  //123456  =>  € ___.__1.234,56
       
        $("#mask_currency2").inputmask('€ 999,999,999.99', { numericInput: true, rightAlignNumerics: false, greedy: false}); //123456  =>  € ___.__1.234,56
        $("#mask_ssn").inputmask("999-99-9999", {placeholder:" ", clearMaskOnLostFocus: true }); //default
        */
    }

    var handleIPAddressInput = function () {
        /*$('#input_ipv4').ipAddress();
        $('#input_ipv6').ipAddress({v:6});*/
    }

    return {
        //main function to initiate the module
        init: function () {
            handleWysihtml5();
            handleToggleButtons();
            handleTagsInput();
            handlejQueryUIDatePickers();
            handleDatePickers();
            handleTimePickers();
            handleDatetimePicker();
            handleDateRangePickers();
            handleClockfaceTimePickers();
            handleColorPicker();
            handleSelec2();
            handleInputMasks();
            handleIPAddressInput();
            handleMultiSelect();

            App.addResponsiveHandler(function(){
                resetWysihtml5();
            })
        }

    };

}();



//form-editable.js
/*var FormEditable = function () {

    $.mockjaxSettings.responseTime = 500;

    var log = function (settings, response) {
        var s = [],
            str;
        s.push(settings.type.toUpperCase() + ' url = "' + settings.url + '"');
        for (var a in settings.data) {
            if (settings.data[a] && typeof settings.data[a] === 'object') {
                str = [];
                for (var j in settings.data[a]) {
                    str.push(j + ': "' + settings.data[a][j] + '"');
                }
                str = '{ ' + str.join(', ') + ' }';
            } else {
                str = '"' + settings.data[a] + '"';
            }
            s.push(a + ' = ' + str);
        }
        s.push('RESPONSE: status = ' + response.status);

        if (response.responseText) {
            if ($.isArray(response.responseText)) {
                s.push('[');
                $.each(response.responseText, function (i, v) {
                    s.push('{value: ' + v.value + ', text: "' + v.text + '"}');
                });
                s.push(']');
            } else {
                s.push($.trim(response.responseText));
            }
        }
        s.push('--------------------------------------\n');
        $('#console').val(s.join('\n') + $('#console').val());
    }

    var initAjaxMock = function () {
        //ajax mocks

        $.mockjax({
            url: '/post',
            response: function (settings) {
                log(settings, this);
            }
        });

        $.mockjax({
            url: '/error',
            status: 400,
            statusText: 'Bad Request',
            response: function (settings) {
                this.responseText = 'Please input correct value';
                log(settings, this);
            }
        });

        $.mockjax({
            url: '/status',
            status: 500,
            response: function (settings) {
                this.responseText = 'Internal Server Error';
                log(settings, this);
            }
        });

        $.mockjax({
            url: '/groups',
            response: function (settings) {
                this.responseText = [{
                        value: 0,
                        text: 'Guest'
                    }, {
                        value: 1,
                        text: 'Service'
                    }, {
                        value: 2,
                        text: 'Customer'
                    }, {
                        value: 3,
                        text: 'Operator'
                    }, {
                        value: 4,
                        text: 'Support'
                    }, {
                        value: 5,
                        text: 'Admin'
                    }
                ];
                log(settings, this);
            }
        });

    }

    var initEditables = function () {
        //defaults

        if (App.getURLParameter('mode') == 'inline') {
            $.fn.editable.defaults.mode = 'inline';
            $('#inline').attr("checked", true);
            jQuery.uniform.update('#inline');
        } else {
            $('#inline').attr("checked", false);
            jQuery.uniform.update('#inline');
        }

        $.fn.editable.defaults.inputclass = 'm-wrap';
        $.fn.editable.defaults.url = '/post';
        $.fn.editableform.buttons = '<button type="submit" class="btn blue editable-submit"><i class="icon-ok"></i></button>';
        $.fn.editableform.buttons += '<button type="button" class="btn editable-cancel"><i class="icon-remove"></i></button>';

        //editables 
        $('#username').editable({
            url: '/post',
            type: 'text',
            pk: 1,
            name: 'username',
            title: 'Enter username'
        });

        $('#firstname').editable({
            validate: function (value) {
                if ($.trim(value) == '') return 'This field is required';
            }
        });

        $('#sex').editable({
            prepend: "not selected",
            source: [{
                    value: 1,
                    text: 'Male'
                }, {
                    value: 2,
                    text: 'Female'
                }
            ],
            display: function (value, sourceData) {
                var colors = {
                    "": "gray",
                    1: "green",
                    2: "blue"
                },
                    elem = $.grep(sourceData, function (o) {
                        return o.value == value;
                    });

                if (elem.length) {
                    $(this).text(elem[0].text).css("color", colors[value]);
                } else {
                    $(this).empty();
                }
            }
        });

        $('#status').editable();

        $('#group').editable({
            showbuttons: false
        });

        $('#vacation').editable();

        $('#dob').editable({
            inputclass: 'm-wrap',
        });

        $('#event').editable({
            placement: 'right',
            combodate: {
                firstItem: 'name'
            }
        });

        $('#meeting_start').editable({
            format: 'yyyy-mm-dd hh:ii',
            viewformat: 'dd/mm/yyyy hh:ii',
            validate: function (v) {
                if (v && v.getDate() == 10) return 'Day cant be 10!';
            },
            datetimepicker: {
                todayBtn: 'linked',
                weekStart: 1
            }
        });

        $('#comments').editable({
            showbuttons: 'bottom'
        });

        $('#note').editable({
            showbuttons : 'right',
            inputclass : '',
        });

        $('#pencil').click(function (e) {
            e.stopPropagation();
            e.preventDefault();
            $('#note').editable('toggle');
        });

        $('#state').editable({
            source: ["Alabama", "Alaska", "Arizona", "Arkansas", "California", "Colorado", "Connecticut", "Delaware", "Florida", "Georgia", "Hawaii", "Idaho", "Illinois", "Indiana", "Iowa", "Kansas", "Kentucky", "Louisiana", "Maine", "Maryland", "Massachusetts", "Michigan", "Minnesota", "Mississippi", "Missouri", "Montana", "Nebraska", "Nevada", "New Hampshire", "New Jersey", "New Mexico", "New York", "North Dakota", "North Carolina", "Ohio", "Oklahoma", "Oregon", "Pennsylvania", "Rhode Island", "South Carolina", "South Dakota", "Tennessee", "Texas", "Utah", "Vermont", "Virginia", "Washington", "West Virginia", "Wisconsin", "Wyoming"]
        });

        $('#fruits').editable({
            pk: 1,
            limit: 3,
            source: [{
                    value: 1,
                    text: 'banana'
                }, {
                    value: 2,
                    text: 'peach'
                }, {
                    value: 3,
                    text: 'apple'
                }, {
                    value: 4,
                    text: 'watermelon'
                }, {
                    value: 5,
                    text: 'orange'
                }
            ]
        });

        $('#tags').editable({
            inputclass: 'input-large m-wrap',
            select2: {
                tags: ['html', 'javascript', 'css', 'ajax'],
                tokenSeparators: [",", " "]
            }
        });

        var countries = [];
        $.each({
            "BD": "Bangladesh",
            "BE": "Belgium",
            "BF": "Burkina Faso",
            "BG": "Bulgaria",
            "BA": "Bosnia and Herzegovina",
            "BB": "Barbados",
            "WF": "Wallis and Futuna",
            "BL": "Saint Bartelemey",
            "BM": "Bermuda",
            "BN": "Brunei Darussalam",
            "BO": "Bolivia",
            "BH": "Bahrain",
            "BI": "Burundi",
            "BJ": "Benin",
            "BT": "Bhutan",
            "JM": "Jamaica",
            "BV": "Bouvet Island",
            "BW": "Botswana",
            "WS": "Samoa",
            "BR": "Brazil",
            "BS": "Bahamas",
            "JE": "Jersey",
            "BY": "Belarus",
            "O1": "Other Country",
            "LV": "Latvia",
            "RW": "Rwanda",
            "RS": "Serbia",
            "TL": "Timor-Leste",
            "RE": "Reunion",
            "LU": "Luxembourg",
            "TJ": "Tajikistan",
            "RO": "Romania",
            "PG": "Papua New Guinea",
            "GW": "Guinea-Bissau",
            "GU": "Guam",
            "GT": "Guatemala",
            "GS": "South Georgia and the South Sandwich Islands",
            "GR": "Greece",
            "GQ": "Equatorial Guinea",
            "GP": "Guadeloupe",
            "JP": "Japan",
            "GY": "Guyana",
            "GG": "Guernsey",
            "GF": "French Guiana",
            "GE": "Georgia",
            "GD": "Grenada",
            "GB": "United Kingdom",
            "GA": "Gabon",
            "SV": "El Salvador",
            "GN": "Guinea",
            "GM": "Gambia",
            "GL": "Greenland",
            "GI": "Gibraltar",
            "GH": "Ghana",
            "OM": "Oman",
            "TN": "Tunisia",
            "JO": "Jordan",
            "HR": "Croatia",
            "HT": "Haiti",
            "HU": "Hungary",
            "HK": "Hong Kong",
            "HN": "Honduras",
            "HM": "Heard Island and McDonald Islands",
            "VE": "Venezuela",
            "PR": "Puerto Rico",
            "PS": "Palestinian Territory",
            "PW": "Palau",
            "PT": "Portugal",
            "SJ": "Svalbard and Jan Mayen",
            "PY": "Paraguay",
            "IQ": "Iraq",
            "PA": "Panama",
            "PF": "French Polynesia",
            "BZ": "Belize",
            "PE": "Peru",
            "PK": "Pakistan",
            "PH": "Philippines",
            "PN": "Pitcairn",
            "TM": "Turkmenistan",
            "PL": "Poland",
            "PM": "Saint Pierre and Miquelon",
            "ZM": "Zambia",
            "EH": "Western Sahara",
            "RU": "Russian Federation",
            "EE": "Estonia",
            "EG": "Egypt",
            "TK": "Tokelau",
            "ZA": "South Africa",
            "EC": "Ecuador",
            "IT": "Italy",
            "VN": "Vietnam",
            "SB": "Solomon Islands",
            "EU": "Europe",
            "ET": "Ethiopia",
            "SO": "Somalia",
            "ZW": "Zimbabwe",
            "SA": "Saudi Arabia",
            "ES": "Spain",
            "ER": "Eritrea",
            "ME": "Montenegro",
            "MD": "Moldova, Republic of",
            "MG": "Madagascar",
            "MF": "Saint Martin",
            "MA": "Morocco",
            "MC": "Monaco",
            "UZ": "Uzbekistan",
            "MM": "Myanmar",
            "ML": "Mali",
            "MO": "Macao",
            "MN": "Mongolia",
            "MH": "Marshall Islands",
            "MK": "Macedonia",
            "MU": "Mauritius",
            "MT": "Malta",
            "MW": "Malawi",
            "MV": "Maldives",
            "MQ": "Martinique",
            "MP": "Northern Mariana Islands",
            "MS": "Montserrat",
            "MR": "Mauritania",
            "IM": "Isle of Man",
            "UG": "Uganda",
            "TZ": "Tanzania, United Republic of",
            "MY": "Malaysia",
            "MX": "Mexico",
            "IL": "Israel",
            "FR": "France",
            "IO": "British Indian Ocean Territory",
            "FX": "France, Metropolitan",
            "SH": "Saint Helena",
            "FI": "Finland",
            "FJ": "Fiji",
            "FK": "Falkland Islands (Malvinas)",
            "FM": "Micronesia, Federated States of",
            "FO": "Faroe Islands",
            "NI": "Nicaragua",
            "NL": "Netherlands",
            "NO": "Norway",
            "NA": "Namibia",
            "VU": "Vanuatu",
            "NC": "New Caledonia",
            "NE": "Niger",
            "NF": "Norfolk Island",
            "NG": "Nigeria",
            "NZ": "New Zealand",
            "NP": "Nepal",
            "NR": "Nauru",
            "NU": "Niue",
            "CK": "Cook Islands",
            "CI": "Cote d'Ivoire",
            "CH": "Switzerland",
            "CO": "Colombia",
            "CN": "China",
            "CM": "Cameroon",
            "CL": "Chile",
            "CC": "Cocos (Keeling) Islands",
            "CA": "Canada",
            "CG": "Congo",
            "CF": "Central African Republic",
            "CD": "Congo, The Democratic Republic of the",
            "CZ": "Czech Republic",
            "CY": "Cyprus",
            "CX": "Christmas Island",
            "CR": "Costa Rica",
            "CV": "Cape Verde",
            "CU": "Cuba",
            "SZ": "Swaziland",
            "SY": "Syrian Arab Republic",
            "KG": "Kyrgyzstan",
            "KE": "Kenya",
            "SR": "Suriname",
            "KI": "Kiribati",
            "KH": "Cambodia",
            "KN": "Saint Kitts and Nevis",
            "KM": "Comoros",
            "ST": "Sao Tome and Principe",
            "SK": "Slovakia",
            "KR": "Korea, Republic of",
            "SI": "Slovenia",
            "KP": "Korea, Democratic People's Republic of",
            "KW": "Kuwait",
            "SN": "Senegal",
            "SM": "San Marino",
            "SL": "Sierra Leone",
            "SC": "Seychelles",
            "KZ": "Kazakhstan",
            "KY": "Cayman Islands",
            "SG": "Singapore",
            "SE": "Sweden",
            "SD": "Sudan",
            "DO": "Dominican Republic",
            "DM": "Dominica",
            "DJ": "Djibouti",
            "DK": "Denmark",
            "VG": "Virgin Islands, British",
            "DE": "Germany",
            "YE": "Yemen",
            "DZ": "Algeria",
            "US": "United States",
            "UY": "Uruguay",
            "YT": "Mayotte",
            "UM": "United States Minor Outlying Islands",
            "LB": "Lebanon",
            "LC": "Saint Lucia",
            "LA": "Lao People's Democratic Republic",
            "TV": "Tuvalu",
            "TW": "Taiwan",
            "TT": "Trinidad and Tobago",
            "TR": "Turkey",
            "LK": "Sri Lanka",
            "LI": "Liechtenstein",
            "A1": "Anonymous Proxy",
            "TO": "Tonga",
            "LT": "Lithuania",
            "A2": "Satellite Provider",
            "LR": "Liberia",
            "LS": "Lesotho",
            "TH": "Thailand",
            "TF": "French Southern Territories",
            "TG": "Togo",
            "TD": "Chad",
            "TC": "Turks and Caicos Islands",
            "LY": "Libyan Arab Jamahiriya",
            "VA": "Holy See (Vatican City State)",
            "VC": "Saint Vincent and the Grenadines",
            "AE": "United Arab Emirates",
            "AD": "Andorra",
            "AG": "Antigua and Barbuda",
            "AF": "Afghanistan",
            "AI": "Anguilla",
            "VI": "Virgin Islands, U.S.",
            "IS": "Iceland",
            "IR": "Iran, Islamic Republic of",
            "AM": "Armenia",
            "AL": "Albania",
            "AO": "Angola",
            "AN": "Netherlands Antilles",
            "AQ": "Antarctica",
            "AP": "Asia/Pacific Region",
            "AS": "American Samoa",
            "AR": "Argentina",
            "AU": "Australia",
            "AT": "Austria",
            "AW": "Aruba",
            "IN": "India",
            "AX": "Aland Islands",
            "AZ": "Azerbaijan",
            "IE": "Ireland",
            "ID": "Indonesia",
            "UA": "Ukraine",
            "QA": "Qatar",
            "MZ": "Mozambique"
        }, function (k, v) {
            countries.push({
                id: k,
                text: v
            });
        });

        $('#country').editable({
            inputclass: 'input-large',
            source: countries
        });

        $('#address').editable({
            url: '/post',
            value: {
                city: "Moscow",
                street: "Lenina",
                building: "12"
            },
            validate: function (value) {
                if (value.city == '') return 'city is required!';
            },
            display: function (value) {
                if (!value) {
                    $(this).empty();
                    return;
                }
                var html = '<b>' + $('<div>').text(value.city).html() + '</b>, ' + $('<div>').text(value.street).html() + ' st., bld. ' + $('<div>').text(value.building).html();
                $(this).html(html);
            }
        });
    }

    return {
        //main function to initiate the module
        init: function () {

            initAjaxMock();

            initEditables();

            $('#enable').click(function () {
                $('#user .editable').editable('toggleDisabled');
            });

            $('#inline').on('change', function (e) {
                if ($('#inline').is(':checked')) {
                    window.location.href = 'form_editable.html?mode=inline';
                } else {
                    window.location.href = 'form_editable.html';
                }
            });

            $('#user .editable').on('hidden', function (e, reason) {
                if (reason === 'save' || reason === 'nochange') {
                    var $next = $(this).closest('tr').next().find('.editable');
                    if ($('#autoopen').is(':checked')) {
                        setTimeout(function () {
                            $next.editable('show');
                        }, 300);
                    } else {
                        $next.focus();
                    }
                }
            });


        }

    };

}();*/



//form-fileupload.js 
var FormFileUpload = function () {


    return {
        //main function to initiate the module
        init: function () {

            // Initialize the jQuery File Upload widget:
            $('#fileupload').fileupload({
                // Uncomment the following to send cross-domain cookies:
                //xhrFields: {withCredentials: true},
                url: 'plugins/jquery-file-upload/server/php/'
            });

            // Load existing files:
            // Demo settings:
            $.ajax({
                // Uncomment the following to send cross-domain cookies:
                //xhrFields: {withCredentials: true},
                url: $('#fileupload').fileupload('option', 'url'),
                dataType: 'json',
                context: $('#fileupload')[0],
                maxFileSize: 5000000,
                acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
                process: [{
                        action: 'load',
                        fileTypes: /^image\/(gif|jpeg|png)$/,
                        maxFileSize: 20000000 // 20MB
                    }, {
                        action: 'resize',
                        maxWidth: 1440,
                        maxHeight: 900
                    }, {
                        action: 'save'
                    }
                ]
            }).done(function (result) {
                $(this).fileupload('option', 'done')
                    .call(this, null, {
                    result: result
                });
            });

            // Upload server status check for browsers with CORS support:
            if ($.support.cors) {
                $.ajax({
                    url: 'plugins/jquery-file-upload/server/php/',
                    type: 'HEAD'
                }).fail(function () {
                    $('<span class="alert alert-error"/>')
                        .text('Upload server currently unavailable - ' +
                        new Date())
                        .appendTo('#fileupload');
                });
            }

            // initialize uniform checkboxes  
            App.initUniform('.fileupload-toggle-checkbox');
        }

    };

}();



//form-samples.js
// var FormSamples = function () {


//     return {
//         //main function to initiate the module
//         init: function () {

//             // use select2 dropdown instead of chosen as select2 works fine with bootstrap on responsive layouts.
//             $('.select2_category').select2({
// 	            placeholder: "Select an option",
// 	            allowClear: true
// 	        });

//             $('.select2_sample1').select2({
//                 placeholder: "Select a State",
//                 allowClear: true
//             });

//             $(".select2_sample2").select2({
//                 placeholder: "Type to select an option",
//                 allowClear: true,
//                 minimumInputLength: 1,
//                 query: function (query) {
//                     var data = {
//                         results: []
//                     }, i, j, s;
//                     for (i = 1; i < 5; i++) {
//                         s = "";
//                         for (j = 0; j < i; j++) {
//                             s = s + query.term;
//                         }
//                         data.results.push({
//                             id: query.term + i,
//                             text: s
//                         });
//                     }
//                     query.callback(data);
//                 }
//             });

//             $(".select2_sample3").select2({
//                 tags: ["red", "green", "blue", "yellow", "pink"]
//             });

//         }

//     };

// }();




//form-validation.js
// var FormValidation = function () {


//     return {
//         //main function to initiate the module
//         init: function () {

//             // for more info visit the official plugin documentation: 
//             // http://docs.jquery.com/Plugins/Validation

//             var form1 = $('#form_sample_1');
//             var error1 = $('.alert-error', form1);
//             var success1 = $('.alert-success', form1);

//             form1.validate({
//                 errorElement: 'span', //default input error message container
//                 errorClass: 'help-inline', // default input error message class
//                 focusInvalid: false, // do not focus the last invalid input
//                 ignore: "",
//                 rules: {
//                     name: {
//                         minlength: 2,
//                         required: true
//                     },
//                     email: {
//                         required: true,
//                         email: true
//                     },
//                     url: {
//                         required: true,
//                         url: true
//                     },
//                     number: {
//                         required: true,
//                         number: true
//                     },
//                     digits: {
//                         required: true,
//                         digits: true
//                     },
//                     creditcard: {
//                         required: true,
//                         creditcard: true
//                     },
//                     occupation: {
//                         minlength: 5,
//                     },
//                     category: {
//                         required: true
//                     }
//                 },

//                 invalidHandler: function (event, validator) { //display error alert on form submit              
//                     success1.hide();
//                     error1.show();
//                     App.scrollTo(error1, -200);
//                 },

//                 highlight: function (element) { // hightlight error inputs
//                     $(element)
//                         .closest('.help-inline').removeClass('ok'); // display OK icon
//                     $(element)
//                         .closest('.control-group').removeClass('success').addClass('error'); // set error class to the control group
//                 },

//                 unhighlight: function (element) { // revert the change dony by hightlight
//                     $(element)
//                         .closest('.control-group').removeClass('error'); // set error class to the control group
//                 },

//                 success: function (label) {
//                     label
//                         .addClass('valid').addClass('help-inline ok') // mark the current input as valid and display OK icon
//                     .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
//                 },

//                 submitHandler: function (form) {
//                     success1.show();
//                     error1.hide();
//                 }
//             });

//             //Sample 2
//             $('#form_2_select2').select2({
//                 placeholder: "Select an Option",
//                 allowClear: true
//             });

//             var form2 = $('#form_sample_2');
//             var error2 = $('.alert-error', form2);
//             var success2 = $('.alert-success', form2);

//             form2.validate({
//                 errorElement: 'span', //default input error message container
//                 errorClass: 'help-inline', // default input error message class
//                 focusInvalid: false, // do not focus the last invalid input
//                 ignore: "",
//                 rules: {
//                     name: {
//                         minlength: 2,
//                         required: true
//                     },
//                     email: {
//                         required: true,
//                         email: true
//                     },
//                     category: {
//                         required: true
//                     },
//                     options1: {
//                         required: true
//                     },
//                     options2: {
//                         required: true
//                     },
//                     occupation: {
//                         minlength: 5,
//                     },
//                     membership: {
//                         required: true
//                     },
//                     service: {
//                         required: true,
//                         minlength: 2
//                     }
//                 },

//                 messages: { // custom messages for radio buttons and checkboxes
//                     membership: {
//                         required: "Please select a Membership type"
//                     },
//                     service: {
//                         required: "Please select  at least 2 types of Service",
//                         minlength: jQuery.format("Please select  at least {0} types of Service")
//                     }
//                 },

//                 errorPlacement: function (error, element) { // render error placement for each input type
//                     if (element.attr("name") == "education") { // for chosen elements, need to insert the error after the chosen container
//                         error.insertAfter("#form_2_education_chzn");
//                     } else if (element.attr("name") == "membership") { // for uniform radio buttons, insert the after the given container
//                         error.addClass("no-left-padding").insertAfter("#form_2_membership_error");
//                     } else if (element.attr("name") == "service") { // for uniform checkboxes, insert the after the given container
//                         error.addClass("no-left-padding").insertAfter("#form_2_service_error");
//                     } else {
//                         error.insertAfter(element); // for other inputs, just perform default behavoir
//                     }
//                 },

//                 invalidHandler: function (event, validator) { //display error alert on form submit   
//                     success2.hide();
//                     error2.show();
//                     App.scrollTo(error2, -200);
//                 },

//                 highlight: function (element) { // hightlight error inputs
//                     $(element)
//                         .closest('.help-inline').removeClass('ok'); // display OK icon
//                     $(element)
//                         .closest('.control-group').removeClass('success').addClass('error'); // set error class to the control group
//                 },

//                 unhighlight: function (element) { // revert the change dony by hightlight
//                     $(element)
//                         .closest('.control-group').removeClass('error'); // set error class to the control group
//                 },

//                 success: function (label) {
//                     if (label.attr("for") == "service" || label.attr("for") == "membership") { // for checkboxes and radip buttons, no need to show OK icon
//                         label
//                             .closest('.control-group').removeClass('error').addClass('success');
//                         label.remove(); // remove error label here
//                     } else { // display success icon for other inputs
//                         label
//                             .addClass('valid').addClass('help-inline ok') // mark the current input as valid and display OK icon
//                         .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
//                     }
//                 },

//                 submitHandler: function (form) {
//                     success2.show();
//                     error2.hide();
//                 }

//             });

//             //apply validation on chosen dropdown value change, this only needed for chosen dropdown integration.
//             $('.chosen, .chosen-with-diselect', form2).change(function () {
//                 form2.validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
//             });

//              //apply validation on select2 dropdown value change, this only needed for chosen dropdown integration.
//             $('.select2', form2).change(function () {
//                 form2.validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
//             });

//         }

//     };

// }();




//form-wizard.js
var SetupWizard = function () {


    return {
        //main function to initiate the module
        init: function () {
            if (!jQuery().bootstrapWizard) {
                return;
            }

            // default form wizard
            $('#setup_wizard').bootstrapWizard({
                'nextSelector': '.button-next',
                'previousSelector': '.button-previous',
                onTabClick: function (tab, navigation, index) {
                    return false;
                },
                onNext: function (tab, navigation, index) {

                    setTimeout(function() {
                        $('#setup_wizard input').trigger('change');
                    }, 200);

                    if($('#setup_wizard .continue').hasClass('disabled')) {

                        return false;

                    } else {

                        var total = navigation.find('li').length;
                        var current = index + 1;

                        // var email_regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                        // var alpha_regex = /^[a-zA-Z ]+$/;
                        // var alpha_num_regex = /^[a-zA-Z0-9_ ]*$/;
                        // var alpha_num_nospace_regex = /^[a-zA-Z0-9_]*$/;
                        // var password_regex = /(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/;

                        // if($('#signup_form #tab1').hasClass("active1")) {

                        //     var errors = 0;

                        //     if($('#first_name').val() == "" || !alpha_regex.test($('#first_name').val())) {
                        //         $('#first_name').css("border", "1px solid #f85e5e");
                        //         errors++;
                        //     }

                        //     if($('#last_name').val() == "" || !alpha_regex.test($('#last_name').val())) {
                        //         $('#last_name').css("border", "1px solid #f85e5e");
                        //         errors++;
                        //     }

                        //     if($('#username').val() == "" || !alpha_num_regex.test($('#username').val())) {
                        //         $('#username').css("border", "1px solid #f85e5e");
                        //         errors++;
                        //     }

                        //     if($('#email').val() == "" || !email_regex.test($('#email').val())) {
                        //         $('#email').css("border", "1px solid #f85e5e");
                        //         errors++;
                        //     }

                        //     if($('#password').val() == "" || !password_regex.test($('#password').val())) {
                        //         $('#password').css("border", "1px solid #f85e5e");
                        //         errors++;
                        //     }

                        //     if($('#password_confirm').val() == "") {
                        //         $('#password_confirm').css("border", "1px solid #f85e5e");
                        //         errors++;
                        //     }

                        //     if($('#password').val() != $('#password_confirm').val()) {
                        //         $('#password_confirm').css("border", "1px solid #f85e5e");
                        //         errors++;
                        //     }

                        //     if(errors > 0) {

                        //         return false;
                        //     }

                        // }


                        // if($('#signup_form #tab2').hasClass("active1")) {

                        //     var errors = 0;

                        //     if($('#business_name').val() == "" || !alpha_num_regex.test($('#business_name').val())) {
                        //         $('#business_name').css("border", "1px solid #f85e5e");
                        //         errors++;
                        //     }

                        //     if($('#subdomain').val() == "" || !alpha_num_nospace_regex.test($('#subdomain').val())) {
                        //         $('#subdomain').css("border", "1px solid #f85e5e");
                        //         errors++;
                        //     }

                        //     if($('#business_type option:selected').val() < 0) {
                        //         $('#business_type').css("border", "1px solid #f85e5e");
                        //         errors++;
                        //     }

                        //     if($('#country option:selected').val() == "") {
                        //         $('#country').css("border", "1px solid #f85e5e");
                        //         errors++;
                        //     }


                        //     if(errors > 0) {

                        //         return false;
                        //     }

                        // }



                        // set wizard title
                        //$('.step-title').text((index + 1) + ' of ' + total);
                        // set done steps
                        jQuery('li').removeClass("done");
                        var li_list = navigation.find('li');
                        for (var i = 0; i < index; i++) {
                            jQuery(li_list[i]).addClass("done");
                        }

                        if (current == 1) {
                            $('#setup_wizard').find('.button-previous').hide();
                        } else {
                            $('#setup_wizard').find('.button-previous').show();
                        }

                        if (current >= total) {
                            $('#setup_wizard').find('.button-next').hide();
                            $('#setup_wizard').find('.button-submit').show();
                        } else {
                            $('#setup_wizard').find('.button-next').show();
                            $('#setup_wizard').find('.button-submit').hide();
                        }

                    }

                },
                onPrevious: function (tab, navigation, index) {

                    $('#setup_wizard').trigger('tab_changed');

                    var total = navigation.find('li').length;
                    var current = index + 1;
                    // set wizard title
                    //$('.step-title', $('#signup_form')).text((index + 1) + ' of ' + total);
                    // set done steps
                    jQuery('li', $('#setup_wizard')).removeClass("done");
                    var li_list = navigation.find('li');
                    for (var i = 0; i < index; i++) {
                        jQuery(li_list[i]).addClass("done");
                    }

                    if (current == 1) {
                        $('#setup_wizard').find('.button-previous').hide();
                    } else {
                        $('#setup_wizard').find('.button-previous').show();
                    }

                    if (current >= total) {
                        $('#setup_wizard').find('.button-next').hide();
                        $('#setup_wizard').find('.button-submit').show();
                    } else {
                        $('#setup_wizard').find('.button-next').show();
                        $('#setup_wizard').find('.button-submit').hide();
                    }

                },
                onTabShow: function (tab, navigation, index) {
                    var total = navigation.find('li').length;
                    var current = index + 1;
                    var $percent = (current / total) * 100;
                    $('#setup_wizard').find('#bar .bar').css({
                        width: $percent + '%'
                    });
                }
            });

            $('#setup_wizard').find('.button-previous').hide();
            $('#setup_wizard .button-submit').click(function () {
                //alert('Finished! Hope you like it :)');
            }).hide();
        }

    };

}();


//gallery.js
// var Gallery = function () {

//     return {
//         //main function to initiate the module
//         init: function () {

//             App.initFancybox();

//         }

//     };

// }();


//inbox.js
// var Inbox = function () {

//     var content = $('.inbox-content');
//     var loading = $('.inbox-loading');

//     var loadInbox = function (name) {
//         var url = 'inbox_inbox.php';
//         var title = $('.inbox-nav > li.' + name + ' a').attr('data-title');

//         loading.show();
//         content.html('');

//         $.post(url, {}, function (res) {
//             $('.inbox-nav > li.active').removeClass('active');
//             $('.inbox-nav > li.' + name).addClass('active');
//             $('.inbox-header > h1').text(title);

//             loading.hide();
//             content.html(res);
//             App.fixContentHeight();
//             App.initUniform();
//         });
//     }

//     var loadMessage = function (name, resetMenu) {
//         var url = 'inbox_view.php';

//         loading.show();
//         content.html('');

//         $.post(url, {}, function (res) {
//             if (resetMenu) {
//                 $('.inbox-nav > li.active').removeClass('active');
//             }
//             $('.inbox-header > h1').text('View Message');

//             loading.hide();
//             content.html(res);
//             App.fixContentHeight();
//             App.initUniform();
//         });
//     }

//     var initTags = function (input) {
//         input.tag({
//             autosizedInput: true,
//             containerClasses: 'span12',
//             inputClasses: 'm-wrap',
//             source: function (query, process) {
//                 return [
//                     'Bob Nilson <bob.nilson@metronic.com>',
//                     'Lisa Miller <lisa.miller@metronic.com>',
//                     'Test <test@domain.com>',
//                     'Dino <dino@demo.com>',
//                     'Support <support@demo.com>']
//             }
//         });
//     }

//     var initWysihtml5 = function () {
//         $('.inbox-wysihtml5').wysihtml5({
//             "stylesheets": ["assets/plugins/bootstrap-wysihtml5/wysiwyg-color.css"]
//         });
//     }

//     var initFileupload = function () {

//         $('#fileupload').fileupload({
//             // Uncomment the following to send cross-domain cookies:
//             //xhrFields: {withCredentials: true},
//             url: 'assets/plugins/jquery-file-upload/server/php/',
//             autoUpload: true
//         });

//         // Upload server status check for browsers with CORS support:
//         if ($.support.cors) {
//             $.ajax({
//                 url: 'assets/plugins/jquery-file-upload/server/php/',
//                 type: 'HEAD'
//             }).fail(function () {
//                 $('<span class="alert alert-error"/>')
//                     .text('Upload server currently unavailable - ' +
//                     new Date())
//                     .appendTo('#fileupload');
//             });
//         }
//     }

//     var loadCompose = function () {
//         var url = 'inbox_compose.php';

//         loading.show();
//         content.html('');

//         // load the form via ajax
//         $.post(url, {}, function (res) {
//             $('.inbox-nav > li.active').removeClass('active');
//             $('.inbox-header > h1').text('Compose');

//             loading.hide();
//             content.html(res);

//             initTags($('[name="to"]'));
//             initFileupload();
//             initWysihtml5();

//             $('.inbox-wysihtml5').focus();
//             App.fixContentHeight();
//             App.initUniform();
//         });
//     }

//     var loadReply = function () {
//         var url = 'inbox_reply.php';

//         loading.show();
//         content.html('');

//         // load the form via ajax
//         $.post(url, {}, function (res) {
//             $('.inbox-nav > li.active').removeClass('active');
//             $('.inbox-header > h1').text('Reply');

//             loading.hide();
//             content.html(res);
//             $('[name="message"]').val($('#reply_email_content_body').html());

//             initTags($('[name="to"]')); // init "TO" input field
//             handleCCInput(); // init "CC" input field

//             initFileupload();
//             initWysihtml5();
//             App.fixContentHeight();
//             App.initUniform();
//         });
//     }

//     var loadSearchResults = function () {
//         var url = 'inbox_search_result.php';

//         loading.show();
//         content.html('');

//         $.post(url, {}, function (res) {
//             $('.inbox-nav > li.active').removeClass('active');
//             $('.inbox-header > h1').text('Search');

//             loading.hide();
//             content.html(res);
//             App.fixContentHeight();
//             App.initUniform();
//         });
//     }

//     var handleCCInput = function () {
//         var the = $('.inbox-compose .mail-to .inbox-cc');
//         var input = $('.inbox-compose .input-cc');
//         the.hide();
//         input.show();
//         initTags($('[name="cc"]'), -10);
//         $('.close', input).click(function () {
//             input.hide();
//             the.show();
//         });
//     }

//     var handleBCCInput = function () {

//         var the = $('.inbox-compose .mail-to .inbox-bcc');
//         var input = $('.inbox-compose .input-bcc');
//         the.hide();
//         input.show();
//         initTags($('[name="bcc"]'), -10);
//         $('.close', input).click(function () {
//             input.hide();
//             the.show();
//         });
//     }

//     return {
//         //main function to initiate the module
//         init: function () {

//             // handle compose btn click
//             $('.inbox .compose-btn a').live('click', function () {
//                 loadCompose();
//             });

//             // handle reply and forward button click
//             $('.inbox .reply-btn').live('click', function () {
//                 loadReply();
//             });

//             // handle view message
//             $('.inbox-content .view-message').live('click', function () {
//                 loadMessage();
//             });

//             // handle inbox listing
//             $('.inbox-nav > li.inbox > a').click(function () {
//                 loadInbox('inbox');
//             });

//             // handle sent listing
//             $('.inbox-nav > li.sent > a').click(function () {
//                 loadInbox('sent');
//             });

//             // handle draft listing
//             $('.inbox-nav > li.draft > a').click(function () {
//                 loadInbox('draft');
//             });

//             // handle trash listing
//             $('.inbox-nav > li.trash > a').click(function () {
//                 loadInbox('trash');
//             });

//             //handle compose/reply cc input toggle
//             $('.inbox-compose .mail-to .inbox-cc').live('click', function () {
//                 handleCCInput();
//             });

//             //handle compose/reply bcc input toggle
//             $('.inbox-compose .mail-to .inbox-bcc').live('click', function () {
//                 handleBCCInput();
//             });

//             //handle loading content based on URL parameter
//             if (App.getURLParameter("a") === "view") {
//                 loadMessage();
//             } else if (App.getURLParameter("a") === "compose") {
//                 loadCompose();
//             } else {
//                 loadInbox('inbox');
//             }

//         }

//     };

// }();



//index.js
// var Index = function () {


//     return {

//         //main function to initiate the module
//         init: function () {

//             App.addResponsiveHandler(function () {
//                 Index.initCalendar();
//                 jQuery('.vmaps').each(function () {
//                     var map = jQuery(this);
//                     map.width(map.parent().width());
//                 });
//             });
//         },

//         initJQVMAP: function () {

//             var showMap = function (name) {
//                 jQuery('.vmaps').hide();
//                 jQuery('#vmap_' + name).show();
//             }

//             var setMap = function (name) {
//                 var data = {
//                     map: 'world_en',
//                     backgroundColor: null,
//                     borderColor: '#333333',
//                     borderOpacity: 0.5,
//                     borderWidth: 1,
//                     color: '#c6c6c6',
//                     enableZoom: true,
//                     hoverColor: '#c9dfaf',
//                     hoverOpacity: null,
//                     values: sample_data,
//                     normalizeFunction: 'linear',
//                     scaleColors: ['#b6da93', '#909cae'],
//                     selectedColor: '#c9dfaf',
//                     selectedRegion: null,
//                     showTooltip: true,
//                     onLabelShow: function (event, label, code) {

//                     },
//                     onRegionOver: function (event, code) {
//                         if (code == 'ca') {
//                             event.preventDefault();
//                         }
//                     },
//                     onRegionClick: function (element, code, region) {
//                         var message = 'You clicked "' + region + '" which has the code: ' + code.toUpperCase();
//                         alert(message);
//                     }
//                 };

//                 data.map = name + '_en';
//                 var map = jQuery('#vmap_' + name);
//                 if (!map) {
//                     return;
//                 }
//                 map.width(map.parent().parent().width());
//                 map.show();
//                 map.vectorMap(data);
//                 map.hide();
//             }

//             setMap("world");
//             setMap("usa");
//             setMap("europe");
//             setMap("russia");
//             setMap("germany");
//             showMap("world");

//             jQuery('#regional_stat_world').click(function () {
//                 showMap("world");
//             });

//             jQuery('#regional_stat_usa').click(function () {
//                 showMap("usa");
//             });

//             jQuery('#regional_stat_europe').click(function () {
//                 showMap("europe");
//             });
//             jQuery('#regional_stat_russia').click(function () {
//                 showMap("russia");
//             });
//             jQuery('#regional_stat_germany').click(function () {
//                 showMap("germany");
//             });

//             $('#region_statistics_loading').hide();
//             $('#region_statistics_content').show();
//         },

//         initCalendar: function () {
//             if (!jQuery().fullCalendar) {
//                 return;
//             }

//             var date = new Date();
//             var d = date.getDate();
//             var m = date.getMonth();
//             var y = date.getFullYear();

//             var h = {};

//             if ($('#calendar').width() <= 400) {
//                 $('#calendar').addClass("mobile");
//                 h = {
//                     left: 'title, prev, next',
//                     center: '',
//                     right: 'today,month,agendaWeek,agendaDay'
//                 };
//             } else {
//                 $('#calendar').removeClass("mobile");
//                 if (App.isRTL()) {
//                     h = {
//                         right: 'title',
//                         center: '',
//                         left: 'prev,next,today,month,agendaWeek,agendaDay'
//                     };
//                 } else {
//                     h = {
//                         left: 'title',
//                         center: '',
//                         right: 'prev,next,today,month,agendaWeek,agendaDay'
//                     };
//                 }               
//             }

//             $('#calendar').fullCalendar('destroy'); // destroy the calendar
//             $('#calendar').fullCalendar({ //re-initialize the calendar
//                 disableDragging: false,
//                 header: h,
//                 editable: true,
//                 events: [{
//                         title: 'All Day Event',                        
//                         start: new Date(y, m, 1),
//                         backgroundColor: App.getLayoutColorCode('yellow')
//                     }, {
//                         title: 'Long Event',
//                         start: new Date(y, m, d - 5),
//                         end: new Date(y, m, d - 2),
//                         backgroundColor: App.getLayoutColorCode('green')
//                     }, {
//                         title: 'Repeating Event',
//                         start: new Date(y, m, d - 3, 16, 0),
//                         allDay: false,
//                         backgroundColor: App.getLayoutColorCode('red')
//                     }, {
//                         title: 'Repeating Event',
//                         start: new Date(y, m, d + 4, 16, 0),
//                         allDay: false,
//                         backgroundColor: App.getLayoutColorCode('green')
//                     }, {
//                         title: 'Meeting',
//                         start: new Date(y, m, d, 10, 30),
//                         allDay: false,
//                     }, {
//                         title: 'Lunch',
//                         start: new Date(y, m, d, 12, 0),
//                         end: new Date(y, m, d, 14, 0),
//                         backgroundColor: App.getLayoutColorCode('grey'),
//                         allDay: false,
//                     }, {
//                         title: 'Birthday Party',
//                         start: new Date(y, m, d + 1, 19, 0),
//                         end: new Date(y, m, d + 1, 22, 30),
//                         backgroundColor: App.getLayoutColorCode('purple'),
//                         allDay: false,
//                     }, {
//                         title: 'Click for Google',
//                         start: new Date(y, m, 28),
//                         end: new Date(y, m, 29),
//                         backgroundColor: App.getLayoutColorCode('yellow'),
//                         url: 'http://google.com/',
//                     }
//                 ]
//             });
//         },

//         initCharts: function () {
//             if (!jQuery.plot) {
//                 return;
//             }

//             var data = [];
//             var totalPoints = 250;

//             // random data generator for plot charts

//             function getRandomData() {
//                 if (data.length > 0) data = data.slice(1);
//                 // do a random walk
//                 while (data.length < totalPoints) {
//                     var prev = data.length > 0 ? data[data.length - 1] : 50;
//                     var y = prev + Math.random() * 10 - 5;
//                     if (y < 0) y = 0;
//                     if (y > 100) y = 100;
//                     data.push(y);
//                 }
//                 // zip the generated y values with the x values
//                 var res = [];
//                 for (var i = 0; i < data.length; ++i) res.push([i, data[i]])
//                 return res;
//             }

//             function showTooltip(title, x, y, contents) {
//                 $('<div id="tooltip" class="chart-tooltip"><div class="date">' + title + '<\/div><div class="label label-success">CTR: ' + x / 10 + '%<\/div><div class="label label-important">Imp: ' + x * 12 + '<\/div><\/div>').css({
//                     position: 'absolute',
//                     display: 'none',
//                     top: y - 100,
//                     width: 75,
//                     left: x - 40,
//                     border: '0px solid #ccc',
//                     padding: '2px 6px',
//                     'background-color': '#fff',
//                 }).appendTo("body").fadeIn(200);
//             }

//             function randValue() {
//                 return (Math.floor(Math.random() * (1 + 50 - 20))) + 10;
//             }

//             var pageviews = [
//                 [1, randValue()],
//                 [2, randValue()],
//                 [3, 2 + randValue()],
//                 [4, 3 + randValue()],
//                 [5, 5 + randValue()],
//                 [6, 10 + randValue()],
//                 [7, 15 + randValue()],
//                 [8, 20 + randValue()],
//                 [9, 25 + randValue()],
//                 [10, 30 + randValue()],
//                 [11, 35 + randValue()],
//                 [12, 25 + randValue()],
//                 [13, 15 + randValue()],
//                 [14, 20 + randValue()],
//                 [15, 45 + randValue()],
//                 [16, 50 + randValue()],
//                 [17, 65 + randValue()],
//                 [18, 70 + randValue()],
//                 [19, 85 + randValue()],
//                 [20, 80 + randValue()],
//                 [21, 75 + randValue()],
//                 [22, 80 + randValue()],
//                 [23, 75 + randValue()],
//                 [24, 70 + randValue()],
//                 [25, 65 + randValue()],
//                 [26, 75 + randValue()],
//                 [27, 80 + randValue()],
//                 [28, 85 + randValue()],
//                 [29, 90 + randValue()],
//                 [30, 95 + randValue()]
//             ];

//             var visitors = [
//                 [1, randValue() - 5],
//                 [2, randValue() - 5],
//                 [3, randValue() - 5],
//                 [4, 6 + randValue()],
//                 [5, 5 + randValue()],
//                 [6, 20 + randValue()],
//                 [7, 25 + randValue()],
//                 [8, 36 + randValue()],
//                 [9, 26 + randValue()],
//                 [10, 38 + randValue()],
//                 [11, 39 + randValue()],
//                 [12, 50 + randValue()],
//                 [13, 51 + randValue()],
//                 [14, 12 + randValue()],
//                 [15, 13 + randValue()],
//                 [16, 14 + randValue()],
//                 [17, 15 + randValue()],
//                 [18, 15 + randValue()],
//                 [19, 16 + randValue()],
//                 [20, 17 + randValue()],
//                 [21, 18 + randValue()],
//                 [22, 19 + randValue()],
//                 [23, 20 + randValue()],
//                 [24, 21 + randValue()],
//                 [25, 14 + randValue()],
//                 [26, 24 + randValue()],
//                 [27, 25 + randValue()],
//                 [28, 26 + randValue()],
//                 [29, 27 + randValue()],
//                 [30, 31 + randValue()]
//             ];

//             if ($('#site_statistics').size() != 0) {

//                 $('#site_statistics_loading').hide();
//                 $('#site_statistics_content').show();

//                 var plot_statistics = $.plot($("#site_statistics"), [{
//                         data: pageviews,
//                         label: "Unique Visits"
//                     }, {
//                         data: visitors,
//                         label: "Page Views"
//                     }
//                 ], {
//                     series: {
//                         lines: {
//                             show: true,
//                             lineWidth: 2,
//                             fill: true,
//                             fillColor: {
//                                 colors: [{
//                                         opacity: 0.05
//                                     }, {
//                                         opacity: 0.01
//                                     }
//                                 ]
//                             }
//                         },
//                         points: {
//                             show: true
//                         },
//                         shadowSize: 2
//                     },
//                     grid: {
//                         hoverable: true,
//                         clickable: true,
//                         tickColor: "#eee",
//                         borderWidth: 0
//                     },
//                     colors: ["#d12610", "#37b7f3", "#52e136"],
//                     xaxis: {
//                         ticks: 11,
//                         tickDecimals: 0
//                     },
//                     yaxis: {
//                         ticks: 11,
//                         tickDecimals: 0
//                     }
//                 });

//                 var previousPoint = null;
//                 $("#site_statistics").bind("plothover", function (event, pos, item) {
//                     $("#x").text(pos.x.toFixed(2));
//                     $("#y").text(pos.y.toFixed(2));
//                     if (item) {
//                         if (previousPoint != item.dataIndex) {
//                             previousPoint = item.dataIndex;

//                             $("#tooltip").remove();
//                             var x = item.datapoint[0].toFixed(2),
//                                 y = item.datapoint[1].toFixed(2);

//                             showTooltip('24 Jan 2013', item.pageX, item.pageY, item.series.label + " of " + x + " = " + y);
//                         }
//                     } else {
//                         $("#tooltip").remove();
//                         previousPoint = null;
//                     }
//                 });
//             }               

//             if ($('#load_statistics').size() != 0) {
//                  //server load
//                 $('#load_statistics_loading').hide();
//                 $('#load_statistics_content').show();
        
//                 var updateInterval = 30;
//                 var plot_statistics = $.plot($("#load_statistics"), [getRandomData()], {
//                 series: {
//                     shadowSize: 1
//                 },
//                 lines: {
//                     show: true,
//                     lineWidth: 0.2,
//                     fill: true,
//                     fillColor: {
//                         colors: [{
//                                 opacity: 0.1
//                             }, {
//                                 opacity: 1
//                             }
//                         ]
//                     }
//                 },
//                 yaxis: {
//                     min: 0,
//                     max: 100,
//                     tickFormatter: function (v) {
//                         return v + "%";
//                     }
//                 },
//                 xaxis: {
//                     show: false
//                 },
//                 colors: ["#e14e3d"],
//                 grid: {
//                     tickColor: "#a8a3a3",
//                     borderWidth: 0
//                 }
//                 });
                
//                 function statisticsUpdate() {
//                 plot_statistics.setData([getRandomData()]);
//                 plot_statistics.draw();
//                 setTimeout(statisticsUpdate, updateInterval);
                
//                 }
                
//                 statisticsUpdate();

//                 $('#load_statistics').bind("mouseleave", function () {
//                     $("#tooltip").remove();
//                 });
//             }

//             if ($('#site_activities').size() != 0) {
//                 //site activities
//                 var previousPoint2 = null;
//                 $('#site_activities_loading').hide();
//                 $('#site_activities_content').show();

//                 var activities = [
//                     [1, 10],
//                     [2, 9],
//                     [3, 8],
//                     [4, 6],
//                     [5, 5],
//                     [6, 3],
//                     [7, 9],
//                     [8, 10],
//                     [9, 12],
//                     [10, 14],
//                     [11, 15],
//                     [12, 13],
//                     [13, 11],
//                     [14, 10],
//                     [15, 9],
//                     [16, 8],
//                     [17, 12],
//                     [18, 14],
//                     [19, 16],
//                     [20, 19],
//                     [21, 20],
//                     [22, 20],
//                     [23, 19],
//                     [24, 17],
//                     [25, 15],
//                     [25, 14],
//                     [26, 12],
//                     [27, 10],
//                     [28, 8],
//                     [29, 10],
//                     [30, 12],
//                     [31, 10],
//                     [32, 9],
//                     [33, 8],
//                     [34, 6],
//                     [35, 5],
//                     [36, 3],
//                     [37, 9],
//                     [38, 10],
//                     [39, 12],
//                     [40, 14],
//                     [41, 15],
//                     [42, 13],
//                     [43, 11],
//                     [44, 10],
//                     [45, 9],
//                     [46, 8],
//                     [47, 12],
//                     [48, 14],
//                     [49, 16],
//                     [50, 12],
//                     [51, 10]
//                 ];

//                 var plot_activities = $.plot(
//                     $("#site_activities"), [{
//                         data: activities,
//                         color: "rgba(107,207,123, 0.9)",
//                         shadowSize: 0,
//                         bars: {
//                             show: true,
//                             lineWidth: 0,
//                             fill: true,
//                             fillColor: {
//                                 colors: [{
//                                         opacity: 1
//                                     }, {
//                                         opacity: 1
//                                     }
//                                 ]
//                             }
//                         }
//                     }
//                 ], {
//                     series: {
//                         bars: {
//                             show: true,
//                             barWidth: 0.9
//                         }
//                     },
//                     grid: {
//                         show: false,
//                         hoverable: true,
//                         clickable: false,
//                         autoHighlight: true,
//                         borderWidth: 0
//                     },
//                     yaxis: {
//                         min: 0,
//                         max: 20
//                     }
//                 });

//                 $("#site_activities").bind("plothover", function (event, pos, item) {
//                     $("#x").text(pos.x.toFixed(2));
//                     $("#y").text(pos.y.toFixed(2));
//                     if (item) {
//                         if (previousPoint2 != item.dataIndex) {
//                             previousPoint2 = item.dataIndex;
//                             $("#tooltip").remove();
//                             var x = item.datapoint[0].toFixed(2),
//                                 y = item.datapoint[1].toFixed(2);
//                             showTooltip('24 Feb 2013', item.pageX, item.pageY, x);
//                         }
//                     }
//                 });

//                 $('#site_activities').bind("mouseleave", function () {
//                     $("#tooltip").remove();
//                 });
//             }
//         },

//         initMiniCharts: function () {
             
//             $('.easy-pie-chart .number.transactions').easyPieChart({
//                 animate: 1000,
//                 size: 75,
//                 lineWidth: 3,
//                 barColor: App.getLayoutColorCode('yellow')
//             });

//             $('.easy-pie-chart .number.visits').easyPieChart({
//                 animate: 1000,
//                 size: 75,
//                 lineWidth: 3,
//                 barColor: App.getLayoutColorCode('green')
//             });
             
//             $('.easy-pie-chart .number.bounce').easyPieChart({
//                 animate: 1000,
//                 size: 75,
//                 lineWidth: 3,
//                 barColor: App.getLayoutColorCode('red')
//             });

//             $('.easy-pie-chart-reload').click(function(){
//                 $('.easy-pie-chart .number').each(function() {
//                     var newValue = Math.floor(100*Math.random());
//                     $(this).data('easyPieChart').update(newValue);
//                     $('span', this).text(newValue);
//                 });
//             });
               
//             $("#sparkline_bar").sparkline([8,9,10,11,10,10,12,10,10,11,9,12,11,10,9,11,13,13,12], {
//                 type: 'bar',
//                 width: '100',
//                 barWidth: 5,
//                 height: '55',
//                 barColor: '#35aa47',
//                 negBarColor: '#e02222'}
//             );

//             $("#sparkline_bar2").sparkline([9,11,12,13,12,13,10,14,13,11,11,12,11,11,10,12,11,10], {
//                 type: 'bar',
//                 width: '100',
//                 barWidth: 5,
//                 height: '55',
//                 barColor: '#ffb848',
//                 negBarColor: '#e02222'}
//             );

//             $("#sparkline_line").sparkline([9,10,9,10,10,11,12,10,10,11,11,12,11,10,12,11,10,12], {
//                 type: 'line',
//                 width: '100',
//                 height: '55',
//                 lineColor: '#ffb848'
//             });

//         },

//         initChat: function () {

//             var cont = $('#chats');
//             var list = $('.chats', cont);
//             var form = $('.chat-form', cont);
//             var input = $('input', form);
//             var btn = $('.btn', form);

//             var handleClick = function (e) {
//                 e.preventDefault();
                
//                 var text = input.val();
//                 if (text.length == 0) {
//                     return;
//                 }

//                 var time = new Date();
//                 var time_str = time.toString('MMM dd, yyyy hh:mm');
//                 var tpl = '';
//                 tpl += '<li class="out">';
//                 tpl += '<img class="avatar" alt="" src="img/avatar1.jpg"/>';
//                 tpl += '<div class="message">';
//                 tpl += '<span class="arrow"></span>';
//                 tpl += '<a href="#" class="name">Bob Nilson</a>&nbsp;';
//                 tpl += '<span class="datetime">at ' + time_str + '</span>';
//                 tpl += '<span class="body">';
//                 tpl += text;
//                 tpl += '</span>';
//                 tpl += '</div>';
//                 tpl += '</li>';

//                 var msg = list.append(tpl);
//                 input.val("");
//                 $('.scroller', cont).slimScroll({
//                     scrollTo: list.height()
//                 });
//             }

//             /*
//             $('.scroller', cont).slimScroll({
//                 scrollTo: list.height()
//             });
//             */


//             var chat = $('.chat');

//             btn.click(handleClick);
//             input.keypress(function (e) {
//                 if (e.which == 13) {
//                     handleClick();
//                     return false; //<---- Add this line
//                 }
//             });
//         },

//         initDashboardDaterange: function () {

//             $('#dashboard-report-range').daterangepicker({
//                 ranges: {
//                     'Today': ['today', 'today'],
//                     'Yesterday': ['yesterday', 'yesterday'],
//                     'Last 7 Days': [Date.today().add({
//                             days: -6
//                         }), 'today'],
//                     'Last 30 Days': [Date.today().add({
//                             days: -29
//                         }), 'today'],
//                     'This Month': [Date.today().moveToFirstDayOfMonth(), Date.today().moveToLastDayOfMonth()],
//                     'Last Month': [Date.today().moveToFirstDayOfMonth().add({
//                             months: -1
//                         }), Date.today().moveToFirstDayOfMonth().add({
//                             days: -1
//                         })]
//                 },
//                 opens: (App.isRTL() ? 'right' : 'left'),
//                 format: 'MM/dd/yyyy',
//                 separator: ' to ',
//                 startDate: Date.today().add({
//                     days: -29
//                 }),
//                 endDate: Date.today(),
//                 minDate: '01/01/2012',
//                 maxDate: '12/31/2014',
//                 locale: {
//                     applyLabel: 'Submit',
//                     fromLabel: 'From',
//                     toLabel: 'To',
//                     customRangeLabel: 'Custom Range',
//                     daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
//                     monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
//                     firstDay: 1
//                 },
//                 showWeekNumbers: true,
//                 buttonClasses: ['btn-danger']
//             },

//             function (start, end) {
//                 App.blockUI(jQuery("#dashboard"));
//                 setTimeout(function () {
//                     App.unblockUI(jQuery("#dashboard"));
//                     $.gritter.add({
//                         title: 'Dashboard',
//                         text: 'Dashboard date range updated.'
//                     });
//                     App.scrollTo();
//                 }, 1000);
//                 $('#dashboard-report-range span').html(start.toString('MMMM d, yyyy') + ' - ' + end.toString('MMMM d, yyyy'));

//             });

//             $('#dashboard-report-range').show();

//             $('#dashboard-report-range span').html(Date.today().add({
//                 days: -29
//             }).toString('MMMM d, yyyy') + ' - ' + Date.today().toString('MMMM d, yyyy'));
//         },

//         initIntro: function () {
//             if ($.cookie('intro_show')) {
//                 return;
//             }

//             $.cookie('intro_show', 1);

//             setTimeout(function () {
//                 var unique_id = $.gritter.add({
//                     // (string | mandatory) the heading of the notification
//                     title: 'Meet Metronic!',
//                     // (string | mandatory) the text inside the notification
//                     text: 'Metronic is a brand new Responsive Admin Dashboard Template you have always been looking for!',
//                     // (string | optional) the image to display on the left
//                     image: 'img/avatar1.jpg',
//                     // (bool | optional) if you want it to fade out on its own or just sit there
//                     sticky: true,
//                     // (int | optional) the time you want it to be alive for before fading out
//                     time: '',
//                     // (string | optional) the class name you want to apply to that specific message
//                     class_name: 'my-sticky-class'
//                 });

//                 // You can have it return a unique id, this can be used to manually remove it later using
//                 setTimeout(function () {
//                     $.gritter.remove(unique_id, {
//                         fade: true,
//                         speed: 'slow'
//                     });
//                 }, 12000);
//             }, 2000);

//             setTimeout(function () {
//                 var unique_id = $.gritter.add({
//                     // (string | mandatory) the heading of the notification
//                     title: 'Buy Metronic!',
//                     // (string | mandatory) the text inside the notification
//                     text: 'Metronic comes with a huge collection of reusable and easy customizable UI components and plugins. Buy Metronic today!',
//                     // (string | optional) the image to display on the left
//                     image: 'img/avatar1.jpg',
//                     // (bool | optional) if you want it to fade out on its own or just sit there
//                     sticky: true,
//                     // (int | optional) the time you want it to be alive for before fading out
//                     time: '',
//                     // (string | optional) the class name you want to apply to that specific message
//                     class_name: 'my-sticky-class'
//                 });

//                 // You can have it return a unique id, this can be used to manually remove it later using
//                 setTimeout(function () {
//                     $.gritter.remove(unique_id, {
//                         fade: true,
//                         speed: 'slow'
//                     });
//                 }, 13000);
//             }, 8000);

//             setTimeout(function () {

//                 $('#styler').pulsate({
//                     color: "#bb3319",
//                     repeat: 10
//                 });

//                 $.extend($.gritter.options, {
//                     position: 'top-left'
//                 });

//                 var unique_id = $.gritter.add({
//                     position: 'top-left',
//                     // (string | mandatory) the heading of the notification
//                     title: 'Customize Metronic!',
//                     // (string | mandatory) the text inside the notification
//                     text: 'Metronic allows you to easily customize the theme colors and layout settings.',
//                     // (string | optional) the image to display on the left
//                     image1: 'img/avatar1.png',
//                     // (bool | optional) if you want it to fade out on its own or just sit there
//                     sticky: true,
//                     // (int | optional) the time you want it to be alive for before fading out
//                     time: '',
//                     // (string | optional) the class name you want to apply to that specific message
//                     class_name: 'my-sticky-class'
//                 });

//                 $.extend($.gritter.options, {
//                     position: 'top-right'
//                 });

//                 // You can have it return a unique id, this can be used to manually remove it later using
//                 setTimeout(function () {
//                     $.gritter.remove(unique_id, {
//                         fade: true,
//                         speed: 'slow'
//                     });
//                 }, 15000);

//             }, 23000);

//             setTimeout(function () {

//                 $.extend($.gritter.options, {
//                     position: 'top-left'
//                 });

//                 var unique_id = $.gritter.add({
//                     // (string | mandatory) the heading of the notification
//                     title: 'Notification',
//                     // (string | mandatory) the text inside the notification
//                     text: 'You have 3 new notifications.',
//                     // (string | optional) the image to display on the left
//                     image1: 'img/image1.jpg',
//                     // (bool | optional) if you want it to fade out on its own or just sit there
//                     sticky: true,
//                     // (int | optional) the time you want it to be alive for before fading out
//                     time: '',
//                     // (string | optional) the class name you want to apply to that specific message
//                     class_name: 'my-sticky-class'
//                 });

//                 setTimeout(function () {
//                     $.gritter.remove(unique_id, {
//                         fade: true,
//                         speed: 'slow'
//                     });
//                 }, 4000);

//                 $.extend($.gritter.options, {
//                     position: 'top-right'
//                 });

//                 var number = $('#header_notification_bar .badge').text();
//                 number = parseInt(number);
//                 number = number + 3;
//                 $('#header_notification_bar .badge').text(number);
//                 $('#header_notification_bar').pulsate({
//                     color: "#66bce6",
//                     repeat: 5
//                 });

//             }, 40000);

//             setTimeout(function () {

//                 $.extend($.gritter.options, {
//                     position: 'top-left'
//                 });

//                 var unique_id = $.gritter.add({
//                     // (string | mandatory) the heading of the notification
//                     title: 'Inbox',
//                     // (string | mandatory) the text inside the notification
//                     text: 'You have 2 new messages in your inbox.',
//                     // (string | optional) the image to display on the left
//                     image1: 'img/avatar1.jpg',
//                     // (bool | optional) if you want it to fade out on its own or just sit there
//                     sticky: true,
//                     // (int | optional) the time you want it to be alive for before fading out
//                     time: '',
//                     // (string | optional) the class name you want to apply to that specific message
//                     class_name: 'my-sticky-class'
//                 });

//                 $.extend($.gritter.options, {
//                     position: 'top-right'
//                 });

//                 setTimeout(function () {
//                     $.gritter.remove(unique_id, {
//                         fade: true,
//                         speed: 'slow'
//                     });
//                 }, 4000);

//                 var number = $('#header_inbox_bar .badge').text();
//                 number = parseInt(number);
//                 number = number + 2;
//                 $('#header_inbox_bar .badge').text(number);
//                 $('#header_inbox_bar').pulsate({
//                     color: "#dd5131",
//                     repeat: 5
//                 });

//             }, 60000);
//         }

//     };

// }();



//lock.js
var Lock = function () {

    return {
        //main function to initiate the module
        init: function () {

              $.backstretch(["img/bg/blurry_dock_small.jpg",
                "img/bg/blurry_dock.jpg",
                "img/bg/sunset.jpg",
                "img/bg/rocks.jpg",
                "img/bg/gudvagen.jpg",
                "img/bg/canoe.jpg",
                "img/bg/sunset_boat.jpg",
                "img/bg/palmtrees.jpg",
                "img/bg/snail.jpg",
                "img/bg/underwater.jpg",
                "img/bg/beach.jpg"
                ], {
                  fade: 2000,
                  duration: 8000
              });
        }
    };

}();



var OverlayLock = function () {

    return {
        //main function to initiate the module
        init: function () {

              $('.login_overlay').backstretch(["img/bg/blurry_dock_small.jpg",
                "img/bg/blurry_dock.jpg",
                "img/bg/sunset.jpg",
                "img/bg/rocks.jpg",
                "img/bg/gudvagen.jpg",
                "img/bg/canoe.jpg",
                "img/bg/sunset_boat.jpg",
                "img/bg/palmtrees.jpg",
                "img/bg/snail.jpg",
                "img/bg/underwater.jpg",
                "img/bg/beach.jpg"
                ], {
                  fade: 2000,
                  duration: 8000
              });
        }
    };

}();



//login.js
var Login = function () {
    
    return {
        //main function to initiate the module
        init: function () {
        	
            $('.login-form').validate({
	            errorElement: 'label', //default input error message container
	            errorClass: 'help-inline', // default input error message class
	            focusInvalid: false, // do not focus the last invalid input
	            rules: {
	                username: {
	                    required: true
	                },
	                password: {
	                    required: true
	                },
	                remember: {
	                    required: false
	                }
	            },

	            messages: {
	                username: {
	                    required: "Username is required."
	                },
	                password: {
	                    required: "Password is required."
	                }
	            },

	            invalidHandler: function (event, validator) { //display error alert on form submit   
	                $('.alert-error', $('.login-form')).show();
	            },

	            highlight: function (element) { // hightlight error inputs
	                //$(element)
	                    //.closest('.control-group').addClass('error'); // set error class to the control group
	            },

	            success: function (label) {
	                //label.closest('.control-group').removeClass('error');
	                label.remove();
	            },

	            errorPlacement: function (error, element) {
	                error.addClass('help-small no-left-padding').insertAfter(element.closest('.input-icon'));
	            },

	        });


	        $('.forget-form').validate({
	            errorElement: 'label', //default input error message container
	            errorClass: 'help-inline', // default input error message class
	            focusInvalid: false, // do not focus the last invalid input
	            ignore: "",
	            rules: {
	                email: {
	                    required: true,
	                    email: true
	                }
	            },

	            messages: {
	                email: {
	                    required: "Email is required."
	                }
	            },

	            invalidHandler: function (event, validator) { //display error alert on form submit   

	            },

	            highlight: function (element) { // hightlight error inputs
	                //$(element)
	                    //.closest('.control-group').addClass('error'); // set error class to the control group
	            },

	            success: function (label) {
	                //label.closest('.control-group').removeClass('error');
	                label.remove();
	            },

	            errorPlacement: function (error, element) {
	                error.addClass('help-small no-left-padding').insertAfter(element.closest('.input-icon'));
	            },

	        });


	        jQuery('#forget-password').click(function () {
	            jQuery('.login-form').hide();
	            jQuery('.forget-form').show();
	        });

	        jQuery('#back-btn').click(function () {
	            jQuery('.login-form').show();
	            jQuery('.forget-form').hide();
	        });

	        /*$('.register-form').validate({
	            errorElement: 'label', //default input error message container
	            errorClass: 'help-inline', // default input error message class
	            focusInvalid: false, // do not focus the last invalid input
	            ignore: "",
	            rules: {
	                username: {
	                    required: true
	                },
	                password: {
	                    required: true
	                },
	                rpassword: {
	                    equalTo: "#register_password"
	                },
	                email: {
	                    required: true,
	                    email: true
	                },
	                tnc: {
	                    required: true
	                }
	            },

	            messages: { // custom messages for radio buttons and checkboxes
	                tnc: {
	                    required: "Please accept TNC first."
	                }
	            },

	            invalidHandler: function (event, validator) { //display error alert on form submit   

	            },

	            highlight: function (element) { // hightlight error inputs
	                $(element)
	                    .closest('.control-group').addClass('error'); // set error class to the control group
	            },

	            success: function (label) {
	                label.closest('.control-group').removeClass('error');
	                label.remove();
	            },

	            errorPlacement: function (error, element) {
	                if (element.attr("name") == "tnc") { // insert checkbox errors after the container                  
	                    error.addClass('help-small no-left-padding').insertAfter($('#register_tnc_error'));
	                } else {
	                    error.addClass('help-small no-left-padding').insertAfter(element.closest('.input-icon'));
	                }
	            },

	        });

	        jQuery('#register-btn').click(function () {
	            jQuery('.login-form').hide();
	            jQuery('.register-form').show();
	        });

	        jQuery('#register-back-btn').click(function () {
	            jQuery('.login-form').show();
	            jQuery('.register-form').hide();
	        });*/
        }

    };

}();



//login-soft.js
var Login = function () {
    
    return {
        //main function to initiate the module
        init: function () {
        	
           $('.login-form').validate({
	            errorElement: 'label', //default input error message container
	            errorClass: 'help-inline', // default input error message class
	            focusInvalid: false, // do not focus the last invalid input
	            rules: {
	                username: {
	                    required: true
	                },
	                password: {
	                    required: true
	                },
	                remember: {
	                    required: false
	                }
	            },

	            messages: {
	                username: {
	                    required: "Username is required."
	                },
	                password: {
	                    required: "Password is required."
	                }
	            },

	            invalidHandler: function (event, validator) { //display error alert on form submit   
	                $('.alert-error', $('.login-form')).show();
	            },

	            highlight: function (element) { // hightlight error inputs
	                $(element)
	                    .closest('.control-group').addClass('error'); // set error class to the control group
	            },

	            success: function (label) {
	                label.closest('.control-group').removeClass('error');
	                label.remove();
	            },

	            errorPlacement: function (error, element) {
	                error.addClass('help-small no-left-padding').insertAfter(element.closest('.input-icon'));
	            },

	        });


	        $('.forget-form').validate({
	            errorElement: 'label', //default input error message container
	            errorClass: 'help-inline', // default input error message class
	            focusInvalid: false, // do not focus the last invalid input
	            ignore: "",
	            rules: {
	                email: {
	                    required: true,
	                    email: true
	                }
	            },

	            messages: {
	                email: {
	                    required: "Email is required."
	                }
	            },

	            invalidHandler: function (event, validator) { //display error alert on form submit   

	            },

	            highlight: function (element) { // hightlight error inputs
	                $(element)
	                    .closest('.control-group').addClass('error'); // set error class to the control group
	            },

	            success: function (label) {
	                label.closest('.control-group').removeClass('error');
	                label.remove();
	            },

	            errorPlacement: function (error, element) {
	                error.addClass('help-small no-left-padding').insertAfter(element.closest('.input-icon'));
	            },

	        });


	        jQuery('#forget-password').click(function () {
	            jQuery('.login-form').hide();
	            jQuery('.forget-form').show();
	        });

	        jQuery('#back-btn').click(function () {
	            jQuery('.login-form').show();
	            jQuery('.forget-form').hide();
	        });

	        $('.register-form').validate({
	            errorElement: 'label', //default input error message container
	            errorClass: 'help-inline', // default input error message class
	            focusInvalid: false, // do not focus the last invalid input
	            ignore: "",
	            rules: {
	                username: {
	                    required: true
	                },
	                password: {
	                    required: true
	                },
	                rpassword: {
	                    equalTo: "#register_password"
	                },
	                email: {
	                    required: true,
	                    email: true
	                },
	                tnc: {
	                    required: true
	                }
	            },

	            messages: { // custom messages for radio buttons and checkboxes
	                tnc: {
	                    required: "Please accept TNC first."
	                }
	            },

	            invalidHandler: function (event, validator) { //display error alert on form submit   

	            },

	            highlight: function (element) { // hightlight error inputs
	                $(element)
	                    .closest('.control-group').addClass('error'); // set error class to the control group
	            },

	            success: function (label) {
	                label.closest('.control-group').removeClass('error');
	                label.remove();
	            },

	            errorPlacement: function (error, element) {
	                if (element.attr("name") == "tnc") { // insert checkbox errors after the container                  
	                    error.addClass('help-small no-left-padding').insertAfter($('#register_tnc_error'));
	                } else {
	                    error.addClass('help-small no-left-padding').insertAfter(element.closest('.input-icon'));
	                }
	            },

	        });

	        jQuery('#register-btn').click(function () {
	            jQuery('.login-form').hide();
	            jQuery('.register-form').show();
	        });

	        jQuery('#register-back-btn').click(function () {
	            jQuery('.login-form').show();
	            jQuery('.register-form').hide();
	        });

	        $.backstretch(["img/bg/blurry_dock_small.jpg",
                "img/bg/blurry_dock.jpg",
                "img/bg/sunset.jpg",
                "img/bg/rocks.jpg",
                "img/bg/gudvagen.jpg",
                "img/bg/canoe.jpg",
                "img/bg/sunset_boat.jpg",
                "img/bg/palmtrees.jpg",
                "img/bg/snail.jpg",
                "img/bg/underwater.jpg",
                "img/bg/beach.jpg"
                ], {
                  fade: 2000,
                  duration: 8000
              });
        }

    };

}();


/*var geocoder;
var map;

//maps.google.js
var MapsGoogle = function () {

    var gmap = function () {
        var map = new GMaps({
            div: '#gmap',
            lat: 40.580585,
            lng: -92.900391,
            zoom: 5
        });

        $('#gmap_remove_markers').click(function() {
            map.setZoom(5);
            map.removeMarkers();
            $("#gmap_geocoding_address").val("");
        });

        var handleAction = function () {
            var text = $.trim($('#gmap_geocoding_address').val());
            GMaps.geocode({
                address: text,
                callback: function (results, status) {
                    if (status == 'OK') {
                        var latlng = results[0].geometry.location;
                        map.setCenter(latlng.lat(), latlng.lng());
                        map.addMarker({
                            lat: latlng.lat(),
                            lng: latlng.lng(),
                            infoWindow: {
                                content: '<span>Open in Google Maps</span><br><span>Get Directions To/From Here</span>'
                            },

                        });

                        map.setZoom(13);

                        App.scrollTo($('#gmap'));
                    }
                }
            });
        }

        $('#gmap_geocoding_btn').click(function (e) {
            e.preventDefault();
            handleAction();
            return false;
        });

        $("#gmap_geocoding_address").keypress(function (e) {
            var keycode = (e.keyCode ? e.keyCode : e.which);
            if (keycode == '13') {
                e.preventDefault();
                handleAction();
            }
        });


        GMaps.geolocate({
            success: function (position) {
                map.setCenter(position.coords.latitude, position.coords.longitude);
            },
            error: function (error) {
                alert('Geolocation failed: ' + error.message);
            },
            not_supported: function () {
                alert("Your browser does not support geolocation");
            },
            always: function () {
                //alert("Geolocation Done!");
            }
        });

        geocoder = new google.maps.Geocoder();



        $('#gmap_routes_start').click(function (e) {
            e.preventDefault();
            App.scrollTo($(this), 400);
            map.travelRoute({
                origin: [-12.044012922866312, -77.02470665341184],
                destination: [-12.090814532191756, -77.02271108990476],
                travelMode: 'driving',
                step: function (e) {
                    $('#gmap_routes_instructions').append('<li>' + e.instructions + '</li>');
                    $('#gmap_routes_instructions li:eq(' + e.step_number + ')').delay(800 * e.step_number).fadeIn(500, function () {
                        map.setCenter(e.end_location.lat(), e.end_location.lng());
                        map.drawPolyline({
                            path: e.path,
                            strokeColor: '#131540',
                            strokeOpacity: 0.6,
                            strokeWeight: 6
                        });
                    });
                }
            });
        });

    }


    function update_ui( address, latLng ) {
        $('#gmap_geocoding_address').autocomplete("close");
        $('#gmap_geocoding_address').val(address);
    }


    function geocode_lookup( type, value, update ) {
        // default value: update = false
        update = typeof update !== 'undefined' ? update : false;

        request = {};
        request[type] = value;

        geocoder.geocode(request, function(results, status) {

          if (status == google.maps.GeocoderStatus.OK) {
            // Google geocoding has succeeded!
            if (results[0]) {
              // Always update the UI elements with new location data
              update_ui( results[0].formatted_address,
                         results[0].geometry.location )

              // Only update the map (position marker and center map) if requested
              if( update ) { update_map( results[0].geometry ) }
            } else {
              // Geocoder status ok but no results!?
            }
          } else {
            // Google Geocoding has failed. Two common reasons:
            //   * Address not recognised (e.g. search for 'zxxzcxczxcx')
            //   * Location doesn't map to address (e.g. click in middle of Atlantic)

            if( type == 'address' ) {
              // User has typed in an address which we can't geocode to a location
            } else {
              // User has clicked or dragged marker to somewhere that Google can't do a
              // reverse lookup for. In this case we display a warning.
              update_ui('', value)
            }
          };
        });
    };


    // initialise the jqueryUI autocomplete element
    var autocomplete_init = function() {
        $("#gmap_geocoding_address").autocomplete({
          types: "geocode",
          componentRestrictions: {country: 'ca'},

          // source is the list of input options shown in the autocomplete dropdown.
          // see documentation: http://jqueryui.com/demos/autocomplete/
          source: function(request,response) {

            // the geocode method takes an address or LatLng to search for
            // and a callback function which should process the results into
            // a format accepted by jqueryUI autocomplete
            geocoder.geocode( {'address': request.term }, function(results, status) {
              response($.map(results, function(item) {
                return {
                  label: item.formatted_address, // appears in dropdown box
                  value: item.formatted_address, // inserted into input element when selected
                  geocode: item                  // all geocode data

                }
              }));
            })
          },

          // event triggered when drop-down option selected
          select: function(event,ui){
            //update_ui(  ui.item.value, ui.item.geocode.geometry.location )
            //update_map( ui.item.geocode.geometry )
          },

          messages: {
                noResults: '',
                results: function() {}
            }
        });

        // triggered when user presses a key in the address box
        $("#gmap_geocoding_address").bind('keydown', function(event) {
          if(event.keyCode == 13) {
            //geocode_lookup( 'address', $('#gmap_geocoding_address').val(), true );

            // ensures dropdown disappears when enter is pressed
            $('#gmap_geocoding_address').autocomplete("disable");
            $('.maps #gmap_geocoding_btn').trigger('click');
          } else {
            // re-enable if previously disabled above
            $('#gmap_geocoding_address').autocomplete("enable")
          }
        });
    }; // autocomplete_init

    


    return {
        //main function to initiate map samples
        init: function () {
            gmap();
            //autocomplete_init();
        }

    };

}();*/

var map;
var directionsService;
var directionsDisplay;

var searchBox;
var handleAction;
var markers;

function initMaps() {

    var directionsOptions = {
        draggable: true
    }

    directionsService = new google.maps.DirectionsService();
    directionsDisplay = new google.maps.DirectionsRenderer(directionsOptions);

    var mapOptions = {
        zoom: 5,
        center: new google.maps.LatLng(40.580585, -92.900391),
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    map = new google.maps.Map(document.getElementById('gmap'), mapOptions);

    directionsDisplay.setMap(map);
    directionsDisplay.setPanel(document.getElementById('directions_panel'));

    google.maps.visualRefresh=true;

    if(navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
          var pos = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

          map.setCenter(pos);
          map.setZoom(8);

        }, function() {
          //Error
        });
    } else {
        // Browser doesn't support Geolocation
    }


    var input = document.getElementById('gmap_geocoding_address');
    var options = {
        radius: 500
    };

    searchBox = new google.maps.places.SearchBox(input, options); //searchBox is different from Autocomplete....

    searchBox.bindTo('bounds', map);



    var directions_start = document.getElementById('start_address');

    directions_start_autocomplete = new google.maps.places.Autocomplete(directions_start, options);

    directions_start_autocomplete.bindTo('bounds', map);



    var directions_end = document.getElementById('end_address');

    directions_end_autocomplete = new google.maps.places.Autocomplete(directions_end, options);

    directions_end_autocomplete.bindTo('bounds', map);


    markers = [];


    google.maps.event.addListener(searchBox, 'places_changed', function() {

        var places = searchBox.getPlaces();

        for (var i = 0, marker; marker = markers[i]; i++) {
          marker.setMap(null);
        }

        markers = [];
        var bounds = new google.maps.LatLngBounds();

        for (var i = 0, place; place = places[i]; i++) {
          var image = {
            url: place.icon,
            size: new google.maps.Size(71, 71),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(17, 34),
            scaledSize: new google.maps.Size(25, 25)
          };

          var marker = new google.maps.Marker({
            map: map,
            icon: image,
            title: place.name,
            position: place.geometry.location
          });

          markers.push(marker);

          bounds.extend(place.geometry.location);

        }

        map.fitBounds(bounds);

        map.setZoom(17);

    });


}




var searchBox;

function initGeoAddress() {


    var add_client = document.getElementById('add_client_geo_address');
    var update_client = document.getElementById('update_client_geo_address');

    searchBox = new google.maps.places.SearchBox(add_client); //searchBox is different from Autocomplete....
    searchBox2 = new google.maps.places.SearchBox(update_client); //searchBox is different from Autocomplete....

}



//search.js
// var Search = function () {

//     return {
//         //main function to initiate the module
//         init: function () {
//             if (jQuery().datepicker) {
//                 $('.date-picker').datepicker();
//             }

//             App.initFancybox();
//         }

//     };

// }();

//table-advanced.js
// var TableAdvanced = function () {

//     var initTable1 = function() {

//         /* Formating function for row details */
//         function fnFormatDetails ( oTable, nTr )
//         {
//             var aData = oTable.fnGetData( nTr );
//             var sOut = '<table>';
//             sOut += '<tr><td>Platform(s):</td><td>'+aData[2]+'</td></tr>';
//             sOut += '<tr><td>Engine version:</td><td>'+aData[3]+'</td></tr>';
//             sOut += '<tr><td>CSS grade:</td><td>'+aData[4]+'</td></tr>';
//             sOut += '<tr><td>Others:</td><td>Could provide a link here</td></tr>';
//             sOut += '</table>';
             
//             return sOut;
//         }

//         /*
//          * Insert a 'details' column to the table
//          */
//         var nCloneTh = document.createElement( 'th' );
//         var nCloneTd = document.createElement( 'td' );
//         nCloneTd.innerHTML = '<span class="row-details row-details-close"></span>';
         
//         $('#sample_1 thead tr').each( function () {
//             this.insertBefore( nCloneTh, this.childNodes[0] );
//         } );
         
//         $('#sample_1 tbody tr').each( function () {
//             this.insertBefore(  nCloneTd.cloneNode( true ), this.childNodes[0] );
//         } );
         
//         /*
//          * Initialse DataTables, with no sorting on the 'details' column
//          */
//         var oTable = $('#sample_1').dataTable( {
//             "aoColumnDefs": [
//                 {"bSortable": false, "aTargets": [ 0 ] }
//             ],
//             "aaSorting": [[1, 'asc']],
//              "aLengthMenu": [
//                 [5, 15, 20, -1],
//                 [5, 15, 20, "All"] // change per page values here
//             ],
//             // set the initial value
//             "iDisplayLength": 10,
//         });

//         jQuery('#sample_1_wrapper .dataTables_filter input').addClass("m-wrap small"); // modify table search input
//         jQuery('#sample_1_wrapper .dataTables_length select').addClass("m-wrap small"); // modify table per page dropdown
//         jQuery('#sample_1_wrapper .dataTables_length select').select2(); // initialzie select2 dropdown
         
//          Add event listener for opening and closing details
//          * Note that the indicator for showing which row is open is not controlled by DataTables,
//          * rather it is done here
         
//         $('#sample_1').on('click', ' tbody td .row-details', function () {
//             var nTr = $(this).parents('tr')[0];
//             if ( oTable.fnIsOpen(nTr) )
//             {
//                 /* This row is already open - close it */
//                 $(this).addClass("row-details-close").removeClass("row-details-open");
//                 oTable.fnClose( nTr );
//             }
//             else
//             {
//                 /* Open this row */                
//                 $(this).addClass("row-details-open").removeClass("row-details-close");
//                 oTable.fnOpen( nTr, fnFormatDetails(oTable, nTr), 'details' );
//             }
//         });
//     }

//      var initTable2 = function() {
//         var oTable = $('#sample_2').dataTable( {           
//             "aoColumnDefs": [
//                 { "aTargets": [ 0 ] }
//             ],
//             "aaSorting": [[1, 'asc']],
//              "aLengthMenu": [
//                 [5, 15, 20, -1],
//                 [5, 15, 20, "All"] // change per page values here
//             ],
//             // set the initial value
//             "iDisplayLength": 10,
//         });

//         jQuery('#sample_2_wrapper .dataTables_filter input').addClass("m-wrap small"); // modify table search input
//         jQuery('#sample_2_wrapper .dataTables_length select').addClass("m-wrap small"); // modify table per page dropdown
//         jQuery('#sample_2_wrapper .dataTables_length select').select2(); // initialzie select2 dropdown

//         $('#sample_2_column_toggler input[type="checkbox"]').change(function(){
//             /* Get the DataTables object again - this is not a recreation, just a get of the object */
//             var iCol = parseInt($(this).attr("data-column"));
//             var bVis = oTable.fnSettings().aoColumns[iCol].bVisible;
//             oTable.fnSetColumnVis(iCol, (bVis ? false : true));
//         });
//     }

//     return {

//         //main function to initiate the module
//         init: function () {
            
//             if (!jQuery().dataTable) {
//                 return;
//             }

//             initTable1();
//             initTable2();
//         }

//     };

// }();



//ul-general.js
// var UIGeneral = function () {

//     var handlePulsate = function () {
//         if (!jQuery().pulsate) {
//             return;
//         }

//         if (App.isIE8() == true) {
//             return; // pulsate plugin does not support IE8 and below
//         }

//         if (jQuery().pulsate) {
//             jQuery('#pulsate-regular').pulsate({
//                 color: "#bf1c56"
//             });

//             jQuery('#pulsate-once').click(function () {
//                 $(this).pulsate({
//                     color: "#399bc3",
//                     repeat: false
//                 });
//             });

//             jQuery('#pulsate-hover').pulsate({
//                 color: "#5ebf5e",
//                 repeat: false,
//                 onHover: true
//             });

//             jQuery('#pulsate-crazy').click(function () {
//                 $(this).pulsate({
//                     color: "#fdbe41",
//                     reach: 50,
//                     repeat: 10,
//                     speed: 100,
//                     glow: true
//                 });
//             });
//         }
//     }

//     var handleGritterNotifications = function () {
//         if (!jQuery.gritter) {
//             return;
//         }

//         $('#gritter-sticky').click(function () {
//             var unique_id = $.gritter.add({
//                 // (string | mandatory) the heading of the notification
//                 title: 'This is a sticky notice!',
//                 // (string | mandatory) the text inside the notification
//                 text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus eget tincidunt velit. Cum sociis natoque penatibus et <a href="#">some link sample</a> montes, nascetur ridiculus mus.',
//                 // (string | optional) the image to display on the left
//                 image: 'img/avatar1.jpg',
//                 // (bool | optional) if you want it to fade out on its own or just sit there
//                 sticky: true,
//                 // (int | optional) the time you want it to be alive for before fading out
//                 time: '',
//                 // (string | optional) the class name you want to apply to that specific message
//                 class_name: 'my-sticky-class'
//             });
//             return false;
//         });

//         $('#gritter-regular').click(function () {

//             $.gritter.add({
//                 // (string | mandatory) the heading of the notification
//                 title: 'This is a regular notice!',
//                 // (string | mandatory) the text inside the notification
//                 text: 'This will fade out after a certain amount of time. Vivamus eget tincidunt velit. Cum sociis natoque penatibus et <a href="#">some link sample</a> montes, nascetur ridiculus mus.',
//                 // (string | optional) the image to display on the left
//                 image: 'img/avatar1.jpg',
//                 // (bool | optional) if you want it to fade out on its own or just sit there
//                 sticky: false,
//                 // (int | optional) the time you want it to be alive for before fading out
//                 time: ''
//             });

//             return false;

//         });

//         $('#gritter-max').click(function () {

//             $.gritter.add({
//                 // (string | mandatory) the heading of the notification
//                 title: 'This is a notice with a max of 3 on screen at one time!',
//                 // (string | mandatory) the text inside the notification
//                 text: 'This will fade out after a certain amount of time. Vivamus eget tincidunt velit. Cum sociis natoque penatibus et <a href="#">some link sample</a> montes, nascetur ridiculus mus.',
//                 // (string | optional) the image to display on the left
//                 image: 'img/avatar1.jpg',
//                 // (bool | optional) if you want it to fade out on its own or just sit there
//                 sticky: false,
//                 // (function) before the gritter notice is opened
//                 before_open: function () {
//                     if ($('.gritter-item-wrapper').length == 3) {
//                         // Returning false prevents a new gritter from opening
//                         return false;
//                     }
//                 }
//             });
//             return false;
//         });

//         $('#gritter-without-image').click(function () {
//             $.gritter.add({
//                 // (string | mandatory) the heading of the notification
//                 title: 'This is a notice without an image!',
//                 // (string | mandatory) the text inside the notification
//                 text: 'This will fade out after a certain amount of time. Vivamus eget tincidunt velit. Cum sociis natoque penatibus et <a href="#">some link sample</a> montes, nascetur ridiculus mus.'
//             });

//             return false;
//         });

//         $('#gritter-light').click(function () {

//             $.gritter.add({
//                 // (string | mandatory) the heading of the notification
//                 title: 'This is a light notification',
//                 // (string | mandatory) the text inside the notification
//                 text: 'Just add a "gritter-light" class_name to your $.gritter.add or globally to $.gritter.options.class_name',
//                 class_name: 'gritter-light'
//             });

//             return false;
//         });

//         $("#gritter-remove-all").click(function () {

//             $.gritter.removeAll();
//             return false;

//         });
//     }

//     var handleDynamicPagination = function() {
//         $('#dynamic_pager_demo1').bootpag({
//             total: 6,
//             page: 1,
//         }).on("page", function(event, num){
//             $("#dynamic_pager_content1").html("Page " + num + " content here"); // or some ajax content loading...
//         });

//         $('#dynamic_pager_demo2').bootpag({
//             total: 24,
//             page: 1,
//             maxVisible: 6 
//         }).on('page', function(event, num){
//             $("#dynamic_pager_content2").html("Page " + num + " content here"); // or some ajax content loading...
//         });
//     }

//     return {
//         //main function to initiate the module
//         init: function () {
//             handlePulsate();
//             handleGritterNotifications();
//             handleDynamicPagination();
//         }

//     };

// }();


//ul-jquery.js
// var UIJQueryUI = function () {

    
//     var handleDatePickers = function () {
        
//         $("#ui_date_picker").datepicker();

//         $("#ui_date_picker_with_button_bar").datepicker({
//           showButtonPanel: true
//         });

//         $("#ui_date_picker_inline").datepicker();

//         $("#ui_date_picker_change_year_month" ).datepicker({
// 	      changeMonth: true,
// 	      changeYear: true
// 	    });

// 	    $("#ui_date_picker_multiple").datepicker({
// 	    	numberOfMonths: 2,
//       		showButtonPanel: true
// 	    });

// 	    $( "#ui_date_picker_range_from" ).datepicker({
// 	      defaultDate: "+1w",
// 	      changeMonth: true,
// 	      numberOfMonths: 2,
// 	      onClose: function( selectedDate ) {
// 	        $( "#ui_date_picker_range_to" ).datepicker( "option", "minDate", selectedDate );
// 	      }
// 	    });
// 	    $( "#ui_date_picker_range_to" ).datepicker({
// 	      defaultDate: "+1w",
// 	      changeMonth: true,
// 	      numberOfMonths: 2,
// 	      onClose: function( selectedDate ) {
// 	        $( "#ui_date_picker_range_from" ).datepicker( "option", "maxDate", selectedDate );
// 	      }
// 	    });

// 	    $("#ui_date_picker_week_year" ).datepicker({
// 	      showWeek: true,
// 	      firstDay: 1
// 	    });

// 	    $("#ui_date_picker_trigger input").datepicker();
// 	    $("#ui_date_picker_trigger .add-on").click(function(){
// 	    	$("#ui_date_picker_trigger input").datepicker("show");
// 	    });
//     }

//     var handleDialogs = function () {

//     	// basic dialog1
//     	$( "#dialog_basic1" ).dialog({
// 		      autoOpen: false,
// 		      dialogClass: 'ui-dialog-yellow',
// 		      show: {
// 		        effect: "blind",
// 		        duration: 500
// 		      },
// 		      hide: {
// 		        effect: "explode",
// 		        duration: 500
// 		      }
// 	    });
	 
// 	    $( "#basic_opener1").click(function() {
// 	      $( "#dialog_basic1" ).dialog( "open" );	
// 	      $('.ui-dialog button').blur();// avoid button autofocus     
// 	    });

// 	    // basic dialog2
//     	$( "#dialog_basic2" ).dialog({
// 		      autoOpen: false,
// 		      dialogClass: 'ui-dialog-purple',
// 		      show: {
// 		        effect: "blind",
// 		        duration: 500
// 		      },
// 		      hide: {
// 		        effect: "explode",
// 		        duration: 500
// 		      }
// 	    });
	 
// 	    $( "#basic_opener2").click(function() {
// 	      $( "#dialog_basic2" ).dialog( "open" );	
// 	      $('.ui-dialog button').blur();// avoid button autofocus     
// 	    });

// 	    // basic dialog3
//     	$( "#dialog_basic3" ).dialog({
// 		      autoOpen: false,
// 		      dialogClass: 'ui-dialog-grey',
// 		      show: {
// 		        effect: "blind",
// 		        duration: 500
// 		      },
// 		      hide: {
// 		        effect: "explode",
// 		        duration: 500
// 		      }
// 	    });
	 
// 	    $( "#basic_opener3").click(function() {
// 	      $( "#dialog_basic3" ).dialog( "open" );	
// 	      $('.ui-dialog button').blur();// avoid button autofocus     
// 	    });

// 	    // basic dialog4
//     	$( "#dialog_basic4" ).dialog({
// 		      autoOpen: false,
// 		      dialogClass: 'ui-dialog-red',
// 		      show: {
// 		        effect: "blind",
// 		        duration: 500
// 		      },
// 		      hide: {
// 		        effect: "explode",
// 		        duration: 500
// 		      }
// 	    });
	 
// 	    $( "#basic_opener4").click(function() {
// 	      $( "#dialog_basic4" ).dialog( "open" );	
// 	      $('.ui-dialog button').blur();// avoid button autofocus     
// 	    });

// 	    // info dialog
// 	    $("#dialog_info").dialog({
// 	      dialogClass: 'ui-dialog-blue',
// 	      autoOpen: false,
// 	      resizable: false,
// 	      height: 250,
// 	      modal: true,
// 	      buttons: [
// 	      	{
// 	      		"text" : "OK",
// 	      		'class' : 'btn green',
// 	      		click: function() {
//         			$(this).dialog( "close" );
//       			}
// 	      	}
// 	      ]
// 	    });

// 	    $( "#info_opener").click(function() {
// 	      $( "#dialog_info" ).dialog( "open" );
// 	       $('.ui-dialog button').blur();// avoid button autofocus
// 	    });

// 	    //confirm dialog
// 	    $("#dialog_confirm" ).dialog({
// 	      dialogClass: 'ui-dialog-green',
// 	      autoOpen: false,
// 	      resizable: false,
// 	      height: 210,
// 	      modal: true,
// 	      buttons: [
// 	      	{
// 	      		'class' : 'btn red',	
// 	      		"text" : "Delete",
// 	      		click: function() {
//         			$(this).dialog( "close" );
//       			}
// 	      	},
// 	      	{
// 	      		'class' : 'btn',
// 	      		"text" : "Cancel",
// 	      		click: function() {
//         			$(this).dialog( "close" );
//       			}
// 	      	}
// 	      ]
// 	    });

// 	    $( "#confirm_opener").click(function() {
// 	      $( "#dialog_confirm" ).dialog( "open" );
// 	       $('.ui-dialog button').blur();// avoid button autofocus
// 	    });

//     }

//     return {
//         //main function to initiate the module
//         init: function () {
//             handleDatePickers();
//             handleDialogs();
//         }

//     };

// }();


// //ul-modals.js
// var UIModals = function () {

    
//     var initModals = function () {
       
//        	$.fn.modalmanager.defaults.resize = true;
// 		$.fn.modalmanager.defaults.spinner = '<div class="loading-spinner fade" style="width: 200px; margin-left: -100px;"><img src="img/ajax-modal-loading.gif" align="middle">&nbsp;<span style="font-weight:300; color: #eee; font-size: 18px; font-family:Open Sans;">&nbsp;Loading...</div>';


//        	var $modal = $('#ajax-modal');
 
// 		$('#modal_ajax_demo_btn').on('click', function(){
// 		  // create the backdrop and wait for next modal to be triggered
// 		  $('body').modalmanager('loading');
		 
// 		  setTimeout(function(){
// 		     $modal.load('ui_modals_ajax_sample.html', '', function(){
// 		      $modal.modal();
// 		    });
// 		  }, 1000);
// 		});
		 
// 		$modal.on('click', '.update', function(){
// 		  $modal.modal('loading');
// 		  setTimeout(function(){
// 		    $modal
// 		      .modal('loading')
// 		      .find('.modal-body')
// 		        .prepend('<div class="alert alert-info fade in">' +
// 		          'Updated!<button type="button" class="close" data-dismiss="alert"></button>' +
// 		        '</div>');
// 		  }, 1000);
// 		}); 
       
//     }

//     return {
//         //main function to initiate the module
//         init: function () {
//             initModals();
//         }

//     };

// }();


// //ul-nestable.js
// var UINestable = function () {

//     var updateOutput = function (e) {
//         var list = e.length ? e : $(e.target),
//             output = list.data('output');
//         if (window.JSON) {
//             output.val(window.JSON.stringify(list.nestable('serialize'))); //, null, 2));
//         } else {
//             output.val('JSON browser support required for this demo.');
//         }
//     };


//     return {
//         //main function to initiate the module
//         init: function () {

//             // activate Nestable for list 1
//             $('#nestable_list_1').nestable({
//                 group: 1
//             })
//                 .on('change', updateOutput);

//             // activate Nestable for list 2
//             $('#nestable_list_2').nestable({
//                 group: 1
//             })
//                 .on('change', updateOutput);

//             // output initial serialised data
//             updateOutput($('#nestable_list_1').data('output', $('#nestable_list_1_output')));
//             updateOutput($('#nestable_list_2').data('output', $('#nestable_list_2_output')));

//             $('#nestable_list_menu').on('click', function (e) {
//                 var target = $(e.target),
//                     action = target.data('action');
//                 if (action === 'expand-all') {
//                     $('.dd').nestable('expandAll');
//                 }
//                 if (action === 'collapse-all') {
//                     $('.dd').nestable('collapseAll');
//                 }
//             });

//             $('#nestable_list_3').nestable();

//         }

//     };

// }();


// //ul-slider.js
// var UISliders = function () {

//     return {
//         //main function to initiate the module
//         initSliders: function () {
//             // basic
//             $(".slider-basic").slider(); // basic sliders

//             // snap inc
//             $("#slider-snap-inc").slider({
//                 value: 100,
//                 min: 0,
//                 max: 1000,
//                 step: 100,
//                 slide: function (event, ui) {
//                     $("#slider-snap-inc-amount").text("$" + ui.value);
//                 }
//             });

//             $("#slider-snap-inc-amount").text("$" + $("#slider-snap-inc").slider("value"));

//             // range slider
//             $("#slider-range").slider({
//                 range: true,
//                 min: 0,
//                 max: 500,
//                 values: [75, 300],
//                 slide: function (event, ui) {
//                     $("#slider-range-amount").text("$" + ui.values[0] + " - $" + ui.values[1]);
//                 }
//             });

//             $("#slider-range-amount").text("$" + $("#slider-range").slider("values", 0) + " - $" + $("#slider-range").slider("values", 1));

//             //range max

//             $("#slider-range-max").slider({
//                 range: "max",
//                 min: 1,
//                 max: 4,
//                 value: 1,
//                 slide: function (event, ui) {
//                     $("#slider-range-max-amount").text(ui.value);
//                 }
//             });

//             $("#slider-range-max-amount").text($("#slider-range-max").slider("value"));

//             // range min
//             $("#slider-range-min").slider({
//                 range: "min",
//                 value: 37,
//                 min: 1,
//                 max: 700,
//                 slide: function (event, ui) {
//                     $("#slider-range-min-amount").text("$" + ui.value);
//                 }
//             });

//             $("#slider-range-min-amount").text("$" + $("#slider-range-min").slider("value"));

//             // 
//             // setup graphic EQ
//             $("#slider-eq > span").each(function () {
//                 // read initial values from markup and remove that
//                 var value = parseInt($(this).text(), 10);
//                 $(this).empty().slider({
//                     value: value,
//                     range: "min",
//                     animate: true,
//                     orientation: "vertical"
//                 });
//             });

//             // vertical slider
//             $("#slider-vertical").slider({
//                 orientation: "vertical",
//                 range: "min",
//                 min: 0,
//                 max: 100,
//                 value: 60,
//                 slide: function (event, ui) {
//                     $("#slider-vertical-amount").text(ui.value);
//                 }
//             });
//             $("#slider-vertical-amount").text($("#slider-vertical").slider("value"));

//             // vertical range sliders
//             $("#slider-range-vertical").slider({
//                 orientation: "vertical",
//                 range: true,
//                 values: [17, 67],
//                 slide: function (event, ui) {
//                     $("#slider-range-vertical-amount").text("$" + ui.values[0] + " - $" + ui.values[1]);
//                 }
//             });

//             $("#slider-range-vertical-amount").text("$" + $("#slider-range-vertical").slider("values", 0) + " - $" + $("#slider-range-vertical").slider("values", 1));

//         },

//         initKnowElements: function () {
//             //knob does not support ie8 so skip it
//             if (!jQuery().knob || App.isIE8()) {
//                 return;
//             }

//             $(".knob").knob({
//                 'dynamicDraw': true,
//                 'thickness': 0.2,
//                 'tickColorizeValues': true,
//                 'skin': 'tron'
//             });

//             if ($(".knobify").size() > 0) {
//                 $(".knobify").knob({
//                     readOnly: true,
//                     skin: "tron",
//                     'width': 100,
//                     'height': 100,
//                     'dynamicDraw': true,
//                     'thickness': 0.2,
//                     'tickColorizeValues': true,
//                     'skin': 'tron',
//                     draw: function () {
//                         // "tron" case
//                         if (this.$.data('skin') == 'tron') {

//                             var a = this.angle(this.cv) // Angle
//                                 ,
//                                 sa = this.startAngle // Previous start angle
//                                 ,
//                                 sat = this.startAngle // Start angle
//                                 ,
//                                 ea // Previous end angle
//                                 ,
//                                 eat = sat + a // End angle
//                                 ,
//                                 r = 1;

//                             this.g.lineWidth = this.lineWidth;

//                             this.o.cursor && (sat = eat - 0.3) && (eat = eat + 0.3);

//                             if (this.o.displayPrevious) {
//                                 ea = this.startAngle + this.angle(this.v);
//                                 this.o.cursor && (sa = ea - 0.3) && (ea = ea + 0.3);
//                                 this.g.beginPath();
//                                 this.g.strokeStyle = this.pColor;
//                                 this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, sa, ea, false);
//                                 this.g.stroke();
//                             }

//                             this.g.beginPath();
//                             this.g.strokeStyle = r ? this.o.fgColor : this.fgColor;
//                             this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, sat, eat, false);
//                             this.g.stroke();

//                             this.g.lineWidth = 2;
//                             this.g.beginPath();
//                             this.g.strokeStyle = this.o.fgColor;
//                             this.g.arc(this.xy, this.xy, this.radius - this.lineWidth + 1 + this.lineWidth * 2 / 3, 0, 2 * Math.PI, false);
//                             this.g.stroke();

//                             return false;

//                         }
//                     }
//                 });
//             }
//         }

//     };

// }();


// //ul-tree.js
// var UITree = function () {

//     return {
//         //main function to initiate the module
//         init: function () {
//             // handle collapse/expand for tree_1
//             $('#tree_1_collapse').click(function () {
//                 $('.tree-toggle', $('#tree_1 > li > ul')).addClass("closed");
//                 $('.branch', $('#tree_1 > li > ul')).removeClass("in");
//             });

//             $('#tree_1_expand').click(function () {
//                 $('.tree-toggle', $('#tree_1 > li > ul')).removeClass("closed");
//                 $('.branch', $('#tree_1 > li > ul')).addClass("in");
//             });

//             // handle collapse/expand for tree_2
//             $('#tree_2_collapse').click(function () {
//                 $('.tree-toggle', $('#tree_2 > li > ul')).addClass("closed");
//                 $('.branch', $('#tree_2 > li > ul')).removeClass("in");
//             });

//             $('#tree_2_expand').click(function () {
//                 //$('.tree-toggle', $('#tree_2 > li > ul')).removeClass("closed");
//                 // iterate tree nodes and exppand all nodes
//                 $('.tree-toggle', $('#tree_2 > li > ul')).each(function () {
//                     $(this).click(); //trigger tree node click
//                 });
//                 $('.branch', $('#tree_2 > li > ul')).addClass("in");
//             });

//             //This is a quick example of capturing the select event on tree leaves, not branches
//             $("#tree_1").on("nodeselect.tree.data-api", "[data-role=leaf]", function (e) {
//                 var output = "";

//                 output += "Node nodeselect event fired:\n";
//                 output += "Node Type: leaf\n";
//                 output += "Value: " + ((e.node.value) ? e.node.value : e.node.el.text()) + "\n";
//                 output += "Parentage: " + e.node.parentage.join("/");

//                 alert(output);
//             });

//             //This is a quick example of capturing the select event on tree branches, not leaves
//             $("#tree_1").on("nodeselect.tree.data-api", "[role=branch]", function (e) {
//                 var output = "Node nodeselect event fired:\n"; + "Node Type: branch\n" + "Value: " + ((e.node.value) ? e.node.value : e.node.el.text()) + "\n" + "Parentage: " + e.node.parentage.join("/") + "\n"

//                 alert(output);
//             });

//             //Listening for the 'openbranch' event. Look for e.node, which is the actual node the user opens

//             $("#tree_1").on("openbranch.tree", "[data-toggle=branch]", function (e) {

//                 var output = "Node openbranch event fired:\n" + "Node Type: branch\n" + "Value: " + ((e.node.value) ? e.node.value : e.node.el.text()) + "\n" + "Parentage: " + e.node.parentage.join("/") + "\n"

//                 alert(output);
//             });


//             //Listening for the 'closebranch' event. Look for e.node, which is the actual node the user closed

//             $("#tree_1").on("closebranch.tree", "[data-toggle=branch]", function (e) {

//                 var output = "Node closebranch event fired:\n" + "Node Type: branch\n" + "Value: " + ((e.node.value) ? e.node.value : e.node.el.text()) + "\n" + "Parentage: " + e.node.parentage.join("/") + "\n"

//                 alert(output);
//             });
//         }

//     };

// }();

//table editable.js
var TableEditable = function () {

    return {

        //main function to initiate the module
        init: function () {


            //CUSTOM SCRIPT TO DETECT PAGE -- NEEDED FOR TABLE COLUMN SORTING

            var page = window.location.pathname.substring(1);

            if(page == "employees" || page == "team_members" || page == "associates") {

                var non_sortable = [0,6];
            
            } else if(page == "clients") {

                var non_sortable = [5];
            
            } else if(page == "inventory") {

                var non_sortable = [5];
            
            } else if(page == "invoices") {

                var non_sortable = [7];
            
            } else if(page == "recurring_invoices") {

                var non_sortable = [6];
            
            } else if(page == "client_profile") {

                var non_sortable = [6];
            
            } else if(page == "product") {

                var non_sortable = [7];
            
            } else if(page == "messages") {

                var non_sortable = [0,1];
            
            } else if(page == "account_settings") {

                var non_sortable = [4,5,6,7,8];
            
            } else {

                var non_sortable = [0];
            }

            //



            function restoreRow(oTable, nRow) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);

                for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
                    oTable.fnUpdate(aData[i], nRow, i, false);
                }

                oTable.fnDraw();
            }

            function editRow(oTable, nRow) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);
                jqTds[0].innerHTML = '<input type="text"  class="m-wrap small" value="' + aData[0] + '">';
                jqTds[1].innerHTML = '<input type="text" class="m-wrap small" value="' + aData[1] + '">';
                jqTds[2].innerHTML = '<input type="text" class="m-wrap small" value="' + aData[2] + '">';
                jqTds[3].innerHTML = '<input type="text" class="m-wrap small" value="' + aData[3] + '">';
                jqTds[4].innerHTML = '<a class="edit" href="">Save</a>';
                jqTds[5].innerHTML = '<a class="cancel" href="">Cancel</a>';
            }

            function editNewRow(oTable, nRow) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);
                jqTds[0].innerHTML = '<input type="text"  class="m-wrap small" value="' + aData[0] + '">';
                jqTds[1].innerHTML = '<input type="text" class="m-wrap small" value="' + aData[1] + '">';
                jqTds[2].innerHTML = '<input type="text" class="m-wrap small" value="' + aData[2] + '">';
                jqTds[3].innerHTML = '<input type="text" class="m-wrap small" value="' + aData[3] + '">';
                jqTds[4].innerHTML = '<a class="edit" href="">Save</a>';
                jqTds[5].innerHTML = '<a class="cancel" data-mode="new" href="">Cancel</a>';
            }

            function saveRow(oTable, nRow) {
                var jqInputs = $('input', nRow);
                oTable.fnUpdate(jqInputs[0].value, nRow, 0, false);
                oTable.fnUpdate(jqInputs[1].value, nRow, 1, false);
                oTable.fnUpdate(jqInputs[2].value, nRow, 2, false);
                oTable.fnUpdate(jqInputs[3].value, nRow, 3, false);
                oTable.fnUpdate('<a class="edit" href="">Edit</a>', nRow, 4, false);
                oTable.fnUpdate('<a class="delete" href="">Delete</a>', nRow, 5, false);
                oTable.fnDraw();
            }

            function cancelEditRow(oTable, nRow) {
                var jqInputs = $('input', nRow);
                oTable.fnUpdate(jqInputs[0].value, nRow, 0, false);
                oTable.fnUpdate(jqInputs[1].value, nRow, 1, false);
                oTable.fnUpdate(jqInputs[2].value, nRow, 2, false);
                oTable.fnUpdate(jqInputs[3].value, nRow, 3, false);
                oTable.fnUpdate('<a class="edit" href="">Edit</a>', nRow, 4, false);
                oTable.fnDraw();
            }

            var oTable = $('#data_table').dataTable({
                "aLengthMenu": [
                    [5, 10, 20, -1],
                    [5, 10, 20, "All"] // change per page values here
                ],
                // set the initial value
                "iDisplayLength": 10,
                "sDom": "<'row-fluid'<'span6'l><'span6 search'f>r>t<'row-fluid'<'span6'i><'span6 hidden-print'p>>",
                "sPaginationType": "bootstrap",
                "oLanguage": {
                    "sLengthMenu": "_MENU_ Records per page",
                    "oPaginate": {
                        "sPrevious": "Prev",
                        "sNext": "Next"
                    }
                },
                "aoColumnDefs": [{
                        'bSortable': false,
                        'aTargets': non_sortable
                    }
                ]
            });

            jQuery('#data_table_wrapper .dataTables_filter input').addClass("m-wrap medium"); // modify table search input
            jQuery('#data_table_wrapper .dataTables_length select').addClass("m-wrap small"); // modify table per page dropdown
            jQuery('#data_table_wrapper .dataTables_length select').select2({
                minimumResultsForSearch: '10' //hide search box with special css class
            }); // initialzie select2 dropdown

            var nEditing = null;

            $('#add_new').click(function (e) {  
                e.preventDefault();
                var aiNew = oTable.fnAddData(['', '', '', '','',
                        '<a class="edit" href="">Edit</a>', '<a class="cancel" data-mode="new" href="">Cancel</a>'
                ]);
                var nRow = oTable.fnGetNodes(aiNew[0]);
                editNewRow(oTable, nRow);
                nEditing = nRow;
            });

            $('#data_table a.delete').live('click', function (e) {
                e.preventDefault();

                if (confirm("Are you sure to delete this row ?") == false) {
                    return;
                }

                var nRow = $(this).parents('tr')[0];
                oTable.fnDeleteRow(nRow);
                alert("Deleted! Do not forget to do some ajax to sync with backend :)");
            });

            $('#data_table a.cancel').live('click', function (e) {
                e.preventDefault();
                if ($(this).attr("data-mode") == "new") {
                    var nRow = $(this).parents('tr')[0];
                    oTable.fnDeleteRow(nRow);
                } else {
                    restoreRow(oTable, nEditing);
                    nEditing = null;
                }
            });

            $('#data_table a.edit').live('click', function (e) {
                e.preventDefault();

                /* Get the row as a parent of the link that was clicked on */
                var nRow = $(this).parents('tr')[0];

                if (nEditing !== null && nEditing != nRow) {
                    /* Currently editing - but not this row - restore the old before continuing to edit mode */
                    restoreRow(oTable, nEditing);
                    editRow(oTable, nRow);
                    nEditing = nRow;
                } else if (nEditing == nRow && this.innerHTML == "Save") {
                    /* Editing this row and want to save it */
                    saveRow(oTable, nEditing);
                    nEditing = null;
                    alert("Updated! Do not forget to do some ajax to sync with backend :)");
                } else {
                    /* No edit in progress - let's start one */
                    editRow(oTable, nRow);
                    nEditing = nRow;
                }
            });
        }

    };

}();


