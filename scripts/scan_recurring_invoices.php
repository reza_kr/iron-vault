<?php
		
	// require("/cron/inc/connect_dbo.php");

	// include_once '/cron/vendor/autoload.php';

	require("/var/www/html/app/inc/connect_dbo.php");

	include_once '/var/www/html/app/vendor/autoload.php';


	date_default_timezone_set("America/Toronto");

	$today = date('Y-m-d');
	$tomorrow = date('Y-m-d', strtotime($today . " +1 day"));

	//$today = date('Y-m-d', strtotime($today . " -1 day"));
	//$tomorrow = date('Y-m-d', strtotime($today . " +1 day"));

	echo '<pre>';
	echo $today . '<br>';

	$counter = 1;

	//Return all the invoices that have a PASSED start date and have NOT yet ended.
	//The WHERE clause in this select statement is simply meant to narrow down the number of rows returned to make the process faster. The logic that determines if the invoice should be generated in done with the \Recurr library.
	//
	$query = $dbo->prepare("SELECT id, account_id, client_id, notes, payment_type, payment_term, currency, discount_amount, discount_type, discount_approved_by, calculate_tax, email_invoice, recurring_interval, recurring_freq, recurring_start_date, recurring_end_date, recurring_date_range, recurring_end_count, recurring_count, last_generated FROM invoice_templates WHERE status = ? AND recurring_start_date <= ? AND (recurring_end_date >= ? OR recurring_end_date = ?)");
	$query->execute(array('active', $today, $today, '0000-00-00'));

	$result = $query->fetchAll();

	foreach($result as $row) {

		echo '<br>' . $counter . ') ';

		$id = $row[0];
		$account_id = $row[1];
		$client_id = $row[2];
		$notes = $row[3];
		$payment_type = $row[4];
		$payment_term = $row[5];
		$currency = $row[6];
		$discount_amount = $row[7];
		$discount_type = $row[8];
		$discount_approved_by = $row[9];
		$calculate_tax = $row[10];
		$email_invoice = $row[11];

		$interval = $row[12]; // I used interval, they used frequency.
		$freq = $row[13]; // I used frequency, they used interval.
		$start_date = $row[14];
		$end_date = $row[15];
		$end = $row[16];
		$occur_count = $row[17]; //total # of times the invoice should get generated
		$current_count = $row[18]; //# of times the invoice has been generated so far
		$last_generated = $row[19];


		$dates_array = array();


		if($end == 'none' || $end == 'by') {

			$constraint = new \Recurr\Transformer\Constraint\BeforeConstraint(new \DateTime($tomorrow));
		
		} else if($end == 'after') {

			$count = ';COUNT=' . $occur_count;
		}


		if($interval == 'daily') {

			$startDate   = new \DateTime($start_date);
			$rule        = new \Recurr\Rule('FREQ=DAILY;INTERVAL=' . $freq . $count, $startDate);
			$transformer = new \Recurr\Transformer\ArrayTransformer();
		
		} else if($interval == 'weekly') {

			$startDate   = new \DateTime($start_date);
			$rule        = new \Recurr\Rule('FREQ=WEEKLY;INTERVAL=' . $freq . $count, $startDate);
			$transformer = new \Recurr\Transformer\ArrayTransformer();
		
		} else if($interval == 'monthly') {

			$startDate   = new \DateTime($start_date);
			$rule        = new \Recurr\Rule('FREQ=MONTHLY;INTERVAL=' . $freq . $count, $startDate);
			$transformer = new \Recurr\Transformer\ArrayTransformer();
		
		} else if($interval == 'yearly') {

			$startDate   = new \DateTime($start_date);
			$rule        = new \Recurr\Rule('FREQ=YEARLY;INTERVAL=' . $freq . $count, $startDate);
			$transformer = new \Recurr\Transformer\ArrayTransformer();
		}



		$transformed = $transformer->transform($rule, NULL, $constraint);

		foreach ($transformed as $dates) {

			$date = date_format($dates->getStart(), 'Y-m-d');

			array_push($dates_array, $date);
		}

		print_r($dates_array);
		

		if(in_array($today, $dates_array)) {

			echo 'ID:' . $id . ' - Invoice day!!! - ' . $today . '<br>';
			
			//If today is invoice day, get all the invoice items for the invoice, etc.
			$items = array();

			$query2 = $dbo->prepare("SELECT item_id, item_qty FROM invoice_template_items WHERE account_id = ? AND invoice_id =?");
			$query2->execute(array($account_id, $id));

			$result2 = $query2->fetchAll();

			foreach($result2 as $row2) {

				$item_id = $row2[0];
				$item_qty = $row2[1];

				$item = array();
				$item['id'] = $item_id;
				$item['qty'] = $item_qty;

				array_push($items, $item);

				// $query3 = $dbo->prepare("SELECT item_name, item_desc, item_cost FROM inventory WHERE account_id = ? AND item_id =?");
				// $query3->execute(array($account_id, $item_id));

				// $result3 = $query3->fetchAll();

				// foreach($result3 as $row3) {

				// 	$item_name = $row3[0];
				// 	$item_desc = $row3[1];
				// 	$item_cost = $row3[2];

				// 	$items .= 'Item ID: ' . $item_id . ', QTY: ' . $item_qty . ', Name: ' . $item_name . ', Desc: ' . $item_desc . ', Cost: $' . $item_cost . '<br>';
				// }
			}

			//print_r($items);

			//Add new invoice to database

			$invoice_date = $today;

			if($payment_term == 0) {

        		$due_date = $invoice_date;
			
			} else {

				$due_date = date('Y-m-d', strtotime($invoice_date . ' +' . $payment_term . ' days'));
			}

			//Current date and time
        	$datetime = date('Y-m-d H:i:s');


        	//Get last invoice number, +1, then create the invoice
        	$query2 = $dbo->prepare("SELECT MAX(invoice_number) AS invoice_number FROM invoices WHERE account_id = ?");
			$query2->execute(array($account_id));

			$result2 = $query2->fetchAll();

			foreach($result2 as $row2) {

    			$max_invoice_number = $row2[0];
 
    		}

    		//Remember... Invoice Number and invoice_id are different!! 
    		$invoice_number = $max_invoice_number + 1;

    		
    		$access_token = hash('SHA1', time());


			$query2 = $dbo->prepare("INSERT INTO invoices SET account_id = ?, client_id = ?, invoice_number = ?, date_created = ?, date_modified = ?, created_by = ?, modified_by = ?, notes = ?,  status = ?, invoice_date = ?, due_date = ?, payment_term = ?, payment_type = ?, currency = ?, discount_amount = ?, discount_type = ?, discount_approved_by = ?, calculate_tax = ?, from_template_id = ?, access_token = ?");
			$query2->execute(array($account_id, $client_id, $invoice_number, $datetime, $datetime, '-1', '-1', $notes, 'draft', $invoice_date, $due_date, $payment_term, $payment_type, $currency, $discount_amount, $discount_type, $discount_approved_by, $calculate_tax, $id, $access_token));

			$invoice_id = $dbo->lastInsertId();
			

        	if($calculate_tax == 1) {

	        	$query2 = $dbo->prepare("SELECT tax_rate FROM account_preferences WHERE account_id = ?");
				$query2->execute(array($account_id));

				$result2 = $query2->fetchAll();

				foreach($result2 as $row2) {

					$tax_rate = $row2[0];

				}
			
			} else {

				$tax_rate = 0;
			}

			$subtotal = 0;
			$total = 0;
        	$after_discount = 0;
        	$tax_amount = 0;


        	foreach ($items as $key => $item) { 

        		$item_id = $item['id'];
        		$item_qty = $item['qty'];

        		//Get the latest price of item
        		$query2 = $dbo->prepare("SELECT item_cost FROM inventory WHERE account_id = ? AND item_id = ?");
				$query2->execute(array($account_id, $item_id));

				$result2 = $query2->fetchAll();

				foreach($result2 as $row2) { 

					$item_cost = $row2[0];
				}

				$item_total = $item_qty * $item_cost;

				$subtotal = $subtotal + $item_total;


				//$query2 = $dbo->prepare("INSERT INTO invoice_items SET account_id = ?, invoice_id = ?, item_id = ?, item_qty = ?, item_cost = ?");
				//$query2->execute(array($account_id, $invoice_id, $item_id, $item_qty, $item_cost));

        	}


        	if($discount_type == 'amount') {

        		$after_discount = $subtotal - $discount_amount;
        	
        	} else if($discount_type == 'percent') {

        		$after_discount = $subtotal * ((100 - $discount_amount) / 100);

        	} else {

        		//no discount
        		$after_discount = $subtotal;
        	}


        	if($calculate_tax == 1) {

	        	if($after_discount > 0) {

	        		$tax_amount = ($after_discount * ($tax_rate / 100));

	        	} else {

	        		$tax_amount = 0;
	        	}

	        }

        	$total = $after_discount + $tax_amount;

        	// =======================

        	$query = $dbo->prepare("UPDATE invoices SET status = ?, date_processed = ?, total = ?, tax_rate = ? WHERE account_id = ? AND id = ? AND status = ?");
			$query->execute(array('processed', $datetime, $total, $tax_rate, $account_id, $invoice_id, 'draft'));


			$current_count++;
        	$last_generated = $today;

        	$query = $dbo->prepare("UPDATE invoice_templates SET recurring_count = ?, last_generated = ? WHERE id = ?");
			$query->execute(array($current_count, $last_generated, $id));

			// =======================


			//Send email to client
			if($email_invoice == 1) {

				//require_once('/cron/classes/class.phpmailer.php');
				require_once('/var/www/html/app/classes/class.phpmailer.php');

				$client_display_name = 'Reza Karami';
				$client_email = 'reza_karami@outlook.com';
				$business_name = 'Sunset Boulevard';
				$invoice_date = '01/16/2015';
				$invoice_link = 'https://sunsetblvd.ironvault.ca/view_invoice?id=' . $invoice_id .'&t=' . $access_token;

				$query2 = $dbo->prepare("SELECT logo FROM accounts WHERE account_id = ?");
				$query2->execute(array($account_id));

				$result2 = $query2->fetchAll();

				foreach($result2 as $row2) { 

					$logo = $row2[0];
				}

				if($logo != '') {

					$business_logo = '<img src="https://cdn.ironvault.ca/logo/' . $logo . '" border="0" style="max-height: 40px; max-width: 300px;vertical-align:middle;display:block;">';
				
				} else {

					$business_logo = '<span style="font-size:28px;line-height:36px;color:#434343;">' . $business_name . '</span>';

				}

				$emailHeader = '<table width="690" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff" align="center" style="max-width:690px;padding-top:0px;font-family:Arial, Helvetica, sans-serif;font-weight:normal;font-size:13px;line-height:21px;color:#434343;border-collapse:collapse;background-color:#ffffff;border:1px solid #dddddd;">
							        <tbody><tr>
							            <td colspan="3" width="100%">
							            <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#f9f9f9" style="padding-top:0px;border-collapse:collapse;font-family:Arial, Helvetica, sans-serif;font-weight:normal;font-size:13px;line-height:21px;color:#434343;background-color:#E6E6E6;border-bottom:1px solid #dddddd;">
							              <tbody><tr>
							                <td width="3%"></td>
							                <td width="94%" height="64">' . $business_logo . '</td>
							                <td width="3%"></td>
							              </tr>
							            </tbody></table></td>
							        </tr>
							        <tr> 
							          <td colspan="3" width="100%" height="30"></td>
							        </tr>';

				$emailFooter = '<tr>
							          <td colspan="3" width="100%" height="25"></td>
							        </tr>
							 
							      
							        <tr>
							          <td colspan="3" width="100%" height="25" style="border-collapse:collapse;border-bottom:1px solid #f1f1f1;"></td>
							        </tr>
							        <tr bgcolor="#E6E6E6" style="background-color:#E6E6E6;">

							            	<td width="3%" style="border-top:1px solid #dddddd;"></td>
							              <td width="94%" style="padding:20px 0 5px 0; border-top:1px solid #dddddd;"><p style="padding:0;font-family:Arial, Helvetica, sans-serif;font-weight:normal;font-size:10px;line-height:12px;color:#989898;">Powered by Iron Vault – www.ironvault.ca 
							              </td>
							              <td width="3%" style="border-top:1px solid #dddddd;"></td>
							        </tr>


							      </tbody>

							</table>';

				$mail = new PHPMailer;
				$mail->CharSet = 'UTF-8';
				$mail->isHTML(true);  
				$mail->WordWrap = 50;
				$mail->From = 'noreply@ironvault.ca';
				$mail->FromName = $business_name; 



				$mail->addAddress($client_email);

				$mail->Subject = $client_display_name . ' – Invoice (' . $invoice_date . ')';

				$mail->Body   .= $emailHeader;	

				$mail->Body    .=  '<tr>
								          <td width="3%"></td>
								          <td width="94%">
								            <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff" style="font-family:Arial, Helvetica, sans-serif;font-weight:normal;font-size:13px;line-height:21px;color:#434343;border-collapse:collapse;">
								                <tbody>
								                    <tr>
								                      <td width="60%" valign="top">

								                          <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff" style="font-family:Arial, Helvetica, sans-serif;font-weight:normal;font-size:13px;line-height:21px;color:#434343;border-collapse:collapse;">
								                                <tbody>

								                                  <tr>
								                                      <td width="100%" style="font-size:28px;line-height:36px;color:#434343;">Your Invoice is Ready</td>
								                                  </tr>
								    
								                                </tbody>
								                          </table>

								                      </td>
								                    </tr>
								                    <tr>
								                    	   <td colspan="2" width="100%" height="10" style="border-collapse:collapse;border-bottom:1px solid #f1f1f1;"></td>
								                    </tr>

								                </tbody>
								            </table>

								          </td>  
								          <td width="3%"></td>
								        </tr>'; 

				$mail->Body   .= '<tr>
								            <td colspan="3" width="100%" height="25"></td>
								        </tr>

								        <tr>
								        	 <td width="3%"></td>

								          	  <td width="94%">
								                  	<p style="padding:0;font-family:Arial, Helvetica, sans-serif;font-weight:normal;font-size:13px;line-height:21px;color:#434343;">

								                  	<strong>Dear ' . $client_display_name . ',</strong><br><br>Your invoice is ready. Please follow the link below to view and/or print your invoice.<br><br> 

								                  	<a href="' . $invoice_link . '" style="background-color: #7dabf5 !important; color: #FFF !important; border: 1px solid #619BF8; border-bottom: 2px solid #3E82F0; outline: none !important; border-radius: 3px !important; padding: 7px 14px; font-family: \'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 14px; text-decoration: none;">View Invoice</a>

								                  	<br><br>Sincerely,<br><strong>' . $business_name . '</strong>


								                	</p>
								              </td>

								        	   <td width="3%"></td>
								        </tr>';

				$mail->Body   .= $emailFooter;


				$status = $mail->send();

				if ($status) { 

				   //return true;
				
				} else {

					//return false;
				}


			}

			exit;
		
		} else {

			echo '<br>';
		}


		print_r($dates_array);

		$counter++;
	}

?>