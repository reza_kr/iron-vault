<?php

   require_once("inc/session_start.php");
   require_once("functions.php");

   $html_array = load_page_html();

   require_once("inc/head.php");
?>
<!-- BEGIN BODY -->
<body class="fixed-top page-sidebar-fixed <?php echo $html_array['page_sidebar_closed']; ?>">
   <?php require_once("inc/top_menu.php"); ?>
   <!-- BEGIN CONTAINER -->   
   <div class="page-container row-fluid">

      <?php require_once("inc/main_menu.php"); ?>

      <!-- BEGIN PAGE -->
      <div class="page-content">
         <div class="container-fluid">

				<!-- BEGIN PAGE CONTENT-->
				<div class="row-fluid">
					<div class="span12">

						<!-- BEGIN EXAMPLE TABLE PORTLET-->
						<div class="portlet box blue">
							<div class="portlet-title">
								<div class="caption"><i class="icon-group"></i>Clients</div>
								<div class="tools">
		                           <i class="icon-th grid" title="Grid View"></i>
		                           <i class="icon-align-justify list selected" title="List View"></i>
		                        </div>
							</div>
							
							<div class="portlet-body no-more-tables clients_table_wrapper">

								<div class="clearfix">

									<div class="btn-group">
										<a id="add_new_client" class="btn light-green" data-toggle="modal" href="#add_new_client_modal"><i class="icon-user"></i>&nbsp;&nbsp;Add New Client</a>
									</div>    

									<?php echo $html_array['export_tools']; ?>
								</div>
								<table class="table table-bordered table-striped table-condensed cf clients saveaspdf" id="data_table">
									<thead class="cf">
										<tr>
											<th class="sorting">Name</th>
											<th class="sorting">Address</th>
											<th class="sorting">Email</th>
											<th class="sorting">Primary Phone</th>
											<th class="sorting">Last Invoice</th>
											<th id="edit_profile_button">Action</th>
										</tr> 
									</thead>
									<tbody>
										<?php echo $html_array['clients_table']; ?>
										
									</tbody>
								</table>


								<div class="gridview row-fluid">

									<div class="row-fluid">
				                     	<div id="clients_search_box" class="span6">
				                     		<input type="text" class="search_clients m-wrap" placeholder="Search..." />
				                     	</div>
				                    </div>

				                    <hr class="grey">

			                     	<div class="row-fluid clients_grid">

			                     		<?php echo $html_array['clients_grid']; ?>
									
										<div class="span3 m-span3 shuffle_sizer"></div>
			                     		<div class="clearfix"></div>
			                     	</div>
								</div>
							</div>
						</div>
						
						<div id="add_new_client_modal" class="modal hide fade" tabindex="-1" data-focus-on="input:first">
							<form class="zero_margin" action="clients?action=add_client" method="post">

								<div class="modal-header">
									<i class="icon-remove right" data-dismiss="modal" aria-hidden="true"></i>
									<h3><i class="icon-user"></i>&nbsp;&nbsp;Add New Client</h3>
								</div>

								<div class="modal-body">

									<div class="row-fluid">
	                                   <div class="span6 ">
	                                      <div class="control-group">
	                                         <label class="control-label">Type</label>
	                                         <div class="controls">
	                                         	<select name="client_type" id="client_type" class="m-wrap span12">
		                                         	<option value="individual">Individual</option>
		                                         	<option value="business">Business</option>
	                                         	</select>
	                                         </div>
	                                      </div>
	                                   </div>
	                                   <!--/span-->
	                                </div>

	                                <hr class="grey">

									<div class="row-fluid individual">
	                                   <div class="span6 ">
	                                      <div class="control-group">
	                                         <label class="control-label">First Name</label>
	                                         <div class="controls">
	                                            <input type="text" name="first_name" id="first_name" class="m-wrap span12" />
	                                         </div>
	                                      </div>
	                                   </div>
	                                   <!--/span-->
	                                   <div class="span6 ">
	                                      <div class="control-group">
	                                         <label class="control-label">Last Name</label>
	                                         <div class="controls">
	                                            <input type="text" name="last_name" id="last_name" class="m-wrap span12" />
	                                         </div>
	                                      </div>
	                                   </div>
	                                   <!--/span-->
	                                </div>

	                                <div class="row-fluid business">
	                                   <div class="span6 ">
	                                      <div class="control-group">
	                                         <label class="control-label">Business Name</label>
	                                         <div class="controls">
	                                            <input type="text" name="business_name" id="business_name" class="m-wrap span12" />
	                                         </div>
	                                      </div>
	                                   </div>
	                                   <!--/span-->
	                                   <div class="span6 ">
	                                      <div class="control-group">
	                                         <label class="control-label">Email</label>
	                                         <div class="controls">
	                                            <input type="text" name="business_email" id="business_email" class="m-wrap span12" />
	                                         </div>
	                                      </div>
	                                   </div>
	                                </div>

	                                <div class="row-fluid individual">
	                                   <div class="span6 ">
	                                      <div class="control-group">
	                                         <label class="control-label">Email</label>
	                                         <div class="controls">
	                                            <input type="text" name="individual_email" id="individual_email" class="m-wrap span12" />
	                                         </div>
	                                      </div>
	                                   </div>
	                                   <!--/span-->
	                                   <div class="span6 ">
	                                      <div class="control-group">
	                                         <label class="control-label" >Gender</label>
	                                         <div class="controls">
	                                            <select  class="m-wrap span12" name="gender" id="gender">
	                                               <option value="male">Male</option>
	                                               <option value="female">Female</option>
	                                            </select>
	                                         </div>
	                                      </div>
	                                   </div>
	                                   <!--/span-->
	                                </div>

	                                <div class="row-fluid">
	                                   <div class="span6 ">
	                                      <div class="control-group">
	                                         <label class="control-label">Primary Phone</label>
	                                         <div class="controls">
	                                            <input type="text" name="primary_phone" id="primary_phone" class="m-wrap span12" />
	                                         </div>
	                                      </div>
	                                   </div>
	                                   <!--/span-->
	                                   <div class="span6 ">
	                                      <div class="control-group">
	                                         <label class="control-label" >Alternative Phone</label>
	                                         <div class="controls">
	                                            <input type="text" name="alternative_phone" id="alternative_phone" class="m-wrap span12" />
	                                         </div>
	                                      </div>
	                                   </div>
	                                   <!--/span-->
	                                </div>


	                                <div class="row-fluid">
	                                   <div class="span12 ">
	                                      <div class="control-group">
	                                         <label class="control-label">Address</label>
	                                         <div class="controls">
	                                            <input type="text" name="address" id="add_client_geo_address" class="m-wrap span12" />
	                                         </div>
	                                      </div>
	                                   </div>
	                                </div>

									<div class="clearfix"></div>

								</div>

								<div class="modal-footer">
									<input type="submit" id="submit" class="btn light-green right" value="Add Client" />
									<input type="button" data-dismiss="modal" class="btn" value="Cancel" />
								</div>

							</form>

							<div class="clearfix"></div>
						</div>


						<div id="delete_client_modal" class="modal hide fade" tabindex="-1" data-focus-on="input:first">

							<form class="zero_margin" action="#" method="post">

								<div class="modal-header">
									<i class="icon-remove right" data-dismiss="modal" aria-hidden="true"></i>
									<h3><i class="icon-user"></i>&nbsp;&nbsp;Delete Client</h3>
								</div>

								<div class="modal-body">

								</div>	
									
								<div class="modal-footer">
									<input type="submit" id="submit" class="btn light-red right delete" value="Delete" />
									<input type="button" data-dismiss="modal" class="btn" value="Cancel" />
								</div>

							</form>
						</div>


						<!-- END EXAMPLE TABLE PORTLET-->
					</div>
				</div>
				<!-- END PAGE CONTENT -->

			</div>

		 
         <!-- END PAGE CONTAINER-->       
      </div>
      <!-- BEGIN PAGE -->
   </div>
   <!-- END PAGE CONTAINER--> 
   <!-- END CONTAINER -->
   <?php
      require_once("inc/footer.php");
   ?>
</body>
<!-- END BODY -->
</html>
<?php
   require_once("inc/session_end.php");
?>