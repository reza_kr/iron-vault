<?php
   require_once("inc/session_start.php");
   require_once("functions.php");

   $html_array = load_page_html();

   require_once("inc/head.php");
?>
<!-- BEGIN BODY -->
<body class="fixed-top page-sidebar-fixed <?php echo $html_array['page_sidebar_closed']; ?>">
	<?php require_once("inc/top_menu.php"); ?>
	<!-- BEGIN CONTAINER -->
	<div class="page-container row-fluid">

		<?php require_once("inc/main_menu.php"); ?>

		<!-- BEGIN PAGE -->
		<div class="page-content">
			<div class="container-fluid">

				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">

						<div class="row-fluid">
							<div class="messages_list <?php echo $html_array['messages_list_span']; ?>">

								<div class="portlet box blue nomarginbottom">
									<div class="portlet-title">
										<div class="caption"><i class="icon-comments"></i>Messages</div>
									</div>
									
									<div class="portlet-body">

										<div class="clearfix">
											<div class="btn-group">
												<a id="new_message" class="btn light-green" href=""><i class="icon-envelope"></i>&nbsp;&nbsp;New Message</a>
											</div>
										</div>

										<table class="table table-bordered table-striped table-condensed cf messages_table" id="data_table">
											<thead class="cf">
												<tr>
													<th></th>
													<th></th>
													<th class="sorting"></th>
													<th class="sorting"></th>
												</tr>
											</thead>
											<tbody>
												<?php echo $html_array['conversations_list']; ?>												
											</tbody>
										</table> 
									</div>
								</div>

							</div>

							<div class="message_box <?php echo $html_array['message_box_span']; ?>">

							</div>

						</div>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTAINER-->    
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
	<?php
	  require_once("inc/footer.php");
	?>
</body>
<!-- END BODY -->
</html>
<?php
   require_once("inc/session_end.php");
?>