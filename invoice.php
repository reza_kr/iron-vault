<?php
   require_once("inc/session_start.php");
   require_once("functions.php");

   $html_array = load_page_html();

   require_once("inc/head.php");
?>
<!-- BEGIN BODY -->
<body class="fixed-top page-sidebar-fixed <?php echo $html_array['page_sidebar_closed']; ?>">
   <?php require_once("inc/top_menu.php"); ?>
   <!-- BEGIN CONTAINER -->   
   <div class="page-container row-fluid">

        <?php require_once("inc/main_menu.php"); ?>

		<div class="page-content">
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">

						<div class="portlet box blue">
							<div class="portlet-title">
								<div class="caption"><a href="invoices.php" class="btn portlet_title_btn"><i class="icon-arrow-left"></i></a><i class="icon-book"></i>Invoice</div>
							</div>
							<div class="portlet-body">

								<div class="row-fluid invoice-logo">

									<?php echo $html_array['view_invoice']['business_logo']; ?>

									<div class="span3">
										
										<?php echo $html_array['view_invoice']['business_info']; ?>

									</div>
									<div class="span3">

										<?php echo $html_array['view_invoice']['employee_info']; ?>

									</div>
									<div class="span3">
										<?php echo $html_array['view_invoice']['payment_status']; ?>
									</div>
								</div>
								<hr />

								<div class="row-fluid">
									<div class="span3">
										<h4>Client:</h4>

										<div class="invoice_client_details">
											<?php echo $html_array['view_invoice']['client_info']; ?>
										</div>
									</div>
									<div class="span3">
										<?php echo $html_array['view_invoice']['client_additional_info']; ?>
									</div>
									<div class="span3">

									</div>
									<div class="span3">
										
										<h4>Invoice #:</h4>
										<span id="invoice_num"><?php echo $html_array['view_invoice']['invoice_number']; ?></span>
									</div>
								</div>
								<hr />
								<div class="row-fluid">
									<div class="span3">
										<h4>Payment Type:</h4>

										<?php echo $html_array['view_invoice']['payment_type']; ?>
									</div>
									<div class="span3">
										<h4>Invoice Date:</h4>
										
										<?php echo $html_array['view_invoice']['invoice_date']; ?>
									</div>
									<div class="span3">
										<h4>Payment Term:</h4>

										<?php echo $html_array['view_invoice']['payment_term']; ?>
									</div>
									<div class="span3">
										<h4>Due Date:</h4>

										<?php echo $html_array['view_invoice']['due_date']; ?>
									</div>
									
								</div>
								<hr />
								<div class="row-fluid">
									<h4>Products/Services:</h4>
									<table class="table table-striped table-hover invoice_items">
										<thead>
											<tr>
												<th id="count">#</th>
												<th id="">Item</th>
												<th class="hidden-480">Description</th>
												<th class="hidden-480">Quantity</th>
												<th class="hidden-480">Rate</th>
												<th>Item Total</th>
											</tr>
										</thead>
										<tbody>
											<?php echo $html_array['view_invoice']['invoice_items']; ?>
										</tbody> 
									</table>

								</div>
								<hr>
								<div class="row-fluid">
									<div class="span6">
										<h4>Additional Notes:</h4>
										<?php echo $html_array['view_invoice']['notes']; ?>
									</div>
									
									<div class="span6 invoice-block">
										<?php echo $html_array['view_invoice']['invoice_totals']; ?>
										
										<div class="clearfix"></div>
										
									</div>
								</div>
								<hr>
								
								<div class="row-fluid">	
									<div class="span12">
										<a class="btn light-red" href="#delete_invoice_modal" data-toggle="modal">Discard Invoice <i class="icon-trash"></i></a>
										<a class="btn light-blue save_draft right">Save Draft <i class="icon-save"></i></a>
										<input type="hidden" id="invoice_id" value="<?php echo $html_array['create_invoice']['invoice_id']; ?>" />
									</div>
								</div>

							</div>
						</div>

						<div class="portlet box blue">
							<div class="portlet-title">
								<div class="caption">
									<i class="icon-file"></i>
									Attachments
								</div>
							</div>
							<div class="portlet-body attachments">

								<!--<span class="box-message">There are no attachments for this invoice. To add one now, select your file and hit 'Upload'</span>-->

								<div class="attachment">
                                    <br>
                                    <i class="icon-cloud-upload xl_icon"></i>
                                    <br><br>
                                    <div class="btn-group">
                                        <a href="#upload_invoice_files_modal" data-toggle="modal" class="btn light-green"><i class="icon-plus"></i> Add File(s)</a>
                                    </div>
                                </div>

								<?php echo $html_array['invoice_files']; ?>

								<div class="clearfix"></div>
							</div>
						</div>

					</div>
				</div>

			</div>
			<!-- END PAGE CONTAINER--> 
		</div>
   	</div>
   	<!-- END PAGE CONTAINER--> 
   	<!-- END CONTAINER -->
   	<?php
      	require_once("inc/footer.php");
   	?>

   	<div id="upload_invoice_files_modal" class="modal hide fade" tabindex="-1" data-focus-on="input:first">
        <form class="zero_margin" action="#" method="post">
            <input type="hidden" id="invoice_id" value="<?php echo $_GET['id']; ?>" />
            <div class="modal-header">
                <i class="icon-remove right" data-dismiss="modal" aria-hidden="true"></i>
                <h3>
                    <i class="icon-cloud-upload"></i>&nbsp;&nbsp;Upload File(s)
                </h3>
            </div>
            <div class="modal-body">
                <div class="file_upload">
                    <div id="file_preview_wrapper" style="display: none;">
                        <img src="#" id="file_preview" style="display:none;" />
                    </div>
                    <span class="btn light-green fileinput-button">
                        <i class="icon-plus"></i>
                        <span>Add File(s)</span>
                        <!-- The file input field used as target for the file upload widget -->
                        <input id="file" type="file" name="file" multiple>
                    </span>
                    <div id="file_progress" class="progress progress_thin progress-success progress-striped" style="display: none;">
                        <div class="bar"></div>
                    </div>
                </div>

                <table class="added_files table table-bordered table-striped table-condensed" style="display:none;">
                    <tr>
                        <th>Filename</th>
                        <th>Filesize</th>
                        <th>Progress</th>
                    </tr>
                </table>
            </div>
            <div class="modal-footer">
                <a href="#" class="btn light-red disabled cancel_uploads" >Cancel Uploads</a>
                <a href="#" data-dismiss="modal" class="btn light-green right">Hide</a>
            </div>
        </form>
    </div>


    <div id="delete_invoice_file_modal" class="modal hide fade" tabindex="-1" data-focus-on="input:first">
        <form class="zero_margin" action="#" method="post">
            <input type="hidden" id="invoice_id" value="<?php echo $_GET['id']; ?>" />
            <input type="hidden" id="file_id" value="" />
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h3>
                    <i class="icon-trash"></i>&nbsp;&nbsp;Delete File
                </h3>
            </div>
            <div class="modal-body"></div>
            <div class="modal-footer">
                <input type="submit" id="submit" class="btn light-red right delete" value="Delete" />
                <input type="button" data-dismiss="modal" class="btn" value="Cancel" />
            </div>
        </form>
    </div>

</body>
<!-- END BODY -->
</html>
<?php
   require_once("inc/session_end.php");
?>