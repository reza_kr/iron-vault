<?php
   require_once("inc/session_start.php");
   require_once("functions.php");

   $html_array = load_page_html(); 

   require_once("inc/head.php");

?>



<body class="login">

  <div class="container-fluid">

    <br><h2 class="business_name"><?php echo $_SESSION['business_name']; ?></h2>

    <div id="login_modal" class="modal fade in" tabindex="-1" data-focus-on="input:first">
      <form class="form-vertical" action="login" method="post">
        <div class="modal-header">
          <h3><i class="icon-lock"></i>&nbsp;&nbsp;Account Login</h3>
        </div>

        <div class="modal-body">

            <!-- BEGIN LOGIN FORM -->

              <h3 class="form-title">Login to your account</h3>

              <?php

                if($_GET['e'] == "login_failed") {
                  echo '<div class="form_error">
                          <span><i class="icon-warning-sign"></i> Wrong username and/or password.</span>
                        </div>';
                } else if($_GET['e'] == "account_disabled") {
                  echo '<div class="form_error">
                          <span><i class="icon-warning-sign"></i> Your account is disabled.</span>
                        </div>';
                }

              ?>

              <div class="control-group">

                <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->

                <label class="control-label visible-ie8 visible-ie9">Username</label>

                <div class="controls">

                  <div class="input-icon left">

                    <i class="icon-user"></i>

                    <input class="m-wrap placeholder-no-fix" type="text" placeholder="Username" name="username"/>

                  </div>

                </div>

              </div>

              <div class="control-group">

                <label class="control-label visible-ie8 visible-ie9">Password</label>

                <div class="controls">

                  <div class="input-icon left">

                    <i class="icon-lock"></i>

                    <input class="m-wrap placeholder-no-fix" type="password" placeholder="Password" name="password"/>

                  </div>

                </div>

              </div>

              <div class="forget-password">

                <p><a href="password_reset" class="">Forgot your password?</a></p>

              </div>    

        </div>

        <div class="modal-footer">
          <input type="submit" id="submit" class="btn light-green fullwidth" data-dismiss="modal" value="Login" />
        </div>

      </form> 
    </div>


    <div class="login_footer">
      <div class="logo">

        <a href="https://www.ironvault.ca"><img src="img/logo_small.png" alt="Iron Vault" /></a> 

      </div>

      <div class="copyright">

        <?php echo date('Y'); ?> © IRON VAULT LTD

      </div>
    </div>

  </div>  



  <!-- BEGIN COPYRIGHT -->

  

  <!-- END COPYRIGHT -->



  

  <?php



    $footer_scritps = load_footer_scripts();



    echo $footer_scritps;



  ?>
<script src="plugins/bootstrap-toastr/toastr.min.js"></script>
<script src="scripts/custom.js?t=<?php echo time(); ?>"></script>


</body>

<!-- END BODY -->

</html>



<?php

   require_once("inc/session_end.php");

?>