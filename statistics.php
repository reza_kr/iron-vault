<?php
	require_once("inc/session_start.php");
	require_once("functions.php");
   
	$html_array = load_page_html();
	
	require_once("inc/head.php");
?>
<!-- BEGIN BODY -->
<body class="fixed-top page-sidebar-fixed <?php echo $html_array['page_sidebar_closed']; ?>">
   <?php require_once("inc/top_menu.php"); ?>
   <!-- BEGIN CONTAINER -->   
   <div class="page-container row-fluid">

      <?php require_once("inc/main_menu.php"); ?>

      <!-- BEGIN PAGE -->
      <div class="page-content"> 
         <div class="container-fluid">

				<!-- BEGIN PAGE CONTENT-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN EXAMPLE TABLE PORTLET-->
						<div class="portlet box blue">
							<div class="portlet-title">
								<div class="caption"><i class="icon-bar-chart"></i>Statistics</div>
								<div id="dashboard-report-range" class="dashboard-date-range tooltips no-tooltip-on-touch-device responsive pull-right" data-tablet="" data-desktop="tooltips" data-placement="top" 
								data-original-title="Change dashboard date range">
									<i class="icon-calendar">&nbsp;</i>
									<span></span>
									<i class="icon-angle-down"></i>
								</div>

							</div>
							
							<div class="portlet-body no-more-tables">
								<h3>Interactive Graph</h3>
								<br>

								<div id="chart_1" class="chart"></div>

								<hr>
								<br>

								<h3>Interactive Graph 2</h3>
								<br>

								<div id="chart_2" class="chart"></div>

								<hr>
								<br>

								<h3>Interactive Graph 3</h3>
								<br>

								<div id="chart_3" class="chart"></div>


							</div>
						</div>
						<!-- END EXAMPLE TABLE PORTLET-->
					</div>
				</div>
				<!-- END PAGE CONTENT -->
			</div>

		 
         <!-- END PAGE CONTAINER-->       
      </div>
      <!-- BEGIN PAGE -->
   </div>
   <!-- END PAGE CONTAINER--> 
   <!-- END CONTAINER -->
   <?php
      require_once("inc/footer.php");
   ?>
</body>
<!-- END BODY -->
</html>
<?php
   require_once("inc/session_end.php");
?>