<?php
   require_once("inc/session_start.php");
   require_once("functions.php");

   $html_array = load_page_html();

   require_once("inc/head.php");
?>
<!-- BEGIN BODY -->
<body class="fixed-top page-sidebar-fixed <?php echo $html_array['page_sidebar_closed']; ?>">
   <?php require_once("inc/top_menu.php"); ?>
   <!-- BEGIN CONTAINER -->   
   <div class="page-container row-fluid">

      <?php require_once("inc/main_menu.php"); ?>

      <!-- BEGIN PAGE -->
      <div class="page-content">
         <div class="container-fluid">
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
               <div class="span12">

                  <div class="portlet box blue">
                     <div class="portlet-title">
                        <div class="caption"><a href="inventory" class="btn portlet_title_btn"><i class="icon-arrow-left"></i></a><i class="icon-edit"></i> Product Details</div>
                     </div>
                     <div class="portlet-body">
                        <div class="row-fluid">
                           <div class="span4 profile-info">

                              <h3><?php echo $html_array['product']['item_name']; ?></h3>

                              <div class="product_image">
                                <?php echo $html_array['product']['item_image']; ?>
                              </div>

                              <table class="inline-table">
                                <tr>
                                  <td>Product SKU/ID</td>
                                  <td><?php echo $html_array['product']['product_id']; ?></td>
                                </tr>

                                <tr>
                                  <td>In Stock</td>
                                  <td><?php echo $html_array['product']['item_qty']; ?></td>
                                </tr>

                                <tr>
                                  <td>Unit Price</td>
                                  <td><?php echo $html_array['currency'] .  $html_array['product']['item_cost']; ?></td>
                                </tr>

                                <tr>
                                  <td>Category</td>
                                  <td><?php echo $html_array['product']['category']; ?></td>
                                </tr>

                                <tr>
                                  <td>Description</td>
                                  <td><?php echo $html_array['product']['item_desc']; ?></td>
                                </tr>
                              </table>

                              <hr>

                              <form action="product?id=<?php echo $html_array['product']['item_id']; ?>&action=add_item_qty" method="post" id="add_item_qty" name="product_add_item_qty">
                                <input type="hidden" name="item_id" value="<?php echo $html_array['product']['item_id']; ?>" />
                                <h3>Quick Restock</h3>
                                <p><input type="text" class="m-wrap xsmall quantity center" name="qty" id="qty" value="1" placeholder="QTY" /><input type="submit" class="btn light-green m_l_10" name="submit" value="Add to Inventory" /></p>
                              </form>


                              <div class="qr_code"><?php echo $html_array['product']['item_qr_code']; ?></div>


                              <br>
                              <a id="update_product" class="btn light-green" data-toggle="modal" href="#update_product_modal">Edit Item</a>
                              <a href="#delete_item_modal" data-toggle="modal" class="btn light-red delete_item right">Delete Item</a>
                           </div>
 
                           <div class="span8">

                              <div class="clearfix">
                                <h3>Sales History</h3>
                              </div>

                              <div class="chart_wrapper">
                                <input type="hidden" id="sales_history_data" value='<?php echo $html_array["product"]["sales_history"]; ?>' />
                                <div class="sales_history_chart"></div>
                                <div class="sales_history_chart_zoom"></div>
                              </div>

                              <hr>

                              <div class="clearfix">
                                <h3>Invoices Containing This Product</h3>

                                 <?php echo $html_array['export_tools']; ?>
                              </div>
                              <table class="table table-bordered table-striped table-condensed cf invoices saveaspdf" id="data_table">
                                <thead class="cf">
                                  <tr>
                                    <th class="sorting">Invoice #</th>
                                    <th class="sorting">Client</th>
                                    <th class="sorting">Last Modified</th>
                                    <th class="sorting">Prepared By</th>
                                    <th class="sorting">Quantity</th>
                                    <th class="sorting">Status</th>
                                    <th class="sorting">Invoice Total</th>
                                    <th>Action</th>
                                  </tr>
                                </thead>
                                <tbody>

                                  <?php echo $html_array['product_invoices']; ?>

                                </tbody>
                              </table>

                           </div>
                        </div>
                     </div>
                  </div>

                  <div id="update_product_modal" class="modal hide fade" tabindex="-1" data-focus-on="input:first">
                     <form class="zero_margin" action="product?id=<?php echo $_GET['id']; ?>&action=update_inventory_item" method="post" enctype="multipart/form-data" name="product_update_product">

                        <div class="modal-header">
                           <i class="icon-remove right" data-dismiss="modal" aria-hidden="true"></i>
                           <h3><i class="icon-pencil"></i>&nbsp;&nbsp;Edit Product Details</h3>
                        </div>

                        <div class="modal-body">

                           <div class="row-fluid">

                              <div class="span6 ">
                                <div class="control-group">
                                  <label class="control-label">Product Name</label>
                                   <div class="controls">
                                     <input type="hidden" name="item_id" class="m-wrap span12" value="<?php echo $html_array['product']['item_id']; ?>" />
                                     <input type="text" name="item_name" class="m-wrap span12" value="<?php echo $html_array['product']['item_name']; ?>" />
                                    </div>
                                </div> 
                              </div>
                              <!--/span-->
                              <div class="span6 ">
                                <div class="control-group">
                                   <label class="control-label">Product SKU/ID</label>
                                   <div class="controls">
                                      <input type="text" name="product_id" class="m-wrap span12" value="<?php echo $html_array['product']['product_id']; ?>" />
                                   </div>
                                </div>
                              </div>
                              <!--/span-->
                           </div>

                           <div class="row-fluid">

                                <div class="span6 ">
                                   <div class="control-group">
                                     <label class="control-label">Description</label>
                                      <div class="controls">
                                       <input type="text" name="item_desc" class="m-wrap span12" value="<?php echo $html_array['product']['item_desc']; ?>" />
                                      </div>
                                   </div>
                                </div>
                                <!--/span-->
                                <div class="span6 ">
                                   <div class="control-group">
                                      <label class="control-label">Category</label>
                                      <div class="controls">
                                        <select name="category" class="m-wrap span12">
                                          <?php echo $html_array['product']['option']; ?>

                                        </select>
                                      </div> 
                                   </div>
                                </div> 
                                <!--/span-->
                             </div>

                              <div class="row-fluid">

                                <div class="span6 ">
                                   <div class="control-group">
                                     <label class="control-label">Quantity</label>
                                      <div class="controls">

                                       <input type="text" name="item_qty" class="m-wrap span12" value="<?php echo $html_array['product']['item_qty']; ?>" />

                                      </div>
                                   </div>
                                </div>
                                <!--/span-->
                                <div class="span6 ">
                                   <div class="control-group">
                                      <label class="control-label">Price</label>
                                      <div class="controls">

                                 <input type="text" name="item_cost" class="m-wrap span12" value="<?php echo $html_array['product']['item_cost']; ?>" />   

                                      </div> 
                                   </div>
                                </div> 
                                <!--/span-->
                              </div>   

                              <div class="row-fluid">

                               <div class="span6 ">
                                  <div class="control-group">
                                    <label class="control-label">Image</label>
                                     <div class="controls">

                                        <div class="fileupload fileupload-new" data-provides="fileupload">
                                          <div class="input-append">
                                             <div class="uneditable-input">
                                                <i class="icon-file fileupload-exists"></i> 
                                                <span class="fileupload-preview"></span>
                                             </div>
                                             <span class="btn btn-file">
                                                <span class="fileupload-new">Select file</span>
                                                <span class="fileupload-exists">Change</span>
                                                <input type="hidden" name="MAX_FILE_SIZE" value="2097152" />
                                                <input name="image_file" type="file" />
                                             </span>
                                             <a href="#" class="btn fileupload-exists remove" data-dismiss="fileupload">Remove</a>
                                          </div>
                                        </div>

                                     </div>
                                  </div>
                               </div>
                               <!--/span-->
                            </div>                                         

                        </div>   

                        <div class="modal-footer">
                           <input type="submit" id="submit" class="btn light-green right" value="Update" />
                           <input type="button" data-dismiss="modal" class="btn" value="Cancel" />
                        </div>


                     </form>  
                  </div>


                  <div id="delete_item_modal" class="modal hide fade" tabindex="-1" data-focus-on="input:first">

                    <form class="zero_margin" action="#" method="post">
                      <input type="hidden" name="item_id" id="item_id" class="m-wrap span12" value="<?php echo $html_array['product']['item_id']; ?>" />
                      <input type="hidden" name="item_name" id="item_name" class="m-wrap span12" value="<?php echo $html_array['product']['item_name']; ?>" />

                      <div class="modal-header">
                        <i class="icon-remove right" data-dismiss="modal" aria-hidden="true"></i>
                        <h3><i class="icon-remove"></i>&nbsp;&nbsp;Delete Item</h3>
                      </div>

                      <div class="modal-body">
                        
                      </div>  
                        
                      <div class="modal-footer">
                        <input type="submit" id="submit" class="btn light-red right delete" value="Delete" />
                        <input type="button" data-dismiss="modal" class="btn" value="Cancel" />
                      </div>

                    </form>

                  </div>

               </div>
            </div>
            <!-- END PAGE CONTENT--> 
         </div>
         <!-- END PAGE CONTAINER-->       
      </div>
      <!-- BEGIN PAGE -->
   </div>
   <!-- END PAGE CONTAINER--> 
   <!-- END CONTAINER -->
   <?php
      require_once("inc/footer.php");
   ?>
</body>
<!-- END BODY -->
</html>
<?php
   require_once("inc/session_end.php");
?>