var express = require("express");
var https = require('https');
var fs = require('fs');
var socketio = require('socket.io');

var privatekey = fs.readFileSync('/usr/local/ssl/private/private.key', 'utf8').toString();
var certificate = fs.readFileSync('/usr/local/ssl/crt/certificate.crt', 'utf8').toString();
var intermediate = fs.readFileSync('/usr/local/ssl/crt/intermediate.crt', 'utf8').toString();

var options = {
    key: privatekey,
    cert: certificate,
    ca: intermediate
}

var app = express();

var server = https.createServer(options, app);

var io = socketio.listen(server);

io.enable('browser client minification');  // send minified client
io.enable('browser client etag');          // apply etag caching logic based on version number
io.enable('browser client gzip');          // gzip the file
io.set('log level', 1); 

var connections = {};

server.listen(8082);

io.sockets.on('connection', function(socket){

    socket.on('new connection', function(data){ 
        socket.connection_id = data.convo_id + "_" + data.user_id;
        connections[socket.connection_id] = socket.id;
    });


    socket.on('send', function(data){
        var message = data.message;
        var first_name = data.first_name;
        var last_name = data.last_name;
        var image = data.image;
        var username = data.username;
        var user_id = data.user_id;
        var account_id = data.account_id;
        var users = data.users;
        var convo_id = data.convo_id;


        var message_object = new Object;

        message_object.message = message;
        message_object.first_name = first_name;
        message_object.last_name = last_name;
        message_object.image = image;
        message_object.user_id = user_id;
        message_object.convo_id = convo_id;

        var users_array = users.split(',');
        var users_count = users.length;

        for (var i = 0; i < users_count; i++) {

            var connection_id = convo_id + "_" + users_array[i];

            io.sockets.socket(connections[connection_id]).emit('message', message_object);
        };

    });

    
    socket.on('disconnect', function(){
        if(!socket.connection_id) {
            return;
        }

        delete connections[socket.connection_id];
    });
});