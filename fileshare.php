<?php

	session_start();

	require_once('error_handler.php');

	require_once("classes/Account.class.php");
	$account = new Account();

	require_once("classes/Storage.class.php");
	$storage = new Storage();

	$result = $account->checkAccountName();
	if(!$result) {

		header('Location: https://www.ironvault.ca/');
	}


	//Elfinder Hash Lookup
	$elfinder_hash = $storage->getElfinderHash();

	$data = $storage->getSharedFiles($elfinder_hash);

?>
<!DOCTYPE html>

<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->

<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->

<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

<!-- BEGIN HEAD -->

<head>
   <noscript>
      <meta http-equiv="refresh" content="0; URL='https://www.ironvault.ca/no_script'">
   </noscript>

   <meta charset="utf-8" />
   <title>Fileshare – <?php echo $_SESSION['business_name']; ?> | Iron Vault</title>
   <meta content="width=device-width, initial-scale=1.0" name="viewport" />

   <link href="css/stylesheet.min.css" rel="stylesheet" type="text/css"/>
   <link href="css/custom.css" rel="stylesheet" type="text/css"/>

   <link rel='shortcut icon' type='image/x-icon' href='img/favicon.ico' />

   <link rel="apple-touch-icon" sizes="57x57" href="apple-icon-57x57.png" />
   <link rel="apple-touch-icon" sizes="72x72" href="apple-icon-72x72.png" />
   <link rel="apple-touch-icon" sizes="114x114" href="apple-icon-114x114.png" />
   <link rel="apple-touch-icon" sizes="144x144" href="apple-icon-144x144.png" />
</head>

<body class="fileshare">

  <div class="container-fluid">

    <br><h2 class="business_name"><?php echo $_SESSION['business_name']; ?></h2>

    <div id="fileshare_modal" class="modal fade in" tabindex="-1" data-focus-on="input:first">
    	<div class="modal-header">
          <h3><i class="icon-file-text"></i>&nbsp;&nbsp;Fileshare</h3>
          <?php echo $data['remove_button']; ?>
        </div>

        <div class="modal-body">

			<?php 

				if($data['files'] != "") {

					echo $data['files'];

				} else if($_GET['r'] == "removed") {

					$message = $storage->fileshareRemovedMessage();

					echo $message;
				} 

			?>
        </div>
    </div>


    <div class="fileshare_footer">
      <div class="logo">

        <a href="https://www.ironvault.ca"><img src="img/logo_small.png" alt="Iron Vault" /></a> 

      </div>

      <div class="copyright">

        <?php echo date('Y'); ?> © IRON VAULT LTD

      </div>
    </div>

  </div>  

<script src="scripts/plugins.min.js"></script>
<script src="scripts/custom.js?t=<?php echo time(); ?>"></script>

</body>
<!-- END BODY -->
</html>

<?php
   require_once("inc/session_end.php");
?>