var socket = io.connect('https://www.ironvault.ca:8084', {'secure': true, 'reconnect': true, 'reconnection delay': 1000});

socket.emit('new connection', user_id);

$(document).ready(function() {

 // ============================

    socket.on('receive_notification', function (data) {

        if(account_id == data.account_id) {
            if(data.notification_type == "message") {
                
                handlePrivateMessageNotification(data.convo_id);
            
            } else if(data.notification_type == "event") {

                handleEventNotification();

            } else if(data.notification_type == "task") {

                handleTaskNotification();
            }
        }
    });

 
});


function sendMessageNotification(receiver_user_ids, convo_id, account_id) { //coming from private_messages.js

    var notification_object = new Object();

    notification_object.notification_type = "message";
    notification_object.receiver_user_ids = receiver_user_ids;
    notification_object.convo_id = convo_id;
    notification_object.account_id = account_id;
    
    socket.emit('notify', notification_object);

    

    //Add notification to DB
    $.ajax({
        type: "POST",
        url: "ajax_functions",
        data: { 
            action: "add_notification",
            receiver_user_ids: receiver_user_ids,
            convo_id: convo_id,
            type: "message"
        }

    }).done(function(result) {

    });

}


function handlePrivateMessageNotification(convo_id) {

    // messages_notifications, events_notifications, tasks_notifications are initialized in footer.php from PHP
    var page = window.location.pathname.substring(1);
    var flag = true;

    if(page == "messages") {
        
        var cur_convo_id = $('.chat-form .convo_id').val();

        if(cur_convo_id == convo_id) {

            flag = false; //user is currently on the convo page; do not add notification to top bar.
        }
    }

    if(flag == true) {

        if($.inArray(convo_id, convo_notifs_array) == -1) { //inArray returns the position in array. -1 means not found in array.

            if(messages_notifications > 0) {

                messages_notifications++;

                $('.messages_notification_badge').text(messages_notifications);
            
            } else {

                messages_notifications = 1;

                $('.messages_notification>a:first-child').append('<span class="badge messages_notification_badge">' + messages_notifications + '</span>');
            }

            //Add the convo_id to list of unseen convo notifications to prevent from adding +1 for each sent message
            convo_notifs_array.push(convo_id);
        }

    }

    updateTotalNotifications();
    
    reloadConversationList();


}


function handleEventNotification() {

    if(events_notifications > 0) {

        events_notifications++;

        $('.events_notification_badge').text(events_notifications);
    
    } else {

        events_notifications = 1;

        $('.events_notification>a:first-child').append('<span class="badge events_notification_badge">' + events_notifications + '</span>');
    }

    updateTotalNotifications();

}


function handleTaskNotification() {

    if(tasks_notifications > 0) {

        tasks_notifications++;

        $('.tasks_notification_badge').text(tasks_notifications);
    
    } else {

        tasks_notifications = 1;

        $('.tasks_notification>a:first-child').append('<span class="badge tasks_notification_badge">' + tasks_notifications + '</span>');
    }

    updateTotalNotifications();

}


function updateTotalNotifications() {

    total_notifications = messages_notifications + events_notifications + tasks_notifications;

    var new_title = "";
    var current_title = $(document).find("title").text();
    var first_char = current_title.charAt(0);

    if(first_char == "(") {
        current_title = current_title.substring(current_title.indexOf(' ')+1);
    }

    if(total_notifications > 0) {
        new_title = "(" + total_notifications + ") " + current_title;
    } else {

        first_char = current_title.charAt(1);
        if(first_char == "–") {
            current_title = current_title.substring(3);
        }

        new_title = current_title;
    }


    document.title = new_title;
}
