<?php 

	session_start();
	require_once('/var/www/html/app/inc/connect_dbo.php');
	//$dbo coming from connect_db.php

	$notifs_to_push = array();
	$count = 0;

	$datetime = date('Y-m-d H:i:s');

	$query = $this->dbo->prepare("SELECT * FROM calendar WHERE start >= ? AND notify = ? AND notified = ?");
	$query->execute(array($datetime, 1, 0));

	$result = $query->fetchAll();

	foreach($result as $row) {
		
		$id = $row[0];
		$account_id = $row[1];
		$user_id = $row[2];
		$start = $row[3];
		$end = $row[4];
		$title = $row[5];
		$all_day = $row[6];
		$notify = $row[7];
		$notification_time = $row[8];

		$notification_time = $notification_time * 60; //convert to seconds
		$cur_time = time();
		$notification_time = $notification_time + $cur_time;

		//$time_left = date(strtotime($start)) ;
		$time_left = date(strtotime($start) - $notification_time + $_SESSION['timezone_offset']);
		// $time_left should be 0 or LESS to indicate that it's time to push the notification

		if($time_left <= 0) {

			$notifs_to_push[$count] = array('id' => $id, 'account_id' => $account_id, 'user_id' => $user_id, 'datetime' => $start, 'type' => 'event');
			$count++;
		}
	}


	$query = $this->dbo->prepare("SELECT * FROM tasks WHERE date_due >= ? AND completed = ? AND notify = ? AND notified = ?");
	$query->execute(array($datetime, 0, 1, 0));

	$result = $query->fetchAll();

	foreach($result as $row) {
		
		$id = $row[0];
		$account_id = $row[1];
		$user_id = $row[2];
		$task = $row[3];
		$date_posted = $row[4];
		$date_due = $row[5];
		$completed = $row[6];
		$notify = $row[7];
		$notification_time = $row[8];

		$notification_time = $notification_time * 60; //convert to seconds
		$cur_time = time();
		$notification_time = $notification_time + $cur_time;

		//$time_left = date(strtotime($date_due)) ;
		$time_left = date(strtotime($date_due) - $notification_time + $_SESSION['timezone_offset']);
		// $time_left should be 0 or LESS to indicate that it's time to push the notification

		if($time_left <= 0) { //CHANGE TO <= 0 !!!!!!!!!!
			$notifs_to_push[$count] = array('id' => $id, 'account_id' => $account_id, 'user_id' => $user_id, 'datetime' => $date_due, 'type' => 'task');
			$count++;
		}
	}


	//echo '<pre>'; print_r($notifs_to_push); echo '</pre>';

	foreach($notifs_to_push as $key => $notif) {

		$id = $notif['id'];
		$account_id = $notif['account_id'];
		$user_id = $notif['user_id'];
		$datetime = $notif['datetime'];
		$type = $notif['type'];

		if($type == 'event') {

			$query = $this->dbo->prepare("INSERT INTO notifications SET account_id = ?, user_id = ?, event_id = ?, type = ?, status = ?, date = ?");
			$query->execute(array($account_id, $user_id, $id, 'event', 'unseen', $datetime));

			//UPDATE event to 'Notified'
			$query = $this->dbo->prepare("UPDATE calendar SET notified = ? WHERE account_id = ? AND user_id = ? AND id = ?");
			$query->execute(array(1, $account_id, $user_id, $id));

		} else if($type == 'task') {

			$query = $this->dbo->prepare("INSERT INTO notifications SET account_id = ?, user_id = ?, task_id = ?, type = ?, status = ?, date = ?");
			$query->execute(array($account_id, $user_id, $id, 'task', 'unseen', $datetime));

			//UPDATE task to 'Notified'
			$query = $this->dbo->prepare("UPDATE tasks SET notified = ? WHERE account_id = ? AND user_id = ? AND id = ?");
			$query->execute(array(1, $account_id, $user_id, $id));

		}

		exec("node /var/www/html/app/notifications/push_events_tasks.js " . $account_id . " " . $user_id . " " . $type);
	}

?>
