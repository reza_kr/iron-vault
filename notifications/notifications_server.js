var express = require("express");
var https = require('https');
var fs = require('fs');
var socketio = require('socket.io');

var privatekey = fs.readFileSync('/usr/local/ssl/private/private.key', 'utf8').toString();
var certificate = fs.readFileSync('/usr/local/ssl/crt/certificate.crt', 'utf8').toString();
var intermediate = fs.readFileSync('/usr/local/ssl/crt/intermediate.crt', 'utf8').toString();

var options = {
    key: privatekey,
    cert: certificate,
    ca: intermediate
}

var app = express();

var server = https.createServer(options, app);

var io = socketio.listen(server);

io.enable('browser client minification');  // send minified client
io.enable('browser client etag');          // apply etag caching logic based on version number
io.enable('browser client gzip');          // gzip the file
io.set('log level', 1); 

var users = {};

server.listen(8084);

io.sockets.on('connection', function(socket){

    socket.on('new connection', function(user_id){ 
        socket.user_id = user_id;
        users[socket.user_id] = socket.id;
    });


    socket.on('notify', function(data){

        var receiver_user_ids = data.receiver_user_ids;
        var notification_type = data.notification_type;
        var account_id = data.account_id;
        var convo_id = data.convo_id;


        var notification_object = new Object;

        notification_object.notification_type = notification_type;
        notification_object.account_id = account_id;
        notification_object.convo_id = convo_id;


        var users_array = receiver_user_ids.split(',');
        var users_count = receiver_user_ids.length;

        for (var i = 0; i < users_count; i++) {

            io.sockets.socket(users[users_array[i]]).emit('receive_notification', notification_object);
        };

    });

    
    socket.on('disconnect', function(){
        if(!socket.user_id) {
            return;
        }

        console.log("User Disconnected:" + socket.user_id);
        delete users[socket.user_id];
    });
});
