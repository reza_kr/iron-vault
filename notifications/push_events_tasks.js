//This script is ONLY  used for pushing notifications from PHP cron jobs to users
var io = require('socket.io-client');

if(process.argv.length > 0) {
    
    //process.argv is an array which contains all parameter variables passed to this script per command line. [0] is 'node', [1] is 'push_events_tasks.js'.
    var account_id = process.argv[2];
    var user_id = process.argv[3];
    var type = process.argv[4];

    var socket = io.connect('https://www.ironvault.ca:8084', {'secure': true, 'reconnect': true, 'reconnection delay': 1000});

    var notification_object = new Object();

    notification_object.notification_type = type;
    notification_object.receiver_user_id = user_id;
    notification_object.account_id = account_id;

    socket.emit('notify', notification_object);

}

setTimeout(function() {
    process.exit();
}, 1000);