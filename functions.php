<?php

	
	
	require_once('error_handler.php');

	require_once("classes/Main.class.php");
	$main = new Main();

	require_once("classes/Account.class.php");
	$account = new Account();

	require_once("classes/User.class.php");
	$user = new User();

	require_once("classes/Client.class.php");
	$client = new Client();

	require_once("classes/Storage.class.php");
	$storage = new Storage();


	

	//$main->logVisitorIP();

	$page = basename($_SERVER['PHP_SELF']);


	$account->checkAccountSession(); // ******* Check whether the account_id in the $_SESSION and the account_id retrieved from the subdomain match. IF NOT, log user out immediately. Send to login screen to subdomain account.
	
	if($_SESSION['status'] == "active") {   

		//check User's account status -- active, inactive, deleted, pending.
		$account->checkUserAccountStatus();

	} else if($_SESSION['status'] == "locked") { 

		if($page != "lock.php" && $page != "unlock.php") {

			header('Location: lock');
		}

	} else {

		if($page != "login.php" && $page != "logout.php" && $page != "password_reset.php" && $page != "password_change.php" && $page != "join.php" && $page != "view_invoice.php") {

			header('Location: login');
		}
	}



	$exclude_pages = array("join.php", "setup_wizard.php", "lock.php", "unlock.php", "404.php", "403.php", "500.php", "file.php", "ajax_functions.php", "doc_upload.php", "export_excel.php", "fileshare_conn.php", "fileshare_create.php", "fileshare_remove.php", "load_calendar.php", "load_work_schedule.php", "recurly.php", "recurly_push.php", "password_reset.php", "password_change", "wkhtmltopdf.php");
	if(!in_array($page, $exclude_pages)) {
		$_SESSION['last_page'] = $page;
	}


	if($_GET['action'] == "init_account") {

		$account->initAccount();
	}

	if($_GET['action'] == "add_user") {

		$account->addUser();
	}

	if($_GET['action'] == "update_client_profile") {

		$client->updateClientProfile(); 
	}
 

	if($_GET['action'] == "add_client") {

		$client->addClient();
	}

	if($_GET['action'] == "add_category") {

		$account->addCategory();
	}

	if($_GET['action'] == "update_category"){

		$account->updateCategory();

	}


	if($_GET['action'] == "delete_category"){

		$account->deleteCategory();
	}


	if($_GET['action'] == "update_personal_info") {

		$user->updatePersonalInfo();
	}

	if($_GET['action'] == "select_profile_picture") {

		$user->selectProfilePicture();
	}

	if($_GET['action'] == "upload_profile_picture") {

		$user->uploadPhoto();
	}

	if($_GET['action'] == "change_password") {

		$user->changePassword();
	}


	if($_GET['action'] == "set_screen_lock") {

		$user->setScreenLock();
	}


	if($_GET['action'] == "add_inventory_item") {

		require_once("classes/Inventory.class.php");
		$inventory = new Inventory();

		$inventory->addInventoryItem();

	}


	if($_GET['action'] == "add_multiple_inventory_items") {

		require_once("classes/Inventory.class.php");
		$inventory = new Inventory();

		$inventory->addMultipleInventoryItems();

	}


	if($_GET['action'] == "update_inventory_item") {

		require_once("classes/Inventory.class.php");
		$inventory = new Inventory();

		$inventory->updateInventoryItem();
	}


	if($_GET['action'] == "add_item_qty") {

		require_once("classes/Inventory.class.php");
		$inventory = new Inventory();

		$inventory->addItemQty();
	}


	if($_GET['action'] == "update_business_profile") {

		$account->updateBusinessProfile();
	}


	if($_GET['action'] == "update_account_preferences") {

		$account->updateAccountPreferences();
	}


	if($_GET['action'] == "update_modules") {

		$account->updateModules();
	}


	if($_GET['action'] == "update_storage_quota") {

		$storage->updateStorageQuota();
	}

	

	function getArray($arrayName) {
	
		require("inc/arrays.php");
		
		switch ($arrayName) {
			case "business_type":
				$array = $business_type;
				break;
			case "account_type":
				$array = $account_type;
				break;	
		
		}
		
		
		$data = "";
		
		for($x = 0; $x < count($array); $x++) {
			$data .= "<option value='$x'>" .$array[$x]. "</option>";
		}
		
		return $data;
	}


	function getSimpleArray($arrayName) {
	
		require("inc/arrays.php");
		
		switch ($arrayName) {
			case "month":
				$array = $month;
				break;
		
			case "countries":
				$array = $countries;
				break;

			case "business_type":
				$array = $business_type;
				break;
		}
		
		
		return $array;
	}
	
	
	function logout($msg) { 

		require_once("classes/User.class.php");
		$user = new User();

		$user->logout($msg);
	}


	function unlock() {

		require_once("classes/User.class.php");
		$user = new User();

		$user->unlock();
	}


	function load_top_menu() {

		require_once("classes/User.class.php");
		$user = new User();

		$data = $user->loadTopMenu();

		return $data;
	}



	function load_page_html() {

		$page = basename($_SERVER['PHP_SELF']);


		require_once("classes/Main.class.php");
		$main = new Main();

		require_once("classes/Account.class.php");
		$account = new Account();

		require_once("classes/Client.class.php");
		$client = new Client();

		require_once("classes/User.class.php");
		$user = new User();

		require_once("classes/Inventory.class.php");
		$inventory = new Inventory();

		require_once("classes/Invoice.class.php");
		$invoice = new Invoice();

		require_once("classes/Storage.class.php");
		$storage = new Storage();

		require_once("classes/Billing.class.php");
		$billing = new Billing();


		//$modules is an array with accessible_modules, accessible_menu
		$modules = $account->selectModules();
		$html_array['main_menu'] = $modules['accessible_menu'];
		$accessible_modules = $modules['accessible_modules'];

		if ($page != 'access_denied.php' && !in_array($page, $accessible_modules)) {

			header('Location: access_denied');
		}


		if($_SESSION['page_sidebar_closed'] == 1) {
			$html_array['page_sidebar_closed'] = "page-sidebar-closed";
		}


		$html_array['image_thumb'] = $_SESSION['image_thumb'];

		$html_array['convo_notifs'] = $user->getUnseenConvoIds();

		$pages_array = array();
		$pages_array['dashboard.php'] = "Dashboard";
		$pages_array['search.php'] = "Search";
		$pages_array['account_settings.php'] = "Account Settings";
		$pages_array['add_item.php'] = "Add Item";
		$pages_array['calendar.php'] = "Calendar";
		$pages_array['client_profile.php'] = "Client Profile";
		$pages_array['clients.php'] = "Clients";
		$pages_array['employees.php'] = "Employees";
		$pages_array['team_members.php'] = "Team Members"; //alias of employees
		$pages_array['associates.php'] = "Associates"; //alias of employees
		$pages_array['inventory.php'] = "Inventory";
		$pages_array['invoice.php'] = "Invoice";
		$pages_array['recurring_invoice.php'] = "Recurring Invoice";
		$pages_array['invoices.php'] = "Invoices";
		$pages_array['recurring_invoices.php'] = "Recurring Invoices";
		$pages_array['new_invoice.php'] = "New Invoice";
		$pages_array['new_recurring_invoice.php'] = "New Recurring Invoice";
		$pages_array['maps.php'] = "Maps";
		$pages_array['messages.php'] = "Messages";
		$pages_array['notes.php'] = "Notes";
		$pages_array['open_chat.php'] = "Open Chat";
		$pages_array['product.php'] = "Product";
		$pages_array['statistics.php'] = "Statistics";
		$pages_array['storage.php'] = "Storage";
		$pages_array['tasks.php'] = "Tasks";
		$pages_array['work_schedule.php'] = "Work Schedule";
		$pages_array['lock.php'] = "Session Locked";
		$pages_array['login.php'] = "Account Login";
		$pages_array['password_reset.php'] = "Password Reset";
		$pages_array['password_change.php'] = "Password Change";
		$pages_array['profile_settings.php'] = "Profile Settings";
		$pages_array['inactive_user.php'] = "Inactive User";
		$pages_array['setup_wizard.php'] = "Setup Wizard";
		$pages_array['join.php'] = "Join";
		$pages_array['access_denied.php'] = "Access Denied";


		switch($page) {


			case "login.php":

				$account_found = $account->checkAccountName();

				if(!$account_found) {
					session_unset();
					session_destroy();

					header('Location: https://ironvault.ca/');
				
				} else {
					
					if($_POST['username'] && $_POST['password']) {

						$user->login();

					}

					if($_SESSION['status'] == "active") {
						header('Location: dashboard');
					}

				}

				break;


			case "password_reset.php":
				$result = $account->checkAccountName();
				if(!$result) {
					header('Location: https://ironvault.ca/');
				} else {

					if($_POST['email']) {

						$result = $user->resetPassword();

						if($result == true) {

							header('Location: password_reset?r=request_sent');
						
						} else {

							header('Location: password_reset?r=account_not_found');
						}
					}
				}

				break;


			case "password_change.php":
				$result = $account->checkAccountName();
				if(!$result) {
					header('Location: https://ironvault.ca/');
				} else {

					if($_GET['token']) {

						//let this return the email address, can be used further down
						$email = $user->checkPasswordResetToken();

						if($email == false) {
							//Faulty token, email address, or expired... notify somehow?!?
							//header('Location: login');

						} else if($email != "") {  

					
							if($_GET['action'] == "process") { 

								$result = $user->resetPasswordChange($email);

							}

						}

					} else {

						header('Location: login');
					}
				}

				break;



			case "recurly.php": 

				$billing->recurlyInit(); //On Recurly.class.php

				$html_array['signature'] = $billing->recurlyCreateSignature(); //temporary solution... switch to ajax_functions after testing.


				break;


			case "recurly_push.php": 

				$billing->recurlyHandlePushNotification(); //On Recurly.class.php

				break;


			case "search.php":

				$html_array['search_filters'] = $modules['accessible_search'];

				if($_GET['query'] != "") {

					require_once("classes/Search.class.php");
					$search = new Search();

					$query = $_GET['query'];

					$html_array['search_results'] = $search->search($query, "");
				} else {

					$html_array['search_results'] = '<div class="search_results_header"><div class="loading"></div></div>';
				}

				break;


			case "dashboard.php": 

				$html_array['page_lock'] = 1;

				$html_array['timeline'] = $account->loadTimeline();

				$html_array['announcements'] = $account->loadAnnouncements();

				if($_SESSION['sound'] == "on") {

					$html_array['chat_sound_btn'] = '<div class="btn-sound on"> 
	                      <a href="" class="btn light-blue icn-only toggle_volume"><i class="icon-volume-up"></i></a>
	                   </div>';

				} else if($_SESSION['sound'] == "off") {

					$html_array['chat_sound_btn'] = '<div class="btn-sound off"> 
	                      <a href="" class="btn icn-only toggle_volume"><i class="icon-volume-off"></i></a>
	                   </div>';
				}


				break;




			case "product.php":

				if($_GET['id']) {

					$html_array['product'] = $inventory->loadProduct();

					$html_array['product_invoices'] = $inventory->loadProductInvoices();

					$html_array['currency'] = $account->getCurrency("");

					if($account->hasExportAccess() == true) {
						$html_array['export_tools'] = '<div class="btn-group pull-right tools">
														<button class="btn dropdown-toggle" data-toggle="dropdown">Tools <i class="icon-angle-down"></i>
														</button>
														<ul class="dropdown-menu pull-right">
															<li><a href="#" class="print">Print</a></li>
															<li><a href="#" class="save_as_pdf">Save as PDF</a></li>
															<li><a href="#" class="export_to_excel">Export as CSV (Excel)</a></li>
														</ul>
													</div>';
					}

				} else {

					header('Location: inventory');
				}

				break; 






			case "open_chat.php":

				if($_SESSION['sound'] == "on") {

					$html_array['chat_sound_btn'] = '<div class="btn-sound on"> 
	                      <a href="" class="btn light-blue icn-only toggle_volume"><i class="icon-volume-up"></i></a>
	                   </div>';

				} else if($_SESSION['sound'] == "off") {

					$html_array['chat_sound_btn'] = '<div class="btn-sound off"> 
	                      <a href="" class="btn icn-only toggle_volume"><i class="icon-volume-off"></i></a>
	                   </div>';
				}


				break;


			case "messages.php":
				require_once("classes/Chat.class.php");
				$chat = new Chat();

				$user->clearMessagesNotifications();
				$html_array['msg_notif_count'] = 0; //This overrides the above set notification count. Only used on this page (Messages.php).

				$html_array['conversations_list'] = $chat->loadConversationList();


				if($_GET['conv_id']) {

					$html_array['messages_list_span'] = "span4";
					$html_array['message_box_span'] = "span8";

					//checkConvoId is different from findConversation. checkConvoId is checking for a convo by its ID. findConversation is looking for a conversation based on the user ID provided.
					$conv_id_found = $chat->checkConvoId($_GET['conv_id']);

					if($conv_id_found == true) {

						//do nothing, all good.

					} else {

						//no conv_id found, send them to messages page.
						header('Location: messages');
					}
					
					//$html_array['conversation'] = $chat->loadConversation();
				
				} else if($_GET['u_id']) {

					$html_array['messages_list_span'] = "span4";
					$html_array['message_box_span'] = "span8";

					//check to see if u_id exists within this account
					$u_id_found = $chat->checkUserId($_GET['u_id']);

					if($u_id_found == true) {

						//try to find existing conversation between you and other user
						$convo_id = $chat->findConversation();

						if($convo_id > 0) {

							header('Location: messages?conv_id=' . $convo_id);
						
						} else {

							//do nothing. If the u_id actually does exists AND there is no convo_id found, then by default it's a new conversation. 
							//Once page is loaded, look for u_id in URL and use it to open the New Message portlet with user's Name prepopulated.
						}
					} else {

						header('Location: messages');
					}
				
				} else {

					$html_array['messages_list_span'] = "span12";
					$html_array['message_box_span'] = "span0";
				}



				break;



			case "notes.php":

				$html_array['notes'] = $account->loadUserNotes();


				break;


			case "tasks.php":

				$html_array['tasks_list'] = $account->loadTasksList();


				break;

			case "calendar.php":


				break;

			case "work_schedule.php":


				break;


			case "clients.php":

				$clients = $client->loadClientsList();
				$html_array['clients_table'] = $clients['table_view'];
				$html_array['clients_grid'] = $clients['grid_view'];

				if($account->hasExportAccess() == true) {
					$html_array['export_tools'] = '<div class="btn-group pull-right tools">
													<button class="btn dropdown-toggle" data-toggle="dropdown">Tools <i class="icon-angle-down"></i>
													</button>
													<ul class="dropdown-menu pull-right">
														<li><a href="#" class="print">Print</a></li>
														<li><a href="#" class="save_as_pdf">Save as PDF</a></li>
														<li><a href="#" class="export_to_excel">Export as CSV (Excel)</a></li>
													</ul>
												</div>';
				}
				
				
				break; 
				
			case "employees.php":
			case "team_members.php":
			case "associates.php":

				$employees = $account->loadEmployeeList();
				$html_array['emp_table'] = $employees['table_view'];
				$html_array['emp_grid'] = $employees['grid_view'];

				if($account->hasExportAccess() == true) {
					$html_array['export_tools'] = '<div class="btn-group pull-right tools">
													<button class="btn dropdown-toggle" data-toggle="dropdown">Tools <i class="icon-angle-down"></i>
													</button>
													<ul class="dropdown-menu pull-right">
														<li><a href="#" class="print">Print</a></li>
														<li><a href="#" class="save_as_pdf">Save as PDF</a></li>
														<li><a href="#" class="export_to_excel">Export as CSV (Excel)</a></li>
													</ul>
												</div>';
				}
				
				
				break;



			case "inventory.php":

				$html_array['categories_list'] = $inventory->loadCategories();

				$html_array['products'] = $inventory->loadProducts();
				
				
				break;

			case "profile.php":

				$html_array['profile'] = $user->loadUserProfile();

				if($_GET['id']) {

		    		$user_id = $_GET['id'];

		    	} else {

		    		$user_id = $_SESSION['user_id'];
		    	}

				$html_array['user_timeline'] = $account->loadTimeline($user_id);

 				$pages_array['profile.php'] = $html_array['profile']['first_name'] . ' ' . $html_array['profile']['last_name'];
				
				break;	


			case "client_profile.php":

				$html_array['client_id'] = $_GET['id'];

				$html_array['client_profile'] = $client->loadClientProfile(); 

				$html_array['client_notes'] = $client->loadClientNotes(); 

				$html_array['client_invoices'] = $invoice->loadClientInvoices();

				$html_array['client_files'] = $client->loadClientFiles();

				if($account->hasExportAccess() == true) {
					$html_array['export_tools'] = '<div class="btn-group pull-right tools">
													<button class="btn dropdown-toggle" data-toggle="dropdown">Tools <i class="icon-angle-down"></i>
													</button>
													<ul class="dropdown-menu pull-right">
														<li><a href="#" class="print">Print</a></li>
														<li><a href="#" class="save_as_pdf">Save as PDF</a></li>
														<li><a href="#" class="export_to_excel">Export as CSV (Excel)</a></li>
													</ul>
												</div>';
				}
				
				
				break;	


			case "profile_settings.php":

				$html_array['profile_settings'] = $user->loadProfileSettings();

				$html_array['timezone_select'] = $account->timezoneList($html_array['profile_settings']['country']);

				$html_array['countries'] = $account->getAbrCountries();	
				

				break;


				
			case "maps.php":

				$html_array['saved_locations'] = $account->loadSavedLocations();

				$html_array['clients_list'] = $client->clientsListDropdown();

				if($_GET['l_id']) {

					$html_array['ref_loc'] = $account->getReferredLocation();
				}


				break;


			case "lock.php": 

				$html_array = $user->lockSession();

				break;


			case "invoices.php":


				$html_array['invoices'] = $invoice->loadInvoices();

				if($account->hasExportAccess() == true) {
					$html_array['export_tools'] = '<div class="btn-group pull-right tools">
													<button class="btn dropdown-toggle" data-toggle="dropdown">Tools <i class="icon-angle-down"></i>
													</button>
													<ul class="dropdown-menu pull-right">
														<li><a href="#" class="print">Print</a></li>
														<li><a href="#" class="save_as_pdf">Save as PDF</a></li>
														<li><a href="#" class="export_to_excel">Export as CSV (Excel)</a></li>
													</ul>
												</div>';
				}


				break;


			case "recurring_invoices.php":


				$html_array['recurring_invoices'] = $invoice->loadRecurringInvoices();

				if($account->hasExportAccess() == true) {
					$html_array['export_tools'] = '<div class="btn-group pull-right tools">
													<button class="btn dropdown-toggle" data-toggle="dropdown">Tools <i class="icon-angle-down"></i>
													</button>
													<ul class="dropdown-menu pull-right">
														<li><a href="#" class="print">Print</a></li>
														<li><a href="#" class="save_as_pdf">Save as PDF</a></li>
														<li><a href="#" class="export_to_excel">Export as CSV (Excel)</a></li>
													</ul>
												</div>';
				}


				break;


			case "new_invoice.php":

				if($_GET['id'] != '' && $_GET['id'] > 0) {

					$html_array['create_invoice'] = $invoice->loadCreateInvoice();

					$html_array['invoice_files'] = $invoice->loadInvoiceFiles('single_invoice');


					if($html_array['create_invoice']['preferences']['discounts'] == "supervisor" && $_SESSION['account_type'] == 3) {

						$supervisors = $account->getSupervisors();

						$html_array['create_invoice']['supervisor_required'] = '<hr>
							<p>Provide supervisor\'s credentials.</p>
							<input type="hidden" id="verify_supervisor_account" value="1" />
		                    <div class="row-fluid">
		                       <div class="span6 ">
		                          <div class="control-group">
		                            <label class="control-label">Supervisor</label>
		                            <div class="controls">
		                               <select name="super_username" id="super_username" class="m-wrap span12">';

		               	$html_array['create_invoice']['supervisor_required'] .= $supervisors;

		                $html_array['create_invoice']['supervisor_required'] .= '</select>
		                            <span class="help-block hidden verify_supervisor_error"></span>
		                            </div>
		                          </div>
		                       </div>

		                       <div class="span6 ">
		                          <div class="control-group">
		                            <label class="control-label">Password</label>
		                            <div class="controls">
		                               <input type="password" name="super_password" id="super_password" class="m-wrap span12" />
		                            </div>
		                          </div>
		                       </div>
		                       
		                    </div>';
					}

				} else if($_GET['like'] != '') {

					$invoice_id = $invoice->cloneInvoice();

					header('Location: new_invoice?id=' . $invoice_id);

				} else {

					$invoice_id = $invoice->createNewInvoice();

					header('Location: new_invoice?id=' . $invoice_id);

				}


				break;


			case "new_recurring_invoice.php":

				if($_GET['id'] != '' && $_GET['id'] > 0) {

					$html_array['create_invoice'] = $invoice->loadCreateRecurringInvoice();

					$html_array['invoice_files'] = $invoice->loadInvoiceFiles('recurring_invoice');


					if($html_array['create_invoice']['preferences']['discounts'] == "supervisor" && $_SESSION['account_type'] == 3) {

						$supervisors = $account->getSupervisors();

						$html_array['create_invoice']['supervisor_required'] = '<hr>
							<p>Provide supervisor\'s credentials.</p>
							<input type="hidden" id="verify_supervisor_account" value="1" />
		                    <div class="row-fluid">
		                       <div class="span6 ">
		                          <div class="control-group">
		                            <label class="control-label">Supervisor</label>
		                            <div class="controls">
		                               <select name="super_username" id="super_username" class="m-wrap span12">';

		               	$html_array['create_invoice']['supervisor_required'] .= $supervisors;

		                $html_array['create_invoice']['supervisor_required'] .= '</select>
		                            <span class="help-block hidden verify_supervisor_error"></span>
		                            </div>
		                          </div>
		                       </div>

		                       <div class="span6 ">
		                          <div class="control-group">
		                            <label class="control-label">Password</label>
		                            <div class="controls">
		                               <input type="password" name="super_password" id="super_password" class="m-wrap span12" />
		                            </div>
		                          </div>
		                       </div>
		                       
		                    </div>';
					}

				} else {

					$invoice_id = $invoice->createNewRecurringInvoice();

					header('Location: new_recurring_invoice?id=' . $invoice_id);

				}


				break;


			case "invoice.php": 

				$html_array['view_invoice'] = $invoice->viewInvoice();

				$html_array['invoice_files'] = $invoice->loadInvoiceFiles('single_invoice');


				break;


			case "recurring_invoice.php": 

				$html_array['view_invoice'] = $invoice->viewRecurringInvoice();

				$html_array['invoice_files'] = $invoice->loadInvoiceFiles('recurring_invoice');


				break;


			case "storage.php":

				break;


			case "statistics.php":

				break;


			case "account_settings.php":

				$html_array['business_profile'] = $account->loadBusinessProfile();

				$html_array['countries'] = $account->getAbrCountries();

				if($html_array['business_profile']['logo'] != '') {

					$html_array['current_logo'] = '<img src="https://cdn.ironvault.ca/logo/' . $html_array['business_profile']['logo'] . '" alt="logo" class="current_logo" />
            		<a href="#" class="btn remove_logo">Remove</a>';

            	} else {

            		$html_array['current_logo'] = '<img src="" alt="logo" class="current_logo" style="display:none;"/>
            		<a href="#" class="btn remove_logo" style="display:none;" >Remove</a>';
            	}

				$html_array['currencies'] = $account->getAbrCurrencies();

				$html_array['preferences'] = $account->loadAccountPreferences();

				$html_array['user_accounts_list'] = $account->loadUserAccountsList(); //different from employees list.

				//$html_array['user_permissions'] = $account->loadUserPermissions();

				$html_array['module_settings'] = $account->loadModuleSettings();

				$html_array['plan_payments'] = $account->loadPlanPayments(); //Literally loads everything for the Plan & Payments tab. Stored in an array.


				break;


			case "setup_wizard.php":

				$account->initWizardCompleteCheck();

				break;


			case "join.php":

				$html_array['result'] = $account->verifyInvitation();

		}


		//Notifications Load
		$notifications = $user->loadNotifications();

		if($notifications[0] > 0) {

			$html_array['messages_notifications'] = '<span class="badge messages_notification_badge">' . $notifications[0] . '</span>';
		}

		if($notifications[1] > 0) {

			$html_array['events_notifications'] = '<span class="badge events_notification_badge">' . $notifications[1] . '</span>';
		}

		if($notifications[2] > 0) {

			$html_array['tasks_notifications'] = '<span class="badge tasks_notification_badge">' . $notifications[2] . '</span>';
		}

		//raw counters for sending to javascript in footer.php
		$html_array['msg_notif_count'] = $notifications[0];
		$html_array['event_notif_count'] = $notifications[1];
		$html_array['task_notif_count'] = $notifications[2];

		$html_array['total_notif_count'] = $notifications[0] + $notifications[1] + $notifications[2];


		if($html_array['total_notif_count'] > 0 && $page != "lock.php") {
			$html_array['page_title'] = "(" . $html_array['total_notif_count'] . ") " . $pages_array[$page] . " – " . $_SESSION['business_name'] . " | Iron Vault"; 
		} else {
			$html_array['page_title'] = $pages_array[$page] . " – " . $_SESSION['business_name'] . " | Iron Vault";
		}


		return $html_array;
	}



	/* ************************ */


	function load_footer_scripts() {

		$page = basename($_SERVER['PHP_SELF']);

		switch($page) {


			case "login.php":
				$data = '
					<script src="scripts/plugins.min.js"></script>
					<script src="scripts/pages.js"></script>
					<script>
						jQuery(document).ready(function() {     
						  App.init();
						  Login.init();
						  Lock.init();
						});
					</script>
					<!-- END JAVASCRIPTS -->
					';
				break;


			case "password_reset.php":
				$data = '
					<script src="scripts/plugins.min.js"></script>
					<script src="scripts/pages.js"></script>
					<script>
						jQuery(document).ready(function() {     
						  App.init();
						  Login.init();
						  Lock.init();
						});
					</script>
					<!-- END JAVASCRIPTS -->
					';
				break;


			case "password_change.php":
				$data = '
					<script src="scripts/plugins.min.js"></script>
					<script src="scripts/pages.js"></script>
					<script>
						jQuery(document).ready(function() {     
						  App.init();
						  Login.init();
						  Lock.init();
						});
					</script>
					<!-- END JAVASCRIPTS -->
					';
				break;


			case "lock.php":
				$data = '   
					<script src="scripts/plugins.min.js"></script>
					<script src="scripts/pages.js"></script>
					<script>
						jQuery(document).ready(function() {    
						   App.init();
						   Lock.init();
						});
					</script>
					<script src="scripts/custom.js"></script>
					<!-- END JAVASCRIPTS -->
					';
				break;


			case "setup_wizard.php":
				$data = '
					<script src="scripts/plugins.min.js"></script>
					<script src="scripts/pages.js"></script>
					<script>
						jQuery(document).ready(function() {     
						  App.init();
						  BG.init();
						  SetupWizard.init();
						});	 
					</script>
					<script src="scripts/custom.js"></script>
					<!-- END JAVASCRIPTS -->
					';
				break;


			case "join.php":
				$data = '
					<script src="scripts/plugins.min.js"></script>
					<script src="scripts/pages.js"></script>
					<script>
						jQuery(document).ready(function() {     
						  App.init();
						  BG.init();
						});	 
					</script>
					<script src="scripts/custom.js"></script>
					<!-- END JAVASCRIPTS -->
					';
				break;


			case "dashboard.php":
				$data = '
					<script src="https://ironvault.ca:8080/socket.io/socket.io.js"></script>
					<script src="chat/open_chat.js"></script> 
					<script src="scripts/chat.js"></script> 
					<script>
						jQuery(document).ready(function() {       
						   // initiate layout and plugins
						   App.init();
						});
					</script>
					<!-- END JAVASCRIPTS -->
					';
				break;


			case "search.php":
				$data = '
					<script>
						jQuery(document).ready(function() {       
						   // initiate layout and plugins
						   App.init();
						});
					</script>
					<!-- END JAVASCRIPTS -->
					';
				break;


			case "messages.php":
				$data = '
					<script>
						jQuery(document).ready(function() {       
						   // initiate layout and plugins
						   App.init();
						   TableEditable.init();
						});
					</script>
					<!-- END JAVASCRIPTS -->
					';
				break;



			case "message.php":
				$data = '
					<script src="https://www.ironvault.ca:8082/socket.io/socket.io.js"></script>
	                <script src="private_messages/private_messages.js"></script>
	                <script src="scripts/messages.js"></script>
					<script>
						jQuery(document).ready(function() {       
						   // initiate layout and plugins
						   App.init();
						   TableEditable.init();
						});
					</script>
					<!-- END JAVASCRIPTS -->
					';
				break;



			case "calendar.php":
				$data = '  
					<script>
						jQuery(document).ready(function() {       
						   // initiate layout and plugins
						   App.init();
						   FormComponents.init();
						});
					</script>
					<!-- END PAGE LEVEL SCRIPTS -->
					';
				break;


			case "work_schedule.php":
				$data = '   
					<script>
						jQuery(document).ready(function() {       
						   // initiate layout and plugins
						   App.init();
						   FormComponents.init();
						});
					</script>
					<!-- END PAGE LEVEL SCRIPTS -->
					';
				break;


			case "tasks.php":
				$data = '
					<script>
						jQuery(document).ready(function() {       
						   // initiate layout and plugins
						   App.init();
						   
						});
					</script>
					<!-- END PAGE LEVEL SCRIPTS -->
					';
				break;


			case "notes.php":
				$data = '  
					<script>
						jQuery(document).ready(function() {       
						   // initiate layout and plugins
						   App.init();
						   
						});
					</script>
					<!-- END PAGE LEVEL SCRIPTS -->
					';
				break;


			case "profile.php":
				$data = '
					<script>
						jQuery(document).ready(function() {       
						   // initiate layout and plugins
						   App.init();
						});
					</script>
					<!-- END JAVASCRIPTS -->
					';
				break;
				
			case "account_settings.php":
				$data = '
					<script>
						jQuery(document).ready(function() {       
						   // initiate layout and plugins
						   App.init();
						   FormComponents.init();
						   TableEditable.init();
						});
					</script>
					<!-- END JAVASCRIPTS -->
					';
				break;


			case "profile_settings.php":
				$data = '
					<script>
						jQuery(document).ready(function() {       
						   // initiate layout and plugins
						   App.init();
						   FormComponents.init();
						});
					</script>
					<!-- END JAVASCRIPTS -->
					';
				break;


			case "invoices.php":
				$data = '
					<script>
						jQuery(document).ready(function() {       
						   // initiate layout and plugins
						   App.init();
						   TableEditable.init();
						});
					</script>
					<!-- END JAVASCRIPTS -->
					';
				break;


			case "recurring_invoices.php":
				$data = '
					<script>
						jQuery(document).ready(function() {       
						   // initiate layout and plugins
						   App.init();
						   TableEditable.init();
						});
					</script>
					<!-- END JAVASCRIPTS -->
					';
				break;


			case "new_invoice.php":
			case "new_recurring_invoice.php":
				$data = '
						<script src="https://maps-api-ssl.google.com/maps/api/js?v=3.exp&libraries=places&sensor=false"></script>
						<script>
							jQuery(document).ready(function() {       
							   // initiate layout and plugins
							   App.init();
							});
						</script>
						<!-- END JAVASCRIPTS -->
					';
				break;


			case "product.php":
				$data = '
						<script>
							jQuery(document).ready(function() {       
							   // initiate layout and plugins
							   App.init();
							   TableEditable.init();
							});
						</script>
						<!-- END JAVASCRIPTS -->
					';
				break;


			case "client_profile.php":
				$data = '
					<script src="https://maps-api-ssl.google.com/maps/api/js?v=3.exp&libraries=places&sensor=false"></script>
					<script src="plugins/gmaps/gmaps.js" type="text/javascript"></script>
						<script>
							jQuery(document).ready(function() {       
							   // initiate layout and plugins
							   App.init();
							   TableEditable.init();
							});
						</script>
						<!-- END JAVASCRIPTS -->
					';
				break;

			case "clients.php":
				$data = ' 
						<script src="https://maps-api-ssl.google.com/maps/api/js?v=3.exp&libraries=places&sensor=false"></script>
						<script>
							jQuery(document).ready(function() {       
								App.init();
								TableEditable.init();
							}); 

						</script>
						';
				break;	
				
				
				 
			case "inventory.php":
				$data = '  
							<script>
								jQuery(document).ready(function() {       
								App.init();
								TableEditable.init();
								}); 

							</script>
							';
				break;		
				
			case "employees.php":
			case "team_members.php":
			case "associates.php":
				$data = '  
							<script> 
								jQuery(document).ready(function() {       
								App.init();
								TableEditable.init();
								}); 

							</script>
							';
				break;	
				
					

			case "maps.php":
				$data = '
					<script src="https://maps-api-ssl.google.com/maps/api/js?v=3.exp&libraries=places&sensor=false"></script>
					<script src="plugins/gmaps/gmaps.js" type="text/javascript"></script>
					<script>
						jQuery(document).ready(function() {    
						   App.init();
						   
						});
					</script>
					<!-- END JAVASCRIPTS -->

					';
				break;


			case "statistics.php":
				$data = '    
						<script>
							jQuery(document).ready(function() {       
							   //initiate layout and plugins
							   App.init();
							   Index.initDashboardDaterange();
							   
							});
						</script>
						<!-- END PAGE LEVEL SCRIPTS -->

					';
				break;


			case "404.php":
				$data = '    
					<script>
					  jQuery(document).ready(function() {    
					     App.init();
					  });
					</script>
					<!-- END JAVASCRIPTS -->
					';
				break;


			case "storage.php":

				$data = '
					<script>
					  jQuery(document).ready(function() {    
					     App.init();
					     
					  });
					</script>
					<!-- END JAVASCRIPTS -->
					';
				break;


			default:
				$data = '    
					<script>
					  jQuery(document).ready(function() {    
					     App.init();
					  });
					</script>
					<!-- END JAVASCRIPTS -->
					';
				break;
		}

		return $data;
	}


?>