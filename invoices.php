<?php
   require_once("inc/session_start.php");
   require_once("functions.php");

   $html_array = load_page_html();

   require_once("inc/head.php");
?>
<!-- BEGIN BODY -->
<body class="fixed-top page-sidebar-fixed <?php echo $html_array['page_sidebar_closed']; ?>">
   <?php require_once("inc/top_menu.php"); ?>
   <!-- BEGIN CONTAINER -->   
   <div class="page-container row-fluid">

      <?php require_once("inc/main_menu.php"); ?>

      <!-- BEGIN PAGE -->
      <div class="page-content">
         <div class="container-fluid">

				<!-- BEGIN PAGE CONTENT-->
				<div class="row-fluid">
					<div class="span12">

						<!-- BEGIN EXAMPLE TABLE PORTLET-->
						<div class="portlet box blue">
							<div class="portlet-title">
								<div class="caption"><i class="icon-book"></i>Invoices</div>
							</div>
							
							<div class="portlet-body no-more-tables">
								<div class="clearfix"> 
									<div class="btn-group">
										<button class="btn light-green dropdown-toggle" data-toggle="dropdown"><i class="icon-book"></i>&nbsp;&nbsp;New Invoice&nbsp;&nbsp;<i class="icon-angle-down"></i></button>
										<ul class="dropdown-menu pull-right">
											<li><a href="new_invoice">Single Invoice</a></li>
											<li><a href="new_recurring_invoice">Recurring Invoice</a></li>
										</ul>
									</div>
								
									<?php echo $html_array['export_tools']; ?>

									<br>
									<select name="invoice_type" id="invoice_type">
										<option value="single" selected>Single Invoices</option>
										<option value="recurring">Recurring Invoices</option>
									</select>
									<br>
								</div>
								<table class="table table-bordered table-striped table-condensed cf invoices saveaspdf" id="data_table">
									<thead class="cf">
										<tr>
											<th class="sorting">Invoice #</th>
											<th class="sorting">Client</th>
											<th class="sorting">Date Created</th>
											<th class="sorting">Last Modified</th>
											<th class="sorting">Prepared By</th>
											<th class="sorting">Status</th>
											<th class="sorting">Total</th>
											<th>Action</th>
										</tr>
									</thead>
									<tbody>

										<?php echo $html_array['invoices']; ?>

									</tbody>
								</table>
							</div>
						</div>
						<!-- END EXAMPLE TABLE PORTLET-->
					</div>
				</div>
				<!-- END PAGE CONTENT -->
			</div>
		 
		 
		  
		 
		 
		 
		 
		 
		 
         <!-- END PAGE CONTAINER-->       
      </div>
      <!-- BEGIN PAGE -->
   </div>
   <!-- END PAGE CONTAINER--> 
   <!-- END CONTAINER -->
   <?php
      require_once("inc/footer.php");
   ?>
</body>
<!-- END BODY -->
</html>
<?php
   require_once("inc/session_end.php");
?>